<?php
require APP_ROOT . 'application/libraries/packageinfo.php';

class Misc {

    private $licPath;
    private $tracker;

    const WARN_THRESHOLD = 30;
    const CHECK_INTERVAL = 3600;

    public function __construct() {
        if (!TB_ON_SITE_MODE) {
            return;
        }

        ini_set('display_errors', 'Off');
        $this->licPath = APP_ROOT . 'application/config/talentbase.lic';
        $this->tracker = sys_get_temp_dir() . '/' . md5($this->licPath);

        $this->initCheck();
    }

    private function initCheck() {
        if (!file_exists($this->licPath)) {
            $this->_die('Application\'s license was not found.');
        }

        $contents = preg_split('/[\r\n]+/', @gzdecode(file_get_contents($this->licPath)));

        if (count($contents) < 4) {
            $this->_die('License file is corrupt');
        }

        array_walk($contents, function(&$line) {
            $line = trim($line);
        });

        $hash = array_pop($contents);
        $hash2 = base64_encode(sha1(join('', $contents)));
        if ($hash !== $hash2) {
            $this->_die('License file is not valid.');
        }

        $info = json_decode($contents[1], true);
        if ($info === false) {
            $this->_die('License file is corrupt.');
        }

        $this->checkInfo($info);
        $this->_serverVerify();
    }

    private function checkInfo($info) {
        if (Packageinfo::instance()->get('name') !== $info['package']) {
            $this->_die('Invalid application license in use.');
        }

        $daysToExpiry = (strtotime("+{$info['validity']} months", strtotime($info['activation_date'])) - time()) / 86400;

        if ($daysToExpiry < 0) {
            $this->_die('Application license has expired.');
        }

        if ($daysToExpiry <= self::WARN_THRESHOLD) {
            $this->_warn("Your appllication license will expire in {$daysToExpiry} day(s)!!!");
        }
    }

    private function _serverVerify() {
        if (file_exists($this->tracker)) {
            $f = intval(file_get_contents($this->tracker));
        } else {
            $f = 0;
        }

        if (!file_exists($this->licPath)) {
            return;
        }

        if ((time() - $f) > self::CHECK_INTERVAL) {
            //perform check

            $c = preg_split('/[\r\n]+/', gzdecode(file_get_contents($this->licPath)));
            $info = json_decode($c[1], true);

            if ($info === false) {
                return;
            }

            $context = stream_context_create(array(
                'http' => array(
                    'method' => 'POST',
                    'content' => http_build_query(array('email' => $info['email'], 'key' => trim($c[0]))),
                    'header' => "Content-Type: application/x-www-form-urlencoded",
                    'timeout' => 15,
                    'ignore_errors' => true,
                )
            ));

            $content = file_get_contents(Packageinfo::instance()->get('server') . 'license/activate', false, $context);
            file_put_contents($this->tracker, time());

            $statusCode = 0;
            foreach ($http_response_header as $header) {
                if (preg_match('/^HTTP\/1\.\d/', $header)) {
                    $statusCode = intval(preg_split('/\s+/', $header)[1]);
                    break;
                }
            }


            if ($statusCode === 400) {
                $this->_die('Your license is not valid! Please ensure your server\'s IP address has not changed.');
            }
            //save check
            file_put_contents($this->licPath, gzencode($content));
        }
    }

    private function _warn($str) {
        $GLOBALS['osLicenseError'] = $str;
    }

    private function _die($error) {
        /* @var $session CI_Session */
        //$session = load_class('session');
        $sessionAdmin = true; //intval($session->userdata('account_type')) === USER_TYPE_ADMIN;
        ?>
        <html>
            <head>
                <title><?= $sessionAdmin ? 'License Error' : 'Application Error' ?> </title>
            </head>
            <body>
                <div style="margin: 20px auto; ;font-family: sans-serif; font-size: 12px; width: 400px; padding: 15px; color: #900; border: solid 1px #ff9933; border-radius: 5px;">
                    <h1 style="font-size: 14px; border-bottom: solid 1px #ccc;"><?= $sessionAdmin ? 'License Error' : 'Application Error' ?></h1>
                    <p>
                        <?= $sessionAdmin ? $error : 'It appears there\'s an issue with the application\'s license, please contact your administrator.' ?>
                    </p>

                    <?php if ($sessionAdmin): ?>
                        <div>
                            To continue, <a href="/setup/index.php?step=license&redo=true">fix license error</a> or contact <?= TALENTBASE_SUPPORT_EMAIL; ?>.
                        </div>
                    <?php endif; ?>
                </div>
            </body>
        </html>
        <?php
        exit;
    }

}
