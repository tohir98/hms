<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace packager;

use Robo\Task\FileSystemStackTask;
use Robo\Tasks;
use Zipper;

/**
 * Description of PackagerBuilder
 *
 * @author JosephT
 */
class PackageBuilder extends Tasks {

    protected $package;
    protected $modules = array();
    protected $sysModules;
    protected $excludes = ['tb', 'license', 'onpremise'];
    protected $tempDir;
    protected $outDir;
    protected $serverUrl;

    const MIN_REV = 403;

    public function __construct($package, $temp, $outDir, $serverUrl='https://portal.talentbase.ng') {
        $this->package = $package;
        if (!OnsiteVersions::exists($package)) {
            throw new Exception("The specified package was not found  : {$package}");
        }


        $this->sysModules = OnsiteVersions::getSystemModules();
        $modules = OnsiteVersions::getModules($package);
        $this->modules = empty($modules) ? $this->sysModules : $modules;
        $this->tempDir = $temp . '/' . $package . DIRECTORY_SEPARATOR;
        $this->outDir = $outDir;
        $this->serverUrl = $serverUrl;
    }
    
    public function getServerUrl() {
        return $this->serverUrl;
    }

    public function setServerUrl($serverUrl) {
        $this->serverUrl = $serverUrl;
        return $this;
    }

    
    public function build($publish = false) {
        $this->say('cleaning temp directory');
        if (file_exists($this->tempDir)) {
            try {
                $this->taskDeleteDir($this->tempDir);
            } catch (Exception $ex) {
                $this->say('something went wrong while removing temp directory');
            }
        }

        if (!mkdir($this->tempDir, 0755, true)) {
            throw new Exception('Could not continue, error while creating temp directory.');
        }

        $this->tempDir = realpath($this->tempDir) . DIRECTORY_SEPARATOR;

        $this->copyFiles();
        $this->zipFile();
        if ($publish) {
            $this->publish();
        }
    }

    private function publish() {
        $this->say('Publishing to storage server');
        $f = $this->getPackageFile();
        $uploadStatus = OnsiteVersions::storage()->getAdapter()->saveFile($f, basename($f));
        if ($uploadStatus) {
            $this->say('Upload successful');
            //write to database
            if (OnsiteVersions::saveRelease($this->package, null, 'Latest release as at ' . date('Y-m-d'))) {
                $this->say('Release has been saved to release table');
            } else {
                $this->yell('Could not save release to DB');
            }
        } else {
            $this->yell('Upload failed');
        }
    }

    private function getPackageFile() {
        return realpath($this->outDir) . DIRECTORY_SEPARATOR . OnsiteVersions::getPackageFile($this->package);
    }

    private function zipFile() {
        $this->say('Creating Zip file');
        $op = $this->getPackageFile();
        $this->say('Output to : ' . $op);
        Zipper::zipDir(str_replace('\\', '/', $this->tempDir), $op, false);
        $this->say('Zip completed.');
    }

    private function copyFiles() {
        //root files 
        $currentWorkDir = getcwd();

        chdir(APP_ROOT);

        /* @var $fsTaskStack FileSystemStackTask */


        $files = ['index.php', 'RoboFile.php', '.htaccess', 'composer.json', 'composer.lock'];
        $mixed = $directories = array();
        //copy system folder.
        $directories[] = 'system';
        $directories[] = 'setup';
        //copy static resources: cal, css, js, img, font
        $this->push($directories, ['cal', 'css', 'js', 'img', 'font']);
        //selectively copy cron jobs
        $this->push($files, $this->getSelectedCronFiles());

        //copy DBVC and revisions, skip *.csv and temp.php
        $this->push($directories, 'dbvc/adapters');
        $this->push($files, 'dbvc/migration.php', 'dbvc/DBMigration.php', 'dbvc/revisions/.htaccess', 'dbvc/bad_revisions_default.txt', $this->getFilteredRevisions());

        //---in application---//
        $skipInAppl = ['config', 'controllers', 'views', 'models', 'logs'];
        foreach ($this->ls('application') as $file) {
            if (!in_array(basename($file), $skipInAppl)) {
                $mixed[] = $file;
            }
        }

        //selective: copy configs 
        $this->push($files, $this->getConfigs());
        //selective: controllers
        //selective : views
        $this->push($mixed, $this->selectModuleDirs('application/controllers'), $this->selectModuleDirs('application/views'), $this->selectModuleDirs('application/models', true));
        //put onsite mode
        foreach ($mixed as $fOrD) {
            if (is_dir($fOrD)) {
                $directories[] = $fOrD;
            } else {
                $files[] = $fOrD;
            }
        }

//        foreach (array_merge($directories, $files) as $cc) {
//            echo $cc, PHP_EOL;
//        }
//        exit;
        $fsTaskStack = $this->taskFileSystemStack();
        foreach ($files as $file) {
            $fsTaskStack->copy($file, $this->tempDir . $file);
        }

        //copy blank db structure
        $fsTaskStack->copy(__DIR__ . '/blank_atversion_403.sql', $this->tempDir . 'dbvc/revisions/default/1_init_db_toV403.sql');
        
        //copy cron job
        $fsTaskStack->copy(__DIR__ . '/op_cron.php', $this->tempDir . 'cron/_onpremise.php');

        if(!file_exists(OnsiteVersions::versionFile())){
            PackagerTasks::updateVersionFile(OnsiteVersions::version());
        }
        
        $fsTaskStack->copy(OnsiteVersions::versionFile(), $this->tempDir . 'VERSION');
        
        $dirsToCopy = array();
        foreach ($directories as $dir) {
            $dirsToCopy[$dir] = $this->tempDir . $dir;
        }

        $this->say('Copying directories');
        $this->taskCopyDir($dirsToCopy)
                ->run();

        $fsTaskStack->run();

        //make tem file store
        mkdir($this->tempDir . 'files');
        //write on site file
        file_put_contents($this->tempDir . 'application/config/.ONSITE_MODE', '1');

        //write package info
        file_put_contents($this->tempDir . 'application/config/packageinfo.json', $this->getPackageInfo());

        //write production
        file_put_contents($this->tempDir . '.htaccess', "\n\nSetEnv TB_ENV production\n", FILE_APPEND);

        $mFile = $this->tempDir . 'system/core/Misc.php';

        file_put_contents($mFile, $this->encodeFile(file_get_contents($mFile)));

        chdir($currentWorkDir);
    }

    private function getFilteredRevisions() {
        $revDir = 'dbvc/revisions/default';
        $files = [];
        foreach ($this->ls($revDir) as $aRev) {
            $macthes = [];
            if (preg_match('/^(\d+)_.+\.sql$/i', basename($aRev), $macthes)) {
                $revNum = floatval($macthes[1]);
                if ($revNum > self::MIN_REV) {
                    $files[] = $aRev;
                }
            }
        }
        return $files;
    }

    private function shouldSkip($baseDir, $excludesOnly = false) {
        $b = basename($baseDir);
        if (in_array($b, $this->excludes)) {
            return true;
        }

        if (!$excludesOnly) {
            return in_array($b, $this->sysModules) && (empty($this->modules) || !in_array($b, $this->modules));
        }

        return false;
    }

    private function selectModuleDirs($dir, $excludesOnly = false) {
        return array_filter($this->ls($dir), function($d) use ($excludesOnly) {
            return !$this->shouldSkip($d, $excludesOnly);
        });
    }

    private function ls($dir) {
        return array_filter(glob($dir . '/{,.}*', GLOB_BRACE), function($b) use ($dir) {
            return $b != "{$dir}/." && $b != "{$dir}/..";
        });
    }

    private function getConfigs() {
        $configs = $this->ls('application/config');
        $files = [];
        $skip = ['email', 'storage', 'database'];

        $pattern = sprintf('/^(%s)(\..+)*\.php/', join(')|(', $skip));
        foreach ($configs as $configFile) {
            if (!preg_match($pattern, basename($configFile))) {
                $files[] = $configFile;
            }
        }

        return $files;
    }

    private function getPackageInfo() {
        $package = array(
            'name' => $this->package,
            'modules' => $this->modules,
            'build-date' => date('c'),
            'version' => OnsiteVersions::version(),
            'server' => trim($this->serverUrl, '\\/') . '/',
        );

        return json_encode($package, JSON_PRETTY_PRINT);
    }

    private function getSelectedCronFiles() {
        $files = ['cron/cron.php'];
        foreach ($this->modules as $module) {
            $cron_file = "cron/_{$module}.php";
            if (file_exists($cron_file)) {
                $files[] = $cron_file;
            }
        }

        return $files;
    }

    private function push(array &$array) {
        $numArgs = func_num_args();
        if (2 > $numArgs) {
            trigger_error(sprintf('%s: expects at least 2 parameters, %s given', __FUNCTION__, $numArgs), E_USER_WARNING);
            return false;
        }

        $values = func_get_args();
        array_shift($values);

        foreach ($values as $v) {
            if (is_array($v)) {
                foreach ($v as $w) {
                    $array[] = $w;
                }
            } else {
                $array[] = $v;
            }
        }

        return count($array);
    }

    protected function taskCopyDir($dirs) {
        return new MyCopyTask($dirs);
    }

    protected function encodeFile($fileContents) {
        $c = explode('<?php', $fileContents);
        array_shift($c);

        $code = strrev(base64_encode(join('<?php', $c)));

        return "<?php eval(base64_decode(strrev('{$code}')));";
    }

}
