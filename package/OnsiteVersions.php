<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace packager;

require_once APP_ROOT . 'application/libraries/storage.php';
require_once APP_ROOT . 'setup/db_utils.php';


define('PACKAGER_S3_BUCKET', 'tb-onsite-packages');
define('PACKAGER_S3_KEY', 'AKIAJ3IYQBYUYHWZKEQQ');
define('PACKAGER_S3_SECRET', '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');

use mysqli;

/**
 * Description of PackagerOnsiteVersions
 *
 * @author JosephT
 */
class OnsiteVersions {

    const PACKAGE_LITE = 'lite';
    const PACKAGE_STANDARD = 'standard';
    const PACKAGE_SUPER = 'super';

    protected static $buildVersion;
    protected static $MODULES = array(
        self::PACKAGE_SUPER => array(), //means all
        self::PACKAGE_LITE => array(
            'employee',
            'account_settings',
            'events',
            'personal',
            'loans',
        ),
        self::PACKAGE_STANDARD => array(
            'employee',
            'account_settings',
            'events',
            'personal',
            'loans',
            'auth',
            'employee_queries',
            'vacation',
            'training',
            'attendance',
        ),
    );

    public static function getModules($package = null) {
        return $package === null ? self::$MODULES : self::$MODULES[$package];
    }

    public static function exists($package) {
        return array_key_exists($package, self::$MODULES);
    }

    public static function getDb() {
        static $db = null;

        if (!$db || !$db->ping()) {
            require_once APP_ROOT . '/application/config/database.php';
            $dbMain = $db['default'];
            $db = new mysqli($dbMain['hostname'], $dbMain['username'], $dbMain['password'], $dbMain['database']);
            if ($db->connect_error || $db->error) {
                throw new Exception('DB Error while retrieving system modules: ' . ($db->connect_error ? : $db->error));
            }
        }


        return $db;
    }

    public static function getSystemModules() {
        static $modules = array();
        if (empty($modules)) {
            $result = self::getDb()->query("select id_string from modules");
            while (($row = $result->fetch_assoc())) {
                $modules[] = $row['id_string'];
            }
        }

        return $modules;
    }

    public static function getPackageFile($package, $version = null) {
        $version = $version ? : self::version();
        return 'talentbase_' . $package . '_' . $version . '.zip';
    }

    /**
     * @return \storage returns storage adapter
     */
    public static function storage() {
        static $storage = null;
        if (is_null($storage)) {
            $storage = new \storage(array(
                'adapter' => 'amazons3',
                'amazons3' => array(
                    'bucket' => PACKAGER_S3_BUCKET,
                    'access_key' => PACKAGER_S3_KEY,
                    'access_secret' => PACKAGER_S3_SECRET,
                )
                    ), true);
        }
        return $storage;
    }

    public static function getFullPathToPackage($package, $version = null) {
        return self::storage()->get_absolute_path(self::getPackageFile($package, $version));
    }

    public static function saveRelease($package, $semVer = null, $description = null, $minVersion = 0) {
        $data = array(
            'package_name' => $package,
            'version_string' => $semVer ? : self::version(),
            'version_number' => self::versionToInt($semVer ? : self::version()),
            'description' => $description,
            'package_file' => basename(self::getPackageFile($package, $semVer)),
            'min_version' => $minVersion ? : self::versionToInt(explode('.', self::version())[0]),
        );

        return db_util_insert(self::getDb(), 'tb_onpremise_release', $data);
    }

    public static function versionToInt($semVer) {
        $parts = explode('.', $semVer);
        $long = 0;
        for ($i = 0; $i < 3; $i++) {
            $val = isset($parts[$i]) ? intval($parts[$i]) : 0;
            $long += ($val << (8 * (2 - $i)));
        }
        return $long;
    }


    public static function setBuildVersion($v) {
        self::$buildVersion = $v;
    }

    public static function version() {
        static $version = null;

        if (self::$buildVersion) {
            return self::$buildVersion;
        }
        
        if (!$version) {
            if (file_exists($f = self::versionFile())) {
                $version = trim(file_get_contents($f));
            } else {
                $version = '1.0';
            }
        }
        
        return $version;
    }

    public static function versionFile() {
        return __DIR__ . '/BUILD_VERSION';
    }

}
