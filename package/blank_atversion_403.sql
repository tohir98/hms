/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.6.17-log : Database - talentbase_blank
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `accounts` */

DROP TABLE IF EXISTS `accounts`;

CREATE TABLE `accounts` (
  `accounttype` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`accounttype`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `accounts` */

insert  into `accounts`(`accounttype`,`name`,`status`,`created`,`modified`) values (1,'Super Administrator','Active','2013-02-15 00:00:00','2013-08-22 13:51:09'),(2,'Administrator','Active','2013-02-17 00:00:00','2014-04-04 10:00:40'),(3,'Candidate','Active','2013-02-17 00:00:00','0000-00-00 00:00:00'),(4,'Employee','Active','2013-08-22 00:00:00','0000-00-00 00:00:00'),(5,'Unit Manager','Active','2014-01-20 00:00:00','0000-00-00 00:00:00'),(6,'Recruiting','Active','2014-01-20 00:00:00','0000-00-00 00:00:00');

/*Table structure for table `admin_ref_questions` */

DROP TABLE IF EXISTS `admin_ref_questions`;

CREATE TABLE `admin_ref_questions` (
  `id_question` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `id_company` int(255) NOT NULL,
  PRIMARY KEY (`id_question`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `admin_ref_questions` */

/*Table structure for table `admins` */

DROP TABLE IF EXISTS `admins`;

CREATE TABLE `admins` (
  `id_user` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `admins` */

/*Table structure for table `applied_app_files` */

DROP TABLE IF EXISTS `applied_app_files`;

CREATE TABLE `applied_app_files` (
  `id_file` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_job` int(255) unsigned NOT NULL,
  `id_candidate` int(255) unsigned NOT NULL,
  `id_app_file` int(255) unsigned NOT NULL,
  `filename` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_file`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

/*Data for the table `applied_app_files` */

/*Table structure for table `applied_app_questions_answers` */

DROP TABLE IF EXISTS `applied_app_questions_answers`;

CREATE TABLE `applied_app_questions_answers` (
  `id_qa` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_job` int(255) unsigned NOT NULL,
  `id_candidate` int(255) unsigned NOT NULL,
  `question` text COLLATE latin1_general_ci,
  `answer` text COLLATE latin1_general_ci,
  PRIMARY KEY (`id_qa`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

/*Data for the table `applied_app_questions_answers` */

/*Table structure for table `applied_app_resume` */

DROP TABLE IF EXISTS `applied_app_resume`;

CREATE TABLE `applied_app_resume` (
  `id_file` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_job` int(255) unsigned NOT NULL,
  `id_candidate` int(255) unsigned NOT NULL,
  `filename` varchar(255) NOT NULL,
  `container` varchar(255) NOT NULL,
  PRIMARY KEY (`id_file`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `applied_app_resume` */

/*Table structure for table `applied_candidate_skills` */

DROP TABLE IF EXISTS `applied_candidate_skills`;

CREATE TABLE `applied_candidate_skills` (
  `id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_candidate` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_job` int(255) unsigned NOT NULL,
  `id_skill` int(255) unsigned NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `applied_candidate_skills` */

/*Table structure for table `applied_candidates_education` */

DROP TABLE IF EXISTS `applied_candidates_education`;

CREATE TABLE `applied_candidates_education` (
  `id_education` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_candidate` int(255) unsigned NOT NULL,
  `id_school` int(255) unsigned NOT NULL,
  `id_course` int(255) unsigned NOT NULL,
  `id_country` int(255) unsigned NOT NULL,
  `id_state` int(255) unsigned NOT NULL,
  `id_qualification` int(255) unsigned NOT NULL,
  `gpa` int(255) NOT NULL,
  `id_level` int(255) DEFAULT NULL,
  `on_going` int(255) NOT NULL DEFAULT '0',
  `id_company` int(255) NOT NULL,
  `id_job` int(255) unsigned NOT NULL,
  `id_cgpa` int(255) unsigned DEFAULT NULL,
  `date_start` timestamp NULL DEFAULT NULL,
  `date_end` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `institution_manual` varchar(255) DEFAULT NULL,
  `cgpa_manual` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_education`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `applied_candidates_education` */

/*Table structure for table `applied_candidates_experience` */

DROP TABLE IF EXISTS `applied_candidates_experience`;

CREATE TABLE `applied_candidates_experience` (
  `id_experience` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_candidate` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_job` int(255) unsigned NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `position_held` varchar(255) NOT NULL,
  `years_of_experience` int(255) unsigned NOT NULL DEFAULT '0',
  `responsibilities` text NOT NULL,
  `id_function` int(255) unsigned NOT NULL,
  `on_going` int(11) NOT NULL DEFAULT '0',
  `date_start` timestamp NULL DEFAULT NULL,
  `date_end` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_experience`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `applied_candidates_experience` */

/*Table structure for table `applied_candidates_interview` */

DROP TABLE IF EXISTS `applied_candidates_interview`;

CREATE TABLE `applied_candidates_interview` (
  `id_interview` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_candidate` int(255) unsigned NOT NULL,
  `date_interview` date NOT NULL,
  `start_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `id_job` int(255) unsigned NOT NULL,
  `id_ref` varchar(255) NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_sender` int(255) unsigned NOT NULL,
  `status` int(255) NOT NULL,
  PRIMARY KEY (`id_interview`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `applied_candidates_interview` */

/*Table structure for table `applied_candidates_personal` */

DROP TABLE IF EXISTS `applied_candidates_personal`;

CREATE TABLE `applied_candidates_personal` (
  `id_personal` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_candidate` int(255) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `address_1` text NOT NULL,
  `address_2` text,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `id_gender` int(255) unsigned NOT NULL,
  `id_state` int(255) NOT NULL,
  `id_country` int(255) unsigned NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `id_marital_status` int(255) unsigned DEFAULT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_job` int(255) unsigned NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `id_country_code` int(255) DEFAULT NULL,
  PRIMARY KEY (`id_personal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `applied_candidates_personal` */

/*Table structure for table `applied_candidates_resumes` */

DROP TABLE IF EXISTS `applied_candidates_resumes`;

CREATE TABLE `applied_candidates_resumes` (
  `id_resume` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_candidate` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_job` int(255) unsigned NOT NULL,
  `resume_name` varchar(255) NOT NULL,
  `resume_path` varchar(255) NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_resume`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `applied_candidates_resumes` */

/*Table structure for table `applied_jobs_candidates` */

DROP TABLE IF EXISTS `applied_jobs_candidates`;

CREATE TABLE `applied_jobs_candidates` (
  `id_application` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_candidate` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_job` int(255) unsigned NOT NULL,
  `cover_letter` text NOT NULL,
  `id_rate` int(1) DEFAULT '0',
  `date_available` timestamp NULL DEFAULT NULL,
  `years_of_experience` int(255) unsigned NOT NULL,
  `id_salary` int(255) unsigned NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `NYSC` int(255) DEFAULT NULL,
  `id_reason` int(255) DEFAULT '0',
  `id_rater` int(255) DEFAULT NULL,
  `date_rated` datetime DEFAULT NULL,
  PRIMARY KEY (`id_application`),
  KEY `index2` (`id_application`,`id_candidate`,`id_company`,`id_job`,`id_rate`,`id_reason`),
  KEY `index3` (`id_candidate`),
  KEY `index4` (`id_job`),
  KEY `index5` (`id_job`,`id_candidate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `applied_jobs_candidates` */

/*Table structure for table `applied_jobs_cnd_notes` */

DROP TABLE IF EXISTS `applied_jobs_cnd_notes`;

CREATE TABLE `applied_jobs_cnd_notes` (
  `id_note` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_candidate` int(255) unsigned NOT NULL,
  `id_job` int(255) unsigned NOT NULL,
  `note` text NOT NULL,
  `date_added` datetime DEFAULT NULL,
  `id_creator` int(255) DEFAULT NULL,
  PRIMARY KEY (`id_note`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `applied_jobs_cnd_notes` */

/*Table structure for table `applied_jobs_cnd_watch_list` */

DROP TABLE IF EXISTS `applied_jobs_cnd_watch_list`;

CREATE TABLE `applied_jobs_cnd_watch_list` (
  `id_list` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_candidate` int(255) unsigned NOT NULL,
  `id_job` int(255) unsigned NOT NULL,
  `date_added` datetime NOT NULL,
  `id_creator` int(255) DEFAULT NULL,
  PRIMARY KEY (`id_list`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `applied_jobs_cnd_watch_list` */

/*Table structure for table `applied_jobs_cns_reasons` */

DROP TABLE IF EXISTS `applied_jobs_cns_reasons`;

CREATE TABLE `applied_jobs_cns_reasons` (
  `id_reason` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `reason` text NOT NULL,
  PRIMARY KEY (`id_reason`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `applied_jobs_cns_reasons` */

/*Table structure for table `appliedjobs` */

DROP TABLE IF EXISTS `appliedjobs`;

CREATE TABLE `appliedjobs` (
  `appliedid` int(11) NOT NULL AUTO_INCREMENT,
  `jobid` int(11) NOT NULL,
  `candidateid` int(11) NOT NULL,
  `coverletter` text NOT NULL,
  `rateid` int(11) NOT NULL DEFAULT '0',
  `availabledate` date NOT NULL,
  `totalyears` varchar(4) NOT NULL DEFAULT '0',
  `expectedsalary` varchar(50) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`appliedid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `appliedjobs` */

/*Table structure for table `appraisal_comments` */

DROP TABLE IF EXISTS `appraisal_comments`;

CREATE TABLE `appraisal_comments` (
  `id_comments` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_kpi` varchar(255) NOT NULL,
  `id_review_cycle` int(255) unsigned NOT NULL,
  `id_user` int(255) unsigned NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `comments` text NOT NULL,
  `date` datetime DEFAULT NULL,
  `status` int(255) unsigned NOT NULL DEFAULT '0',
  `id_company` int(255) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `review_for_id_user` int(255) unsigned NOT NULL,
  `step` int(255) NOT NULL,
  `appraisal_type` varchar(255) NOT NULL,
  `id_ref` varchar(255) NOT NULL,
  PRIMARY KEY (`id_comments`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `appraisal_comments` */

/*Table structure for table `appraisal_completed_forms` */

DROP TABLE IF EXISTS `appraisal_completed_forms`;

CREATE TABLE `appraisal_completed_forms` (
  `id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_ref` varchar(255) NOT NULL,
  `id_kpi` varchar(255) NOT NULL,
  `id_template_type` int(255) unsigned NOT NULL,
  `id_appraisal_type` int(255) unsigned NOT NULL,
  `id_user` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_review_cycle` int(255) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `min_label` varchar(255) DEFAULT NULL,
  `min_score` int(255) unsigned NOT NULL DEFAULT '0',
  `max_label` varchar(255) DEFAULT NULL,
  `max_score` int(255) unsigned NOT NULL DEFAULT '0',
  `comments` text,
  `score_value` int(255) NOT NULL DEFAULT '0',
  `date_modified` timestamp NULL DEFAULT NULL,
  `status` int(255) unsigned NOT NULL DEFAULT '0',
  `id_reviewed_by` int(255) unsigned NOT NULL,
  `date_start` timestamp NULL DEFAULT NULL,
  `date_due` timestamp NULL DEFAULT NULL,
  `id_section` int(255) unsigned NOT NULL,
  `weight` int(255) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `appraisal_completed_forms` */

/*Table structure for table `appraisal_default_kpis` */

DROP TABLE IF EXISTS `appraisal_default_kpis`;

CREATE TABLE `appraisal_default_kpis` (
  `id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `ref_id` varchar(255) NOT NULL,
  `kpi` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `appraisal_default_kpis` */

/*Table structure for table `appraisal_downloads` */

DROP TABLE IF EXISTS `appraisal_downloads`;

CREATE TABLE `appraisal_downloads` (
  `id_file` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) NOT NULL,
  `id_user` int(255) unsigned NOT NULL,
  `id_kpi` varchar(255) NOT NULL,
  `rack_file_name` varchar(255) NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_review` int(255) unsigned NOT NULL,
  `id_ref` varchar(255) NOT NULL,
  PRIMARY KEY (`id_file`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `appraisal_downloads` */

/*Table structure for table `appraisal_manager_steps_settings` */

DROP TABLE IF EXISTS `appraisal_manager_steps_settings`;

CREATE TABLE `appraisal_manager_steps_settings` (
  `id_steps` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `steps` int(255) unsigned NOT NULL,
  `id_user` int(255) unsigned NOT NULL DEFAULT '0',
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `temp_ref_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id_steps`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `appraisal_manager_steps_settings` */

/*Table structure for table `appraisal_request_kpi_input` */

DROP TABLE IF EXISTS `appraisal_request_kpi_input`;

CREATE TABLE `appraisal_request_kpi_input` (
  `id_request` int(255) NOT NULL AUTO_INCREMENT,
  `id_review_cycle` int(255) unsigned NOT NULL,
  `message` text NOT NULL,
  `id_sender` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `due_date` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `id_company` int(255) NOT NULL,
  `status` int(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_request`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `appraisal_request_kpi_input` */

/*Table structure for table `appraisal_review_cycle` */

DROP TABLE IF EXISTS `appraisal_review_cycle`;

CREATE TABLE `appraisal_review_cycle` (
  `id_review_cycle` int(255) NOT NULL AUTO_INCREMENT,
  `date_period_start` timestamp NULL DEFAULT NULL,
  `date_period_end` timestamp NULL DEFAULT NULL,
  `date_review_start` timestamp NULL DEFAULT NULL,
  `date_review_end` timestamp NULL DEFAULT NULL,
  `id_company` int(255) NOT NULL,
  `status` int(255) NOT NULL DEFAULT '1',
  `date_created` timestamp NULL DEFAULT NULL,
  `id_template` int(255) unsigned NOT NULL,
  `id_frequency` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_review_cycle`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `appraisal_review_cycle` */

/*Table structure for table `appraisal_sections` */

DROP TABLE IF EXISTS `appraisal_sections`;

CREATE TABLE `appraisal_sections` (
  `id_section` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `sections` varchar(255) NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `status` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_section`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `appraisal_sections` */

/*Table structure for table `appraisal_settings_templates` */

DROP TABLE IF EXISTS `appraisal_settings_templates`;

CREATE TABLE `appraisal_settings_templates` (
  `id_template` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_frequency` int(255) unsigned NOT NULL,
  `cycle_start` date NOT NULL,
  `cycle_end` date NOT NULL,
  `review_start` date NOT NULL,
  `review_end` date NOT NULL,
  `status` int(255) unsigned NOT NULL DEFAULT '1',
  `id_company` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_template`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `appraisal_settings_templates` */

/*Table structure for table `appraisal_summary_ratings` */

DROP TABLE IF EXISTS `appraisal_summary_ratings`;

CREATE TABLE `appraisal_summary_ratings` (
  `id_ratings` int(255) NOT NULL AUTO_INCREMENT,
  `id_kpi` varchar(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `rating_label` varchar(255) NOT NULL,
  `point_start` int(255) NOT NULL,
  `point_end` int(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  PRIMARY KEY (`id_ratings`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `appraisal_summary_ratings` */

/*Table structure for table `appraisal_template_forms` */

DROP TABLE IF EXISTS `appraisal_template_forms`;

CREATE TABLE `appraisal_template_forms` (
  `id_template` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_kpi` varchar(255) NOT NULL,
  `kpi_ref_id` varchar(255) DEFAULT NULL,
  `id_appraisal_type` int(255) unsigned NOT NULL,
  `id_template_type` int(255) unsigned NOT NULL,
  `id_role` int(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `min_label` varchar(255) DEFAULT NULL,
  `min_score` int(255) unsigned DEFAULT '0',
  `max_label` varchar(255) DEFAULT NULL,
  `max_score` int(255) DEFAULT '0',
  `id_company` int(255) unsigned NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `status` int(255) unsigned NOT NULL DEFAULT '1',
  `id_section` int(255) unsigned NOT NULL,
  `weight` int(255) unsigned NOT NULL DEFAULT '0',
  `kpis` int(255) unsigned NOT NULL,
  `id_user` int(255) unsigned NOT NULL,
  `id_review_cycle` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_template`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `appraisal_template_forms` */

/*Table structure for table `appraisal_template_users` */

DROP TABLE IF EXISTS `appraisal_template_users`;

CREATE TABLE `appraisal_template_users` (
  `id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(255) unsigned NOT NULL,
  `id_template` int(255) unsigned NOT NULL,
  `status` int(255) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `due_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `appraisal_template_users` */

/*Table structure for table `appraisal_types` */

DROP TABLE IF EXISTS `appraisal_types`;

CREATE TABLE `appraisal_types` (
  `id_appraisal_type` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `appraisal_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id_appraisal_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `appraisal_types` */

insert  into `appraisal_types`(`id_appraisal_type`,`appraisal_type`) values (1,'Self'),(2,'Manager');

/*Table structure for table `att_tracker` */

DROP TABLE IF EXISTS `att_tracker`;

CREATE TABLE `att_tracker` (
  `id` int(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `att_tracker` */

insert  into `att_tracker`(`id`) values (0);

/*Table structure for table `attendance_daily_shift` */

DROP TABLE IF EXISTS `attendance_daily_shift`;

CREATE TABLE `attendance_daily_shift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pin` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `shift` varchar(100) DEFAULT NULL,
  `cit` varchar(20) DEFAULT NULL,
  `cot` varchar(20) DEFAULT NULL,
  `bit` varchar(20) DEFAULT NULL,
  `bot` varchar(20) DEFAULT NULL,
  `ftci` varchar(100) DEFAULT NULL,
  `ftco` varchar(100) DEFAULT NULL,
  `breaktime` varchar(100) DEFAULT NULL,
  `overtime` varchar(100) DEFAULT NULL,
  `lateness` varchar(100) DEFAULT NULL,
  `id_company` int(11) NOT NULL,
  `day` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `attendance_daily_shift` */

/*Table structure for table `attendance_daily_shifts` */

DROP TABLE IF EXISTS `attendance_daily_shifts`;

CREATE TABLE `attendance_daily_shifts` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `id_company` int(25) DEFAULT NULL,
  `id_location` int(25) DEFAULT NULL,
  `data` longtext,
  `day` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `attendance_daily_shifts` */

/*Table structure for table `attendance_employees` */

DROP TABLE IF EXISTS `attendance_employees`;

CREATE TABLE `attendance_employees` (
  `id_attendance_employee` int(255) NOT NULL AUTO_INCREMENT,
  `id_user` int(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_dept` int(255) NOT NULL,
  `id_role` int(255) NOT NULL,
  `id_location` int(255) NOT NULL,
  `id_creator` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_removed` datetime DEFAULT NULL,
  PRIMARY KEY (`id_attendance_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `attendance_employees` */

/*Table structure for table `attendance_last_refresh` */

DROP TABLE IF EXISTS `attendance_last_refresh`;

CREATE TABLE `attendance_last_refresh` (
  `id_last_refresh` int(11) NOT NULL AUTO_INCREMENT,
  `last_refresh` datetime DEFAULT NULL,
  PRIMARY KEY (`id_last_refresh`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

/*Data for the table `attendance_last_refresh` */

/*Table structure for table `attendance_record` */

DROP TABLE IF EXISTS `attendance_record`;

CREATE TABLE `attendance_record` (
  `id_attendance_record` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `id_location` int(25) unsigned DEFAULT NULL,
  `id_company` int(25) unsigned DEFAULT NULL,
  `id_user` int(25) unsigned DEFAULT NULL,
  `first_name` varchar(150) DEFAULT NULL,
  `shift` int(10) unsigned DEFAULT NULL,
  `hours_clocked` varchar(45) DEFAULT NULL,
  `lateness` int(10) unsigned DEFAULT NULL,
  `absent` int(10) unsigned DEFAULT NULL,
  `location_tag` varchar(150) DEFAULT NULL,
  `id_string` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_attendance_record`) USING BTREE,
  UNIQUE KEY `attendance_record_uniq_ind` (`start_date`,`end_date`,`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `attendance_record` */

/*Table structure for table `attendance_settings` */

DROP TABLE IF EXISTS `attendance_settings`;

CREATE TABLE `attendance_settings` (
  `id_attendance_settings` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `email_report` int(25) unsigned DEFAULT NULL,
  `often` int(25) unsigned DEFAULT NULL,
  `id_company` int(25) unsigned DEFAULT NULL,
  `date_added` date DEFAULT NULL,
  `date_updated` date DEFAULT NULL,
  PRIMARY KEY (`id_attendance_settings`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `attendance_settings` */

/*Table structure for table `attendance_settings_location` */

DROP TABLE IF EXISTS `attendance_settings_location`;

CREATE TABLE `attendance_settings_location` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `id_location` longtext,
  `last_cycle` date DEFAULT NULL,
  `next_cycle_from` date DEFAULT NULL,
  `next_cycle_to` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `id_company` int(25) DEFAULT NULL,
  `recipient` longtext,
  `pdf_link` varchar(200) DEFAULT NULL,
  `email_report` int(11) NOT NULL COMMENT '0 or 1',
  `often` int(11) NOT NULL COMMENT 'weekly,monthly,daily',
  `date_added` date NOT NULL COMMENT 'date',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `attendance_settings_location` */

/*Table structure for table `attendance_settings_recipient` */

DROP TABLE IF EXISTS `attendance_settings_recipient`;

CREATE TABLE `attendance_settings_recipient` (
  `id_attendance_settings` int(25) DEFAULT NULL,
  `id_user` int(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

/*Data for the table `attendance_settings_recipient` */

/*Table structure for table `attendance_shift` */

DROP TABLE IF EXISTS `attendance_shift`;

CREATE TABLE `attendance_shift` (
  `id_attendance_shift` int(11) NOT NULL AUTO_INCREMENT,
  `shift_name` varchar(100) NOT NULL,
  `cit` varchar(20) NOT NULL,
  `cot` varchar(20) NOT NULL,
  `bit` varchar(20) DEFAULT NULL,
  `bot` varchar(20) DEFAULT NULL,
  `ftci` varchar(100) DEFAULT NULL,
  `ftco` varchar(100) DEFAULT NULL,
  `overtime` int(11) DEFAULT NULL,
  `id_company` int(11) DEFAULT NULL,
  `attendance_shift_type` int(11) DEFAULT NULL,
  `lateness` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_attendance_shift`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `attendance_shift` */

/*Table structure for table `attendance_shift_days` */

DROP TABLE IF EXISTS `attendance_shift_days`;

CREATE TABLE `attendance_shift_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sun` int(11) DEFAULT NULL,
  `mon` int(11) DEFAULT NULL,
  `tue` int(11) DEFAULT NULL,
  `wed` int(11) DEFAULT NULL,
  `thu` int(11) DEFAULT NULL,
  `fri` int(11) DEFAULT NULL,
  `sat` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `date_added` date DEFAULT NULL,
  `last_updated` date DEFAULT NULL,
  `id_attendance_shift` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `attendance_shift_days` */

/*Table structure for table `attendance_shift_settings` */

DROP TABLE IF EXISTS `attendance_shift_settings`;

CREATE TABLE `attendance_shift_settings` (
  `id_shift_settings` int(11) NOT NULL AUTO_INCREMENT,
  `shift_planner` int(11) NOT NULL,
  `shift_supervisor` int(11) NOT NULL,
  `id_company` int(11) NOT NULL,
  PRIMARY KEY (`id_shift_settings`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `attendance_shift_settings` */

/*Table structure for table `attendance_sim_info` */

DROP TABLE IF EXISTS `attendance_sim_info`;

CREATE TABLE `attendance_sim_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_sn` varchar(50) NOT NULL,
  `sim_no` varchar(50) DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `sim_sn` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `attendance_sim_info` */

/*Table structure for table `attendance_user_info` */

DROP TABLE IF EXISTS `attendance_user_info`;

CREATE TABLE `attendance_user_info` (
  `user_id` int(11) unsigned NOT NULL,
  `id_user` int(25) unsigned NOT NULL,
  `dept_id` int(11) DEFAULT NULL,
  `name` varchar(40) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `card` varchar(20) DEFAULT NULL,
  `priviledge` int(11) DEFAULT NULL,
  `acc_group` int(11) DEFAULT NULL,
  `time_zones` varchar(20) DEFAULT NULL,
  `gender` varchar(2) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `street` varchar(40) DEFAULT NULL,
  `zip` varchar(6) DEFAULT NULL,
  `ophone` varchar(20) DEFAULT NULL,
  `fphone` varchar(20) DEFAULT NULL,
  `pager` varchar(20) DEFAULT NULL,
  `minzu` varchar(8) DEFAULT NULL,
  `title` varchar(20) DEFAULT NULL,
  `sn` varchar(20) DEFAULT NULL,
  `ssn` varchar(20) DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `city` varchar(2) DEFAULT NULL,
  `security_flags` smallint(6) DEFAULT NULL,
  `del_tag` smallint(6) NOT NULL,
  `register_ot` int(11) DEFAULT NULL,
  `auto_sch_plan` int(11) DEFAULT NULL,
  `min_auto_sch_interval` int(11) DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  `id_company` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE,
  KEY `userinfo_SN` (`sn`) USING BTREE,
  KEY `userinfo_defaultdeptid` (`dept_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `attendance_user_info` */

/*Table structure for table `auth_config` */

DROP TABLE IF EXISTS `auth_config`;

CREATE TABLE `auth_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_enabled` tinyint(1) DEFAULT '1',
  `adapter` varchar(16) NOT NULL,
  `id_company` int(10) unsigned NOT NULL,
  `last_modified` datetime DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id_user_last_modified` int(10) unsigned DEFAULT NULL,
  `config_data` text NOT NULL,
  `is_default` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `adapter` (`adapter`),
  KEY `id_user_last_modified` (`id_user_last_modified`),
  KEY `id_company` (`id_company`),
  CONSTRAINT `auth_config_ibfk_1` FOREIGN KEY (`id_user_last_modified`) REFERENCES `employees` (`id_user`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `auth_config_ibfk_2` FOREIGN KEY (`id_company`) REFERENCES `companies` (`id_company`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `auth_config` */

/*Table structure for table `banks` */

DROP TABLE IF EXISTS `banks`;

CREATE TABLE `banks` (
  `id_bank` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` int(3) unsigned zerofill NOT NULL,
  PRIMARY KEY (`id_bank`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `banks` */

insert  into `banks`(`id_bank`,`name`,`code`) values (1,'Access Bank Plc',044),(2,'Citibank Nigeria Limited',023),(3,'Diamond Bank Plc',063),(4,'Ecobank Nigeria Plc',050),(5,'Enterprise Bank',000),(6,'Fidelity Bank Plc',070),(7,'First Bank of Nigeria Plc',011),(8,'First City Monument Bank Plc',214),(9,'Guaranty Trust Bank Plc',058),(10,'Key Stone Bank',000),(11,'MainStreet Bank',000),(12,'Skye Bank Plc',076),(13,'Stanbic IBTC Bank Ltd.',039),(14,'Standard Chartered Bank Nigeria Ltd.',068),(15,'Sterling Bank Plc',232),(16,'Union Bank of Nigeria Plc',032),(17,'United Bank For Africa Plc',033),(18,'Unity  Bank Plc',215),(19,'Wema Bank Plc',035),(20,'Zenith Bank Plc',057),(21,'Heritage Banking Company Ltd.',000);

/*Table structure for table `bday_messages` */

DROP TABLE IF EXISTS `bday_messages`;

CREATE TABLE `bday_messages` (
  `id_bday_messages` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_bday_messages`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `bday_messages` */

/*Table structure for table `candidateskills` */

DROP TABLE IF EXISTS `candidateskills`;

CREATE TABLE `candidateskills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidateid` int(11) NOT NULL,
  `jobid` int(11) NOT NULL,
  `employerid` int(11) NOT NULL,
  `skillid` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `candidateskills` */

/*Table structure for table `cgpa_level` */

DROP TABLE IF EXISTS `cgpa_level`;

CREATE TABLE `cgpa_level` (
  `id_cgpa` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id_cgpa`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `cgpa_level` */

insert  into `cgpa_level`(`id_cgpa`,`name`) values (1,'First Class'),(2,'Distinction'),(3,'Second Class Upper'),(4,'Second Class Lower'),(5,'Third Class'),(6,'Upper Credit'),(7,'Lower Credit'),(8,'UNLISTED');

/*Table structure for table `cgpalevel` */

DROP TABLE IF EXISTS `cgpalevel`;

CREATE TABLE `cgpalevel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `cgpalevel` */

insert  into `cgpalevel`(`id`,`name`,`created`,`modified`) values (1,'First Class','2013-12-18 13:02:40','0000-00-00 00:00:00'),(2,'Distinction','2013-12-18 13:02:40','0000-00-00 00:00:00'),(3,'Second Class Upper','2013-12-18 13:02:40','0000-00-00 00:00:00'),(4,'Second Class Lower','2013-12-18 13:02:40','0000-00-00 00:00:00'),(5,'Third Class','2013-12-18 13:02:40','0000-00-00 00:00:00'),(6,'Upper Credit','2013-12-18 13:02:40','0000-00-00 00:00:00'),(7,'Lower Credit','2013-12-18 13:02:40','0000-00-00 00:00:00'),(8,'UNLISTED','2013-12-18 13:02:40','0000-00-00 00:00:00');

/*Table structure for table `check_in_out` */

DROP TABLE IF EXISTS `check_in_out`;

CREATE TABLE `check_in_out` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `SN` varchar(20) DEFAULT NULL,
  `checkdate` date NOT NULL,
  `checkin` varchar(20) DEFAULT NULL,
  `checkout` varchar(20) DEFAULT NULL,
  `breakout` time DEFAULT NULL,
  `breakin` time DEFAULT NULL,
  `otin` time DEFAULT NULL,
  `otout` time DEFAULT NULL,
  `verifycode` int(11) DEFAULT NULL,
  `sensorid` varchar(5) DEFAULT NULL,
  `WorkCode` varchar(20) DEFAULT NULL,
  `Reserved` varchar(20) DEFAULT NULL,
  `stat` varchar(10) DEFAULT 'no',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tbl_checkinout_unq_ind1` (`user_id`,`SN`,`checkdate`),
  KEY `FK81D47223C9F2E8D` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `check_in_out` */

/*Table structure for table `checkinout` */

DROP TABLE IF EXISTS `checkinout`;

CREATE TABLE `checkinout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `checktime` datetime NOT NULL,
  `checktype` varchar(1) NOT NULL,
  `verifycode` int(11) NOT NULL,
  `SN` varchar(20) DEFAULT NULL,
  `sensorid` varchar(5) DEFAULT NULL,
  `WorkCode` varchar(20) DEFAULT NULL,
  `Reserved` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userid` (`userid`,`checktime`),
  KEY `checkinout_userid` (`userid`),
  KEY `checkinout_SN` (`SN`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `checkinout` */

/*Table structure for table `companies` */

DROP TABLE IF EXISTS `companies`;

CREATE TABLE `companies` (
  `id_company` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_own_company` int(255) unsigned NOT NULL DEFAULT '0',
  `company_name` varchar(255) DEFAULT NULL,
  `about_us` text NOT NULL,
  `banner_text` text NOT NULL,
  `description` text,
  `id_state` int(255) DEFAULT '0',
  `id_industry` int(255) DEFAULT '0',
  `website` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `range_of_employees` int(255) DEFAULT '0',
  `date_registered` timestamp NULL DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `id_activated_by` int(255) DEFAULT '0',
  `id_string` varchar(255) NOT NULL,
  `logo_path` varchar(255) DEFAULT NULL,
  `logo_text` varchar(255) DEFAULT NULL,
  `banner_image_path` varchar(255) DEFAULT NULL,
  `id_account_type` int(255) DEFAULT '1',
  `id_account_manager` int(255) DEFAULT '1',
  `refid` varchar(255) NOT NULL DEFAULT '41020001',
  `cdn_container` varchar(255) NOT NULL,
  `bg_color` varchar(255) DEFAULT 'rgb(23, 176, 47)',
  PRIMARY KEY (`id_company`,`id_string`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `companies` */

/*Table structure for table `company_locations` */

DROP TABLE IF EXISTS `company_locations`;

CREATE TABLE `company_locations` (
  `id_location` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(255) unsigned NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `id_state` int(255) DEFAULT '0',
  `id_country` int(255) DEFAULT '0',
  `location_tag` varchar(255) DEFAULT NULL,
  `id_head` int(255) unsigned NOT NULL DEFAULT '0',
  `id_parent` int(255) unsigned NOT NULL DEFAULT '0',
  `date_created` timestamp NULL DEFAULT NULL,
  `id_creator` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_location`),
  KEY `id_company` (`id_company`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `company_locations` */

/*Table structure for table `company_modules` */

DROP TABLE IF EXISTS `company_modules`;

CREATE TABLE `company_modules` (
  `id_module` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_module`,`id_company`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `company_modules` */

/*Table structure for table `company_phone_numbers` */

DROP TABLE IF EXISTS `company_phone_numbers`;

CREATE TABLE `company_phone_numbers` (
  `id_company` int(255) unsigned NOT NULL,
  `id_address` int(255) unsigned NOT NULL,
  `phone_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `company_phone_numbers` */

/*Table structure for table `compensation_access` */

DROP TABLE IF EXISTS `compensation_access`;

CREATE TABLE `compensation_access` (
  `id_paygrade_access` int(255) NOT NULL AUTO_INCREMENT,
  `id_user` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  PRIMARY KEY (`id_paygrade_access`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_access` */

/*Table structure for table `compensation_access_paygrade` */

DROP TABLE IF EXISTS `compensation_access_paygrade`;

CREATE TABLE `compensation_access_paygrade` (
  `id_paygrade_access` int(255) NOT NULL AUTO_INCREMENT,
  `id_user` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  PRIMARY KEY (`id_paygrade_access`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_access_paygrade` */

/*Table structure for table `compensation_allowance` */

DROP TABLE IF EXISTS `compensation_allowance`;

CREATE TABLE `compensation_allowance` (
  `id_allowance` int(255) NOT NULL AUTO_INCREMENT,
  `id_company` int(255) NOT NULL,
  `id_pay_structure` int(255) NOT NULL,
  `allowance` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `type` int(1) NOT NULL,
  `id_allowance_default` int(255) NOT NULL,
  PRIMARY KEY (`id_allowance`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_allowance` */

/*Table structure for table `compensation_allowance_default` */

DROP TABLE IF EXISTS `compensation_allowance_default`;

CREATE TABLE `compensation_allowance_default` (
  `id_allowance_default` int(255) NOT NULL AUTO_INCREMENT,
  `id_company` int(255) NOT NULL,
  `allowance` varchar(255) NOT NULL,
  `type` int(1) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_allowance_default`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_allowance_default` */

/*Table structure for table `compensation_basic_pay` */

DROP TABLE IF EXISTS `compensation_basic_pay`;

CREATE TABLE `compensation_basic_pay` (
  `id_basic_pay` int(255) NOT NULL AUTO_INCREMENT,
  `id_company` int(255) NOT NULL,
  `id_pay_grade` int(255) NOT NULL,
  `basic_pay` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  PRIMARY KEY (`id_basic_pay`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_basic_pay` */

/*Table structure for table `compensation_calculation` */

DROP TABLE IF EXISTS `compensation_calculation`;

CREATE TABLE `compensation_calculation` (
  `id_calculation` int(255) NOT NULL AUTO_INCREMENT,
  `id_contribution` int(255) NOT NULL,
  `id_pay_grade` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `pay` varchar(255) NOT NULL,
  `value` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id_calculation`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_calculation` */

/*Table structure for table `compensation_calculation_default` */

DROP TABLE IF EXISTS `compensation_calculation_default`;

CREATE TABLE `compensation_calculation_default` (
  `id_calculation` int(255) NOT NULL AUTO_INCREMENT,
  `id_contribution_default` int(255) NOT NULL,
  `default_pay` varchar(255) NOT NULL,
  `value` int(1) NOT NULL,
  PRIMARY KEY (`id_calculation`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `compensation_calculation_default` */

insert  into `compensation_calculation_default`(`id_calculation`,`id_contribution_default`,`default_pay`,`value`) values (1,1,'Basic',1),(2,1,'Transport',1),(3,1,'Utility',0);

/*Table structure for table `compensation_contribution` */

DROP TABLE IF EXISTS `compensation_contribution`;

CREATE TABLE `compensation_contribution` (
  `id_contribution` int(255) NOT NULL AUTO_INCREMENT,
  `id_contribution_default` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_pay_structure` int(255) NOT NULL,
  `contribution` varchar(255) NOT NULL,
  `percentage` varchar(255) NOT NULL,
  PRIMARY KEY (`id_contribution`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_contribution` */

/*Table structure for table `compensation_contribution_default` */

DROP TABLE IF EXISTS `compensation_contribution_default`;

CREATE TABLE `compensation_contribution_default` (
  `id_contribution_default` int(255) NOT NULL AUTO_INCREMENT,
  `contribution` varchar(255) NOT NULL,
  `percentage` float NOT NULL,
  `status` int(1) NOT NULL,
  `id_company` int(255) NOT NULL,
  PRIMARY KEY (`id_contribution_default`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `compensation_contribution_default` */

insert  into `compensation_contribution_default`(`id_contribution_default`,`contribution`,`percentage`,`status`,`id_company`) values (1,'Pension',7.5,1,0),(2,'NHF',2.5,1,0);

/*Table structure for table `compensation_currency` */

DROP TABLE IF EXISTS `compensation_currency`;

CREATE TABLE `compensation_currency` (
  `id_currency` int(255) NOT NULL AUTO_INCREMENT,
  `id_company` int(255) NOT NULL,
  `id_pay_grade` int(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  PRIMARY KEY (`id_currency`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_currency` */

/*Table structure for table `compensation_deduction` */

DROP TABLE IF EXISTS `compensation_deduction`;

CREATE TABLE `compensation_deduction` (
  `id_deduction` int(255) NOT NULL AUTO_INCREMENT,
  `id_deduction_default` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_pay_structure` int(255) NOT NULL,
  `deduction` varchar(255) NOT NULL,
  `percentage` varchar(255) NOT NULL,
  `type` int(1) NOT NULL,
  PRIMARY KEY (`id_deduction`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_deduction` */

/*Table structure for table `compensation_deduction_default` */

DROP TABLE IF EXISTS `compensation_deduction_default`;

CREATE TABLE `compensation_deduction_default` (
  `id_deduction_default` int(255) NOT NULL AUTO_INCREMENT,
  `id_company` int(255) NOT NULL,
  `deduction` varchar(255) NOT NULL,
  `percentage` float NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id_deduction_default`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_deduction_default` */

/*Table structure for table `compensation_employee_allowance` */

DROP TABLE IF EXISTS `compensation_employee_allowance`;

CREATE TABLE `compensation_employee_allowance` (
  `id_employee_allowance` int(255) NOT NULL AUTO_INCREMENT,
  `id_compensation_employee` int(255) NOT NULL,
  `id_allowance` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_pay_structure` int(255) NOT NULL,
  `allowance` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `type` int(1) NOT NULL,
  `id_allowance_default` int(255) NOT NULL,
  PRIMARY KEY (`id_employee_allowance`),
  KEY `ind_compensation_emp_allw` (`id_compensation_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_employee_allowance` */

/*Table structure for table `compensation_employee_allowance_archive` */

DROP TABLE IF EXISTS `compensation_employee_allowance_archive`;

CREATE TABLE `compensation_employee_allowance_archive` (
  `id_employee_allowance` int(255) NOT NULL AUTO_INCREMENT,
  `id_compensation_employee` int(255) NOT NULL,
  `id_allowance` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_pay_structure` int(255) NOT NULL,
  `allowance` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `type` int(1) NOT NULL,
  `id_allowance_default` int(255) NOT NULL,
  PRIMARY KEY (`id_employee_allowance`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_employee_allowance_archive` */

/*Table structure for table `compensation_employee_contribution` */

DROP TABLE IF EXISTS `compensation_employee_contribution`;

CREATE TABLE `compensation_employee_contribution` (
  `id_employee_contribution` int(255) NOT NULL AUTO_INCREMENT,
  `id_compensation_employee` int(255) NOT NULL,
  `id_contribution_default` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_pay_structure` int(255) NOT NULL,
  `id_contribution` int(255) NOT NULL,
  `contribution` varchar(255) NOT NULL,
  `percentage` varchar(255) NOT NULL,
  PRIMARY KEY (`id_employee_contribution`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_employee_contribution` */

/*Table structure for table `compensation_employee_contribution_archive` */

DROP TABLE IF EXISTS `compensation_employee_contribution_archive`;

CREATE TABLE `compensation_employee_contribution_archive` (
  `id_employee_contribution` int(255) NOT NULL AUTO_INCREMENT,
  `id_compensation_employee` int(255) NOT NULL,
  `id_contribution_default` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_pay_structure` int(255) NOT NULL,
  `id_contribution` int(255) NOT NULL,
  `contribution` varchar(255) NOT NULL,
  `percentage` varchar(255) NOT NULL,
  PRIMARY KEY (`id_employee_contribution`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_employee_contribution_archive` */

/*Table structure for table `compensation_employee_deduction` */

DROP TABLE IF EXISTS `compensation_employee_deduction`;

CREATE TABLE `compensation_employee_deduction` (
  `id_employee_deduction` int(255) NOT NULL AUTO_INCREMENT,
  `id_compensation_employee` int(255) NOT NULL,
  `id_deduction_default` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_pay_structure` int(255) NOT NULL,
  `id_deduction` int(255) NOT NULL,
  `deduction` varchar(255) NOT NULL,
  `percentage` varchar(255) NOT NULL,
  `type` int(1) NOT NULL,
  PRIMARY KEY (`id_employee_deduction`),
  KEY `ind_compensation_emp_deduc` (`id_compensation_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_employee_deduction` */

/*Table structure for table `compensation_employee_deduction_archive` */

DROP TABLE IF EXISTS `compensation_employee_deduction_archive`;

CREATE TABLE `compensation_employee_deduction_archive` (
  `id_employee_deduction` int(255) NOT NULL AUTO_INCREMENT,
  `id_compensation_employee` int(255) NOT NULL,
  `id_contribution_default` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_pay_structure` int(255) NOT NULL,
  `id_deduction` int(255) NOT NULL,
  `deduction` varchar(255) NOT NULL,
  `percentage` varchar(255) NOT NULL,
  `type` int(1) NOT NULL,
  `id_deduction_default` int(255) NOT NULL,
  PRIMARY KEY (`id_employee_deduction`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_employee_deduction_archive` */

/*Table structure for table `compensation_employee_json` */

DROP TABLE IF EXISTS `compensation_employee_json`;

CREATE TABLE `compensation_employee_json` (
  `id_compensation_employee_json` int(255) NOT NULL AUTO_INCREMENT,
  `id_user` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `compensation` text NOT NULL,
  PRIMARY KEY (`id_compensation_employee_json`),
  UNIQUE KEY `comp_emp_uniq` (`id_user`,`id_company`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_employee_json` */

/*Table structure for table `compensation_employee_leave_allowance` */

DROP TABLE IF EXISTS `compensation_employee_leave_allowance`;

CREATE TABLE `compensation_employee_leave_allowance` (
  `id_employee_leave_allowance` int(255) NOT NULL AUTO_INCREMENT,
  `id_compensation_employee` int(255) NOT NULL,
  `id_pay_structure` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `amount` int(255) NOT NULL,
  `year` int(4) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id_employee_leave_allowance`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_employee_leave_allowance` */

/*Table structure for table `compensation_employee_leave_allowance_archive` */

DROP TABLE IF EXISTS `compensation_employee_leave_allowance_archive`;

CREATE TABLE `compensation_employee_leave_allowance_archive` (
  `id_employee_leave_allowance` int(255) NOT NULL AUTO_INCREMENT,
  `id_compensation_employee` int(255) NOT NULL,
  `id_pay_structure` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `amount` int(255) NOT NULL,
  `id_allowance_default` int(255) NOT NULL,
  `year` int(4) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id_employee_leave_allowance`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_employee_leave_allowance_archive` */

/*Table structure for table `compensation_employees` */

DROP TABLE IF EXISTS `compensation_employees`;

CREATE TABLE `compensation_employees` (
  `id_compensation_employee` int(255) NOT NULL AUTO_INCREMENT,
  `id_pay_structure` int(255) NOT NULL,
  `id_pay_grade` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_role` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `id_currency` int(255) NOT NULL,
  `tax` int(1) NOT NULL,
  `archive` int(1) NOT NULL,
  `overtime_hourly_rate` int(20) NOT NULL DEFAULT '0',
  `pay_scale` varchar(255) DEFAULT NULL,
  `hours_per_week` int(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_compensation_employee`),
  UNIQUE KEY `ind_compensation_emp_unq` (`id_company`,`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_employees` */

/*Table structure for table `compensation_employees_archive` */

DROP TABLE IF EXISTS `compensation_employees_archive`;

CREATE TABLE `compensation_employees_archive` (
  `id_compensation_employee` int(255) NOT NULL AUTO_INCREMENT,
  `id_pay_structure` int(255) NOT NULL,
  `id_pay_grade` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_role` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `id_currency` int(255) NOT NULL,
  `tax` int(1) NOT NULL,
  `archive` int(1) NOT NULL DEFAULT '0',
  `overtime_hourly_rate` int(15) NOT NULL DEFAULT '0',
  `pay_scale` varchar(255) NOT NULL,
  `hours_per_week` int(15) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_compensation_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_employees_archive` */

/*Table structure for table `compensation_leave_allowance` */

DROP TABLE IF EXISTS `compensation_leave_allowance`;

CREATE TABLE `compensation_leave_allowance` (
  `id_leave_allowance` int(255) NOT NULL AUTO_INCREMENT,
  `id_pay_structure` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `amount` int(255) NOT NULL,
  PRIMARY KEY (`id_leave_allowance`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_leave_allowance` */

/*Table structure for table `compensation_pay_grade` */

DROP TABLE IF EXISTS `compensation_pay_grade`;

CREATE TABLE `compensation_pay_grade` (
  `id_pay_grade` int(255) NOT NULL AUTO_INCREMENT,
  `id_company` int(255) NOT NULL,
  `pay_grade` varchar(255) NOT NULL,
  `id_role` int(255) NOT NULL,
  `number` int(20) NOT NULL,
  PRIMARY KEY (`id_pay_grade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_pay_grade` */

/*Table structure for table `compensation_pay_structure` */

DROP TABLE IF EXISTS `compensation_pay_structure`;

CREATE TABLE `compensation_pay_structure` (
  `id_pay_structure` int(255) NOT NULL AUTO_INCREMENT,
  `id_pay_grade` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_currency` int(255) NOT NULL,
  `id_role` int(255) NOT NULL,
  `tax` int(1) NOT NULL DEFAULT '0',
  `archive` int(1) NOT NULL DEFAULT '0',
  `overtime_hourly_rate` int(20) NOT NULL DEFAULT '0',
  `pay_scale` varchar(255) NOT NULL,
  `hours_per_week` int(20) NOT NULL,
  `ref_code` varchar(45) NOT NULL,
  PRIMARY KEY (`id_pay_structure`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `compensation_pay_structure` */

/*Table structure for table `consultant_request` */

DROP TABLE IF EXISTS `consultant_request`;

CREATE TABLE `consultant_request` (
  `id_consultant_request` int(255) NOT NULL AUTO_INCREMENT,
  `consultant_ref_id` int(255) NOT NULL,
  `id_client` int(255) NOT NULL,
  `modules` text NOT NULL,
  `note` text NOT NULL,
  `status` int(1) NOT NULL,
  `request_date` timestamp NULL DEFAULT NULL,
  `expire_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_consultant_request`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `consultant_request` */

/*Table structure for table `countries` */

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id_country` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `country` varchar(100) NOT NULL,
  `code` int(11) NOT NULL,
  PRIMARY KEY (`id_country`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `countries` */

insert  into `countries`(`id_country`,`country`,`code`) values (1,'Afghanistan',0),(2,'Albania',0),(3,'Algeria',0),(4,'Angola',0),(5,'Antigua and Barbuda',0),(6,'Argentina',0),(7,'Armenia',0),(8,'Austria',0),(9,'Azerbaijan',0),(10,'Bahrain',0),(11,'Bangladesh',0),(12,'Belarus',0),(13,'Belgium',0),(14,'Belize',0),(15,'Benin',0),(16,'Bhutan',0),(17,'Bolivia',0),(18,'Bosnia and Herzegovina',0),(19,'Botswana',0),(20,'Brazil',0),(21,'Bulgaria',0),(22,'Burkina Faso',0),(23,'Burundi',0),(24,'Cambodia',0),(25,'Cameroon',0),(26,'Canada',0),(27,'Cape Verde',0),(28,'Central African Republic',0),(29,'Chad',0),(30,'Chile',0),(31,'China',0),(32,'Colombia',0),(33,'Comoros',0),(34,'Democratic Republic of Congo',0),(35,'Republic of Congo',0),(36,'Costa Rica',0),(37,'Cote d Ivoire',0),(38,'Croatia',0),(39,'Czech Republic',0),(40,'Denmark',0),(41,'Djibouti',0),(42,'Dominica',0),(43,'Dominican Republic',0),(44,'Ecuador',0),(45,'Egypt',0),(46,'El Salvador',0),(47,'Equatorial Guinea',0),(48,'Eritrea',0),(49,'Estonia',0),(50,'Ethiopia',0),(51,'Fiji',0),(52,'Finland',0),(53,'France',0),(54,'Gabon',0),(55,'The Gambia',0),(56,'Georgia',0),(57,'Germany',0),(58,'Ghana',0),(59,'Greece',0),(60,'Grenada',0),(61,'Guatemala',0),(62,'Guinea',0),(63,'Guinea-Bissau',0),(64,'Guyana',0),(65,'Haiti',0),(66,'Honduras',0),(67,'Hungary',0),(68,'Iceland',0),(69,'India',0),(70,'Indonesia',0),(71,'Iran',0),(72,'Iraq',0),(73,'Israel',0),(74,'Italy',0),(75,'Jamaica',0),(76,'Japan',0),(77,'Jordan',0),(78,'Kazakhstan',0),(79,'Kenya',0),(80,'Kiribati',0),(81,'Korea',0),(82,'Kosovo',0),(83,'Kuwait',0),(84,'Kyrgyz Republic',0),(85,'Lao PDR',0),(86,'Latvia',0),(87,'Lebanon',0),(88,'Lesotho',0),(89,'Liberia',0),(90,'Libya',0),(91,'Lithuania',0),(92,'Luxembourg',0),(93,'FYR of Macedonia',0),(94,'Madagascar',0),(95,'Malawi',0),(96,'Malaysia',0),(97,'Maldives',0),(98,'Mali',0),(99,'Marshall Islands',0),(100,'Mauritania',0),(101,'Mauritius',0),(102,'Mexico',0),(103,'FS Micronesia',0),(104,'Moldova',0),(105,'Mongolia',0),(106,'Montenegro',0),(107,'Morocco',0),(108,'Mozambique',0),(109,'Myanmar',0),(110,'Namibia',0),(111,'Nepal',0),(112,'Netherlands',0),(113,'Nicaragua',0),(114,'Niger',0),(115,'Nigeria',0),(116,'Norway',0),(117,'Oman',0),(118,'Pakistan',0),(119,'Palau',0),(120,'Panama',0),(121,'Papua New Guinea',0),(122,'Paraguay',0),(123,'Peru',0),(124,'Philippines',0),(125,'Poland',0),(126,'Portugal',0),(127,'Qatar',0),(128,'Romania',0),(129,'Russia',0),(130,'Rwanda',0),(131,'Samoa',0),(132,'Sao Tome and Principe',0),(133,'Senegal',0),(134,'Serbia',0),(135,'Seychelles',0),(136,'Sierra Leone',0),(137,'Singapore',0),(138,'Slovak Republic',0),(139,'Slovenia',0),(140,'Solomon Islands',0),(141,'Somalia',0),(142,'South Africa',0),(143,'South Sudan',0),(144,'Spain',0),(145,'Sri Lanka',0),(146,'St. Kitts and Nevis',0),(147,'St. Lucia',0),(148,'St. Vincent and Grenadines',0),(149,'Sudan',0),(150,'Suriname',0),(151,'Swaziland',0),(152,'Sweden',0),(153,'Switzerland',0),(154,'Syria',0),(155,'Tajikistan',0),(156,'Tanzania',0),(157,'Thailand',0),(158,'Timor Leste (East Timor)',0),(159,'Togo',0),(160,'Tonga',0),(161,'Trinidad and Tobago',0),(162,'Tunisia',0),(163,'Turkmenistan',0),(164,'Tuvalu',0),(165,'Uganda',0),(166,'Ukraine',0),(167,'United Arab Emirates',0),(168,'United Kingdom',0),(169,'United States of America',0),(170,'Uruguay',0),(171,'Uzbekistan',0),(172,'Vanuatu',0),(173,'Venezuela',0),(174,'Vietnam',0),(175,'West Bank and Gaza',0),(176,'Yemen',0),(177,'Zambia',0),(178,'Zimbabwe',0);

/*Table structure for table `courses` */

DROP TABLE IF EXISTS `courses`;

CREATE TABLE `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseid` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `subcategoryid` varchar(10) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`courseid`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

/*Data for the table `courses` */

insert  into `courses`(`id`,`courseid`,`name`,`subcategoryid`,`created`,`modified`) values (1,'0','Languages','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(2,'1','Arabic Studies','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(3,'2','Chinese','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(4,'3','Efik-Ibibio','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(5,'4','English Language','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(6,'5','European and Nigerian Languages','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(7,'6','French','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(8,'7','French with German/Russian','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(9,'8','Fulfulde','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(10,'9','German','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(11,'10','German Combined with French Russian','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(12,'11','Hausa','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(13,'12','Igbo','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(14,'13','Igbo Linguistics','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(15,'14','Kanuri','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(16,'15','Kiswahili','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(17,'16','Languages Arts','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(18,'17','Languages Arts & Yoruba','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(19,'18','Language and Linguistics','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(20,'19','Linguistics','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(21,'20','Linguistics, Igbo and Other African Languages','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(22,'21','Linguistics/Yoruba','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(23,'22','Linguistics/Edo','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(24,'23','Linguistics/Urhobo','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(25,'24','Modern European Languages','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(26,'25','Modern Languages','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(27,'26','Modern Language and Translation','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(28,'27','Nigerian Languages','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(29,'28','Russian with French/German','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(30,'29','Portuguese','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(31,'30','Portuguese/English','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(32,'31','Russian','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(33,'32','Yoruba','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(34,'33','Yoruba and Communication','0','2013-05-31 09:18:30','2013-05-31 06:18:30'),(35,'100','Performing Arts','1','2013-05-31 09:18:30','2013-05-31 06:18:30'),(36,'101','Creative Arts','1','2013-05-31 09:18:30','2013-05-31 06:18:30'),(37,'102','Drama/Dramatic Arts/Performing Arts/Theatre Arts','1','2013-05-31 09:18:30','2013-05-31 06:18:30'),(38,'103','Music','1','2013-05-31 09:18:30','2013-05-31 06:18:30'),(39,'104','Theatre Arts','1','2013-05-31 09:18:30','2013-05-31 06:18:30'),(40,'105','Theatre and Film Studies','1','2013-05-31 09:18:30','2013-05-31 06:18:30'),(41,'106','Arts - Combined','1','2013-05-31 09:18:30','2013-05-31 06:18:30'),(42,'107','Film and Video Studies','1','2013-05-31 09:18:30','2013-05-31 06:18:30'),(43,'200','Fine Art/Fine and Applied Arts','2','2013-05-31 09:18:30','2013-05-31 06:18:30'),(44,'201','Fine Arts and Design','2','2013-05-31 09:18:30','2013-05-31 06:18:30'),(45,'202','Fine and Industrial Arts','2','2013-05-31 09:18:30','2013-05-31 06:18:30'),(46,'300','Philosophy','3','2013-05-31 09:18:30','2013-05-31 06:18:30'),(47,'301','Philosophy and Religious Studies','3','2013-05-31 09:18:30','2013-05-31 06:18:30'),(48,'400','History','4','2013-05-31 09:18:30','2013-05-31 06:18:30'),(49,'401','Classical Studies','4','2013-05-31 09:18:30','2013-05-31 06:18:30'),(50,'402','History and Archeology','4','2013-05-31 09:18:30','2013-05-31 06:18:30'),(51,'403','History/International Studies','4','2013-05-31 09:18:30','2013-05-31 06:18:30'),(52,'404','History and International Relations','4','2013-05-31 09:18:30','2013-05-31 06:18:30'),(53,'405','History and Diplomacy Studies','4','2013-05-31 09:18:30','2013-05-31 06:18:30'),(54,'406','History and Strategic Studies','4','2013-05-31 09:18:30','2013-05-31 06:18:30'),(55,'407','History and Political Science','4','2013-05-31 09:18:30','2013-05-31 06:18:30'),(56,'500','Literature in English','5','2013-05-31 09:18:30','2013-05-31 06:18:30'),(57,'501','African and Asian Studies','5','2013-05-31 09:18:30','2013-05-31 06:18:30'),(58,'502','English Studies','5','2013-05-31 09:18:30','2013-05-31 06:18:30'),(59,'503','English Language and Literature','5','2013-05-31 09:18:30','2013-05-31 06:18:30'),(60,'504','English and Literary Studies','5','2013-05-31 09:18:30','2013-05-31 06:18:30'),(61,'505','English and International Studies','5','2013-05-31 09:18:30','2013-05-31 06:18:30'),(62,'506','Language and Literature','5','2013-05-31 09:18:30','2013-05-31 06:18:30'),(63,'507','Literature','5','2013-05-31 09:18:30','2013-05-31 06:18:30'),(64,'600','Religious Studies','6','2013-05-31 09:18:30','2013-05-31 06:18:30'),(65,'601','Comparative Religious Studies','6','2013-05-31 09:18:30','2013-05-31 06:18:30'),(66,'602','Christian Studies','6','2013-05-31 09:18:30','2013-05-31 06:18:30'),(67,'603','Christian Religious Knowledge Studies','6','2013-05-31 09:18:30','2013-05-31 06:18:30'),(68,'604','Arabic and Islamic Studies','6','2013-05-31 09:18:30','2013-05-31 06:18:30'),(69,'605','Arabic Language and Literature','6','2013-05-31 09:18:30','2013-05-31 06:18:30'),(70,'606','Islamic Studies','6','2013-05-31 09:18:30','2013-05-31 06:18:30'),(71,'607','Islamic Studies with Actuarial Science','6','2013-05-31 09:18:30','2013-05-31 06:18:30'),(72,'608','Religious and Cultural Studies','6','2013-05-31 09:18:30','2013-05-31 06:18:30'),(73,'609','Religious and Human Relations','6','2013-05-31 09:18:30','2013-05-31 06:18:30'),(74,'610','Religion and Philosophy','6','2013-05-31 09:18:30','2013-05-31 06:18:30'),(75,'611','Religious Studies/Theology','6','2013-05-31 09:18:30','2013-05-31 06:18:30'),(76,'612','Religion and Science','6','2013-05-31 09:18:30','2013-05-31 06:18:30'),(77,'613','Theology','6','2013-05-31 09:18:30','2013-05-31 06:18:30'),(78,'614','Religion','6','2013-05-31 09:18:30','2013-05-31 06:18:30'),(79,'1000','Agriculture','10','2013-05-31 09:18:30','2013-05-31 06:18:30'),(80,'1001','Agricultural Administration','10','2013-05-31 09:18:30','2013-05-31 06:18:30'),(81,'1002','Agricultural and Fisheries Management','10','2013-05-31 09:18:30','2013-05-31 06:18:30'),(82,'1003','Agricultural Business and Financial Management','10','2013-05-31 09:18:30','2013-05-31 06:18:30'),(83,'1004','Agricultural Cooperative','10','2013-05-31 09:18:30','2013-05-31 06:18:30'),(84,'1005','Agricultural Economics and Extension','10','2013-05-31 09:18:30','2013-05-31 06:18:30'),(85,'1006','Agricultural Economics and Farm Management','10','2013-05-31 09:18:30','2013-05-31 06:18:30'),(86,'1007','Agricultural Economics and Management Studies','10','2013-05-31 09:18:30','2013-05-31 06:18:30'),(87,'1008','Agricultural Economics','10','2013-05-31 09:18:30','2013-05-31 06:18:30'),(88,'1009','Agricultural Extension and Rural Development','10','2013-05-31 09:18:30','2013-05-31 06:18:30'),(89,'1010','Agricultural Extension and Rural Sociology','10','2013-05-31 09:18:30','2013-05-31 06:18:30'),(90,'1011','Agricultural Extension Services','10','2013-05-31 09:18:30','2013-05-31 06:18:30'),(91,'1012','Agriculture and Development Extension','10','2013-05-31 09:18:30','2013-05-31 06:18:30'),(92,'1013','Agric Meterology and Water Management','10','2013-05-31 09:18:30','2013-05-31 06:18:30'),(93,'1014','Farm Management and Agric Extension','10','2013-05-31 09:18:30','2013-05-31 06:18:30'),(94,'1015','Pasture and Range Management','10','2013-05-31 09:18:30','2013-05-31 06:18:30'),(95,'1016','Agronomy','10','2013-05-31 09:18:30','2013-05-31 06:18:30'),(96,'1200','Animal Science','12','2013-05-31 09:18:30','2013-05-31 06:18:30'),(97,'1201','Animal Science Nutrition','12','2013-05-31 09:18:30','2013-05-31 06:18:30'),(98,'1202','Animal Science and Range Management','12','2013-05-31 09:18:30','2013-05-31 06:18:30'),(99,'1203','Animal Science and Production','12','2013-05-31 09:18:30','2013-05-31 06:18:30'),(100,'1204','Animal Production','12','2013-05-31 09:18:30','2013-05-31 06:18:30'),(101,'1205','Animal Production with Nutrition','12','2013-05-31 09:18:30','2013-05-31 06:18:30'),(102,'1206','Animal Production and Health Services','12','2013-05-31 09:18:30','2013-05-31 06:18:30'),(103,'1207','Animal Physiology','12','2013-05-31 09:18:30','2013-05-31 06:18:30'),(104,'1208','Animal Breeding and Genetics','12','2013-05-31 09:18:30','2013-05-31 06:18:30'),(105,'1209','Livestock Production Technology','12','2013-05-31 09:18:30','2013-05-31 06:18:30'),(106,'1300','Crop Production','13','2013-05-31 09:18:30','2013-05-31 06:18:30'),(107,'1301','Crop Protection','13','2013-05-31 09:18:30','2013-05-31 06:18:30'),(108,'1302','Crop Science','13','2013-05-31 09:18:30','2013-05-31 06:18:30'),(109,'1303','Crop, Soil, and Evironmental Science','13','2013-05-31 09:18:30','2013-05-31 06:18:30'),(110,'1304','Eco-Tourism and Wildlife Management','13','2013-05-31 09:18:30','2013-05-31 06:18:30'),(111,'1305','Environmental Management and Toxicology','13','2013-05-31 09:18:30','2013-05-31 06:18:30'),(112,'1306','Horticulture','13','2013-05-31 09:18:30','2013-05-31 06:18:30'),(113,'1400','Fisheries','14','2013-05-31 09:18:30','2013-05-31 06:18:30'),(114,'1401','Fisheries and Wildlife Management','14','2013-05-31 09:18:30','2013-05-31 06:18:30'),(115,'1402','Fisheries and Aquaculture','14','2013-05-31 09:18:30','2013-05-31 06:18:30'),(116,'1500','Food Science','15','2013-05-31 09:18:30','2013-05-31 06:18:30'),(117,'1501','Food Science and Technology','15','2013-05-31 09:18:30','2013-05-31 06:18:30'),(118,'1502','Food Science and Tourism','15','2013-05-31 09:18:30','2013-05-31 06:18:30'),(119,'1503','Food Science with Business','15','2013-05-31 09:18:30','2013-05-31 06:18:30'),(120,'1504','Food Services and Tourism','15','2013-05-31 09:18:30','2013-05-31 06:18:30'),(121,'1600','Forestry and Environmental Management','16','2013-05-31 09:18:30','2013-05-31 06:18:30'),(122,'1601','Forestry and Fisheries','16','2013-05-31 09:18:30','2013-05-31 06:18:30'),(123,'1602','Forestry and Wildlife','16','2013-05-31 09:18:30','2013-05-31 06:18:30'),(124,'1603','Forestry and Wood Technology','16','2013-05-31 09:18:30','2013-05-31 06:18:30'),(125,'1604','Forestry Wildlife and Fisheries','16','2013-05-31 09:18:30','2013-05-31 06:18:30'),(126,'1605','Forestry/Forest Resources Management','16','2013-05-31 09:18:30','2013-05-31 06:18:30'),(127,'1700','Home Economics','17','2013-05-31 09:18:30','2013-05-31 06:18:30'),(128,'1701','Home Science and Management','17','2013-05-31 09:18:30','2013-05-31 06:18:30'),(129,'1702','Home Science Nutrition and Dietetics','17','2013-05-31 09:18:30','2013-05-31 06:18:30'),(130,'1703','Family Nutrition and Consumer Sciences','17','2013-05-31 09:18:30','2013-05-31 06:18:30'),(131,'1704','Nutrition and Dietetics','17','2013-05-31 09:18:30','2013-05-31 06:18:30'),(132,'1705','Home Economics and Food Management','17','2013-05-31 09:18:30','2013-05-31 06:18:30'),(133,'1800','Soil Science','18','2013-05-31 09:18:30','2013-05-31 06:18:30'),(134,'1801','Soil Science and Land Management','18','2013-05-31 09:18:30','2013-05-31 06:18:30'),(135,'1802','Soil Science and Environmental Management','18','2013-05-31 09:18:30','2013-05-31 06:18:30'),(136,'2000','Accounting','20','2013-05-31 09:18:30','2013-05-31 06:18:30'),(137,'2001','Accounting Management and Finance','20','2013-05-31 09:18:30','2013-05-31 06:18:30'),(138,'2002','Accountancy/Finance/Accounting','20','2013-05-31 09:18:30','2013-05-31 06:18:30'),(139,'2003','Taxation','20','2013-05-31 09:18:30','2013-05-31 06:18:30'),(140,'2100','Marketing','21','2013-05-31 09:18:30','2013-05-31 06:18:30'),(141,'2101','Public Relations and Advertising','21','2013-05-31 09:18:30','2013-05-31 06:18:30'),(142,'2200','Banking/Finance','22','2013-05-31 09:18:30','2013-05-31 06:18:30'),(143,'2201','Banking, Finance and Insurance','22','2013-05-31 09:18:30','2013-05-31 06:18:30'),(144,'2202','Finance','22','2013-05-31 09:18:30','2013-05-31 06:18:30'),(145,'2300','Insurance','23','2013-05-31 09:18:30','2013-05-31 06:18:30'),(146,'2301','Insurance and Risk Management','23','2013-05-31 09:18:30','2013-05-31 06:18:30'),(147,'2302','Insurance and Actuarial Science','23','2013-05-31 09:18:30','2013-05-31 06:18:30'),(148,'2303','Actuarial Science','23','2013-05-31 09:18:30','2013-05-31 06:18:30'),(149,'2400','Public Administration','24','2013-05-31 09:18:30','2013-05-31 06:18:30'),(150,'2401','Public Administration and Local Government','24','2013-05-31 09:18:30','2013-05-31 06:18:30'),(151,'2402','Secreterial Administration','24','2013-05-31 09:18:30','2013-05-31 06:18:30'),(152,'2403','Local Government Administration','24','2013-05-31 09:18:30','2013-05-31 06:18:30'),(153,'2404','Local Government and Development Studies','24','2013-05-31 09:18:30','2013-05-31 06:18:30'),(154,'2405','Government and Public Administration','24','2013-05-31 09:18:30','2013-05-31 06:18:30'),(155,'2500','Business Administration','25','2013-05-31 09:18:30','2013-05-31 06:18:30'),(156,'2501','Business Economics','25','2013-05-31 09:18:30','2013-05-31 06:18:30'),(157,'2502','Business Management','25','2013-05-31 09:18:30','2013-05-31 06:18:30'),(158,'2503','Entreprenuership','25','2013-05-31 09:18:30','2013-05-31 06:18:30'),(159,'2504','Management Studies','25','2013-05-31 09:18:30','2013-05-31 06:18:30'),(160,'2505','Management and Enterpreneurship','25','2013-05-31 09:18:30','2013-05-31 06:18:30'),(161,'2506','Operation Research','25','2013-05-31 09:18:30','2013-05-31 06:18:30'),(162,'2600','International Relations','26','2013-05-31 09:18:30','2013-05-31 06:18:30'),(163,'2601','International Studies and Diplomacy','26','2013-05-31 09:18:30','2013-05-31 06:18:30'),(164,'2602','International Relations and Strategic Studies','26','2013-05-31 09:18:30','2013-05-31 06:18:30'),(165,'2700','Management Information System','27','2013-05-31 09:18:30','2013-05-31 06:18:30'),(166,'2701','Office and Information Management','27','2013-05-31 09:18:30','2013-05-31 06:18:30'),(167,'2702','Office Management Technology','27','2013-05-31 09:18:30','2013-05-31 06:18:30'),(168,'2703','Office System Management','27','2013-05-31 09:18:30','2013-05-31 06:18:30'),(169,'2705','Information Resource Management','27','2013-05-31 09:18:30','2013-05-31 06:18:30'),(170,'2706','Management Technology','27','2013-05-31 09:18:30','2013-05-31 06:18:30'),(171,'2800','Human Resources Management','28','2013-05-31 09:18:30','2013-05-31 06:18:30'),(172,'2801','Industrial Relations and Personnel Management','28','2013-05-31 09:18:30','2013-05-31 06:18:30'),(173,'2802','Industrial Relations','28','2013-05-31 09:18:30','2013-05-31 06:18:30'),(174,'2803','Industrial Relations and Personnel Management','28','2013-05-31 09:18:30','2013-05-31 06:18:30'),(175,'2804','Industrial and Labor Relations','28','2013-05-31 09:18:30','2013-05-31 06:18:30'),(176,'2805','Personnel Management','28','2013-05-31 09:18:30','2013-05-31 06:18:30'),(177,'2900','Hospitality and Tourism Management','29','2013-05-31 09:18:30','2013-05-31 06:18:30'),(178,'2901','Hotel Tourism and Management','29','2013-05-31 09:18:30','2013-05-31 06:18:30'),(179,'2902','Tourism and Event Management','29','2013-05-31 09:18:30','2013-05-31 06:18:30'),(180,'2903','Tourism Studies','29','2013-05-31 09:18:30','2013-05-31 06:18:30'),(181,'2507','Sport Management','25','2013-05-31 09:18:30','2013-05-31 06:18:30'),(182,'3000','Arts Education','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(183,'3001','Education and Music','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(184,'3002','Education and Yoruba','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(185,'3003','Education/Arabic','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(186,'3004','Education/Arts','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(187,'3005','Education/Christian Religious Studies','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(188,'3006','Education/Edo Language','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(189,'3007','Education/English Language','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(190,'3008','Education/Fine and Applied Arts','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(191,'3009','Education/French','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(192,'3010','Education/Hausa','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(193,'3011','Education/History','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(194,'3012','Education/Igbo','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(195,'3013','Education/Igbo/Linguistics','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(196,'3014','Education/Islamic Studies','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(197,'3015','Education/Language and French','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(198,'3016','Education/Language Arts','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(199,'3017','Education/Language/English','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(200,'3018','Education/Linguistics','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(201,'3019','Education/Religious Studies','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(202,'3020','Adult Education/English Literature','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(203,'3021','Intercultural Education','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(204,'3022','Interculture Education','30','2013-05-31 09:18:30','2013-05-31 06:18:30'),(205,'3100','Agricultural Education','31','2013-05-31 09:18:30','2013-05-31 06:18:30'),(206,'3101','Home Economics and Hotel Management','31','2013-05-31 09:18:30','2013-05-31 06:18:30'),(207,'3102','Home Economics Education','31','2013-05-31 09:18:30','2013-05-31 06:18:30'),(208,'3200','Education Aministration','32','2013-05-31 09:18:30','2013-05-31 06:18:30'),(209,'3201','Business Education','32','2013-05-31 09:18:30','2013-05-31 06:18:30'),(210,'3202','Education and Business Administration','32','2013-05-31 09:18:30','2013-05-31 06:18:30'),(211,'3203','Curriculum and Instruction','32','2013-05-31 09:18:30','2013-05-31 06:18:30'),(212,'3204','Curriculum and Teaching','32','2013-05-31 09:18:30','2013-05-31 06:18:30'),(213,'3205','Curriculum Studies','32','2013-05-31 09:18:30','2013-05-31 06:18:30'),(214,'3206','Education Management','32','2013-05-31 09:18:30','2013-05-31 06:18:30'),(215,'3207','Education/Accountancy','32','2013-05-31 09:18:30','2013-05-31 06:18:30'),(216,'3208','Educational Administration and Supervision','32','2013-05-31 09:18:30','2013-05-31 06:18:30'),(217,'3209','Educational Management and Planning','32','2013-05-31 09:18:30','2013-05-31 06:18:30'),(218,'3210','Education Admin','32','2013-05-31 09:18:30','2013-05-31 06:18:30'),(219,'3300','Adult and Community Education','33','2013-05-31 09:18:30','2013-05-31 06:18:30'),(220,'3301','Adult and Non-Formal Education','33','2013-05-31 09:18:30','2013-05-31 06:18:30'),(221,'3302','Adult Education/Economics and Statistics','33','2013-05-31 09:18:30','2013-05-31 06:18:30'),(222,'3303','Adult Education/Geography and Regional Planning','33','2013-05-31 09:18:30','2013-05-31 06:18:30'),(223,'3304','Adult Education/Political Science and Public Administration','33','2013-05-31 09:18:30','2013-05-31 06:18:30'),(224,'3400','Engr/Technology Education','34','2013-05-31 09:18:30','2013-05-31 06:18:30'),(225,'3401','Automobile Technology Education','34','2013-05-31 09:18:30','2013-05-31 06:18:30'),(226,'3402','Computer and Information Technology','34','2013-05-31 09:18:30','2013-05-31 06:18:30'),(227,'3403','Electrical/Electronics Technology','34','2013-05-31 09:18:30','2013-05-31 06:18:30'),(228,'3404','Industrial Technical Education','34','2013-05-31 09:18:30','2013-05-31 06:18:30'),(229,'3405','Metal Technology Education','34','2013-05-31 09:18:30','2013-05-31 06:18:30'),(230,'3406','Technical Education','34','2013-05-31 09:18:30','2013-05-31 06:18:30'),(231,'3407','Technology and Vocation Education','34','2013-05-31 09:18:30','2013-05-31 06:18:30'),(232,'3408','Technology Education','34','2013-05-31 09:18:30','2013-05-31 06:18:30'),(233,'3409','Vocational Education and Technical Education','34','2013-05-31 09:18:30','2013-05-31 06:18:30'),(234,'3410','Vocational Education','34','2013-05-31 09:18:30','2013-05-31 06:18:30'),(235,'3411','Wood Work/Education','34','2013-05-31 09:18:30','2013-05-31 06:18:30'),(236,'3500','Environmental Science Education','35','2013-05-31 09:18:30','2013-05-31 06:18:30'),(237,'3501','Building Education','35','2013-05-31 09:18:30','2013-05-31 06:18:30'),(238,'3502','Environmental Education','35','2013-05-31 09:18:30','2013-05-31 06:18:30'),(239,'3503','Geography and Environmental Management','35','2013-05-31 09:18:30','2013-05-31 06:18:30'),(240,'3600','Health Education','36','2013-05-31 09:18:30','2013-05-31 06:18:30'),(241,'3700','Education/Science','37','2013-05-31 09:18:30','2013-05-31 06:18:30'),(242,'3701','Education/Integrated Science Education','37','2013-05-31 09:18:30','2013-05-31 06:18:30'),(243,'3702','Education/Mathematics','37','2013-05-31 09:18:30','2013-05-31 06:18:30'),(244,'3703','Biology Education','37','2013-05-31 09:18:30','2013-05-31 06:18:30'),(245,'3704','Computer Education','37','2013-05-31 09:18:30','2013-05-31 06:18:30'),(246,'3705','Education and Computer Science','37','2013-05-31 09:18:30','2013-05-31 06:18:30'),(247,'3706','Education and Integrated Science','37','2013-05-31 09:18:30','2013-05-31 06:18:30'),(248,'3707','Education/Chemistry','37','2013-05-31 09:18:30','2013-05-31 06:18:30'),(249,'3708','Education/Technology/Introductory Education','37','2013-05-31 09:18:30','2013-05-31 06:18:30'),(250,'3709','Human Kinetics and Health Education','37','2013-05-31 09:18:30','2013-05-31 06:18:30'),(251,'3710','Human Kinetics Education','37','2013-05-31 09:18:30','2013-05-31 06:18:30'),(252,'3711','Library Science','37','2013-05-31 09:18:30','2013-05-31 06:18:30'),(253,'3712','Library Studies','37','2013-05-31 09:18:30','2013-05-31 06:18:30'),(254,'3713','Mathematics and Education Technology','37','2013-05-31 09:18:30','2013-05-31 06:18:30'),(255,'3714','Mathematics/Statistics Education','37','2013-05-31 09:18:30','2013-05-31 06:18:30'),(256,'3715','Physical and Health Education','37','2013-05-31 09:18:30','2013-05-31 06:18:30'),(257,'3716','Physical Education','37','2013-05-31 09:18:30','2013-05-31 06:18:30'),(258,'3717','Physical Education/Physical Education and Recreational Studies','37','2013-05-31 09:18:30','2013-05-31 06:18:30'),(259,'3718','Science and Education','37','2013-05-31 09:18:30','2013-05-31 06:18:30'),(260,'3719','Statistics/Computer Science Education','37','2013-05-31 09:18:30','2013-05-31 06:18:30'),(261,'3800','Education/Social Science','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(262,'3801','Education Foundation and Management','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(263,'3802','Education Psychology, Guidance and Counseling','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(264,'3803','Education/Geography','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(265,'3804','Education/Political Science','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(266,'3805','Education/Sociology','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(267,'3806','Educational Foundation','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(268,'3807','Elementary Education','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(269,'3808','Guidance and Counseling','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(270,'3809','Psychological Education','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(271,'3810','Secreterial Administration and Education','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(272,'3811','Secretarial Education','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(273,'3812','Social Work and Community Development','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(274,'3813','Sociology Education','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(275,'3814','Statistics Education','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(276,'3815','Arts and Social Sciences Education','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(277,'3816','Nursery and Primary Education','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(278,'3817','Counseling and Psychology','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(279,'3818','Pre-Primary Education','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(280,'3819','Primary Education','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(281,'3820','Special Education - Visual Handicaps','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(282,'3821','Special Education','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(283,'3822','Teacher Education','38','2013-05-31 09:18:30','2013-05-31 06:18:30'),(284,'4000','Computer Engineering','40','2013-05-31 09:18:30','2013-05-31 06:18:30'),(285,'4001','Computer Science','40','2013-05-31 09:18:30','2013-05-31 06:18:30'),(286,'4002','Computer Science with Economics/Mathematics','40','2013-05-31 09:18:30','2013-05-31 06:18:30'),(287,'4003','Computer and Information Science','40','2013-05-31 09:18:30','2013-05-31 06:18:30'),(288,'4004','Computer with Statistics','40','2013-05-31 09:18:30','2013-05-31 06:18:30'),(289,'4005','Computer Science and Engineering','40','2013-05-31 09:18:30','2013-05-31 06:18:30'),(290,'4006','Cyber Security Science','40','2013-05-31 09:18:30','2013-05-31 06:18:30'),(291,'4007','Electrical Engineering','40','2013-05-31 09:18:30','2013-05-31 06:18:30'),(292,'4008','Electronics Engineering','40','2013-05-31 09:18:30','2013-05-31 06:18:30'),(293,'4009','Electrical/Electronics/Computer Engineering','40','2013-05-31 09:18:30','2013-05-31 06:18:30'),(294,'4010','Electrical Electronics Engineering','40','2013-05-31 09:18:30','2013-05-31 06:18:30'),(295,'4011','Software Engineering','40','2013-05-31 09:18:30','2013-05-31 06:18:30'),(296,'4012','Systems Engineering','40','2013-05-31 09:18:30','2013-05-31 06:18:30'),(297,'4013','ICT Engineering','40','2013-05-31 09:18:30','2013-05-31 06:18:30'),(298,'4014','Telecommunication Engineering','40','2013-05-31 09:18:30','2013-05-31 06:18:30'),(299,'4015','Electronics and Computer Technology','40','2013-05-31 09:18:30','2013-05-31 06:18:30'),(300,'4016','Communication Technology','40','2013-05-31 09:18:30','2013-05-31 06:18:30'),(301,'4017','Communication Technology and Wireless Technology','40','2013-05-31 09:18:30','2013-05-31 06:18:30'),(302,'4018','Artificial Intelligence','40','2013-05-31 09:18:30','2013-05-31 06:18:30'),(303,'4019','Telecommunication and Information','40','2013-05-31 09:18:30','2013-05-31 06:18:30'),(304,'4100','Chemical Engineering','41','2013-05-31 09:18:30','2013-05-31 06:18:30'),(305,'4101','Chemical and Polymer Engineering','41','2013-05-31 09:18:30','2013-05-31 06:18:30'),(306,'4102','Chemical Petroleum/Petrochemical Engineering','41','2013-05-31 09:18:30','2013-05-31 06:18:30'),(307,'4103','Gas Engineering','41','2013-05-31 09:18:30','2013-05-31 06:18:30'),(308,'4104','Petroleum Engineering','41','2013-05-31 09:18:30','2013-05-31 06:18:30'),(309,'4105','Petroleum and Gas Engineering','41','2013-05-31 09:18:30','2013-05-31 06:18:30'),(310,'4106','Polymer and Textile Engineering','41','2013-05-31 09:18:30','2013-05-31 06:18:30'),(311,'4107','Polymer Engineering','41','2013-05-31 09:18:30','2013-05-31 06:18:30'),(312,'4108','Production Engineering','41','2013-05-31 09:18:30','2013-05-31 06:18:30'),(313,'4109','Energy and Petroleum Studies','41','2013-05-31 09:18:30','2013-05-31 06:18:30'),(314,'4110','Petroleum and Petrochemical Sciences','41','2013-05-31 09:18:30','2013-05-31 06:18:30'),(315,'4111','Textile Science and Technology','41','2013-05-31 09:18:30','2013-05-31 06:18:30'),(316,'4112','Wood Products Engineering','41','2013-05-31 09:18:30','2013-05-31 06:18:30'),(317,'4113','Industrial Production and Engineering','41','2013-05-31 09:18:30','2013-05-31 06:18:30'),(318,'4200','Civil Engineering','42','2013-05-31 09:18:30','2013-05-31 06:18:30'),(319,'4201','Civil Engineering/Hydrology','42','2013-05-31 09:18:30','2013-05-31 06:18:30'),(320,'4300','Environmental Engineering','43','2013-05-31 09:18:30','2013-05-31 06:18:30'),(321,'4301','Environmental Technology','43','2013-05-31 09:18:30','2013-05-31 06:18:30'),(322,'4302','Environmental Management Technology','43','2013-05-31 09:18:30','2013-05-31 06:18:30'),(323,'4304','Transportation and Management Technology','43','2013-05-31 09:18:30','2013-05-31 06:18:30'),(324,'4305','Water Resources and Environmental Engineering','43','2013-05-31 09:18:30','2013-05-31 06:18:30'),(325,'4306','Remote Sensing and Geosciences Information System','43','2013-05-31 09:18:30','2013-05-31 06:18:30'),(326,'4307','Meterology Engineering','43','2013-05-31 09:18:30','2013-05-31 06:18:30'),(327,'4308','Irrigation Engineering','43','2013-05-31 09:18:30','2013-05-31 06:18:30'),(328,'4400','Marine Engineering','44','2013-05-31 09:18:30','2013-05-31 06:18:30'),(329,'4401','Marine Management Technology','44','2013-05-31 09:18:30','2013-05-31 06:18:30'),(330,'4402','Maritime Management Technology','44','2013-05-31 09:18:30','2013-05-31 06:18:30'),(331,'4403','Navalachitecture','44','2013-05-31 09:18:30','2013-05-31 06:18:30'),(332,'4404','Shipping and Maritime Technology','44','2013-05-31 09:18:30','2013-05-31 06:18:30'),(333,'4500','Mechanical Engineering','45','2013-05-31 09:18:30','2013-05-31 06:18:30'),(334,'4501','Mechatronics Engineering','45','2013-05-31 09:18:30','2013-05-31 06:18:30'),(335,'4502','Mechanical and Production Engineering','45','2013-05-31 09:18:30','2013-05-31 06:18:30'),(336,'4600','Minning Engineering','46','2013-05-31 09:18:30','2013-05-31 06:18:30'),(337,'4601','Metallurgical Engineering','46','2013-05-31 09:18:30','2013-05-31 06:18:30'),(338,'4602','Metallurgical and Material Engineering','46','2013-05-31 09:18:30','2013-05-31 06:18:30'),(339,'4603','Methalogical and Material Engineering','46','2013-05-31 09:18:30','2013-05-31 06:18:30'),(340,'4604','Materials and Production Engineering','46','2013-05-31 09:18:30','2013-05-31 06:18:30'),(341,'4606','Glass Technology','46','2013-05-31 09:18:30','2013-05-31 06:18:30'),(342,'4700','Management Technology','47','2013-05-31 09:18:30','2013-05-31 06:18:30'),(343,'4701','Information Management Technology','47','2013-05-31 09:18:30','2013-05-31 06:18:30'),(344,'4702','Information and Media Technology','47','2013-05-31 09:18:30','2013-05-31 06:18:30'),(345,'4703','Information System','47','2013-05-31 09:18:30','2013-05-31 06:18:30'),(346,'4704','Information Technology','47','2013-05-31 09:18:30','2013-05-31 06:18:30'),(347,'4705','Accounting Technology','47','2013-05-31 09:18:30','2013-05-31 06:18:30'),(348,'4706','Technological Management','47','2013-05-31 09:18:30','2013-05-31 06:18:30'),(349,'4707','Project Management Technology','47','2013-05-31 09:18:30','2013-05-31 06:18:30'),(350,'4708','Information and Communication Technology','47','2013-05-31 09:18:30','2013-05-31 06:18:30'),(351,'4709','Information Science and Technology','47','2013-05-31 09:18:30','2013-05-31 06:18:30'),(352,'4800','Visual Arts Technology','48','2013-05-31 09:18:30','2013-05-31 06:18:30'),(353,'4801','Industrial Arts','48','2013-05-31 09:18:30','2013-05-31 06:18:30'),(354,'4802','Industrial Design','48','2013-05-31 09:18:30','2013-05-31 06:18:30'),(355,'4900','Food Engineering','49','2013-05-31 09:18:30','2013-05-31 06:18:30'),(356,'4903','Food Science and Technology/Engineering','49','2013-05-31 09:18:30','2013-05-31 06:18:30'),(357,'4904','Food Science and Engineering','49','2013-05-31 09:18:30','2013-05-31 06:18:30'),(358,'4905','Food Technology','49','2013-05-31 09:18:30','2013-05-31 06:18:30'),(359,'4906','Agricultural Engineering','49','2013-05-31 09:18:30','2013-05-31 06:18:30'),(360,'4907','Agricultural and Environmental Engineering','49','2013-05-31 09:18:30','2013-05-31 06:18:30'),(361,'4908','Agricultural and Bioresources Engineering','49','2013-05-31 09:18:30','2013-05-31 06:18:30'),(362,'4910','Public Health Technology','49','2013-05-31 09:18:30','2013-05-31 06:18:30'),(363,'5000','Architecture','50','2013-05-31 09:18:30','2013-05-31 06:18:30'),(364,'5001','Building','50','2013-05-31 09:18:30','2013-05-31 06:18:30'),(365,'5002','Building Technology','50','2013-05-31 09:18:30','2013-05-31 06:18:30'),(366,'5100','Surveying','51','2013-05-31 09:18:30','2013-05-31 06:18:30'),(367,'5101','Quantity Surveying','51','2013-05-31 09:18:30','2013-05-31 06:18:30'),(368,'5102','Surveying and Geo-Informatics','51','2013-05-31 09:18:30','2013-05-31 06:18:30'),(369,'5103','Land Surveying and Geo-Informatics','51','2013-05-31 09:18:30','2013-05-31 06:18:30'),(370,'5104','Land Surveying','51','2013-05-31 09:18:30','2013-05-31 06:18:30'),(371,'5200','Estate Management','52','2013-05-31 09:18:30','2013-05-31 06:18:30'),(372,'5300','Urban and Regional Planning','53','2013-05-31 09:18:30','2013-05-31 06:18:30'),(373,'5301','Transport Planning and Management','53','2013-05-31 09:18:30','2013-05-31 06:18:30'),(374,'5302','Transport and Planning','53','2013-05-31 09:18:30','2013-05-31 06:18:30'),(375,'5303','Transport and Tourism','53','2013-05-31 09:18:30','2013-05-31 06:18:30'),(376,'5304','Cooperative and Rural Development','53','2013-05-31 09:18:30','2013-05-31 06:18:30'),(377,'5400','Environmental Management','54','2013-05-31 09:18:30','2013-05-31 06:18:30'),(378,'5401','Environmental Resources Management','54','2013-05-31 09:18:30','2013-05-31 06:18:30'),(379,'5402','Marine Environmental Science','54','2013-05-31 09:18:30','2013-05-31 06:18:30'),(380,'5403','Meterology','54','2013-05-31 09:18:30','2013-05-31 06:18:30'),(381,'5404','Natural and Environmental Science','54','2013-05-31 09:18:30','2013-05-31 06:18:30'),(382,'5405','Water Resources and Agro Meterology','54','2013-05-31 09:18:30','2013-05-31 06:18:30'),(383,'6000','Anatomy','60','2013-05-31 09:18:30','2013-05-31 06:18:30'),(384,'6001','Biochemistry','60','2013-05-31 09:18:30','2013-05-31 06:18:30'),(385,'6002','Applied Biochemistry','60','2013-05-31 09:18:30','2013-05-31 06:18:30'),(386,'6003','Physiology','60','2013-05-31 09:18:30','2013-05-31 06:18:30'),(387,'6004','Pharmacology','60','2013-05-31 09:18:30','2013-05-31 06:18:30'),(388,'6005','Biomedical Technology','60','2013-05-31 09:18:30','2013-05-31 06:18:30'),(389,'6006','Biotechnology','60','2013-05-31 09:18:30','2013-05-31 06:18:30'),(390,'6100','Medicine and Surgery - Human Medicine','61','2013-05-31 09:18:30','2013-05-31 06:18:30'),(391,'6101','Medical Laboratory and Technology','61','2013-05-31 09:18:30','2013-05-31 06:18:30'),(392,'6102','Medical Rehabilitation','61','2013-05-31 09:18:30','2013-05-31 06:18:30'),(393,'6103','Medical Biochemistry','61','2013-05-31 09:18:30','2013-05-31 06:18:30'),(394,'6104','Nursing/Nursing Science','61','2013-05-31 09:18:30','2013-05-31 06:18:30'),(395,'6105','Optometry','61','2013-05-31 09:18:30','2013-05-31 06:18:30'),(396,'6106','Physiotherapy','61','2013-05-31 09:18:30','2013-05-31 06:18:30'),(397,'6107','Prosthesis and Orthopaedic Technology','61','2013-05-31 09:18:30','2013-05-31 06:18:30'),(398,'6108','Radiography','61','2013-05-31 09:18:30','2013-05-31 06:18:30'),(399,'6109','Medical Radiography and Radiological Science','61','2013-05-31 09:18:30','2013-05-31 06:18:30'),(400,'6110','Medical Laboratory Science','61','2013-05-31 09:18:30','2013-05-31 06:18:30'),(401,'6111','Veterinary Medicine','61','2013-05-31 09:18:30','2013-05-31 06:18:30'),(402,'6112','Optometry/Industrial Biochemistry','61','2013-05-31 09:18:30','2013-05-31 06:18:30'),(403,'6113','Public and Community Health','61','2013-05-31 09:18:30','2013-05-31 06:18:30'),(404,'6114','Public Health Technology','61','2013-05-31 09:18:30','2013-05-31 06:18:30'),(405,'6200','Dentistry and Dental Technology','62','2013-05-31 09:18:30','2013-05-31 06:18:30'),(406,'6300','Pharmacy','63','2013-05-31 09:18:30','2013-05-31 06:18:30'),(407,'7000','Law','70','2013-05-31 09:18:30','2013-05-31 06:18:30'),(408,'7001','Common and Islamic Law','70','2013-05-31 09:18:30','2013-05-31 06:18:30'),(409,'7002','Common Law','70','2013-05-31 09:18:30','2013-05-31 06:18:30'),(410,'7003','Civil Law','70','2013-05-31 09:18:30','2013-05-31 06:18:30'),(411,'7004','International Law and Jurisprudence','70','2013-05-31 09:18:30','2013-05-31 06:18:30'),(412,'7006','Islamic Law and Sharia Law','70','2013-05-31 09:18:30','2013-05-31 06:18:30'),(413,'7007','Law/International Law and Diplomacy','70','2013-05-31 09:18:30','2013-05-31 06:18:30'),(414,'7008','Private and Islamic Law','70','2013-05-31 09:18:30','2013-05-31 06:18:30'),(415,'7009','Public and International Law','70','2013-05-31 09:18:30','2013-05-31 06:18:30'),(416,'7010','Public and Islamic Law','70','2013-05-31 09:18:30','2013-05-31 06:18:30'),(417,'8000','Biology','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(418,'8001','Applied Biology and Biotechnology','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(419,'8002','Applied Microbiology and Brewing','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(420,'8003','Environmental Biology and Fisheries','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(421,'8004','Ecology','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(422,'8005','Industrial Microbiology','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(423,'8006','Microbiology','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(424,'8007','Microbiology and Industrial Biotechnology','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(425,'8008','Botany and Microbiology','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(426,'8009','Plant Biology','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(427,'8010','Plant Science','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(428,'8011','Plant Science and Biotechnology','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(429,'8012','Plant Science and Forestry','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(430,'8013','Plant Science and Microbiology','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(431,'8014','Pure and Applied Biology','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(432,'8015','Plant Science and Crop Production','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(433,'8016','Plant Physiology and Crop Production','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(434,'8017','Plant Physiology and Crop Ecology','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(435,'8018','Plant Breeding and Seed Technology','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(436,'8019','Genetics and Biotechnology','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(437,'8020','Cell Biology and Genetics','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(438,'8021','Anthropology - Biological','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(439,'8022','Animal Biology and Environment','80','2013-05-31 09:18:30','2013-05-31 06:18:30'),(440,'8100','Chemistry','81','2013-05-31 09:18:30','2013-05-31 06:18:30'),(441,'8101','Chemistry and Industrial Chemistry','81','2013-05-31 09:18:30','2013-05-31 06:18:30'),(442,'8102','Applied Chemistry','81','2013-05-31 09:18:30','2013-05-31 06:18:30'),(443,'8103','Industrial Chemistry','81','2013-05-31 09:18:30','2013-05-31 06:18:30'),(444,'8104','Industrial and Environmental Chemistry','81','2013-05-31 09:18:30','2013-05-31 06:18:30'),(445,'8105','Petroleum Chemistry','81','2013-05-31 09:18:30','2013-05-31 06:18:30'),(446,'8106','Pure and Industrial Chemistry','81','2013-05-31 09:18:30','2013-05-31 06:18:30'),(447,'8200','Geology','82','2013-05-31 09:18:30','2013-05-31 06:18:30'),(448,'8201','Applied Geophysics','82','2013-05-31 09:18:30','2013-05-31 06:18:30'),(449,'8202','Applied Geology','82','2013-05-31 09:18:30','2013-05-31 06:18:30'),(450,'8204','Geology and Mineral Science','82','2013-05-31 09:18:30','2013-05-31 06:18:30'),(451,'8205','Geology and Geophysics','82','2013-05-31 09:18:30','2013-05-31 06:18:30'),(452,'8206','Geophysics','82','2013-05-31 09:18:30','2013-05-31 06:18:30'),(453,'8300','Physics','83','2013-05-31 09:18:30','2013-05-31 06:18:30'),(454,'8301','Industrial Physics','83','2013-05-31 09:18:30','2013-05-31 06:18:30'),(455,'8302','Industrial Physics with Electronics and IT Application','83','2013-05-31 09:18:30','2013-05-31 06:18:30'),(456,'8303','Industrial Physics with Renewable Energy','83','2013-05-31 09:18:30','2013-05-31 06:18:30'),(457,'8304','Physics/Industrial Physics','83','2013-05-31 09:18:30','2013-05-31 06:18:30'),(458,'8305','Physics/Astrology','83','2013-05-31 09:18:30','2013-05-31 06:18:30'),(459,'8306','Physical Sciences','83','2013-05-31 09:18:30','2013-05-31 06:18:30'),(460,'8307','Physics with Computational Modeling','83','2013-05-31 09:18:30','2013-05-31 06:18:30'),(461,'8308','Physics and Applied Physics','83','2013-05-31 09:18:30','2013-05-31 06:18:30'),(462,'8309','Physics with Solar Energy','83','2013-05-31 09:18:30','2013-05-31 06:18:30'),(463,'8310','Pure and Applied Physics','83','2013-05-31 09:18:30','2013-05-31 06:18:30'),(464,'8400','Mathematics/Statistics','84','2013-05-31 09:18:30','2013-05-31 06:18:30'),(465,'8401','Applied Mathematics with Statistics','84','2013-05-31 09:18:30','2013-05-31 06:18:30'),(466,'8402','Applied Statistics','84','2013-05-31 09:18:30','2013-05-31 06:18:30'),(467,'8403','Industrial Mathematics/Computer','84','2013-05-31 09:18:30','2013-05-31 06:18:30'),(468,'8404','Industrial Mathematics','84','2013-05-31 09:18:30','2013-05-31 06:18:30'),(469,'8405','Mathematics/Statistics/Computer Science','84','2013-05-31 09:18:30','2013-05-31 06:18:30'),(470,'8407','Mathematics with Statistics','84','2013-05-31 09:18:30','2013-05-31 06:18:30'),(471,'8408','Mathematics with Computer Science','84','2013-05-31 09:18:30','2013-05-31 06:18:30'),(472,'8409','Statistics','84','2013-05-31 09:18:30','2013-05-31 06:18:30'),(473,'8410','Statistics/Computer Science','84','2013-05-31 09:18:30','2013-05-31 06:18:30'),(474,'8500','Library Science','85','2013-05-31 09:18:30','2013-05-31 06:18:30'),(475,'8501','Library Studies','85','2013-05-31 09:18:30','2013-05-31 06:18:30'),(476,'8502','Library and Information Science','85','2013-05-31 09:18:30','2013-05-31 06:18:30'),(477,'8600','Sport Science','86','2013-05-31 09:18:30','2013-05-31 06:18:30'),(478,'8601','Exercise and Sport Science','86','2013-05-31 09:18:30','2013-05-31 06:18:30'),(479,'8700','Science Laboratory and Technology','87','2013-05-31 09:18:30','2013-05-31 06:18:30'),(480,'9000','Economics','90','2013-05-31 09:18:30','2013-05-31 06:18:30'),(481,'9001','Economic and Development Studies','90','2013-05-31 09:18:30','2013-05-31 06:18:30'),(482,'9002','Economic and Operation Research','90','2013-05-31 09:18:30','2013-05-31 06:18:30'),(483,'9003','Economics and Statistics','90','2013-05-31 09:18:30','2013-05-31 06:18:30'),(484,'9100','Geography','91','2013-05-31 09:18:30','2013-05-31 06:18:30'),(485,'9101','Demography and Social Statistics','91','2013-05-31 09:18:30','2013-05-31 06:18:30'),(486,'9102','Environmental Protection Resources Management','91','2013-05-31 09:18:30','2013-05-31 06:18:30'),(487,'9103','Geography and Planning','91','2013-05-31 09:18:30','2013-05-31 06:18:30'),(488,'9104','Geography and Meterology','91','2013-05-31 09:18:30','2013-05-31 06:18:30'),(489,'9105','Geography and Planning Science','91','2013-05-31 09:18:30','2013-05-31 06:18:30'),(490,'9106','Geography and Environmental Management','91','2013-05-31 09:18:30','2013-05-31 06:18:30'),(491,'9107','Geography and Environmental Studies','91','2013-05-31 09:18:30','2013-05-31 06:18:30'),(492,'9108','Geography and Resources Management','91','2013-05-31 09:18:30','2013-05-31 06:18:30'),(493,'9109','Geography and Regional Planning','91','2013-05-31 09:18:30','2013-05-31 06:18:30'),(494,'9110','Population Studies','91','2013-05-31 09:18:30','2013-05-31 06:18:30'),(495,'9200','International Studies','92','2013-05-31 09:18:30','2013-05-31 06:18:30'),(496,'9201','History and International Relations','92','2013-05-31 09:18:30','2013-05-31 06:18:30'),(497,'9202','International Business','92','2013-05-31 09:18:30','2013-05-31 06:18:30'),(498,'9203','International Relationship and Strategic Studies','92','2013-05-31 09:18:30','2013-05-31 06:18:30'),(499,'9204','International and Comparative Politics','92','2013-05-31 09:18:30','2013-05-31 06:18:30'),(500,'9205','International Studies and Diplomacy','92','2013-05-31 09:18:30','2013-05-31 06:18:30'),(501,'9206','International Relations','92','2013-05-31 09:18:30','2013-05-31 06:18:30'),(502,'9207','International Relationship and Diplomacy','92','2013-05-31 09:18:30','2013-05-31 06:18:30'),(503,'9208','International and Comparative Politics','92','2013-05-31 09:18:30','2013-05-31 06:18:30'),(504,'9209','International Studies and Diplomacy','92','2013-05-31 09:18:30','2013-05-31 06:18:30'),(505,'9210','History and International Relations','92','2013-05-31 09:18:30','2013-05-31 06:18:30'),(506,'9211','French and International Relations','92','2013-05-31 09:18:30','2013-05-31 06:18:30'),(507,'9212','Peace and Development Studies','92','2013-05-31 09:18:30','2013-05-31 06:18:30'),(508,'9213','Peace and Conflict Studies','92','2013-05-31 09:18:30','2013-05-31 06:18:30'),(509,'9214','Policy and Strategic Studies','92','2013-05-31 09:18:30','2013-05-31 06:18:30'),(510,'9215','Regional Integration and Diplomacy','92','2013-05-31 09:18:30','2013-05-31 06:18:30'),(511,'9300','Mass Communication','93','2013-05-31 09:18:30','2013-05-31 06:18:30'),(512,'9301','Mass Communication and Media Technology','93','2013-05-31 09:18:30','2013-05-31 06:18:30'),(513,'9302','Communication Arts','93','2013-05-31 09:18:30','2013-05-31 06:18:30'),(514,'9303','Communication and Multimedia Advertising','93','2013-05-31 09:18:30','2013-05-31 06:18:30'),(515,'9304','Communication and Multimedia Printing Journalism','93','2013-05-31 09:18:30','2013-05-31 06:18:30'),(516,'9305','Communication and Multimedia Television/Filming','93','2013-05-31 09:18:30','2013-05-31 06:18:30'),(517,'9306','Communication and Multimedia Design','93','2013-05-31 09:18:30','2013-05-31 06:18:30'),(518,'9307','Communication and Language Arts','93','2013-05-31 09:18:30','2013-05-31 06:18:30'),(519,'9400','Political Science','94','2013-05-31 09:18:30','2013-05-31 06:18:30'),(520,'9401','Government and Public Administration','94','2013-05-31 09:18:30','2013-05-31 06:18:30'),(521,'9402','Political Science and History','94','2013-05-31 09:18:30','2013-05-31 06:18:30'),(522,'9403','Political Philosophy/Economics','94','2013-05-31 09:18:30','2013-05-31 06:18:30'),(523,'9404','Political Science and Public Administration','94','2013-05-31 09:18:30','2013-05-31 06:18:30'),(524,'9405','Political Science and Diplomacy','94','2013-05-31 09:18:30','2013-05-31 06:18:30'),(525,'9406','Political Science and Conflict Resolution','94','2013-05-31 09:18:30','2013-05-31 06:18:30'),(526,'9407','Political Science and International Relations','94','2013-05-31 09:18:30','2013-05-31 06:18:30'),(527,'9408','Political Science and Administrative Studies','94','2013-05-31 09:18:30','2013-05-31 06:18:30'),(528,'9409','Policy and Administrative Studies','94','2013-05-31 09:18:30','2013-05-31 06:18:30'),(529,'9500','Sociology and Psychology','95','2013-05-31 09:18:30','2013-05-31 06:18:30'),(530,'9501','Anthropology','95','2013-05-31 09:18:30','2013-05-31 06:18:30'),(531,'9502','Criminology and Penelogy','95','2013-05-31 09:18:30','2013-05-31 06:18:30'),(532,'9503','Gender and Woman Studies','95','2013-05-31 09:18:30','2013-05-31 06:18:30'),(533,'9504','Intelligence and Security Services','95','2013-05-31 09:18:30','2013-05-31 06:18:30'),(534,'9505','Psychology and Human Development','95','2013-05-31 09:18:30','2013-05-31 06:18:30'),(535,'9506','Psychology','95','2013-05-31 09:18:30','2013-05-31 06:18:30'),(536,'9507','Pure and Applied Psychology','95','2013-05-31 09:18:30','2013-05-31 06:18:30'),(537,'9508','Social Work and Administration','95','2013-05-31 09:18:30','2013-05-31 06:18:30'),(538,'9509','Social Work/Community Development','95','2013-05-31 09:18:30','2013-05-31 06:18:30'),(539,'9510','Sociology','95','2013-05-31 09:18:30','2013-05-31 06:18:30'),(540,'9511','Social Work','95','2013-05-31 09:18:30','2013-05-31 06:18:30'),(541,'9512','Sociology and Anthropology','95','2013-05-31 09:18:30','2013-05-31 06:18:30'),(542,'9513','Archeology','95','2013-05-31 09:18:30','2013-05-31 06:18:30'),(543,'9600','Hospitality and Tourism','96','2013-05-31 09:18:30','2013-05-31 06:18:30'),(544,'9601','Hotel and Tourism Management','96','2013-05-31 09:18:30','2013-05-31 06:18:30'),(545,'9602','Hospitality and Tourism Management','96','2013-05-31 09:18:30','2013-05-31 06:18:30'),(546,'9603','Tourism and Event Management','96','2013-05-31 09:18:30','2013-05-31 06:18:30');

/*Table structure for table `departments` */

DROP TABLE IF EXISTS `departments`;

CREATE TABLE `departments` (
  `deptid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(255) unsigned NOT NULL,
  `departments` varchar(500) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `id_head` int(255) unsigned NOT NULL DEFAULT '0',
  `id_parent` int(255) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`deptid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `departments` */

/*Table structure for table `devcmds` */

DROP TABLE IF EXISTS `devcmds`;

CREATE TABLE `devcmds` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SN` varchar(20) NOT NULL,
  `CmdContent` longtext NOT NULL,
  `CmdCommitTime` datetime NOT NULL,
  `CmdTransTime` datetime DEFAULT NULL,
  `CmdOverTime` datetime DEFAULT NULL,
  `CmdReturn` int(11) DEFAULT NULL,
  `User_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `devcmds` */

/*Table structure for table `devices` */

DROP TABLE IF EXISTS `devices`;

CREATE TABLE `devices` (
  `sn` varchar(20) NOT NULL,
  `state` int(11) NOT NULL,
  `last_activity` datetime DEFAULT NULL,
  `trans_times` varchar(50) DEFAULT NULL,
  `trans_interval` int(11) NOT NULL,
  `log_stamp` varchar(20) DEFAULT NULL,
  `op_log_stamp` varchar(20) DEFAULT NULL,
  `photo_stamp` varchar(20) DEFAULT NULL,
  `alias` varchar(20) NOT NULL,
  `dept_id` int(11) DEFAULT NULL,
  `update_db` varchar(10) NOT NULL,
  `style` varchar(20) DEFAULT NULL,
  `firmware_version` varchar(30) DEFAULT NULL,
  `fp_count` int(11) DEFAULT NULL,
  `transaction_count` int(11) DEFAULT NULL,
  `user_count` int(11) DEFAULT NULL,
  `main_time` varchar(20) DEFAULT NULL,
  `max_finger_count` int(11) DEFAULT NULL,
  `max_att_log_count` int(11) DEFAULT NULL,
  `device_name` varchar(30) DEFAULT NULL,
  `alg_ver` varchar(30) DEFAULT NULL,
  `flash_size` varchar(10) DEFAULT NULL,
  `free_flash_size` varchar(10) DEFAULT NULL,
  `language` varchar(30) DEFAULT NULL,
  `volume` varchar(10) DEFAULT NULL,
  `dt_fmt` varchar(10) DEFAULT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  `is_tft` varchar(5) DEFAULT NULL,
  `platform` varchar(20) DEFAULT NULL,
  `brightness` varchar(5) DEFAULT NULL,
  `backup_dev` varchar(30) DEFAULT NULL,
  `oem_vendor` varchar(30) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `acc_fun` smallint(6) NOT NULL,
  `tz_adj` smallint(6) NOT NULL,
  `del_tag` smallint(6) NOT NULL,
  `fp_version` varchar(10) DEFAULT NULL,
  `push_version` varchar(10) DEFAULT NULL,
  `id_string` varchar(150) DEFAULT NULL,
  `location` varchar(150) DEFAULT NULL,
  `date_added` date DEFAULT NULL,
  PRIMARY KEY (`sn`),
  KEY `iclock_DeptID` (`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `devices` */

/*Table structure for table `employee_bank_details` */

DROP TABLE IF EXISTS `employee_bank_details`;

CREATE TABLE `employee_bank_details` (
  `id_employee_bank_detail` int(255) NOT NULL AUTO_INCREMENT,
  `id_user` int(255) NOT NULL,
  `id_bank` int(255) NOT NULL,
  `bank_account_name` varchar(255) NOT NULL,
  `bank_account_number` varchar(255) NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_bank_account_type` int(255) NOT NULL,
  `default` int(255) unsigned NOT NULL DEFAULT '0',
  `ref_id` varchar(255) DEFAULT NULL,
  `last_validated` datetime DEFAULT NULL,
  PRIMARY KEY (`id_employee_bank_detail`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employee_bank_details` */

/*Table structure for table `employee_bank_details_temp` */

DROP TABLE IF EXISTS `employee_bank_details_temp`;

CREATE TABLE `employee_bank_details_temp` (
  `id_employee_bank_detail` int(255) NOT NULL AUTO_INCREMENT,
  `id_user` int(255) NOT NULL,
  `id_bank` int(255) NOT NULL,
  `bank_account_name` varchar(255) NOT NULL,
  `bank_account_number` varchar(255) NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_bank_account_type` int(255) NOT NULL,
  `default` int(255) unsigned NOT NULL DEFAULT '0',
  `ref_id` varchar(255) DEFAULT NULL,
  `last_validated` datetime DEFAULT NULL,
  PRIMARY KEY (`id_employee_bank_detail`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employee_bank_details_temp` */

/*Table structure for table `employee_credentials` */

DROP TABLE IF EXISTS `employee_credentials`;

CREATE TABLE `employee_credentials` (
  `id_user` int(255) unsigned NOT NULL,
  `type` int(1) unsigned NOT NULL DEFAULT '0',
  `description` text,
  `file_name` varchar(255) DEFAULT NULL,
  `file_uri` text,
  `id_uploader_user` int(255) unsigned NOT NULL DEFAULT '0',
  `id_cred` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `file_title` varchar(255) NOT NULL,
  `due_date` date DEFAULT NULL,
  `container` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_cred`),
  KEY `id_user` (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

/*Data for the table `employee_credentials` */

/*Table structure for table `employee_data_approver` */

DROP TABLE IF EXISTS `employee_data_approver`;

CREATE TABLE `employee_data_approver` (
  `id_employee_data_approver` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_employee_data_approver`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `employee_data_approver` */

/*Table structure for table `employee_job_types` */

DROP TABLE IF EXISTS `employee_job_types`;

CREATE TABLE `employee_job_types` (
  `id_user` int(255) unsigned NOT NULL,
  `id_job` int(255) unsigned NOT NULL,
  KEY `id_user` (`id_user`),
  KEY `all` (`id_user`,`id_job`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `employee_job_types` */

/*Table structure for table `employee_notes` */

DROP TABLE IF EXISTS `employee_notes`;

CREATE TABLE `employee_notes` (
  `id_employee_note` int(255) NOT NULL AUTO_INCREMENT,
  `id_company` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `id_creator` int(255) NOT NULL,
  `creator_access_level` int(255) NOT NULL,
  `access` int(1) NOT NULL,
  `note` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `priority` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_employee_note`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employee_notes` */

/*Table structure for table `employee_phone` */

DROP TABLE IF EXISTS `employee_phone`;

CREATE TABLE `employee_phone` (
  `id_employee_phone` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(255) unsigned NOT NULL,
  `id_phone_type` int(255) unsigned NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `primary` int(255) unsigned DEFAULT '0',
  `id_country_code` int(255) unsigned DEFAULT NULL,
  `id_company` int(255) unsigned DEFAULT NULL,
  `ref_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_employee_phone`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `employee_phone` */

/*Table structure for table `employee_phone_temp` */

DROP TABLE IF EXISTS `employee_phone_temp`;

CREATE TABLE `employee_phone_temp` (
  `id_employee_phone` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(255) unsigned NOT NULL,
  `id_phone_type` int(255) unsigned NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `primary` int(255) unsigned DEFAULT '0',
  `id_country_code` int(255) unsigned DEFAULT NULL,
  `id_company` int(255) unsigned DEFAULT NULL,
  `ref_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_employee_phone`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `employee_phone_temp` */

/*Table structure for table `employee_query` */

DROP TABLE IF EXISTS `employee_query`;

CREATE TABLE `employee_query` (
  `id_employee_query` int(255) NOT NULL AUTO_INCREMENT,
  `query_number` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_user_from` int(255) NOT NULL,
  `id_user_to` int(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `date_issued` varchar(255) NOT NULL,
  `date_due` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `id_task` int(200) NOT NULL,
  `pdf_link` varchar(255) NOT NULL,
  PRIMARY KEY (`id_employee_query`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employee_query` */

/*Table structure for table `employee_query_response` */

DROP TABLE IF EXISTS `employee_query_response`;

CREATE TABLE `employee_query_response` (
  `id_employee_query_response` int(255) NOT NULL AUTO_INCREMENT,
  `id_employee_query` int(255) NOT NULL,
  `id_user_from` int(255) NOT NULL,
  `id_user_to` int(255) NOT NULL,
  `details` text NOT NULL,
  `date_issued` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id_employee_query_response`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employee_query_response` */

/*Table structure for table `employee_request` */

DROP TABLE IF EXISTS `employee_request`;

CREATE TABLE `employee_request` (
  `id_employee_request` int(255) NOT NULL AUTO_INCREMENT,
  `id_request_template` int(255) NOT NULL,
  `template_code` varchar(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `request` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `instruction` text NOT NULL,
  `request_date` timestamp NULL DEFAULT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id_employee_request`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employee_request` */

/*Table structure for table `employee_request_approval` */

DROP TABLE IF EXISTS `employee_request_approval`;

CREATE TABLE `employee_request_approval` (
  `id_request_approval` int(255) NOT NULL AUTO_INCREMENT,
  `template_code` varchar(255) NOT NULL,
  `id_employee_request` int(255) NOT NULL,
  `id_approver` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `stage` int(10) NOT NULL,
  PRIMARY KEY (`id_request_approval`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employee_request_approval` */

/*Table structure for table `employee_request_approval_stage` */

DROP TABLE IF EXISTS `employee_request_approval_stage`;

CREATE TABLE `employee_request_approval_stage` (
  `id_request_approval_stage` int(255) NOT NULL AUTO_INCREMENT,
  `template_code` varchar(255) NOT NULL,
  `id_request_template` int(255) NOT NULL,
  `id_employee_request` int(255) NOT NULL,
  `id_approver` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `stage` int(10) NOT NULL,
  PRIMARY KEY (`id_request_approval_stage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employee_request_approval_stage` */

/*Table structure for table `employee_request_attachment` */

DROP TABLE IF EXISTS `employee_request_attachment`;

CREATE TABLE `employee_request_attachment` (
  `id_request_attachment` int(255) NOT NULL AUTO_INCREMENT,
  `id_request_template` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `template_code` varchar(255) NOT NULL,
  `attachment_name` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  PRIMARY KEY (`id_request_attachment`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employee_request_attachment` */

/*Table structure for table `employee_request_employee_attachment` */

DROP TABLE IF EXISTS `employee_request_employee_attachment`;

CREATE TABLE `employee_request_employee_attachment` (
  `id_request_attachment` int(255) NOT NULL AUTO_INCREMENT,
  `id_employee_request` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `template_code` varchar(255) NOT NULL,
  `attachment_name` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  PRIMARY KEY (`id_request_attachment`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employee_request_employee_attachment` */

/*Table structure for table `employee_request_employee_docs` */

DROP TABLE IF EXISTS `employee_request_employee_docs`;

CREATE TABLE `employee_request_employee_docs` (
  `id_supporting_document` int(255) NOT NULL AUTO_INCREMENT,
  `template_code` varchar(255) NOT NULL,
  `id_request_template` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `doc` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id_supporting_document`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employee_request_employee_docs` */

/*Table structure for table `employee_request_entries` */

DROP TABLE IF EXISTS `employee_request_entries`;

CREATE TABLE `employee_request_entries` (
  `id_employee_request_entry` int(255) NOT NULL AUTO_INCREMENT,
  `id_temp_request` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_user` int(255) unsigned NOT NULL,
  `field_name` varchar(255) NOT NULL,
  `field_value` varchar(255) NOT NULL,
  `request_type` int(255) unsigned NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_employee_request_entry`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `employee_request_entries` */

/*Table structure for table `employee_request_supporting_documents` */

DROP TABLE IF EXISTS `employee_request_supporting_documents`;

CREATE TABLE `employee_request_supporting_documents` (
  `id_supporting_document` int(255) NOT NULL AUTO_INCREMENT,
  `template_code` varchar(255) NOT NULL,
  `id_request_template` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id_supporting_document`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employee_request_supporting_documents` */

/*Table structure for table `employee_request_template` */

DROP TABLE IF EXISTS `employee_request_template`;

CREATE TABLE `employee_request_template` (
  `id_request_template` int(255) NOT NULL AUTO_INCREMENT,
  `template_code` varchar(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `instruction` text NOT NULL,
  `confirmation_msg` text NOT NULL,
  `date_added` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_request_template`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employee_request_template` */

/*Table structure for table `employee_temp_request` */

DROP TABLE IF EXISTS `employee_temp_request`;

CREATE TABLE `employee_temp_request` (
  `id_temp_request` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(255) unsigned NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `due_date` date NOT NULL,
  PRIMARY KEY (`id_temp_request`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employee_temp_request` */

/*Table structure for table `employee_temp_request_task` */

DROP TABLE IF EXISTS `employee_temp_request_task`;

CREATE TABLE `employee_temp_request_task` (
  `id_employee_temp_request_task_id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_temp_request` int(255) unsigned NOT NULL,
  `id_task` int(255) unsigned NOT NULL,
  `request_type` int(255) unsigned NOT NULL,
  `status` int(255) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_employee_temp_request_task_id`),
  KEY `FK_employee_temp_request_task_1` (`id_temp_request`),
  KEY `FK_employee_temp_request_task_2` (`id_task`),
  CONSTRAINT `FK_employee_temp_request_task_1` FOREIGN KEY (`id_temp_request`) REFERENCES `employee_temp_request` (`id_temp_request`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_employee_temp_request_task_2` FOREIGN KEY (`id_task`) REFERENCES `tasks` (`id_task`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

/*Data for the table `employee_temp_request_task` */

/*Table structure for table `employee_vacation_allowance_stages` */

DROP TABLE IF EXISTS `employee_vacation_allowance_stages`;

CREATE TABLE `employee_vacation_allowance_stages` (
  `id_employee_allowance_stage` int(255) NOT NULL AUTO_INCREMENT,
  `id_allowance_stage` int(255) NOT NULL,
  `id_approver` int(255) NOT NULL,
  `approver` varchar(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `stage` int(255) NOT NULL,
  PRIMARY KEY (`id_employee_allowance_stage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employee_vacation_allowance_stages` */

/*Table structure for table `employee_vacation_approval_stages` */

DROP TABLE IF EXISTS `employee_vacation_approval_stages`;

CREATE TABLE `employee_vacation_approval_stages` (
  `id_employee_approval_stage` int(255) NOT NULL AUTO_INCREMENT,
  `id_approver` int(255) NOT NULL,
  `approver` varchar(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `stage` int(255) NOT NULL,
  PRIMARY KEY (`id_employee_approval_stage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `employee_vacation_approval_stages` */

/*Table structure for table `employees` */

DROP TABLE IF EXISTS `employees`;

CREATE TABLE `employees` (
  `id_user` int(255) unsigned NOT NULL,
  `id_role` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_staff` varchar(255) DEFAULT '0',
  `id_activated_by` int(255) unsigned DEFAULT '0',
  `id_dept` int(255) unsigned DEFAULT '0',
  `id_job_type` int(255) unsigned DEFAULT '0',
  `id_manager` int(255) unsigned DEFAULT '0',
  `employment_date` timestamp NULL DEFAULT NULL,
  `date_fire` timestamp NULL DEFAULT NULL,
  `lock_mode` int(1) unsigned DEFAULT '0',
  `work_number` varchar(255) DEFAULT NULL,
  `is_manager` int(1) unsigned NOT NULL DEFAULT '0',
  `id_access_level` int(255) unsigned NOT NULL,
  `bank_account_name` varchar(255) DEFAULT NULL,
  `id_bank` int(255) unsigned NOT NULL DEFAULT '0',
  `id_bank_account_type` int(255) unsigned NOT NULL DEFAULT '0',
  `bank_account_number` varchar(255) DEFAULT NULL,
  `bank_request_date` timestamp NULL DEFAULT NULL,
  `bank_request_instructions` text,
  `id_manager_vacation` int(255) NOT NULL DEFAULT '1',
  `id_country_code` int(255) DEFAULT '0',
  KEY `id_user` (`id_user`),
  KEY `user_company` (`id_user`,`id_company`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

/*Data for the table `employees` */

/*Table structure for table `event_invitees` */

DROP TABLE IF EXISTS `event_invitees`;

CREATE TABLE `event_invitees` (
  `id_invite` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_event` int(255) unsigned NOT NULL,
  `id_user` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_invite`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `event_invitees` */

/*Table structure for table `events` */

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `id_event` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `event_name` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `location` varchar(255) NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `status` int(1) unsigned NOT NULL DEFAULT '1',
  `date_start` timestamp NULL DEFAULT NULL,
  `date_end` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_event`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `events` */

/*Table structure for table `goals` */

DROP TABLE IF EXISTS `goals`;

CREATE TABLE `goals` (
  `id_goal` int(255) NOT NULL AUTO_INCREMENT,
  `id_user` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `goal` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `end_result` text NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `month` varchar(255) NOT NULL,
  `year` int(4) NOT NULL,
  `recurring` varchar(255) NOT NULL,
  `term` varchar(20) NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `progress` int(3) NOT NULL DEFAULT '0',
  `end_date` timestamp NULL DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `approval_status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_goal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `goals` */

/*Table structure for table `goals_access` */

DROP TABLE IF EXISTS `goals_access`;

CREATE TABLE `goals_access` (
  `id_goals_access` int(50) NOT NULL AUTO_INCREMENT,
  `id_company` int(50) NOT NULL,
  `access_level` varchar(255) NOT NULL,
  `goal_type` varchar(50) NOT NULL,
  `id_user` int(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_goals_access`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `goals_access` */

/*Table structure for table `goals_approver` */

DROP TABLE IF EXISTS `goals_approver`;

CREATE TABLE `goals_approver` (
  `id_goals_approver` int(50) NOT NULL AUTO_INCREMENT,
  `id_goal` int(50) NOT NULL,
  `id_approver` int(50) NOT NULL,
  `approver` varchar(150) NOT NULL,
  `stage` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id_goals_approver`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `goals_approver` */

/*Table structure for table `goals_approver_stage` */

DROP TABLE IF EXISTS `goals_approver_stage`;

CREATE TABLE `goals_approver_stage` (
  `id_goals_approver_stage` int(50) NOT NULL AUTO_INCREMENT,
  `id_company` int(50) NOT NULL,
  `type` varchar(255) NOT NULL,
  `approver` varchar(255) NOT NULL,
  `id_approver` int(50) NOT NULL,
  `stage` int(5) NOT NULL,
  `goal_type` varchar(50) NOT NULL,
  PRIMARY KEY (`id_goals_approver_stage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `goals_approver_stage` */

/*Table structure for table `goals_company` */

DROP TABLE IF EXISTS `goals_company`;

CREATE TABLE `goals_company` (
  `id_goal_company` int(255) NOT NULL AUTO_INCREMENT,
  `goal_code` varchar(50) NOT NULL,
  `id_company` int(255) NOT NULL,
  `goal` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `end_result` text NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `month` varchar(255) NOT NULL,
  `year` int(4) NOT NULL,
  `recurring` varchar(255) NOT NULL,
  `id_dept` int(50) NOT NULL,
  `id_location` int(50) NOT NULL,
  `term` varchar(20) NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `status` int(1) NOT NULL,
  `approval_status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_goal_company`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `goals_company` */

/*Table structure for table `goals_company_users` */

DROP TABLE IF EXISTS `goals_company_users`;

CREATE TABLE `goals_company_users` (
  `id_company` int(50) NOT NULL,
  `id_user` int(50) NOT NULL,
  `id_goal` int(50) NOT NULL,
  `id_goal_company` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `goals_company_users` */

/*Table structure for table `goals_progress` */

DROP TABLE IF EXISTS `goals_progress`;

CREATE TABLE `goals_progress` (
  `id_goal_progress` int(255) NOT NULL AUTO_INCREMENT,
  `id_goal` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `progress` int(3) NOT NULL,
  `progress_total` int(3) NOT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `id_updated_by` int(255) NOT NULL,
  PRIMARY KEY (`id_goal_progress`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `goals_progress` */

/*Table structure for table `hr_clients` */

DROP TABLE IF EXISTS `hr_clients`;

CREATE TABLE `hr_clients` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `id_client` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_user` int(255) unsigned NOT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_clients` */

/*Table structure for table `hr_consultant` */

DROP TABLE IF EXISTS `hr_consultant`;

CREATE TABLE `hr_consultant` (
  `id_consultant` int(255) NOT NULL AUTO_INCREMENT,
  `consultant_ref_id` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_consultant`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_consultant` */

/*Table structure for table `hr_consultant_speciality` */

DROP TABLE IF EXISTS `hr_consultant_speciality`;

CREATE TABLE `hr_consultant_speciality` (
  `id_speciality` int(255) NOT NULL AUTO_INCREMENT,
  `id_company` int(255) NOT NULL,
  `speciality` varchar(255) NOT NULL,
  PRIMARY KEY (`id_speciality`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hr_consultant_speciality` */

/*Table structure for table `industries` */

DROP TABLE IF EXISTS `industries`;

CREATE TABLE `industries` (
  `id_industry` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_industry`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `industries` */

insert  into `industries`(`id_industry`,`name`) values (1,'Advertising/Media'),(2,'Agriculture/Poultry/Fishing'),(3,'Banking/Financial Services'),(4,'Media/Broadcasting'),(5,'Construction/Real Estate'),(6,'Consulting'),(7,'Education'),(8,'Fashion/Art'),(9,'Food Services'),(10,'Government'),(11,'Healthcare'),(12,'Hospitality/Leisure'),(13,'ICT/Telecommunications'),(14,'Legal'),(15,'Logistics/Transportation'),(16,'Manufacturing/Production'),(17,'Non-profit'),(18,'Oil &amp; Gas/Mining'),(19,'Others'),(20,'Retail/Wholesales'),(21,'Trade/Services'),(22,'Travels/Tours');

/*Table structure for table `job_attachments` */

DROP TABLE IF EXISTS `job_attachments`;

CREATE TABLE `job_attachments` (
  `id_file` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_job` int(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  PRIMARY KEY (`id_file`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `job_attachments` */

/*Table structure for table `job_categories` */

DROP TABLE IF EXISTS `job_categories`;

CREATE TABLE `job_categories` (
  `id_category` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id_category`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `job_categories` */

insert  into `job_categories`(`id_category`,`title`) values (1,'IT & Programming'),(2,'Design & Multimedia'),(3,'Writing & Translation'),(4,'Sales & Marketing'),(5,'Admin'),(6,'Engr & Manuf'),(7,'Finance & Mgmt'),(8,'Legal');

/*Table structure for table `job_categories_reference` */

DROP TABLE IF EXISTS `job_categories_reference`;

CREATE TABLE `job_categories_reference` (
  `id_category` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `new_id` int(25) DEFAULT NULL,
  PRIMARY KEY (`id_category`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `job_categories_reference` */

insert  into `job_categories_reference`(`id_category`,`title`,`new_id`) values (1,'IT & Programming',NULL),(2,'Design & Multimedia',NULL),(3,'Writing & Translation',NULL),(4,'Sales & Marketing',NULL),(5,'Admin',NULL),(6,'Engr & Manuf',NULL),(7,'Finance & Mgmt',NULL),(8,'Legal',NULL),(9,'Education',NULL),(10,'Agriculture',NULL),(11,'Arts',NULL),(12,'Construction',NULL),(13,'Consumer Goods',NULL),(14,'Corporate',NULL),(15,'Educational',NULL),(16,'Finance',NULL),(17,'Government',NULL),(18,'High Tech',NULL),(19,'Legal',NULL),(20,'Manufacturing',NULL),(21,'Media',NULL),(22,'Medical',NULL),(23,'Non-profit',NULL),(24,'Recreational',NULL),(25,'Service',NULL),(26,'Transportation',NULL);

/*Table structure for table `job_ccategories` */

DROP TABLE IF EXISTS `job_ccategories`;

CREATE TABLE `job_ccategories` (
  `id_job` int(255) unsigned NOT NULL,
  `id_category` int(255) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `job_ccategories` */

/*Table structure for table `job_cfile_attachments` */

DROP TABLE IF EXISTS `job_cfile_attachments`;

CREATE TABLE `job_cfile_attachments` (
  `id_job` int(255) unsigned NOT NULL,
  `file_attachment` text NOT NULL,
  `id_file_attachment` int(255) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_file_attachment`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `job_cfile_attachments` */

/*Table structure for table `job_cfunctions` */

DROP TABLE IF EXISTS `job_cfunctions`;

CREATE TABLE `job_cfunctions` (
  `id_job` int(255) unsigned NOT NULL,
  `id_function` int(255) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `job_cfunctions` */

/*Table structure for table `job_cquestions` */

DROP TABLE IF EXISTS `job_cquestions`;

CREATE TABLE `job_cquestions` (
  `id_question` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_job` int(255) unsigned NOT NULL,
  `question` text NOT NULL,
  PRIMARY KEY (`id_question`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `job_cquestions` */

/*Table structure for table `job_cstates` */

DROP TABLE IF EXISTS `job_cstates`;

CREATE TABLE `job_cstates` (
  `id_job` int(255) unsigned NOT NULL,
  `id_state` int(255) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `job_cstates` */

/*Table structure for table `job_ctypes` */

DROP TABLE IF EXISTS `job_ctypes`;

CREATE TABLE `job_ctypes` (
  `id_job` int(255) unsigned NOT NULL,
  `id_type` int(255) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `job_ctypes` */

/*Table structure for table `job_function_categories` */

DROP TABLE IF EXISTS `job_function_categories`;

CREATE TABLE `job_function_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_category` varchar(10) NOT NULL,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id_category`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `job_function_categories` */

insert  into `job_function_categories`(`id`,`id_category`,`name`) values (1,'1','Accounting & Financial Services'),(2,'2','Engineering '),(3,'3','Human Resources'),(4,'4','IT'),(5,'5','Legal'),(6,'6','Oil & Gas'),(7,'7','Procurement & Supply Chain'),(8,'8','Sales & Marketing');

/*Table structure for table `job_functions` */

DROP TABLE IF EXISTS `job_functions`;

CREATE TABLE `job_functions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_function` varchar(10) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `id_category` varchar(10) NOT NULL,
  PRIMARY KEY (`id_function`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `job_functions` */

insert  into `job_functions`(`id`,`id_function`,`name`,`id_category`) values (1,'1000','Accountant / Bookkeeper','1'),(2,'1001','Accounts Payable','1'),(3,'1002','Accounts Receivable','1'),(4,'1003','Back Office - Operations','1'),(5,'1004','Business Analyst/Systems Analyst','1'),(6,'1005','Chief Executive Officer','1'),(7,'1006','Chief Financial Officer','1'),(8,'1007','Compliance','1'),(9,'1008','Credit Research','1'),(10,'1009','Credit Risk','1'),(11,'1010','Equity Research','1'),(12,'1011','External Reporting/Corporate Accounting','1'),(13,'1012','Financial Controller','1'),(14,'1013','Financial Planning Manager','1'),(15,'1014','Financial Reporting IFRS','1'),(16,'1015','Fund Accounting','1'),(17,'1016','Institutional Sales','1'),(18,'1017','Internal Audit','1'),(19,'1018','Investment Banking','1'),(20,'1019','Lawyer/General Counsel','1'),(21,'1020','Management Consulting','1'),(22,'1021','Market Risk','1'),(23,'1022','Middle Office - Operations','1'),(24,'1023','Non-Profit Development','1'),(25,'1024','Non-Profit Grant Accounting','1'),(26,'1025','Operational Risk','1'),(27,'1026','Ops-Client/Investor Relations','1'),(28,'1027','Payroll','1'),(29,'1028','Performance Management','1'),(30,'1029','Portfolio Manager','1'),(31,'1030','Private Banking','1'),(32,'1031','Product Control','1'),(33,'1032','Project Manager - Financial Services','1'),(34,'1033','Public Practice - Audit','1'),(35,'1034','Public Practise - Corporate Finance','1'),(36,'1035','Public Practise - Tax Trusts','1'),(37,'1036','Quantititative Risk Analyst','1'),(38,'1037','Recent Graduate','1'),(39,'1038','Recruitment Consultant','1'),(40,'1039','Relationship Manager','1'),(41,'1040','Retail Banking - Mortgage','1'),(42,'1041','Risk Management/Quantitative Analyst','1'),(43,'1042','Structured Finance','1'),(44,'1043','Tax','1'),(45,'1044','Trading','1'),(46,'1045','Treasury','1'),(47,'1046','Underwriter','1'),(48,'2000','After Sales Service (Field Service)','2'),(49,'2001','Application Engineering','2'),(50,'2002','Automation Specialist','2'),(51,'2003','Calculation Engineering','2'),(52,'2004','Chemical & Process Engineering','2'),(53,'2005','Civil / Structural Engineering','2'),(54,'2006','Draughtsman','2'),(55,'2007','E&M Health, Safety & Environment','2'),(56,'2008','E&M Project Management','2'),(57,'2009','E&M Quality','2'),(58,'2010','Electrical / Electronic Engineering','2'),(59,'2011','Engineering / Manufacturing Design','2'),(60,'2012','Faciilities Management / Technical Manager','2'),(61,'2013','Lean Manufacturing / Processing','2'),(62,'2014','Mechanical Engineering','2'),(63,'2015','Operations','2'),(64,'2016','Plant Manager','2'),(65,'2017','Production / Manufacturing','2'),(66,'2018','Research & Development','2'),(67,'3000','HR Assistant/Administrator','3'),(68,'3001','HR Benefits','3'),(69,'3002','HR Campus Recruiter','3'),(70,'3003','HR Compensation','3'),(71,'3004','HR Director','3'),(72,'3005','HR Generalist','3'),(73,'3006','HR Manager','3'),(74,'3007','HR Recruiter','3'),(75,'3008','HR Training & Development','3'),(76,'3009','Recruitment Consultant','3'),(77,'3010','Senior Vice President HR','3'),(78,'3011','Vice President HR','3'),(79,'4000','Business Analysis','4'),(80,'4001','Development','4'),(81,'4002','Helpdesk / Support','4'),(82,'4003','Infrastructure','4'),(83,'4004','IT Architect','4'),(84,'4005','IT Director / CTO / CIO','4'),(85,'4006','IT Manager','4'),(86,'4007','IT Training','4'),(87,'4008','Project/Programme Manager (PMO)','4'),(88,'5000','Associate - Law Firm','5'),(89,'5001','Contracts Manager - In-house','5'),(90,'5002','In-house Assistant General Counsel','5'),(91,'5003','In-house Attorney','5'),(92,'5004','Legal Secretary / Support Staff','5'),(93,'5005','Paralegal','5'),(94,'6000','Offshore Services','6'),(95,'6001','Oil & Gas Health, Safety & Environment Quality','6'),(96,'6002','Oil & Gas Project / Contract Management','6'),(97,'6003','Subsea Engineering','6'),(98,'7000','Contract & Vendor Management','7'),(99,'7001','Customer Service (Supply Chain)','7'),(100,'7002','Demand Planning','7'),(101,'7003','Direct Procurement','7'),(102,'7004','Import / Export','7'),(103,'7005','Indirect Procurement ','7'),(104,'7006','Information Flow / Logistics (Head Office)','7'),(105,'7007','Inventory Manager','7'),(106,'7008','Material Handler','7'),(107,'7009','Order Management / Master Data PO Number (Purchase Order)','7'),(108,'7010','Procurement / Spend Analysis','7'),(109,'7011','Procurement Coordinator','7'),(110,'7012','Procurement Director','7'),(111,'7013','Project Buyer','7'),(112,'7014','Project Consultancy','7'),(113,'7015','Real Flow / Logistics (In Factory)','7'),(114,'7016','Supply Chain Analysis','7'),(115,'7017','Supply Chain Director','7'),(116,'7018','Supply Chain Manager','7'),(117,'7019','Supply Planning','7'),(118,'7020','Transport / Distribution Manager','7'),(119,'7021','Warehouse Manager','7'),(120,'8000','Account Manager','8'),(121,'8001','Advertising','8'),(122,'8002','Agency','8'),(123,'8003','Analysis & Planning','8'),(124,'8004','Brand Marketing','8'),(125,'8005','Channel Sales','8'),(126,'8006','Client / Investor Relations','8'),(127,'8007','Creative','8'),(128,'8008','Direct Marketing','8'),(129,'8009','Direct Sales','8'),(130,'8010','Event / Roadshow','8'),(131,'8011','Field Sales','8'),(132,'8012','General Management','8'),(133,'8013','Inside Sales','8'),(134,'8014','Marketing Assistant','8'),(135,'8015','Marketing Communications','8'),(136,'8016','Marketing Management','8'),(137,'8017','Marketing Director / VP','8'),(138,'8018','New Business Development','8'),(139,'8019','Online Marketing','8'),(140,'8020','Online Sales','8'),(141,'8021','Product Marketing','8'),(142,'8022','Public Relations','8'),(143,'8023','Recent Graduate','8'),(144,'8024','Recruitment Consultant','8'),(145,'8025','Retail Sales','8'),(146,'8026','Sales B2B','8'),(147,'8027','Sales Manager /Director / VP','8'),(148,'8028','Trade Marketing / Category Marketing','8'),(149,'8029','Writing / Editor','8');

/*Table structure for table `job_functions_reference` */

DROP TABLE IF EXISTS `job_functions_reference`;

CREATE TABLE `job_functions_reference` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_function` varchar(10) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `id_category` varchar(10) NOT NULL,
  `new_id` int(25) NOT NULL,
  PRIMARY KEY (`id_function`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `job_functions_reference` */

insert  into `job_functions_reference`(`id`,`id_function`,`name`,`id_category`,`new_id`) values (1,'1000','Accountant / Bookkeeper','1',9001),(2,'1001','Accounts Payable','1',9001),(3,'1002','Accounts Receivable','1',9001),(4,'1003','Back Office - Operations','1',9013),(5,'1004','Business Analyst/Systems Analyst','1',9013),(6,'1005','Chief Executive Officer','1',9013),(7,'1006','Chief Financial Officer','1',9013),(8,'1007','Compliance','1',9013),(9,'1008','Credit Research','1',9013),(10,'1009','Credit Risk','1',9013),(11,'1010','Equity Research','1',9013),(12,'1011','External Reporting/Corporate Accounting','1',9001),(13,'1012','Financial Controller','1',9013),(14,'1013','Financial Planning Manager','1',9013),(15,'1014','Financial Reporting IFRS','1',9001),(16,'1015','Fund Accounting','1',9001),(17,'1016','Institutional Sales','1',9013),(18,'1017','Internal Audit','1',9013),(19,'1018','Investment Banking','1',9013),(20,'1019','Lawyer/General Counsel','1',9013),(21,'1020','Management Consulting','1',9013),(22,'1021','Market Risk','1',9013),(23,'1022','Middle Office - Operations','1',9013),(24,'1023','Non-Profit Development','1',9013),(25,'1024','Non-Profit Grant Accounting','1',9013),(26,'1025','Operational Risk','1',9013),(27,'1026','Ops-Client/Investor Relations','1',9013),(28,'1027','Payroll','1',9001),(29,'1028','Performance Management','1',9015),(30,'1029','Portfolio Manager','1',9013),(31,'1030','Private Banking','1',9013),(32,'1031','Product Control','1',9013),(33,'1032','Project Manager - Financial Services','1',9013),(34,'1033','Public Practice - Audit','1',9001),(35,'1034','Public Practise - Corporate Finance','1',9013),(36,'1035','Public Practise - Tax Trusts','1',9001),(37,'1036','Quantititative Risk Analyst','1',9013),(38,'1037','Recent Graduate','1',9013),(39,'1038','Recruitment Consultant','1',9015),(40,'1039','Relationship Manager','1',9013),(41,'1040','Retail Banking - Mortgage','1',9013),(42,'1041','Risk Management/Quantitative Analyst','1',9013),(43,'1042','Structured Finance','1',9013),(44,'1043','Tax','1',9001),(45,'1044','Trading','1',9013),(46,'1045','Treasury','1',9013),(47,'1046','Underwriter','1',9013),(48,'2000','After Sales Service (Field Service)','2',9012),(49,'2001','Application Engineering','2',9012),(50,'2002','Automation Specialist','2',9012),(51,'2003','Calculation Engineering','2',9012),(52,'2004','Chemical & Process Engineering','2',9012),(53,'2005','Civil / Structural Engineering','2',9012),(54,'2006','Draughtsman','2',9012),(55,'2007','E&M Health, Safety & Environment','2',9012),(56,'2008','E&M Project Management','2',9012),(57,'2009','E&M Quality','2',9012),(58,'2010','Electrical / Electronic Engineering','2',9012),(59,'2011','Engineering / Manufacturing Design','2',9012),(60,'2012','Faciilities Management / Technical Manager','2',9012),(61,'2013','Lean Manufacturing / Processing','2',9012),(62,'2014','Mechanical Engineering','2',9012),(63,'2015','Operations','2',9012),(64,'2016','Plant Manager','2',9012),(65,'2017','Production / Manufacturing','2',9012),(66,'2018','Research & Development','2',9012),(150,'2019','Project Management','2',9012),(67,'3000','HR Assistant/Administrator','3',9015),(68,'3001','HR Benefits','3',9015),(69,'3002','HR Campus Recruiter','3',9015),(70,'3003','HR Compensation','3',9015),(71,'3004','HR Director','3',9015),(72,'3005','HR Generalist','3',9015),(73,'3006','HR Manager','3',9015),(74,'3007','HR Recruiter','3',9015),(75,'3008','HR Training & Development','3',9015),(76,'3009','Recruitment Consultant','3',9015),(77,'3010','Senior Vice President HR','3',9015),(78,'3011','Vice President HR','3',9015),(79,'4000','Business Analysis','4',9017),(80,'4001','Development','4',9017),(81,'4002','Helpdesk / Support','4',9008),(82,'4003','Infrastructure','4',9017),(83,'4004','IT Architect','4',9017),(84,'4005','IT Director / CTO / CIO','4',9017),(85,'4006','IT Manager','4',9017),(86,'4007','IT Training','4',9017),(87,'4008','Project/Programme Manager (PMO)','4',9017),(88,'5000','Associate - Law Firm','5',9018),(89,'5001','Contracts Manager - In-house','5',9018),(90,'5002','In-house Assistant General Counsel','5',9018),(91,'5003','In-house Attorney','5',9018),(92,'5004','Legal Secretary / Support Staff','5',9018),(93,'5005','Paralegal','5',9018),(94,'6000','Offshore Services','6',9012),(95,'6001','Oil & Gas Health, Safety & Environment Quality','6',9012),(96,'6002','Oil & Gas Project / Contract Management','6',9012),(97,'6003','Subsea Engineering','6',9012),(98,'7000','Contract & Vendor Management','7',9033),(99,'7001','Customer Service (Supply Chain)','7',9033),(100,'7002','Demand Planning','7',9033),(101,'7003','Direct Procurement','7',9033),(102,'7004','Import / Export','7',9033),(103,'7005','Indirect Procurement ','7',9033),(104,'7006','Information Flow / Logistics (Head Office)','7',9033),(105,'7007','Inventory Manager','7',9033),(106,'7008','Material Handler','7',9033),(107,'7009','Order Management / Master Data PO Number (Purchase Order)','7',9033),(108,'7010','Procurement / Spend Analysis','7',9033),(109,'7011','Procurement Coordinator','7',9033),(110,'7012','Procurement Director','7',9033),(111,'7013','Project Buyer','7',9033),(112,'7014','Project Consultancy','7',9033),(113,'7015','Real Flow / Logistics (In Factory)','7',9033),(114,'7016','Supply Chain Analysis','7',9033),(115,'7017','Supply Chain Director','7',9033),(116,'7018','Supply Chain Manager','7',9033),(117,'7019','Supply Planning','7',9033),(118,'7020','Transport / Distribution Manager','7',9033),(119,'7021','Warehouse Manager','7',9033),(120,'8000','Account Manager','8',9030),(121,'8001','Advertising','8',9021),(122,'8002','Agency','8',9021),(123,'8003','Analysis & Planning','8',9021),(124,'8004','Brand Marketing','8',9021),(125,'8005','Channel Sales','8',9030),(126,'8006','Client / Investor Relations','8',9021),(127,'8007','Creative','8',9021),(128,'8008','Direct Marketing','8',9021),(129,'8009','Direct Sales','8',9030),(130,'8010','Event / Roadshow','8',9030),(131,'8011','Field Sales','8',9030),(132,'8012','General Management','8',9030),(133,'8013','Inside Sales','8',9030),(134,'8014','Marketing Assistant','8',9021),(135,'8015','Marketing Communications','8',9021),(136,'8016','Marketing Management','8',9021),(137,'8017','Marketing Director / VP','8',9021),(138,'8018','New Business Development','8',9021),(139,'8019','Online Marketing','8',9021),(140,'8020','Online Sales','8',9030),(141,'8021','Product Marketing','8',9021),(142,'8022','Public Relations','8',9021),(143,'8023','Recent Graduate','8',9021),(144,'8024','Recruitment Consultant','8',9021),(145,'8025','Retail Sales','8',9030),(146,'8026','Sales B2B','8',9030),(147,'8027','Sales Manager /Director / VP','8',9030),(148,'8028','Trade Marketing / Category Marketing','8',9030),(149,'8029','Writing / Editor','8',9035),(151,'9000','Teaching','9',9011);

/*Table structure for table `job_messages` */

DROP TABLE IF EXISTS `job_messages`;

CREATE TABLE `job_messages` (
  `id_message` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_type` int(255) unsigned NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_message`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `job_messages` */

/*Table structure for table `job_questionnaires` */

DROP TABLE IF EXISTS `job_questionnaires`;

CREATE TABLE `job_questionnaires` (
  `id_job` int(255) unsigned NOT NULL,
  `id_question` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  PRIMARY KEY (`id_question`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `job_questionnaires` */

/*Table structure for table `job_subcategories` */

DROP TABLE IF EXISTS `job_subcategories`;

CREATE TABLE `job_subcategories` (
  `id_sub_category` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_category` int(255) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id_sub_category`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `job_subcategories` */

insert  into `job_subcategories`(`id_sub_category`,`id_category`,`title`) values (1,1,'Web Programming'),(2,1,'Web Design'),(3,1,'Mobile Apps'),(4,1,'Application Development'),(5,1,'SEO & SEM'),(6,1,'Blogs'),(7,1,'Database Development'),(8,1,'Flash Animation'),(9,1,'System Administration'),(10,1,'Technical Support'),(11,1,'Usability Design'),(12,1,'Project Management'),(13,1,'Testing & QA'),(14,1,'Networking & Security'),(15,2,'Graphic Design'),(16,2,'Logos'),(17,2,'Illustration'),(18,2,'Videography & Editing'),(19,2,'Animation'),(20,2,'Brochures'),(21,2,'Banner Ads'),(22,2,'Page & Book Design'),(23,2,'Digital Image Editing'),(24,2,'Presentation Design'),(25,2,'Voice Talent'),(26,2,'Corporate Identity Kit'),(27,2,'Emails & Newsletters'),(28,2,'Cartoons & Comics'),(29,2,'Label & Package Design'),(30,2,'Print Ad Design'),(31,2,'Photography'),(32,3,'Article Writing'),(33,3,'Web Content'),(34,3,'Transalation'),(35,3,'E-books and Blogs'),(36,3,'Copywriting'),(37,3,'Academic Writing'),(38,3,'Editing & Proofreading'),(39,3,'Ghost Writing'),(40,3,'Sales Writing'),(41,3,'Technical Writing'),(42,3,'Press Releases'),(43,3,'Resumes & Cover Letters'),(44,3,'Newsletters'),(45,3,'Report Writing'),(46,3,'Speeches'),(47,3,'User Guides & Manuals'),(48,4,'Search & Online Marketing'),(49,4,'Lead Generation'),(50,4,'Advertising'),(51,4,'Telemarketing'),(52,4,'Marketing Plans'),(53,4,'Grassroots Marketing'),(54,4,'Email & Direct Marketing'),(55,4,'Marketing & Sales Consulting'),(56,4,'Market Research & Surveys'),(57,4,'Advertising Campaigns'),(58,4,'Competitive Analysis'),(59,4,'Viral Marketing'),(60,4,'Sales Presentations'),(61,4,'Business Plans'),(62,5,'Data Entry'),(63,5,'Virtual Assistant'),(64,5,'Research'),(65,5,'Transcription'),(66,5,'Mailing List Development'),(67,5,'Customer Service'),(68,5,'Word Processing'),(69,5,'Presentation Formatting'),(70,5,'Event Planning'),(71,5,'Fact Checking'),(72,5,'Office Management'),(73,5,'Travel Planning'),(74,6,'Industrial Design'),(75,6,'CAD'),(76,6,'Electrical'),(77,6,'Architecture'),(78,6,'Mechanical'),(79,6,'Contract Manufacturing'),(80,6,'Civil & Structural'),(81,6,'Interior Design'),(82,7,'Accounting & Bookkeeping'),(83,7,'Financial Planning'),(84,7,'Management Consulting'),(85,7,'Financial Reporting'),(86,7,'Tax'),(87,7,'HR Policies & Plans'),(88,7,'Billing & Collections'),(89,7,'Outsourcing Consulting'),(90,8,'Patent, Copyright and Trademarks'),(91,8,'Contracts'),(92,8,'Business and Corporate'),(93,8,'Real Estate'),(94,8,'Litigation'),(95,8,'Incorporation'),(96,8,'Bankruptcy'),(97,8,'Criminal'),(98,8,'Tax'),(99,8,'Wills, Trusts and Estates'),(100,8,'Immigration');

/*Table structure for table `job_subcategories_reference` */

DROP TABLE IF EXISTS `job_subcategories_reference`;

CREATE TABLE `job_subcategories_reference` (
  `id_sub_category` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_category` int(255) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `new_id` int(25) NOT NULL,
  PRIMARY KEY (`id_sub_category`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `job_subcategories_reference` */

insert  into `job_subcategories_reference`(`id_sub_category`,`id_category`,`title`,`new_id`) values (1,1,'Web Programming',173),(2,1,'Web Design',173),(3,1,'Mobile Apps',173),(4,1,'Application Development',173),(5,1,'SEO & SEM',173),(6,1,'Blogs',173),(7,1,'Database Development',173),(8,1,'Flash Animation',210),(9,1,'System Administration',173),(10,1,'Technical Support',250),(11,1,'Usability Design',173),(12,1,'Project Management',173),(13,1,'Testing & QA',173),(14,1,'Networking & Security',180),(15,2,'Graphic Design',115),(16,2,'Logos',115),(17,2,'Illustration',115),(18,2,'Videography & Editing',107),(19,2,'Animation',107),(20,2,'Brochures',115),(21,2,'Banner Ads',115),(22,2,'Page & Book Design',115),(23,2,'Digital Image Editing',115),(24,2,'Presentation Design',111),(25,2,'Voice Talent',110),(26,2,'Corporate Identity Kit',111),(27,2,'Emails & Newsletters',115),(28,2,'Cartoons & Comics',210),(29,2,'Label & Package Design',115),(30,2,'Print Ad Design',111),(31,2,'Photography',114),(32,3,'Article Writing',207),(33,3,'Web Content',208),(34,3,'Transalation',243),(35,3,'E-books and Blogs',207),(36,3,'Copywriting',207),(37,3,'Academic Writing',207),(38,3,'Editing & Proofreading',207),(39,3,'Ghost Writing',207),(40,3,'Sales Writing',207),(41,3,'Technical Writing',207),(42,3,'Press Releases',207),(43,3,'Resumes & Cover Letters',207),(44,3,'Newsletters',207),(45,3,'Report Writing',207),(46,3,'Speeches',207),(47,3,'User Guides & Manuals',207),(48,4,'Search & Online Marketing',135),(49,4,'Lead Generation',135),(50,4,'Advertising',135),(51,4,'Telemarketing',135),(52,4,'Marketing Plans',135),(53,4,'Grassroots Marketing',135),(54,4,'Email & Direct Marketing',135),(55,4,'Marketing & Sales Consulting',135),(56,4,'Market Research & Surveys',136),(57,4,'Advertising Campaigns',135),(58,4,'Competitive Analysis',136),(59,4,'Viral Marketing',135),(60,4,'Sales Presentations',135),(61,4,'Business Plans',135),(62,5,'Data Entry',251),(63,5,'Virtual Assistant',251),(64,5,'Research',251),(65,5,'Transcription',251),(66,5,'Mailing List Development',251),(67,5,'Customer Service',250),(68,5,'Word Processing',251),(69,5,'Presentation Formatting',251),(70,5,'Event Planning',251),(71,5,'Fact Checking',251),(72,5,'Office Management',251),(73,5,'Travel Planning',251),(74,6,'Industrial Design',198),(75,6,'CAD',198),(76,6,'Electrical',196),(77,6,'Architecture',118),(78,6,'Mechanical',198),(79,6,'Contract Manufacturing',198),(80,6,'Civil & Structural',119),(81,6,'Interior Design',111),(82,7,'Accounting & Bookkeeping',156),(83,7,'Financial Planning',152),(84,7,'Management Consulting',134),(85,7,'Financial Reporting',156),(86,7,'Tax',156),(87,7,'HR Policies & Plans',143),(88,7,'Billing & Collections',156),(89,7,'Outsourcing Consulting',134),(90,8,'Patent, Copyright and Trademarks',182),(91,8,'Contracts',182),(92,8,'Business and Corporate',182),(93,8,'Real Estate',182),(94,8,'Litigation',182),(95,8,'Incorporation',182),(96,8,'Bankruptcy',182),(97,8,'Criminal',182),(98,8,'Tax',182),(99,8,'Wills, Trusts and Estates',182),(100,8,'Immigration',182),(101,6,'Quantity Survey',118),(102,9,'Teaching',102);

/*Table structure for table `job_types` */

DROP TABLE IF EXISTS `job_types`;

CREATE TABLE `job_types` (
  `id_type` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

/*Data for the table `job_types` */

insert  into `job_types`(`id_type`,`name`) values (1,'Full-Time'),(2,'Part-Time'),(3,'Internship (Full-Time)'),(4,'Internship (Part-Time)'),(5,'Contract'),(6,'Temp');

/*Table structure for table `jobs` */

DROP TABLE IF EXISTS `jobs`;

CREATE TABLE `jobs` (
  `id_job` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(255) unsigned NOT NULL,
  `id_creator` int(255) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `id_qualification` int(255) unsigned NOT NULL,
  `desired_qualities` text NOT NULL,
  `id_country` int(255) unsigned NOT NULL,
  `id_job_level` int(255) unsigned NOT NULL,
  `id_years_exp` int(255) unsigned NOT NULL,
  `id_duration` int(255) unsigned NOT NULL,
  `status` int(1) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `id_reference` varchar(10) NOT NULL,
  `id_title` varchar(255) NOT NULL,
  PRIMARY KEY (`id_job`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `jobs` */

/*Table structure for table `loan_paid` */

DROP TABLE IF EXISTS `loan_paid`;

CREATE TABLE `loan_paid` (
  `id_paid_loan` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_loan` int(255) unsigned NOT NULL,
  `id_user` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `amount` int(255) unsigned NOT NULL,
  `paid` int(255) unsigned NOT NULL,
  `owed` int(10) unsigned NOT NULL,
  `date_approved` datetime NOT NULL,
  `next_payment` datetime DEFAULT NULL,
  `loan_status` int(255) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_paid_loan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `loan_paid` */

/*Table structure for table `loan_payment_schedules` */

DROP TABLE IF EXISTS `loan_payment_schedules`;

CREATE TABLE `loan_payment_schedules` (
  `id_pay` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_paid_loan` int(255) unsigned NOT NULL,
  `month_pay` decimal(60,2) NOT NULL,
  `amount_paid` decimal(60,2) NOT NULL,
  `pay_status` int(255) NOT NULL,
  `balance` decimal(60,2) NOT NULL,
  `date_paid` datetime DEFAULT NULL,
  `date_due` datetime DEFAULT NULL,
  `id_period` int(25) DEFAULT NULL,
  PRIMARY KEY (`id_pay`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `loan_payment_schedules` */

/*Table structure for table `loan_stages` */

DROP TABLE IF EXISTS `loan_stages`;

CREATE TABLE `loan_stages` (
  `id_loan_stage` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(255) unsigned NOT NULL,
  `id_loan` int(255) unsigned NOT NULL,
  `id_loan_string` varchar(255) NOT NULL,
  `id_stage` int(255) unsigned NOT NULL,
  `id_user` int(255) unsigned NOT NULL,
  `status` int(255) unsigned NOT NULL,
  `description` text NOT NULL,
  `id_stage_manager` int(255) unsigned NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id_loan_stage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `loan_stages` */

/*Table structure for table `loans` */

DROP TABLE IF EXISTS `loans`;

CREATE TABLE `loans` (
  `id_loan` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_loan_string` varchar(255) NOT NULL,
  `id_user` int(255) unsigned NOT NULL,
  `description` text NOT NULL,
  `amount` int(255) unsigned NOT NULL,
  `id_payment_plan` int(255) unsigned NOT NULL,
  `pay_back_period` int(255) unsigned NOT NULL,
  `start_period` datetime NOT NULL,
  `id_start_period` int(25) NOT NULL,
  `current_stage` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `status` int(255) unsigned NOT NULL,
  `date_next_payment` datetime DEFAULT NULL,
  `date_requested` datetime DEFAULT NULL,
  PRIMARY KEY (`id_loan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `loans` */

/*Table structure for table `loans_approval_stage` */

DROP TABLE IF EXISTS `loans_approval_stage`;

CREATE TABLE `loans_approval_stage` (
  `id_settings` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `stage` int(255) unsigned NOT NULL,
  `id_user` int(255) unsigned NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id_settings`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `loans_approval_stage` */

/*Table structure for table `module_group` */

DROP TABLE IF EXISTS `module_group`;

CREATE TABLE `module_group` (
  `id_group` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `id_sort` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_group`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `module_group` */

insert  into `module_group`(`id_group`,`subject`,`id_sort`) values (1,'Manage Staff',2),(2,'Company Hub',3),(3,'Personal',1),(4,'Recruiting',4),(5,'Account Settings',5);

/*Table structure for table `module_perms` */

DROP TABLE IF EXISTS `module_perms`;

CREATE TABLE `module_perms` (
  `id_perm` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_module` int(255) unsigned NOT NULL,
  `subject` varchar(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `id_owner` int(255) unsigned NOT NULL,
  `in_menu` int(1) unsigned NOT NULL,
  `status` int(255) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_perm`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `module_perms` */

insert  into `module_perms`(`id_perm`,`id_module`,`subject`,`id_string`,`id_owner`,`in_menu`,`status`) values (1,1,'Staff Directory','view_staff_directory',1,1,3),(2,1,'Add Departments','add_departments',1,0,1),(3,1,'Edit Departments','edit_departments',1,0,1),(4,1,'Delete Departments','delete_departments',1,0,1),(5,2,'My Vacation','my_vacation',5,1,3),(6,2,'Vacation Settings','vacation_types',5,1,1),(7,2,'Public Holidays','holidays',5,1,0),(8,3,'My Appraisal','my_appraisal',3,1,3),(9,3,'Submit My Appraisal','submit_my_appraisal',3,0,3),(10,3,'Appraisal Records','appraisal_records',3,1,1),(11,1,'Add New Employee','add_new_employee',1,1,1),(12,1,'Add Company Location','add_company_location',1,0,1),(13,1,'Edit Company Location','edit_company_location',1,0,1),(14,1,'Delete Company Location','delete_company_location',1,0,1),(15,1,'Departments','view_departments',1,1,1),(19,1,'Employee Roles','view_roles',1,1,1),(20,1,'Company Locations','view_company_location',1,1,1),(21,1,'Add Roles','add_roles',1,0,1),(22,1,'Edit Roles','Edit_roles',1,0,1),(23,1,'Delete Roles','delete_roles',1,0,1),(25,1,'Reset Employee Password','reset_employee_password',1,0,1),(26,7,'Shared Documents','view_downloads',2,1,3),(27,7,'Add Downloads','add_downloads',2,0,1),(28,7,'Edit Downloads','edit_downloads',2,0,1),(29,7,'Delete Downloads','delete_downloads',2,0,1),(30,9,'Upcoming Events','view_events',4,1,3),(31,9,'Create New Events','add_events',4,1,1),(32,9,'Edit Events','edit_events',4,0,1),(33,9,'Delete Events','delete_events',4,0,1),(34,6,'Admin Profile Settings','admin_profile_settings',7,0,1),(35,6,'Company Profile Settings','company_profile_settings',7,0,1),(36,5,'My Tasks','my_tasks',3,1,3),(46,6,'View Landing Page Settings','view_landing_page_settings',7,0,1),(47,6,'Landing Page Settings','landing_page_settings',7,1,1),(48,4,'Jobs','view_live_jobs',10,1,1),(49,4,'Post New Job','post_job',10,1,1),(50,4,'Edit Live Job Status','edit_live_job_status',10,0,1),(51,4,'Edit Job','edit_job',10,0,1),(52,4,'Watchlist','watchlist',10,0,1),(53,4,'Search Database','search_database',10,0,1),(54,2,'Employee Vacation','allocate_vacation',5,0,1),(55,3,'Appraisal Settings','appraisal_settings',3,1,1),(56,5,'Change Task Status','change_task_status',6,0,3),(57,10,'Run Payroll','payroll',11,1,1),(58,10,'Payroll Settings','settings',11,1,1),(59,10,'Employee Compensation','paid_employee',11,1,1),(61,10,'My Payslip','my_payslip',11,1,1),(62,10,'Approve Payroll Process','approve_payroll_process',11,0,1),(63,10,'Edit Payroll Currency','edit_payroll_currency',11,0,1),(64,10,'Payroll Records','records',11,1,1),(65,10,'List Payroll History','history',11,0,1),(66,10,'Search Payroll History','search_payroll_history',11,0,1),(67,10,'View Payroll History Details','view_payroll_history_details',11,0,1),(68,10,'Run Payroll','start_payroll',11,0,1),(69,10,'Paid Employee','paid_employee',11,0,1),(70,10,'Select Employee To Process Payroll','select_employee_to_process_payroll',11,0,1),(71,10,'Edit Payslip','edit_payslip',11,0,1),(72,10,'Cancel Payroll Process','cancel_payroll_process',11,0,1),(73,10,'Initiate Payment','initiate_payment',11,0,1),(74,10,'Run Payroll','payroll',11,0,1),(75,10,'Add To Payroll','unpaid_employee',11,0,1),(76,10,'View Employee Payslip','view_employee_payslip',11,0,1),(77,10,'Edit Employee Payslip','edit_employee_payslip',11,0,1),(78,9,'Events Settings','view_events_settings',4,1,1),(79,3,'Appraisal Forms','appraisal_form',3,1,1),(81,2,'Vacation Records','view_summary',5,0,1),(91,11,'View Access Control','view_access_control',12,1,1),(92,12,'Issue New Query','issue_new_query',5,1,1),(93,12,'My Queries','my_queries',5,1,0),(94,12,'Query Records','query_records',5,1,1),(95,12,'Query','query',5,0,10),(96,3,'Appraisal Sections','view_appraisal_section',3,0,1),(97,3,'Add Appraisal Section','add_appraisal_section',3,0,1),(98,3,'Edit Appraisal Section','edit_appraisal_section',3,0,1),(99,3,'Delete Appraisal Section','delete_appraisal_section',3,0,1),(100,13,'Clients','clients',3,1,1),(101,13,'Add New Client','add_new_client',3,1,1),(102,13,'Account History','account_history',3,1,1),(123,13,'Edit HR Client Access','edit_hr_client_access',3,0,1),(131,1,'Organogram','organogram',1,1,1),(132,14,'Device Locations','device_location',1,1,0),(133,14,'Current Employees','current_employees',1,1,0),(134,14,'Add Employee to Attendance','add_employee_to_attendance',1,0,1),(135,14,'Today\'s Attendance','today',1,1,0),(136,14,'Attendance Records','records',1,1,0),(137,14,'Attendance Settings','settings',1,1,0),(138,1,'View Employee Records','view_records',1,0,1),(139,1,'Change status','change_status',1,0,1),(140,15,'Employee Compensation','employees',1,1,10),(141,15,'Add Employee','add_compensation',1,0,10),(142,15,'Pay Structure View','view_pay_structure',1,0,10),(143,15,'Add Pay Structure','add_pay_structure',1,0,10),(144,15,'Edit Pay Structure','edit_pay_structure',1,0,10),(145,15,'Delete Pay Structure','delete_pay_structure',1,0,10),(146,15,'Add Compensation','add_compensation',1,0,10),(147,15,'Edit Compensation','edit_compensation',1,0,10),(148,15,'Delete Compensation','delete_compensation',1,0,10),(149,15,'View Compensation','view_compensation',1,0,10),(150,15,'Pay Grade','pay_structure',1,1,10),(151,15,'Compensation Settings','settings',1,1,1),(152,16,'My Loan','my_loan',1,1,0),(153,16,'Pending Approval','pending_approval',1,1,1),(154,16,'Loan Settiings','loan_settings',1,1,1),(161,14,'View Employee Attendance Records','employee_attendance_records',1,0,1),(162,14,'Edit Employees Shift','edit_employees_shift',1,0,1),(164,14,'Remove Employee','remove_employee',1,0,1),(165,14,'Shift Overview','shift',1,1,0),(166,4,'Access Applications','access_applications',10,0,1),(167,14,'My Shift','my_shift',1,1,0),(168,1,'Manager View','manager_view',1,1,0),(171,4,'Settings','settings',10,1,1),(173,1,'Edit/Request Employee Records','edit_request_employee_records',1,0,1),(174,17,'My Goals','my_goals',0,1,1),(175,17,'Employee Goals','overview',0,1,1),(176,18,'My Requests','my_request',1,1,0),(177,18,'Pending Requests','pending_requests',1,1,0),(178,18,'Request Form','settings',1,1,0),(179,5,'My Profile','my_profile',3,1,3),(180,5,'View My Profile Picture','view_my_profile_picture',3,0,3),(181,5,'View My Profile Work info','view_my_profile_work_info',3,0,3),(182,5,'View My Emergency Contacts','view_my_emergency_contacts',3,0,3),(183,5,'View My Profile Personal info','view_my_profile_personal_info',3,0,3),(184,5,'View My Emergency Contacts','view_my_emergency_contacts',3,0,3),(185,5,'View My Bank Details','view_my_bank_details',3,0,0),(186,5,'View My Benefits Details','view_my_benefits_details',3,0,0),(187,10,'Payroll Incidentals','incidentals',11,1,1),(188,5,'Edit Personal Profile','edit_personal_profile',3,0,2),(189,5,'Edit My Profile Picture','edit_my_profile_picture',3,0,2),(190,5,'Edit My Profile Personal Info','edit_my_profile_personal_info',3,0,2),(191,5,'Edit My Emergency Contacts','edit_my_emergency_contacts',3,0,2),(192,5,'Edit My Banks Details','edit_my_bank_details',3,0,2),(193,5,'Edit My Benefits Details','edit_benefits_details',3,0,2),(194,5,'Edit My Credentials','edit_my_credentials',3,0,2),(195,19,'Current Offers','current_offers',1,1,1),(196,19,'My Account','my_account',1,1,1),(197,20,'Manage','manage',12,1,1),(198,20,'Create New','create',12,1,1),(199,20,'View Companies dashboard','dashboard',12,0,1),(200,17,'Manager View','manager_view',1,1,0),(201,21,'SMS','sms',12,1,0),(202,11,'Consultant Access','consultant_access_control',12,1,1),(203,17,'Add Goal','add_goal',2,0,1),(204,17,'Edit Goal','edit_goal',2,0,1),(205,17,'Delete Goal','delete_goal',2,0,1),(206,17,'Update Goal','update_goal',2,0,1),(207,2,'Vacation Planner','vacation_planner',1,0,1),(208,15,'Analytics','analytics',1,1,1),(209,3,'Appraisal Analytics','appraisal_analytics',3,1,1),(210,1,'Analytics','employee_analytics',1,1,1),(211,2,'Vacation Allowance','vacation_allowance',5,0,1),(212,2,'Analytics','analytics',5,1,1),(213,2,'Employee Entitlement','employee_entitlement',5,1,1),(214,2,'Pending Request','pending_request',5,1,1),(215,22,'Employee Rewards','rewards',1,1,1),(216,22,'My Rewards','my_rewards',1,1,1),(217,22,'Manager View','manager_view',1,1,1),(218,22,'Manage Allocation','allocations',1,1,1),(219,22,'Bulk Upload','bulk_upload',1,1,1),(220,22,'Settings','reward_settings',1,1,1),(221,22,'Analytics','reward_analytics',1,1,1),(222,18,'Employee Records','employee_records',1,1,1),(223,18,'Analytics','analytics',1,1,1),(224,2,'Manager View','manager_view',1,1,1),(225,17,'Analytics','analytics',1,1,0),(226,17,'Settings','settings',1,1,0),(227,10,'Payroll Disbursement','payroll_disursement',11,0,1),(228,10,'Approve Final Disbursement','approve_final_disbursement',11,0,1),(229,17,'Company Goals','company_goals',1,1,0),(230,21,'Transaction History','transaction_history',12,1,0),(231,1,'Edit My Work Number','edit_my_work_number',1,0,1),(232,1,'Edit My Birthday','edit_my_birthday',1,0,1),(233,1,'Edit My Home Address','edit_my_home_address',1,0,1),(234,1,'Edit My Personal Number','edit_my_personal_number',1,0,1),(235,1,'Edit My Emergency Contact 1','edit_my_emergency_contact_1',1,0,1),(236,1,'Edit My Emergency Contact 2','edit_my_emergency_contact_2',1,0,1),(237,1,'Edit My Bank Account','edit_my_bank_account',1,0,1),(238,1,'Edit My Pension Details','edit_my_pension_details',1,0,1),(239,23,'My Training','training',1,1,1),(240,23,'Manage Vendors','vendors',1,1,1),(241,23,'Browse Courses','courses',1,1,1),(242,23,'Manage Courses','manage_courses',1,1,1),(243,23,'Employee Trainings','employee_training',1,1,1),(244,23,'Manager View','manager_view',1,1,1),(245,23,'Settings','settings',1,1,1),(246,1,'Settings','settings',1,1,1),(247,1,'Pending Approvals','pending_approval',1,1,1),(248,17,'Pending Approvals','employee_pending_approvals',1,1,1),(249,24,'Configure','configure',1,1,1),(250,23,'Budget','budget',1,1,1);

/*Table structure for table `module_setup` */

DROP TABLE IF EXISTS `module_setup`;

CREATE TABLE `module_setup` (
  `id_module_setup` int(25) NOT NULL AUTO_INCREMENT,
  `id_module` int(25) NOT NULL,
  `id_company` int(25) NOT NULL,
  `status` int(25) NOT NULL,
  `created_by` int(25) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `step` int(25) DEFAULT NULL,
  PRIMARY KEY (`id_module_setup`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `module_setup` */

insert  into `module_setup`(`id_module_setup`,`id_module`,`id_company`,`status`,`created_by`,`date_created`,`step`) values (391,15,1,2,1,'2014-09-23 20:50:55',1),(392,15,1,2,1,'2014-09-23 00:00:00',2),(393,14,1,2,1,'2014-10-18 14:36:35',1),(394,14,1,1,1,'2014-10-18 01:36:09',1),(444,16,1,1,1,'2014-10-27 11:23:54',1),(445,16,1,1,1,'2014-10-27 11:24:14',1),(446,16,1,1,1,'2014-10-27 11:24:26',1),(447,16,1,1,1,'2014-10-27 11:24:35',1),(448,16,1,1,1,'2014-10-27 11:24:44',1),(449,16,1,1,1,'2014-10-27 11:24:50',2),(450,16,1,1,1,'2014-10-27 11:24:50',1),(451,16,1,2,1,'2014-10-27 11:24:56',2),(452,10,1,1,1,'2014-11-21 09:21:46',1),(453,10,1,1,1,'2014-11-21 09:22:10',2),(454,10,1,1,1,'2014-11-21 09:22:22',3),(455,10,1,1,1,'2014-11-21 09:22:36',3),(456,10,1,2,1,'2014-11-21 09:22:46',3);

/*Table structure for table `modules` */

DROP TABLE IF EXISTS `modules`;

CREATE TABLE `modules` (
  `id_module` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `id_group` int(255) unsigned NOT NULL,
  `status` int(255) DEFAULT '1',
  `requires_login` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_module`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `modules` */

insert  into `modules`(`id_module`,`subject`,`id_string`,`id_group`,`status`,`requires_login`) values (1,'Staff Records','employee',1,1,1),(2,'Vacation','vacation',1,1,1),(3,'Appraisal','appraisal',1,1,1),(4,'Recruiting','recruiting',4,1,1),(5,'Personal','personal',3,1,1),(6,'Account Settings','account_settings',5,1,1),(7,'Documents','downloads',2,1,1),(9,'Events','events',2,1,1),(10,'Payroll','payroll',1,1,1),(11,'Access Control','access_control',5,1,1),(12,'Employee Queries','employee_queries',1,1,1),(13,'HR Clients','hr_clients',1,1,1),(14,'Attendance','attendance',1,1,1),(15,'Compensation','compensation',1,1,1),(16,'Loans','loans',1,1,1),(23,'Training','training',1,1,1),(24,'Authentication','auth',5,1,0);

/*Table structure for table `payment_options` */

DROP TABLE IF EXISTS `payment_options`;

CREATE TABLE `payment_options` (
  `id_payment_options` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `payment_options` varchar(255) NOT NULL,
  PRIMARY KEY (`id_payment_options`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `payment_options` */

/*Table structure for table `payroll_allowance` */

DROP TABLE IF EXISTS `payroll_allowance`;

CREATE TABLE `payroll_allowance` (
  `id_payroll_allowance` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(255) unsigned DEFAULT NULL,
  `id_company` int(255) unsigned DEFAULT NULL,
  `id_payroll_currency_settings` int(255) unsigned DEFAULT NULL,
  `id_payroll_period` int(255) unsigned DEFAULT NULL,
  `grosspay` varchar(255) DEFAULT NULL,
  `components` longtext,
  PRIMARY KEY (`id_payroll_allowance`),
  KEY `FK_payroll_compensation_id_user` (`id_user`),
  KEY `FK_payroll_compensation_id_company` (`id_company`),
  KEY `FK_payroll_compensation_id_currency` (`id_payroll_currency_settings`),
  KEY `FK_payroll_compensation_id_payroll_period` (`id_payroll_period`),
  CONSTRAINT `payroll_allowance_ibfk_1` FOREIGN KEY (`id_company`) REFERENCES `companies` (`id_company`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `payroll_allowance_ibfk_2` FOREIGN KEY (`id_payroll_currency_settings`) REFERENCES `payroll_currency_settings` (`id_payroll_currency_settings`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `payroll_allowance_ibfk_3` FOREIGN KEY (`id_payroll_period`) REFERENCES `payroll_period` (`id_payroll_period`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `payroll_allowance_ibfk_4` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_allowance` */

/*Table structure for table `payroll_allowance_default` */

DROP TABLE IF EXISTS `payroll_allowance_default`;

CREATE TABLE `payroll_allowance_default` (
  `id_allowance_default` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `allowance` varchar(255) DEFAULT NULL,
  `id_company` int(255) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_allowance_default`),
  KEY `FK_payroll_allowance_default_1` (`id_company`),
  CONSTRAINT `FK_payroll_allowance_default_1` FOREIGN KEY (`id_company`) REFERENCES `companies` (`id_company`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_allowance_default` */

/*Table structure for table `payroll_allowance_settings` */

DROP TABLE IF EXISTS `payroll_allowance_settings`;

CREATE TABLE `payroll_allowance_settings` (
  `id_payroll_allowance_settings` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `component_name` varchar(255) DEFAULT NULL,
  `component_percent` varchar(255) DEFAULT NULL,
  `component_taxable` int(255) unsigned DEFAULT NULL,
  `id_company` int(255) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_payroll_allowance_settings`),
  KEY `FK_payroll_allowance_settings_1` (`id_company`),
  CONSTRAINT `FK_payroll_allowance_settings_1` FOREIGN KEY (`id_company`) REFERENCES `companies` (`id_company`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_allowance_settings` */

/*Table structure for table `payroll_approved_stages` */

DROP TABLE IF EXISTS `payroll_approved_stages`;

CREATE TABLE `payroll_approved_stages` (
  `id_stages_approver` int(11) NOT NULL AUTO_INCREMENT,
  `approver_id` int(11) NOT NULL,
  `id_company` int(11) NOT NULL,
  `id_run` varchar(255) NOT NULL,
  `stage` int(11) NOT NULL,
  `date_approved` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_stages_approver`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_approved_stages` */

/*Table structure for table `payroll_approvers` */

DROP TABLE IF EXISTS `payroll_approvers`;

CREATE TABLE `payroll_approvers` (
  `id_payroll_approver` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(255) unsigned DEFAULT NULL,
  `id_company` int(255) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_payroll_approver`),
  KEY `FK_payroll_approvers_1` (`id_company`),
  CONSTRAINT `FK_payroll_approvers_1` FOREIGN KEY (`id_company`) REFERENCES `companies` (`id_company`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_approvers` */

/*Table structure for table `payroll_benchmark` */

DROP TABLE IF EXISTS `payroll_benchmark`;

CREATE TABLE `payroll_benchmark` (
  `id_benchmark` int(25) NOT NULL AUTO_INCREMENT,
  `page` varchar(255) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `id_company` int(25) DEFAULT NULL,
  `no_of_employee` int(25) DEFAULT NULL,
  `id_run` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_benchmark`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_benchmark` */

/*Table structure for table `payroll_company_banks` */

DROP TABLE IF EXISTS `payroll_company_banks`;

CREATE TABLE `payroll_company_banks` (
  `id_payroll_company_bank` int(255) NOT NULL AUTO_INCREMENT,
  `id_company` int(255) NOT NULL,
  `id_bank` varchar(255) NOT NULL,
  `account_name` varchar(255) NOT NULL,
  `account_number` varchar(255) NOT NULL,
  `primary` int(1) NOT NULL DEFAULT '0',
  `branch` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_payroll_company_bank`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_company_banks` */

/*Table structure for table `payroll_company_deductions` */

DROP TABLE IF EXISTS `payroll_company_deductions`;

CREATE TABLE `payroll_company_deductions` (
  `id_payroll_company_deductions` int(255) NOT NULL AUTO_INCREMENT,
  `id_company` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `rate` varchar(255) NOT NULL,
  `default` int(1) NOT NULL DEFAULT '0',
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_payroll_company_deductions`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_company_deductions` */

/*Table structure for table `payroll_compensation` */

DROP TABLE IF EXISTS `payroll_compensation`;

CREATE TABLE `payroll_compensation` (
  `id_payroll_compensation` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_payroll_allowance` int(255) unsigned DEFAULT NULL,
  `id_payroll_deduction` int(255) unsigned DEFAULT NULL,
  `id_payroll_contribution` int(255) unsigned DEFAULT NULL,
  `id_payroll_otherpay` int(255) unsigned DEFAULT NULL,
  `id_company` int(255) unsigned DEFAULT NULL,
  `id_user` int(255) unsigned DEFAULT NULL,
  `id_schedule` int(255) unsigned DEFAULT NULL,
  `id_payroll_paygrade` int(255) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_payroll_compensation`),
  UNIQUE KEY `Index_uniq_payroll_compensation` (`id_user`,`id_company`),
  KEY `FK_payroll_compensation_7` (`id_payroll_paygrade`),
  CONSTRAINT `FK_payroll_compensation_7` FOREIGN KEY (`id_payroll_paygrade`) REFERENCES `payroll_paygrade` (`id_payroll_paygrade`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_compensation` */

/*Table structure for table `payroll_compensation_employee_allowance` */

DROP TABLE IF EXISTS `payroll_compensation_employee_allowance`;

CREATE TABLE `payroll_compensation_employee_allowance` (
  `id_employee_allowance` int(255) NOT NULL AUTO_INCREMENT,
  `id_compensation_employee` int(255) NOT NULL,
  `id_allowance` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_pay_structure` int(255) NOT NULL,
  `allowance` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `type` int(1) NOT NULL,
  `id_schedule` varchar(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  PRIMARY KEY (`id_employee_allowance`),
  UNIQUE KEY `ind_pay_allow_unq` (`id_company`,`id_user`,`allowance`,`id_schedule`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_compensation_employee_allowance` */

/*Table structure for table `payroll_compensation_employee_allowance_archive` */

DROP TABLE IF EXISTS `payroll_compensation_employee_allowance_archive`;

CREATE TABLE `payroll_compensation_employee_allowance_archive` (
  `id_employee_allowance` int(255) NOT NULL AUTO_INCREMENT,
  `id_compensation_employee` int(255) NOT NULL,
  `id_allowance` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_pay_structure` int(255) NOT NULL,
  `allowance` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `type` int(1) NOT NULL,
  `id_schedule` varchar(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id_employee_allowance`),
  KEY `ind_pay_allow_arc` (`id_company`,`id_user`,`allowance`,`id_schedule`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_compensation_employee_allowance_archive` */

/*Table structure for table `payroll_compensation_employee_contribution` */

DROP TABLE IF EXISTS `payroll_compensation_employee_contribution`;

CREATE TABLE `payroll_compensation_employee_contribution` (
  `id_employee_contribution` int(255) NOT NULL AUTO_INCREMENT,
  `id_compensation_employee` int(255) NOT NULL,
  `id_contribution_default` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_pay_structure` int(255) NOT NULL,
  `id_contribution` int(255) NOT NULL,
  `contribution` varchar(255) NOT NULL,
  `percentage` varchar(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `id_schedule` varchar(255) NOT NULL,
  PRIMARY KEY (`id_employee_contribution`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_compensation_employee_contribution` */

/*Table structure for table `payroll_compensation_employee_contribution_archive` */

DROP TABLE IF EXISTS `payroll_compensation_employee_contribution_archive`;

CREATE TABLE `payroll_compensation_employee_contribution_archive` (
  `id_employee_contribution` int(255) DEFAULT NULL,
  `id_compensation_employee` int(255) NOT NULL,
  `id_contribution_default` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_pay_structure` int(255) NOT NULL,
  `id_contribution` int(255) NOT NULL,
  `contribution` varchar(255) NOT NULL,
  `percentage` varchar(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `id_schedule` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `payroll_compensation_employee_contribution_archive` */

/*Table structure for table `payroll_compensation_employee_deduction` */

DROP TABLE IF EXISTS `payroll_compensation_employee_deduction`;

CREATE TABLE `payroll_compensation_employee_deduction` (
  `id_employee_deduction` int(255) NOT NULL AUTO_INCREMENT,
  `id_compensation_employee` int(255) NOT NULL,
  `id_contribution_default` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_pay_structure` int(255) NOT NULL,
  `id_deduction` int(255) NOT NULL,
  `deduction` varchar(255) NOT NULL,
  `percentage` varchar(255) NOT NULL,
  `type` int(1) NOT NULL,
  `id_schedule` varchar(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  PRIMARY KEY (`id_employee_deduction`),
  UNIQUE KEY `ind_pay_deduc_unq` (`id_company`,`id_user`,`deduction`,`id_schedule`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_compensation_employee_deduction` */

/*Table structure for table `payroll_compensation_employee_deduction_archive` */

DROP TABLE IF EXISTS `payroll_compensation_employee_deduction_archive`;

CREATE TABLE `payroll_compensation_employee_deduction_archive` (
  `id_employee_deduction` int(255) NOT NULL AUTO_INCREMENT,
  `id_compensation_employee` int(255) NOT NULL,
  `id_contribution_default` int(255) DEFAULT NULL,
  `id_company` int(255) NOT NULL,
  `id_pay_structure` int(255) NOT NULL,
  `id_deduction` int(255) NOT NULL,
  `deduction` varchar(255) NOT NULL,
  `percentage` varchar(255) NOT NULL,
  `type` int(1) NOT NULL,
  `id_schedule` varchar(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  PRIMARY KEY (`id_employee_deduction`),
  KEY `ind_pay_deduc_arc` (`id_company`,`id_user`,`deduction`,`id_schedule`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_compensation_employee_deduction_archive` */

/*Table structure for table `payroll_compensation_employees` */

DROP TABLE IF EXISTS `payroll_compensation_employees`;

CREATE TABLE `payroll_compensation_employees` (
  `id_compensation_employee` int(255) NOT NULL,
  `id_pay_structure` int(255) NOT NULL,
  `id_pay_grade` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_role` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `id_currency` int(255) NOT NULL,
  `tax` int(1) NOT NULL,
  `archive` int(1) NOT NULL,
  `id_schedule` int(255) unsigned NOT NULL,
  `pay_scale` varchar(255) DEFAULT NULL,
  `copied` int(11) NOT NULL DEFAULT '0',
  KEY `ind_pay_emp` (`id_company`,`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_compensation_employees` */

/*Table structure for table `payroll_compensation_employees_archive` */

DROP TABLE IF EXISTS `payroll_compensation_employees_archive`;

CREATE TABLE `payroll_compensation_employees_archive` (
  `id_compensation_employee` int(255) NOT NULL,
  `id_pay_structure` int(255) NOT NULL,
  `id_pay_grade` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_role` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `id_currency` int(255) NOT NULL,
  `tax` int(1) NOT NULL,
  `archive` int(1) NOT NULL,
  `id_schedule` varchar(255) NOT NULL,
  `pay_scale` varchar(150) DEFAULT NULL,
  `id_run` varchar(150) DEFAULT NULL,
  KEY `ind_pay_emp_arc` (`id_company`,`id_user`,`id_schedule`,`id_run`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_compensation_employees_archive` */

/*Table structure for table `payroll_compensation_temp` */

DROP TABLE IF EXISTS `payroll_compensation_temp`;

CREATE TABLE `payroll_compensation_temp` (
  `id_payroll_compensation` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_payroll_allowance` int(255) unsigned DEFAULT NULL,
  `id_payroll_deduction` int(255) unsigned DEFAULT NULL,
  `id_payroll_contribution` int(255) unsigned DEFAULT NULL,
  `id_payroll_otherpay` int(255) unsigned DEFAULT NULL,
  `id_company` int(255) unsigned DEFAULT NULL,
  `id_user` int(255) unsigned DEFAULT NULL,
  `id_schedule` int(255) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_payroll_compensation`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_compensation_temp` */

/*Table structure for table `payroll_confirmation_code` */

DROP TABLE IF EXISTS `payroll_confirmation_code`;

CREATE TABLE `payroll_confirmation_code` (
  `id_payroll_conformation_code` int(25) NOT NULL AUTO_INCREMENT,
  `code_encrypt` varchar(255) NOT NULL,
  `date_generated` datetime NOT NULL,
  `date_expired` datetime NOT NULL,
  `used_at` datetime DEFAULT NULL,
  `id_user` int(25) NOT NULL,
  `id_company` int(25) NOT NULL,
  `used` int(11) DEFAULT '0',
  PRIMARY KEY (`id_payroll_conformation_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_confirmation_code` */

/*Table structure for table `payroll_contribution` */

DROP TABLE IF EXISTS `payroll_contribution`;

CREATE TABLE `payroll_contribution` (
  `id_payroll_contribution` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(255) unsigned DEFAULT NULL,
  `contribution` longtext,
  PRIMARY KEY (`id_payroll_contribution`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `payroll_contribution` */

/*Table structure for table `payroll_contribution_settings` */

DROP TABLE IF EXISTS `payroll_contribution_settings`;

CREATE TABLE `payroll_contribution_settings` (
  `id_payroll_contribution_settings` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `contribution` varchar(255) DEFAULT NULL,
  `percentage` varchar(255) DEFAULT NULL,
  `id_company` int(255) unsigned DEFAULT NULL,
  `ref_id` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `component` varchar(255) DEFAULT NULL,
  `type` int(255) unsigned DEFAULT NULL,
  `id_payroll_contribution_type` int(255) unsigned DEFAULT NULL,
  `taxable` int(255) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_payroll_contribution_settings`),
  KEY `FK_payroll_contribution_settings_1` (`id_company`),
  KEY `FK_payroll_contribution_settings_2` (`id_payroll_contribution_type`),
  CONSTRAINT `FK_payroll_contribution_settings_1` FOREIGN KEY (`id_company`) REFERENCES `companies` (`id_company`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_payroll_contribution_settings_2` FOREIGN KEY (`id_payroll_contribution_type`) REFERENCES `payroll_contribution_type` (`id_payroll_contribution_type`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_contribution_settings` */

/*Table structure for table `payroll_contribution_type` */

DROP TABLE IF EXISTS `payroll_contribution_type`;

CREATE TABLE `payroll_contribution_type` (
  `id_payroll_contribution_type` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `contribution_type` varchar(255) DEFAULT NULL,
  `id_company` int(255) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_payroll_contribution_type`),
  KEY `FK_payroll_contribution_type_1` (`id_company`),
  CONSTRAINT `FK_payroll_contribution_type_1` FOREIGN KEY (`id_company`) REFERENCES `companies` (`id_company`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_contribution_type` */

/*Table structure for table `payroll_csv_upload_tracker` */

DROP TABLE IF EXISTS `payroll_csv_upload_tracker`;

CREATE TABLE `payroll_csv_upload_tracker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `file_name` varchar(255) NOT NULL,
  `id_run` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_csv_upload_tracker` */

/*Table structure for table `payroll_currency_settings` */

DROP TABLE IF EXISTS `payroll_currency_settings`;

CREATE TABLE `payroll_currency_settings` (
  `id_payroll_currency_settings` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_payroll_currency_settings`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_currency_settings` */

/*Table structure for table `payroll_debit_account` */

DROP TABLE IF EXISTS `payroll_debit_account`;

CREATE TABLE `payroll_debit_account` (
  `id_payroll_debit_account` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `account_number` varchar(255) NOT NULL,
  `account_name` varchar(255) NOT NULL,
  `mandate_code` varchar(255) DEFAULT NULL,
  `bank_id` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_creator` int(255) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `ref_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id_payroll_debit_account`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `payroll_debit_account` */

/*Table structure for table `payroll_deduction` */

DROP TABLE IF EXISTS `payroll_deduction`;

CREATE TABLE `payroll_deduction` (
  `id_payroll_deduction` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(255) unsigned DEFAULT NULL,
  `deduction` longtext,
  PRIMARY KEY (`id_payroll_deduction`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_deduction` */

/*Table structure for table `payroll_deduction_settings` */

DROP TABLE IF EXISTS `payroll_deduction_settings`;

CREATE TABLE `payroll_deduction_settings` (
  `id_payroll_deduction_setting` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `deduction` varchar(255) DEFAULT NULL,
  `percentage` int(255) unsigned DEFAULT NULL,
  `component` varchar(150) DEFAULT NULL,
  `id_company` int(255) unsigned DEFAULT NULL,
  `ref_id` varchar(255) DEFAULT NULL,
  `type` int(255) unsigned DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `taxable` int(255) unsigned DEFAULT NULL,
  `id_payroll_deduction_type` int(255) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_payroll_deduction_setting`),
  KEY `FK_payroll_deduction_settings_1` (`id_company`),
  KEY `FK_payroll_deduction_settings_2` (`id_payroll_deduction_type`),
  CONSTRAINT `FK_payroll_deduction_settings_1` FOREIGN KEY (`id_company`) REFERENCES `companies` (`id_company`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_payroll_deduction_settings_2` FOREIGN KEY (`id_payroll_deduction_type`) REFERENCES `payroll_deduction_type` (`id_payroll_deduction_type`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_deduction_settings` */

/*Table structure for table `payroll_deduction_type` */

DROP TABLE IF EXISTS `payroll_deduction_type`;

CREATE TABLE `payroll_deduction_type` (
  `id_payroll_deduction_type` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `deduction_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_payroll_deduction_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `payroll_deduction_type` */

insert  into `payroll_deduction_type`(`id_payroll_deduction_type`,`deduction_type`) values (1,'NHF'),(2,'Pension'),(3,'Other Deductions');

/*Table structure for table `payroll_disbursement` */

DROP TABLE IF EXISTS `payroll_disbursement`;

CREATE TABLE `payroll_disbursement` (
  `id_payroll_disbursement` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `ref_id` varchar(255) NOT NULL,
  `id_run` varchar(255) NOT NULL,
  `status` int(255) unsigned NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `payment_option` varchar(255) NOT NULL,
  `no_of_employee` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_payroll_disbursement`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `payroll_disbursement` */

/*Table structure for table `payroll_disbursement_approval` */

DROP TABLE IF EXISTS `payroll_disbursement_approval`;

CREATE TABLE `payroll_disbursement_approval` (
  `id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `payroll_disbursement_approval` */

/*Table structure for table `payroll_disbursement_details` */

DROP TABLE IF EXISTS `payroll_disbursement_details`;

CREATE TABLE `payroll_disbursement_details` (
  `id_payroll_disbursement_details` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_run` varchar(255) NOT NULL,
  `id_payroll_disbursement` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_payroll_disbursement_details`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `payroll_disbursement_details` */

/*Table structure for table `payroll_disbursement_initiate` */

DROP TABLE IF EXISTS `payroll_disbursement_initiate`;

CREATE TABLE `payroll_disbursement_initiate` (
  `id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `payroll_disbursement_initiate` */

/*Table structure for table `payroll_employee_deductions` */

DROP TABLE IF EXISTS `payroll_employee_deductions`;

CREATE TABLE `payroll_employee_deductions` (
  `id_payroll_employee_deductions` int(255) NOT NULL AUTO_INCREMENT,
  `id_company` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `id_payroll_company_deductions` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `rate` varchar(255) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_payroll_employee_deductions`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_employee_deductions` */

/*Table structure for table `payroll_employee_loan` */

DROP TABLE IF EXISTS `payroll_employee_loan`;

CREATE TABLE `payroll_employee_loan` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `id_user` int(25) NOT NULL,
  `id_company` int(25) NOT NULL,
  `amount_due` varchar(150) NOT NULL,
  `id_period` int(25) NOT NULL,
  `id_run` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ind_pay_loan_unq` (`id_company`,`id_user`,`id_run`,`id_period`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_employee_loan` */

/*Table structure for table `payroll_employee_loan_archive` */

DROP TABLE IF EXISTS `payroll_employee_loan_archive`;

CREATE TABLE `payroll_employee_loan_archive` (
  `id` int(25) NOT NULL,
  `id_user` int(25) NOT NULL,
  `id_company` int(25) NOT NULL,
  `amount_due` varchar(150) NOT NULL,
  `id_period` int(25) NOT NULL,
  `id_run` varchar(150) NOT NULL,
  KEY `ind_pay_loan_arc` (`id_company`,`id_user`,`id_run`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_employee_loan_archive` */

/*Table structure for table `payroll_employee_pay_types` */

DROP TABLE IF EXISTS `payroll_employee_pay_types`;

CREATE TABLE `payroll_employee_pay_types` (
  `id_payroll_employee_pay_type` int(255) NOT NULL AUTO_INCREMENT,
  `id_company` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `id_payroll_pay_types` int(255) NOT NULL,
  `amount` int(255) NOT NULL,
  PRIMARY KEY (`id_payroll_employee_pay_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_employee_pay_types` */

/*Table structure for table `payroll_employee_paygrade` */

DROP TABLE IF EXISTS `payroll_employee_paygrade`;

CREATE TABLE `payroll_employee_paygrade` (
  `id_payroll_employee_paygrade` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_payroll_paygrade` int(255) unsigned DEFAULT NULL,
  `id_company` int(255) unsigned DEFAULT NULL,
  `id_user` int(255) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_payroll_employee_paygrade`),
  KEY `FK_payroll_employee_paygrade_1` (`id_payroll_paygrade`),
  KEY `FK_payroll_employee_paygrade_2` (`id_company`),
  KEY `FK_payroll_employee_paygrade_3` (`id_user`),
  CONSTRAINT `FK_payroll_employee_paygrade_1` FOREIGN KEY (`id_payroll_paygrade`) REFERENCES `payroll_paygrade` (`id_payroll_paygrade`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_payroll_employee_paygrade_2` FOREIGN KEY (`id_company`) REFERENCES `companies` (`id_company`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_payroll_employee_paygrade_3` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_employee_paygrade` */

/*Table structure for table `payroll_employee_payslip` */

DROP TABLE IF EXISTS `payroll_employee_payslip`;

CREATE TABLE `payroll_employee_payslip` (
  `id_payroll_employee` int(255) NOT NULL AUTO_INCREMENT,
  `id_company` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `id_payroll_records` int(255) NOT NULL,
  `id_payroll_pay_schedule` int(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `date_hired` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pay_frequency` varchar(255) NOT NULL,
  `bank_account` varchar(255) NOT NULL,
  `rsa_pin` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `gross_pay` text NOT NULL,
  `deductions` text NOT NULL,
  `netpay` text NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `start_date` varchar(255) DEFAULT NULL,
  `end_date` varchar(255) DEFAULT NULL,
  `tax_status` int(1) NOT NULL DEFAULT '1',
  `tax` varchar(255) NOT NULL,
  `pay_date` varchar(255) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `pay_status` int(1) NOT NULL DEFAULT '0',
  `date_confirmation` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_payroll_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_employee_payslip` */

/*Table structure for table `payroll_employee_reimbursibles` */

DROP TABLE IF EXISTS `payroll_employee_reimbursibles`;

CREATE TABLE `payroll_employee_reimbursibles` (
  `id_payrollreimbursibles` int(255) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) NOT NULL,
  `id_user` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `paid` int(1) NOT NULL DEFAULT '0',
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_payrollreimbursibles`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_employee_reimbursibles` */

/*Table structure for table `payroll_employee_settings` */

DROP TABLE IF EXISTS `payroll_employee_settings`;

CREATE TABLE `payroll_employee_settings` (
  `id_payroll_employee` int(255) NOT NULL AUTO_INCREMENT,
  `id_company` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `payment_method` varchar(100) NOT NULL,
  `id_payroll_pay_schedule` int(255) NOT NULL,
  `schedule_name` varchar(255) NOT NULL,
  `pay_schedule` varchar(255) NOT NULL,
  `gross_pay` int(255) NOT NULL,
  `net_pay` int(255) NOT NULL,
  `frequency` varchar(255) NOT NULL,
  `tax_status` int(1) NOT NULL DEFAULT '1',
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_payroll_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_employee_settings` */

/*Table structure for table `payroll_employee_specific_deductions` */

DROP TABLE IF EXISTS `payroll_employee_specific_deductions`;

CREATE TABLE `payroll_employee_specific_deductions` (
  `id_payroll_employee_specific_deductions` int(255) NOT NULL AUTO_INCREMENT,
  `id_company` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `spread` int(50) NOT NULL,
  `spread_count` int(50) NOT NULL,
  PRIMARY KEY (`id_payroll_employee_specific_deductions`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_employee_specific_deductions` */

/*Table structure for table `payroll_incidental` */

DROP TABLE IF EXISTS `payroll_incidental`;

CREATE TABLE `payroll_incidental` (
  `id_incidental` int(11) NOT NULL AUTO_INCREMENT,
  `id_company` int(11) DEFAULT NULL,
  `period` varchar(255) DEFAULT NULL,
  `amount` varchar(255) NOT NULL,
  `tax` int(11) DEFAULT NULL,
  `id_employee_note` int(11) NOT NULL,
  `priority` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `id_creator` int(11) DEFAULT NULL,
  `date_added` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(50) DEFAULT NULL,
  `working_days` int(11) DEFAULT NULL,
  `id_run` varchar(255) NOT NULL,
  `id_period` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_incidental`),
  KEY `ind_incidental` (`id_company`,`status`,`tax`,`id_period`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_incidental` */

/*Table structure for table `payroll_incidental_access` */

DROP TABLE IF EXISTS `payroll_incidental_access`;

CREATE TABLE `payroll_incidental_access` (
  `id_incidental_access` int(255) NOT NULL AUTO_INCREMENT,
  `id_user` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  PRIMARY KEY (`id_incidental_access`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_incidental_access` */

/*Table structure for table `payroll_log` */

DROP TABLE IF EXISTS `payroll_log`;

CREATE TABLE `payroll_log` (
  `id_log` int(25) NOT NULL AUTO_INCREMENT,
  `action` varchar(150) NOT NULL,
  `time` datetime NOT NULL,
  `id_company` int(25) NOT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `payroll_log` */

/*Table structure for table `payroll_otherpay` */

DROP TABLE IF EXISTS `payroll_otherpay`;

CREATE TABLE `payroll_otherpay` (
  `id_payroll_otherpay` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(255) unsigned DEFAULT NULL,
  `otherpay` longtext,
  `id_user` int(255) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_payroll_otherpay`) USING BTREE,
  KEY `FK_payroll_otherpay_1` (`id_company`),
  KEY `FK_payroll_otherpay_2` (`id_user`),
  CONSTRAINT `FK_payroll_otherpay_1` FOREIGN KEY (`id_company`) REFERENCES `companies` (`id_company`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_payroll_otherpay_2` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_otherpay` */

/*Table structure for table `payroll_otherpay_settings` */

DROP TABLE IF EXISTS `payroll_otherpay_settings`;

CREATE TABLE `payroll_otherpay_settings` (
  `otherpay_id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `otherpay` varchar(255) DEFAULT NULL,
  `percentage` int(255) unsigned DEFAULT NULL,
  `id_company` int(255) unsigned DEFAULT NULL,
  `ref_id` varchar(255) DEFAULT NULL,
  `component` varchar(255) DEFAULT NULL,
  `type` int(255) unsigned DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `taxable` int(255) unsigned DEFAULT NULL,
  PRIMARY KEY (`otherpay_id`),
  KEY `FK_otherpay_1` (`id_company`),
  CONSTRAINT `FK_otherpay_1` FOREIGN KEY (`id_company`) REFERENCES `companies` (`id_company`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_otherpay_settings` */

/*Table structure for table `payroll_pay_schedule` */

DROP TABLE IF EXISTS `payroll_pay_schedule`;

CREATE TABLE `payroll_pay_schedule` (
  `id_schedule` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(255) unsigned NOT NULL,
  `schedule_name` varchar(255) NOT NULL,
  `schedule_type` int(255) unsigned NOT NULL DEFAULT '1',
  `dates_json` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_creator` int(255) unsigned NOT NULL,
  `status` int(11) NOT NULL,
  `ref_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_schedule`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

/*Data for the table `payroll_pay_schedule` */

/*Table structure for table `payroll_pay_types` */

DROP TABLE IF EXISTS `payroll_pay_types`;

CREATE TABLE `payroll_pay_types` (
  `id_payroll_pay_types` int(255) NOT NULL AUTO_INCREMENT,
  `id_company` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `default` int(1) NOT NULL DEFAULT '0',
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_payroll_pay_types`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_pay_types` */

/*Table structure for table `payroll_paygrade` */

DROP TABLE IF EXISTS `payroll_paygrade`;

CREATE TABLE `payroll_paygrade` (
  `id_payroll_paygrade` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `pay_grade` varchar(255) DEFAULT NULL,
  `id_company` int(255) unsigned DEFAULT NULL,
  `id_payroll_compensation` int(255) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_payroll_paygrade`),
  KEY `FK_payroll_paygrade_1` (`id_company`),
  KEY `FK_payroll_paygrade_2` (`id_payroll_compensation`),
  CONSTRAINT `FK_payroll_paygrade_1` FOREIGN KEY (`id_company`) REFERENCES `companies` (`id_company`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_payroll_paygrade_2` FOREIGN KEY (`id_payroll_compensation`) REFERENCES `payroll_compensation` (`id_payroll_compensation`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

/*Data for the table `payroll_paygrade` */

/*Table structure for table `payroll_payslip_deductions` */

DROP TABLE IF EXISTS `payroll_payslip_deductions`;

CREATE TABLE `payroll_payslip_deductions` (
  `id_payroll_payslip_deductions` int(255) NOT NULL AUTO_INCREMENT,
  `id_payroll_records` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `id_employee_deductions` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `rate` varchar(255) NOT NULL,
  PRIMARY KEY (`id_payroll_payslip_deductions`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_payslip_deductions` */

/*Table structure for table `payroll_payslip_pay_types` */

DROP TABLE IF EXISTS `payroll_payslip_pay_types`;

CREATE TABLE `payroll_payslip_pay_types` (
  `id_payroll_payslip_pay_types` int(255) NOT NULL AUTO_INCREMENT,
  `id_payroll_records` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `id_employee_pay_types` int(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_payroll_payslip_pay_types`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_payslip_pay_types` */

/*Table structure for table `payroll_period` */

DROP TABLE IF EXISTS `payroll_period`;

CREATE TABLE `payroll_period` (
  `id_payroll_period` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `payroll_period` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_payroll_period`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `payroll_period` */

insert  into `payroll_period`(`id_payroll_period`,`payroll_period`) values (1,'Week'),(2,'Month'),(3,'Year');

/*Table structure for table `payroll_processed` */

DROP TABLE IF EXISTS `payroll_processed`;

CREATE TABLE `payroll_processed` (
  `id_payroll_processed` int(25) NOT NULL AUTO_INCREMENT,
  `id_run` varchar(255) NOT NULL,
  `id_user` int(25) NOT NULL,
  `gross_pay` varchar(255) NOT NULL,
  `nhf` varchar(150) DEFAULT NULL,
  `pension` varchar(150) DEFAULT NULL,
  `loan` varchar(150) DEFAULT NULL,
  `tax` varchar(255) NOT NULL,
  `deduction` varchar(255) NOT NULL,
  `net_pay` varchar(255) NOT NULL,
  `id_company` int(25) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id_payroll_processed`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_processed` */

/*Table structure for table `payroll_record_access` */

DROP TABLE IF EXISTS `payroll_record_access`;

CREATE TABLE `payroll_record_access` (
  `id_payroll_record_access` int(255) NOT NULL AUTO_INCREMENT,
  `id_user` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  PRIMARY KEY (`id_payroll_record_access`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_record_access` */

/*Table structure for table `payroll_records` */

DROP TABLE IF EXISTS `payroll_records`;

CREATE TABLE `payroll_records` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `id_payroll_records` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_payroll_pay_schedule` int(255) NOT NULL,
  `pay_schedule` varchar(255) NOT NULL,
  `schedule_name` varchar(255) NOT NULL,
  `pay_date` timestamp NULL DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_records` */

/*Table structure for table `payroll_run_schedule` */

DROP TABLE IF EXISTS `payroll_run_schedule`;

CREATE TABLE `payroll_run_schedule` (
  `id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `from` timestamp NULL DEFAULT NULL,
  `to` timestamp NULL DEFAULT NULL,
  `pay_date` timestamp NULL DEFAULT NULL,
  `id_schedule` int(255) unsigned NOT NULL,
  `inserted_by` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `status` int(1) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `schedule_name` varchar(255) NOT NULL,
  `id_run` varchar(255) NOT NULL,
  `id_period` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_payroll_run_schedule_2` (`id_company`),
  KEY `FK_payroll_run_schedule_id_schedule` (`id_schedule`),
  KEY `FK_payroll_run_schedule_inserter` (`inserted_by`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `payroll_run_schedule` */

/*Table structure for table `payroll_run_schedule_archive` */

DROP TABLE IF EXISTS `payroll_run_schedule_archive`;

CREATE TABLE `payroll_run_schedule_archive` (
  `id_run` varchar(255) NOT NULL,
  `from` timestamp NULL DEFAULT NULL,
  `to` timestamp NULL DEFAULT NULL,
  `pay_date` timestamp NULL DEFAULT NULL,
  `id_schedule` varchar(255) NOT NULL,
  `inserted_by` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `status` int(1) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `schedule_name` varchar(255) NOT NULL,
  `id_period` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `payroll_run_schedule_archive` */

/*Table structure for table `payroll_stage_perms` */

DROP TABLE IF EXISTS `payroll_stage_perms`;

CREATE TABLE `payroll_stage_perms` (
  `id_payroll_stage_perms` int(255) NOT NULL AUTO_INCREMENT,
  `id_status` int(255) DEFAULT NULL,
  `id_user` int(255) unsigned DEFAULT NULL,
  `id_company` int(255) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_payroll_stage_perms`),
  KEY `FK_payroll_stage_perms_1` (`id_user`),
  KEY `FK_payroll_stage_perms_2` (`id_company`),
  CONSTRAINT `FK_payroll_stage_perms_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_payroll_stage_perms_2` FOREIGN KEY (`id_company`) REFERENCES `companies` (`id_company`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

/*Data for the table `payroll_stage_perms` */

/*Table structure for table `payroll_step_processed` */

DROP TABLE IF EXISTS `payroll_step_processed`;

CREATE TABLE `payroll_step_processed` (
  `id_step_processed` int(25) NOT NULL AUTO_INCREMENT,
  `id_run` varchar(150) NOT NULL,
  `step` int(25) NOT NULL,
  `emp_count` int(25) DEFAULT NULL,
  `status` int(25) DEFAULT '0',
  `id_company` int(25) NOT NULL,
  PRIMARY KEY (`id_step_processed`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_step_processed` */

/*Table structure for table `payroll_temp_selected` */

DROP TABLE IF EXISTS `payroll_temp_selected`;

CREATE TABLE `payroll_temp_selected` (
  `id_temp` int(25) NOT NULL AUTO_INCREMENT,
  `id_company` int(25) NOT NULL,
  `selected_emp` text,
  `step` int(25) NOT NULL,
  `id_run` varchar(255) NOT NULL,
  `emp_count` int(25) DEFAULT NULL,
  PRIMARY KEY (`id_temp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `payroll_temp_selected` */

/*Table structure for table `payroll_transaction_log` */

DROP TABLE IF EXISTS `payroll_transaction_log`;

CREATE TABLE `payroll_transaction_log` (
  `id_payroll_transaction_log` int(200) NOT NULL AUTO_INCREMENT,
  `transfer_code` varchar(255) NOT NULL,
  `mac` varchar(255) NOT NULL,
  `status_code` int(255) NOT NULL,
  `details` text NOT NULL,
  `id_user` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_payroll_records` int(255) NOT NULL,
  `id_payroll_employee` int(255) NOT NULL,
  `date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_payroll_transaction_log`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_transaction_log` */

/*Table structure for table `pension_fund_administrators` */

DROP TABLE IF EXISTS `pension_fund_administrators`;

CREATE TABLE `pension_fund_administrators` (
  `id_pension_fund_administrator` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `custodian` varchar(255) NOT NULL,
  `id_bank` int(255) NOT NULL,
  `account_number` int(10) unsigned zerofill NOT NULL,
  `account_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_pension_fund_administrator`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `pension_fund_administrators` */

insert  into `pension_fund_administrators`(`id_pension_fund_administrator`,`name`,`custodian`,`id_bank`,`account_number`,`account_name`) values (1,'ARM Pension Managers Limited','UBA Pension Custodian Ltd',17,1005385538,'UPCL/ARM Pension RSA Contribution Account (UBA Pension Custodian Limited/ARM Pension Managers Contribution)'),(2,'Crusader Sterling Pensions Limited','First Pension Custodian Limited',7,2147483647,'FPCNL-RE-CPL Contribution'),(3,'Fidelity Pension Managers','First Pension Custodian Limited',7,2008709035,'FCNL-REFIDELITY PML CONTRIBUTION'),(4,'GTB-AM Pensions Limited','Diamond Pension Fund Custodian Limited',3,0005741189,'Â DPFC/GTB-AM Pensions Ltd'),(5,'Leadway Pensure PFA Limited','UBA Pensions Custodian Limited',17,1005906472,'Leadway Pensure PFA/UBA PFC'),(6,'NLPC Pension Fund Administrators Limited','First Pension Fund Custodian Nigeria Limited',7,2006895606,'FPCNL/NLPC PFA LTD'),(7,'Premium Pension Limited','First Pension Fund Custodian Nigeria Limited',7,2006857970,'FCNL-PPL-RE-CONTRIBUTION'),(8,'Stanbic IBTC Pension Managers Limited','Zenith Pensions Custodian Limited',20,1010885522,'ZPC/SIPML CONTRIBUTION ACCOUNT'),(9,'AIICO Pension Managers Limited','First Pension Custodian Nigerian Limited',7,2007203062,'FPCNL RE: AIICO'),(10,'First Guarantee Pension Limited','First Pension Custodian Limited',7,2007030987,'FPCNL-RE-FGPL-CONTRIBUTION'),(11,'OAK Pensions Limited','First Pension Custodian',7,2007909319,'FPCNL RE OAK CONTRIBUTION'),(12,'Legacy Pension Managers Limited','UBA Pension Fund Custodian',17,1005385514,'UBA PENSIONS CUSTODIAN/LEGACY'),(13,'Trustfund Pensions Plc','Zenith Pensions Custodian Limited',20,1010925059,'ZPC/Trust fund Pension Contribution Account');

/*Table structure for table `phone_types` */

DROP TABLE IF EXISTS `phone_types`;

CREATE TABLE `phone_types` (
  `id_phone_type` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `phone_type` varchar(255) DEFAULT NULL,
  `id_company` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_phone_type`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

/*Data for the table `phone_types` */

insert  into `phone_types`(`id_phone_type`,`phone_type`,`id_company`) values (1,'Work',0),(2,'Home',0),(3,'Personal',0);

/*Table structure for table `qualifications` */

DROP TABLE IF EXISTS `qualifications`;

CREATE TABLE `qualifications` (
  `id_qualification` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_qualification`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `qualifications` */

insert  into `qualifications`(`id_qualification`,`name`) values (1,'OND'),(2,'HND'),(3,'Bachelors'),(4,'Masters'),(5,'Doctorate'),(6,'O-Level'),(7,'MBA');

/*Table structure for table `ref_questions` */

DROP TABLE IF EXISTS `ref_questions`;

CREATE TABLE `ref_questions` (
  `id_question` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  PRIMARY KEY (`id_question`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ref_questions` */

/*Table structure for table `reward_allocation` */

DROP TABLE IF EXISTS `reward_allocation`;

CREATE TABLE `reward_allocation` (
  `id_reward_allocation` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `point` varchar(255) NOT NULL,
  `cash` varchar(255) NOT NULL,
  `commission` varchar(255) NOT NULL,
  `recognition` varchar(255) NOT NULL,
  PRIMARY KEY (`id_reward_allocation`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `reward_allocation` */

/*Table structure for table `reward_commission` */

DROP TABLE IF EXISTS `reward_commission`;

CREATE TABLE `reward_commission` (
  `id_reward_commission` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_currency` int(255) unsigned NOT NULL,
  `amount` varchar(255) NOT NULL,
  `description` text,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `id_reward_status` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_reward_commission`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `reward_commission` */

/*Table structure for table `reward_manager_permission` */

DROP TABLE IF EXISTS `reward_manager_permission`;

CREATE TABLE `reward_manager_permission` (
  `id_reward_perm` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_access` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_reward_perm`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `reward_manager_permission` */

/*Table structure for table `reward_permission` */

DROP TABLE IF EXISTS `reward_permission`;

CREATE TABLE `reward_permission` (
  `id_reward_access` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_reward_access`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `reward_permission` */

/*Table structure for table `reward_points` */

DROP TABLE IF EXISTS `reward_points`;

CREATE TABLE `reward_points` (
  `id_reward_point` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `amount` varchar(255) NOT NULL,
  `description` text,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `id_reward_status` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_reward_point`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `reward_points` */

/*Table structure for table `reward_recognition` */

DROP TABLE IF EXISTS `reward_recognition`;

CREATE TABLE `reward_recognition` (
  `id_reward_recognition` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `amount` int(255) unsigned NOT NULL,
  `description` text,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `id_reward_status` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_reward_recognition`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `reward_recognition` */

/*Table structure for table `reward_report` */

DROP TABLE IF EXISTS `reward_report`;

CREATE TABLE `reward_report` (
  `id_reward_report` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `ref_id` varchar(255) NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_user` int(255) unsigned NOT NULL,
  `date_generated` datetime NOT NULL,
  `run_by` int(255) unsigned NOT NULL,
  `status` int(255) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_reward_report`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `reward_report` */

/*Table structure for table `reward_status` */

DROP TABLE IF EXISTS `reward_status`;

CREATE TABLE `reward_status` (
  `id_reward_status` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `reward_status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_reward_status`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `reward_status` */

insert  into `reward_status`(`id_reward_status`,`reward_status`) values (1,'Pending'),(2,'Earned');

/*Table structure for table `reward_temp_selected` */

DROP TABLE IF EXISTS `reward_temp_selected`;

CREATE TABLE `reward_temp_selected` (
  `id_temp` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(255) unsigned DEFAULT NULL,
  `selected_emp` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `month` varchar(255) DEFAULT NULL,
  `ref_id` varchar(255) DEFAULT NULL,
  `run_by` int(255) unsigned NOT NULL,
  `date_created` date DEFAULT NULL,
  PRIMARY KEY (`id_temp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `reward_temp_selected` */

/*Table structure for table `reward_transaction` */

DROP TABLE IF EXISTS `reward_transaction`;

CREATE TABLE `reward_transaction` (
  `id_reward_transaction` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `no_of_staff` int(255) unsigned NOT NULL,
  `datetime` datetime DEFAULT NULL,
  `id_company` int(255) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_reward_transaction`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `reward_transaction` */

/*Table structure for table `reward_type` */

DROP TABLE IF EXISTS `reward_type`;

CREATE TABLE `reward_type` (
  `id_reward_type` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `reward_type` varchar(255) NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_reward_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `reward_type` */

/*Table structure for table `rewards` */

DROP TABLE IF EXISTS `rewards`;

CREATE TABLE `rewards` (
  `id_reward` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_currency` int(255) unsigned DEFAULT NULL,
  `amount` varchar(255) NOT NULL,
  `description` text,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `id_reward_status` int(255) unsigned NOT NULL,
  `id_reward_type` int(255) unsigned NOT NULL,
  `id_reward_transaction` int(255) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_reward`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rewards` */

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id_role` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(255) unsigned NOT NULL,
  `roles` varchar(255) NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `job_description` text,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `roles` */

/*Table structure for table `roles_job_functions` */

DROP TABLE IF EXISTS `roles_job_functions`;

CREATE TABLE `roles_job_functions` (
  `id_role` int(255) NOT NULL,
  `id_function` int(255) NOT NULL,
  `id_company` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `roles_job_functions` */

/*Table structure for table `salary_range` */

DROP TABLE IF EXISTS `salary_range`;

CREATE TABLE `salary_range` (
  `id_salary` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id_salary`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `salary_range` */

insert  into `salary_range`(`id_salary`,`name`) values (1,'40,000 to 70,000'),(2,'70,000 to 100,000'),(3,'100,000 to 150,000'),(4,'150,000 to 200,000'),(5,'200,000 to 300,000'),(6,'300,000 to 500,000'),(7,'500,000+');

/*Table structure for table `schools` */

DROP TABLE IF EXISTS `schools`;

CREATE TABLE `schools` (
  `schoolid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `stateid` int(11) NOT NULL,
  `countryid` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`schoolid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

/*Data for the table `schools` */

insert  into `schools`(`schoolid`,`name`,`stateid`,`countryid`,`created`,`modified`) values (1,'Abia State University Uturu',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(2,'ABTI - American University of Nigeria Yola',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(3,'Abubakar Tafawa Balewa University Bauchi',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(4,'Achievers University Owo Ondo State',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(5,'Adamawa State University Yola',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(6,'Adekunle Ajasin University Akungba-Akoko',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(7,'Afe Babalola University Ado-Ekiti',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(8,'Ahmadu Bello University Zaria Kaduna State',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(9,'Ajayi Crowther University Oyo',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(10,'Akwa Ibom State University of Science and Technology',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(11,'Al Hikmah University Ilorin Kwara State',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(12,'Ambrose Alli University Ekpoma',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(13,'Anambra State University Uli',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(14,'Babcock University Ilishan-Remo',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(15,'Bayero University Kano',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(16,'Baze University,Kuchigoro',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(17,'Bells University of Technology Ota',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(18,'Benson Idahosa University Benin-City',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(19,'Benue State University Makurdi',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(20,'Bingham University New Karu',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(21,'Bowen University Iwo',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(22,'Bukar Abba Ibrahim University Damaturu',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(23,'Caleb University',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(24,'Caritas University Enugu',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(25,'Catholic University of Nigeria Abuja',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(26,'Cetep University Lagos',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(27,'City University of Technology Kaduna',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(28,'City University Yaba',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(29,'Covenant University Ota',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(30,'Crawford University Faith City Igbesa Ogun State',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(31,'Crawford University Oye-Ekiti Campus',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(32,'Crescent University Abeokuta, Ogun State',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(33,'Cross River State University of Technology Ekpo-Abasi',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(34,'Delta State University Abraka',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(35,'Ebonyi State University Abakaliki',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(36,'Enugu State University Of Science And Technology Enugu',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(37,'Federal Polytechnic birnin kebbi',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(38,'Federal University of Petroleum Resource Effurun',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(39,'Federal University of Technology Akure',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(40,'Federal University of Technology Minna',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(41,'Federal University of Technology Owerri',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(42,'Fountain University Osogbo',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(43,'Godfrey Okoye University',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(44,'Gombe State university Gombe',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(45,'Ibrahim Babangida University Lapai',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(46,'Igbinedion University Okada',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(47,'Imo State University',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(48,'Joseph Ayo Babalola University Ikeji-Arakeji',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(49,'Kaduna State University Kaduna',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(50,'Kano State University of Technology Wudil',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(51,'Kebbi State University of Technology Kebbi',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(52,'Kogi State University Ayigba',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(53,'Kwara State University Malete',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(54,'Ladoke Akintola University of Technology Ogbomoso',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(55,'Lagos State University Ojo',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(56,'Landmark University,Omu-Aran',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(57,'Lead City University Ibadan',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(58,'Madonna University (Ihiala) Okija',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(59,'Michael Okpara Federal University of Agriculture Umudike',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(60,'Modibbo Adama University of Technology Yola',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(61,'Moses Orimolade University Omu-Aran',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(62,'Nasarawa State University Keffi',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(63,'National Open University of Nigeria, Lagos',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(64,'Niger Delta University, Wilberforce Island, Bayelsa State',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(65,'Nigerian Defence Academy Kaduna',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(66,'Nnamdi Azikiwe University Awka',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(67,'Novena University Ogume, Delta State',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(68,'Obafemi Awolowo University Ile-Ife',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(69,'Obong University Obong Ntak, Akwa Ibom',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(70,'Oduduwa University, Ipetumodu',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(71,'Olabisi Onabanjo University Ago-Iwoye, Ogun State',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(72,'Ondo State University of Science and Technology Okitipupa',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(73,'Osun State University Osogbo',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(74,'Pan-African University Lagos',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(75,'Paul University College, Awka Anambra',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(76,'Redeemer University Redemption City',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(77,'Renaissance University Agbani',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(78,'Rhema University, Obeama-Asa',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(79,'Rivers State University of Science and Technology Port Harcourt',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(80,'Salem University Lokoja Kogi State',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(81,'Samuel Adegboyega University,Ogwa',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(82,'Tai Solarin University of Education Ijebu-Ode, Ogun State',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(83,'Tansian University,Umunya',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(84,'Taraba State University, Jalingo Ahmed Usman Jalingo',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(85,'Umaru Musa Yar Adua University',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(86,'University of Abuja Gwagwalada',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(87,'University of Abuja Gwagwalada',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(88,'University of Ado Ekiti',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(89,'University of Agriculture Abeokuta',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(90,'University of Agriculture, Abeokuta Abeokuta',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(91,'University of Agriculture, Makurdi Makurdi',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(92,'University of Benin Benin City',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(93,'University of Calabar Calabar',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(94,'University of Education, Ikere Ekiti',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(95,'University of Ibadan Ibadan',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(96,'University of Ilorin Ilorin',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(97,'University of Jos Jos',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(98,'University of Lagos Akoka',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(99,'University of Maiduguri Maiduguri',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(100,'University of Mkar Mkar-Gboko, Benue State',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(101,'University of Mkar',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(102,'University of Nigeria Nsukka',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(103,'University of Port Harcourt',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(104,'University of Uyo Uyo',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(105,'Usman Dan Fodio University Sokoto',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(106,'Veritas University',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(107,'Wellspring University, Evbuobanosa',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(108,'Wesley University Ondo',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(109,'Western Delta University Oghara, Delta State',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(110,'Wukari Jubilee University',1,1,'2013-02-26 09:58:27','2013-02-26 07:58:27'),(111,'Others',1,1,'2013-07-30 00:00:00','0000-00-00 00:00:00');

/*Table structure for table `shared_files` */

DROP TABLE IF EXISTS `shared_files`;

CREATE TABLE `shared_files` (
  `id_file` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) NOT NULL,
  `file_path` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `file_type` varchar(50) NOT NULL,
  `id_company` int(10) unsigned NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `added_by` varchar(255) DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `rack_file_name` varchar(255) DEFAULT NULL,
  `container` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_file`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `shared_files` */

/*Table structure for table `sms_company_info` */

DROP TABLE IF EXISTS `sms_company_info`;

CREATE TABLE `sms_company_info` (
  `id_info` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(255) unsigned NOT NULL,
  `total_units` decimal(20,2) NOT NULL,
  `date_created` date DEFAULT NULL,
  `time_created` time DEFAULT NULL,
  PRIMARY KEY (`id_info`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sms_company_info` */

/*Table structure for table `sms_debit_credit_transactions` */

DROP TABLE IF EXISTS `sms_debit_credit_transactions`;

CREATE TABLE `sms_debit_credit_transactions` (
  `id_transaction` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `debit` decimal(20,2) DEFAULT NULL,
  `credit` decimal(20,2) DEFAULT NULL,
  `id_company` int(255) NOT NULL,
  `balance` decimal(60,2) NOT NULL,
  PRIMARY KEY (`id_transaction`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sms_debit_credit_transactions` */

/*Table structure for table `sms_messages` */

DROP TABLE IF EXISTS `sms_messages`;

CREATE TABLE `sms_messages` (
  `id_message` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_module` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_sender` int(255) unsigned NOT NULL,
  `id_recipient` int(255) unsigned DEFAULT NULL,
  `msg_type` varchar(20) NOT NULL,
  `message` text NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `units` decimal(6,2) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `delivery_report` text,
  `phone_number` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_message`),
  KEY `id_module` (`id_module`),
  KEY `id_company` (`id_company`),
  KEY `id_sender` (`id_sender`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sms_messages` */

/*Table structure for table `sms_recipients` */

DROP TABLE IF EXISTS `sms_recipients`;

CREATE TABLE `sms_recipients` (
  `id_recipient` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_message` int(255) unsigned NOT NULL,
  `id_user` int(255) unsigned NOT NULL,
  `id_sms_api` varchar(64) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `phone_number` varchar(11) NOT NULL,
  PRIMARY KEY (`id_recipient`),
  KEY `id_user` (`id_user`),
  KEY `id_message` (`id_message`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sms_recipients` */

/*Table structure for table `sms_transfer_credits` */

DROP TABLE IF EXISTS `sms_transfer_credits`;

CREATE TABLE `sms_transfer_credits` (
  `id_sms_credits` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `time` time DEFAULT NULL,
  `units` decimal(20,2) NOT NULL,
  `amount` int(255) unsigned NOT NULL,
  `status` int(255) unsigned NOT NULL,
  `id_transfer_type` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_sms_credits`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sms_transfer_credits` */

/*Table structure for table `states` */

DROP TABLE IF EXISTS `states`;

CREATE TABLE `states` (
  `id_state` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `state` varchar(100) NOT NULL,
  `id_country` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_state`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

/*Data for the table `states` */

insert  into `states`(`id_state`,`state`,`id_country`) values (1,'Abia',115),(2,'Abuja',115),(3,'Adamawa',115),(4,'Akwa Ibom',115),(5,'Anambara',115),(6,'Bauchi',115),(7,'Bayelsa',115),(8,'Benue',115),(9,'Borno',115),(10,'Cross River',115),(11,'Delta',115),(12,'Ebonyi',115),(13,'Edo',115),(14,'Ekiti',115),(15,'Enugu',115),(16,'Gombe',115),(17,'Imo',115),(18,'Jigawa',115),(19,'Kaduna',115),(20,'Kano',115),(21,'Katsina',115),(22,'Kebbi',115),(23,'Kogi',115),(24,'Kwara',115),(25,'Lagos',115),(26,'Nassarawa',115),(27,'Niger',115),(28,'Ogun',115),(29,'Ondo',115),(30,'Osun',115),(31,'Oyo',115),(32,'Plateau',115),(33,'Rivers',115),(34,'Sokoto',115),(35,'Taraba',115),(36,'Yobe',115),(37,'Zamfara',115);

/*Table structure for table `subdomain` */

DROP TABLE IF EXISTS `subdomain`;

CREATE TABLE `subdomain` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `subdomain` varchar(200) NOT NULL,
  `logo` varchar(200) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `subdomain` */

/*Table structure for table `tasks` */

DROP TABLE IF EXISTS `tasks`;

CREATE TABLE `tasks` (
  `id_task` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_user_from` int(255) unsigned NOT NULL,
  `id_user_to` int(255) unsigned NOT NULL,
  `id_module` int(255) unsigned NOT NULL,
  `subject` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_finished` timestamp NULL DEFAULT NULL,
  `date_will_start` timestamp NULL DEFAULT NULL,
  `date_will_end` timestamp NULL DEFAULT NULL,
  `tb_name` varchar(255) DEFAULT NULL,
  `tb_column_name` varchar(255) DEFAULT NULL,
  `tb_id_record` int(255) unsigned DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`id_task`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tasks` */

/*Table structure for table `tb_account_type_setup` */

DROP TABLE IF EXISTS `tb_account_type_setup`;

CREATE TABLE `tb_account_type_setup` (
  `id_account` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `account_name` varchar(255) NOT NULL,
  `status` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_account`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

/*Data for the table `tb_account_type_setup` */

insert  into `tb_account_type_setup`(`id_account`,`account_name`,`status`) values (1,'Free Trial',1),(2,'Premium',1);

/*Table structure for table `tb_admin_billings` */

DROP TABLE IF EXISTS `tb_admin_billings`;

CREATE TABLE `tb_admin_billings` (
  `id_billing` varchar(255) NOT NULL,
  `vendor` varchar(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `period_from` datetime NOT NULL,
  `period_to` datetime NOT NULL,
  `due_date` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `subtotal` int(225) unsigned NOT NULL,
  `tax` int(255) unsigned NOT NULL,
  `total` decimal(60,2) NOT NULL,
  `balance` int(255) NOT NULL,
  `status` int(255) NOT NULL,
  PRIMARY KEY (`id_billing`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_admin_billings` */

/*Table structure for table `tb_admin_invoice_details` */

DROP TABLE IF EXISTS `tb_admin_invoice_details`;

CREATE TABLE `tb_admin_invoice_details` (
  `id_invoice` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `unit_cost` int(255) NOT NULL,
  `qty` int(255) NOT NULL,
  `id_details` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id_details`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_admin_invoice_details` */

/*Table structure for table `tb_admin_invoices` */

DROP TABLE IF EXISTS `tb_admin_invoices`;

CREATE TABLE `tb_admin_invoices` (
  `id_invoice` varchar(255) NOT NULL,
  `id_billing` varchar(255) NOT NULL,
  `date_issued` datetime NOT NULL,
  `vendor` varchar(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `status` int(255) NOT NULL,
  `total` decimal(60,2) NOT NULL,
  `date_created` datetime NOT NULL,
  `tax` int(255) unsigned NOT NULL,
  `subtotal` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_invoice`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_admin_invoices` */

/*Table structure for table `tb_admin_perms` */

DROP TABLE IF EXISTS `tb_admin_perms`;

CREATE TABLE `tb_admin_perms` (
  `id_admin` int(255) unsigned NOT NULL,
  `id_role` int(255) unsigned NOT NULL,
  `id_perm` int(255) unsigned NOT NULL,
  KEY `id_admin` (`id_admin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `tb_admin_perms` */

/*Table structure for table `tb_admin_roles` */

DROP TABLE IF EXISTS `tb_admin_roles`;

CREATE TABLE `tb_admin_roles` (
  `id_admin` int(255) unsigned NOT NULL,
  `id_role` int(255) unsigned NOT NULL,
  KEY `id_admin` (`id_admin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `tb_admin_roles` */

/*Table structure for table `tb_admins` */

DROP TABLE IF EXISTS `tb_admins`;

CREATE TABLE `tb_admins` (
  `id_admin` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `last_login_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_role` int(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_admin`,`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `tb_admins` */

/*Table structure for table `tb_bulk_users` */

DROP TABLE IF EXISTS `tb_bulk_users`;

CREATE TABLE `tb_bulk_users` (
  `id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `id_bulk_upload` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_bulk_users` */

/*Table structure for table `tb_bulk_users_upload` */

DROP TABLE IF EXISTS `tb_bulk_users_upload`;

CREATE TABLE `tb_bulk_users_upload` (
  `id_bulk_upload` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(255) unsigned NOT NULL,
  `total_users_added` int(255) unsigned NOT NULL,
  `total_users_failed` int(255) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id_bulk_upload`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_bulk_users_upload` */

/*Table structure for table `tb_failed_emails_registration` */

DROP TABLE IF EXISTS `tb_failed_emails_registration`;

CREATE TABLE `tb_failed_emails_registration` (
  `id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `id_bulk_upload` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_failed_emails_registration` */

/*Table structure for table `tb_menu` */

DROP TABLE IF EXISTS `tb_menu`;

CREATE TABLE `tb_menu` (
  `id_menu` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `id_sort` int(255) NOT NULL DEFAULT '0',
  `id_string` varchar(255) NOT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

/*Data for the table `tb_menu` */

insert  into `tb_menu`(`id_menu`,`subject`,`id_sort`,`id_string`) values (1,'Users',0,'users'),(2,'Employers',0,'employers'),(3,'Modules',0,'modules'),(4,'HR Consultants',0,'hr_consultants'),(5,'Attendance',0,'attendance');

/*Table structure for table `tb_role_perms` */

DROP TABLE IF EXISTS `tb_role_perms`;

CREATE TABLE `tb_role_perms` (
  `id_perm` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_role` int(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `id_menu` int(255) unsigned NOT NULL,
  `in_menu` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_perm`,`id_string`),
  KEY `id_menu` (`id_menu`,`in_menu`),
  KEY `id_menu_2` (`id_menu`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

/*Data for the table `tb_role_perms` */

insert  into `tb_role_perms`(`id_perm`,`id_role`,`subject`,`id_string`,`id_menu`,`in_menu`) values (1,1,'Add User','add_user',1,1),(2,1,'Edit User Status','edit_user_status',1,0),(3,1,'Edit User Permissions','edit_user_permissions',1,0),(4,1,'Reset Password','reset_password',1,0),(5,1,'Add Employer','add_employer',2,1),(6,1,'List Employers','list_employers',2,1),(7,1,'View Employer Details','view_employer_details',2,0),(8,1,'Edit Employer Status','edit_employer_status',2,0),(9,3,'Add Employer','add_employer',2,0),(10,3,'List Employers','list_employers',2,1),(11,3,'View Employer Details','view_employer_details',2,0),(12,3,'Edit Employer Status','edit_employer_status',2,0),(13,1,'List Modules','list_modules',3,1),(14,1,'Add Module','add_module',3,0),(15,1,'Edit Module Status','Edit_module_status',3,0),(16,1,'Delete Module','delete_module',3,0),(17,1,'List HR Consultants','list_hr_consultants',4,1),(18,1,'Add HR Consultant','add_hr_consultant',4,1),(19,1,'Edit HR Consultant Status','edit_hr_consultant_status',4,0),(20,1,'Delete HR Consultant','delete_hr_consultant',4,0),(21,1,'Manage Device','manage_device',5,1),(22,1,'Bulk Message','bulk_message',1,1);

/*Table structure for table `tb_roles` */

DROP TABLE IF EXISTS `tb_roles`;

CREATE TABLE `tb_roles` (
  `id_role` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

/*Data for the table `tb_roles` */

insert  into `tb_roles`(`id_role`,`role_name`) values (1,'Super Admin'),(2,'Admin'),(3,'Support Staff'),(4,'Developer');

/*Table structure for table `tbl_checkin` */

DROP TABLE IF EXISTS `tbl_checkin`;

CREATE TABLE `tbl_checkin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `checkinout_id` int(11) unsigned DEFAULT NULL,
  `checkdate` date DEFAULT NULL,
  `checkin` time DEFAULT NULL,
  `type` smallint(6) DEFAULT NULL COMMENT '0-checkin, 1-checkout, 2-breakin, 3-breakout, 4-otin 5-otout',
  `SN` varchar(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `checkinout_id` (`checkinout_id`),
  KEY `FK416B98ACFE27C04D` (`checkinout_id`),
  CONSTRAINT `FK416B98ACFE27C04D` FOREIGN KEY (`checkinout_id`) REFERENCES `check_in_out` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

/*Data for the table `tbl_checkin` */

/*Table structure for table `temp_appraisal_step_1` */

DROP TABLE IF EXISTS `temp_appraisal_step_1`;

CREATE TABLE `temp_appraisal_step_1` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `kpis` int(255) NOT NULL,
  `ref_id` varchar(255) NOT NULL,
  `id_kpi` varchar(255) NOT NULL,
  `id_template_type` int(255) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `id_section` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `id_role` int(255) unsigned NOT NULL,
  `id_appraisal_type` int(255) unsigned NOT NULL,
  `weight` int(255) NOT NULL,
  `status` int(255) unsigned NOT NULL DEFAULT '0',
  `id_review_cycle` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `temp_appraisal_step_1` */

/*Table structure for table `temp_appraisal_step_2` */

DROP TABLE IF EXISTS `temp_appraisal_step_2`;

CREATE TABLE `temp_appraisal_step_2` (
  `id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `ref_id` varchar(255) NOT NULL,
  `id_user` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `temp_appraisal_step_2` */

/*Table structure for table `training_approver` */

DROP TABLE IF EXISTS `training_approver`;

CREATE TABLE `training_approver` (
  `id_training_approver` int(50) NOT NULL,
  `id_training` int(50) NOT NULL,
  `id_user` int(50) NOT NULL,
  `id_approver` int(50) NOT NULL,
  `approver` varchar(45) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_training_approver`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `training_approver` */

/*Table structure for table `training_approver_stage` */

DROP TABLE IF EXISTS `training_approver_stage`;

CREATE TABLE `training_approver_stage` (
  `id_training_approver_stage` int(50) NOT NULL,
  `id_company` int(50) NOT NULL,
  `id_approver` int(50) NOT NULL,
  `approver` varchar(45) NOT NULL,
  `stage` int(10) NOT NULL,
  PRIMARY KEY (`id_training_approver_stage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `training_approver_stage` */

/*Table structure for table `training_company_budget` */

DROP TABLE IF EXISTS `training_company_budget`;

CREATE TABLE `training_company_budget` (
  `id_training_budget` int(50) NOT NULL AUTO_INCREMENT,
  `id_company` int(50) NOT NULL,
  `id_user` int(50) NOT NULL,
  `budget` int(50) NOT NULL,
  `year` int(4) NOT NULL,
  PRIMARY KEY (`id_training_budget`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `training_company_budget` */

/*Table structure for table `training_course` */

DROP TABLE IF EXISTS `training_course`;

CREATE TABLE `training_course` (
  `id_training_course` int(50) NOT NULL,
  `id_training_vendor` int(50) NOT NULL,
  `course_code` varchar(45) NOT NULL,
  `id_company` int(50) NOT NULL,
  `title` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `objective` text,
  `requirement` text,
  `restriction` text,
  `cost` varchar(45) NOT NULL,
  PRIMARY KEY (`id_training_course`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `training_course` */

/*Table structure for table `training_date` */

DROP TABLE IF EXISTS `training_date`;

CREATE TABLE `training_date` (
  `id_training_date` int(11) NOT NULL,
  `id_training_course` int(50) NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_time` varchar(10) NOT NULL,
  `end_time` varchar(10) NOT NULL,
  PRIMARY KEY (`id_training_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `training_date` */

/*Table structure for table `training_location` */

DROP TABLE IF EXISTS `training_location`;

CREATE TABLE `training_location` (
  `id_training_location` int(50) NOT NULL,
  `id_training_course` int(50) NOT NULL,
  `location` text NOT NULL,
  PRIMARY KEY (`id_training_location`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `training_location` */

/*Table structure for table `training_vendors` */

DROP TABLE IF EXISTS `training_vendors`;

CREATE TABLE `training_vendors` (
  `id_training_vendor` int(50) NOT NULL AUTO_INCREMENT,
  `vendor_code` varchar(45) NOT NULL,
  `id_company` int(50) NOT NULL,
  `vendor_name` varchar(45) NOT NULL,
  `details` text NOT NULL,
  `logo` varchar(255) NOT NULL,
  `contact_name` varchar(45) NOT NULL,
  `contact_phone` varchar(45) NOT NULL,
  `contact_email` varchar(145) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id_training_vendor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `training_vendors` */

/*Table structure for table `trainings` */

DROP TABLE IF EXISTS `trainings`;

CREATE TABLE `trainings` (
  `id_training` int(50) NOT NULL AUTO_INCREMENT,
  `id_company` int(50) NOT NULL,
  `id_user` int(50) NOT NULL,
  `id_training_course` int(50) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_training`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trainings` */

/*Table structure for table `user_group_perms` */

DROP TABLE IF EXISTS `user_group_perms`;

CREATE TABLE `user_group_perms` (
  `id_group` int(255) unsigned NOT NULL,
  `id_perm` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_group_perms` */

/*Table structure for table `user_groups` */

DROP TABLE IF EXISTS `user_groups`;

CREATE TABLE `user_groups` (
  `id_group` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  PRIMARY KEY (`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_groups` */

/*Table structure for table `user_logs` */

DROP TABLE IF EXISTS `user_logs`;

CREATE TABLE `user_logs` (
  `id_user` int(255) unsigned NOT NULL,
  `message` text NOT NULL,
  `type` int(1) unsigned NOT NULL DEFAULT '1',
  `log_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `user_logs` */

/*Table structure for table `user_perms` */

DROP TABLE IF EXISTS `user_perms`;

CREATE TABLE `user_perms` (
  `id_user` int(255) unsigned NOT NULL,
  `id_perm` int(255) unsigned NOT NULL,
  `id_module` int(255) unsigned NOT NULL,
  `id_company` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_perms` */

/*Table structure for table `user_perms_temp` */

DROP TABLE IF EXISTS `user_perms_temp`;

CREATE TABLE `user_perms_temp` (
  `id_user` int(255) unsigned NOT NULL,
  `id_perm` int(255) unsigned NOT NULL,
  `id_creator` int(255) unsigned NOT NULL,
  `date_expire` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_module` int(255) unsigned NOT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_perms_temp` int(255) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_perms_temp`),
  KEY `id_user` (`id_user`),
  KEY `id_perm` (`id_perm`),
  KEY `date_expire` (`date_expire`),
  KEY `id_user_module` (`id_user`,`id_perm`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `user_perms_temp` */

/*Table structure for table `user_phone_numbers` */

DROP TABLE IF EXISTS `user_phone_numbers`;

CREATE TABLE `user_phone_numbers` (
  `id_user` int(255) unsigned NOT NULL,
  `phone_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_phone_numbers` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id_user` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_string` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `date_registered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_last_login` timestamp NULL DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `account_type` int(255) DEFAULT '0',
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) NOT NULL DEFAULT '0',
  `id_state` int(255) unsigned DEFAULT '0',
  `id_country` int(255) unsigned DEFAULT '0',
  `id_gender` int(1) unsigned NOT NULL,
  `id_marital_status` int(1) unsigned DEFAULT NULL,
  `dob` timestamp NULL DEFAULT NULL,
  `id_nationality` int(1) unsigned DEFAULT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_creator` int(255) unsigned NOT NULL,
  `date_probation_expire` timestamp NULL DEFAULT NULL,
  `emergency_first_name` varchar(255) DEFAULT NULL,
  `emergency_last_name` varchar(255) DEFAULT NULL,
  `emergency_address` varchar(255) DEFAULT NULL,
  `emergency_email` varchar(255) DEFAULT NULL,
  `emergency_phone_number` varchar(255) DEFAULT NULL,
  `emergency_employer` varchar(255) DEFAULT NULL,
  `emergency_work_address` varchar(255) DEFAULT NULL,
  `emergency_first_name2` varchar(255) DEFAULT NULL,
  `emergency_last_name2` varchar(255) DEFAULT NULL,
  `emergency_address2` varchar(255) DEFAULT NULL,
  `emergency_email2` varchar(255) DEFAULT NULL,
  `emergency_phone_number2` varchar(255) DEFAULT NULL,
  `emergency_employer2` varchar(255) DEFAULT NULL,
  `emergency_work_address2` varchar(255) DEFAULT NULL,
  `emergency_request_instructions` text,
  `emergency_request_date` timestamp NULL DEFAULT NULL,
  `id_company_location` int(255) unsigned NOT NULL DEFAULT '0',
  `date_termination` timestamp NULL DEFAULT NULL,
  `profile_picture_name` varchar(255) DEFAULT NULL,
  `profile_picture_uri` text,
  `id_pfs` int(255) DEFAULT NULL,
  `rsa_pin` varchar(255) DEFAULT 'NONE',
  `account_blocked` int(1) DEFAULT '0',
  PRIMARY KEY (`id_user`,`id_string`),
  UNIQUE KEY `id_user` (`id_user`),
  KEY `auth` (`email`,`password`),
  KEY `all` (`id_user`,`id_string`,`email`,`status`,`id_company`),
  KEY `id_company` (`id_company`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `users` */

/*Table structure for table `users_temp` */

DROP TABLE IF EXISTS `users_temp`;

CREATE TABLE `users_temp` (
  `id_user` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_string` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `date_registered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_modified` timestamp NULL DEFAULT NULL,
  `date_last_login` timestamp NULL DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `account_type` int(255) DEFAULT '0',
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) NOT NULL DEFAULT '0',
  `id_state` int(255) unsigned DEFAULT '0',
  `id_country` int(255) unsigned DEFAULT '0',
  `id_gender` int(1) unsigned NOT NULL,
  `id_marital_status` int(1) unsigned DEFAULT NULL,
  `dob` timestamp NULL DEFAULT NULL,
  `id_nationality` int(1) unsigned DEFAULT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_creator` int(255) unsigned NOT NULL,
  `date_probation_expire` timestamp NULL DEFAULT NULL,
  `emergency_first_name` varchar(255) DEFAULT NULL,
  `emergency_last_name` varchar(255) DEFAULT NULL,
  `emergency_address` varchar(255) DEFAULT NULL,
  `emergency_email` varchar(255) DEFAULT NULL,
  `emergency_phone_number` varchar(255) DEFAULT NULL,
  `emergency_employer` varchar(255) DEFAULT NULL,
  `emergency_work_address` varchar(255) DEFAULT NULL,
  `emergency_first_name2` varchar(255) DEFAULT NULL,
  `emergency_last_name2` varchar(255) DEFAULT NULL,
  `emergency_address2` varchar(255) DEFAULT NULL,
  `emergency_email2` varchar(255) DEFAULT NULL,
  `emergency_phone_number2` varchar(255) DEFAULT NULL,
  `emergency_employer2` varchar(255) DEFAULT NULL,
  `emergency_work_address2` varchar(255) DEFAULT NULL,
  `emergency_request_instructions` text,
  `emergency_request_date` timestamp NULL DEFAULT NULL,
  `id_company_location` int(255) unsigned NOT NULL DEFAULT '0',
  `date_termination` timestamp NULL DEFAULT NULL,
  `profile_picture_name` varchar(255) DEFAULT NULL,
  `profile_picture_uri` text,
  `id_pfs` int(255) DEFAULT NULL,
  `rsa_pin` varchar(255) DEFAULT NULL,
  `account_blocked` int(1) DEFAULT '0',
  PRIMARY KEY (`id_user`,`id_string`),
  UNIQUE KEY `id_user` (`id_user`),
  KEY `auth` (`email`,`password`),
  KEY `all` (`id_user`,`id_string`,`email`,`status`,`id_company`),
  KEY `id_company` (`id_company`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `users_temp` */

/*Table structure for table `vacation` */

DROP TABLE IF EXISTS `vacation`;

CREATE TABLE `vacation` (
  `id_vacation` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_type` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_user` int(255) unsigned NOT NULL,
  `id_task` int(100) NOT NULL,
  `description` text NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `status` enum('Pending','Approved','Declined') NOT NULL DEFAULT 'Pending',
  `no_of_days` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_vacation`),
  KEY `id_type` (`id_type`),
  KEY `id_company` (`id_company`,`id_user`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `vacation` */

/*Table structure for table `vacation_allocated` */

DROP TABLE IF EXISTS `vacation_allocated`;

CREATE TABLE `vacation_allocated` (
  `id_allocation` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_type` int(255) unsigned NOT NULL,
  `id_company` int(255) unsigned NOT NULL,
  `id_user` int(255) unsigned NOT NULL,
  `no_of_days` smallint(5) unsigned NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `recurring` enum('Yes','No') NOT NULL,
  `approver` varchar(255) NOT NULL DEFAULT 'Manager',
  PRIMARY KEY (`id_allocation`),
  KEY `id_type` (`id_type`),
  KEY `id_company` (`id_company`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `vacation_allocated` */

/*Table structure for table `vacation_allowance_approver` */

DROP TABLE IF EXISTS `vacation_allowance_approver`;

CREATE TABLE `vacation_allowance_approver` (
  `id_allowance_approver` int(255) NOT NULL AUTO_INCREMENT,
  `id_vacation_allowance_request` int(255) NOT NULL,
  `id_allowance_stage` int(255) NOT NULL,
  `id_approver` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `stage` int(255) NOT NULL,
  `status` int(255) NOT NULL,
  `id_task` int(255) NOT NULL,
  PRIMARY KEY (`id_allowance_approver`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `vacation_allowance_approver` */

/*Table structure for table `vacation_allowance_request` */

DROP TABLE IF EXISTS `vacation_allowance_request`;

CREATE TABLE `vacation_allowance_request` (
  `id_vacation_allowance_request` int(255) NOT NULL AUTO_INCREMENT,
  `id_user` int(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `year` int(5) NOT NULL,
  `status` int(1) NOT NULL,
  `date_requested` timestamp NULL DEFAULT NULL,
  `amount` int(25) NOT NULL,
  PRIMARY KEY (`id_vacation_allowance_request`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `vacation_allowance_request` */

/*Table structure for table `vacation_allowance_stages` */

DROP TABLE IF EXISTS `vacation_allowance_stages`;

CREATE TABLE `vacation_allowance_stages` (
  `id_allowance_stage` int(255) NOT NULL AUTO_INCREMENT,
  `approver` varchar(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `stage` int(255) NOT NULL,
  PRIMARY KEY (`id_allowance_stage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `vacation_allowance_stages` */

/*Table structure for table `vacation_compensation` */

DROP TABLE IF EXISTS `vacation_compensation`;

CREATE TABLE `vacation_compensation` (
  `id_vacation_compensation` int(255) NOT NULL AUTO_INCREMENT,
  `id_vacation_type` int(255) NOT NULL,
  `id_pay_structure` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `no_of_days` int(255) NOT NULL,
  PRIMARY KEY (`id_vacation_compensation`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `vacation_compensation` */

/*Table structure for table `vacation_holidays` */

DROP TABLE IF EXISTS `vacation_holidays`;

CREATE TABLE `vacation_holidays` (
  `id_holiday` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_company` int(255) unsigned NOT NULL,
  `holiday_name` varchar(255) NOT NULL,
  `holiday_date` datetime NOT NULL,
  `annual` enum('Yes','No') NOT NULL DEFAULT 'No',
  `company_locations` text NOT NULL,
  PRIMARY KEY (`id_holiday`),
  KEY `id_company` (`id_company`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `vacation_holidays` */

/*Table structure for table `vacation_plan` */

DROP TABLE IF EXISTS `vacation_plan`;

CREATE TABLE `vacation_plan` (
  `id_vacation_plan` int(255) NOT NULL AUTO_INCREMENT,
  `id_plan_request` int(255) NOT NULL,
  `id_type` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `no_of_days` int(255) NOT NULL,
  `month_year` varchar(25) NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `year` int(5) NOT NULL,
  `status` varchar(15) NOT NULL,
  PRIMARY KEY (`id_vacation_plan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `vacation_plan` */

/*Table structure for table `vacation_plan_approver` */

DROP TABLE IF EXISTS `vacation_plan_approver`;

CREATE TABLE `vacation_plan_approver` (
  `id_plan_approver` int(255) NOT NULL AUTO_INCREMENT,
  `id_vacation_plan` int(255) NOT NULL,
  `id_request_stage` int(255) NOT NULL,
  `id_approver` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `stage` int(255) NOT NULL,
  `status` int(255) NOT NULL,
  `id_task` int(255) NOT NULL,
  PRIMARY KEY (`id_plan_approver`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `vacation_plan_approver` */

/*Table structure for table `vacation_plan_request` */

DROP TABLE IF EXISTS `vacation_plan_request`;

CREATE TABLE `vacation_plan_request` (
  `id_plan_request` int(255) NOT NULL AUTO_INCREMENT,
  `id_company` int(255) NOT NULL,
  `period` varchar(255) NOT NULL,
  `id_type` int(255) NOT NULL,
  `due_date` timestamp NULL DEFAULT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id_plan_request`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `vacation_plan_request` */

/*Table structure for table `vacation_plan_stages` */

DROP TABLE IF EXISTS `vacation_plan_stages`;

CREATE TABLE `vacation_plan_stages` (
  `id_plan_stage` int(255) NOT NULL AUTO_INCREMENT,
  `approver` varchar(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `stage` int(255) NOT NULL,
  PRIMARY KEY (`id_plan_stage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `vacation_plan_stages` */

/*Table structure for table `vacation_planner_report_history` */

DROP TABLE IF EXISTS `vacation_planner_report_history`;

CREATE TABLE `vacation_planner_report_history` (
  `id_vacation_planner_history` int(55) NOT NULL AUTO_INCREMENT,
  `id_company` int(55) NOT NULL,
  `year` int(4) NOT NULL,
  `id_user` int(55) NOT NULL,
  `date_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_vacation_planner_history`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `vacation_planner_report_history` */

/*Table structure for table `vacation_request_approver` */

DROP TABLE IF EXISTS `vacation_request_approver`;

CREATE TABLE `vacation_request_approver` (
  `id_request_approver` int(255) NOT NULL AUTO_INCREMENT,
  `id_approver` int(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `id_vacation` int(255) NOT NULL,
  `stage` int(255) NOT NULL,
  `id_task` int(255) NOT NULL,
  `status` int(255) NOT NULL,
  PRIMARY KEY (`id_request_approver`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `vacation_request_approver` */

/*Table structure for table `vacation_request_stages` */

DROP TABLE IF EXISTS `vacation_request_stages`;

CREATE TABLE `vacation_request_stages` (
  `id_request_stage` int(255) NOT NULL AUTO_INCREMENT,
  `approver` varchar(255) NOT NULL,
  `id_user` int(255) NOT NULL,
  `id_company` int(255) NOT NULL,
  `stage` int(255) NOT NULL,
  PRIMARY KEY (`id_request_stage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `vacation_request_stages` */

/*Table structure for table `vacation_restricted_period` */

DROP TABLE IF EXISTS `vacation_restricted_period`;

CREATE TABLE `vacation_restricted_period` (
  `id_restricted_period` int(25) NOT NULL AUTO_INCREMENT,
  `id_vacation_type` int(55) unsigned NOT NULL,
  `id_company` int(25) unsigned NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_restricted_period`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `vacation_restricted_period` */

/*Table structure for table `vacation_types` */

DROP TABLE IF EXISTS `vacation_types`;

CREATE TABLE `vacation_types` (
  `id_type` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `carry_allowed` smallint(5) unsigned NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `id_company` int(255) unsigned NOT NULL,
  PRIMARY KEY (`id_type`),
  KEY `id_company` (`id_company`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `vacation_types` */

/*Table structure for table `checkin_view` */

DROP TABLE IF EXISTS `checkin_view`;

/*!50001 DROP VIEW IF EXISTS `checkin_view` */;
/*!50001 DROP TABLE IF EXISTS `checkin_view` */;

/*!50001 CREATE TABLE  `checkin_view`(
 `userid` int(11) ,
 `checkdate` varchar(10) ,
 `checktime` varchar(9) ,
 `checktype` varchar(1) ,
 `id_user` int(25) unsigned 
)*/;

/*Table structure for table `checkin_view1` */

DROP TABLE IF EXISTS `checkin_view1`;

/*!50001 DROP VIEW IF EXISTS `checkin_view1` */;
/*!50001 DROP TABLE IF EXISTS `checkin_view1` */;

/*!50001 CREATE TABLE  `checkin_view1`(
 `userid` int(11) ,
 `checkdate` varchar(10) ,
 `checktime` varchar(9) ,
 `checktype` varchar(1) ,
 `id_user` int(25) unsigned 
)*/;

/*Table structure for table `checkout_view` */

DROP TABLE IF EXISTS `checkout_view`;

/*!50001 DROP VIEW IF EXISTS `checkout_view` */;
/*!50001 DROP TABLE IF EXISTS `checkout_view` */;

/*!50001 CREATE TABLE  `checkout_view`(
 `userid` int(11) ,
 `checkdate` varchar(10) ,
 `checktime` varchar(9) ,
 `checktype` varchar(1) ,
 `id_user` int(25) unsigned 
)*/;

/*Table structure for table `payroll_employees_grosspay_vi` */

DROP TABLE IF EXISTS `payroll_employees_grosspay_vi`;

/*!50001 DROP VIEW IF EXISTS `payroll_employees_grosspay_vi` */;
/*!50001 DROP TABLE IF EXISTS `payroll_employees_grosspay_vi` */;

/*!50001 CREATE TABLE  `payroll_employees_grosspay_vi`(
 `id_user` int(255) ,
 `grosspay` double ,
 `id_schedule` varchar(255) 
)*/;

/*View structure for view checkin_view */

/*!50001 DROP TABLE IF EXISTS `checkin_view` */;
/*!50001 DROP VIEW IF EXISTS `checkin_view` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `checkin_view` AS select `c`.`userid` AS `userid`,substr(`c`.`checktime`,1,10) AS `checkdate`,substr(`c`.`checktime`,11,20) AS `checktime`,`c`.`checktype` AS `checktype`,`a`.`id_user` AS `id_user` from (`checkinout` `c` left join `attendance_user_info` `a` on((`c`.`userid` = `a`.`user_id`))) where (`c`.`checktype` = 0) */;

/*View structure for view checkin_view1 */

/*!50001 DROP TABLE IF EXISTS `checkin_view1` */;
/*!50001 DROP VIEW IF EXISTS `checkin_view1` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `checkin_view1` AS select `c`.`userid` AS `userid`,substr(`c`.`checktime`,1,10) AS `checkdate`,substr(`c`.`checktime`,11,20) AS `checktime`,`c`.`checktype` AS `checktype`,`a`.`id_user` AS `id_user` from (`checkinout` `c` left join `attendance_user_info` `a` on((`c`.`userid` = `a`.`user_id`))) where (`c`.`checktype` = 0) */;

/*View structure for view checkout_view */

/*!50001 DROP TABLE IF EXISTS `checkout_view` */;
/*!50001 DROP VIEW IF EXISTS `checkout_view` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `checkout_view` AS select `c`.`userid` AS `userid`,substr(`c`.`checktime`,1,10) AS `checkdate`,substr(`c`.`checktime`,11,20) AS `checktime`,`c`.`checktype` AS `checktype`,`a`.`id_user` AS `id_user` from (`checkinout` `c` left join `attendance_user_info` `a` on((`c`.`userid` = `a`.`user_id`))) where (`c`.`checktype` = 1) */;

/*View structure for view payroll_employees_grosspay_vi */

/*!50001 DROP TABLE IF EXISTS `payroll_employees_grosspay_vi` */;
/*!50001 DROP VIEW IF EXISTS `payroll_employees_grosspay_vi` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `payroll_employees_grosspay_vi` AS select `payroll_compensation_employee_allowance`.`id_user` AS `id_user`,sum(`payroll_compensation_employee_allowance`.`amount`) AS `grosspay`,`payroll_compensation_employee_allowance`.`id_schedule` AS `id_schedule` from `payroll_compensation_employee_allowance` group by `payroll_compensation_employee_allowance`.`id_user`,`payroll_compensation_employee_allowance`.`id_schedule` */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
