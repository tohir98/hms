<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$isOnsite = file_exists(APP_ROOT . 'application/config/.ONSITE_MODE');

$jobby->add('onpremiseUpdate', array(
    'command' => 'php ' . APPROOT . 'vendor/bin/robo --auto-update op:update',
    'schedule' => '0 0 * * *',
    'output' => 'logs/onpremiseUpdate.log',
    'enabled' => $isOnsite,
));

$jobby->add('onpremiseBackup', array(
    'command' => 'php ' . APPROOT . 'vendor/bin/robo --dir="/backup" op:backup',
    'schedule' => '0 3 * * *',
    'output' => 'logs/onpremiseBackup.log',
    'enabled' => $isOnsite,
));


