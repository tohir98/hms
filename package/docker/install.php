<?php

define('APP_ROOT', __DIR__ . '/public_html');

if (!($licenceKey = getenv('TB_LICENSE')) || !($package = strtolower(getenv('TB_PACKAGE')))) {
    echo 'You must specify both license key and package to download. Specify License key with -e TB_LICENSE=KEY and -e TB_PACKAGE=Package', PHP_EOL;
    exit(1);
}

if (!is_dir('/backup')) {
    eol('Backup Volume is missing');
    exit(2);
}

$server = trim(getenv('TB_SERVER'), '/\\') ? : 'https://portal.talentbase.ng';
$url = "{$server}/release/download/{$package}/?version=latest&key=" . urlencode($licenceKey);
eol('Downloading from URL: ' . $url);
$handle = fopen($url, 'r');
if (!$handle) {
    eol('Download of package failed');
    exit(3);
}
$dest = APP_ROOT . '/package.zip';
$saved = file_put_contents($dest, $handle);

if (!$saved) {
    eol("Could not save file");
    exit(3);
}

eol("Package downloaded to {$dest}");

echo shell_exec('unzip -q -o ' . $dest);
@unlink($dest);

if (($dbConfig = getEnvDbConfig())) {
    $host = ($dbConfig['port'] && $dbConfig['port'] != 3306) ? "{$dbConfig['host']}:{$dbConfig['port']}" : $dbConfig['host'];
    $content = <<<DBCONFIG
<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

if (!defined('__LOCAL_')) {
    define('__LOCAL_', 22);
}

\$active_group = 'default';
\$active_record = TRUE;

\$db = array(
   'default' => array(
       'hostname' => '{$host}',
       'username' => '{$dbConfig['username']}',
       'password' => '{$dbConfig['password']}',
       'database' => '{$dbConfig['name']}',
       'dbdriver' => 'mysqli',
       'dbprefix' => '',
       'pconnect' => false,
       'char_set' => 'utf8',
       'dbcollat' => 'utf8_general_ci',
       'autoinit' => true,
   ),
);

DBCONFIG;

    $dbFile = APP_ROOT . '/application/config/database.php';
    eol('Writing database configuration... to file:', $dbFile);
    file_put_contents($dbFile, $content);
    eol('Done writing DB config.');
}

function eol() {
    echo join(' ', func_get_args()), PHP_EOL;
}

function getEnvDbConfig() {
    if (($host = getenv('TB_DB_HOST')) && ($user = getenv('TB_DB_USER')) && ($dbname = getenv('TB_DB_NAME'))) {
        return array(
            'host' => $host,
            'username' => $user,
            'password' => getenv('TB_DB_PASS'),
            'name' => $dbname,
            'port' => getenv('TB_DB_PORT'),
        );
    }
    return null;
}
