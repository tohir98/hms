#!/bin/bash
set -e

if [ ! -f "/home/opuser/public_html/index.php" ]; then
	echo "Application missing, running installer"
	tb_install.sh
	LAST_EXEC=$?
	if [ $LAST_EXEC != 0 ]; then
		echo >&2 'Could not complete install.sh'
		exit $LAST_EXEC
	fi
fi

#echo "192.168.59.103	portal.tb.com.ng" >> /etc/hosts
#run cron
/etc/init.d/cron start

exec "$@"
