CONTAINER_NAME=tbv1test
docker stop $CONTAINER_NAME
docker rm $CONTAINER_NAME

if [ -n $1 ]; then
	TB_SERVER=$1
else
	TB_SERVER="portal.talentbase.ng"
fi
docker run -p 9000:80 --name=$CONTAINER_NAME -e TB_LICENSE=7Z0ZHQ5WEDNNH7ZKXHCHZMQM -e TB_PACKAGE=lite -e TB_SERVER=$TB_SERVER -v /media/tb/talentbasev01/package/docker/volume:/backup --link some-mysql:mysql -d torilogbon/talentbase
docker logs -f $CONTAINER_NAME
