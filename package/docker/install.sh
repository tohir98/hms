#!/bin/bash
echo "192.168.59.103	portal.tb.com.ng" >> /etc/hosts

echo "Creating web directory"
mkdir /home/opuser/public_html
cd /home/opuser/public_html

echo  "Processing database"
#is a mysql container linked to this
if [ -n "$MYSQL_PORT_3306_TCP" ]; then
	TB_DBI_HOST='mysql'
fi

# if we're linked to MySQL, and we're using the root user, and our linked
# container has a default "root" password set up and passed through... :)
: ${TB_DBI_USER:=root}
if [ "$TB_DBI_USER" = 'root' ]; then
	: ${TB_DBI_PASS:=$MYSQL_ENV_MYSQL_ROOT_PASSWORD}
	if [ -z "$TB_DBI_NAME" ]; then
		TB_DBI_NAME="talentbasev1"
	fi
fi



if [ -n "$TB_DBI_NAME" -a -n "$TB_DBI_USER" -a "$TB_DBI_PASS" -a "$TB_DBI_HOST" ]; then
	echo "Creating database "
	echo "Warning: This will drop any existing database named $TB_DBI_NAME"
	
	export TB_DB_PASS=`openssl rand -base64 14 | md5sum | cut -b 1-12`
	TB_APP_DB_USER="opuser_`openssl rand -base64 14 | md5sum | cut -b 1-6`"
	
	echo "DROP DATABASE IF EXISTS $TB_DBI_NAME;" \
		"CREATE DATABASE  $TB_DBI_NAME CHARACTER SET = 'utf8';" \
		"GRANT ALL ON $TB_DBI_NAME.* TO '$TB_APP_DB_USER' IDENTIFIED BY '$TB_DB_PASS';" | mysql -u$TB_DBI_USER -p"$TB_DBI_PASS" -h$TB_DBI_HOST 
	
	if [ $? != 0 ]; then
		echo "DB could not be created properly"
		exit 1
	fi
	
	export TB_DB_NAME=$TB_DBI_NAME
	export TB_DB_USER=$TB_APP_DB_USER
	export TB_DB_HOST=$TB_DBI_HOST
	export TB_DB_PORT=3306
fi
#END DB Processing
#run installer
php /home/opuser/install.php

LAST_EXEC=$?

if [ $LAST_EXEC != 0 ]; then
	echo >&2 'Could not complete php installer'
	exit $LAST_EXEC
fi


mkdir -p /backup/composer/cache
echo "Downloading  composer"
curl -sS https://getcomposer.org/installer | php
export COMPOSER_HOME=/backup/composer
chmod -R 0777 $COMPOSER_HOME

#installing composer
php composer.phar install 

#run db migration
if [ -f "application/config/database.php" ]; then
	echo "Performing Database migration and preloading"
	./vendor/bin/robo db:migrate --start=0 default
	if [ $? != 0 ]; then
		echo "Database could not be installed"
		exit 2
	fi
fi

#set cron jobs
echo "TB_ENV=production" >> "/var/spool/cron/crontabs/opuser"
echo "COMPOSER_HOME=/backup/composer" >> "/var/spool/cron/crontabs/opuser"
echo "* * * * * cd /home/wwwuser/public_html/cron && php cron.php 1>> /dev/null 2>&1" >> "/var/spool/cron/crontabs/opuser"

chown -R opuser:opuser /home/opuser/public_html
chown -R opuser:opuser /backup/composer

echo "IncludeOptional conf-enabled/*.conf" >> /etc/apache2/apache2.conf
echo "IncludeOptional sites-enabled/*.conf" >> /etc/apache2/apache2.conf

rm /etc/apache2/sites-enabled/000-default.conf
rm /etc/apache2/conf-enabled/*.conf