<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace packager;

use Robo\Result;
use Robo\Task\BaseDirTask;
use Robo\Task\Shared\TaskException;

/**
 * Description of MyCopyTasks
 *
 * @author JosephT
 */
class MyCopyTask extends BaseDirTask {

    public function run() {
        foreach ($this->dirs as $src => $dst) {
            $this->copyDir($src, $dst);
            $this->printTaskInfo("Copied from <info>$src</info> to <info>$dst</info>");
        }
        return Result::success($this);
    }

    private function copyDir($src, $dst) {
        $dir = @opendir($src);
        if (false === $dir) {
            throw new TaskException(__CLASS__, "Cannot open source directory '" . $src . "'");
        }

        @mkdir($dst, 0755, true);
        while (false !== ($file = readdir($dir))) {
            if (($file !== '.') && ($file !== '..')) {
                $srcFile = $src . '/' . $file;
                $destFile = $dst . '/' . $file;
                if (is_dir($srcFile)) {
                    $this->copyDir($srcFile, $destFile);
                } else {
                    $destDir = dirname($destFile);

                    if (!is_dir($destDir)) {
                        mkdir($destDir, 0755, true);
                    }

                    copy($srcFile, $destFile);
                }
            }
        }
        closedir($dir);
    }

}
