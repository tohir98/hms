<?php

/**
 * Packager file for packaging onsite versions of the application
 */
require_once __DIR__ . '/../vendor/autoload.php';
if (!defined('BASEPATH')) {
    define('BASEPATH', realpath(__DIR__ . '/../system'));
}

require_once APP_ROOT . 'application/libraries/zipper.php';
require_once __DIR__ . '/MyCopyTasks.php';
require_once __DIR__ . '/OnsiteVersions.php';
require_once __DIR__ . '/PackagerTasks.php';
require_once __DIR__ . '/PackageBuilder.php';
