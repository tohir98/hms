<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace packager;

use Robo\Tasks;

/**
 * Description of Tasks
 *
 * @author JosephT
 * @method self serverUrl(string $serverUrl) set the server ur to use in package info
 */
class PackagerTasks extends Tasks {

    use \Robo\Task\Shared\DynamicConfig;

    protected $targetDir;
    protected $temp;
    private $versionBump = true;
    protected $serverUrl = 'https://portal.talentbase.ng';

    public function __construct($targetDir = null) {
        if (!$targetDir || !is_dir($targetDir) || !is_writable($targetDir)) {
            $targetDir = __DIR__ . '/dist';
        }

        if (!is_dir($targetDir)) {
            mkdir($targetDir, 0755, true);
        }

        $this->targetDir = $targetDir;
        $this->temp = sys_get_temp_dir() . '/tbpackageza1';
    }

    public function cleanTempDir() {
        if (is_dir($this->temp)) {
            return $this->taskDeleteDir($this->temp)->run();
        }
        return true;
    }

    public function makeTmpDir() {
        return mkdir($this->temp, 0755, true);
    }

    public function build($package = null, $publish = true) {
        $this->cleanTempDir();
        if (!$this->makeTmpDir()) {
            throw new Exception('error completing build.');
        }
        if (!$package) {
            $package = array_keys(OnsiteVersions::getModules());
        }

        if (!is_array($package)) {
            $package = preg_split('/[,\s]+/', $package);
        }

        foreach ($package as $aPackage) {
            $builder = new PackageBuilder($aPackage, $this->temp, $this->targetDir, $this->serverUrl);
            $builder->build($publish);
        }

        if ($this->versionBump) {
            $this->versionBump();
        }

        return true;
    }

    public function getTargetDir() {
        return $this->targetDir;
    }

    public function getTempDir() {
        return $this->temp;
    }

    public function setVersionBump($bool = true) {
        $this->versionBump = $bool;
        return $this;
    }

    public function versionBump($newVersion = null) {

        $previousVersion = OnsiteVersions::version();
        if (!$newVersion) {
            $versionParts = explode('.', $previousVersion);
            if(!isset($versionParts[2])){
                $versionParts[2] = 0;
            }
            $versionParts[2] ++;
            $newVersion = implode('.', $versionParts);
        }
        
        self::updateVersionFile($newVersion);
    }

    public static function updateVersionFile($newVersion) {
        file_put_contents(__DIR__ . '/BUILD_VERSION', $newVersion);
    }

}
