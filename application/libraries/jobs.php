<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Talentbase
 * Jobs library
 * 
 * @category   Library
 * @package    Jobs
 * @subpackage Jobs
 * @author     Chuks O. <chuks.olloh@hired.com.ng>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

class Jobs {
	
	/**
     * Codeigniter instance
     * 
     * @access private
     * @var object
     */
    private $CI;
	
	public function __construct() {
		
		// Load CI object
        $this->CI = get_instance();
		
	}
	
	/**
     * Reference array for job status
     * 
     * @access private
     * @var object
     */
	public function get_ref_arr() {
		
		return array (
			RECRUITING_STATUS_CANCELLED => 'Cancelled',
			RECRUITING_STATUS_PUBLISHED => 'Published',
			RECRUITING_STATUS_PENDING => 'Pending',
			RECRUITING_STATUS_DRAFT => 'Draft',
			RECRUITING_STATUS_CLOSED => 'Closed'
		);
		
	} // End func get_ref_arr
	
}
