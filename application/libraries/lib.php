<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of lib
 *
 * @author JosephT
 * 
 * @property CI_Loader $load
 * @property CI_DB_active_record $db
 * @property user_auth $user_auth Description
 */
abstract class lib {

    public function __construct() {
        $this->load->database();
    }

    //put your code here

    public function __get($name) {
        return get_instance()->$name;
    }

    protected final function _loadAdapters($dir, array &$container, array $suffices = ['']) {
        //$dir = __DIR__ . '/payment/adapters';
        $reader = new DirectoryIterator($dir);
        foreach ($reader as $providerDir) {
            if ($providerDir->isDir() && !$providerDir->isDot()) {

                foreach ($suffices as $aSuffix) {
                    $exists = file_exists($providerClassFile = $providerDir->getPathname()
                            . '/'
                            . $providerDir->getBasename()
                            . $aSuffix
                            . '.php');
                    
                    if ($exists) {
                        include_once realpath($providerClassFile);
                        $providerClass = $providerDir->getBasename() . $aSuffix;
                        if (class_exists($providerClass) && (call_user_func(array($providerClass, 'active')))) {
                            $id = call_user_func(array($providerClass, 'getId'));
                            $providerData = array(
                                'name' => call_user_func(array($providerClass, 'getName')),
                                'class' => $providerClass,
                                'dir' => $providerDir->getPathname(),
                            );
                            $container[$id] = $providerData;
                        } else {
                            log_message('error', "{$providerDir->getBasename()} class does not exist.");
                        }
                    }
                }
            }
        }
    }

}
