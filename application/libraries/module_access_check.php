<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of module_access_check
 *
 * @author JosephT
 * @property subdomain $subdomain 
 */
class module_access_check {
    //put your code here

    /**
     *
     * @var CI_Controller
     */
    private $CI;
    private $skipChecks = array('login', 'tbadmin', 'forgot_password', 'admin');
    private $suspectRemap = array(
        'compensation' => 'payroll',
        //'document_center' => 'downloads'
    );

    public function __construct() {
        $this->CI = get_instance();
        $this->CI->load->library(['user_auth', 'uri', 'subdomain']);
        $this->CI->load->helper('url');
        $this->initCheck();
    }

    private function initCheck() {
        //check module
        $moduleSuspect = $this->CI->uri->segments[1];
        //skip this module.
        if (!$moduleSuspect || in_array($moduleSuspect, $this->skipChecks)) {
            return;
        }

        if (array_key_exists($moduleSuspect, $this->suspectRemap)) {
            $moduleSuspect = $this->suspectRemap[$moduleSuspect];
        }

        //die($moduleSuspect);
        $this->CI->load->database();
        /* @var $db CI_DB_active_record */
        $db = $this->CI->db;
        $row = $db->get_where('modules', array(
                    'id_string' => $moduleSuspect
                        ), 1)->result();

        if (empty($row)) {
            return;
        }

        $module = current($row);
        if ($module->requires_login) {
            $this->CI->user_auth->check_login();
        }


        $companyId = $this->CI->subdomain->get_company_id();
        $this->CI->load->model('company/company_model', 'company');
        /* @var $company Company_model */
        $company = $this->CI->company;

        if (!$company->module_enabled($companyId, $module->id_module)) {
            log_message('error', 'Attempted to access module not allowed: Company- ' . $companyId . ' Module : ' . $module->id_string);
            redirect(site_url('user/access_denied'), 'refresh');
        }
    }

}
