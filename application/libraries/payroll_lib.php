<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Talentbase
 * Tasks library (for all users)
 * 
 * @category   Library
 * @package    Payroll
 * @subpackage Payroll
 * @author     Obinna Ukpabi <obinna.ukpabi@talentbase.ng>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

class payroll_lib {
    
    /**
     * Codeigniter instance
     * 
     * @access private
     * @var object
     */
    private $CI;
    
    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        // Load CI object
        $this->CI = get_instance();
        
		// Load model
        $this->CI->load->model('payroll/payroll_model');
    }

    /**
     * Check if payroll id is valid
     *
     * @access public
	 * @param int $id_user
     * @return mixed (bool | array)
     **/
    public function valid_payroll_code($id_payroll)
    {

        $result = $this->CI->payroll_model->validate_payroll_id($id_payroll);
        return $result;
		
    } // End func valid_payroll_code
	



    /**
     * Get active tasks by user id
     *
     * @access public
     * @param int $id_user
     * @return mixed (bool | array)
     **/
    public function valid_payslip_id($id_payslip)
    {

        $result = $this->CI->payroll_model->validate_payslip_id($id_payslip);
        return $result;
        
    } // End func valid_payroll_code
	
    
}
