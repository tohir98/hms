<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Talentbase
 * Admin authentication library
 * 
 * @category   Library
 * @package    Admin
 * @subpackage Authentication
 * @author     Obinna Ukpabi <obinna.ukpabi@talentbase.ng>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 * @property CI_DB_active_record $db database.
 */
class Admin_auth {

    private $ad_prefix = '_tb_adm_';

    /**
     * User email
     * 
     * @access private
     * @var string
     */
    private $email_user;

    /**
     * User id
     * 
     * @access private
     * @var int
     */
    private $id_admin;

    /**
     * Codeigniter instance
     * 
     * @access private
     * @var object
     */
    private $CI;

    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        // Load CI object
        $this->CI = get_instance();

        // Load libraries
        $this->CI->load->library('session');

        // Load models
        $this->CI->load->model('tb/admin_model');

        // Load helper
        $this->CI->load->helper('url');

        // Set user email value form sessions
        $this->email_user = $this->CI->session->userdata($this->ad_prefix . 'email');
        $this->id_admin = $this->CI->session->userdata($this->ad_prefix . 'id_admin');

        $this->statuses = array(
            USER_STATUS_INACTIVE => 'Inactive',
            USER_STATUS_ACTIVE => 'Active',
            USER_STATUS_SUSPENDED => 'Suspended',
            USER_STATUS_TERMINATED => 'Terminated',
            USER_STATUS_PROBATION_ACCESS => 'Probation/Access',
            USER_STATUS_ACTIVE_NO_ACCESS => 'Active/No access',
            USER_STATUS_PROBATION_NO_ACCESS => 'Probation/No access'
        );

        $this->db = $this->CI->load->database('', true);
    }

    /**
     * Login method
     *
     * @access public
     * @param string $email
     * @param string $password
     * @return mixed (bool | array)
     * */
    public function login($email, $password) {

        $password = $this->encrypt($password);

        // Fetch user data from database by email and password
        $result = $this->CI->admin_model->fetch_account(array('email' => $email, 'password' => $password));

        if (!$result) {
            // User does not exists
            return FALSE;
        }

        // Define params
        $status = $result[0]->status;

        // Check if user status is ACTIVE
        if ($status == USER_STATUS_ACTIVE or $status == USER_STATUS_PROBATION_ACCESS) {

            $key = sha1($result[0]->email . '_' . $status);

            // Build user session array
            $session_vars = array(
                // More session variables to be added later here.
                $this->ad_prefix . 'id_admin' => $result[0]->id_admin,
                $this->ad_prefix . 'email' => $result[0]->email,
                $this->ad_prefix . 'status' => $status,
                $this->ad_prefix . 'display_name' => $result[0]->first_name,
                $this->ad_prefix . 'last_name' => $result[0]->last_name,
                $this->ad_prefix . 'id_role' => $result[0]->id_role,
                $this->ad_prefix . 'k' => $key
            );

            // Add user record details to session
            $this->CI->session->set_userdata($session_vars);
        }

        // Return array with user data
        return array(
            "status" => $status
        );
    }

// End func login

    /**
     * Logout method
     *
     * @access public
     * @return void
     * */
    public function logout() {
        // Destroy current user session
        $this->CI->session->sess_destroy();
    }

// End func logout

    /**
     * Redirect to login page if user not logged in.
     * 
     * @access public
     * @return void
     */
    public function check_login() {

        if (!$this->logged_in()) {
            redirect(site_url('tbadmin/login'), 'refresh');
        }
    }

    /**
     * Check if user logged in
     *
     * @access public
     * @return bool
     * */
    public function logged_in() {

        $cdata = array(
            'email' => $this->CI->session->userdata($this->ad_prefix . 'email'),
            'status' => $this->CI->session->userdata($this->ad_prefix . 'status')
        );

        foreach ($cdata as $data) {
            if (trim($data) == '') {
                return false;
            }
        }

        $s_k = $this->CI->session->userdata($this->ad_prefix . 'k');
        $c_k = sha1($cdata['email'] . '_' . $cdata['status']);

        if ($s_k != $c_k) {
            return false;
        }

        return true;
    }

// End func logged_in

    /**
     * Get session variable value assigned to user. 
     * 
     * @access public
     * @param string $item
     * @return mixed (bool | string)
     */
    public function get($item) {

        if (!$this->logged_in()) {
            return false;
        }

        return $this->CI->session->userdata($this->ad_prefix . $item);
    }

// End func get

    /**
     * Encrypt string to sha1 
     * 
     * @access public
     * @param string $str
     * @return string
     */
    public function encrypt($str) {
        return sha1($str);
    }

// End func encrypt

    /**
     * Redirect to user's access denied page, if user have not permission.
     * 
     * @access public 
     * @param type $id_perm 
     * @return void
     */
    public function check_perm($id_perm) {
        if (!$this->have_perm($id_perm)) {
            redirect(site_url('tbadmin/access_denied'), 'refesh');
        }
    }

    /**
     * Check if user has permission 
     * 
     * @access public
     * @param int|array|string $id_perm
     * @param int id_admin
     * @return bool
     */
    public function have_perm($id_perm) {

        $ret = false;
        if(is_string($id_perm) && strpos($id_perm, ':')){
            $parts = explode(':', $id_perm, 2);
            $id_perm = ['menu' => $parts[0], 'permission' => $parts[1]];
        }

        if (is_array($id_perm)) {
            assert(isset($id_perm['menu'], $id_perm['permission']), 'menu and permission must be set');
            $id_role = $this->get('id_role');
            $roleDef = $this->db->select()
                    ->from('tb_roles')
                    ->where('id_role', $id_role)
                    ->get()
                    ->result();
            
            $roleAuthLevel = (int) current($roleDef)->auth_level;

            $roleRes = $this->db->select()
                    ->from('tb_roles AS r')
                    ->join('tb_role_perms as rp', 'rp.id_role=r.id_role')
                    ->join('tb_menu as m', 'm.id_menu=rp.id_menu')
                    ->where('r.auth_level <= ' . $roleAuthLevel, null,false)
                    ->where(array(
                        'rp.id_string' => $id_perm['permission'],
                        'm.id_string' => $id_perm['menu'],
                    ))
                    ->get()
                    ->result_array()
            ;
            
            $ret = !empty($roleRes);
        } else {
            if (!is_numeric($id_perm) or ! is_numeric($this->id_admin)) {
                return false;
            }

            $ret = $this->CI->admin_model->get_admin_perm(
                    array(
                        'id_admin' => $this->id_admin,
                        'id_perm' => $id_perm,
            ));
        }


        return $ret;
    }

// End func have_perm
}

/* End of file admin_auth.php */
/* Location: ./application/libraries/admin_auth.php */
