<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once 'page_nav.php';

/**
 * Talentbase
 * Admin Nav library
 * 
 * @category   Library
 * @package    Admin
 * @subpackage Nav
 * @author     Obinna Ukpabi <obinna.ukpabi@talentbase.ng>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */
class Admin_nav extends Page_nav {

    private $id_admin;

    public function __construct() {
        parent::__construct();
        // Load libraries
        $this->CI->load->library('admin_auth');
        // Load model
        $this->CI->load->model('tb/admin_model');
        // Load helper
        $this->CI->load->helper('admin_nav');
        $this->id_admin = $this->CI->admin_auth->get("id_admin");
    }

    /**
     * get_assoc method
     *
     * @access public
     * @return mixed (bool | array)
     * */
    public function get_assoc() {
        if (!$this->id_admin) {
            return false;
        }
        // Fetch user permissions
        $result = $this->CI->admin_model->fetch_admin_groups_modules_perms($this->id_admin);
        if (!$result) {
            return false;
        }

        $a = array();

        foreach ($result as $row) {
            if ($row['menu_subject'] != '') {
                $a[$row['menu_subject']][] = $row;
            }
        }

        return $a;
    }

    public function get_top_menu() {
        return array(
            'dashboard_url' => 'tbadmin/dashboard',
            'logout_url' => site_url('tbadmin/logout'),
            'display_name' => $this->CI->admin_auth->get('display_name')
        );
    }
   

    public function get_user_link() {
        return 'tbadmin';
    }

    public function company_name() {
        return TALENTBASE_BUSINESS_NAME;
    }
// End func run_page
}

/* End of file admin_nav.php */
/* Location: ./application/libraries/admin_nav.php */
