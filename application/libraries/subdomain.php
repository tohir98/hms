<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Talentbase
 * Company subdomain id lib
 * 
 * @category   Library
 * @package    Company
 * @subpackage Subdomain
 * @author     David A. <tokernel@gmail.com>
 * @copyright  Copyright Â© 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */
class subdomain {

    /**
     * Codeigniter instance
     * 
     * @access private
     * @var object
     */
    private $CI;

    /**
     * Detected subdomain 
     * 
     * @access private
     * @var string
     */
    private $subdomain;
    private $child_subdomain;
    private $company;
    private $except_ids = array(
        'tb',
        'testbase',
        'hired',
        'studenthub',
        'talentbase',
        'testhired',
        'www',
        'portal',
    );

    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        // Load CI object
        $this->CI = get_instance();

        $this->CI->load->helper('url');
        $this->CI->load->library('uri');
        // Load company model
        $this->CI->load->model('company/company_model', 'c_model');

        $this->init();
    }

// End func __construct

    /**
     * Initialize subdomain detection
     *
     * @access public
     * @param string $email
     * @param string $password
     * @return mixed (bool | array)
     * */
    private function init() {

        $this->init_subdomain();

        if (!$this->subdomain) {

            if ($this->CI->uri->segment(1) == '') {
                redirect(site_url('home'));
            }

            if ($this->CI->uri->segment(1) != 'home') {
                return true;
            }
            return;
        }

        $this->load_company();

        if (empty($this->company)) {
            show_error('The account you\'re looking for does not exist. Please check the address and try again. If you need further assistance, please contact our help desk via: ' . TALENTBASE_SUPPORT_EMAIL . '<br/>', 404, 'Account not Found');
        }
    }

    private function init_subdomain() {

        //single site mode.
        if (TB_ON_SITE_MODE) {
            return $this->subdomain = true;
        }

        $subdomain_arr = explode('.', $_SERVER['HTTP_HOST']);
        $subdomain_name = $subdomain_arr[0];

        // Assume www.{subdomain}
        if (isset($subdomain_arr[1])) {
            if ($subdomain_arr[0] == 'www' and ! in_array($subdomain_arr[1], $this->except_ids)) {
                $this->subdomain = $subdomain_arr[1];
                return true;
            }
        }

        // Landing page.
        if (in_array($subdomain_name, $this->except_ids)) {
            $this->subdomain = false;
        } else {
            $this->subdomain = $subdomain_name;
        }
    }

// End func init_subdomain

    public function subdomain_name() {

        $subdomain_arr = explode('.', $_SERVER['HTTP_HOST']);
        return $subdomain_arr[1];
    }

// End func subdomain_name

    private function load_company() {

        if (!$this->subdomain) {
            $this->company = [];
            return false;
        }

        if (TB_ON_SITE_MODE) {
            $db = $this->CI->c_model->db;
            /* @var $db CI_DB_active_record */
            $this->company = (array) $db->get_where('companies', array('status' => 1), 1)->result();
            return !empty($this->company);
        }

        $this->company = $this->CI->c_model->load_company($this->subdomain, null) ? : array();

        if (!empty($this->company)) {
            //$this->child_subdomain = $this->company[0]->child_subdomain;
            $company = $this->company[0];
            //if company has parent and parent is not same as company, then go to parent.
            //this prevents users from logging in directly to sub-companies.
            if ($company->id_own_company && $company->id_own_company != $company->id_company) {
                $this->checkHasParent($company->id_own_company);
            }

            return true;
        }

        return false;
    }

    private function checkHasParent($id_own_company) {
        //load parent company
        $parentCo = $this->CI->c_model->fetch_by_id($id_own_company);
        if ($parentCo) {
            $subdomain_arr = explode('.', $_SERVER['HTTP_HOST']);
            foreach ($subdomain_arr as $idx => $part) {
                if ($part == $this->subdomain) {
                    $subdomain_arr[$idx] = $parentCo->id_string;
                }
            }


            $scheme = $_SERVER['HTTPS'] ? 'https' : 'http';
            $host = join('.', $subdomain_arr);
            $port = in_array($_SERVER['SERVER_PORT'], [80, 443]) ? '' : ":{$_SERVER['SERVER_PORT']}";
            $url = "{$scheme}://{$host}{$port}{$_SERVER['REQUEST_URI']}";
            redirect($url, 'location', 301);
        }
    }

// End func load_company

    public function get_company_id() {
        return $this->company[0]->id_company ? : -1;
    }

// End func get_company_id

    public function get_subdomain() {
        return $this->subdomain;
    }

// End func get_subdomain

    public function get_child_subdomain() {
        return $this->child_subdomain;
    }

    public function company_subdomain() {
        return $this->subdomain;
    }

    public function company() {
        return $this->company;
    }

    public function get_company() {
        return current((array) $this->company);
    }

}

// End class subdomain
// End file user_auth.php
