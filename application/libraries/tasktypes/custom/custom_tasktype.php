<?php

require_once __DIR__ . '/../tasktype.php';

/**
 * Description of custom_tasktype
 *
 * @author JosephT
 */
class Custom_TaskType extends TaskType {

    public function editBootstrap(&$pageData) {
        
    }

    public function assignBootstrap(\employee\ChecklistItem $checklistItem, &$itemData) {
        parent::assignBootstrap($checklistItem, $itemData);
        $itemData['approvers'] = (array) $checklistItem->getParams()['approvers'];
    }

    public function getSupportsAttachment() {
        return true;
    }

    public function validateParams($params) {
        if (!$params['description']) {
            return 'Task description should be supplied.';
        }
    }

    public static function active() {
        return true;
    }

    public static function getId() {
        return 'custom';
    }

    public static function getName() {
        return 'Custom Task';
    }

    public function assignTask(\employee\ChecklistItem $checklistItem, $user, $idEmployeeChecklist, $assignData) {
        $extras = array(
            'id_employee_checklist' => $idEmployeeChecklist,
            'id_checklist_item' => $checklistItem->id,
            'attachment_title' => $checklistItem->attachment_description,
            'attachment_file' => $checklistItem->attached_file_name,
        );

        $params = $checklistItem->getParams();
        return $this->getEmployeeInfoRequest()->createCustomTask($user->id_string, $params['description'], $assignData['due_date'], $extras, (array) $assignData['approvers']);
    }

}
