<?php

use employee\ChecklistItem;
use utils\JSONSerializableObject;

require_once __DIR__ . '/../base_adapter.php';

/**
 * Description of TaskType
 *
 * @author JosephT
 */
abstract class TaskType extends JSONSerializableObject implements Base_adapter {

    abstract public function editBootstrap(&$pageData);
    abstract public function assignTask(ChecklistItem $checklistItem, $user, $idEmployeeChecklist, $assignData);

    public function getTypeName() {
        return static::getName();
    }

    abstract public function getSupportsAttachment();

    abstract public function validateParams($params);

    public function assignBootstrap(ChecklistItem $checklistItem, &$itemData) {
        $dur = (int) $checklistItem->duration;
        $itemData['due_date'] = date_format_strtotime("+{$dur} days");
    }
    
    /**
     * 
     * @return Employee_info_request
     */
    public function getEmployeeInfoRequest(){
        get_instance()->load->library('employee_info_request');
        return get_instance()->employee_info_request;
    }
    
    

}
