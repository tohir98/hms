<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once __DIR__ . '/../tasktype.php';

class Document_Upload_TaskType extends TaskType {

    public function editBootstrap(&$pageData) {
        //nothing to request
    }

    public function getSupportsAttachment() {
        return true;
    }

    public function validateParams($params) {
        if (!$params['description'] || !$params['title']) {
            return 'Document title and description should be supplied.';
        }
    }

    public static function active() {
        return true;
    }

    public static function getId() {
        return 'document_upload';
    }

    public static function getName() {
        return 'Document Upload';
    }

    public function assignTask(\employee\ChecklistItem $checklistItem, $user, $idEmployeeChecklist, $assignData) {
        $extras = array(
            'id_employee_checklist' => $idEmployeeChecklist,
            'id_checklist_item' => $checklistItem->id,
            'attachment_title' => $checklistItem->attachment_description,
            'attachment_file' => $checklistItem->attached_file_name,
        );
        $params = $checklistItem->getParams();
        return $this->getEmployeeInfoRequest()->requestCredentials($user->id_string, $params['title'], $params['description'], $assignData['due_date'], $extras);
    }

}
