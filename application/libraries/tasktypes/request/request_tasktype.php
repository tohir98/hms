<?php

require_once __DIR__ . '/../tasktype.php';

/**
 * Description of request_tasktype
 *
 * @author JosephT
 */
class Request_Tasktype extends TaskType {
    
    public static function active() {
        return true;
    }
    
    public static function getId() {
        return 'request';
    }

    public static function getName() {
        return 'Information Request';
    }

    public function editBootstrap(&$pageData) {
        get_instance()->load->library(['employee_info_request']);
        $infoReq = get_instance()->employee_info_request;
        /* @var $infoReq Employee_info_request */
        $pageData['requestTypeNames'] = $infoReq->getRequestNames();
        unset($pageData['requestTypeNames']['uploaded_docs']);
    }

    public function getSupportsAttachment() {
        return false;
    }
    
    public function validateParams($params) {
        if(count(array_filter($params['requests'])) < 1){
            return 'At least one piece of information must be selected for request.';
        }
        return null;
    }
    
    public function assignTask(\employee\ChecklistItem $checklistItem, $user, $idEmployeeChecklist, $assignData) {
        $extras = array(
            'id_employee_checklist' => $idEmployeeChecklist,
            'id_checklist_item' => $checklistItem->id,
        );
        return $this->getEmployeeInfoRequest()->sendRequest($user->id_string, $assignData['due_date'], $checklistItem->getParams()['requests'], $user, [], $extras);
    }
    
    
}
