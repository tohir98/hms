<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Talentbase
 * Tasks library (for all users)
 * 
 * @category   Library
 * @package    Payroll
 * @subpackage CSV
 * @author     Obinna Ukpabi <obinna.ukpabi@talentbase.ng>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

class csv_helper {
    
/**
 * Codeigniter instance
 * 
 * @access private
 * @var object
     */
    private $CI;
    
    private $col_letters = array(
        0 => 'A',
        1 => 'B',
        2 => 'C',
        3 => 'D',
        4 => 'E',
        5 => 'F',
        6 => 'G',
        7 => 'H',
        8 => 'I',
        9 => 'J',
        10 => 'K',
        11 => 'L',
        12 => 'M',
        13 => 'N',
        14 => 'O',
        15 => 'P',
        16 => 'Q',
        17 => 'R',
        18 => 'S',
        19 => 'T',
        20 => 'U',
        21 => 'V',
        22 => 'W',
        23 => 'X',
        24 => 'Y',
        25 => 'Z',
        26 => 'AA',
        27 => 'AB',
        28 => 'AC',
        29 => 'AD',
        30 => 'AE'
    );
    
/**
 * Class constructor
 * 
 * @access public
 * @return void
 */
    public function __construct() {
        // Load CI object
        $this->CI = get_instance();
        
        // Load model
        $this->CI->load->model('payroll/payroll_model');
    }

/**
 * Check if payroll id is valid
 *
 * @access public
 * @param int $id_user
 * @return mixed (bool | array)
 **/


    function array_to_csv($array, $download = "")
    {
        if ($download != "")
        {    
            header("Content-Type: application/vnd.ms-excel; name='excel'");
            header('Content-Disposition: attachement; filename="' . $download . '"');
        }        
 
        ob_start();
        $f = fopen('php://output', 'w') or show_error("Can't open php://output");
        $n = 0;        
        foreach ($array as $line)
        {
            $n++;
            if ( ! fputcsv($f, $line))
            {
                show_error("Can't write line $n: $line");
            }
        }
        fclose($f) or show_error("Can't close php://output");
        $str = ob_get_contents();
        ob_end_clean();
 
        if ($download == "")
        {
            return $str;    
        }
        else
        {    
            echo $str;
        }        
    }




/*
 * Check if payroll id is valid
 *
 * @access public
 * @param int $id_user
 * @return mixed (bool | array)
 **/


    function convert_xls($data, $download = "")
    {
           
        header("Content-Type: application/vnd.ms-excel; name='excel'");
        header('Content-Disposition: attachement; filename="' . $download . '"');
                
        echo $data;
        
        exit;

    }


/*
 * Check if payroll id is valid
 *
 * @access public
 * @param int $id_user
 * @return mixed (bool | array)
 **/

    
  public function create_excel_file($pay_detail, $net_pay, $pension, $nhf, $tax, $name)
  {
        //Create new PHPExcel object";
        $objPHPExcel = new PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("Talentbase");
        $objPHPExcel->getProperties()->setLastModifiedBy("Talentbase");
        $objPHPExcel->getProperties()->setTitle("Payroll");
        $objPHPExcel->getProperties()->setSubject("Payroll Sheet");
        $objPHPExcel->getProperties()->setDescription("List of employee payroll details");
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Payroll Details');
        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $rowCount = 1;
        foreach ($pay_detail as $data) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $data[0]);
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $data[1]);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $data[2]);
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $data[3]);
            $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $data[4]);
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $data[5]);
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $data[6]);
            $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $data[7]);
            $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $data[8]);
            $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $data[9]);
            $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $data[10]);
            $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, $data[11]);
            $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, $data[12]);
            $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount, $data[13]);
            $rowCount++;
        }


        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(1);
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Net Pay');
        $rowCount = 1;
        foreach ($net_pay as $dat) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $dat[0]);
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $dat[1]);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $dat[2]);
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $dat[3]);
            $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $dat[4]);
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $dat[5]);
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $dat[6]);
            $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $dat[7]);
            $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $dat[8]);
            $rowCount++;
        }


        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(2);
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Pension');
        $rowCount = 1;
        foreach ($pension as $pen) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $pen[0]);
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $pen[1]);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $pen[2]);
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $pen[3]);
            $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $pen[4]);
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $pen[5]);
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $pen[6]);
            $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $pen[7]);
            $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $pen[8]);
            $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $pen[9]);
            $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $pen[10]);
            $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, $pen[11]);
            $rowCount++;
        }


        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(3);
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('TAX');
        $rowCount = 1;
        foreach ($tax as $tx) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $tx[0]);
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $tx[1]);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $tx[2]);
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $tx[3]);
            $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $tx[4]);
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $tx[5]);
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $tx[6]);
            $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $tx[7]);
            $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $tx[8]);
            $rowCount++;
        }

        
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(4);
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('NHF');
        $rowCount = 1;
        foreach ($nhf as $n) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $n[0]);
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $n[1]);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $n[2]);
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $n[3]);
            $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $n[4]);
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $n[5]);
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $n[6]);
            $rowCount++;
        }

         



        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$name.'".xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
  }
  
    public function payroll_report($pay_detail, $payroll_detail_data, $compensation_data, $tax_details, $nhf, $name){

        //Create new PHPExcel object";
        $objPHPExcel = new PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("Talentbase");
        $objPHPExcel->getProperties()->setLastModifiedBy("Talentbase");
        $objPHPExcel->getProperties()->setTitle("Payroll Report");
        $objPHPExcel->getProperties()->setSubject("Payroll Report");
//        $objPHPExcel->getProperties()->setDescription("List of employee payroll details");
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Intro');
        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getStyle('F4:F14')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('G4:G14')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        
        $rowCount = 4;
         foreach ($pay_detail as $data) {
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $data[0]);
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $data[1]);
            $rowCount++;
        }
        
         $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(1);
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Payroll_Detail');
        $objPHPExcel->getActiveSheet()->getStyle('A3:T3')->getFont()->setBold(true);
         $objPHPExcel->getActiveSheet()->mergeCells('K2:M2');
        $objPHPExcel->getActiveSheet()->SetCellValue('K2','PAYE Allowances');
        $objPHPExcel->getActiveSheet()->getStyle('K2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->mergeCells('O2:S2');
        $objPHPExcel->getActiveSheet()->SetCellValue('O2','Deductions');
        $objPHPExcel->getActiveSheet()->getStyle('O2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(16);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(16);
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(16);
        
        $rowCount = 3;
        foreach ($payroll_detail_data as $dat) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $dat[0]);
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $dat[1]);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $dat[2]);
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $dat[3]);
            $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $dat[4]);
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $dat[5]);
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $dat[6]);
            $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $dat[7]);
            $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $dat[8]);
            $objPHPExcel->getActiveSheet()->SetCellValue('j'.$rowCount, $dat[9]);
            $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $dat[10]);
            $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, $dat[11]);
            $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, $dat[12]);
            $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount, $dat[13]);
            $objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount, $dat[14]);
            $objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowCount, $dat[15]);;
            $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowCount, $dat[16]);
            $objPHPExcel->getActiveSheet()->SetCellValue('R'.$rowCount, $dat[17]);
            $objPHPExcel->getActiveSheet()->SetCellValue('S'.$rowCount, $dat[18]);
            $objPHPExcel->getActiveSheet()->SetCellValue('T'.$rowCount, $dat[19]);
            $rowCount++;
        }


        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(2);
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Compensation');
        $rowCount = 3;
        $objPHPExcel->getActiveSheet()->mergeCells('A2:H2');
        $objPHPExcel->getActiveSheet()->SetCellValue('A2','Compensation');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->mergeCells('J2:N2');
        $objPHPExcel->getActiveSheet()->SetCellValue('J2','Pension');
        $objPHPExcel->getActiveSheet()->getStyle('J2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(12);
        
        $objPHPExcel->getActiveSheet()->getStyle('A3:N3')->getFont()->setBold(true);
        
        foreach ($compensation_data as $com) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $com[0]);
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $com[1]);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $com[2]);
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $com[3]);
            $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $com[4]);
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $com[5]);
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $com[6]);
            $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $com[7]);
            $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $com[8]);
            $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $com[9]);
            $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $com[10]);
            $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, $com[11]);
            $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, $com[12]);
            $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount, $com[13]);
            $rowCount++;
        }


        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(3);
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Tax_Summary');
        $objPHPExcel->getActiveSheet()->getStyle('A3:C3')->getFont()->setBold(true);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
        
        $rowCount = 3;
        foreach ($tax_details as $tx) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $tx[0]);
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $tx[1]);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $tx[2]);
            $rowCount++;
        }


        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(4);
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('NHF');
        $objPHPExcel->getActiveSheet()->getStyle('A3:D3')->getFont()->setBold(true);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
        
        $rowCount = 3;
        foreach ($nhf as $n) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $n[0]);
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $n[1]);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $n[2]);
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $n[3]);
            $rowCount++;
        }

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$name.'".xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
  }
  
  public function compensationReport($pay_detail, $compensation_data, $count, $file_name){
      //Create new PHPExcel object";
        $objPHPExcel = new PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("Talentbase");
        $objPHPExcel->getProperties()->setLastModifiedBy("Talentbase");
        $objPHPExcel->getProperties()->setTitle("Payroll Report");
        $objPHPExcel->getProperties()->setSubject("Payroll Report");
//        $objPHPExcel->getProperties()->setDescription("List of employee payroll details");
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Overview');
        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getStyle('F4:F14')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('G4:G14')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        
        $rowCount = 4;
         foreach ($pay_detail as $data) {
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $data[0]);
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $data[1]);
            $rowCount++;
        }
        
        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(1);
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Compensation');
        $objPHPExcel->getActiveSheet()->getStyle('A4:Z4')->getFont()->setBold(true);
        
        $rowCount = 4;
        foreach ($compensation_data as $data) {
            for ( $k = 0; $k <= $count-1; ++$k) {
                $objPHPExcel->getActiveSheet()->SetCellValue($this->col_letters[$k].$rowCount, $data[$k]);
            }
            $rowCount++;
        }
        
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$file_name.'".xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
  }
  
  public function payroll_template($pay_detail, $count, $name){
        //Create new PHPExcel object";
        $objPHPExcel = new PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("Talentbase");
        $objPHPExcel->getProperties()->setLastModifiedBy("Talentbase");
        $objPHPExcel->getProperties()->setTitle("Attendance Report");
        $objPHPExcel->getProperties()->setSubject("Attendance Report");
//        $objPHPExcel->getProperties()->setDescription("List of employee payroll details");
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Template');

        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getStyle('A1:'.$this->col_letters[$count-1].'1')->getFont()->setBold(true);
        $rowCount = 1;
        
        for ( $j = 0; $j <= 5; ++$j) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($this->col_letters[$j])->setWidth(14);
        }
        
        foreach ($pay_detail as $data) {
            for ( $k = 0; $k <= $count-1; ++$k) {
                $objPHPExcel->getActiveSheet()->SetCellValue($this->col_letters[$k].$rowCount, $data[$k]);
            }
            $rowCount++;
        }
        
         // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$name.'".xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        
        
  }//end func payroll_template
  
  public function personnel_audit_record($pay_detail, $count, $file_name){
      
        //Create new PHPExcel object";
        $objPHPExcel = new PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("Talentbase");
        $objPHPExcel->getProperties()->setLastModifiedBy("Talentbase");
        $objPHPExcel->getProperties()->setTitle("Attendance Report");
        $objPHPExcel->getProperties()->setSubject("Attendance Report");
//        $objPHPExcel->getProperties()->setDescription("List of employee payroll details");
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Template');

        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getStyle('B2:'.$this->col_letters[$count].'2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('B2:'.$this->col_letters[$count].'2')->getFont()->setUnderline(true);
        $rowCount = 2;
        
        foreach ($pay_detail as $data) {
            for ( $k = 0; $k <= $count-1; ++$k) {
                $objPHPExcel->getActiveSheet()->SetCellValue($this->col_letters[$k+1].$rowCount, $data[$k]);
            }
            $rowCount++;
        }
        
         // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$file_name.'".xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        
        
  }//end func personnel_audit_record





   /*
 * Check if payroll id is valid
 *
 * @access public
 * @param int $id_user
 * @return mixed (bool | array)
 **/

    
  public function create_planner_excel_file($planner_array, $approved, $pending, $name='download')
  {

        //Create new PHPExcel object";
        $objPHPExcel = new PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("Talentbase");
        $objPHPExcel->getProperties()->setLastModifiedBy("Talentbase");
        $objPHPExcel->getProperties()->setTitle("Vacation Planner");
        $objPHPExcel->getProperties()->setSubject("Vacation Planner");
        $objPHPExcel->getProperties()->setDescription("List of employee vacation plan details");

        $style_header = array(
                            'fill' => array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb'=>'E1E0F7')
                            )
                        );

        $style_green = array(
                            'fill' => array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb'=>'00CD00')
                            )
                        );

        $style_orange = array(
                            'fill' => array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb'=>'FFA500')
                            )
                        );


        
        $active_sheet = 0;
        foreach ($planner_array as $month => $data) {
    
            // Add some data
            if($active_sheet > 0){
                $objPHPExcel->createSheet();
            }
            
            $objPHPExcel->setActiveSheetIndex($active_sheet);
            // Rename sheet
            $objPHPExcel->getActiveSheet()->setTitle($month);
            $rowCount = 1;
           
            foreach ($data as $k => $d) {
                $col = 0;
                for ($i=0; $i <= 35 ; $i++) { 
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $rowCount, $d[$i]);
                    
                    if(is_numeric($d[$i])){
                        if($k != 0){
                            //$d[$i] = "";
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $rowCount, $d[$i])->getStyleByColumnAndRow($col, $rowCount)->applyFromArray($style_header);
                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $rowCount, '');
                        }

                        
                        if(!empty($approved[$active_sheet][$k])){
                            if(in_array($d[$i], $approved[$active_sheet][$k])){
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $rowCount, $d[$i])->getStyleByColumnAndRow($col, $rowCount)->applyFromArray($style_green);
                                if($k != 0){
                                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $rowCount, '');
                                }
                                                                
                            }


                        }

                        if(!empty($pending[$active_sheet][$k])){
                            if(in_array($d[$i], $pending[$active_sheet][$k])){
                                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $rowCount, $d[$i])->getStyleByColumnAndRow($col, $rowCount)->applyFromArray($style_orange);  
                                if($k != 0){
                                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $rowCount, ''); 
                                }

                            }
                        }
                        
                            
                    
                    }
                    $col++;
                }

                
                $rowCount++;
            }
            $a[] = $active_sheet;
            $active_sheet++;
        }
      

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$name.'".xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
  }



    /*
 * Check if payroll id is valid
 *
 * @access public
 * @param int $id_user
 * @return mixed (bool | array)
 **/

    
  public function create_record_excel_file($record, $name='download')
  {

        //Create new PHPExcel object";
        $objPHPExcel = new PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("Talentbase");
        $objPHPExcel->getProperties()->setLastModifiedBy("Talentbase");
        $objPHPExcel->getProperties()->setTitle("Vacation Summary");
        $objPHPExcel->getProperties()->setSubject("Vacation Summary");
        $objPHPExcel->getProperties()->setDescription("List of employee vacation Request");

        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('Vacation Request Records');

        $rowCount = 1;

        foreach ($record as $d) {
            
            $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $d[0]);
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $d[1]);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $d[2]);
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $d[3]);
            $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $d[4]);
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $d[5]);
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $d[6]);

            $rowCount++;
           
        }
      

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$name.'".xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
  }


}


/* End of file csv_helper.php */
/* Location: ./applicaton/library/csv_helper.php */