<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once __DIR__ . '/../base_adapter.php';
/**
 * Description of AuthProvider
 *
 * @author JosephT
 */
abstract class AuthProvider implements Base_adapter {

    protected $companyId;

    /**
     *
     * @var array
     */
    protected $config;
    protected $enabled = false;
    protected $default = false;
    protected $rawData = array();

    /**
     *
     * @var user_auth
     */
    protected $user_auth;

    /**
     *
     * @var CI_DB_active_record
     */
    protected $db;

    const AUTH_CONFIG_TABLE = 'auth_config';

    final public function __construct($companyId, $enabled = false, array $config = array(), $default = false, $loadConfig = true) {
        $this->companyId = $companyId;
        $ci = get_instance();
        $this->db = $ci->load->database('', true);
        $ci->load->helper('url');
        $ci->load->library(['user_auth']);
        $this->user_auth = $ci->user_auth;
        $this->enabled = $enabled;
        $this->default = $default;
        $this->config = $config;

        if ($loadConfig) {
            $this->loadConfig();
        }

        $this->init();
    }

    protected function loadConfig() {
        $configRow = $this->db->get_where(static::AUTH_CONFIG_TABLE, array(
                    'id_company' => $this->companyId,
                    'adapter' => static::getId(),
                        ), 1)->result('array');

        if (!empty($configRow)) {
            $myConfigRow = current($configRow);
            $this->enabled = !!$myConfigRow['is_enabled'];
            $this->config = json_decode($myConfigRow['config_data'], true);
            $this->default = !!$myConfigRow['is_default'];
            $this->rawData = $myConfigRow;
        }
    }

    /**
     * @return array config
     */
    public function getConfigData($reload = false) {
        if ($reload) {
            $this->loadConfig();
        }

        return $this->config;
    }

    abstract protected function init();

    abstract public function validateConfig($configData, &$error = null);

    /**
     * @return boolean true if configured, false otherwise
     */
    public function isConfigured() {
        return !empty($this->config);
    }

    public function isEnabled() {
        return $this->enabled;
    }

    final public function isDefault() {
        return $this->default;
    }

    public function updateConfig(array $configData, $enabled = true, $default = false) {
        $data = array(
            'config_data' => json_encode($configData),
            'is_enabled' => (int) $enabled,
            'is_default' => (int) $default,
            'last_modified' => date('Y-m-d H:i:s'),
            'id_user_last_modified' => $this->user_auth->get('id_user'),
        );

        $this->db->trans_start();
        //first, turn off other defaults
        if ($default && !$this->default) {
            $this->db->update(static::AUTH_CONFIG_TABLE, array(
                'is_default' => 0
                    ), array(
                'id_company' => $this->companyId
            ));
        }

        if ($this->isConfigured()) {
            //update
            $this->db->update(static::AUTH_CONFIG_TABLE, $data, array(
                'id_company' => $this->companyId,
                'adapter' => static::getId()
            ));
        } else {
            $data['id_company'] = $this->companyId;
            $data['adapter'] = static::getId();
            $this->db->insert(static::AUTH_CONFIG_TABLE, $data);
        }

        $this->db->trans_complete();
        //reload config from DB
        $this->loadConfig();
        return true;
    }

    protected function signinByEmail($email, &$error) {
        if (($resp = $this->user_auth->login($email, null, $this->companyId, true))) {
            $ci = get_instance();
            $ci->load->library('mixpanel');

            $ci->mixpanel->track('Auth: Login with ' . static::getId(), array(
                "distinct_id" => $this->user_auth->get('id_string'),
                "status" => 'Successful'
            ));

            auth_log_user_action($resp['id_user'], 'User logged in successfully using ' . static::getName(), LOG_TYPE_LOGIN);
            auth_redirect_by_user_type($resp["account_type"], auth_next_url());
        } else {
            if ($resp === false) {
                //account does not exist
                $error = 'Your administrator is yet to create an account for you on this portal.';
            } else {
                $error = $this->user_auth->getLastError();
            }
        }
    }

    protected function getNextUrl() {
        return auth_next_url();
    }

    /**
     * Action called when a user wants to sign in.
     */
    abstract public function signinAction();

    /**
     * Action called when a user wants to signout
     */
    abstract public function signout();
    
    public static function active() {
        return true;
    }
}
