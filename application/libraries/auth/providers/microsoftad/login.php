<div class="login-body">
    <h2>Active Directory Login</h2>
    <form method='POST' class='form-validate' id="test">
        <div class="well well-small">
                        Sign in with your <strong><?= $account_domain_name ?></strong> Active Directory domain credentials.
        </div>
        <?php if($error): ?>
        <div class="alert alert-error">
            <?= $error; ?>
        </div>
        <?php endif; ?>
        <div class="control-group <?php echo $email_error_class; ?>">
            <div class="controls email">
                <input type="text" name='username' placeholder="Username" class='input-block-level' data-rule-required="true" data-rule-alnum="true">
            </div>
        </div>
        <div class="control-group <?php echo $password_error_class; ?>">
            <div class="pw controls">
                <input type="password" name="password" placeholder="Password" class='input-block-level' data-rule-required="true">
            </div>
        </div>
        <div class="submit">
            <input type="submit" value="Sign me in" class='btn btn-primary'>
        </div>
    </form>
    <div class="forget">
        <a href="<?php echo site_url("login?pwd_auth=1"); ?>">
            <span>Use another login method</span>
        </a>
    </div>

</div>