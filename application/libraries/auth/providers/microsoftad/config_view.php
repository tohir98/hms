<?php $configData = $data['config']; ?>
<?=
form_bs_control('Active Directory Server', function() use ($configData) {
    echo form_input(['name' => 'config[host]', 'required' => true], $configData['host']);
    ?>
    <div class="help help-block">
        <small>Address (domain name or IP address) to your Active Directory domain controller server.</small>
    </div>
    <?php
});
?>
<?=
form_bs_control('Active Directory Domain', function() use ($configData) {
    echo form_input(['name' => 'config[account_domain_name]', 'required' => true], $configData['account_domain_name']);
    ?>
    <div class="help help-block">
        <small>Active Directory domain for your organization.</small>
    </div>
    <?php
});
?>
<?=
form_bs_control('Account Canonical Form', function() use ($configData) {
    echo form_dropdown('config[account_canonical_form]', MicrosoftAd::$accountFormats, $configData['account_canonical_form'] ? : \Zend\Ldap\Ldap::ACCTNAME_FORM_PRINCIPAL, 'class="input-xxlarge"');
    ?>
    <div class="help help-block">
        <small>The canonical format used by your AD server, contact your Domain Admin if unsure.</small>
    </div>
    <?php
});
?>
<label class="checkbox">
    <?= form_checkbox_unchecked('config[use_start_tls]', 1, 0, [], !!$configData['use_start_tls']); ?> Use Secure Connection <small>(Use this if your Active Directory LDAP server uses secure connection.)</small>
</label>