<?php

use Zend\Ldap\Exception\LdapException;
use Zend\Ldap\Ldap;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of microsoft_ad
 *
 * @author JosephT
 */
class MicrosoftAd extends AuthProvider {

    /**
     *
     * @var CI_Input
     */
    private $input;

    /**
     *
     * @var Auth
     */
    private $CI;

    /**
     *
     * @var Bugsnag
     */
    private $bugsnag;
    public static $accountFormats = array(
        Ldap::ACCTNAME_FORM_DN => 'DN format e.g. "CN=John Doe,DC=domain,DC=example,DC=com"',
        Ldap::ACCTNAME_FORM_USERNAME => 'Username only e.g. johndoe',
        Ldap::ACCTNAME_FORM_BACKSLASH => 'Backslash e.g. DOMAIN\username, Windows 2000 server and below',
        Ldap::ACCTNAME_FORM_PRINCIPAL => 'Principal (preferred) e.g. johndoe@domain.com, Windows 2000 server and above',
    );

    protected function init() {
        $this->CI = get_instance();
        $this->CI->load->helper('validation');
        $this->CI->load->library('input');
        $this->input = $this->CI->input;
        $this->bugsnag = load_class('bugsnag', 'libraries', '');
    }

    private function ldapSignin($username, $password, &$error) {
        $tmp = explode('.', $this->config['account_domain_name']);
        $baseDnArr = [];
        foreach ($tmp as $part) {
            $baseDnArr[] = "DC={$part}";
        }
        $baseDn = join(',', $baseDnArr);
//        array_shift($tmp);
//        $domainShort = join('.', $tmp);

        $options = array(
            'host' => $this->config['host'],
            'useStartTls' => !!$this->config['use_start_ssl'],
//            'username' => $username,
//            'password' => $password,
            'accountDomainName' => $this->config['account_domain_name'],
            'accountDomainNameShort' => $this->config['account_domain_name'],
            'baseDn' => $baseDn,
            'accountCanonicalForm' => intval($this->config['account_canonical_form']) ? : Ldap::ACCTNAME_FORM_PRINCIPAL,
        );

        $ldap = new Ldap($options);

        try {
            $accountName = $ldap->bind($username, $password)
                    ->getCanonicalAccountName($username, Ldap::ACCTNAME_FORM_PRINCIPAL)
            ;
            
            return $accountName;
        } catch (LdapException $ex) {
            $this->bugsnag->getClient()->notifyException($ex);
            $error = $ex->getMessage();
        }

        return false;
    }

    public function signinAction() {
        $error = '';
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            if ($username && $password && ($ldapUserInfo = $this->ldapSignin($username, $password, $error))) {
                //do login connection stuff
                $this->signinByEmail($ldapUserInfo, $error);
            } else {
                $error = $error ? : 'Supply both Username and Password';
            }
        }

        $viewData = array(
            'account_domain_name' => $this->config['account_domain_name'],
            'error' => $error,
            'login_tpl' => __DIR__ . '/login.php',
            'company' => current($this->CI->subdomain->company())
        );


        $this->CI->load->view('user/login', $viewData);
    }

    public function signout() {
        //nothing to do.
    }

    public function validateConfig($configData, &$error = null) {
        if (!$configData['host'] || !$configData['account_domain_name']) {
            $error = 'Host and Account domain must be specified';
        } else {
            if (!filter_var($configData['host'], FILTER_VALIDATE_URL) && !validate_domain_name($configData['host'])) {
                $error = 'The server host is not valid.';
            } elseif (!validate_domain_name($configData['account_domain_name'])) {
                $error = 'The account domain name is not valid.';
            } else {
                return true;
            }
        }

        return false;
    }

    public static function getId() {
        return 'ms-ads';
    }

    public static function getName() {
        return 'Active Directory by Microsoft&reg;';
    }

}
