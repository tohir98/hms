<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Talentbase
 * CDN upload library
 * 
 * @category   Library
 * @package    CDN
 * @subpackage Document Upload
 * @author     Chuks Olloh <chuks.olloh@hired.com.ng>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

class Docs_upload {
	
	/**
	 * __construct
	 */
	public function __construct() {
		
		// Load CI object
		$this->CI = get_instance();
		
		// Call Libs.
		$this->CI->load->library(array('cdn_api'));
		
	}// End func __construct
	
	/**
	 * Upload documents to rackspace
	 * 
	 * @param array $params
	 * @return boolean
	 */
	public function upload_docs(array $params) {
		
		if(empty($params)){
			return false;
		}
		
		if(!isset($params['name'])){
			return array('status' => 500, 'message' => 'Error! No file name was passed');
		}
		
		if(isset($params['container'])){
			$container_name = $params['container'];
		} else {
			$container_name = $this->CI->user_auth->get('cdn_container');
		}
		
		// Create connection with rackspace
		$this->CI->cdn_api->connect(
			array(
				'username' => RACKSPACE_USERNAME,
				'api_key' => RACKSPACE_API_KEY,
				'container' => $container_name
			));
		
		
		$n = basename($params['file']['name']);
		
		$x = pathinfo($n, PATHINFO_EXTENSION); // Get file extension
		$new_file_name = $params['name'].'_'.md5($n).'.'.$x; // Rename file name
		
		$uri = $this->CI->cdn_api->upload_file(
			array(
				'file_name' => $new_file_name,
				'file_path' => $params['file']['path']
			));
		
		if($uri != '') {
			return array(
				'status' => 200, 
				'message' => 'Document upload successful', 
				'info' => array(
					'file_type' => $x,
					'rackspace_file_name' => $new_file_name
				),
				'src' => $uri
			);
			
		} else {
			return array('status' => 500, 'message' => 'Error! Unable to push document to cloud. Please try again');
		}
		
	}// End func upload_docs
	
}// End class Docs_upload
