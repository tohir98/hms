<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Talentbase
 * Applicant authentication library
 * 
 * @category   Library
 * @package    Users
 * @subpackage Authentication
 * @author     David A. <tokernel@gmail.com>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

class applicant_auth {
	
	/**
	 * Applicant email
	 * 
	 * @access private
	 * @var string
	 */
	private $email_applicant;
        
    /**
	 * User id
	 * 
	 * @access private
	 * @var int
	 */
	private $id_applicant;
	
	/**
	 * Codeigniter instance
	 * 
	 * @access private
	 * @var object
	 */
    private $CI;
	
	/* 
	 * Salt for applicant password crypting.
	 * 
	 * @access private
	 * @var string
	 */
	private $salt = 'Hired.com.ng@2013';
	
    /**
	 * Class constructor
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct()
	{	
		
		// Load CI object
		$this->CI = get_instance();
		
		// Load libraries
        $this->CI->load->library('session');
		
		// Load models
		$this->CI->load->model('user/applicant_model');
		
		$this->CI->load->helper('url');
		
		// Set user email value form sessions
		$this->email_applicant = $this->CI->session->userdata('applicant_email');
        $this->id_applicant = $this->CI->session->userdata('id_applicant');
		
	} // End func __construct
	
	/**
	 * Login method
	 *
     * @access public
     * @param string $email
     * @param string $password
	 * @return mixed (bool | array)
	 **/
	public function login($email, $password)
	{
		
		$password = $this->encrypt($password);
		
		// Fetch applicant data from database by email and password
        $result = $this->CI->applicant_model->fetch_account(array('email' => $email, 'password' => $password));

        if(!$result) {
            // applicant does not exists
            return FALSE;
        }
                
        // Define params
        $status = $result[0]->status;
       
        // Check if applicant status is ACTIVE
        if($status == 'Active') {
 
			$key = sha1($result[0]->email . '_' . $status);
			
			// Build applicant session array
            $session_vars = array(
                            // More session variables to be added later here.
							'id_applicant' => $result[0]->candidateid,
                            'applicant_email' => $result[0]->email,
                            'applicant_status' => $status,
                            'applicant_display_name' => $result[0]->firstname,
							'applicant_k' => $key
                        );
                       
			// Add user record details to session
            $this->CI->session->set_userdata($session_vars);
		}

        // Return array with user data
        return array(
               "status" => $status
              );
		
	} // End func login
	
	/**
	 * Logout method
	 *
	 * @access public
	 * @return void
	 **/
	public function logout()
	{
		$session_vars = array(
			// More session variables to be added later here.
			'id_applicant' => '',
            'applicant_email' => '',
            'applicant_status' => '',
            'applicant_display_name' => '',
			'applicant_k' => ''
        );
                       
		// Add user record details to session
        $this->CI->session->set_userdata($session_vars);
				
	} // End func logout
	
	/**
	 * Redirect to login page if user not logged in.
	 * 
	 * @access public
	 * @return void
	 */
	public function check_login() {
		
		if(!$this->logged_in()) {
			redirect(site_url('applicant/login'), 'refresh');
		}
				
	}
	
	/**
	 * Check if user logged in
	 *
	 * @access public
	 * @return bool
	 **/
	public function logged_in()
	{
		
		$cdata = array(
			'email' => $this->CI->session->userdata('applicant_email'),
			'status' => $this->CI->session->userdata('applicant_status'),
			'id_applicant' => $this->CI->session->userdata('id_applicant')
		);
		
		foreach($cdata as $data) {
			if(trim($data) == '') {
				return false;
			}
		}
		
        $s_k = $this->CI->session->userdata('applicant_k');
		$c_k = sha1($cdata['email'] . '_' . $cdata['status']);
                            				
		if($s_k != $c_k) {
			return false;
		}
		
		return true;
		
	} // End func logged_in
	
	/**
	 * Get session variable value assigned to user. 
	 * 
	 * @access public
	 * @param string $item
	 * @return mixed (bool | string)
	 */
	public function get($item) {
		
		if(!$this->logged_in()) {
			return false;
		}
		
		return $this->CI->session->userdata($item);
		
	} // End func get
	
	/**
	 * Encrypt string to sha1 
	 * 
	 * @access public
	 * @param string $str
	 * @return string
	 */
	public function encrypt($str) {
		return crypt($str, $this->salt);
	} // End func encrypt
    
	/**
	 * Redirect to user's access denied page, if user have not permission.
	 * 
	 * @access public 
	 * @param type $id_perm 
	 * @return void
	 */
	/*
	public function check_perm($id_perm) {
		if(!$this->have_perm($id_perm)) {
			redirect(site_url('user/access_denied'), 'refesh');
		}
	}
	*/
	
    /**
	 * Check if user has permission 
	 * 
	 * @access public
	 * @param int $id_perm
     * @param int id_user
	 * @return bool
	 */
	/*
     public function have_perm($id_perm){
            
            if(!is_numeric($id_perm) or !is_numeric($this->id_user)) {
                return false;
            }
            
            $ret = $this->CI->user_model->get_user_perm(
                                                        array(
                                                            "id_user" => $this->id_user,
                                                            "id_perm" => $id_perm
                                                        ));
            
            return $ret;
     }// End func have_perm
	 * 
	 */
	 
} // End class user_auth

// End file user_auth.php
?>
