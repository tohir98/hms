<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Talentbase
 * Users authentication library
 * 
 * @category   Library
 * @package    Users
 * @subpackage Authentication
 * @author     David A. <tokernel@gmail.com>
 * @author     Chuks O. <chuks.olloh@hired.com.ng>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */
class user_auth {

    private $default_perms = array(
        'Employee' => array(
            1 => array(
                'view_staff_directory' => 1,
                'organogram' => 131
            ),
            2 => array(
                'my_vacation' => 5,
                'holidays' => 7,
                'make_vacation_request' => 88
            ),
            3 => array(
                'my_appraisal' => 8,
                'submit_my_appraisal' => 9
            ),
            4 => array(
                'my_profile' => 109,
            //'view_my_profile_picture'=>111,
            //'view_my_profile_work_info'=>112,
            //'view_my_profile_personal_info'=>113
            ),
            7 => array(
                'view_downloads' => 26
            ),
            9 => array(
                'view_events' => 30,
                'add_events' => 31,
                'edit_events' => 32,
                'view_events_calendar' => 78
            ),
            12 => array(
                'my_queries' => 93
            ),
            23 => array(
                'my_traininng' => 239,
                'browse_courses' => 241
            ),
            17 => array(
                'my_goals' => 174
            ),
            22 => array(
                'my_rewards' => 216
            ),
        ),
        'Unit Manager' => array(
            1 => array(
                'view_staff_directory' => 1,
                'view_records' => 138,
                'organogram' => 131
            ),
            2 => array(
                'my_vacation' => 5,
                'holidays' => 7,
                'make_vacation_request' => 88,
                'pending_approvals' => 80,
                'approve_vacation_request' => 89,
                'decline_vacation_request' => 90,
                'view_summary' => 81
            ),
            3 => array(
                'my_appraisal' => 8,
                'submit_my_appraisal' => 9,
                'appraisal_records' => 10
            ),
            4 => array(
                'my_profile' => 109,
            ),
            7 => array(
                'view_downloads' => 26
            ),
            9 => array(
                'view_events' => 30,
                'add_events' => 31,
                'view_events_calendar' => 78
            ),
            12 => array(
                'my_queries' => 93,
                'issue_new_query' => 92,
                'query_records' => 94
            ),
            11 => array(
                'view_access_control' => 91
            ),
            22 => array(
                'my_rewards' => 216,
                'manager_view' => 217
            )
        )
    );

    /**
     * User email
     * 
     * @access private
     * @var string
     */
    private $email_user;

    /**
     * User id
     * 
     * @access private
     * @var int
     */
    private $id_user;

    /**
     * Company id
     * 
     * @access private
     * @var int
     */
    private $id_company;

    /**
     * Codeigniter instance
     * 
     * @access private
     * @var object
     */
    private $CI;

    /**
     * User statuses
     * @var type 
     */
    private $statuses;
    private $statuses_without_terminated;
    private $lastError = '';

    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        // Load CI object
        $this->CI = get_instance();

        // Load libraries
        $this->CI->load->library('session');

        // Load models
        $this->CI->load->model('user/user_model');
        $this->CI->load->model('user/employee_model');

        $this->CI->load->helper(['url', 'auth']);

        // Set user email value form sessions
        $this->email_user = $this->CI->session->userdata('email');
        $this->id_user = $this->CI->session->userdata('id_user');
        $this->id_company = $this->CI->session->userdata('id_company');

        $this->statuses = array(
            USER_STATUS_INACTIVE => 'Inactive',
            USER_STATUS_ACTIVE => 'Active',
            USER_STATUS_SUSPENDED => 'Suspended',
            USER_STATUS_TERMINATED => 'Terminated',
            USER_STATUS_PROBATION_ACCESS => 'Probation/Access',
            USER_STATUS_ACTIVE_NO_ACCESS => 'Active/No access',
            USER_STATUS_PROBATION_NO_ACCESS => 'Probation/No access'
        );

        $this->statuses_without_terminated = array(
            USER_STATUS_INACTIVE => 'Inactive',
            USER_STATUS_ACTIVE => 'Active',
            USER_STATUS_SUSPENDED => 'Suspended',
            USER_STATUS_PROBATION_ACCESS => 'Probation/Access',
            USER_STATUS_ACTIVE_NO_ACCESS => 'Active/No access',
            USER_STATUS_PROBATION_NO_ACCESS => 'Probation/No access'
        );
    }

// End func __construct

    public function get_default_perms($type) {

        if (!isset($this->default_perms[$type])) {
            return false;
        }

        return $this->default_perms[$type];
    }

// End func get_default_perm

    public function statuses() {
        return $this->statuses;
    }

    public function statuses_without_terminated() {
        return $this->statuses_without_terminated;
    }

    /**
     * Login method
     *
     * @access public
     * @param string $email
     * @param string $password
     * @return mixed (bool | array)
     * */
    public function login($email, $password, $id_company) {
        
        $loginWhere = array('email' => $email);

        $loginWhere['password'] = $this->encrypt($password);
        
        // Fetch user data from database by email and password
        $result = $this->CI->user_model->fetch_account($loginWhere);
        if (!$result) {
            // User does not exists
            return FALSE;
        }

        // Define params
        $status = $result[0]->status;
        $account_type = $result[0]->account_type;
        $company_status = $result[0]->company_status;
        $account_blocked = $result[0]->account_blocked;
        $access_level = $result[0]->id_access_level;
        $id_user = $result[0]->id_user;

        $basicdata = array(
            'status' => $status,
            'account_type' => $account_type,
            'access_level' => $access_level,
            'company_status' => $company_status,
            'account_blocked' => $account_blocked,
            'id_user' => $id_user
        );
        // Check if user status is ACTIVE
        if ($status == USER_STATUS_ACTIVE ) {

            $key = sha1($result[0]->email . '_' . $status . '_' . $account_type);

            // Build user session array
            $session_vars = array(
                // More session variables to be added later here.
                'id_user' => $result[0]->id_user,
                'id_string' => $result[0]->id_string,
                'email' => $result[0]->email,
                'status' => $status,
                'account_type' => $account_type,
                'access_level' => $result[0]->id_access_level,
                'display_name' => $result[0]->first_name,
                'last_name' => $result[0]->last_name,
                'k' => $key,
                'id_company' => $result[0]->id_company,
                'company_name' => $result[0]->company_name,
                'company_id_string' => $result[0]->company_id_string,
                'cdn_container' => $result[0]->cdn_container
            );

            // Add user record details to session
            $this->CI->session->set_userdata($session_vars);
            $this->lastError = '';
           
            return $basicdata;
        } else {
            $this->lastError = auth_login_error($basicdata);
            return null;
        }
    }

    public function assignAllModulePermissionsToHrAdmin() {
        if ($this->is_admin()) {
            $this->CI->user_model->assignAllPermissions($this->get('id_user'), $this->get('id_company'));
        }
    }

    public function assignDefaultPermissionsToEmployee() {

        $this->CI->user_model->assignEmployeeDefaultPermissions($this->get('id_user'), $this->get('id_company'), $this->default_perms['Employee']);
    }

    public function getLastError() {
        return $this->lastError;
    }

// End func login

    /**
     * Return true if user is admin.
     * 
     * See the USER_TYPE_ADMIN, USER_TYPE_EMPLOYEE and the users.account_type in table. 
     */
    public function is_admin() {

        if (!$this->logged_in()) {
            return false;
        }

        if ($this->get('account_type') == USER_TYPE_ADMIN) {
            return true;
        }

        return false;
    }

// End func 

    /**
     * Logout method
     *
     * @access public
     * @return void
     * */
    public function logout() {
        // Destroy current user session
        $this->CI->session->sess_destroy();
    }

// End func logout

    /**
     * Redirect to login page if user not logged in.
     * 
     * @access public
     * @return void
     */
    public function check_login($url = '') {
        if (!$url) {
            $url = $this->CI->input->server('REQUEST_URI');
        }

        if (!$this->logged_in() || $this->hasExpired()) {
            redirect(site_url('login') . '?next_url=' . urlencode($url));
        }
    }
    
    public function hasExpired(){
        if (strtotime(date('Y-m-d')) > strtotime(date(EXPIRING_DATE))){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    /**
     * Check if user logged in
     *
     * @access public
     * @return bool
     * */
    public function logged_in() {

        $cdata = array(
            'email' => $this->CI->session->userdata('email'),
            'status' => $this->CI->session->userdata('status'),
            'account_type' => $this->CI->session->userdata('account_type')
        );

        foreach ($cdata as $data) {
            if (trim($data) == '') {
                return false;
            }
        }

        $s_k = $this->CI->session->userdata('k');
        $c_k = sha1($cdata['email'] . '_' . $cdata['status'] . '_' . $cdata['account_type']);

        if ($s_k != $c_k) {
            return false;
        }

        return true;
    }

// End func logged_in

    /**
     * Get session variable value assigned to user. 
     * 
     * @access public
     * @param string $item
     * @return mixed (bool | string)
     */
    public function get($item = null) {

        if (!$this->logged_in()) {
            return false;
        }

        return $item === null ? $this->CI->session->all_userdata() : $this->CI->session->userdata($item);
    }

// End func get

    /**
     * Encrypt string to sha1 
     * 
     * @access public
     * @param string $str
     * @return string
     */
    public static function encrypt($str) {
        return sha1($str);
    }

// End func encrypt

    /**
     * Redirect to user's access denied page, if user have not permission.
     * 
     * @access public 
     * @param int|array $id_perm ID of the permission 
     * or an array of module (id_string of module) and permission (id_string of permission)
     * @return void
     */
    public function check_perm($id_perm) {
        if (!$this->have_perm($id_perm)) {
            redirect(site_url('user/access_denied'), 'refesh');
        }
    }

    /**
     * Check if user has permission 
     * 
     * @access public
     * @param int|array|string $id_perm ID of the permission 
     * or an array of module (id_string of module) and permission (id_string of permission)
     * or a string in the form module:permission
     * @param int id_user
     * @return bool
     */
    public function have_perm($id_perm) {

        $ret = false;
        
        if (is_string($id_perm) && strpos($id_perm, ':')) {
            $parts = explode(':', $id_perm, 2);
            $id_perm = ['module' => $parts[0], 'permission' => $parts[1]];            
        }
        
        if (is_array($id_perm)) {
            assert(isset($id_perm['module'], $id_perm['permission']), 'Module and Permission string must be set');
            if ($id_perm['module'] && $id_perm['permission']) {
                $db = $this->CI->load->database('', true);
                /* @var $db CI_DB_active_record */
                $result = $db->select('up.id_perm')
                        ->from('user_perms AS up')
                        ->join('module_perms AS mp', 'mp.id_perm = up.id_perm')
                        ->join('modules AS m', 'm.id_module = mp.id_module')
                        ->where(array(
                            'up.id_company' => $this->id_company,
                            'm.id_string' => $id_perm['module'],
                            'mp.id_string' => $id_perm['permission'],
                            'up.id_user' => $this->id_user
                        ))
                        ->get()
                        ->result_array()
                ;
                $ret = !empty($result);
            }
        } else {
            if (is_numeric($id_perm) && is_numeric($this->id_user)) {
                $ret = $this->CI->user_model->get_user_perm(
                        array(
                            'id_user' => $this->id_user,
                            'id_perm' => $id_perm,
                            'id_company' => $this->id_company
                ));
            }
        }

        return $ret;
    }

// End func have_perm

    /**
     * Create Unique ID for user
     * 
     * @access public
     * @return string 
     */
    public function create_id_string($id_company = NULL, $company_name = NULL) {

        if (is_null($company_name)) {
            $company_name = $this->get('company_name');
        }

        if (is_null($id_company)) {
            $id_company = $this->get('id_company');
        }

        $id_string = $this->CI->user_model->last_id_string($id_company);

        if ($id_string == '') {
            $count = '1';
        } else {
            $count = substr($id_string, 3, 100);
            // $count = str_replace('0', '', $count);
            $_count = (int) $count;
            $_count++;
        }

        $ID = '';

        $ID .= strtoupper(substr($company_name, 0, 3));
        $ID .= str_pad($_count, 6, '0', STR_PAD_LEFT);

        return $ID;
    }

// End func unique_id

    public function login_by_cu_id_strings($c_id_string, $u_id_string) {

        $y = $this->CI->user_model->fetch_user_and_company_data($u_id_string, $c_id_string);

        if (empty($y)) {
            return false;
        }

        // Define params
        $status = $y[0]->u_status;
        $account_type = $y[0]->account_type;

        // Check if user status is ACTIVE
        if ($status == USER_STATUS_ACTIVE or $status == USER_STATUS_PROBATION_ACCESS) {

            $key = sha1($y[0]->email . '_' . $status . '_' . $account_type);

            // Build user session array
            $session_vars = array(
                // More session variables to be added later here.
                'id_user' => $y[0]->id_user,
                'id_string' => $y[0]->u_id_string,
                'email' => $y[0]->email,
                'status' => $status,
                'account_type' => $account_type,
                'display_name' => $y[0]->first_name,
                'last_name' => $y[0]->last_name,
                'k' => $key,
                'id_company' => $y[0]->id_company,
                'company_name' => $y[0]->company_name,
                'company_id_string' => $y[0]->c_id_string,
                'company_switch_selected' => 0,
                'company_switch_selected' => 1
            );

            // Add user record details to session
            $this->CI->session->set_userdata($session_vars);
        }

        // Return array with user data
        return array(
            "status" => $status,
            "account_type" => $account_type
        );
    }

// End func login_by_id_strings

    /**
     * Direct access by user id string
     * 
     * @access public
     * @return void 
     */
    public function login_by_id_string($id_string, $id_company_client, $id_company) {

        if ($id_string == '' or $id_company_client == '' or $id_company == '') {
            return false;
        }

        $y = $this->CI->user_model->fetch_user_client_details(
                array(
                    'id_string' => $id_string,
                    'id_company' => $id_company
        ));

        $c = $this->CI->user_model->fetch_client_company_info(
                array(
                    'id_company' => $id_company_client
        ));

        if (empty($y)) {
            return false;
        }

        // Define params
        $status = $y[0]->status;
        $account_type = $y[0]->account_type;

        // Check if user status is ACTIVE
        if ($status == USER_STATUS_ACTIVE or $status == USER_STATUS_PROBATION_ACCESS) {

            $key = sha1($y[0]->email . '_' . $status . '_' . $account_type);

            // Build user session array
            $session_vars = array(
                // More session variables to be added later here.
                'id_user' => $y[0]->id_user,
                'id_string' => $y[0]->id_string,
                'email' => $y[0]->email,
                'status' => $status,
                'account_type' => $account_type,
                'display_name' => $y[0]->first_name,
                'last_name' => $y[0]->last_name,
                'k' => $key,
                'id_company' => $c[0]->id_company,
                'company_name' => $c[0]->company_name,
                'company_id_string' => $c[0]->id_string,
            );

            // Add user record details to session
            $this->CI->session->set_userdata($session_vars);
        }

        // Return array with user data
        return array(
            "status" => $status,
            "account_type" => $account_type
        );
    }

// End func login_by_id_string

    /**
     * Get account types
     * @author obinna ukpabi
     * @access public
     * @return bool
     */
    public function get_account_type($id_account_type) {

        $ret = $this->CI->user_model->get_account_types($id_account_type);

        return $ret;
    }

// End func account_types

    /**
     * Get access level
     * @author obinna ukpabi
     * @access public
     * @return bool
     */
    public function get_access_level($id_user) {

        $ret = $this->CI->user_model->get_access_level($id_user);

        return $ret->id_access_level;
    }

// End func access level

    public function get_employee_phone_number($id_user) {

        return $this->CI->employee_model->fetch_employee_primary_number($id_user, EMPLOYEE_PRIMARY_PHONE);
    }

    public function get_all_sub_companies($id_company, $email) {
        $sub = $this->CI->user_model->get_user_sub_companies($email, $id_company);
        if (empty($sub)) {
            if (!$this->CI->user_model->isParent($id_company)) {
                $sub = $this->CI->user_model->fetch_company_groups($id_company, $email);
            }
        }

        return $sub;
    }
    
    public function get_parent_company($id_company, $email){
        if (!$this->CI->user_model->isParent($id_company)) {
            return $this->CI->user_model->get_company_parent($id_company, $email);
        }else{
            return $this->CI->user_model->get_parent_company($id_company, $email);
        }
    }

// End func get_employee_phone_number
}

// End class user_auth
// End file user_auth.php