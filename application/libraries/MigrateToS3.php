<?php

use Aws\S3\S3Client;
use OpenCloud\Rackspace;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MigrateToS3
 *
 * @author JosephT
 */
class MigrateToS3 {

    /**
     *
     * @var \OpenCloud\ObjectStore\Service
     */
    private $rsService;

    /**
     *
     * @var S3Client
     */
    private $s3ClientProd;

    /**
     *
     * @var S3Client
     */
    private $s3ClientTest;
    private $s3bucketTest = 'tbresourcetest';
    private $s3bucketProd = 'talentbaseprod';

    public function __construct() {
        $this->s3ClientProd = S3Client::factory(array(
                    'key' => 'AKIAIL7ZJPT4MXEY26BA',
                    'secret' => '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',
        ));

        $this->s3ClientTest = S3Client::factory(array(
                    'key' => 'AKIAJKGAS4CAZ2V3PDBQ',
                    'secret' => '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',
        ));


        $client = new Rackspace(Rackspace::US_IDENTITY_ENDPOINT, array(
            'username' => RACKSPACE_USERNAME,
            'apiKey' => RACKSPACE_API_KEY,
        ));


        $this->rsService = $client->objectStoreService('cloudFiles', 'LON');
    }

    public function start() {
        self::eop('Checking list of containers...');
        $containers = $this->rsService->listContainers();
        self::eop('found', count($containers), ' containers');

        foreach ($containers as $container) {
            $this->moveContainer($container);
        }
    }

    protected function getContainerObjectList(OpenCloud\ObjectStore\Resource\Container $container) {
        $list = array();
        $hasNext = true;
        $lastObject = null;
        do {
            $params = array(
                'limit' => 500,
            );
            if ($lastObject) {
                $params['marker'] = $lastObject->getName();
            }
            /* @var $collection OpenCloud\Common\Collection\PaginatedIterator */
            $collection = $container->objectList();
            foreach ($collection as $object) {
                $list[] = $object;
            }
            $hasNext = $collection->count() > 0 && count($list) < $container->getObjectCount();
        } while ($hasNext);

        return $list;
    }

    /**
     * 
     * @param OpenCloud\ObjectStore\Resource\Container $container
     */
    protected function moveContainer($container) {
        self::eop('processing objects in container: ', $container->name);

        $objectList = $this->getContainerObjectList($container);
        $objectCount = count($objectList);
        self::eop('found: ', $objectCount, 'items');
        $count = 0;
        $isTest = $this->isTest($container->name);
        if ($isTest) {
            self::eop('Using bucket for test');
        } else {
            self::eop('Using bucket for production');
        }

        $bucket = $isTest ? $this->s3bucketTest : $this->s3bucketProd;
        $client = $isTest ? $this->s3ClientTest : $this->s3ClientProd;
        /* @var $client S3Client */
        foreach ($objectList as $object) {
            /* @var $object OpenCloud\ObjectStore\Resource\DataObject */
            self::eop('checking object', ++$count, 'of', $objectCount, ': ', $object->getName());
            $s3path = $this->_s3path($container->name, $object->getName());
            self::eop('S3 Path: ', $s3path, 'checking existence');
            try {
                $headResult = $client->headObject(array(
                    'Bucket' => $bucket,
                    'Key' => $s3path,
                    'IfNoneMatch' => $object->getEtag()
                ));

                //self::eop('Head Result: ', print_r($headResult, true));
            } catch (Exception $ex) {
                $headResult = null;
                self::eop('Exception occured : ', $ex->getMessage());
            }


            if (empty($headResult)) {
                self::eop('Pushing to S3...');
                $body = file_get_contents($object->getTemporaryUrl(300, 'GET'));
                try {
                    $status = $client->putObject(array(
                        'Body' => $body,
                        'Bucket' => $bucket,
                        'Key' => $s3path,
                        'ContentType' => $object->getContentType(),
                        'ACL' => $container->isCdnEnabled() ? 'public-read' : 'private'
                    ));

                    if (!empty($status)) {
                        self::eop('File successfully uploaded to : ', $status['ObjectURL']);
                    } else {
                        self::eop('Seems upload failed.');
                    }
                } catch (Exception $ex) {
                    self::eop('Could not upload: ', $ex->getMessage(), "\n\t", $ex->getTraceAsString());
                }
            } else {
                self::eop('Object exists... skipping');
            }
        }
    }

    private function _s3path($container, $object) {
        //$bucket = $this->isTest($container) ? $this->s3bucketTest : $this->s3ClientProd;
        //return "{$bucket}/{$container}/{$object}";
        return "{$container}/{$object}";
    }

    protected function isTest($container) {
        return preg_match('/^tbtest_/i', $container);
    }

    public static function eop() {
        $text = join(' ', func_get_args());

        echo date('[Y-m-d H:i:s]'), $text, PHP_EOL;
    }

}
