<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of packageinfo
 *
 * @author JosephT
 */
class Packageinfo {

    //put your code here
    protected $info = array();
    private static $instance;

    private function __construct() {
        $jsonfile = rtrim(APP_ROOT, '/\\') . '/application/config/packageinfo.json';
        if (file_exists($jsonfile)) {
            $this->info = json_decode(file_get_contents($jsonfile), true);
        }
    }

    public function get($name, $default = null) {
        return array_key_exists($name, $this->info) ? $this->info[$name] : $default;
    }

    public function __get($name) {
        return $this->get($name);
    }

    /**
     * 
     * @return Packageinfo
     */
    public static function instance() {
        if (!static::$instance) {
            static::$instance = new static();
        }
        
        return static::$instance;
    }

}
