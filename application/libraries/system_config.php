<?php

if(!class_exists('lib')){
    require_once 'lib.php';
}
/**
 * Description of system_config
 *
 * @author JosephT
 */
class System_config extends lib {

    //put your code here
    const TABLE_SYSTEM_CONFIG = 'system_config';

    protected $config = [];

    public function get($configName, $default = null) {        
        if (!array_key_exists($configName, $this->config)) {
            $rs = $this->db
                    ->where('config_name', $configName)
                    ->get_where(self::TABLE_SYSTEM_CONFIG)
                    ->result();
            
            foreach ($rs as $config) {
                $this->config[$config->config_name] = $config->config_value;
            }
        }

        return isset($this->config[$configName]) ? $this->config[$configName] : $default;
    }

}
