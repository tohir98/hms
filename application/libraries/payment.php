<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'lib.php';
require_once 'payment/PaymentHandlerAbstract.php';
require_once 'payment/PaymentPayoutHandler.php';
require_once 'payment/PaymentReceiptHandler.php';

/**
 * Description of payment
 *
 * @paymentor JosephT
 * @property Payment_model $pay_model payment model
 * @property Payroll_model $payroll payment model
 * @property Db_expression $db_expression
 * @property mailer $mailer
 * @property Employee_model $e_model
  1 */
class Payment extends lib {

    const CONFIRM_CODE_TTL = 900;

    public static $ourAccountData = array(
        'id_bank' => 2,
        'account_no' => '0021113211',
        'account_name' => 'TalentBase',
        'sort_code' => '023150005',
    );
    //put your code here

    protected $lastError = null;
    protected $companyConfig = null;
    public static $PAYMENT_STATUSES = array(
        PAYMENT_STATUS_PENDING => 'Pending',
        PAYMENT_STATUS_DONE => 'Done',
        PAYMENT_STATUS_QUEUED => 'Queued',
        PAYMENT_STATUS_FAILED => 'Failed',
        PAYMENT_STATUS_CANCELED => 'Cancelled',
        PAYMENT_STATUS_ONGOING => 'In progress',
    );

    /**
     *
     * @var array
     */
    private $availableHandlers = [];

    public function __construct() {
        parent::__construct();
        $this->_loadAdapters(__DIR__ . '/payment/handlers', $this->availableHandlers, array('_payout', '_receipt'));
        $this->load->model('payment/payment_model', 'pay_model');
        $this->load->library(['db_expression', 'mailer']);
        $this->load->helper(['payroll']);
    }

    protected function getCompanyConfig($companyId = null) {

        if ($companyId !== null || ($companyId = $this->user_auth->get('id_company'))) {
            return $this->pay_model->loadCompanyConfig($companyId);
        }

        return [];
    }

    public function getAllowedMethods($companyId) {
        $config = $this->getCompanyConfig($companyId);
        return array_key_exists(PAYMENT_CONFIG_ALLOWED_ADAPTERS, $config) ? $config[PAYMENT_CONFIG_ALLOWED_ADAPTERS] : [];
    }

    public function getCompanyHandlers($companyId = null, $type = PaymentPayoutHandler::TYPE_ALL) {
        $allowedHandlers = $this->getAllowedMethods($companyId);

        return array_filter($this->getHandlers(), function(PaymentHandlerAbstract $handler) use ($type, $allowedHandlers) {
            if (empty($allowedHandlers) || in_array($handler->getId(), $allowedHandlers)) {
                if (($type & PaymentPayoutHandler::TYPE_PAYOUT) && is_a($handler, PaymentPayoutHandler::class)) {
                    return true;
                }

                if (($type & PaymentPayoutHandler::TYPE_RECEIPT) && is_a($handler, PaymentReceiptHandler::class)) {
                    return true;
                }
            }
            return false;
        });
    }

    /**
     * @return PaymentPayoutHandler[] list of handlers
     */
    public function getHandlers() {
        $handlers = [];
        foreach (array_keys($this->availableHandlers) as $id) {
            $handlers[$id] = $this->getHandler($id);
        }

        uasort($handlers, function(PaymentHandlerAbstract $a, PaymentHandlerAbstract $b) {
            if ($a->getPriority() > $b->getPriority()) {
                return 1;
            } else if ($a->getPriority() < $b->getPriority()) {
                return -1;
            } else {
                return 0;
            }
        });

        return $handlers;
    }

    /**
     * 
     * @staticvar array $handlerStore
     * @param string $id
     * @return PaymentHandlerAbstract
     */
    public function getHandler($id) {
        static $handlerStore = [];

        if ($id && array_key_exists($id, $this->availableHandlers)) {
            if (!in_array($id, $handlerStore) && $this->availableHandlers[$id]['class']) {
                $handlerStore[$id] = new $this->availableHandlers[$id]['class']();
            }

            return $handlerStore[$id];
        }

        throw new RuntimeException('ID cannot be empty and must be valid [' . $id . ']');
    }

    /**
     * 
     * @param string $id
     * @return PaymentReceiptHandler the handler
     */
    public function getReceiptHandler($id) {
        $handler = $this->getHandler($id);
        if ($handler && is_a($handler, PaymentReceiptHandler::class)) {
            return $handler;
        }
        throw new RuntimeException("Handler : {$handler->getName()} is not a receipt handler");
    }

    /**
     * 
     * @param string $id
     * @return PaymentPayoutHandler the handler
     */
    public function getPayoutHandler($id) {
        $handler = $this->getHandler($id);
        if ($handler && is_a($handler, PaymentPayoutHandler::class)) {
            return $handler;
        }
        throw new RuntimeException("Handler : {$handler->getName()} is not a payout handler");
    }

    public function disbursePayroll($paymentMethod, $idRunRef, $paymentType, $idUserList, $narration = '') {
        $this->load->model('payroll/payroll_model', 'payroll');
        if (!array_key_exists($paymentMethod, $this->getCompanyHandlers())) {
            return $this->_error('The selected payment method is not available.');
        }

        $payrollArchiveIds = [];
        $recipients = $this->payroll->getRecipientAccounts($idRunRef, $paymentType, $idUserList, $narration, $payrollArchiveIds);

        //create payout
        $payoutId = $this->createPayout($paymentMethod, $idRunRef, $recipients, $paymentType);

        //create payout
        //set to under processing for status
        //$this->payroll->setPayStatus($payrollArchiveIds, $paymentType, PAYMENT_STATUS_QUEUED);

        return $payoutId;
    }

    /**
     * 
     * @param type $paymentMethod
     * @param type $ref
     * @param \payment\PayoutRecipient[] $recipients
     * @param type $paymentType
     */
    private function createPayout($paymentMethod, $ref, array $recipients, $paymentType) {

        $this->db->trans_start();

        $handler = $this->getHandler($paymentMethod);
        $payoutData = array(
            'id_user' => $this->user_auth->get('id_user'),
            'id_company' => $this->user_auth->get('id_company'),
            'ref' => $ref,
            'status' => PAYMENT_STATUS_PENDING,
            'gateway' => $paymentMethod,
            'batches' => ceil(count($recipients) / $handler->getMaxPayeePerBatch()),
            'payment_type' => $paymentType
        );

        if ($this->db->insert(Payment_model::TABLE_PAYOUT, $payoutData) && ($payoutId = $this->db->insert_id())) {
            //insert, then get insert ID
            $recipientRows = [];

            foreach ($recipients as $recipient) {
                $recipientRows[] = array_merge($recipient->asArray(), array(
                    'id_payment_payout' => $payoutId,
                    'processing_fee_gateway' => $handler->computeGatewayProcessingFee($recipient->getAmount(), PaymentPayoutHandler::TYPE_PAYOUT),
                    'processing_fee_ours' => $handler->computeOurProcessingFee($recipient->getAmount()),
                ));
            }

            $this->db->insert_batch(Payment_model::TABLE_PAYOUT_RECIPIENT, $recipientRows);
        }

        $this->db->trans_complete();

        return $payoutId;
    }

    public function _error($str) {
        $this->lastError = $str;

        return false;
    }

    public function getLastError() {
        return $this->lastError;
    }

    public function getView($action, PaymentHandlerAbstract $handler) {
        $cls = $reflection = new ReflectionObject($handler);
        $dir = dirname($cls->getFileName());

        $file = $dir . '/views/_' . preg_replace('/[^A-Za-z0-9_]+/', '', $action) . '.php';
        return realpath($file);
    }

    public function getBanks() {
        static $banks = [];
        if (empty($banks)) {
            $r = $this->db->get_where('banks')->result_array();
            foreach ($r as $aBank) {
                $banks[$aBank['id_bank']] = $aBank;
            }
        }
        return $banks;
    }

    public function createAndSendConfirmationCode($payout) {

        $confirmCode = rand(100001, 999999);

        if (intval($payout->confirmation_retries) > PAYMENT_CONFIRMATION_MAX_RETRIES) {
            return -1;
        }

        $this->db->update(Payment_model::TABLE_PAYOUT, array(
            'confirmation_code' => $confirmCode,
            'confirmation_code_expiry' => $this->db_expression->make('DATE_ADD(now(), INTERVAL ' . self::CONFIRM_CODE_TTL . '  SECOND)'),
            'confirmation_retries' => $this->db_expression->make('1 + COALESCE(confirmation_retries, 0)'),
                ), array('id' => intval($payout->id)), 1);

        if ($this->db->affected_rows()) {
            $this->_sendConfirmationCodeEmail($confirmCode, $payout);
            $this->_sendConfirmationCodeSms($confirmCode);
            return true;
        }

        return false;
    }

    private function _sendConfirmationCodeEmail($confirmCode, $payout) {
        $data = array(
            'confirmation_code' => $confirmCode,
            'name' => $this->user_auth->get('first_name') . " " . $this->user_auth->get('last_name'),
            'total_payout' => $this->pay_model->getTotalPayout($payout->id),
            'processing_fee' => $this->pay_model->getTotalProcessingFee($payout->id),
        );
        //send as SMS
        $email = $this->load->view('email_templates/payment_payout_confirmation', $data, true);
        return $this->mailer
                        ->setFrom(TALENTBASE_SUPPORT_EMAIL, TALENTBASE_BUSINESS_NAME)
                        ->sendMessage('Payment Transfer Confirmation', $email, $this->user_auth->get('email'))
        ;
    }

    private function _sendConfirmationCodeSms($confirmCode) {
        //don't put this in constructor, will break CLI
        $this->load->model('user/employee_model', 'e_model');
        $payoutConfirmMsg = "Confirmation code for payment transfer: {$confirmCode}";
        $workPhone = $this->e_model->fetch_employee_phone_number($this->user_auth->get('id_user'), WORK_PHONE_TYPE);
        return send_sms($payoutConfirmMsg, $this->user_auth->get('id_user'), $workPhone, 'TalentBase');
    }

    public function verifConfirmationCode($payout, $confirmationCode) {
        return $payout->confirmation_code === $confirmationCode && strtotime($payout->confirmation_code_expiry) >= time();
    }

    private function _payoutEmailData($payout) {
        return array(
            'payout' => $payout,
            'totalProcessingFee' => $this->pay_model->getTotalProcessingFee($payout->id),
            'totalPayout' => $this->pay_model->getTotalPayout($payout->id),
            'recipientsCount' => $this->pay_model->countRecipients($payout->id, false),
            'owner' => $this->pay_model->getPayoutOwner($payout)
        );
    }

    public function payoutIsDone($payout) {
        $data = $this->_payoutEmailData($payout);
        $message = $this->load->view('email_templates/payment_payout_completed.php', $data, true);
        return $this->mailer->setFrom(TALENTBASE_SUPPORT_EMAIL, TALENTBASE_BUSINESS_NAME)
                        ->sendMessage('Payment Processing Successfully Completed', $message, $data['owner']->email);
    }

    public function payoutHasFailed($payout, $error) {
        $data = $this->_payoutEmailData($payout);
        $data['error'] = $error;
        $message = $this->load->view('email_templates/payment_payout_failed.php', $data, true);
        return $this->mailer->setFrom(TALENTBASE_SUPPORT_EMAIL, TALENTBASE_BUSINESS_NAME)
                        ->sendMessage('Payment Processing Failed', $message, $data['owner']->email);
    }

    public function processPending() {
        $pendingPayouts = $this->pay_model->getPendingPayouts(false);

        self::cliOp("Found", count($pendingPayouts), "pending payout");
        if (count($pendingPayouts) > 0) {
            foreach ($pendingPayouts as $aPayout) {
                $gatewayId = $aPayout->gateway;
                $error = "";
                self::cliOp("Processing:", $aPayout->ref);
                if (($handler = $this->getHandler($gatewayId)) //handler was found
                        && ($status = $handler->processPayout($aPayout, $error)) //status returned
                        && $status['success'] //at least one successful
                        && $status['failure'] < 1 //none failed
                ) {
                    self::cliOp("Successfully processed:", $aPayout->ref);
                } else if ($handler) {
                    self::cliOp("An error occured while processing", $aPayout->ref, "Error: ", $error);
                } else {
                    self::cliOp("Payment handler with ID: ", $gatewayId, "not found.");
                }
            }
        }
    }

    public static function cliOp() {
        echo date('[Y-m-d H:i:s]') . ' [' . getmypid() . ']' . "\t" . join(' ', func_get_args()) . PHP_EOL;
    }

}
