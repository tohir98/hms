<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace payment\receipt;

/**
 * Description of LineItem
 *
 * @author JosephT
 */
class LineItem extends \utils\JSONSerializableObject {

    protected $quantity;
    protected $unitAmount;
    protected $description;
    protected $itemInfo;
    protected $discount = 0;

    public function __construct($unitAmount, $description = null, $quantity = 1, array $itemInfo = array(), $discount=0) {
        $this->quantity = $quantity;
        $this->unitAmount = $unitAmount;
        $this->description = $description;
        $this->itemInfo = $itemInfo;
        $this->discount = $discount;
    }

    public function getQuantity() {
        return $this->quantity;
    }

    public function getUnitAmount() {
        return $this->unitAmount;
    }

    public function getUnitAmountDiscounted() {
        return $this->unitAmount * (100 - $this->discount) / 100.0;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getItemInfo() {
        return $this->itemInfo;
    }

    public function setQuantity($quantity) {
        $this->quantity = $quantity;
        return $this;
    }

    public function setUnitAmount($unitAmount) {
        $this->unitAmount = $unitAmount;
        return $this;
    }

    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    public function setItemInfo($itemInfo) {
        $this->itemInfo = $itemInfo;
        return $this;
    }

    public function getDiscount() {
        return $this->discount;
    }

    public function setDiscount($discount) {
        $this->discount = $discount;
        return $this;
    }

}
