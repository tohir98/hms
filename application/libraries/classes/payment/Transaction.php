<?php

namespace payment;

/**
 * Description of Transaction
 *
 * @author JosephT
 */
class Transaction extends \db\Entity {

    private static $RECEIPTS_CACHE = [];

    /**
     *
     * @var Receipt
     */
    private $receipt;

    public function isPending() {
        return intval($this->status) === PAYMENT_STATUS_PENDING;
    }

    public function isPaid() {
        return intval($this->status) === PAYMENT_STATUS_DONE;
    }

    public function isFailed() {
        return intval($this->status) === PAYMENT_STATUS_FAILED;
    }

    /**
     * 
     * @staticvar \Payment_model $payModel
     * @return Receipt
     */
    public function getPaymentReceipt() {
        static $payModel = null;
        /* @var $payModel \Payment_model */
        if (!$this->receipt) {
            if (!$payModel) {
                $ci = get_instance();
                $ci->load->model('payment/payment_model', 'pay_model');
                $payModel = $ci->pay_model;
            }

            if (!array_key_exists($this->id_payment_receipt, self::$RECEIPTS_CACHE)) {
                self::$RECEIPTS_CACHE[$this->id_payment_receipt] = $payModel->getReceipt($this->id_payment_receipt);
            }

            $this->receipt = self::$RECEIPTS_CACHE[$this->id_payment_receipt];
        }

        return $this->receipt;
    }

    public function getReceiptRef() {
        return $this->getPaymentReceipt()->getRef();
    }
    
    public function getCurrency(){
        return $this->getPaymentReceipt()->getCurrency();
    }

    /**
     * 
     * @staticvar \Payment $paymentLib
     * @return \PaymentReceiptHandler RECEIPT HANDLER
     */
    public function getPaymentHandler(){
        static $paymentLib = null;
        /* @var  $paymentLib \Payment */
        if(!$paymentLib){
            get_instance()->load->library(['payment']);
            $paymentLib = get_instance()->payment;
        }
        
        return $paymentLib->getHandler($this->gateway);
    }
    
    public function getPaymentMethod(){
        return $this->getPaymentHandler()->getName();
    }

    public function __ignoredJsonFields(){
        return ['paymentReceipt', 'paymentHandler'];
    }

    public static function setJsonFormats() {
        
        //2. set filters for each field
        $dateFormatter = function($v) {
            return date('c', strtotime($v));
        };

        static::jsonFilterMap([
            'id' => 'intval',
            'last_updated_on' => $dateFormatter,
            'created_on' => $dateFormatter,
            'status' => 'intval',
            'amount' => 'floatval',
        ]);
        
        //end json formats
    }

}
