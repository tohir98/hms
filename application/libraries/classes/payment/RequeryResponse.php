<?php

namespace payment;

/**
 * Description of PaymentResponse
 *
 * @author intelWorX
 */
class RequeryResponse extends \utils\JSONSerializableObject {

    protected $responseCode;
    protected $amount;
    protected $statusText;
    protected $successful;

    public function __construct($responseCode, $amount = 0, $statusText = null, $successful = false) {
        $this->responseCode = $responseCode;
        $this->amount = floatval($amount);
        $this->statusText = $statusText;
        $this->successful = $successful;
    }

    public function isSuccessful() {
        return $this->successful;
    }

    public function getResponseCode() {
        return $this->responseCode;
    }

    public function getAmount() {
        return $this->amount;
    }

    public function getStatusText() {
        return $this->statusText;
    }

}
