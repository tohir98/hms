<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace payment;

/**
 * 
 * Description of PaymentReceiptModel
 * This interface must be implemened by all models register in model_class column of payment_receipt column
 * this interface is called when payment receipt actions happen.
 *
 * @author JosephT
 */
interface ReceiptModelInterface {


    /**
     * This method is called when the payment tied to this model's object with $receipt->model_object_ref is paid.
     * @param Receipt $receipt
     * @return bool true if successful and false otherwise, it's 
     *      important this method returns a boolean value as this method will be called within a transaction
     *      and the transaction will be rolled back if this fails.
     * 
     */
    public function paymentReceived(Receipt $receipt);

    /**
     * 
     * This method is called when the payment tied to this model's object with $receipt->model_object_ref is cancelled.
     * @param \stdClass $receipt
     * 
     */
    public function paymentCancelled($receipt);
}
