<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace payment;

use Exception;
use payment\receipt\LineItem;
use utils\JSONSerializableObject;

/**
 * Description of Receipt
 *
 * @author JosephT
 */
class Receipt extends JSONSerializableObject {

    //put your code here

    protected $ref;
    protected $companyId;
    protected $userId;
    protected $description;
    protected $modelClass;
    protected $modelObjectRef;
    protected $id;
    protected $nextUrl;
    protected $status = 0;
    protected $vatInclusive = true;
    protected $vatRate = 5.0;
    protected $currency = CURRENCY_DEFAULT_ISO;
    protected $createdOn;
    protected $userGenerated = true;

    const REF_LENGTH = 8;

    /**
     *
     * @var ReceiptModelInterface
     */
    private $model;

    /**
     *
     * @var LineItem[]
     */
    protected $items = [];

    public function __construct($ref, $companyId, $modelClass, array $items, $description = '') {
        $this->ref = $ref;
        $this->companyId = $companyId;
        $this->modelClass = $modelClass;
        $this->description = $description;

        $filtered = array_filter($items, function($item) {
            return is_a($item, LineItem::class);
        });

        if (!assert(!empty($filtered) && count($filtered) === count($items), 'Items is a set of line items')) {
            throw new Exception('Items must be a set of');
        }

        $this->items = $items;
    }

    public function __ignoredJsonFields() {
        return array('model', 'modelClass', 'modelObjectRef');
    }

    /**
     * 
     * Returns the CI model that owns the receipt
     * @return ReceiptModelInterface
     * @throws Exception
     */
    public function getModel() {
        if (!$this->model) {
            if (!$this->modelClass) {
                throw new Exception('Payment does not have a model class');
            }

            $ci = get_instance();
            $modelName = 'payment_receipt_model_' . preg_replace('/[^a-zA-Z]+/', '_', $this->modelClass);
            $ci->load->model($this->modelClass, $modelName);
            $this->model = $ci->$modelName;
        }

        return $this->model;
    }

    public function getRef() {
        return $this->ref;
    }

    public function getCompanyId() {
        return $this->companyId;
    }

    public function getUserId() {
        return $this->userId;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getModelClass() {
        return $this->modelClass;
    }

    public function getModelObjectRef() {
        return $this->modelObjectRef;
    }

    public function setRef($ref) {
        $this->ref = $ref;
        return $this;
    }

    public function setCompanyId($companyId) {
        $this->companyId = $companyId;
        return $this;
    }

    public function setUserId($userId) {
        $this->userId = $userId;
        return $this;
    }

    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    public function setModelClass($modelClass) {
        $this->modelClass = $modelClass;
        return $this;
    }

    public function setModelObjectRef($modelObjectRef) {
        $this->modelObjectRef = $modelObjectRef;
        return $this;
    }

    /**
     * 
     * @return LineItem[]
     */
    public function getItems() {
        return $this->items;
    }

    public static function generateRef($prefix = 'T') {
        get_instance()->load->helper(['string']);
        return strtoupper($prefix) . random_string('numeric', self::REF_LENGTH);
    }

    public function getItemTotal() {
        $total = 0;
        foreach ($this->items as $anItem) {
            $total += $anItem->getQuantity() * $anItem->getUnitAmountDiscounted();
        }

        return $total;
    }

    public function getVatAmount() {
        return $this->getItemTotal() * $this->vatRate / 100.00;
    }

    public function getTotalAmount() {
        if ($this->vatInclusive) {
            return $this->getItemTotal();
        } else {
            return (1 + ($this->vatRate / 100.00)) * $this->getItemTotal();
        }
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getMemo() {
        $memo = "";
        if ($this->description) {
            $memo .= $this->description . ": ";
        }
        $lItems = array();
        foreach ($this->items as $item) {
            $lItems[] = $item->getDescription();
        }
        $memo .= join('; ', $lItems);
        return $memo;
    }

    public function getNextUrl() {
        return $this->nextUrl;
    }

    public function setNextUrl($nextUrl) {
        $this->nextUrl = $nextUrl;
        return $this;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = intval($status);
        return $this;
    }

    public function isPending() {
        return $this->status === PAYMENT_STATUS_PENDING;
    }

    public function isPaid() {
        return $this->status === PAYMENT_STATUS_DONE;
    }

    public function isFailed() {
        return $this->status === PAYMENT_STATUS_FAILED;
    }

    public function isCancelled() {
        return $this->status === PAYMENT_STATUS_CANCELED;
    }

    public function isVatInclusive() {
        return $this->vatInclusive;
    }

    public function getVatRate() {
        return $this->vatRate;
    }

    public function setVatInclusive($vatInclusive) {
        $this->vatInclusive = $vatInclusive;
        return $this;
    }

    public function setVatRate($vatRate) {
        $this->vatRate = $vatRate;
        return $this;
    }

    public function getCurrency() {
        return $this->currency;
    }

    public function setCurrency($currency) {
        $this->currency = $currency;
        return $this;
    }

    public function getCreatedOn() {
        return $this->createdOn;
    }

    public function setCreatedOn($createdOn) {
        $this->createdOn = date('c', strtotime($createdOn));
        return $this;
    }

    public function isUserGenerated() {
        return $this->userGenerated;
    }

    public function setUserGenerated($userGenerated) {
        $this->userGenerated = (bool) $userGenerated;
        return $this;
    }

    /**
     * 
     * @param \stdClass $receipt
     * @param array $items
     * @return self
     */
    public static function fromRow($receipt, array $items) {
        $obj = new self($receipt->receipt_ref, $receipt->id_company, $receipt->model_class, $items, $receipt->description);
        $obj->setId($receipt->id)
                ->setModelObjectRef($receipt->model_object_ref)
                ->setNextUrl($receipt->on_success_url)
                ->setUserId($receipt->id_user)
                ->setStatus($receipt->payment_status)
                ->setVatInclusive(!!$receipt->vat_inclusive)
                ->setVatRate(floatval($receipt->vat_rate))
                ->setCurrency($receipt->currency_iso)
                ->setCreatedOn($receipt->created_on)
                ->setUserGenerated(!!$receipt->is_user_generated)
        ;

        return $obj;
    }

}
