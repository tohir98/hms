<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace payment;

/**
 * Description of Recipient
 *
 * @author JosephT
 */
class PayoutRecipient {
    
    
    private $bankId;
    private $userId;
    private $accountName;
    private $accountNumber;
    private $sortCode;
    private $description;
    private $amount;
    private $refObjectTable;
    private $refObjectId;
    
    public function __construct($userId, $bankId, $accountName, $accountNumber, $amount, $sortCode = null, $description=null) {
        $this->bankId = $bankId;
        $this->userId = $userId;
        $this->accountName = $accountName;
        $this->accountNumber = $accountNumber;
        $this->sortCode = $sortCode;
        $this->description = $description;
        $this->amount = floatval($amount);
        if(!$sortCode){
            
        }
    }

    public function getAmount() {
        return $this->amount;
    }

    public function setAmount($amount) {
        $this->amount = $amount;
        return $this;
    }

        public function getBankId() {
        return $this->bankId;
    }

    public function getUserId() {
        return $this->userId;
    }

    public function getAccountName() {
        return $this->accountName;
    }

    public function getAccountNumber() {
        return $this->accountNumber;
    }

    public function getSortCode() {
        return $this->sortCode;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setBankId($bankId) {
        $this->bankId = $bankId;
        return $this;
    }

    public function setUserId($userId) {
        $this->userId = $userId;
        return $this;
    }

    public function setAccountName($accountName) {
        $this->accountName = $accountName;
        return $this;
    }

    public function setAccountNumber($accountNumber) {
        $this->accountNumber = $accountNumber;
        return $this;
    }

    public function setSortCode($sortCode) {
        $this->sortCode = $sortCode;
        return $this;
    }

    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    public function getRefObjectTable() {
        return $this->refObjectTable;
    }

    public function getRefObjectId() {
        return $this->refObjectId;
    }

    public function setRefObjectTable($refObjectTable) {
        $this->refObjectTable = $refObjectTable;
        return $this;
    }

    public function setRefObjectId($refObjectId) {
        $this->refObjectId = $refObjectId;
        return $this;
    }


    public function asArray(){
        return array(
            'id_user' => $this->userId,
            'id_bank' => $this->bankId,
            'account_name' => $this->accountName,
            'sort_code' => $this->sortCode,
            'account_no' => $this->accountNumber,
            'description' => $this->description,
            'amount' => $this->amount,
            'ref_object_table' => $this->refObjectTable,
            'ref_object_id' => $this->refObjectId,
        );
    }
    
}
