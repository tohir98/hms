<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace payment;

/**
 * Description of ProcessingFee
 *
 * @author JosephT
 */
class ProcessingFee {

    //put your code here
    const TYPE_FIXED = 'fixed';
    const TYPE_VARIABLE = 'variable';

    private $type;
    private $value;
    private $inclusive;
    private $cap;

    /**
     * 
     * @param array $params
     *      Parameters contain: 
     *      - type: fixed or variable
     *      - value: the value, should be in percentage for variable and unit of currency for fixed.
     *      - inclusive: if the processing fee is inclusive
     *      - cap : is there a cap to transaction fee?
     */
    public function __construct(array $params) {
        $this->type = $params['type'] ? : self::TYPE_FIXED;
        $this->value = floatval($params['value']);
        $this->inclusive = isset($params['inclusive']) && $params['inclusive'];
        $this->cap = isset($params['cap']) ? floatval($params['cap']) : -1;
    }

    public function getType() {
        return $this->type;
    }

    public function getValue() {
        return $this->value;
    }

    public function getCap() {
        return $this->cap;
    }

    public function isInclusive() {
        return $this->inclusive;
    }

    public function compute($amount) {
        if ($this->type === self::TYPE_FIXED) {
            $f = $this->value;
        } elseif ($this->inclusive) {
            $f = ($this->value * $amount) / (100.0 - $this->value);
        } else {
            $f = ($amount * $this->value) / 100.0;
        }

        //is there a cap?
        return $this->cap > 0 ? min([$this->cap, $f]) : $f;
    }

}
