<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api;

/**
 * Description of UserInfoShort
 *
 * @author JosephT
 */
class UserInfoShort extends UserBase {

    public function jsonSerialize() {
        return array(
            'id' => (int) $this->id_user,
            'id_staff' => $this->id_staff,
            'id_user_string' => $this->id_string,
            'first_name' => $this->first_name,
            'middle_name' => $this->middle_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'profile_picture' => $this->getProfilePictureUrl(),
            'role' => $this->roles,
            'department' => $this->departments,
            'location' => array(
                'id' => $this->id_company_location,
                'address' => $this->location_address,
                'city' => $this->location_city,
                'state' => $this->location_state,
            ),
            'status' => $this->getStatus()
        );
    }

}
