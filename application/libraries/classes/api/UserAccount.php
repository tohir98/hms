<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api;

/**
 * Description of UserAccount
 *
 * @author JosephT
 */
class UserAccount extends UserBase {

    public function __construct() {
        if(!get_instance()->storage){
            get_instance()->load->library('storage');
        }
    }
    public function jsonSerialize() {
        return array(
            'id' => intval($this->id_user),
            'id_user_string' => $this->u_id_string,
            'id_company' => $this->id_company,
            'id_company_string' => $this->c_id_string,
            'company_name' => $this->company_name,
            'website' => $this->website,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'middle_name' => $this->middle_name,
            'access_level' => $this->getAccessLevel(),
            'profile_picture' => $this->getProfilePictureUrl(),
            'logo_url' => $this->logo_path ?
                    get_instance()->storage->temporary_url($this->logo_path, STORAGE_CONTAINER_ASSETS) :
                    site_url('/img/talentbase-logo.png'),
        );
    }

}
