<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api;

/**
 * Description of Task
 *
 * @author JosephT
 */
class Task extends \db\Entity {

    protected static $tasks = array(
        'pending' => TASK_STATUS_TO_DO,
        'completed' => TASK_STATUS_COMPLETE,
        'in_progress' => TASK_STATUS_IN_PROGRESS,
        'declined' => TASK_STATUS_DECLINED,
    );

    public function jsonSerialize() {
        return array(
            'id_task' => $this->id_task,
            'subject' => trim(strip_tags($this->subject)),
            'notes' => $this->notes,
            'status' => self::findStatus($this->status),
            'url' => $this->getTaskUrl(),
            'start_date' => $this->_formatDate($this->date_will_start),
            'end_date' => $this->_formatDate($this->date_will_end),
            'created' => $this->_formatDate($this->date_created, 'c'),
            //'finished' => $this->_formatDate($this->date_finished),
            'from' => array(
                'id' => intval($this->id_user_from),
                'first_name' => $this->first_name,
                'middle_name' => $this->middle_name,
                'last_name' => $this->last_name,
                'email' => $this->email,
            )
        );
    }

    public static function findStatus($status) {
        return array_search($status, static::$tasks);
    }

    public static function getStatus($status_str) {
        return array_key_exists($status_str, self::$tasks) ? self::$tasks[$status_str] : null;
    }

    protected function _formatDate($val, $format = 'Y-m-d') {
        return $val ? date($format, strtotime($val)) : null;
    }

    public function getTaskUrl() {
        $matches = [];
        if(preg_match('/href=[\'"](.+)[\'"]/', $this->subject, $matches)) {
            return $matches[1];
        }
        return null;
    }

}
