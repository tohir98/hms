<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api;

/**
 * Description of UserBase
 *
 * @author JosephT
 */
abstract class UserBase extends \db\Entity {

    protected $phones;

    //put your code here

    public function getProfilePictureUrl() {
        get_instance()->load->library('storage');
        $storage = get_instance()->storage;
        /* @var $storage \storage */
        if ($this->profile_picture_name) {
            return $storage->temporary_url($this->profile_picture_name, $this->cdn_container);
        } else {
            return site_url('/img/profile-default.jpg');
        }
    }

    public function getAccessLevel() {
        if ($this->id_access_level == ACCOUNT_TYPE_ADMINISTRATOR) {
            $r = 'Admin';
        } else if ($this->id_access_level == ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR) {
            $r = 'Secondary Admin';
        } else if ($this->id_access_level == ACCOUNT_TYPE_UNIT_MANAGER) {
            $r = 'Unit Manager';
        } else {
            $r = 'Employee';
        }

        return $r;
    }

    public function getPhones() {
        if (is_null($this->phones)) {
            $phones = $this->_getDb()
                    ->select('*')
                    ->from('employee_phone ep')
                    ->join('phone_types pt', 'pt.id_phone_type=ep.id_phone_type')
                    ->where('id_user', (int) $this->id_user)
                    ->get()
                    ->result()
            ;

            $this->phones = [];
            foreach ($phones as $aPhone) {
                $this->phones[] = array(
                    'type' => $aPhone->phone_type,
                    'country_code' => $aPhone->id_country_code,
                    'number' => $aPhone->phone_number,
                    'ref' => $aPhone->ref_id,
                    'is_primary' => !!$aPhone->primary,
                );
            }
        }

        return $this->phones;
    }

    public function getStatus() {
        static $statuses = array(
            USER_STATUS_INACTIVE => 'Inactive',
            USER_STATUS_ACTIVE => 'Active',
            USER_STATUS_ACTIVE_NO_ACCESS => 'Active (no access)',
            USER_STATUS_PROBATION_ACCESS => 'Probabtion',
            USER_STATUS_PROBATION_NO_ACCESS => 'Probabtion (no access)',
            USER_STATUS_TERMINATED => 'Terminanted',
            USER_STATUS_SUSPENDED => 'Suspended',
        );

        return $statuses[$this->status];
    }

}
