<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api;

/**
 * Description of Device
 *
 * @author JosephT
 */
class UserDevice extends \db\Entity {

    const TYPE_ANDROID = 'android';
    const TYPE_IOS = 'ios';
    const TYPE_WINDOWS = 'windows';
    const TYPE_BLACKBERRY = 'blackberry';

    public static function deviceTypes() {
        return [self::TYPE_ANDROID, self::TYPE_BLACKBERRY, self::TYPE_IOS, self::TYPE_WINDOWS];
    }

}
