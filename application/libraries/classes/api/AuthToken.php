<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api;

/**
 * Description of AuthToken
 *
 * @author JosephT
 */
class AuthToken extends \db\Entity {

    //put your code here
    private $user;

    public function expiryDateISO() {
        return date('c', strtotime($this->expiry_date));
    }

    public function getUser() {
        if (!$this->user) {
            get_instance()->load->model('user/user_model', 'user_model');
            $user_model = get_instance()->user_model;
            /*@var $user_model \User_model */
            $this->user = $user_model
                            ->fetch_account_info($this->id_company, ['u.id_user' => (int) $this->id_user_account_selected], 1, 0, UserInfo::class)[0];
        }

        return $this->user;
    }

    public function get($field) {
        if ($this->getUser()) {
            return $this->user->{$field};
        }

        return null;
    }
    
    public function clearUser(){
        $this->user = null;
        return $this;
    }

    public function getCurrentUserId(){
        return $this->id_user_account_selected;
    }
}
