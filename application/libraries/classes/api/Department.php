<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace api;

/**
 * Description of Department
 *
 * @author JosephT
 */
class Department extends \db\Entity {

    //put your code here

    public function jsonSerialize() {
        $data = array(
            'id_dept' => $this->deptid,
            'name' => $this->departments,
            'child_count' => (int) $this->childs_count,
            'employees_count' => (int) $this->employees_count,
            'parent' => $this->id_parent ? array(
                'id_dept' => $this->id_parent,
                'name' => $this->parent_department,
                    ) : null,
            'head' => $this->id_head ? array(
                'id_user' => $this->id_head,
                'first_name' => $this->first_name,
                'last_name' => $this->last_name,
                'email' => $this->email) : null,
        );

        return $data;
    }

}
