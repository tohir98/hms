<?php

namespace employee;

/**
 * Description of EmployeeChecklistLite
 *
 * @author JosephT
 */
class EmployeeChecklistLite extends BaseChecklistEntity {

    public function jsonSerialize() {
        return array_merge(parent::jsonSerialize(), array(
        ));
    }

}

EmployeeChecklistLite::jsonFilterMap(array(
    'id_user' => 'intval',
    'task_count' => 'intval',
    'date_assigned' => function($c) {
        return date_format_strtotime($c, 'c');
    },
    'approval_status' => function($k) {
        if (strlen($k) < 1) {
            return '-None-';
        }

        $approvalStatuses = [
            APPROVAL_STATUS_PENDING => 'Pending',
            APPROVAL_STATUS_APPROVED => 'Approved',
            APPROVAL_STATUS_CANCELED => 'Canceled',
            APPROVAL_STATUS_DECLINED => 'Declined',
        ];
        $tmp = explode('|', $k);
        $statuses = [];
        foreach ($tmp as $p) {
            $k = $approvalStatuses[$p];
            $statuses[$k] = isset($statuses[$k]) ? ($statuses[$k] + 1) : 1;
        }

        $statuses['total'] = count($tmp);
        return $statuses;
    },
            'completion_status' => function($a) {
                return Checklist::completionStatuses()[intval($a)];
    }
        ));
        