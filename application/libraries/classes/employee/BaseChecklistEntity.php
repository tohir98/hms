<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace employee;

/**
 * Description of BaseChecklistEntity
 *
 * @author JosephT
 * 
 * @property \Checklist_Model $checklist_model checklist db abstraction
 */
abstract class BaseChecklistEntity extends \db\Entity {

    public function __construct() {
        if (!$this->checklist_model) {
            $this->load->model('user/checklist_model', 'checklist_model');
        }
    }

}
