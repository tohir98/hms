<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace employee;

/**
 * Description of Checklist
 *
 * @author JosephT
 * 
 */
class Checklist extends BaseChecklistEntity {

    private $itemsCount = -1;
    private $employeesCount = -1;
    private $items = null;

    public function getItemsCount() {
        if ($this->itemsCount < 0) {
            $this->itemsCount = $this->checklist_model->countItems($this->id);
        }
        return $this->itemsCount;
    }

    public function getEmployeesCount() {
        if ($this->employeesCount < 0) {
            $this->employeesCount = $this->checklist_model->countAssignedEmployee($this->id);
        }

        return $this->employeesCount;
    }

    /**
     * 
     * @return ChecklistItem
     */
    public function getItems() {
        if ($this->items === null) {
            $this->items = $this->checklist_model->getChecklistItems($this->id);
        }

        return $this->items;
    }

    /**
     * 
     * @return ChecklistApprover[]
     */
    public function getApprovers() {
        return $this->checklist_model->getChecklistApprovers($this->id);
    }

    public function approverIds() {
        $c = [];
        foreach ($this->getApprovers() as $approver) {
            $c[] = $approver->id_user;
        }
        return $c;
    }

    public function jsonSerialize() {
        return array_merge(
            parent::jsonSerialize(),
            array(
                'approvers' => $this->approverIds(),
            )
        );
    }
    
    public static function completionStatuses(){
        return array(
            CHECKLIST_STATUS_COMPLETED => 'Completed',
            CHECKLIST_STATUS_ONGOING => 'Ongoing',
            CHECKLIST_STATUS_PENDING => 'Pending',
            CHECKLIST_STATUS_STOPPED => 'Stopped',
        );
    }

}
