<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace employee;

/**
 * Description of UserBasicInfo
 *
 * @author JosephT
 */
class UserBasicInfo extends \db\Entity {

    //put your code here

    public function jsonSerialize() {
        return array(
            'id_user' => $this->id_user,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'department' => $this->departments,
            'role' => $this->roles,
        );
    }

}
