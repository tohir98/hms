<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of page_nav
 *
 * @author JosephT
 */
abstract class Page_nav {

    protected $CI;

    public function __construct() {
        $this->CI = & get_instance();
    }

    abstract public function get_assoc();

// End func get_assoc
    public function run_page_with_view($view_script, $data, $title = 'TalentBase', $activity_bar = false, $show_top_menu = true, $show_side_bar = true, $minimal = false) {
        return $this->run_page(null, $title, $activity_bar, $show_top_menu, function() use ($view_script, $data) {
                    $this->CI->load->view($view_script, $data, false);
                }, $show_side_bar, $minimal);
    }

    abstract public function get_user_link();

    abstract public function get_top_menu();
    abstract public function company_name();

    
    public function run_page($buffer, $title = 'HMS', $activity_bar = true, $show_top_menu = true, $content_cb = '', $show_side_bar = true, $minimal = false) {

        $header_data = array(
            'page_title' => $title
        );


        $top_menu_data = $left_sidebar_data = [];

        if (!$minimal) {
            $user_link = $this->get_user_link();
            //1
            $top_menu_data = $this->get_top_menu();
            $nav_menu = $this->get_assoc();

            $top_menu_data['top_nav_menu'] = build_top_nav_part($nav_menu, $user_link);
           
            //2
            $left_sidebar_data = array(
                'company_name' => $this->company_name(),
                'side_nav_menu' => build_sidebar_nav_part($nav_menu)
                    );
            
        }

        $page_parts = array_merge(array(
            'page_content' => $buffer,
            'content_cb' => $content_cb,
            //'footer' => $footer_buffer,
            'activity_bar' => $activity_bar,
            'isMinimal' => $minimal,
            'showSideBar' => $show_side_bar,
                ), $header_data, $left_sidebar_data, $top_menu_data);

        $this->CI->load->view('templates/page', $page_parts);
    }

}
