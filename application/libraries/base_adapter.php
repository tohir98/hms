<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

interface Base_adapter {

    /**
     * returns the system identifiable ID for this adapater, 
     * used in URL and database identification, must be 
     * alphanumeric and contain no spaces.
     * 
     */
    public static function getId();

    /**
     * Returns user-friendly name for provider
     */
    public static function getName();

    /**
     * Returns true if provider is globally active.
     */
    public static function active();

}
