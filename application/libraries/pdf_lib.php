<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Talentbase
 * PDF generator lib
 * 
 * @category   Library
 * @package    Company
 * @subpackage PDF Generator
 * @author     Chuks O. <chuks.olloh@hired.com.ng>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

class Pdf_lib extends fpdf\FPDF {
	
	/**
	 * Codeigniter instance
	 * 
	 * @access private
	 * @var object
	 */
	private $CI;
	
	/**
	 * Company name
	 * 
	 * @access private
	 * @var string
	 */
	private $company_name = 'TalentBase.NG';
	
	/**
	 * Page heading
	 * 
	 * @access private
	 * @var string
	 */
	private $heading = 'Copyright @ 2014. ';
	
	/**
	 * Footer message
	 * 
	 * @access private
	 * @var string
	 */
	private $footer = '';
	
	private $HREF;
	
	/**
	 * Class construct
	 * 
	 * @access public
	 * @return void 
	 */
	public function __construct() {
		
		parent::__construct();
		
		// Load CI
		$this->CI = get_instance();
		
	}// End func __construct
	
	/**
	 * Build PDF header
	 * 
	 * @access public
	 * @return void
	 */
	public function header(){
		
		// Arial bold 15
		$this->SetFont('Arial','B',15);
		// Move to the right
		$this->Cell(80);
		// Title
		$this->Cell(30,10,$this->heading,0,0,'C');
		// Line break
		$this->Ln(20);
		
	}// End func header
	
	/**
	 * Build PDF footer
	 * 
	 * @access public
	 * @return void
	 */
	public function footer(){
		
		// Position at 1.5 cm from bottom
		$this->SetY(-15);
		// Arial italic 8
		$this->SetFont('Arial','I',8);
		// Page number
		$this->Cell(0,10, $this->footer .' Page '.$this->PageNo().'/{nb}',0,0,'C');
		
	}// End func footer
	
	public function WriteHTML($html)
	{
		// HTML parser
		$html = str_replace("\n",' ',$html);
		$a = preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
		foreach($a as $i=>$e)
		{
			if($i%2==0)
			{
				// Text
				if($this->HREF)
					$this->PutLink($this->HREF,$e);
				else
					$this->Write(5,$e);
			}
			else
			{
				// Tag
				if($e[0]=='/'){
					$this->CloseTag(strtoupper(substr($e,1)));
				}
				else
				{
					// Extract attributes
					$a2 = explode(' ',$e);
					$tag = strtoupper(array_shift($a2));
					$attr = array();
					foreach($a2 as $v)
					{
						if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
							$attr[strtoupper($a3[1])] = $a3[2];
					}
					$this->OpenTag($tag,$attr);
				}
			}
		}
	}

	public function OpenTag($tag, $attr)
	{
		// Opening tag
		if($tag=='B' || $tag=='I' || $tag=='U')
			$this->SetStyle($tag,true);
		if($tag=='A')
			$this->HREF = $attr['HREF'];
		if($tag=='BR')
			$this->Ln(5);
	}

	public function CloseTag($tag)
	{
		// Closing tag
		if($tag=='B' || $tag=='I' || $tag=='U')
			$this->SetStyle($tag,false);
		if($tag=='A')
			$this->HREF = '';
	}

	public function SetStyle($tag, $enable)
	{
		// Modify style and select corresponding font
		$this->$tag += ($enable ? 1 : -1);
		$style = '';
		foreach(array('B', 'I', 'U') as $s)
		{
			if($this->$s>0){
				$style .= $s;
			}
		}
		$this->SetFont('',$style);
	}

	public function PutLink($URL, $txt)
	{
		// Put a hyperlink
		$this->SetTextColor(0,0,255);
		$this->SetStyle('U',true);
		$this->Write(5,$txt,$URL);
		$this->SetStyle('U',false);
		$this->SetTextColor(0);
	}
	
	/**
	 * Build PDF body page
	 * 
	 * @access public
	 * @return void
	 */
	public function build_pdf(array $params){
		
		if(empty($params)){
			return false;
		}
		
		/*$this->company_name = $params['company_name'];
		$this->heading = $params['heading'];
		$this->footer = $params['footer'];*/
		
		$this->AliasNbPages();
		$this->AddPage();
		$this->SetFont('Times','',12);
		
//		for($i = 1; $i <= 40; $i++){
//			$this->Cell(0,10,'Printing line number '.$i,0,1);
//		}
		
		foreach($params as $k){
			$this->Cell(0,10,'id user : '.$k->id_user,0,1);
		}
		
		$html = '<html><body><h1>hello world</h1></body></html>';
		
		$this->WriteHTML($html);
		
		$this->Output('c:/websrc/test_2.pdf', 'F');
		
	}// End func build_pdf
	
}// End class pdf_helper
