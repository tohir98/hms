<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Talentbase
 * SMS library (for all users)
 * 
 * @category   Library
 * @package    SMS
 * @subpackage SMS
 * @author     Tunmise Akinsola <tunmise.akinsola@talentbase.ng>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */
class sms_lib {

    /**
     * Codeigniter instance
     * 
     * @access private
     * @var object
     */
    private $CI;

    /**
     * Library active state
     * 
     * @access private
     * @var object
     */
    private $library_active = true;

    /**
     * Message type (Plain | Flash)
     * 
     * @access private
     * @var object
     */
    private $msg_type = 0;

    /**
     * Enable or disable delivery report
     * 
     * @access private
     * @var object
     */
    private $delivery_report = 1;

    /**
     * SMS sender
     * 
     * @access private
     * @var object
     */
    private $sender = 'Talentbase';

    /**
     * SMS HTTP API send url
     * 
     * @access private
     * @var object
     */
    private $sms_send_url = 'http://smsgator.com/bulksms';

    /**
     * SMS HTTP API balance url
     * 
     * @access private
     * @var object
     */
    private $sms_balance_url = 'http://smsgator.com/bulksms/balance';

    /**
     * SMS HTTP API status url
     * 
     * @access private
     * @var object
     */
    private $sms_status_url = 'http://smsgator.com/bulksms/status';

    /**
     * Authentication email
     * 
     * @access private
     * @var object
     */
    private $email = 'ozioma.obiaka@talentbase.ng';

    /**
     * Authentication password
     * 
     * @access private
     * @var object
     */
    private $password = 'talentbasesms';

    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        // Load CI object
        $this->CI = get_instance();

        // Load user authentication library
        $this->CI->load->library('user_auth');

        // Load model
        $this->CI->load->model('sms/sms_model');
        $this->CI->load->model('loans/loans_model', 'l_model');

        // Append authentication papameters to urls that require it
        $this->sms_send_url = "{$this->sms_send_url}?email={$this->email}&password={$this->password}";

        $this->sms_balance_url = "{$this->sms_balance_url}?email={$this->email}&password={$this->password}";
    }

    /**
     * Check if payroll id is valid
     *
     * @access public
     * @param array $params
     * @return array $result
     * */
    public function send_sms($params) {

        $result = array();
        if (!isset($params['id_module']) || empty($params['recipients']) || !isset($params['message'])) {
            $result['type'] = 'error';
            $result['msg'] = 'Incomplete parameters!';
            return $result;
        }

        if ((isset($params['dev_mode']) && in_array($params['dev_mode'], array(true, false)))) {
            $this->library_active = $params['dev_mode'];
        }

        if (isset($params['msg_type']) && in_array($params['msg_type'], array(0, 1))) {
            $this->msg_type = $params['msg_type'];
        }

        if (isset($params['sender'])) {
            $this->sender = substr($params['sender'], 0, 11);
        }

        $recipients = $params['recipients'];

        /* CHECK IF COMMPANY HAS SUFFICENT CREDIT TO SEND SMS */
        $res = $this->_check_company_sms_credit_balance(array('id_company' => $this->CI->user_auth->get('id_company')));

        if (!$res) {
            return array(
                'type' => 'error',
                'msg' => 'Insufficient SMS Credit'
            );
        }

        $db_params = array(
            'id_module' => $params['id_module'],
            'id_company' => $this->CI->user_auth->get('id_company'),
            'message' => $params['message'],
            'date' => date('Y-m-d H:i:s'),
            'msg_type' => $this->_get_sms_type($this->msg_type),
            'id_sender' => $this->CI->user_auth->get('id_user'),
            'id_recipient' => $params['recipients']['id_user'],
            'phone_number' => $params['recipients']['number'],
            'time' => date('H:i:s')
                //'units' => $this->_calculate_sms_unit_cost(array('message' => $params['message']))
        );

        $ret = $this->CI->sms_model->log_message($db_params, $recipients);

        if (!$ret) {
            $result['type'] = 'error';
            $result['msg'] = 'Error! Could not log message in database!';
        } else {

            if (!$this->library_active) {
                $result['type'] = 'success';
                $result['msg'] = 'SMS message sent successfully!';
            } else {
                $sendparams['dlr'] = $this->delivery_report;
                $sendparams['message'] = $params['message'];
                $sendparams['type'] = $this->msg_type;
                $sendparams['sender'] = substr($this->sender, 0, 11);

                $url = $this->_prep_url($this->sms_send_url, $sendparams, $recipients['number']);
                $response = $this->_send($url);

                if (isset($response)) {
                    $update_params = array();
                    $res_msg = explode(',', $response);

                    foreach ($res_msg as $msg) {
                        if (!isset($msg) || $msg == '') {
                            continue;
                        }

                        $update_info = explode('|', $msg);
                        if (count($update_info) == 3) {
                            $update_params[] = array(
                                'status' => $this->_get_sms_status_from_code($update_info[0]),
                                'id_sms_api' => $update_info[1],
                                'phone_number' => $update_info[2]
                            );

                            $result['type'] = 'success';
                            $result['msg'] = 'SMS message sent successfully!';

                            // UPDATE SMS_MESSAGES TABLE. - CHUKS
                            $this->CI->sms_model->update_log_message(
                                    array(
                                        'id_message' => $ret,
                                        'data' => array(
                                            'units' => $this->_calculate_sms_unit_cost(array('message' => $params['message'])),
                                            'status' => $this->_get_sms_status_from_code($update_info[0]),
                                            'delivery_report' => 'Success'
                                        ),
                                        'id_company' => $this->CI->user_auth->get('id_company')
                            ));
                        } else {
                            $update_params[] = array(
                                'status' => $this->_get_sms_status_from_code($update_info[0])
                            );

                            $result['type'] = $update_params;
                            $result['msg'] = 'Sms message could not be sent!';
                        }
                    }

                    $this->CI->sms_model->update_messages($ret, $update_params);
                } else {
                    $result['type'] = 'error';
                    $result['msg'] = 'Error! Could not connect to sms server. Please try again!';
                }
            }
        }

        return $result;
    }

// End func send_sms

    /**
     * Check sms balance
     *
     * @access public
     * @param array $params
     * @return mixed (bool | array)
     * */
    public function check_sms_balance() {
        return $this->_send($this->sms_balance_url);
    }

// End func check_sms_balance

    /**
     * Get the type of sms sent (Plain text | Flash message)
     *
     * @access private
     * @param int $type
     * @return string $type_name
     * */
    private function _get_sms_type($type) {
        $type_name = 'Plain Text';
        if ($type == 1) {
            $type_name = 'Flash Message';
        }

        return $type_name;
    }

// End func get_sms_type

    /**
     * Get the status message of sent sms 
     *
     * @access private
     * @param int $code
     * @return string $status
     * */
    private function _get_sms_status_from_code($code) {
        $status = 'Unknown Response';

        $status_codes = array(
            '2601' => 'Success',
            '2602' => 'Invalid Url',
            '2603' => 'Invalid Username/Password',
            '2604' => 'Invalid Type',
            '2605' => 'Invalid Message',
            '2606' => 'Invalid Destination',
            '2607' => 'Invalid Sender',
            '2608' => 'Invalid Delivery Report Type',
            '2609' => 'Invalid Scheduled Date Format',
            '2610' => 'Insufficient Credit',
            '2611' => 'Message Sending/Scheduling Failed'
        );

        if (array_key_exists($code, $status_codes)) {
            $status = $status_codes[$code];
        }

        return $status;
    }

// End func _get_sms_status_from_code

    /**
     * Append all parameters to appropriate url 
     *
     * @access private
     * @param string $url, array $params, mixed (string | array) $recipients
     * @return string $url
     * */
    private function _prep_url($url, $params, $recipients = NULL) {
        foreach ($params as $name => $value) {
            $url .= "&{$name}=" . urlencode($value);
        }

        if (isset($recipients)) {
            if (is_array($recipients)) {
                $url .= '&destination=' . implode(',', $recipients);
            } else {
                $url .= "&destination={$recipients}";
            }
        }

        return $url;
    }

// End func _prep_url

    /**
     * Send request to specified url
     *
     * @access private
     * @param string $url
     * @return string $response
     * */
    private function _send($url) {

        $curl_ch = curl_init();

        curl_setopt($curl_ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl_ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($curl_ch, CURLOPT_URL, $url);

        $response = curl_exec($curl_ch);
        $req_info = curl_getinfo($curl_ch);

        curl_close($curl_ch);

        if ($req_info['http_code'] == 0) {
            return $req_info['http_code'];
        }

        return $response;
    }

// End func _send

    /**
     * 
     * @param array $params
     * @return boolean
     */
    private function _calculate_sms_unit_cost(array $params) {

        if (empty($params)) {
            return false;
        }

        // CHARACTERS LESS THAN 160, RETURN SMS FIXED RATE
        if ($params['message'] > 0 or $params['message'] <= 160) {
            return SMS_PER_UNIT_CHARGE;
        } elseif ($params['message'] > 160 or $params['message'] <= 313) {
            return SMS_PER_UNIT_CHARGE * 2;
        } elseif ($params['message'] > 313 or $params['message'] <= 466) {
            return SMS_PER_UNIT_CHARGE * 3;
        } elseif ($params['message'] > 466 or $params['message'] <= 619) {
            return SMS_PER_UNIT_CHARGE * 4;
        }

        return false;
    }

// End func _calculate_sms_unit_cost

    private function _check_company_sms_credit_balance(array $params) {

        if (empty($params)) {
            return false;
        }

        $res = $this->CI->l_model->fetch_data(
                array(
                    'table' => 'sms_company_info',
                    'where' => array(
                        'id_company' => $params['id_company']
                    )
        ));

        if (empty($res)) {
            $this->CI->l_model->insert(array(
                'table' => 'sms_company_info',
                'vals' => array(
                    'id_company' => $params['id_company'],
                    'total_units' => '50.00',
                    'date_created' => date('Y/m/d'),
                    'time_created' => date('H:i:s')
                )
            ));

//			$this->CI->l_model->insert(array(
//				'table' => 'sms_debit_credit_transactions',
//				'vals' => array(
//					'date' => date('Y/m/d'),
//					'time' => date('H:i:s'),
//					'credit' => '50.00',
//					'id_company' => $params['id_company'],
//					'balance' => '50.00'
//				)
//			));
//			
//			$this->CI->l_model->insert(array(
//				'table' => 'sms_transfer_credits',
//				'vals' => array(
//					'date' => date('Y/m/d'),
//					'time' => date('H:i:s'),
//					'units' => '50.00',
//					'status' => 'Success',
//					'id_transfer_type' => 1,
//					'id_company' => $params['id_company']
//				)
//			));

            $res = $this->CI->l_model->fetch_data(array(
                'table' => 'sms_company_info',
                'where' => array(
                    'id_company' => $params['id_company']
                )
            ));
        }

        $i = $res[0];

        if ($i->total_units > 2) {
            return true;
        }

        return false;
    }

// End func _check_company_sms_credit_balance
}
