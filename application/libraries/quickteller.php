<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Talentbase
 * Tasks library (for all users)
 * 
 * @category   Library
 * @package    Payroll
 * @subpackage CSV
 * @author     Obinna Ukpabi <obinna.ukpabi@talentbase.ng>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

class quickteller {
    
/**
 * Codeigniter instance
 * 
 * @access private
 * @var object
     */
    private $CI;
    
/**
 * Class constructor
 * 
 * @access public
 * @return void
 */
    public function __construct() {
        // Load CI object
        $this->CI = get_instance();
        
        // Load model
        $this->CI->load->model('payroll/payroll_model');
    }



/**
 * Query Quicktell webservce and make fund transfer
 *
 * @access public
 * @param arr $data
 * @return array
 **/


    function autopay($em)
    {

        $amt = $em['amt'];
        $unhashed = $amt."566CA".$amt."566ACNG";
        $hash = hash('SHA512', $unhashed);
        $gen = random_string('numeric', 8);;
        $transfer_code = "1159".$gen;
         
        $xml = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:quic='http://services.interswitchng.com/quicktellerservice/'>
                   <soapenv:Header/>
                   <soapenv:Body>
                      <quic:DoTransfer>
                         <!--Optional:-->
                         <quic:xmlParams><![CDATA[<RequestDetails><InitiatingEntityCode>TBL</InitiatingEntityCode>
                            <MAC>$hash</MAC>
                            <TransferCode>$transfer_code</TransferCode>
                            <Sender>
                                <Lastname>Ozioma</Lastname>
                                <Othernames>Obiaka</Othernames>
                                <Email>askphntm@gmail.com</Email>
                                <Phone>07065465506</Phone>
                            </Sender>
                            <Beneficiary>
                                <Lastname>{$em['lname']}</Lastname>
                                <Othernames>{$em['fname']}</Othernames>
                                <Email></Email>
                                <Phone></Phone>
                            </Beneficiary>
                            <Initiation>
                                <Amount>$amt</Amount>
                                <Channel>7</Channel>
                                <PaymentMethodCode>CA</PaymentMethodCode>
                                <CurrencyCode>566</CurrencyCode>
                                <Processor>
                                    <Lastname>Ozioma</Lastname>
                                    <Othernames>Obiaka</Othernames>
                                </Processor>
                                <DepositSlip></DepositSlip>
                            </Initiation>
                            <Termination>
                                <PaymentMethodCode>AC</PaymentMethodCode>
                                <Amount>$amt</Amount>
                                <CurrencyCode>566</CurrencyCode>
                                <CountryCode>NG</CountryCode>
                                <EntityCode>{$em['bank_code']}</EntityCode>
                                <AccountReceivable>
                                    <AccountNumber>{$em['acct_num']}</AccountNumber>
                                    <AccountType>10</AccountType>
                                </AccountReceivable>
                            </Termination>
                            </RequestDetails>]]></quic:xmlParams>
                      </quic:DoTransfer>
                   </soapenv:Body>
                </soapenv:Envelope>";

          
        //Change this variables.
        $location_URL = 'http://stageserv.interswitchng.com/uat_quicktellerservice/quickteller.svc';
        $action_URL = "DoTransfer";

        $client = new SoapClient(null, array(
                    'location' => $location_URL,
                    'uri'      => "",
                    'trace'    => 1,
                ));
         
        try{
            
            $xmlstr = $client->__doRequest($xml,$location_URL,$action_URL,1, 0);

            $xmlString = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $xmlstr);

            $xml = SimpleXML_Load_String($xmlString);

            $xml = new SimpleXMLElement($xml->asXML());

            $parse = $xml->sBody->DoTransferResponse->DoTransferResult;

            $token_xml = new SimpleXMLElement($parse->asXML());

            $token = simplexml_load_string($token_xml);
            
            if ($token->ResponseCode == 90000) {

                $timestamp = time();
                $date = date('Y-m-d H:i:s', $timestamp);

                $db_array = array(
                    'transfer_code' => $transfer_code,
                    'mac' => $hash,
                    'status_code' => $token->ResponseCode,
                    'id_user' => $em['id_user'],
                    'id_company' => $em['id_company'],
                    'details' => "$token->TransactionReference",
                    'id_payroll_records' => $em['id_payroll_records'],
                    'id_payroll_employee' => $em['id_payroll_employee'],
                    'date' => $date
                    );



                $db_array2 = array(
                        'pay_status' => 1,
                        'date_confirmation' => $date,
                        'status' => 'paid'
                    );

                $where2 = array(
                    'id_payroll_employee' => $em['id_payroll_employee']
                    );
                $table2= 'payroll_employee_payslip';

                $this->CI->payroll_model->add('payroll_transaction_log', $db_array);
                $this->CI->payroll_model->update($table2, $db_array2, $where2);

                return true;
            }
            else{
                
                return false;
            }

            
        }
        catch (SoapFault $exception){
            return false;
        }


    }



}