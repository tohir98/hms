<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//require('vendor/autoload.php');
use Mailgun\Mailgun;

/**
 * Talentbase
 * Email library
 * 
 * @category   Email
 * @package    Email
 * @subpackage Outgoing mail
 * @author     Chuks O. <chuks.olloh@hired.com.ng>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 * @deprecated since 1.1 deprecated by Joseph T. Orilogbon please use libraries/mailer instead
 */
class mailgun_api
{

    /**
     * Email params
     * 
     * @access private
     * @var array
     */
    private $params = array();

    /**
     * TalentBase domain
     * 
     * @access private
     * @var string
     */
    private $domain = "testbase.com.ng";

    /**
     * Response code
     * 
     * @access private
     * @var int
     */
    private $response;

    /**
     * Json response
     * 
     * @access private
     * @var string
     */
    private $resp_encode;

    /**
     * Json response
     * 
     * @access private
     * @var string
     */
    private $resp_decode;
    private $key = "key-8pftzziax4bhh1lyupgpdf7a2eelwi08";
    private $client;

    protected $tagsToHeaders = array(
        'o:deliverytime' => 'X-Mailgun-Deliver-By',
        'o:track' => '',
        'o:tag' => 'X-Mailgun-Tag',
        'o:campaign' => 'X-Mailgun-Campaign-Id',
        'o:deliverytime' => 'X-Mailgun-Deliver-By',
        'o:testmode' => 'X-Mailgun-Drop-Message',
        'o:tracking' => 'X-Mailgun-Track',
        'o:tracking-clicks' => 'X-Mailgun-Track-Clicks',
        'o:tracking-opens' => 'X-Mailgun-Track-Opens',
        'v:my-custom-data' => 'X-Mailgun-Variables',
     );
    /**
     *
     * @var mailer
     */
    protected $mailer;

    public function __construct()
    {
        $this->mailer = load_class('mailer', 'libraries', '');
    }

    public function connect()
    {

        if ($this->key != '') {
            try {
                $this->client = new Mailgun($this->key);
            } catch (Exception $ex) {
                echo 'Exception caught: ' . $ex->getMessage();
            }
        }
    }

// End func connect

    /*
     * Send email via mailgun api
     */

    public function send_mail($params = array())
    {
        // Return false if no from, to and subject fields passed
        if (!isset($params['from']) or ! isset($params['to']) or ! isset($params['subject'])) {
            log_message('error', 'Required parameters missing');
            return false;
        }

        //sort parameters

        $this->mailer->reset();
        $this->mailer->setSender($params['from']);
        $attachments = [];
        $extraHeaders = [];
        foreach ($params as $key => $paramVal) {
            if ($key === 'attachment') {
                $attachments[] = preg_replace('/^@/', '', $paramVal);
            } elseif(array_key_exists($key, $this->tagsToHeaders)) {
                $extraHeaders[$this->tagsToHeaders[$key]] = $paramVal;
            }
        }
        
        return $this->mailer->sendMessage($params['subject'], $params['html'] ? : $params['text'], $params['to'], $params['cc'], $params['bcc'], $attachments, $extraHeaders);
    }

// End func send_mail

    /**
     * Unsubscribe an email address from hired database
     */
    public function unsubscribe_account()
    {

        $this->connect();

        $result = $this->client->get($this->domain . "/unsubscribes", array(
            'limit' => 100,
            'skip' => 0
        ));

        return $result;
    }

// End func unsubcribe_account
}

// End class mailgun_api
