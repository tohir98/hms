<?php

require_once __DIR__ . '/../base_adapter.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentHandlerAbstract
 *
 * @author JosephT
 * @property Payment_model $payment_model Description
 * @property Payment $payment Description
 * @property CI_DB_active_record $db Database
 * @property CI_Loader $load Loader
 */
abstract class PaymentHandlerAbstract implements Base_adapter, \utils\Prioritizable {

    const TYPE_PAYOUT = 1;
    const TYPE_RECEIPT = 2;
    const TYPE_ALL = 255;

    protected $config = array();

    public function __construct() {
        $ci = get_instance();
        $ci->load->model('payment/payment_model', 'payment_model');
        $ci->load->library(['payment', 'mailer']);
    }

    public function getConfig($key = null) {
        $key === null ? $this->config : $this->config[$key];
    }

    public function __get($name) {
        return get_instance()->$name;
    }

    public static function getPriority() {
        return 100;
    }
    
    public function hasLogo(){
        return true;
    }
}
