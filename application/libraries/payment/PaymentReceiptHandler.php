<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'PaymentHandlerAbstract.php';

/**
 * Description of PaymentReceiptHandler
 *
 * @author JosephT
 * @property mailer $mailer mailer library
 * @property CI_Cache $cache cache
 */
abstract class PaymentReceiptHandler extends PaymentHandlerAbstract {

    const CACHE_TTL = 3600;

    //put your code here

    abstract public function processAction(\payment\Receipt $receipt, array &$pageData = []);

    abstract public function notifyAction(\payment\Transaction $transaction);

    /**
     * @return payment\ProcessingFee Description
     */
    abstract protected function getProcessingFee();

    public function computeTransactionAmount(\payment\Receipt $receipt) {
        $amount = $receipt->getTotalAmount();
        $processingFee = $this->getProcessingFee();

        if ($processingFee->isInclusive()) {
            return $amount;
        } else {
            return $amount + $processingFee->compute($amount);
        }
    }

    public function computeProcessingFee($amount) {
        $processingFee = $this->getProcessingFee();
        if (!$processingFee) {
            throw new Exception('Processing fee cannot be empty');
        }

        return $processingFee->compute($amount);
    }

    public function transactionFailed(payment\Transaction $transaction, $responseDesc, $responseCode) {
        if ($this->payment_model->setTransactionFailed($transaction, $responseCode, $responseDesc)) {
            //send mail 
            $transaction = $this->payment_model->getTransactionById($transaction->id);
            //@todo send mail.
            $this->sendFailureMail($transaction);
            return true;
        }
        return false;
    }

    public function transactionSuccessful(payment\Transaction $transaction, $responseDesc, $responseCode) {
        $this->db->trans_start();

        $callback = function() use ($transaction, $responseDesc, $responseCode) {
            if ($this->payment_model->setTransactionSuccess($transaction, $responseCode, $responseDesc)) {
                $receipt = $transaction->getPaymentReceipt();

                //mark receipt as paid.
                if (!$this->payment_model->setReceiptPaid($receipt)) {
                    log_message('error', 'Receipt with Ref: ' . $receipt->getRef() . ' could not be marked as paid.');
                    return false;
                }

                return $receipt->getModel()->paymentReceived($receipt);
            }
            return false;
        };

        if ($callback()) {
            $this->db->trans_complete();
            //send mail
            //@todo send mail
            $this->sendSuccessMail($transaction);
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    private function _notifyData(\payment\Transaction $transaction) {
        return array(
            'owner' => $this->payment_model->getUser($transaction->id_user),
            'handler' => $this,
            'transaction' => $transaction,
            'receipt' => $transaction->getPaymentReceipt(),
        );
    }

    protected function sendSuccessMail(\payment\Transaction $transaction) {
        $data = $this->_notifyData($transaction);
        $message = $this->load->view('email_templates/payment_txn_successful.php', $data, true);
        return $this->mailer->setFrom(TALENTBASE_SUPPORT_EMAIL, TALENTBASE_BUSINESS_NAME)
                        ->sendMessage('Payment Transaction Successful', $message, $data['owner']->email);
    }

    protected function sendFailureMail(\payment\Transaction $transaction) {
        $data = $this->_notifyData($transaction);
        $message = $this->load->view('email_templates/payment_txn_failed.php', $data, true);
        return $this->mailer->setFrom(TALENTBASE_SUPPORT_EMAIL, TALENTBASE_BUSINESS_NAME)
                        ->sendMessage('Payment Transaction Failed', $message, $data['owner']->email);
    }

    /**
     * @return payment\RequeryResponse Description
     */
    abstract public function requery(\payment\Transaction $transaction);

    /**
     * 
     * @param \payment\Transaction $transaction
     * @return payment\RequeryResponse
     */
    public function requeryWithCache(\payment\Transaction $transaction) {
        $this->load->driver('cache', array('adapter' => 'file', 'backup' => 'file'));
        $cacheKey = 'TXN_' . $transaction->ref;
        if (($response = $this->cache->get($cacheKey))) {
            log_message('info', 'Cache HIT for ref: ' . $cacheKey);
            return $response;
        }

        $response = $this->requery($transaction);
        $this->cache->save($cacheKey, $response, self::CACHE_TTL);
        return $response;
    }

}
