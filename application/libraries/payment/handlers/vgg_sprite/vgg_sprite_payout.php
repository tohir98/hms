<?php

use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use payment\PayoutRecipient;
use payment\ProcessingFee;
use Vgg_Sprite_Api_Client as VggClient;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once __DIR__ . '/../../PaymentPayoutHandler.php';

require_once 'Vgg_Sprite_Api_Client.php';
require_once 'Vgg_Sprite_PayoutRecipient.php';

/**
 * Description of vgg
 *
 * @author JosephT
 * @property CI_Loader $load 
 * @property CI_Input $input 
 */
class Vgg_Sprite_Payout extends PaymentPayoutHandler {

    protected static $accountData = array(
        'id_bank' => 3,
        'account_no' => '0090113211',
        'account_name' => 'VGG Projects',
        'sort_code' => '023150005',
    );

    public function getPaymentStatus($recipient) {
        throw new Exception('Not yet implemented');
    }

    public function payoutAction($payout) {

        if (request_is_post() && intval($payout->status) === PAYMENT_STATUS_PENDING) {
            $gateway_data = $this->input->post('gateway');

            $update = array('status' => PAYMENT_STATUS_QUEUED);
            if (!$this->payment->verifConfirmationCode($payout, $this->input->post('confirmation_code'))) {
                $error = "Invalid Verification code.";
            } elseif ($gateway_data['mandate_code']) {

                $this->db->trans_start();

                $status = $this->savePayoutGatewayData($payout, $gateway_data, $update) && $this->addOurProcessingFeeRecipient($payout) && $this->addVggProcessingFeeRecipient($payout)
                ;

                $this->payment_model->updateRecipientPayoutStatus($payout, PAYMENT_STATUS_QUEUED);
                $this->db->trans_complete();

                if ($status) {
                    flash_message_create('Your request has been submitted for processing and you will be notified when processing is completed.', 'success');
                    redirect($_SERVER['REQUEST_URI']);
                } else {
                    $error = 'Your payment could not be processed at the moment.';
                }
            } else {
                $error = 'You did not supply a mandate code';
            }
            return array(
                'error' => $error
            );
        }

        return array();
    }

    public static function getId() {
        return 'vgg-sprite';
    }

    public static function getName() {
        return 'Direct Debit';
    }

    public function getGatewayProcessingFee() {
        return new ProcessingFee(array(
            //the type of processing fee
            'type' => ProcessingFee::TYPE_VARIABLE,
            //the value percent if variable and Unit of currency if fixed
            'value' => 0.2,
            //should the fee be removed from total amount paid (true) or as separate entity (false)
            'inclusive' => false,
        ));
    }

    public function getOurProcessingFee() {
        return new ProcessingFee(array(
            //the type of processing fee
            'type' => ProcessingFee::TYPE_VARIABLE,
            //the value percent if variable and Unit of currency if fixed
            'value' => 0.3,
            //should the fee be removed from total amount paid (true) or as separate entity (false)
            'inclusive' => false,
        ));
    }

    protected function addVggProcessingFeeRecipient($payout) {
        $bankData = self::$accountData;
        $amount = $this->payment_model->getTotalProcessingFeeGateway($payout->id);
        $paymentRecipient = new PayoutRecipient(null, $bankData['id_bank'], $bankData['account_name'], $bankData['account_no'], $amount, $bankData['sort_code'], "VGG:Processing fee on {$payout->ref}");

        return $this->payment_model->addPaymoutRecipient($payout->id, $paymentRecipient, true);
    }

    /**
     * This method submits the payment for this batch to the gateway.
     * @param type $payout
     * @param type $batchNumber
     * @param type $batchNumber
     * @throws Exception
     */

    /**
     * 
     * @param type $payout
     * @param type $recipientsInBatch
     * @param type $batchNumber
     * @param type $error
     * @return type
     */
    protected function sendPayments($payout, $recipientsInBatch, $batchNumber, &$error = null) {
        if (!$payout->gateway_data_decoded) {
            $payout->gateway_data_decoded = $this->retrieveGatewayData($payout);
        }

        $total = 0.0;

        foreach ($recipientsInBatch as $recipient) {
            $total += floatval($recipient->amount);
        }
        //get mandate code
        $data = array(
            'PayerMandateCode' => $payout->gateway_data_decoded['mandate_code'],
            'Narration' => $payout->description,
            'PaymentReference' => self::getBatchRef($payout, $batchNumber),
            'TotalAmount' => $total,
            'Beneficiaries' => Vgg_Sprite_PayoutRecipient::batchCreate($recipientsInBatch),
        );

        log_message('info', 'Raw Request' . PHP_EOL . json_encode($data));

        //$response = null
        try {
            $response = VggClient::instance()->sendNeftRequest($data);

            log_message('info', $response->getBody());
            $reference = $response->json()['NEFTReference'];
            //update recipients to done
            return $reference;
        } catch (ConnectException $ex) {
            //update recipients to fail
            $error = 'Could not communicate with server';
            log_message('error', $ex->getMessage());
        } catch (RequestException $tEx) {
            log_message('error', $tEx->getMessage());
            log_message('error', $tEx->getTraceAsString());


            if ($tEx->hasResponse()) {
                $response = $tEx->getResponse();
                $errors = json_decode($response->getBody(), true);
                $error = $errors['ExceptionMessage'];
                if ($response->getStatusCode() == 500) {
                    $this->failedTotalError = $error;
                }
            }
        } catch (Exception $e) {
            log_message('error', $e->getMessage());
            log_message('error', $e->getTraceAsString());
            $error = 'An error occured while processing';
        }


        return null;
    }

    public static function active() {
        return ENVIRONMENT !== 'production';
    }

}
