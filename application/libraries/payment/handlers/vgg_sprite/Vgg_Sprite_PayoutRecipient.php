<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Vgg_Sprite_PayoutRecipient
 *
 * @author JosephT
 */
class Vgg_Sprite_PayoutRecipient implements JsonSerializable {

    private $recipientData = array();

    /**
     * 
     * @param stdClass|array $row row retured from Payment_model::getRecipients()
     */
    public function __construct($row) {
        if (is_object($row)) {
            $row = get_object_vars($row);
        }
        $this->recipientData = $row;
    }

    public function jsonSerialize() {
        return array(
            "Amount" => floatval($this->recipientData['amount']),
            "Name" => $this->recipientData['account_name'],
            "AccountNoRef" => $this->recipientData['account_no'],
            "SortCode" => $this->recipientData['sort_code'],
            "Naration" => $this->recipientData['description']
        );
    }

    /**
     * 
     * @param array $recipients
     * @return Vgg_Sprite_PayoutRecipient[] transformed recipients
     */
    public static function batchCreate(array $recipients) {
        $result = [];
        foreach ($recipients as $aRecipient) {
            $result[] = new self($aRecipient);
        }

        return $result;
    }

}
