<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Vgg_Sprite_Api_Client
 *
 * @author JosephT
 */
class Vgg_Sprite_Api_Client {

    /**
     *
     * @var GuzzleHttp\Client
     */
    protected $client;

    /**
     *
     * @var Vgg_Sprite_Api_Client
     */
    private static $instance;
    private $baseUrl = null;

    private function __construct() {
        $config = include(__DIR__ . '/config.php');
        $this->baseUrl = $config['endpoint'];
        $this->client = new \GuzzleHttp\Client([
            'base_url' => $config['endpoint'],
            'future' => false,
            'defaults' => [
                'headers' => ['ApiKey' => $config['ApiKey'], 'ApiCode' => $config['ApiCode'], 'Content-Type' => 'application/json'],
                'proxy' => $config['proxy'] ? $config['proxy'] : null
            ]
        ]);
    }

    /**
     * 
     * @param type $data
     * @return type
     */
    public function sendNeftRequest($data) {
        return $this->client->post('/api/Neft/PostNeftTransactions', ['body' => json_encode($data)]);
    }

    /**
     * 
     * @return static
     */
    public static function instance() {
        if (!static::$instance) {
            static::$instance = new self();
        }
        return static::$instance;
    }

}
