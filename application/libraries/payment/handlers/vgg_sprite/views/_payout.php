<div class="control-group">
    <label class="control-label">Mandate Code</label>
    <div class="controls">
        <input class="input-block-level" type="password" name="gateway[mandate_code]" required=""/>
        <div class="help-block">
            <small><i class="icon icon-question-sign"></i> Don't have a mandate code, <a href="#" target="_blank">Click here</a> for instructions on how to get one.</small>
        </div>
    </div>
</div>