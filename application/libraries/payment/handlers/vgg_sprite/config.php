<?php


$gatewayCommonConfig = array(
);

if (ENVIRONMENT === 'development') {
    //This is needed when testing locally,
    //to set up proxy, to the test server, see this page:
    //- http://www.cs.columbia.edu/~crf/howto/ssh-tunnel-web.html#windows
    // the connection should be through testhired.com.ng and not cunix.columbia.edu
    $gatewayCommonConfig['proxy'] = 'socks5://localhost:9999';
}

if (ENVIRONMENT === 'production') {
    return array_merge($gatewayCommonConfig, array(
        'endpoint' => 'http://sprite.vggprojects.com',
        'ApiCode' => '',
        'ApiKey' => '',
    ));
} else {
    return array_merge($gatewayCommonConfig, array(
        'endpoint' => 'http://testsprite.splasherstech.com',
        'ApiCode' => '7DD1A61D-1DE6-43C6-9E13-1753EFA890CB',
        'ApiKey' => 'ncaa',
    ));
}
