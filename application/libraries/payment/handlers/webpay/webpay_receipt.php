<?php

use payment\ProcessingFee;
use payment\Receipt;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once __DIR__ . '/../../PaymentReceiptHandler.php';

/**
 * Description of webpay
 *
 * @author JosephT
 */
class Webpay_Receipt extends PaymentReceiptHandler {

    const SUCCESS_STATUS = '00';

    /**
     *
     * @var ProcessingFee
     */
    protected $processingFee;

    public function __construct() {
        parent::__construct();
        $this->processingFee = new ProcessingFee(array(
            'value' => 1.5,
            'type' => ProcessingFee::TYPE_VARIABLE,
            'cap' => 2000,
            //change this to false when we need customers to start apying charges
            'inclusive' => true,
        ));

        $this->config = include 'config.php';
        $this->load->helper(['array', 'payment']);
    }

    public static function active() {
        return ENVIRONMENT !== 'production';
    }

    public function processAction(Receipt $receipt, array &$pageData = []) {
        $transaction = $this->payment_model->createTransaction($receipt, $this);

        $user = $this->payment_model->getReceiptOwner($receipt);

        $data = array(
            'product_id' => $this->config['product_id'],
            'pay_item_id' => $this->config['item_id'],
            'amount' => $transaction->amount * 100.0,
            'currency' => $this->config['currency'],
            'site_redirect_url' => site_url('payment/receipt/notify/' . $transaction->ref),
            'txn_ref' => $transaction->ref,
            'cust_name' => $user->first_name . ' ' . $user->last_name,
            'cust_id' => $user->id_user,
            'cust_name_desc' => 'User Name',
            'merchant_name' => $this->config['merchant_name'],
        );

        $data['hash'] = $this->computePaymentHash($data);

        $pageData['form_data'] = $data;
        $pageData['config'] = $this->config;
        $pageData['view_script'] = __DIR__ . '/views/process.php';
        echo $this->load->view('payment/process', $pageData, true);
        exit;
    }

    public static function getId() {
        return 'webpay';
    }

    public static function getName() {
        return 'Interswitch WebPay';
    }

    protected function getProcessingFee() {
        return $this->processingFee;
    }

    private function verify($transaction, array &$response = array()) {
        $data = array(
            'productid' => $this->config['product_id'],
            'transactionreference' => $transaction->ref,
            'amount' => $transaction->amount * 100.0,
        );

        $response = $this->_callWebservice($data);

        return $response['ResponseCode'] === self::SUCCESS_STATUS;
    }

    private function _callWebservice($data) {
        $payload = "{$data['productid']}{$data['transactionreference']}" . $this->config['mac'];
        $hash = $this->_hash($payload);

        $url = $this->config['endpoint'] . '/api/v1/gettransaction.json?' . http_build_query($data);
        $context = stream_context_create(array(
            'http' => array(
                'user_agent' => 'TalentBase/V1',
                'header' => "Hash: {$hash}"
            ),
        ));

        $response = file_get_contents($url, false, $context);
        log_message('info', "Raw response from Interswitch: " . $response);

        return json_decode($response, true) ? : array(
            'ResponseCode' => 'SYSERROR',
            'ResponseDescription' => 'Payment status could not be verified'
        );
    }

    private function computePaymentHash($data) {
        $keys = ['txn_ref', 'product_id', 'pay_item_id', 'amount', 'site_redirect_url'];
        $target = elements($keys, $data);
        $target['m'] = $this->config['mac'];
        return $this->_hash(join('', $target));
    }

    private function _hash($payload) {
        return strtoupper(hash('sha512', $payload));
    }

    public function notifyAction(\payment\Transaction $transaction) {
        $response = array();
        if (!$this->verify($transaction, $response)) {
            //mark as failed, redirect to failure.
            $this->transactionFailed($transaction, $response['ResponseDescription'], $response['ResponseCode']);
            $receipt = $transaction->getPaymentReceipt();
            //message.
            flash_message_create("Your payment could not be completed.<br/>Reason: {$response['ResponseDescription']} ({$response['ResponseCode']})<br/>Transaction Reference: {$transaction->ref}<br/>", 'error');

            redirect(payment_receipt_pay_url($receipt->getRef(), $receipt->getId()));
        } else {
            $receipt = $transaction->getPaymentReceipt();
            //mark as completed, redirect to success.
            if ($this->transactionSuccessful($transaction, $response['ResponseDescription'], $response['ResponseCode'])) {
                flash_message_create('Payment was successfully completed', 'success');

                if (($url = $receipt->getNextUrl())) {
                    redirect($url);
                } else {
                    redirect('/payment/receipt/success/' . $transaction->ref);
                }
            }

            $support = TALENTBASE_SUPPORT_EMAIL;

            flash_message_create("Your payment could not be completed.<br/>Reason: System error occured while processing your payment, please contact {$support} for assistance, state your transaction reference: {$transaction->ref}", 'error');
            redirect(payment_receipt_pay_url($receipt->getRef(), $receipt->getId()));
        }
    }

    public function requery(\payment\Transaction $transaction) {
        $response = array();
        $success = $this->verify($transaction, $response);
        return new payment\RequeryResponse($response['ResponseCode'], $response['Amount']/100.0 , $response['ResponseDescription'], $success);
    }
    
    public static function getPriority() {
        return 1;
    }

}
