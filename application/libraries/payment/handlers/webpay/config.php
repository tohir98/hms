<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (ENVIRONMENT === 'production') {
    $envConfig = array(
        'endpoint' => '',
        'product_id' => '',
        'item_id' => '',
        'merchant_name' => '',
        'mac' => ''
    );
} else {
    $envConfig = array(
        'endpoint' => 'https://stageserv.interswitchng.com/test_paydirect',
        'mac' => '199F6031F20C63C18E2DC6F9CBA7689137661A05ADD4114ED10F5AFB64BE625B6A9993A634F590B64887EEB93FCFECB513EF9DE1C0B53FA33D287221D75643AB',
        'item_id' => '101',
        'product_id' => '4220',
    );
}



return array_merge($envConfig, array('currency' => 566));
