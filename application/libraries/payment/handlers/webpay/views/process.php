<html>
    <head>
        <title>Processing Payment...</title>
    </head>
    <body onload="document.getElementById('payForm').submit();">
        <form name="form1" id="payForm" action="<?= $config['endpoint'] ?>/pay" method="post">
            <?php foreach ($form_data as $key => $value): ?>
                <input name="<?= $key ?>" type="hidden" value="<?= $value ?>"/>
            <?php endforeach; ?>
        </form>
    </body>
</html>