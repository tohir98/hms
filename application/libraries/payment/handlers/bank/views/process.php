<div class="receipt-container">
    <?= $receiptData ?>
    <div class="box box-bordered">
        <div class="box-title">
            <h3>Pay with Bank</h3>
        </div>

        <div class="box-content">
            <p>
                Please make deposit into any of the following bank account(s). 
                <br/>
                <strong>NOTE:</strong> Specify your reference (<?= $receipt->getRef(); ?>) when making deposits.
            </p>


            <?php foreach ($bankAccounts as $i => $bankAccount): ?>
                <hr/>
                <div class="row-fluid">
                    <div class="span1">
                        <?= $i + 1 ?>.
                    </div>
                    <div class="span5">

                        <strong>Bank: </strong> <?= $bankAccount->bank_name; ?>
                        <br/>
                        <strong>Account Name: </strong> <?= $bankAccount->account_name; ?>
                        <br/>
                        <strong>Account Number: </strong> <?= $bankAccount->account_number; ?>
                        <br/>
                        <?php if ($bankAccount->swift_code): ?> 
                            <p>
                                <strong>Swift Code: </strong> <?= $bankAccount->account_number; ?>
                            </p>
                        <?php endif; ?>
                    </div>
                    <div class="span2">
                        <?php if ($bankAccount->logo): ?>
                            <img src="<?= site_url('/img/payment/' . $bankAccount->logo) ?>" class="img-polaroid"/>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>

    </div>
</div>