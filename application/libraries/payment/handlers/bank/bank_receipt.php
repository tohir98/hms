<?php

use payment\ProcessingFee;
use payment\Receipt;
use payment\Transaction;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if(!class_exists('PaymentReceiptHandler')){
    require_once __DIR__ . '/../../PaymentReceiptHandler.php';
}

/**
 * Description of bank_receipt
 *
 * @author JosephT
 * @property Bank_Payment_Model $bank_model 
 */
class Bank_receipt extends PaymentReceiptHandler {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('payment/bank_payment_model', 'bank_model');
    }
    
    protected function getProcessingFee() {
        return new ProcessingFee(array(
            'value' => 0,
            'type' => ProcessingFee::TYPE_FIXED,
            'is_inclusive' => true,
        ));
    }

    public function notifyAction(Transaction $transaction) {
        //throw new Exception('Notification is not allowed for Bank Payment.');
    }

    public function processAction(Receipt $receipt, array &$pageData = array()) {
        
        $pageData['config'] = $this->config;
        $pageData['receipt'] = $receipt;
        $pageData['receiptData'] = $this->load->view('common/receipt', $pageData, true);
        $pageData['view_script'] = __DIR__ . '/views/process.php';
        $pageData['bankAccounts'] = $this->bank_model->getBankAccounts();
        $this->sendPaymentInstructions($receipt, $pageData);
    }

    private function sendPaymentInstructions(Receipt $receipt, array $pageData=[]){
        if(!$this->bank_model->checkNotified($receipt->getId())){
            //send message
            //@todo, generate receipt pdf and send
            //send bank instructions
            
            $owner = $this->payment_model->getReceiptOwner($receipt);
            $pageData['owner'] = $owner;
            $message = $this->load->view('email_templates/payment_bank_instructions', $pageData, true);
            $sent = $this->mailer->setFrom(TALENTBASE_SUPPORT_EMAIL, TALENTBASE_BUSINESS_NAME)
                        ->sendMessage('Bank Payment Instructions', $message, $owner->email);
            
            if($sent){
                $this->bank_model->setNotified($receipt->getId());
            }
            return $sent;
        }
        return true;
    }

    public function requery(Transaction $transaction) {
        
    }

    public static function active() {
        return true;
    }

    public static function getId() {
        return 'bank';
    }

    public static function getName() {
        return 'Bank Deposit';
    }

    public function hasLogo() {
        return false;
    }
}
