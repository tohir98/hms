<?php

use payment\ProcessingFee;

require_once 'PaymentHandlerAbstract.php';

/**
 * Description of PaymentAdapter
 *
 * @author JosephT
 */
abstract class PaymentPayoutHandler extends PaymentHandlerAbstract {

    const STATUS_FAILED_TOTALLY = -1;

    protected $failedTotalError = null;

    abstract public function payoutAction($payout);

    abstract protected function sendPayments($payout, $recipientsInBatch, $batchNumber, &$error = null);

    abstract public function getPaymentStatus($recipient);

    /**
     * @return ProcessingFee out processing fee for this gateway.
     */
    abstract public function getOurProcessingFee();

    /**
     * @return ProcessingFee the processing fee object for this gateway
     */
    abstract public function getGatewayProcessingFee();

    public function computeOurProcessingFee($amount) {
        $processingFee = $this->getOurFeeForCompany() ? : $this->getOurProcessingFee();
        return $processingFee->compute($amount);
    }

    public function computeGatewayProcessingFee($amount, $type = self::TYPE_PAYOUT) {
        $processingFee = $this->getGatewayFeeForCompany() ? : $this->getGatewayProcessingFee($type);
        return $processingFee->compute($amount);
    }

    public function getMaxPayeePerBatch() {
        return PAYMENT_DEFAULT_PAYEE_PER_BATCH;
    }

    /**
     * @return null|ProcessingFee Description
     */
    protected function getOurFeeForCompany() {
        $config = $this->payment_model->loadCompanyConfig($this->user_auth->get('id_company'));
        $key = PAYMENT_CONFIG_FEE_OURS . '_' . static::getId();
        if (isset($config[$key])) {
            return new ProcessingFee($config[$key]);
        }
        return null;
    }

    /**
     * @return null|ProcessingFee Description
     */
    protected function getGatewayFeeForCompany() {
        $config = $this->payment_model->loadCompanyConfig($this->user_auth->get('id_company'));
        $key = PAYMENT_CONFIG_FEE_GATEWAY . '_' . static::getId();
        if (isset($config[$key])) {
            return new ProcessingFee($config[$key]);
        }
        return null;
    }

    protected function savePayoutGatewayData($payout, array $gwData, array $rowData = array()) {
        $dataStr = json_encode($gwData);
        $encrypter = utils\Encrypter::encrypt($dataStr, $payout->ref);
        $data = array_merge($rowData, array(
            'gateway_data' => $encrypter
        ));
        return $this->db->update(Payment_model::TABLE_PAYOUT, $data, array(
                    'id' => (int) $payout->id
                        ), 1);
    }

    protected function retrieveGatewayData($payout) {
        $derypted = utils\Encrypter::decrypt($payout->gateway_data, $payout->ref);
        return json_decode($derypted, true);
    }

    protected function addOurProcessingFeeRecipient($payout) {
        $bankData = Payment::$ourAccountData;
        $amount = $this->payment_model->getTotalProcessingFeeOurs($payout->id);
        $paymentRecipient = new payment\PayoutRecipient(null, $bankData['id_bank'], $bankData['account_name'], $bankData['account_no'], $amount, $bankData['sort_code'], "TB:Processing fee on {$payout->ref}");

        return $this->payment_model->addPaymoutRecipient($payout->id, $paymentRecipient, true);
    }

    public function processPayout($payoutOrig, &$error) {

        $status = ['success' => 0, 'failure' => 0];

        $this->db->trans_start();
        //refresh payout from DB and lock for update, prevent double processing.
        $payout = $this->payment_model->getPayout($payoutOrig->id, $payoutOrig->id_user, true);
        if (intval($payout->status) !== PAYMENT_STATUS_QUEUED) {
            $error = "The payment is not in queue.";
        } else if (($recipientsCount = $this->payment_model->countRecipients($payout->id, null)) < 1) {
            $error = "There are no recipients.";
        } else if (!$this->payment_model->setPayoutStatus($payout, PAYMENT_STATUS_ONGOING)) {
            $error = "Could not update status of payout.";
        } else {
            $error = $this->batchProcess($payout, $recipientsCount, $status);
        }

        $this->db->trans_complete();
        return $status;
    }

    private function batchProcess($payout, $recipientsCount, &$status) {
        $batches = (int) ceil($recipientsCount / $this->getMaxPayeePerBatch());
        $errors = [];
        $failedTotally = false;

        for ($batchNumber = 1; $batchNumber <= $batches; $batchNumber++) {

            //throw new Exception('*****DONT FORGET TO IMPLEMENT ME****');
            $limit = $this->getMaxPayeePerBatch();
            $offset = ($batchNumber - 1) * $this->getMaxPayeePerBatch();

            $recipientsRaw = $this->payment_model->getPayoutRecipients($payout->id, null, $limit, $offset);

            $recipientsInBatch = array_filter($recipientsRaw, function($recipient) {
                return intval($recipient->status) === PAYMENT_STATUS_QUEUED;
            });

            if (empty($recipientsInBatch)) {
                $errors[$batchNumber] = 'No recipients to process in batch';
            } else {
                //change to get or create
                $batchId = $this->payment_model->createBatch($payout->id, $batchNumber, null, $payout->send_attempts + 1);
                if (!$batchId) {
                    $errors[$batchNumber] = 'Could not create batch entry';
                }
            }

            if (!$errors[$batchNumber]) {
                $updated = $this->payment_model->updateRecipients($recipientsInBatch, array(
                    'id_payment_payout_batch' => $batchId,
                    'status' => PAYMENT_STATUS_ONGOING
                ));

                if (!$updated) {
                    //could not change status
                    $errors[$batchNumber] = 'Could not update recipient status for batch : ' . $batchNumber;
                } else if (($neftRef = $this->sendPayments($payout, $recipientsInBatch, $batchNumber, $errors[$batchNumber]))) {
                    //payments successfully sent
                    $status['success'] ++;
                    $this->payment_model->updateRecipients($recipientsInBatch, array(
                        'status' => PAYMENT_STATUS_DONE
                    ));

                    $this->payment_model->updateBatch($batchId, array('gateway_ref' => $neftRef));
                } else if ($this->failedTotalError !== null) {
                    //a global error occured
                    $status['failure'] ++; //increments because it breaks out of the loop here.
                    $failedTotally = true;
                    $this->payment_model->updateRecipients($recipientsInBatch, array(
                        'status' => PAYMENT_STATUS_FAILED
                    ));
                    break;
                } else {
                    $this->payment_model->updateRecipients($recipientsInBatch, array(
                        //queue for retrial
                        'status' => PAYMENT_STATUS_QUEUED
                    ));
                }
            }

            if ($errors[$batchNumber]) {
                $status['failure'] ++;
            }
        }

        $error = $this->failedTotalError ? : join(PHP_EOL, array_filter($errors));

        if ($status['success'] === $batches) {
            $this->payment_model->setPayoutStatus($payout, PAYMENT_STATUS_DONE, true);
            $this->payment->payoutIsDone($payout);
        } else if ($failedTotally || ((intval($payout->send_attempts) + 1) >= PAYMENT_MAX_SEND_ATTEMPTS)) {
            //$this->payment_model->setPayoutStatus($payout, PAYMENT_STATUS_FAILED, true);
            $this->payment_model->setPayoutFailed($payout, true);
            $this->payment->payoutHasFailed($payout, $error);
        } else {
            $this->payment_model->setPayoutStatus($payout, PAYMENT_STATUS_QUEUED, true);
        }

        return $error;
    }

    public static function getBatchRef($payout, $batchNumber) {
        return $payout->ref . '|' . sprintf("%04d", $batchNumber) . '|' . sprintf("%02d", $payout->send_attempts + 1);
    }

}
