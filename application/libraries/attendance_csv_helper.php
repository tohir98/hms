<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Talentbase
 * Tasks library (for all users)
 * 
 * @category   Library
 * @package    Attendance
 * @subpackage CSV
 * @author     Tohir Omoloye <thohiru.omoloye@talentbase.ng>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */
class Attendance_csv_helper {

    /**
     * Codeigniter instance
     * 
     * @access private
     * @var object
     */
    private $CI;

    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        // Load CI object
        $this->CI = get_instance();

        $this->CI->load->library('user_auth');

        // Load model
        $this->CI->load->model('attendance/attendance_model', 'a_model');

        $this->id_company = $this->CI->user_auth->get('id_company');
    }

    /**
     * Check if payroll id is valid
     *
     * @access public
     * @param int $id_user
     * @return mixed (bool | array)
     * */
    function array_to_csv($array, $download = "") {
        if ($download != "") {
            header("Content-Type: application/vnd.ms-excel; name='excel'");
            header('Content-Disposition: attachement; filename="' . $download . '"');
        }

        ob_start();
        $f = fopen('php://output', 'w') or show_error("Can't open php://output");
        $n = 0;
        foreach ($array as $line) {
            $n++;
            if (!fputcsv($f, $line)) {
                show_error("Can't write line $n: $line");
            }
        }
        fclose($f) or show_error("Can't close php://output");
        $str = ob_get_contents();
        ob_end_clean();

        if ($download == "") {
            return $str;
        } else {
            echo $str;
        }
    }

    /*
     * Check if payroll id is valid
     *
     * @access public
     * @param int $id_user
     * @return mixed (bool | array)
     * */

    function convert_xls($data, $download = "") {

        header("Content-Type: application/vnd.ms-excel; name='excel'");
        header('Content-Disposition: attachement; filename="' . $download . '"');

        echo $data;

        exit;
    }

    /*
     * Check if payroll id is valid
     *
     * @access public
     * @param int $id_user
     * @return mixed (bool | array)
     * */

    public function create_excel_file($summary_data, $month1, $mon_1_tab, $name) { //$summary_data, $monthly_report, $download

        //Create new PHPExcel object";
        $objPHPExcel = new PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("Talentbase");
        $objPHPExcel->getProperties()->setLastModifiedBy("Talentbase");
        $objPHPExcel->getProperties()->setTitle("Attendance Record");
        $objPHPExcel->getProperties()->setSubject("Attendance Record");
//        $objPHPExcel->getProperties()->setDescription("List of employee payroll details");
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Summary');
        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $rowCount = 1;
        foreach ($summary_data as $data) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $data[0]);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $data[1]);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $data[2]);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $data[3]);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $data[4]);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $data[5]);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $data[6]);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $data[7]);
            $rowCount++;
        }


        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex(1);
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle($mon_1_tab);
        $rowCount = 3;
        foreach ($month1 as $dat) {
//            $last_val = count($dat);
//            for ($i = 0; $i <= $last_val; $i++) {
//                $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $dat[$i]);
//            }
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $dat[0]);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $dat[1]);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $dat[2]);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $dat[3]);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $dat[4]);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $dat[5]);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $dat[6]);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $dat[7]);
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $dat[8]);
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $dat[9]);
            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $dat[10]);
            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $dat[11]);
            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $dat[12]);
            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $dat[13]);
            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $dat[14]);
            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $dat[15]);
            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $dat[16]);
            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $dat[17]);
            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, $dat[18]);
            $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $dat[19]);
            $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $dat[20]);
            $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $dat[21]);
            $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $dat[22]);
            $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $dat[23]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $rowCount, $dat[24]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AB' . $rowCount, $dat[25]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AC' . $rowCount, $dat[26]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AD' . $rowCount, $dat[27]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AE' . $rowCount, $dat[28]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AF' . $rowCount, $dat[29]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AG' . $rowCount, $dat[30]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AH' . $rowCount, $dat[31]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AI' . $rowCount, $dat[32]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AJ' . $rowCount, $dat[33]);
            $rowCount++;
        }


        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $name . '".xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    public function simple_report($summary_data, $name) {
        //Create new PHPExcel object";
        $objPHPExcel = new PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("Talentbase");
        $objPHPExcel->getProperties()->setLastModifiedBy("Talentbase");
        $objPHPExcel->getProperties()->setTitle("Attendance Report");
        $objPHPExcel->getProperties()->setSubject("Attendance Report");
//        $objPHPExcel->getProperties()->setDescription("List of employee payroll details");
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Summary');
        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $rowCount = 1;
        foreach ($summary_data as $data) {
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $data[0]);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $data[1]);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $data[2]);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $data[3]);
            $rowCount++;
        }


        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $name . '".xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    public function create_excel_file3($summary_data, $pay_detail, $tab_name, $name) {

        //Create new PHPExcel object";
        $objPHPExcel = new PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("Talentbase");
        $objPHPExcel->getProperties()->setLastModifiedBy("Talentbase");
        $objPHPExcel->getProperties()->setTitle("Attendance");
        $objPHPExcel->getProperties()->setSubject("Attendance Sheet");
        $objPHPExcel->getProperties()->setDescription("Attendance details");


        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle($tab_name);
        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $rowCount = 1;
        foreach ($summary_data as $data) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $data[0]);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $data[1]);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $data[2]);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $data[3]);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $data[4]);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $data[5]);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $data[6]);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $data[7]);
            $rowCount++;
        }


        // Rename sheet
        $objPHPExcel->createSheet();
        $objPHPExcel->getActiveSheet()->setTitle('Summary');
        // Add some data
        $objPHPExcel->setActiveSheetIndex(1);
        $rowCount = 1;

        $size = count($pay_detail);
        foreach ($pay_detail as $info) {
            $k = 0;
            $data = array(explode(", ", $info));

            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $data[$k][0]);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $data[$k][1]);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $data[$k][2]);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $data[$k][3]);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $data[$k][4]);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $data[$k][5]);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $data[$k][6]);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $data[$k][7]);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $data[$k][8]);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $data[$k][9]);
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $data[$k][10]);
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $data[$k][11]);
            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $data[$k][12]);
            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $data[$k][13]);
            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $data[$k][14]);
            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $data[$k][15]);
            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $data[$k][16]);
            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $data[$k][17]);
            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $data[$k][18]);
            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $data[$k][19]);
            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, $data[$k][20]);
            $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $data[$k][21]);
            $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $data[$k][22]);
            $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $data[$k][23]);
            $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $data[$k][24]);
            $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $data[$k][25]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $rowCount, $data[$k][26]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AB' . $rowCount, $data[$k][27]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AC' . $rowCount, $data[$k][28]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AD' . $rowCount, $data[$k][29]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AE' . $rowCount, $data[$k][30]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AF' . $rowCount, $data[$k][31]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AG' . $rowCount, $data[$k][32]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AH' . $rowCount, $data[$k][33]);
            $objPHPExcel->getActiveSheet()->SetCellValue('AI' . $rowCount, $data[$k][34]);
            $rowCount++;
            $k++;

//            }
        }

//                   die();


        /* $objPHPExcel->createSheet();
          $objPHPExcel->setActiveSheetIndex(1);
          // Rename sheet
          $objPHPExcel->getActiveSheet()->setTitle('Net Pay');
          $rowCount = 1;
          foreach ($net_pay as $dat) {
          $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $dat[0]);
          $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $dat[1]);
          $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $dat[2]);
          $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $dat[3]);
          $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $dat[4]);
          $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $dat[5]);
          $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $dat[6]);
          $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $dat[7]);
          $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $dat[8]);
          $rowCount++;
          } */


        /* $objPHPExcel->createSheet();
          $objPHPExcel->setActiveSheetIndex(2);
          // Rename sheet
          $objPHPExcel->getActiveSheet()->setTitle('Pension');
          $rowCount = 1;
          foreach ($pension as $pen) {
          $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $pen[0]);
          $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $pen[1]);
          $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $pen[2]);
          $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $pen[3]);
          $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $pen[4]);
          $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $pen[5]);
          $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $pen[6]);
          $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $pen[7]);
          $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $pen[8]);
          $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $pen[9]);
          $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $pen[10]);
          $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, $pen[11]);
          $rowCount++;
          } */


        /* $objPHPExcel->createSheet();
          $objPHPExcel->setActiveSheetIndex(3);
          // Rename sheet
          $objPHPExcel->getActiveSheet()->setTitle('TAX');
          $rowCount = 1;
          foreach ($tax as $tx) {
          $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $tx[0]);
          $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $tx[1]);
          $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $tx[2]);
          $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $tx[3]);
          $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $tx[4]);
          $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $tx[5]);
          $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $tx[6]);
          $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $tx[7]);
          $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $tx[8]);
          $rowCount++;
          } */


        /* $objPHPExcel->createSheet();
          $objPHPExcel->setActiveSheetIndex(4);
          // Rename sheet
          $objPHPExcel->getActiveSheet()->setTitle('NHF');
          $rowCount = 1;
          foreach ($nhf as $n) {
          $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $n[0]);
          $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $n[1]);
          $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $n[2]);
          $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $n[3]);
          $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $n[4]);
          $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $n[5]);
          $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $n[6]);
          $rowCount++;
          } */





        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $name . '".xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    public function create_excel_file4($s_date, $e_date, $id_location, $name) {

        //Create new PHPExcel object";
        $objPHPExcel = new PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("Talentbase");
        $objPHPExcel->getProperties()->setLastModifiedBy("Talentbase");
        $objPHPExcel->getProperties()->setTitle("Attendance");
        $objPHPExcel->getProperties()->setSubject("Attendance Sheet");
        $objPHPExcel->getProperties()->setDescription("Attendance details");

        /* for summary 
          $end_date = $e_date; $start_date = $s_date;
          $wkend = "false"; $pub_holidays == "false";
          $sh_array= array( 'id_company' => $this->id_company );
          $shift = $this->CI->a_model->get('attendance_shift',$sh_array);
          $emp_shift = $this->CI->a_model->get_all('attendance_shift_days');

          if(!empty($id_location)) : foreach($id_location as $lo) :

          $ar = array('id_location' => $lo);
          $location = $this->CI->a_model->get_row('company_locations',$ar);

          $array = array('id_company' => $this->id_company,
          'id_location' => $lo
          );
          $att_user = $this->CI->a_model->get_attendance_info_with_location($this->user_auth->get('id_company'),$lo);               //fetch all users in location

          if(!empty($att_user)) {

          foreach ($att_user as $user) {
          $total = 0;
          $thr=0; $hr=0; $tmin=0; $min=0; $tsec=0; $sec=0;
          $absent = 0;
          $lateness = 0;
          $emp_shift_count =0;

          $today=$start_date;
          $mon1 = $user->id_string.", ".$user->first_name.", "."$user->last_name";
          $days = (strtotime("$end_date") - strtotime(date("$today"))) / (60 * 60 * 24);
          for($i=1; $i<=$days+1; $i++ ){

          $day = date('D', strtotime($today));
          //
          if ($wkend == "true" and ( $day == "Sun" or $day == "Sat"))
          {
          $today = strtotime($today) + 86400;
          $today = date('Y-m-d',$today);
          continue;

          }else if ( $pub_holidays == "true" and $this->a_model->is_public( $today, $this->user_auth->get('id_company') ) )
          {
          $today = strtotime($today) + 86400;
          $today = date('Y-m-d',$today);
          continue;
          }else{

          //get checkinout
          $checkin = $this->CI->a_model->get_check_in_out($user->user_id,$today);

          if(!empty ($checkin)){
          $clockin = $checkin->checkin;
          $clockout = $checkin->checkout;
          }else{
          $clockin = "-";  $clockout = "-";
          }

          if($clockin != "-" and $clockout != "-") {
          $diff1 = strtotime($clockout) - (strtotime($clockin)+ 3600);
          $diff = date('h:i:s',$diff1);
          list ($hr, $min, $sec) = explode(':',$diff);
          $thr=$thr+$hr; $tmin=$tmin+$min; $tsec=$tsec+$sec;
          $total += $diff1;
          }else{
          $diff ="-";
          }

          //for absent
          if($clockin == "-" and $clockout == "-")
          {
          $absent = $absent + 1;
          }

          //count employee shifts
          $day = strtolower(date('D', strtotime($today)));
          $shift = $this->a_model->count('attendance_shift_days', $day, $user->id_user);
          $emp_shift_count += $shift->count;

          //for lateness
          if($clockin != "-"){
          $myshift = $this->a_model->shift_of_day('attendance_shift_days', $day, $user->id_user);
          if (isset($myshift) and !empty($myshift)){
          $cit = date('G:i:s', strtotime( $myshift->cit ) + ($myshift->lateness * 60));
          if (strtotime($clockin) > strtotime($cit)) {
          $lateness++;
          }
          }
          }

          $today = strtotime($today) + 86400;
          $today = date('Y-m-d',$today);

          $mon1 .= ", ".$diff;

          }//End if not weekend/public holiday


          }//End for days

          $total11 = $this->a_model->gettime("$thr:$tmin:$tsec");
          $total11 = $this->a_model->roundDurationUp_HM($total11);
          $emp_shift = $this->a_model->get_employee_shift($user->id_user);

          echo "<tr>
          <td align='center'>$user->first_name $user->last_name </td>
          <td align='center'> $emp_shift_count</td>
          <td align='center'>$total11</td>
          <td align='center'>$lateness</td>
          <td align='center'> $absent </td>
          </tr>";

          $kkk[] = $mon1;

          $summary_data1[] =  array('id_string' => $user->id_string,
          'first_name' => $user->first_name,
          'last_name' => $user->last_name,
          'location_tag' => $location->location_tag,
          'shift' => $emp_shift_count,
          'hours_clocked' => $total11,
          'lateness' => '-',
          'absent' => $absent );

          }//End foreach user

          }//End if user isset


          endforeach ;

          else :
          echo "<h4>No Records Returned </h4>";
          endif ;

          echo "</tbody></table><br><br>"; */

        /* end summary */


        $a = date('m', strtotime($s_date));
        $b = date('m', strtotime($e_date));

//    for ($starting = $a; $starting <= b;) {}

        while ($a <= $b) {
            $sheet = 0;
            $kkk = 'kkk_' . $a; //die();
            $kkk = array();

            $start = date('Y-' . $a . '-01', strtotime(date('Y-' . $a . '-d')));
            $end = date('Y-' . $a . '-t', strtotime(date('Y-' . $a . '-d')));

            $sheet_name = date('M', strtotime(date('Y-' . $a . '-d')));

            $today = date('Y-' . $a . '-01');
            $days = (strtotime("$end") - strtotime($start)) / (60 * 60 * 24);

            $df = "REF ID, FNAME, LNAME";                          //Build the header
            for ($i = 1; $i <= $days + 1; $i++) {
                $df .= ", " . date('D d', strtotime($today));
                $today = strtotime($today) + 86400;
                $today = date('Y-m-d', $today);
            }

            $kkk[] = $df;


            if (!empty($id_location)) : foreach ($id_location as $lo) :

                    $array = array('id_company' => $this->id_company,
                        'id_location' => $lo);

                    $att_user = $this->CI->a_model->get_attendance_info_with_location($this->CI->user_auth->get('id_company'), $lo);

                    if (isset($att_user)) {
                        foreach ($att_user as $user) {
                            $total = 0;
                            $thr = 0;
                            $hr = 0;
                            $tmin = 0;
                            $min = 0;
                            $tsec = 0;
                            $sec = 0;
                            $absent = 0;
                            $lateness = 0;
                            $emp_shift_count = 0;
                            $mon1 = $user->id_string . ", " . $user->first_name . ", " . "$user->last_name";

                            for ($i = 1; $i <= $days + 1; $i++) {
                                //get checkinout
                                $checkin = $this->CI->a_model->get_check_in_out($user->user_id, $today);
                                if (!empty($checkin)) {
                                    $clockin = $checkin->checkin;
                                    $clockout = $checkin->checkout;
                                } else {
                                    $clockin = "-";
                                    $clockout = "-";
                                }

                                if ($clockin != "-" and $clockout != "-") {
                                    $diff1 = strtotime($clockout) - (strtotime($clockin) + 3600);
                                    $diff = date('h:i:s', $diff1);
                                    list ($hr, $min, $sec) = explode(':', $diff);
                                    $thr = $thr + $hr;
                                    $tmin = $tmin + $min;
                                    $tsec = $tsec + $sec;
                                    $total += $diff1;
                                } else {
                                    $diff = "-";
                                }

                                $today = date('Y-' . $a . '-d', strtotime($today) + 86400);
                                $mon1 .= ", " . $diff;
                            }//end loop of days of month

                            $kkk[] = $mon1;
                        }//end user
                    }//end isset user



                endforeach;
            endif;
            var_dump($kkk);
            die();
            /* start excel activities */

            // Rename sheet
            $objPHPExcel->createSheet();
            $objPHPExcel->getActiveSheet()->setTitle($sheet_name);
            // Add some data
            $objPHPExcel->setActiveSheetIndex($sheet);
            $rowCount = 1;

            $size = count($kkk);
            foreach ($kkk as $info) {
                $data = array(explode(", ", $info));
//                    var_dump($data); die();
                for ($k = 0; $k <= $size - 1; $k++) {
//                        echo $data[$k][0]; echo $data[$k][1];

                    $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $data[$k][0]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $data[$k][1]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $data[$k][2]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $data[$k][3]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $data[$k][4]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $data[$k][5]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $data[$k][6]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $data[$k][7]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $data[$k][8]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $data[$k][9]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $data[$k][10]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $data[$k][11]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $data[$k][12]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $data[$k][13]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $data[$k][14]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $data[$k][15]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $data[$k][16]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $data[$k][17]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $data[$k][18]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('T' . $rowCount, $data[$k][19]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('U' . $rowCount, $data[$k][20]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('V' . $rowCount, $data[$k][21]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('W' . $rowCount, $data[$k][22]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('X' . $rowCount, $data[$k][23]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $rowCount, $data[$k][24]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $rowCount, $data[$k][25]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $rowCount, $data[$k][26]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AB' . $rowCount, $data[$k][27]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AC' . $rowCount, $data[$k][28]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AD' . $rowCount, $data[$k][29]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AE' . $rowCount, $data[$k][30]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AF' . $rowCount, $data[$k][31]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AG' . $rowCount, $data[$k][32]);
                    $objPHPExcel->getActiveSheet()->SetCellValue('AH' . $rowCount, $data[$k][33]);
                    $rowCount++;
                }
            }

            /* end excel activities */
            ++$a;
            $a = sprintf("%02d", $a);
            ++$sheet;
        }//end of while
//         die();
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $name . '".xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

//End Func
}

/* End of file csv_helper.php */
/* Location: ./applicaton/library/csv_helper.php */