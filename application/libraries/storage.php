<?php

require_once 'storage/StorageException.php';

/**
 * Storage class is the library that handles all file storages, see 
 * application/config/storage.php for configurations
 * 
 */
class storage {

    /**
     *
     * @var StorageAdapter
     */
    protected $adapter;
    protected $currentContainer;
    protected $config;

    public function __construct($storageConfig = [], $configSet = false) {
        if (!$configSet) {
            $configPath = APPPATH . 'config/';

            $config = null;
            $storageConfig = [];

            if (file_exists($configPath . 'storage.php')) {
                include $configPath . 'storage.php';
            }

            if (defined('ENVIRONMENT')) {
                $configEnv = $configPath . 'storage.' . ENVIRONMENT . '.php';
                if (file_exists($configEnv)) {
                    include $configEnv;
                }
            }
            
            if (!empty($config)) {
                $storageConfig = $config['storage'];
            }
        }



        if (!isset($storageConfig['adapter']) || !$storageConfig['adapter']) {
            throw new StorageException("Storage adapter was not set!");
        }

        $adp = $storageConfig['adapter'];
        if (!isset($storageConfig[$adp]) || !is_array($storageConfig[$adp])) {
            throw new StorageException("Invalid configuration for adapter : {$adp}!");
        }

        $this->config = $storageConfig;
        $this->loadAdapter();
        $this->setCurrentContainer();
    }

    private function loadAdapter() {
        $adapterClass = $this->config['adapter'];
        $path = __DIR__ . '/storage/adapters/' . strtolower($adapterClass) . '.php';
        if (!file_exists($path)) {
            throw new StorageException("class file for adapter: {$adapterClass} was not found!");
        }

        require_once $path;
        if (!class_exists($adapterClass)) {
            throw new StorageException("Adapter Class [{$adapterClass}] does not exists.");
        }

        $adpaterConfig = $this->config[$adapterClass];
        $this->adapter = new $adapterClass($adpaterConfig);
    }

    /**
     * This method is no longer neccessary
     * @deprecated since version 1.1
     * @param array $config
     * @throws StorageException
     */
    public function connect(array $config) {
        //throw new StorageException();
        if (isset($config['container'])) {
            $this->setCurrentContainer($config['container']);
        }
    }

    /**
     * @deprecated since version 1.1 retained for legacy purpose
     * @return boolean
     */
    public function close() {
        return true;
    }

    public function create_container($container) {
        return $this->adapter->createContainer($container) && $this->currentContainer = $container;
    }

    public final function setCurrentContainer($container = '') {
        if (!$container && function_exists('load_class')) {
            /* @var $userAuth user_auth */
            $userAuth = load_class('user_auth', 'libraries', '');
            if ($userAuth->logged_in()) {
                $container = $userAuth->get('cdn_container');
            } else {
                /* @var $subdomain subdomain */
                $subdomain = load_class('subdomain', 'libraries', '');
                $company = $subdomain->get_company();
                if ($company) {
                    $container = $company->cdn_container;
                }
            }
        }

        $this->currentContainer = $container;
    }

    public function getCurrentContainer($name = '') {
        return $name !== '' ? $name : $this->currentContainer;
    }

    public function upload_file(array $file, $container = '') {
        $path = $file['file_path'];
        $name = $file['file_name'];
        if (is_file($path) && $this->adapter->saveFile($path, $name, $this->getCurrentContainer($container))) {
            return $this->temporary_url($name);
        }

        return null;
    }

// End func upload_file

    public function delete_file($fileName, $container = null) {
        return $this->adapter->deleteFile($fileName, $this->getCurrentContainer($container));
    }

    /**
     * @todo completed list objects probably for later consideration.
     * @throws StorageException
     */
    public function list_objects($container = '') {
        throw new StorageException('Method is not implemented yet for all containers : ' . $container);
    }

    public function move_to_container($container_to, $container_from, $file_name) {
        return $this->adapter->moveFile($container_from, $container_to, $file_name);
    }

// End func move_to_container

    /**
     * This method returns a proxy URL that can be used to access files
     * @param string $fileName
     * @param string $container defaults to current user's container
     * @return string
     */
    public function temporary_url($fileName, $container = '', $download = false) {
        if (preg_match('/^(https?|ftp):\/\//', $fileName)) {
            return $fileName;
        }

        $container = $this->getCurrentContainer($container);
        return site_url('storage/file/' . ($container ? $container : '-') . '/' . $fileName)
                . ($download ? '?dl=1' : '');
    }

    public function get_absolute_path($fileName, $container = '') {
        $container = $this->getCurrentContainer($container);
        if (($tempCopy = $this->adapter->getCachedCopy($fileName, $container))) {
            return $tempCopy;
        }
        return $this->adapter->getAbsolutePath($fileName, $container);
    }

    /**
     * 
     * @return StorageAdapter
     */
    public function getAdapter() {
        return $this->adapter;
    }

    public function getMimeType($ext) {
        static $extensions = array();
        if (empty($extensions)) {
            $extensions = include(__DIR__ . '/storage/ext_mime_map.php');
        }

        $ext = strtolower(trim($ext));
        return array_key_exists($ext, $extensions) ? $extensions[$ext] : 'application/octet-stream';
    }

}
