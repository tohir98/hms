<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Talentbase
 * Tasks library (for all users)
 * 
 * @category   Library
 * @package    Tasks
 * @subpackage Tasks
 * @author     Tohir O. <thohiru.omoloye@talentbase.ng>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */
class internet {
    //put your code here
    
    /**
	 * Check if there is Internet
	 * 
	 * @access public
	 * @param 
	 * @return (bool )
	 **/
    public function check_connectivity(){
        
         $connected = @fsockopen("www.google.com", 80); 
                                        //website, port  (try 80 or 443)
        if ($connected){
            $is_conn = true; //action when connected
            fclose($connected);
        }else{
            $is_conn = false; //action in connection failure
        }
        return $is_conn;
        
    } // end func check_connectivity
}

?>
