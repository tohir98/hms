<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Talentbase
 * Admin authentication library
 * 
 * @category   Library
 * @package    module_settings
 * @subpackage module_settings
 * @author     Obinna Ukpabi <obinna.ukpabi@talentbase.ng>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 * @property CI_DB_active_record $db database.
 */


class company_module_settings{

	/**
     * Codeigniter instance
     * 
     * @access private
     * @var object
     */
    private $CI;

    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        // Load CI object
        $this->CI = get_instance();

        // Load libraries
        $this->CI->load->library('session');

        // Load models
        $this->CI->load->model('tb/admin_model');

        // Load helper
        $this->CI->load->helper('url');

        $this->db = $this->CI->load->database('', true);
    }


    /**
     * get module setting
     *
     * @access public
     * @param int $id_company
     * @param int $id_module
     * @param string $setting_name
     * @return (bool | array)
     * */
    public function get($id_company, $id_module, $setting_name) {

    	$this->CI->db->where('id_company', $id_company);
    	$this->CI->db->where('id_module', $id_module);
    	$this->CI->db->where('setting_name', $setting_name);
    	$q =  $this->CI->db->get('company_module_settings')->result();
    	return $q;
    }


    /**
	 * add data to company_module_settings
	 *
     * @access public
     * @param $data array
     * @return (bool | array) 
     */
    public function add($data){

        if(empty($data)){
            return false;
        }

        $q = $this->CI->db->insert('company_module_settings', $data);
        return $q;
                
    }// End func


    /**
     * update data to company_module_settings
     * 
     * @access public
     * @param $data array
     * @param $where array
     * @return (bool | array) 
     */
    public function update($data, $where){

        if(empty($data) OR empty($where)){
            return false;
        }
        
        $q = $this->CI->db->update('company_module_settings', $data, $where);
        return $q;

    }// End func



    /**
     * delete data from company_module_settings
     * 
     * 
     * @access public
     * @param $where array
     * @return (bool | array) 
     */
    public function delete($where){

        if(empty($where)){
            return false;
        }
        
        $q = $this->CI->db->delete('company_module_settings', $where);
        return $q;

        
    }// End func
}
