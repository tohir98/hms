<?php

require_once 'vendor/soqrate/html2pdf/HTML2PDF.php';

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



class html2pdf_lib {
	
	public function run_pdf_version($content){
		
		try {
			
			$html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8', array(15, 5, 15, 5));
			
			$html2pdf->pdf->SetDisplayMode('fullpage');
			$html2pdf->writeHTML($content);
			$html2pdf->Output('exemple02.pdf');
			
		} catch (Exception $ex) {
			echo $ex->getMessage();
		}
	}
}