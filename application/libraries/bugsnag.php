<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Talentbase
 * Bugsnag library
 * 
 * @category   Library
 * @package    bugsnag
 * @subpackage Nav
 * @author     Obinna Ukpabi <obinna.ukpabi@talentbase.ng>
 * @copyright  Copyright Â© 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */
require_once "Bugsnag/Autoload.php";

class Bugsnag
{

    protected $ci;

    /**
     *
     * @var Bugsnag_Client
     */
    protected $bugsnag_client;
    protected $handle_errors = true;

    protected $stages = array('testing', 'production');
    /**
     * Email params
     * 
     * @access private
     * @var array
     */
    public function __construct()
    {
        $this->ci = & get_instance();
        $this->bugsnag_client = new Bugsnag_Client("3b403e957d9969ab7edc0aa2a53f5e3b");

        $this->bugsnag_client
                ->setReleaseStage(ENVIRONMENT)
                ->setErrorReportingLevel(error_reporting())
                ->setNotifyReleaseStages($this->stages);

        $name = $this->ci->session->userdata('display_name') . ' ' . $this->ci->session->userdata('last_name');

        $this->bugsnag_client->setUser(array(
            'name' => ucwords($name),
            'email' => $this->ci->session->userdata('email'),
            'id_user' => $this->ci->session->userdata('id_user'),
            'company_name' => $this->ci->session->userdata('company_name'),
            'company id_string' => $this->ci->session->userdata('company_id_string')
        ));
        
        $this->setHandleError(in_array(ENVIRONMENT, $this->stages));
    }

    public function errorHandler($errno, $errstr, $errfile = '', $errline = 0)
    {
        return $this->bugsnag_client->errorHandler($errno, $errstr, $errfile, $errline);
    }
    
    /**
     * @return Bugsnag_Client client
     */
    public function getClient(){
        return $this->bugsnag_client;
    }

    private function setHandlers()
    {
        set_error_handler(array($this, "errorHandler"));
        set_exception_handler(array($this, "exceptionHandler"));
    }

    private function resetHandlers()
    {
        //set_exception_handler(NULL);
        restore_exception_handler();
        //set_error_handler(NULL);
        restore_error_handler();
    }

    public function exceptionHandler($exception)
    {
        return $this->bugsnag_client->exceptionHandler($exception);
    }

    final public function setHandleError($bool = true)
    {
        $this->handle_errors = $bool;
        if ($bool) {
            $this->setHandlers();
        } else {
            $this->resetHandlers();
        }
        return $this;
    }

    public function isHandleErrors()
    {
        return $this->handle_errors;
    }

}

/* End of file bugsnag.php */
/* Location: ./application/libraries/bugsnag.php */
