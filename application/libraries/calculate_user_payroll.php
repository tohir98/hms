<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Talentbase
 * Bugsnag library
 * 
 * @category   Library
 * @package    Payroll
 * @subpackage Calculate User Payroll
 * @author     Obinna Ukpabi <obinna.ukpabi@talentbase.ng>
 * @copyright  Copyright Â© 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */
require_once "Bugsnag/Autoload.php";

class Calculate_user_payroll
{
    protected $ci;
    private $payroll_interval_months = 12;
    private $period = array(
        'Week' => 52,
        'Month' => 12,
        'Annual' => 1
    );

    /**
     * Construct
     * 
     * @access private
     * @var array
     */
    public function __construct()
    {
        $this->ci = & get_instance();
        $this->ci->load->model('payroll/compensation_model', 'comp');

    }

    public function get_user_payroll($id_user) {

        if ($id_user === '') {
            return 0;
        }        

        $comp = $this->ci->comp->fetch_employee_compensation($id_user);
        $compensation = $comp[0];
        
        $divisor = $this->ci->comp->getDivisor($compensation->schedule_type, $compensation->id_payroll_period);

        $allowances = json_decode($compensation->components);
        $grosspay = 0;
        $grosspayTaxable = 0;
        if (!empty($allowances)) {
            foreach ($allowances as $all) {
                if ($all->component_taxable == TAXABLE) {
                    $grosspayTaxable += $all->amount;
                }
                $grosspay += $all->amount;
            }
        }

        $deduction = json_decode($compensation->deduction);

        $sum_deduction = 0;
        $taxable_deduction = 0;
        if (!empty($deduction)) {
            foreach ($deduction as $d) {
                if (isTaxableDeduction($d->deduction_name)) {
                    $taxable_deduction += $d->deduction_amount;
                }
                $sum_deduction += $d->deduction_amount;
            }
        }

        $tax = calculate_tax($grosspayTaxable, $taxable_deduction, $this->period[$compensation->payroll_period]) / $this->period[$compensation->payroll_period];

        return $data = array(
            'take_home' => ($grosspay - ($sum_deduction + $tax))/$divisor,
            'total_deduction' => $sum_deduction/$divisor,
            'tax' => $tax/$divisor,
            'anuual_take_home' => $grosspay - ($sum_deduction + $tax),
        );
    }

}