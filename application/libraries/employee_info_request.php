<?php

require_once 'lib.php';

/**
 * Description of employee_info_request
 *
 * @author JosephT
 * @property tasks $tasks tasks library
 * @property Tasks_model $t_model tasks model
 * @property User_model $u_model tasks model
 * @property Employee_model $e_model employee model
 * @property user_auth  $user_auth
 * @property mailer $mailer
 */
class Employee_info_request extends lib {

    //start clearables
    private $requests = [];
    protected $id_string;
    protected $due_date;
    protected $id_temp_request;
    protected $user;
    //end clearables

    protected $id_company;
    protected static $perms = array(
        'emergency1' => EDIT_MY_EMERGENCY_CONTACT_1,
        'emergency2' => EDIT_MY_EMERGENCY_CONTACT_2,
            //'pension_details' => EDIT_MY_BENEFITS_DETAILS,
    );
    private $request_type_names = array(
        1 => 'Work Email',
        2 => 'Work Phone',
        3 => 'Birthday',
        4 => 'Home Address',
        5 => 'Personal Phone',
        6 => 'Emergency Contact 1',
        7 => 'Emergency Contact 2',
        8 => 'Document Upload',
        9 => 'Bank Details',
        10 => 'Pension Details',
        11 => 'Gender',
        12 => 'Marital Status',
        13 => 'Country',
        14 => 'City',
        15 => 'State',
        16 => 'Nationality',
    );
    private $request_types = array(
        'work_email' => 1,
        'work_phone' => 2,
        'birthday' => 3,
        'home_address' => 4,
        'personal_phone' => 5,
        'emergency1' => 6,
        'emergency2' => 7,
        'uploaded_docs' => 8,
        'bank_account' => 9,
        'pension_details' => 10,
        'gender' => 11,
        'marital_status' => 12,
        'country' => 13,
        'id_country' => 13,
        'city' => 14,
        'state' => 15,
        'id_state' => 15,
        'nationality' => 16, //for backward compatibility
        'id_nationality' => 16, //for remap
    );

    public function __construct() {
        parent::__construct();
        $this->load->library(['tasks', 'mailer', 'user_auth']);
        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('user/user_model', 'u_model');
        $this->load->model('tasks/tasks_model', 't_model');
        $this->id_company = $this->user_auth->get('id_company');
    }

    public function clear() {
        $this->requests = [];
        $this->user = $this->due_date = $this->id_temp_request = $this->id_string = null;
        return $this;
    }

    public function sendRequest($id_string, $due_date, $requests, &$user = null, array $notes = [], array $taskExtra = []) {
        $this->clear();
        $this->id_string = $id_string;
        $this->due_date = date('Y-m-d', strtotime($due_date));

        $this->requests = array_filter($requests, function($req) {
            return array_key_exists($req, $this->request_types);
        });
        //get user
        if (!($user = $this->_getUser())) {
            log_message('error', 'User was not found.');
            return false;
        }

        //chec for request
        $chk = array('id_user' => $this->user->id_user, 'id_company' => $this->id_company);

        //check if user has a temp request
        if (!($this->id_temp_request = $this->e_model->getIdTempRequest($chk))) {
            $this->id_temp_request = $this->_insert_temp_request(array(
                'id_user' => $this->user->id_user,
                'id_string' => $this->id_string,
                'due_date' => $this->due_date,
            ));
        }

        if (!$this->id_temp_request) {
            log_message('error', 'Send request failed for user: ' . $this->id_string);
            return false;
        }

        $processed = true;
        foreach ($this->requests as $to_request) {
            if (array_key_exists($to_request, $this->request_types) && $this->_process($to_request, $notes[$to_request], $taskExtra)) {
                ++$processed;
            }
        }

        if ($processed > 0) {
            $this->_send_mail();
        }


        return $processed > 0;
    }

    private function _send_mail($url = 'personal/my_profile_info', $req = null) {
        $mail_data = array(
            'first_name' => $this->user->first_name,
            'company_name' => $this->user_auth->get('company_name'),
            'company_string' => $this->user_auth->get('company_id_string'),
            'company_url' => site_url($url),
            'request_types' => $this->request_types,
            'request_type_names' => $this->request_type_names,
            'requests' => $this->requests,
            'req' => $req,
        );

        $msg = $this->load->view('email_templates/employee_request', $mail_data, true);

        // send email notification
        $this->send_request_notification($msg, $this->user->email);
    }

    public function send_request_notification($msg, $email) {
        $sender = $this->user_auth->get('display_name') . ' ' . $this->user_auth->get('last_name');
        return $this->mailer->setFrom($this->user_auth->get('email'), $sender)
                        ->sendMessage('New Task: Information Request from HR', $msg, $email);
    }

    public function getRequestTypes() {
        return $this->request_types;
    }

    private function _insert_temp_request(array $params) {
        $params['id_company'] = $this->user_auth->get('id_company');
        // insert into employee_temp_request
        $id_temp_request = $this->e_model->insert_data(array(
            'table' => 'employee_temp_request',
            'vals' => $params
        ));
        return $id_temp_request;
    }

    public function getRequestTypeNames() {
        return $this->request_type_names;
    }

    private function _process($request, $note = null, array $taskExtra = []) {
        $title = $this->request_type_names[$this->request_types[$request]];
        $request_desc = $note ? : "Please fill out your {$this->request_type_names[$this->request_types[$request]]}";

        $sender = $this->user_auth->get('display_name') . ' ' .
                $this->user_auth->get('last_name');


        $request_sub = "{$title} request from " . $sender;

        $id_task = $this->_request_task($request_desc, $request_sub, $this->_get_perm($request), 'personal/my_profile_info', $taskExtra);

        return $this->_insert_temp_request_task(array(
                    'id_temp_request' => $this->id_temp_request,
                    'request_type' => $this->request_types[$request],
                    'id_task' => $id_task,
        ));
    }

    private function _get_perm($request) {
        if (array_key_exists($request, static::$perms)) {
            return static::$perms[$request];
        }

        $const = 'EDIT_MY_' . strtoupper($request);
        return static::$perms[$request] = (defined($const) ? constant($const) : EDIT_MY_HOME_ADDRESS);
    }

    private function _request_task($request_desc, $request_sub, $perm_id, $url = 'personal/my_profile_info', array $taskParams = []) {

        $link = anchor($url, $request_sub);

        // Add temp perm

        if ($perm_id) {
            $perms_temp = array(
                'id_user' => $this->user->id_user,
                'id_creator' => $this->user_auth->get('id_user'),
                'date_expire' => $this->due_date,
                'id_module' => 1,
                'date_created' => date('Y-m-d'),
                'id_company' => $this->id_company,
                'id_perm' => $perm_id
            );

            $tb_id_record = $this->u_model->replace_perm_temp(0, $perms_temp)[0];
        }


        $tasks = array_merge(array(
            'id_user_from' => $this->user_auth->get('id_user'),
            'id_user_to' => $this->user->id_user,
            'id_module' => 1,
            'subject' => $link,
            'status' => 0,
            'date_will_start' => date('Y-m-d'),
            'date_will_end' => $this->due_date,
            'tb_name' => 'user_perms_temp',
            'tb_column_name' => 'id_perms_temp',
            'tb_id_record' => $tb_id_record,
            'notes' => $request_desc
                ), $taskParams);

        //$this->t_model->add_task($tasks);
        return $this->t_model->create_task($tasks);
    }

    private function _insert_temp_request_task(array $params) {
        $params['status'] = EMPLOYEE_REQUEST_PENDING;
        // insert into emp_temp_request_task
        return $this->e_model->insert_data(array(
                    'table' => 'employee_temp_request_task',
                    'vals' => $params
        ));
    }

    private function _getUser() {
        return $this->user = $this->e_model->get_where('users', array(
                    'id_string' => $this->id_string,
                    'id_company' => $this->id_company
                ))[0];
    }

    public function requestCredentials($id_string, $title, $description, $due_date, array $taskExtras = []) {
        $this->clear();
        if (!$title && !trim($description)) {
            return false;
        }

        $this->due_date = date_format_strtotime($due_date, 'Y-m-d');
        $this->id_string = $id_string;

        if (!$this->_getUser()) {
            log_message('error', 'User was not found.');
            return false;
        }

        $ins_data = array(
            array(
                'id_user' => $this->user->id_user,
                'type' => 0,
                'description' => $description,
                'file_title' => $title,
                'id_uploader_user' => $this->user_auth->get('id_user'),
                'due_date' => $due_date
            )
        );

        $url = site_url('personal/my_profile#credentials_container');
        $this->db->trans_start();
        $this->e_model->insert_credentials($ins_data);
        $doc_id = $this->db->insert_id();

        //complexities
        $taskExtras = array_merge($taskExtras, array(
            'tb_name' => 'employee_credentials',
            'tb_column_name' => 'id_cred',
            'tb_id_record' => $doc_id,
        ));

        $this->_request_task(sprintf("Upload document : (%s : %s)", $title, $description), $title, null, $url, $taskExtras);
        $this->db->trans_complete();
        $this->_send_mail($url, '<b>Documents Upload</b> <br>' . $description . '<br>');
        return true;
    }

    public function createCustomTask($id_string, $description, $due_date, array $taskExtras = [], array $approvers = []) {
        $this->clear();

        if (!trim($description)) {
            return false;
        }

        $this->due_date = date_format_strtotime($due_date, 'Y-m-d');
        $this->id_string = $id_string;

        if (!$this->_getUser()) {
            log_message('error', 'User was not found.');
            return false;
        }

        $this->db->trans_start();
        
        $this->load->helper(['text']);
        $url = 'personal/my_tasks';
        
        $taskExtras['approvers'] = $approvers;
        $this->_request_task("Task : {$description}", word_limiter($description, 5), null, 'personal/my_tasks', $taskExtras);
        
        
        $this->db->trans_complete();
        $this->_send_mail($url, $description . '<br>');
        return true;
    }

    public function getRequestNames() {
        $data = [];
        foreach ($this->request_types as $k => $k_id) {
            $data[$k] = $this->request_type_names[$k_id];
        }

        return $data;
    }

}
