<?php

use OpenCloud\ObjectStore\Resource\Container;
use OpenCloud\ObjectStore\Service;
use OpenCloud\Rackspace;

require_once 'RemoteSource.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class cloudfiles extends RemoteSource {

    private $username;
    private $api_key;

    /**
     *
     * @var Service
     */
    private $service;

    /**
     *
     * @var Container[]
     */
    private $containers;
    private $expirationTimeInSeconds = 3600;
    private $httpMethodAllowed = 'GET';

    public function __construct($config, $temp = '') {
        parent::__construct($config, $temp);
        if (!isset($config['api_key'], $config['username'])) {
            throw new FileStorageException("API Key and Username must be set");
        }

        if (!($this->username = $config['username']) && defined('RACKSPACE_USERNAME')) {
            $this->username = RACKSPACE_USERNAME;
        }

        if (!($this->api_key = $config['api_key']) && defined('RACKSPACE_API_KEY')) {
            $this->api_key = RACKSPACE_API_KEY;
        }
    }

    /**
     * Gets a container from cloud file
     * @param string $name the name of the container
     * @return Container
     */
    private function getContainer($name) {
        if (!isset($this->containers[$name])) {
            $this->containers[$name] = $this->getService()->getContainer($name);
        }

        return $this->containers[$name];
    }

    /**
     * @return Service object store service
     */
    private function getService() {
        if (!$this->service) {
            // 1. Instantiate a Rackspace client.
            $client = new Rackspace(Rackspace::US_IDENTITY_ENDPOINT, array(
                'username' => $this->username,
                'apiKey' => $this->api_key
            ));

            $this->service = $client->objectStoreService('cloudFiles', 'LON');
        }

        return $this->service;
    }

    public function createContainer($dir) {
        try {
            $this->containers[$dir] = $this->service->createContainer($dir);
            return true;
        } catch (Exception $ex) {
            $this->reportException($ex);
            return false;
        }
    }

    public function deleteFile($fileName, $dir = '') {
        try {
            $object = $this->getContainer($dir)->getObject($fileName);
            if ($object) {
                return $object->delete();
            }
        } catch (Exception $ex) {
            $this->reportException($ex);
        }

        return false;
    }

    public function getAbsolutePath($fileName, $dir = '') {
       return $this->_remoteToLocal($fileName, $dir);
    }

    public function getFileSize($fileName, $dir = '') {
        try {
            $container = $this->getContainer($dir);
            if ($container->objectExists($fileName) && ($object = $container->getObject($fileName))) {
                /* @var $object \OpenCloud\ObjectStore\Resource\DataObject */
                return $object->getContentLength();
            }
        } catch (Exception $ex) {
            $this->reportException($ex);
        }
        return -1;
    }

    public function getTempUrl($fileName, $dir = '') {
        try {
            $container = $this->getContainer($dir);
            if ($container->objectExists($fileName) && ($object = $container->getObject($fileName))) {
                return $object->getTemporaryUrl($this->expirationTimeInSeconds, $this->httpMethodAllowed);
            }
        } catch (Exception $ex) {
            $this->reportException($ex);
        }

        return null;
    }

    public function moveFile($oldDir, $newDir, $fileName) {
        try {
            $containerFrom = $this->getContainer($oldDir);
            if ($containerFrom->objectExists($fileName) && ($object = $containerFrom->getObject($fileName))) {
                $object->copy($newDir . '/' . $fileName);
                $object->delete();
                return true;
            }
        } catch (Exception $ex) {
            $this->reportException($ex);
        }

        return false;
    }

    public function saveFile($sourceFilePath, $fileName, $dir = '') {
        $fileData = fopen($sourceFilePath, 'r');
        return $this->saveFileResource($fileData, $fileName, $dir);
    }

    public function saveFileResource($fhResource, $fileName, $dir = '') {
        try {
            if (is_resource($fhResource)) {
                return $this->getContainer($dir)->uploadObject($fileName, $fhResource);
            }
        } catch (Exception $ex) {
            $this->reportException($ex);
        }
        return false;
    }

}
