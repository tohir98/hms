<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once __DIR__ . '/../StorageAdapter.php';
require_once __DIR__ . '/../StorageException.php';

class localstorage extends StorageAdapter {

    protected $dir;

    const DEFAULT_DIR_PERM = 0777;

    public function __construct($config, $temp = '') {
        parent::__construct($config, $temp);
        if (!isset($config['dir'])) {
            throw new StorageException("The direcory to local storage is not set!");
        }

        $dir = $config['dir'];
        if (!is_dir($dir) && !mkdir($dir, self::DEFAULT_DIR_PERM, true)) {
            throw new StorageException("local storage directory [{$dir}] not found");
        }

        if (!is_readable($dir) || !is_writable($dir)) {
            throw new StorageException("local storage directory [{$dir}] not readable or writable");
        }

        $this->dir = realpath($dir) . DIRECTORY_SEPARATOR;
    }

    public function createContainer($dir) {
        return mkdir($this->_dir($dir, false), self::DEFAULT_DIR_PERM);
    }

    private function _dir($dir, $make = true) {
        $d = $this->dir;
        if ($dir) {
            $d .= $dir . DIRECTORY_SEPARATOR;
        }

        if (!is_dir($d) && $make) {
            return mkdir($d, self::DEFAULT_DIR_PERM) ? $d : null;
        }
        return $d;
    }

    private function _file($fileName, $dir = '', $checkExist = false) {
        $d = $this->_dir($dir, true);
        if($fileName){
            $d .= $fileName;
        }

        return !$checkExist || is_file($d) ? $d : null;
    }

    public function deleteFile($fileName, $dir = '') {
        return unlink($this->_file($fileName, $dir));
    }

    public function getAbsolutePath($fileName, $dir = '') {
        return $this->_file($fileName, $dir, true);
    }

    public function getFileSize($fileName, $dir = '') {
        $file = $this->_file($fileName, $dir, true);
        return $file ? filesize($file) : -1;
    }

    public function moveFile($oldDir, $newDir, $fileName) {
        $oldFile = $this->_file($fileName, $oldDir, true);
        if ($oldFile) {
            return rename($oldFile, $this->_file($fileName, $newDir));
        }
        return false;
    }

    public function saveFile($sourceFilePath, $fileName, $dir = '') {
        return rename($sourceFilePath, $this->_file($fileName, $dir));
    }

    public function saveFileResource($fhResource, $fileName, $dir = '') {
        $file = $this->_file($fileName, $dir);
        if (!is_resource($fhResource)) {
            throw new StorageException("Invalid file handle resource");
        }

        return file_put_contents($file, stream_get_contents($fhResource));
    }

    public function getCachedCopy($fileName, $dir = '') {
        return $this->_file($fileName, $dir, true);
    }

}
