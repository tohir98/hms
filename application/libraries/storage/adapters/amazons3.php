<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'RemoteSource.php';

class AmazonS3 extends RemoteSource {

    protected $bucket;
    protected $credential;

    /**
     *
     * @var \Aws\S3\S3Client
     */
    protected $client;

    public function __construct($config, $temp = '') {
        parent::__construct($config, $temp);
        if (!$config['bucket']) {
            throw new StorageException('AmazonS3 requires a bucket to be set');
        }

        $this->bucket = $config['bucket'];

        if (!($config['access_key'] && $config['access_secret']) && !$config['profile']) {
            throw new StorageException('Access Key/Secret pair should be set or a profile should be specified');
        }

        $s3Config = array();
        if ($config['access_key'] && $config['access_secret']) {
            $s3Config = ['key' => $config['access_key'], 'secret' => $config['access_secret']];
        } else {
            $s3Config = ['profile' => $config['profile']];
        }

        $this->client = \Aws\S3\S3Client::factory($s3Config);
        \Aws\S3\StreamWrapper::register($this->client);

//        $context = stream_context_create(array(
//            's3' => array(
//                //'seekable' => true,
//                'throw_errors' => true,
//            )
//        ));
    }

    private function _path($dir, $fileName = '') {
        $path = 's3://' . $this->bucket;
        if($dir){
            $path .= '/' . trim($dir, '/\\');
        }
        
        if ($fileName) {
            $path .= '/' . trim($fileName, '/\\');
        }
        return $path;
    }

    public function createContainer($dir) {
        return mkdir($this->_path($dir), 0700);
    }

    public function deleteFile($fileName, $dir = '') {
        return unlink($this->_path($dir, $fileName));
    }

    public function getAbsolutePath($fileName, $dir = '') {
        return $this->_remoteToLocal($fileName, $dir);
    }

    public function getFileSize($fileName, $dir = '') {
        return filesize($this->_path($dir, $fileName));
    }

    public function getTempUrl($fileName, $dir = '') {
        $path = $this->_path($dir, $fileName);
        if(!file_exists($path)){
            return null;
        }
        
        return $path;
    }
    
    public function moveFile($oldDir, $newDir, $fileName) {
        $old = $this->_path($oldDir, $fileName);
        $new = $this->_path($newDir, $fileName);
        return rename($old, $new);
    }

    public function saveFile($sourceFilePath, $fileName, $dir = '') {
        $newName = $this->_path($dir, $fileName);
        return copy($sourceFilePath, $newName);
    }

    public function saveFileResource($fhResource, $fileName, $dir = '') {
        $file = $this->_path($fileName, $dir);
        if (!is_resource($fhResource)) {
            throw new StorageException("Invalid file handle resource");
        }

        return file_put_contents($file, stream_get_contents($fhResource));
    }

}
