<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once __DIR__ . '/../StorageAdapter.php';
require_once __DIR__ . '/../StorageException.php';

/**
 * Description of RemoteSource
 *
 * @author JosephT
 */
abstract class RemoteSource extends StorageAdapter {

    //put your code here
    /**
     * Used to store remote files to temporary drive
     * @param string $fileName
     * @param string $dir
     * @return string
     */
    protected function _remoteToLocal($fileName, $dir = '') {
        $tempUrl = $this->getTempUrl($fileName, $dir);
        $tempFolder = $this->getTempDir() . DIRECTORY_SEPARATOR . $dir;
        if ($tempUrl && (file_exists($tempFolder) || mkdir($tempFolder, 0700, true))) {
            $localPath = $tempFolder . DIRECTORY_SEPARATOR . $fileName;
            if (file_exists($localPath) || file_put_contents($localPath, file_get_contents($tempUrl))) {
                return $localPath;
            }
        }

        return null;
    }

    public function getCachedCopy($fileName, $dir = '') {
        $copy = $this->getTempDir() . '/' . ($dir ? "{$dir}/" : '') . $fileName;
        return file_exists($copy) ? $copy : null;
    }

    /**
     * Gets a temporary URL for a file that can be used by users to access the file content.
     * @param string $fileName name of the file
     * @param string $dir container or folder.
     * @return string The URL to access file, or null if it fails.
     */
    abstract public function getTempUrl($fileName, $dir = '');
}
