<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FileStorageInterface
 *
 * @author JosephT
 */
abstract class StorageAdapter {

    /**
     *
     * @var file storage config, see application/config/storage.php
     */
    protected $config = array();
    protected $temp = '';

    /**
     *
     * @var Bugsnag
     */
    protected $bugsnag = null;

    /**
     * Defines default constructor for adapters
     * @param array $config the config specific to this adapter $config['storage'][adaptername]
     * @param string $temp path to temp drive defined in $config['storage']['temp_path']
     */
    public function __construct($config, $temp = '') {
        $this->config = $config;
        $this->temp = $temp;
        if (function_exists('load_class')) {
            $this->bugsnag = load_class('bugsnag', 'libraries', '');
        }
    }

    /**
     * Saves a file from source to a given destination.
     * @param string $sourceFilePath path to the source file
     * @param string $fileName path to the new name
     * @param string $dir or container as the case may be.
     * @return boolean true if successful, false otherwise
     * 
     */
    abstract public function saveFile($sourceFilePath, $fileName, $dir = '');

    /**
     * Saves a file from from a readable file handle resource
     * @param string $fhResource the resource handle for source file
     * @param string $fileName path to the new name
     * @param string $dir or container as the case may be.
     * @return boolean true if successful, false otherwise
     * 
     */
    abstract public function saveFileResource($fhResource, $fileName, $dir = '');

    /**
     * Returns absolute path to a file, which can then be used within application
     * @param string $fileName
     * @param string $dir directory/container name.
     * @return string the absolute path to the file copy, null if it fails.
     */
    abstract public function getAbsolutePath($fileName, $dir = '');

    /**
     * Deletes a file from the given storage
     * @param string $fileName name of the file
     * @param string $dir container or folder.
     * @return boolean true if successful, false otherwise
     */
    abstract public function deleteFile($fileName, $dir = '');

    /**
     * Returns the size of the file in bytes
     * @param string $fileName name of the file
     * @param string $dir container or folder.
     * @return long the size of the file or -1 if it fails
     */
    abstract public function getFileSize($fileName, $dir = '');

    /**
     * 
     * @param string $oldDir old directory / container
     * @param string $newDir new directory / container
     * @param string $fileName the file being moved
     * @return boolean true if successful, false otherwise
     * 
     */
    abstract public function moveFile($oldDir, $newDir, $fileName);

    /**
     * Creates directory or container with the specified name
     * @param string $dir directory or container
     * @return boolean true if successful, false otherwise
     */
    abstract public function createContainer($dir);

    protected function getTempDir() {
        if (!$this->temp || !is_dir($this->temp) || !is_writable($this->temp)) {
            $this->temp = sys_get_temp_dir();
        }

        $tempDir = $this->temp . DIRECTORY_SEPARATOR . strtolower(preg_replace('/[^A-Za-z0-9]+/', '', get_class($this)));
        if (!file_exists($tempDir)) {
            mkdir($tempDir, 0700);
        }

        return $tempDir;
    }

    protected function reportException(Exception $ex) {
        if ($this->bugsnag) {
            log_message('error', get_class($ex) . ' occured: ' . $ex->getMessage());
            $this->bugsnag->getClient()->notifyException($ex);
        }else{
            trigger_error($ex->getMessage(), E_USER_WARNING);
        }
    }

    abstract public function getCachedCopy($fileName, $dir = '');
}
