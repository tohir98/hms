<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Talentbase
 * Meta tags library (for all pages)
 * 
 * @category   Library
 * @package    Tempaltes
 * @subpackage Meta tags
 * @author     David A. <tokernel@gmail.com>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

class meta_tags {
    
    /**
     * Codeigniter instance
     * 
     * @access private
     * @var object
     */
    private $CI;
    
	/**
	 * Meta tags
	 * 
	 * @access private
	 * @var array
	 */
	private $tags = array();
	
    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        // Load CI object
        $this->CI = get_instance();
        
		// Load model (example)
        // $this->CI->load->model('test/test_model');
    }
   
	public function set_tag($data) {
		$this->tags[] = $data;
	} // End func set_tag
	
	public function print_all_tags() {
		
		if(empty($this->tags)) {
			return;
		}
		
		foreach($this->tags as $d) {
			echo "\t" . $d . "\n";
		}
		
	} // End func print_all_tags
	
} // End class meta_tags
