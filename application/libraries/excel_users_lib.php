<?php
/* Load vendor package */


class Excel_users_lib {
	
	/**
	 * Read spreadsheet
	 * 
	 * @access public
	 * @return void 
	 */
	public function read_excel_file($dir){
		
		if($dir == ''){
			return false;
		}
		
		$obj = new PHPExcel_Reader_Excel5();
		$obj->setReadDataOnly(true);
		$obj_excel = $obj->load($dir);
		
		$row_iterator = $obj_excel->getActiveSheet()->getRowIterator();
		
		//$sheet = $obj_excel->getActiveSheet();
		
		foreach($row_iterator as $row){
			$cellIterator = $row->getCellIterator();
			$cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set

			$rowIndex = $row->getRowIndex();
			$array_data[$rowIndex] = array('A'=>'', 'B'=>'','C'=>'','D'=>'','E'=>'','F'=>'','G'=>'','H'=>'','I'=>'','J'=>'','K'=>'','L'=>'','M'=>'','N'=>'','O'=>'','P'=>'','Q'=>'','R'=>'','S'=>'','T'=>'','U'=>'','V'=>'','W'=>'');

			foreach ($cellIterator as $cell) {
				if('A' == $cell->getColumn()){
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				} elseif('B' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				} elseif('C' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				} elseif('D' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				} elseif('E' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				} elseif('F' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				} elseif('G' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				} elseif('H' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				} elseif('I' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				} elseif('J' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				} elseif('K' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				} elseif('L' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				} elseif('M' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				} elseif('N' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				} elseif('O' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				} elseif('P' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				} elseif('Q' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				}elseif('R' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				}elseif('S' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				} elseif('T' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				}elseif('U' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				}elseif('V' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				}elseif('W' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				}elseif('X' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				}elseif('Y' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				}elseif('Z' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				}elseif('AA' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				}elseif('AB' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				}elseif('AC' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				}elseif('AD' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				}elseif('AE' == $cell->getColumn()) {
					$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				}
			}
		}
		
		return $array_data;
		
		//echo '<pre>';
		//print_r($array_data);
		
	}// End func read_excel_file
	
	
}// End class excel_users_lib
