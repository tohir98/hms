<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Talentbase
 * Tasks library (for all users)
 * 
 * @category   Library
 * @package    Tasks
 * @subpackage Tasks
 * @author     David A. <tokernel@gmail.com>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 * 
 * @property Tasks_model $tasks_model tasks model
 */
class tasks {

    /**
     * Codeigniter instance
     * 
     * @access private
     * @var object
     */
    private $CI;

    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        // Load CI object
        $this->CI = get_instance();

        // Load model
        $this->CI->load->model('tasks/tasks_model');
    }

    /**
     * Get active tasks by user id
     *
     * @access public
     * @param int $id_user
     * @return mixed (bool | array)
     * */
    public function get_active_tasks_by_user_id($id_user) {
        // Fetch user permissions
        $result = $this->CI->tasks_model->fetch_records(
                array(
                    'id_user_to' => $id_user,
                    'status' => TASK_STATUS_TO_DO
                )
        );

        if (!$result) {
            return false;
        }

        return $result;
    }

// End func get_active_tasks_by_user_id

    /**
     * Get "To Do" and "In progress" tasks by user id
     *
     * @access public
     * @param int $id_user
     * @return mixed (bool | array)
     * */
    public function get_todo_inprogress_tasks_by_user_id($id_user) {
        // Fetch user permissions
        $result = $this->CI->tasks_model->fetch_by_user_id_status(
                $id_user, array(
            TASK_STATUS_TO_DO,
            TASK_STATUS_IN_PROGRESS
                )
        );

        if (!$result) {
            return false;
        }

        return $result;
    }

// End func get_active_tasks_by_user_id

    /**
     * Get "Completed Task" by user id - Chuks
     * 
     * @access public
     * @param int $id_user
     * @return mixed (bool | array)
     * */
    public function get_completed_tasks_by_user_id($id_user) {

        $result = $this->CI->tasks_model->fetch_by_user_id_status(
                $id_user, array(
            TASK_STATUS_COMPLETE
        ));

        if (!$result) {
            return false;
        }

        return $result;
    }

// End func get_completed_tasks_by_user_id


    public function get_tasks($id_user, $status = null, $maxId = null, $limit = PAGINATION_PER_PAGE) {
        
        $statuses = [];
        if($status !== null){
            $statuses[] = $status;
        }
        
        $where = [];
        if($maxId){
            $where['id_task <'] = $maxId;
        }
        
        return $this->CI->tasks_model->fetch_by_user_id_status($id_user, $statuses, $where, $limit, 0, \api\Task::class);
    }

    /**
     * Get tasks sent by user id - Chuks
     * 
     * @access public
     * @param int $id_user
     * @return mixed (bool | array)
     * */
    public function fetch_tasks_sent_by_user_id($id_user) {

        $result = $this->CI->tasks_model->fetch_tasks_sent_by_user_id($id_user);

        if (!$result) {
            return false;
        }

        return $result;
    }

// End func get_completed_tasks_by_user_id

    /**
     * Push Tasks to the database from other controllers
     *
     * @access public
     * @param arr $data
     * @return mixed (bool)
     * */
    public function push_task($data) {

        $result = $this->CI->tasks_model->add_task($data);

        if (!$result) {
            return false;
        }

        return true;
    }

// End func get_active_tasks_by_user_id

    public function get_ref_arr() {

        return array(
            TASK_STATUS_TO_DO => 'To do',
            TASK_STATUS_COMPLETE => 'Complete',
            TASK_STATUS_IN_PROGRESS => 'In progress',
            TASK_STATUS_DECLINED => 'Decline'
        );
    }

// End func 

    public function update_task(array $params) {

        if (empty($params)) {
            return false;
        }

        $this->CI->tasks_model->change_task_status($params['id_task'], $params['status']);
    }

// End func upddate_tasks
}
