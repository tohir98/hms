<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'lib.php';

/**
 * Description of checklist_lib
 *
 * @author JosephT
 * @property Checklist_Model $checklist_model 
 */
class Checklist_Lib extends lib {

    //put your code here

    private $taskTypeList = [];

    /**
     *
     * @var TaskType[]
     */
    private $taskTypes = [];

    public function __construct() {
        parent::__construct();
        $this->_loadAdapters(__DIR__ . '/tasktypes', $this->taskTypeList, ['_tasktype']);
        $this->init();
        $this->load->model('user/checklist_model', 'checklist_model');
    }

    private function init() {
        foreach ($this->taskTypeList as $type => $info) {
            $this->taskTypes[$type] = new $info['class']();
        }
    }

    /**
     * 
     * @param string $type
     * @return TaskType
     */
    public function getTaskType($type) {
        if (array_key_exists($type, $this->taskTypes)) {
            return $this->taskTypes[$type];
        }

        return null;
    }

    /**
     * 
     * @return TaskType[]
     */
    public function getTaskTypes() {
        return $this->taskTypes;
    }

    public function validate($checklist, &$error) {
        if (!$checklist['title'] || !$checklist['description']) {
            $error = 'Checklist title an desscription must be specified.';
            return false;
        }

        if (empty($checklist['items'])) {
            $error = 'At least one checklist item must be specified.';
            return false;
        }

        $idx = 1;
        foreach ($checklist['items'] as $item) {
            if (!$item['title']) {
                $error = 'Item title was not specified';
            }

            $type = $item['item_type'];
            if (!$type || !($taskType = $this->getTaskType($type))) {
                $error = "Invalid item type was specified at item {$idx}";
                return false;
            }

            if (($error = $taskType->validateParams($item['params']))) {
                return false;
            }

            ++$idx;
        }

        return true;
    }

    /**
     * 
     * @param \employee\Checklist $checklist
     * @param stdClass[] $users
     * @param array $assignData a mapping of id_checklist_item => taskParams{due_date, approvers, etc}
     * @param array $approvers an array of approver user ids in order of how they will be informed.
     * @return boolean
     */
    public function assign(\employee\Checklist $checklist, array $users, array $assignData, array $approvers) {
        $assigned = 0;

        foreach ($assignData as $idx => $data) {
            if (!$data['due_date']) {
                log_message('error', 'No due date found for assignment at : ' . $idx);
                return false;
            }
        }
        
        foreach ($users as $user) {
            if ($this->assignChecklist($checklist, $user, $assignData, $approvers, false)) {
                $assigned++;
            }
        }
        $this->checklist_model->updateChecklist($checklist->id, ['date_last_assigned'=> date('Y-m-d')]);
        return $assigned > 0;
    }

    public function assignChecklist(\employee\Checklist $checklist, $user, array $assignData, array $approvers, $singleMode=true) {
        $this->db->trans_start();
        //create employee checklist
        $employeeChkId = $this->checklist_model->createEmployeeChecklist($checklist, $user, $approvers);

        $assigned = 0;
        if ($employeeChkId) {
            //then assign tasks
            foreach ($checklist->getItems() as $item) {
                $taskType = $this->getTaskType($item->item_type);
                if ($taskType->assignTask($item, $user, $employeeChkId, $assignData[$item->id])) {
                    $assigned++;
                }
            }
        }

        if (!$employeeChkId || $assigned < count($checklist->getItems())) {
            $this->db->trans_rollback();
            log_message('error', "employee checklist ID: {$employeeChkId}, tasks assigned: {$assigned}");
            return false;
        }

        if($singleMode){
            $this->checklist_model->updateChecklist($checklist->id, ['date_last_assigned' => date('Y-m-d')]);
        }
        
        $this->db->trans_complete();
        return true;
    }

}
