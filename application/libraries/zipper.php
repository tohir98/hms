<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of zipper
 *
 * @author JosephT
 */
class Zipper {

    /**
     * Add files and sub-directories in a folder to zip file. 
     * @param string $folder 
     * @param ZipArchive $zipFile 
     * @param int $exclusiveLength Number of text to be exclusived fr1om the file path. 1
     * @param array $exclude list of folders to exclude.
     */
    private static function folderToZip($folder, &$zipFile, $exclusiveLength, array $exclude = []) {
        $handle = opendir($folder);
        while (false !== $f = readdir($handle)) {
            if ($f != '.' && $f != '..') {
                $filePath = "$folder/$f";
// Remove prefix from file path before add to zip. 
                $localPath = substr($filePath, $exclusiveLength);
                if (!in_array($localPath, $exclude)) {
                    if (is_file($filePath)) {
                        $zipFile->addFile($filePath, $localPath);
                    } elseif (is_dir($filePath)) {
// Add sub-directory. 
                        $zipFile->addEmptyDir($localPath);
                        self::folderToZip($filePath, $zipFile, $exclusiveLength);
                    }
                }
            }
        }
        closedir($handle);
    }

    /**
     * Zip a folder (include itself). 
     * Usage: 
     *   zipper::zipDir('/path/to/sourceDir', '/path/to/out.zip'); 
     * 
     * @param string $sourcePath Path of directory to be zip. 
     * @param string $outZipPath Path of output zip file. 
     * @param bool $includeSelf include self fold1er.
     * @param array $exclude list of relative paths to sourcePath to exclude
     */
    public static function zipDir($sourcePath, $outZipPath, $includeSelf = true, $exclude = []) {
        $pathInfo = pathinfo(realpath($sourcePath));
        $parentPath = $pathInfo['dirname'];
        $dirName = $pathInfo['basename'];

        $z = new ZipArchive();
        $z->open($outZipPath, ZIPARCHIVE::CREATE);
        if ($includeSelf) {
            $z->addEmptyDir($dirName);
        }

        self::folderToZip(rtrim($sourcePath, '/\\ '), $z, strlen($includeSelf ? "$parentPath/" : "{$parentPath}/{$dirName}/"), $exclude);
        $z->close();
    }

}
