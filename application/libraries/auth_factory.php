<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require 'auth/AuthProvider.php';

/**
 * Description of auth_factory
 *
 * @author JosephT
 */
class auth_factory {
    //put your code here

    /**
     *
     * @var CI_DB_active_record
     */
    protected $db;

    /**
     *
     * @var user_auth
     */
    protected $user_auth;

    /**
     *
     * @var subdomain
     */
    protected $subdomain;
    private $availableProviders = array();

    public function __construct() {
        $ci = get_instance();
        $this->db = $ci->load->database('', true);
        $ci->load->library('user_auth');
        $ci->load->library('subdomain');
        $this->user_auth = $ci->user_auth;
        $this->subdomain = $ci->subdomain;
        $this->_loadAvailable();
    }

    private function _loadAvailable() {
        $dir = __DIR__ . '/auth/providers';
        $reader = new DirectoryIterator($dir);

        foreach ($reader as $providerDir) {
            if ($providerDir->isDir() && !$providerDir->isDot()) {
                $providerClassFile = $providerDir->getPathname() . '/' . $providerDir->getBasename() . '.php';
                if (file_exists($providerClassFile)) {
                    include_once realpath($providerClassFile);
                    $providerClass = $providerDir->getBasename();
                    if (class_exists($providerClass)) {

                        $id = call_user_func(array($providerClass, 'getId'));
                        $active = call_user_func(array($providerClass, 'active'));

                        if ($active) {
                            $providerData = array(
                                'name' => call_user_func(array($providerClass, 'getName')),
                                'class' => $providerClass,
                                'dir' => $providerDir->getPathname(),
                            );

                            $this->availableProviders[$id] = $providerData;
                        }
                    } else {
                        log_message('error', "{$providerDir->getBasename()} class does not exist.");
                    }
                } else {
                    log_message('error', "{$providerDir->getBasename()} is not a valid auth proider");
                }
            }
        }
    }

    /**
     * 
     * @param int|null $companyId the id of the company, if null, then ID of the current user,
     * using the user's address.
     * is used, if that is not available an exception is thrown
     * @param bool $enabledOnly specifies if only enabled adapters hould be used
     * @return AuthProvider[] an array of auth providers
     * 
     */
    public function getProviders($companyId = null, $enabledOnly = false) {
        $providers = array();
        foreach (array_keys($this->availableProviders) as $id) {
            if (($provider = $this->getProvider($id, $companyId, $enabledOnly))) {
                $providers[$id] = $provider;
            }
        }

        return $providers;
    }

    private function getCompanyId($companyId = null) {
        if (!$companyId && !($companyId = $this->subdomain->get_company_id())) {
            throw new Exception('Company ID must specified for loading auth providers');
        }
        return $companyId;
    }

    /**
     * 
     * @staticvar AuthProvider[] $providerStore
     * @param string $id of the provider
     * @param int|null $companyId
     * @param boolean $enabledOnly
     * @return null|AuthProvider
     */
    public function getProvider($id, $companyId = null, $enabledOnly = false) {
        /* @var $providerStore AuthProvider */
        static $providerStore = array();
        $companyId = $this->getCompanyId($companyId);
        $key = $companyId . '-' . $id;

        if ($this->isAvailable($id)) {
            if (!isset($providerStore[$key])) {
                $providerConfig = $this->availableProviders[$id];
                $providerStore[$key] = new $providerConfig['class']($companyId);
            }

            return !$enabledOnly || $providerStore[$key]->isEnabled() ? $providerStore[$key] : null;
        }

        return null;
    }

    public function getName($id) {
        return array_key_exists($id, $this->availableProviders) ? $this->availableProviders[$id]['name'] : null;
    }

    public function isAvailable($id) {
        return array_key_exists($id, $this->availableProviders);
    }

    public function getConfigViewPath($id) {
        if ($this->isAvailable($id)) {

            $viewPathCb = array($this->availableProviders[$id]['class'], 'getConfigViewPath');
            if (is_callable($viewPathCb)) {
                return call_user_func($viewPathCb);
            } else {
                return $this->availableProviders[$id]['dir'] . '/' . 'config_view.php';
            }
        }

        return null;
    }

    /**
     * Get company's default provider.
     * @param int $companyId
     * @return null|AuthProvider
     */
    public function getDefaultProvider($companyId = null) {
        $companyId = $this->getCompanyId($companyId);
        $providerRes = $this->db->get_where(AuthProvider::AUTH_CONFIG_TABLE, array(
                    'id_company' => $companyId,
                    'is_enabled' => 1,
                    'is_default' => 1
                        ), 1)->result();

        if (!empty($providerRes)) {
            $providerCfg = current($providerRes);
            if ($this->isAvailable($providerCfg->adapter)) {
                $class = $this->availableProviders[$providerCfg->adapter]['class'];
                return new $class($companyId, true, json_decode($providerCfg->config_data, true), true, false);
            }
        }

        return null;
    }

    public function signinUrl($id){
        if($this->isAvailable($id)){
            return site_url("auth/{$id}/login");
        }
        return null;
    }
}
