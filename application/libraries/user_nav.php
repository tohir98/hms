<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once 'page_nav.php';

/**
 * Talentbase
 * Users navigation library
 * 
 * @category   Library
 * @package    Users
 * @subpackage Navigation
 * @author     Chuks O. <chuks.olloh@hired.com.ng>
 * @copyright  Copyright Â© 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */
class User_nav extends Page_nav {

    /**
     * User id_user
     * 
     * @access private
     * @var string
     */
    private $id_user;

    /**
     * User account_type
     * 
     * @access private
     * @var string
     */
    private $account_type;

    /**
     * User Company ID
     * 
     * @access private
     * @var string
     */
    private $id_company;

    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();

        // Load libraries
        $this->CI->load->library('user_auth');
        // Load model
        $this->CI->load->model('user/user_model');
        // Load helper
        $this->CI->load->helper('user_nav');
        $this->id_user = $this->CI->user_auth->get("id_user");
        $this->account_type = $this->CI->user_auth->get("account_type");
        $this->id_company = $this->CI->user_auth->get("id_company");
    }

    /**
     * get_assoc method
     *
     * @access public
     * @return mixed (bool | array)
     * */
    public function get_assoc() {
        if ((!$this->account_type) or ( !$this->id_user)) {
            return false;
        }

        // Fetch user permissions
        $result = $this->CI->user_model->fetch_user_groups_modules_perms($this->id_user, DEFAULT_COMPANY);
        
        if (!$result) {
            return false;
        }

        $a = array();

        foreach ($result as $row) {

            if ($row['group_subject'] != '') {
                $a[$row['group_subject']][$row['module_id_string']]['subject'] = $row['module_subject'];
                $a[$row['group_subject']][$row['module_id_string']]['items'][] = $row;
                $a[$row['group_subject']][$row['module_id_string']]['id_string'] = $row['module_id_string'];
            }
        }
        
        return $a;
    }

// End func run_page
    public function get_user_link() {
        $user_type = $this->CI->user_auth->get('account_type');

        if ($user_type == USER_TYPE_ADMIN) {
            $user_link = 'admin';
        } elseif ($user_type == USER_TYPE_EMPLOYEE) {
            $user_link = 'employee';
        } else {
            log_message('error', 'Unknown user type');
            $user_link = '';
        }

        return $user_link;
    }

    public function get_top_menu() {
        return array(
            'dashboard_url' => '/' . $this->get_user_link() . '/dashboard',
            'logout_url' => '/logout',
            'display_name' => 'HMS',
//            'user_companies' => $this->CI->user_model->get_all_companies($this->CI->user_auth->get('email'), $this->CI->user_auth->get('id_company'))
        );
    }

    public function company_name() {
        return 'HMS';
    }
    
    public function run_setup_page($buffer, $title = 'TalentBase', $activity_bar = true) {

        $header_data = array(
            'page_title' => $title
        );

        $user_type = $this->CI->user_auth->get('account_type');

        if ($user_type == USER_TYPE_ADMIN) {
            $user_link = 'admin';
        } elseif ($user_type == USER_TYPE_EMPLOYEE) {
            $user_link = 'employee';
        } else {
            trigger_error('Unknown user type!', E_USER_WARNING);
        }

        $top_menu = array(
            'dashboard_url' => '/' . $user_link . '/dashboard',
            'logout_url' => '/logout',
            'display_name' => $this->CI->user_auth->get('display_name')
        );

        $nav_menu = $this->get_assoc();

        $top_menu_buffer = build_top_nav_part($nav_menu, $user_link);
        $left_sidebar_content = build_sidebar_nav_part($nav_menu);
        //$dashboard_buttons = build_dashboard_buttons_nav_part($nav_menu, $user_link);
        //$top_menu['nav_menu'] = $top_menu_buffer;
        $top_menu['nav_menu'] = '';

        $left_sidebar = array(
            'company_name' => $this->CI->user_auth->get('company_name'),
            'nav_menu' => $left_sidebar_content
        );

        $header_buffer = $this->CI->load->view('templates/header', $header_data, true);
        $top_nav = $this->CI->load->view('templates/top_menu', $top_menu, true);
        $top_nav = $this->CI->load->view('templates/top_menu', $top_menu, true);
        $left_sidebar_buffer = $this->CI->load->view('templates/left_sidebar', $left_sidebar, true);
        $footer_buffer = $this->CI->load->view('templates/footer', '', true);

        $page_parts = array(
            'header' => $header_buffer,
            'top_nav' => $top_nav,
            'left_sidebar' => $left_sidebar_buffer,
            'page_content' => $buffer,
            'footer' => $footer_buffer,
            'activity_bar' => $activity_bar
        );

        $this->CI->load->view('templates/page', $page_parts);
    }

// End func run_setup_page
}
