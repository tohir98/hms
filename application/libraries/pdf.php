<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class pdf {
	function pdf()
    {
        $CI = & get_instance();
        log_message('Debug', 'mPDF class is loaded.');
    }
 
    function load($param = NULL)
    {
         
        if ($param == NULL)
        {
            //$param = "'utf-8', 'A4-L', 0, '', 15, 15, 16, 16, 9, 9";    
			//$param = "'utf-8', 'A4-L'";
        }
         
        return new mPDF('c', 'A4-L');
    }
}
