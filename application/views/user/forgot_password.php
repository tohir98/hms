<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Talentbase
 * Forgot password view
 * 
 * @category   Views
 * @package    User
 * @subpackage Forgot password
 * @author     David A. <tokernel@gmail.com>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

if($message != '') {
	$message_class = ' error';
} else {
	$message_class = '';
}

?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<!-- Apple devices fullscreen -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<!-- Apple devices fullscreen -->
	<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />
	
	<title>TalentBase - Forgot password</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?php echo site_url(); ?>css/bootstrap.min.css">
	<!-- Bootstrap responsive -->
	<link rel="stylesheet" href="<?php echo site_url(); ?>css/bootstrap-responsive.min.css">
	<!-- icheck -->
	<link rel="stylesheet" href="<?php echo site_url(); ?>css/plugins/icheck/all.css">
	<!-- Theme CSS -->
	<link rel="stylesheet" href="<?php echo site_url(); ?>css/style.css">
	<!-- Color CSS -->
	<link rel="stylesheet" href="<?php echo site_url(); ?>css/themes.css">


	<!-- jQuery -->
	<script src="<?php echo site_url(); ?>js/jquery.min.js"></script>
	
	<!-- Nice Scroll -->
	<script src="<?php echo site_url(); ?>js/plugins/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- Validation -->
	<script src="<?php echo site_url(); ?>js/plugins/validation/jquery.validate.min.js"></script>
	<script src="<?php echo site_url(); ?>js/plugins/validation/additional-methods.min.js"></script>
	<!-- icheck -->
	<script src="<?php echo site_url(); ?>js/plugins/icheck/jquery.icheck.min.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo site_url(); ?>js/bootstrap.min.js"></script>
	<script src="<?php echo site_url(); ?>js/eakroko.js"></script>

	<!--[if lte IE 9]>
		<script src="<?php echo site_url(); ?>js/plugins/placeholder/jquery.placeholder.min.js"></script>
		<script>
			$(document).ready(function() {
				$('input, textarea').placeholder();
			});
		</script>
	<![endif]-->
	

	<!-- Favicon -->
	<link rel="shortcut icon" href="<?php echo site_url(); ?>img/favicon.ico" />
	<!-- Apple devices Homescreen icon -->
	<link rel="apple-touch-icon-precomposed" href="<?php echo site_url(); ?>img/apple-touch-icon-precomposed.png" />

</head>

<body class='login'>
	<div class="wrapper">
		<h1><a href="<?php echo site_url(); ?>"><img src="<?php echo site_url(); ?>img/tb_logo_default.png" alt="" class='retina-ready' width="59" height="49">TalentBase</a></h1>
		<div class="login-body">
			<h2>FORGOT PASSWORD</h2>
			<form action="<?php echo site_url("forgot_password"); ?>" method='POST' class='form-validate' id="test">
				<?php if($success_message != '') { ?>
				<div class="control-group alert alert-success">
					<?php echo $success_message; ?>
				</div>
				<?php } ?>
				<div class="control-group <?php echo $message_class; ?>">
					<div class="email controls">
						<input type="text" value="<?php echo $email; ?>" name='uemail' placeholder="Email address" class='input-block-level' data-rule-required="true" data-rule-email="true">
						<?php if($message != '') { ?>
							<span for="uemail" class="help-block error"><?php echo $message; ?></span>
						<?php } ?>
					</div>
				</div>
				<div class="submit">
					<input type="submit" value="Get reset password email" class='btn btn-primary'>
				</div>
			</form>
			<div class="forget">
				<a href="<?php echo site_url("login"); ?>"><span>Return to login page</span></a>
			</div>
		</div>
	</div>
</body>
</html>
