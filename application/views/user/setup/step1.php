<!-- Display error/success message -->
<br/>
<?php if ($this->session->userdata('errors') != '') { ?>
<div class="alert alert-danger">
     <button data-dismiss="alert" class="close" type="button">x</button>
     <?php echo $this->session->userdata('errors'); ?>
</div>
<?php $this->session->unset_userdata('errors'); } 
if ($this->session->userdata('confirm') != '') { ?>
<div class="alert alert-success">
     <button data-dismiss="alert" class="close" type="button">x</button>
     <?php echo $this->session->userdata('confirm'); ?>
</div>
<?php $this->session->unset_userdata('confirm'); }  ?>

<div class="page-header">
	<div class="pull-left">
        <h2>Company Setup : Step 1 of 3</h2>
	</div>
</div>


<div>
    <form class="form-horizontal form-wizard ui-formwizard" id="ss" novalidate="novalidate">
        <div class="step ui-formwizard-content" id="firstStep" style="display: block;">
            <ul class="wizard-steps steps-4">
                <li class="active">
                    <div class="single-step">
                        <span class="title">
                            1</span>
                        <span class="circle">
                            <span class="active"></span>
                        </span>
                        <span class="description">
                            Create Company Locations
                        </span>
                    </div>
                </li>
                <li class="">
                    <div class="single-step">
                        <span class="title">
                            2</span>
                        <span class="circle"></span>
                        <span class="description">
                            Create Departments
                        </span>
                    </div>
                </li>
                <li class="">
                    <div class="single-step">
                        <span class="title">
                            3</span>
                        <span class="circle"></span>
                        <span class="description">
                            Create Roles
                        </span>
                    </div>
                </li>
                <li class="">
                    <div class="single-step">
                        <span class="title">
                            4</span>
                        <span class="circle"></span>
                        <span class="description">
                            Create Company Structure/Levels
                        </span>
                    </div>
                </li>
            </ul>
        </div>
    </form>
</div>

<div class="row-fluid">
    
    <div class="box box-bordered">
        <form method="post" action="">
            <div class="box-title" style="padding-right:5px;">
                <h3>
                    Company Locations
                </h3>
                <a href="<?php echo site_url('employee/add_company_location'); ?>" class="btn btn-warning pull-right add_new">Add New Company Location</a>
            </div>
            
            <div class="box-content-padless" style="max-height: 300px; overflow-y: scroll">

                <table class='table table-striped'>
                    <?php
                    if (!empty($locations)) {
                        foreach ($locations as $l) {
                            ?>
                            <tr>
                                <td><?php echo ucfirst($l->location_tag); ?></td>
                                <td><?php echo $l->address; ?></td>
                                <td><?php echo $l->first_name .' '. $l->last_name; ?></td>
                                <td>&nbsp;</td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </table>

            </div>
            <div class="control-group row" style=" margin-top: 30px"> 
                <div class="controls controls-row pull-right">
                    <?php update_module_setup_step(10, 1, MODULE_SETUP_PENDING); ?>
                    <a href="<?= site_url('setup/run_setup/step2')?>" class="btn btn-primary" value="next">&nbsp;Next &nbsp;</a>
                </div>
            </div>
        </form>
    </div>
   
</div>





<div id="exit-modal-popup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-body">
    <p>Are you sure you want to exit the setup process?</p>
  </div>
  <div class="modal-footer">
    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
    <a href="#" class="btn btn-danger closeme">Yes, Exit</a>
  </div>
</div>

<div id="welcome-modal-popup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-body">
    <p><h3>Welcome to TalentBase Payroll.<br/> Get setup in 2 quick steps.<h3></p>
  </div>
  <div class="modal-footer">
      <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"> &nbsp;Start&nbsp;</button>
  </div>
</div>


<?php include APPPATH . 'views/locations/_location_script.php'; ?>