<?php include '_setup_scripts.php'; ?>

<div class="page-header">
	<div class="pull-left">
        <h2>Company Setup : Step 2 of 4</h2>
	</div>
</div>

<div>
    <form class="form-horizontal form-wizard ui-formwizard" id="ss" novalidate="novalidate">
        <div class="step ui-formwizard-content" id="firstStep" style="display: block;">
            <ul class="wizard-steps steps-4">
                <li class="">
                    <div class="single-step">
                        <span class="title">
                            1</span>
                        <span class="circle">
                            <span class="active"></span>
                        </span>
                        <span class="description">
                            Create Company Locations
                        </span>
                    </div>
                </li>
                <li class="active">
                    <div class="single-step">
                        <span class="title">
                            2</span>
                        <span class="circle"></span>
                        <span class="description">
                            Create Departments
                        </span>
                    </div>
                </li>
                <li class="">
                    <div class="single-step">
                        <span class="title">
                            3</span>
                        <span class="circle"></span>
                        <span class="description">
                            Create Roles
                        </span>
                    </div>
                </li>
                <li class="">
                    <div class="single-step">
                        <span class="title">
                            4</span>
                        <span class="circle"></span>
                        <span class="description">
                            Create Company Structure/Levels
                        </span>
                    </div>
                </li>
            </ul>
        </div>
    </form>
</div>

<div class="row-fluid">
    
    <div class="box box-bordered">
        <form method="post" action="">
            <div class="box-title" style="padding-right:5px;">
                <h3>
                    Departments
                </h3>
                <a href="<?php echo site_url('employee/add_departments'); ?>" class="btn btn-warning pull-right add_new">Add New Department</a>
            </div>
            
            <div class="box-content-padless" style="max-height: 300px; overflow-y: scroll">

                <table class='table table-striped'>
                    <thead>
                        <tr>
                            <th>Department</th>
                            <th>Department Parent</th>
                            <th>Department Head</th>
                        </tr>
                    </thead>
                    <?php
                    if (!empty($depts)) {
                        foreach ($depts as $d) {
                            ?>
                            <tr>
                                <td><?php echo ucfirst($d->departments); ?></td>
                                <td><?php echo $d->parent_department; ?></td>
                                <td><?php echo $d->first_name .' '. $d->last_name; ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </table>

            </div>
            <div class="control-group row" style=" margin-top: 30px"> 
                <div class="controls controls-row pull-right">
                    <a href="<?php echo site_url('setup/restart_setup'); ?>" class="btn btn-danger modal_restart" >&nbsp;Restart&nbsp;</a>
                    <a href="<?= site_url('setup/run_setup/step3')?>" class="btn btn-primary" value="next">&nbsp;Next &nbsp;</a>
                </div>
            </div>
        </form>
    </div>
   
</div>





<div id="exit-modal-popup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-body">
    <p>Are you sure you want to exit the setup process?</p>
  </div>
  <div class="modal-footer">
    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
    <a href="#" class="btn btn-danger closeme">Yes, Exit</a>
  </div>
</div>

<div id="restart-modal-popup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-body">
        <p>Are you sure you want to restart the setup process?</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
        <a href="#" class="btn btn-danger closeme">Yes, Restart</a>
    </div>
</div>

<?php include APPPATH . 'views/departments/_department_script.php'; ?>
<script>
    $('.modal_restart').click(function (eve) {
        eve.preventDefault();
        $('#restart-modal-popup').modal('show').fadeIn();

        var page = $(this).attr("href");
        $('a.closeme').attr('href', page);

    });
</script>

