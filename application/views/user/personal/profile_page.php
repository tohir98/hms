<div class="box">
    <div class="page-header">
        <div class="pull-left">
            <h1>My Profile</h1>
        </div>
    </div>
    <div class="box-content-padless">
        <div class="row-fluid">
            <p>
            <ul class="nav nav-tabs" id="myTab">
                <li class="active">
                    <a href="">Profile</a>
                </li>
                <li class="">
                    <a href="<?= site_url('personal/my_profile_info') ?>" >My Request</a>
                </li>
            </ul>
            </p>
            <div class="span12">
                <h4><?php echo $profile->first_name . ' ' . $profile->last_name; ?>, <?php echo $profile->roles; ?>, <?php echo $profile->departments; ?></h4>
            </div>
        </div>
        <div class="row-fluid">

            <div class="span2">
                <div class="box">
                    <?php //if($this->user_auth->have_perm(VIEW_MY_PROFILE_PICTURE)) { ?>
                    <div class="box-title" style="border:1px solid #CCCCCC;height:210px;padding:0;">
                        <div class="row-fluid" style="height:180px;overflow:hidden">
                            <?php if ($profile_picture_src != '') { ?>
                                <img src="<?php echo $profile_picture_src; ?>" />
                            <?php } else { ?>
                                <img src="/img/profile-default.jpg" />
                            <?php } ?>
                        </div>
                        <?php if ($this->user_auth->have_perm(EDIT_MY_PROFILE_PICTURE)) { ?>
                            <div class="row-fluid">
                                <div class="btn-group span12">
                                    <a class="btn btn-primary span12" href="<?php echo site_url('personal/edit_my_profile_picture'); ?>"> Edit</a>
                                </div>
                            </div>
                        <?php } ?>

                    </div>
                    <?php //} ?>
                    <div class="_box-content">

                        <div class="span12">
                            <p></p>
                            <ul class="nav nav-tabs nav-stacked left_menu">
                                <?php if ($this->user_auth->have_perm(VIEW_MY_PROFILE_WORK_INFO) or $this->user_auth->have_perm(VIEW_MY_PROFILE_PERSONAL_INFO)) { ?>
                                    <li class="active"><a href="#work_info_personal_info_container" class="left_menu_button" data-container="work_info_personal_info_container">Profile</a></li>
                                <?php } ?>
                                <?php if ($docs_menu == true) { ?>
                                    <li><a href="#credentials_container" class="left_menu_button" data-container="credentials_container">Document Uploads</a></li>
                                <?php } ?>
                                <?php if ($this->user_auth->have_perm(VIEW_MY_EMERGENCY_CONTACTS) or $this->user_auth->have_perm(EDIT_MY_EMERGENCY_CONTACTS)) { ?>
                                    <li><a href="#emergency_container" class="left_menu_button" data-container="emergency_container">Emergency contacts</a></li>
                                <?php } ?>
                            </ul>
                            <p></p>
                        </div>

                        <div class="span12">
                            <?php if ($this->user_auth->have_perm(EDIT_MY_BENEFITS_DETAILS) or $this->user_auth->have_perm(EDIT_MY_BANK_DETAILS)) { ?>
                                <h6>Others</h6>
                            <?php } ?>
                            <p></p>
                            <ul class="nav nav-tabs nav-stacked">
                                <?php if ($this->user_auth->have_perm(EDIT_MY_BANK_DETAILS)) { ?>
                                    <li><a href="#bank_details_container" class="left_menu_button" data-container="bank_details_container">Bank details</a></li>
                                <?php } ?>
                                <?php if ($this->user_auth->have_perm(EDIT_MY_BENEFITS_DETAILS)) { ?>
                                    <li><a href="#benefits_details_container" class="left_menu_button" data-container="benefits_details_container">Benefits details</a></li>
                                <?php } ?>
                            </ul>
                            <p></p>
                        </div>

                    </div>
                </div>

            </div>
            <div class="span10">
                <?php
                echo $profile_content;
                echo $emergency_content;
                echo $bank_details_content;
                echo $benefits_details_content;
                echo $resume_content;
                ?>
            </div>
        </div>

    </div>
</div>

<div class="modal hide fade modal-full" id="popup_modal_full" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" >

</div>
<script type="text/javascript">

    $(document).ready(function () {

        $('.left_menu_button').click(function () {
            $('.left_menu_button').parent().removeClass('active');
            $(this).parent().addClass('active');

            $('.employee_info_box').hide();

            var container_id = $(this).attr('href');
            $(container_id).show();
            window.location.href = container_id;
            return false;
        });

        if (window.location.hash !== '' && window.location.hash !== '#') {
            var k = window.location.hash.substring();
            $('.left_menu_button[href="' + k + '"]').click();
        }

        $('.popup_full').click(function (eve) {
            eve.preventDefault();
            $('#popup_modal_full').modal('show').fadeIn();
            $('#popup_modal_full').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

            var page = $(this).attr("href");
            $.get(page, function (html) {

                $('#popup_modal_full').html('');
                $('#popup_modal_full').html(html).show();
                icheck();

            });
        });

    });

</script>