<style>
	.control-group {
		padding:15px !important;
	}
	h4, h5 {
		padding-left:15px;
	}
</style>
<div class="box">
	<div class="page-header">
		<div class="pull-left">
			<h1>Edit Benefits details</h1>
		</div>
    </div>
	<div class="breadcrumbs">
		<ul>
			<li><a href="<?php echo site_url('personal'); ?>">My Profile</a><i class="icon-angle-right"></i></li>
			<li><a href="#">Edit Benefits details</a></li>
		</ul>
		<div class="close-bread"></div>
	</div>
	<?php if($success_message != '') { ?>
	<div class="alert alert-success" style="margin-top:20px;">
			<button data-dismiss="alert" class="close" type="button">×</button>
			<strong>Success!</strong> <?php echo $success_message; ?>
	</div>
	<?php } ?>
	<?php 
	if(!empty($error_messages)) { ?>
		<div class="alert alert-danger" style="margin-top:20px;">
			<button data-dismiss="alert" class="close" type="button">×</button>
		<?php
		foreach($error_messages as $err_message) {
	?>
		<strong><?php echo $err_message; ?></strong><br />
	<?php } ?>
		</div>
	<?php
	}
	?>
	<div class="box-content">
		<form action="" method="POST" class="form-horizontal form-wizard form-wysiwyg form-striped" id="edit_profile" enctype="multipart/form-data" style="border:1px solid #DDDDDD">
			<div class="control-group">
				<label class="control-label" for="rsa_pin">RSA PIN:</label>
				<div class="controls">
					<input type="text" data-rule-required="true" class="input-xlarge" id="rsa_pin" name="rsa_pin" value="<?php echo $profile->rsa_pin; ?>" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="pfs">Pension Fund Administrator:</label>
				<div class="controls">
					<select name="pfs" id="pfs" data-rule-required="true" style="width:300px;">
					<?php foreach($pfas as $pfa) { 
						if($pfa->id_pension_fund_administrator == $profile->id_pfs) {
							$sel = ' selected="selected" ';
						} else {
							$sel = '';
						}	
						?>
						<option value="<?php echo $pfa->id_pension_fund_administrator; ?>" <?php echo $sel; ?>><?php echo $pfa->name; ?></option>
					<?php } ?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label for="managers" class="control-label"></label>
				<div class="controls">
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</div>
		</form>
	</div>	
</div>
			
<script type="text/javascript">

$(document).ready(function() {
	
	$('#edit_profile').validate({
		rules: {
			rsa_pin: {
				required: true
			},
			pfs : {
				required:true
			}	
		},
		messages: {
			rsa_pin: {
				required: "Enter RSA PIN"
			},
			pfs : {
				required: "Select Pension Fund Administrator"
			}
		}
	});
	
});

</script>