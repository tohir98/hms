<style>
	.control-group {
		padding:15px !important;
	}
	h4, h5 {
		padding-left:15px;
	}
</style>
<div class="box">
	<div class="page-header">
		<div class="pull-left">
			<h1>Edit Profile picture</h1>
		</div>
    </div>
	<div class="breadcrumbs">
		<ul>
			<li><a href="<?php echo site_url('personal'); ?>">My Profile</a><i class="icon-angle-right"></i></li>
			<li><a href="#">Edit Profile picture</a></li>
		</ul>
		<div class="close-bread"></div>
	</div>
	<?php if($success_message != '') { ?>
	<div class="alert alert-success" style="margin-top:20px;">
			<button data-dismiss="alert" class="close" type="button">×</button>
			<strong>Success!</strong> <?php echo $success_message; ?>
	</div>
	<?php } ?>
	<?php 
	if(!empty($error_messages)) { ?>
		<div class="alert alert-danger" style="margin-top:20px;">
			<button data-dismiss="alert" class="close" type="button">×</button>
		<?php
		foreach($error_messages as $err_message) {
	?>
		<strong><?php echo $err_message; ?></strong><br />
	<?php } ?>
		</div>
	<?php
	}
	
	if($profile_picture_src != '') {
		$existing_pic = $profile_picture_src;
	} else {
		$existing_pic = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image';
	}	
	
	?>
	<div class="box-content">
		<form action="" method="POST" class="form-horizontal form-wizard form-wysiwyg form-striped" id="edit_profile" enctype="multipart/form-data" style="border:1px solid #DDDDDD">
			<div class="control-group">
				<label class="control-label" for="first_name">Profile picture:</label>
				<div class="controls">
					<div class="fileupload fileupload-new" data-provides="fileupload">
						<div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
							<img src="<?php echo $existing_pic; ?>" />
						</div>
						<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
						<div>
							<span class="btn btn-file">
								<span class="fileupload-new">Select file</span>
								<span class="fileupload-exists">Change</span>
								<input type="file" name="profile_picture" data-rule-required="true" />
							</span>
							<!-- <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a> -->
						</div>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label for="managers" class="control-label"></label>
				<div class="controls">
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</div>
		</form>
	</div>	
</div>
			
<script type="text/javascript">

$(document).ready(function() {
	
	$('#edit_profile').validate({
		rules: {
			profile_picture: {
				required: true
			}
		},
		messages: {
			profile_picture: {
				required: "Include picture"
			}
		}
	});
	
});

</script>