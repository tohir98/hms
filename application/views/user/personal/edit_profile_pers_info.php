<style>
	.control-group {
		padding:15px !important;
	}
	h4, h5 {
		padding-left:15px;
	}
</style>
<div class="box">
	<div class="page-header">
		<div class="pull-left">
			<h1>Edit Profile / Personal info</h1>
		</div>
    </div>
	<div class="breadcrumbs">
		<ul>
			<li><a href="<?php echo site_url('personal'); ?>">My Profile</a><i class="icon-angle-right"></i></li>
			<li><a href="#">Edit Profile / Personal info</a></li>
		</ul>
		<div class="close-bread"></div>
	</div>
	<?php if($success_message != '') { ?>
	<div class="alert alert-success" style="margin-top:20px;">
			<button data-dismiss="alert" class="close" type="button">×</button>
			<strong>Success!</strong> <?php echo $success_message; ?>
	</div>
	<?php } ?>
	<?php 
	if(!empty($error_messages)) { ?>
		<div class="alert alert-danger" style="margin-top:20px;">
			<button data-dismiss="alert" class="close" type="button">×</button>
		<?php
		foreach($error_messages as $err_message) {
	?>
		<strong><?php echo $err_message; ?></strong><br />
	<?php } ?>
		</div>
	<?php
	}
	?>
	<div class="box-content">
		<form action="" method="POST" class="form-horizontal form-wizard form-wysiwyg form-striped" id="edit_profile" enctype="multipart/form-data" style="border:1px solid #DDDDDD">
			<h4>Personal info</h4>
			<div class="control-group">
				<label for="dob" class="control-label">First name:</label>
				<div class="controls">
					<input type="text" id="first_name" name="first_name" class="" data-rule-required="true" value="<?php echo $profile['first_name']; ?>" />
				</div>
			</div>
			<div class="control-group">
				<label for="dob" class="control-label">Last name:</label>
				<div class="controls">
					<input type="text" id="last_name" name="last_name" class="" data-rule-required="true" value="<?php echo $profile['last_name']; ?>" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="first_name">Gender:</label>
				<div class="controls">
					<select name="id_gender" id="id_gender">
						<?php 
						if($profile->id_gender == '1') { 
							$sel_male = ' selected="selected" ';
						} else {
							$sel_male = '';
						}
						if($profile->id_gender == '2') { 
							$sel_female = ' selected="selected" ';
						} else {
							$sel_female = '';
						}
						?>
						<option value="1" <?php echo $sel_male; ?>>Male</option>
						<option value="2" <?php echo $sel_female; ?>>Female</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label for="id_marital_status" class="control-label">Marital Status:</label>
				<div class="controls">
					<select name="id_marital_status" id="id_marital_status">
						<?php 
						if($profile['id_marital_status'] == '1') {
							$sel_simgle = ' selected="selected" ';
						} else {
							$sel_simgle = '';
						}
						if($profile['id_marital_status'] == '2') {
							$sel_married = ' selected="selected" ';
						} else {
							$sel_married = '';
						}
						?>
						<option value="1" <?php echo $sel_simgle; ?>>Single</option>
						<option value="2" <?php echo $sel_married; ?>>Married</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label for="dob" class="control-label">Birth Date:</label>
				<div class="controls">
					<input type="text" id="dob" name="dob" class="datepick" data-rule-required="true" value="<?php echo $profile['dob']; ?>" />
				</div>
			</div>
			<div class="control-group">
				<label for="id_nationality" class="control-label">Country of Origin:</label>
				<div class="controls">
					<select name="id_nationality" id="id_nationality">
						<?php if(!empty($countries)) { 
							foreach($countries as $c) { 
								if($profile['id_nationality'] == $c->id_country) {
									$sel = ' selected="selected" ';
								} else {
									$sel = '';
								}	
								?>
								<option value="<?php echo $c->id_country; ?>" <?php echo $sel; ?>><?php echo $c->country; ?></option>
							<?php }
						} ?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label for="address" class="control-label">Address:</label>
				<div class="controls">
					<input type="text" id="address" name="address" class="" data-rule-required="true" value="<?php echo $profile['address']; ?>" />
				</div>
			</div>
			<div class="control-group">
				<label for="city" class="control-label">City:</label>
				<div class="controls">
					<input type="text" id="city" name="city" class="" data-rule-required="true" value="<?php echo $profile['city']; ?>" />
				</div>
			</div>
			<div class="control-group">
				<label for="country" class="control-label">Country:</label>
				<div class="controls">
					<select name="country" id="country">
						<?php if(!empty($countries)) { 
							foreach($countries as $c) { 
								if($profile['id_country'] == $c->id_country) {
									$sel = ' selected="selected" ';
								} else {
									$sel = '';
								}	
								?>
								<option value="<?php echo $c->id_country; ?>" <?php echo $sel; ?>><?php echo $c->country; ?></option>
							<?php }
						} ?>	
					</select>
				</div>
			</div>
			<div class="control-group">
				<label for="state" class="control-label">State:</label>
				<div class="controls" id="states_container">
					<select name="id_state" id="id_state">
						<option>[State]</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label for="managers" class="control-label"></label>
				<div class="controls">
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</div>
		</form>
	</div>	
</div>
			
<script type="text/javascript">

$(document).ready(function() {
	
	select_country(<?php echo $profile['id_country']; ?>);
	
	$('#country').change(function() {
		var id_country = $(this).val();
		select_country(id_country);
	});

	function select_country(id_country) {
		
		var countries_url = '<?php echo site_url('recruiting/recruiting/states_select/'); ?>'; 
		var ajax_url = countries_url + '/' + id_country + '/single/id_state';
		
		$.ajax({
		  type: 'POST',
		  url: ajax_url,
		  cache: false,
		  dataType: "html",
		  
		  beforeSend: function(html) {
			$('#states_container').html('Loading states...'); 
		  },
		  		  
	      success: function(html){
			$('#states_container').html(html); 
		  },	
		  error: function(html) {
			$('#states_container').html('Error: cannot load data.'); 
		  }
		 
		});
		
	}
	
	$('#edit_profile').validate({
		rules: {
			first_name: {
				required: true
			},
			last_name: {
				required: true
			},
			dob: {
				required: true
			},
			address : {
				required:true
			},
			city : {
				required:true
			}
		},
		messages: {
			first_name: {
				required: "Enter First name"
			},
			last_name: {
				required: "Enter Last name"
			},
			dob: {
				required: "Enter Birth Date"
			},
			address : {
				required: "Enter address"
			},
			city : {
				required: "Enter city"
			}
		}
	});
	
});

</script>