<style>
	.control-group {
		padding:15px !important;
	}
	h4, h5 {
		padding-left:15px;
	}
</style>
<div class="box">
	<div class="page-header">
		<div class="pull-left">
			<h1>Upload file</h1>
		</div>
    </div>
	<div class="breadcrumbs">
		<ul>
			<li><a href="#">Upload file</a></li>
		</ul>
		<div class="close-bread"></div>
	</div>
	<?php if($success_message != '') { ?>
	<div class="alert alert-success" style="margin-top:20px;">
			<button data-dismiss="alert" class="close" type="button">×</button>
			<strong>Success!</strong> <?php echo $success_message; ?>
	</div>
	<?php } ?>
	<?php 
	if(!empty($error_messages)) { ?>
		<div class="alert alert-danger" style="margin-top:20px;">
			<button data-dismiss="alert" class="close" type="button">×</button>
		<?php
		foreach($error_messages as $err_message) {
	?>
		<strong><?php echo $err_message; ?></strong><br />
	<?php } ?>
		</div>
	<?php
	}
	?>
	<div class="box-content">
		<form action="" method="POST" class="form-horizontal form-wizard form-wysiwyg form-striped" id="ss" enctype="multipart/form-data" style="border:1px solid #DDDDDD">
			<input type="hidden" value="upload_now_tab" name="form_type" id="form_type" />
			<h4>Upload file</h4>
			<div id="upload_now_tab" class="form-file-tabs">
				<div style="display: block;" id="file_now_template">	
						<div class="control-group">
							<label class="control-label" for="">File name</label>
							<label class="control-label" for="">
								<?php echo $doc->file_title; ?>
							</label>
						</div>
						<div class="control-group">
							<label class="control-label" for="">File Description</label>
							<label class="control-label" for="">
								<?php echo $doc->description; ?>
							</label>
						</div>
						<div class="control-group">
							<label class="control-label" for=""></label>
							<div class="controls file_now_container">
								<input type="file" data-rule-required="true" name="upload_file">
							</div>
						</div>
				</div>
			</div>
			<div class="control-group">
				<label for="" class="control-label"></label>
				<div class="controls file_now_container">
					<button type="submit" class="btn btn-primary" name="update" id="update">Upload</button> 
				</div>
			</div>
		</form>
	</div>	
</div>
