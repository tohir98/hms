<style>
	.control-group {
		padding:15px !important;
	}
	h4, h5 {
		padding-left:15px;
	}
</style>
<div class="box">
	<div class="page-header">
		<div class="pull-left">
			<h1>Edit Bank details</h1>
		</div>
    </div>
	<div class="breadcrumbs">
		<ul>
			<li><a href="<?php echo site_url('personal'); ?>">My Profile</a><i class="icon-angle-right"></i></li>
			<li><a href="#">Edit Bank details</a></li>
		</ul>
		<div class="close-bread"></div>
	</div>
	<?php if($success_message != '') { ?>
	<div class="alert alert-success" style="margin-top:20px;">
			<button data-dismiss="alert" class="close" type="button">×</button>
			<strong>Success!</strong> <?php echo $success_message; ?>
	</div>
	<?php } ?>
	<?php 
	if(!empty($error_messages)) { ?>
		<div class="alert alert-danger" style="margin-top:20px;">
			<button data-dismiss="alert" class="close" type="button">×</button>
		<?php
		foreach($error_messages as $err_message) {
	?>
		<strong><?php echo $err_message; ?></strong><br />
	<?php } ?>
		</div>
	<?php
	}
	?>
	<div class="box-content">
		<form action="" method="POST" class="form-horizontal form-wizard form-wysiwyg form-striped" id="edit_profile" enctype="multipart/form-data" style="border:1px solid #DDDDDD">
			<div class="control-group">
				<label class="control-label" for="first_name">Account name:</label>
				<div class="controls">
					<input type="text" data-rule-required="true" class="input-xlarge" id="account_name" name="account_name" value="<?php echo $profile->bank_account_name; ?>" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="first_name">Bank:</label>
				<div class="controls">
					<select name="bank" id="bank" data-rule-required="true">
					<?php foreach($banks as $bank) { 
						if($bank->id_bank == $profile->id_bank) {
							$sel = ' selected="selected" ';
						} else {
							$sel = '';
						}	
						?>
						<option value="<?php echo $bank->id_bank; ?>" <?php echo $sel; ?>><?php echo $bank->name; ?></option>
					<?php } ?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="first_name">Account type:</label>
				<div class="controls">
					<select name="account_type" id="account_type" data-rule-required="true">
						<?php foreach($account_types as $index => $type) { 
						if($index == $profile->id_bank_account_type) {
							$sel = ' selected="selected" ';
						} else {
							$sel = '';
						}	
						?>
						<option value="<?php echo $index; ?>" <?php echo $sel; ?>><?php echo $type; ?></option>
					<?php } ?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="first_name">Account number:</label>
				<div class="controls">
					<input type="text" id="account_number" name="account_number" data-rule-required="true" value="<?php echo $profile->bank_account_number; ?>" />
				</div>
			</div>
			<div class="control-group">
				<label for="managers" class="control-label"></label>
				<div class="controls">
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</div>
		</form>
	</div>	
</div>
			
<script type="text/javascript">

$(document).ready(function() {
	
	$('#edit_profile').validate({
		rules: {
			account_name: {
				required: true
			},
			bank : {
				required:true
			},
			account_type:{
				required:true
			},
			account_number:{
				required:true
			}	
		},
		messages: {
			account_name: {
				required: "Enter Account name"
			},
			bank : {
				required: "Select Bank"
			},
			account_type : {
				required:"Select Account type"
			},
			account_number : {
				required:"Enter Account number"
			}
		}
	});
	
});

</script>