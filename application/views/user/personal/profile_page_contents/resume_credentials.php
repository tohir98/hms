<div class="box box-color box-bordered primary employee_info_box" id="credentials_container" style="display:none;">
    <div class="box-title">
        <h3>Document Uploads</h3>
    </div>
    <div class="box-content">
        <div style="border-bottom:1px solid #CCCCCC;">
            <ul class="nav nav-pills" style="margin-bottom:5px;">
                <li class="active">
                    <h4>Document Uploads</h4>
                </li>
            </ul>
        </div>
        <div style="margin-top:20px;">
            <?php
            if (!empty($uploads)) {
                $container = $this->user_auth->get('cdn_container');
                ?>
                <table cellpadding="3" class="table table-hover table-nomargin">
                    <thead>
                        <tr>
                            <th><strong>File name</strong></th>
                            <th><strong>Description</strong></th>
                            <th><strong>Status</strong></th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <?php
                    foreach ($uploads as $r) {
                        if ($r->type == 0) {
                            $status = 'Outstanding';
                        } else {
                            $status = 'Submitted';
                        }

                        if ($r->file_name != '') {
                            $file_get_url = site_url('cdn_file_url/file_temp_url') . '/' . $container . '/' . $r->file_name;
                        } else {
                            $file_get_url = '';
                        }
                        ?>
                        <tr>
                            <?php if ($file_get_url == '') { ?>
                                <td><?php echo $r->file_title; ?></td>
                            <?php } else { ?>	
                                <td><a href="<?php echo $file_get_url; ?>" target="_blank"><?php echo $r->file_title; ?></a></td>
                            <?php } ?>
                            <td><?php echo $r->description; ?></td>
                            <td><?php echo $status; ?></td>
                            <td>
                                <?php if ($this->user_auth->have_perm(EDIT_MY_CREDENTIALS) and $r->type == 1) { ?>
                                    <ul class="nav nav-pills" style="margin-bottom:5px;">
                                        <li class="dropdown pull-right">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">Action <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo site_url('personal/edit_document/' . $r->id_cred . '/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)); ?>">Edit file</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                <?php } elseif ($r->type == 0) { ?>	
                                    <ul class="nav nav-pills" style="margin-bottom:5px;">
                                        <li class="dropdown pull-right">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">Action <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo site_url('personal/upload_document/' . $r->id_cred . '/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)); ?>">Upload file</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                <?php } else { ?>
                                    &nbsp;
                                <?php } ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
            </table>
            <?php
            } 
            ?>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            </table>
        </div>
    </div>
</div>