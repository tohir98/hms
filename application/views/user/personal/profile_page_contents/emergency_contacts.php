<div class="box box-color box-bordered primary employee_info_box" id="emergency_container" style="display:none;">
    <div class="box-title">
        <h3>Emergency contacts</h3>
        <ul class="nav nav-pills" style="margin-bottom:5px;">
            <?php if ($this->user_auth->have_perm(EDIT_MY_EMERGENCY_CONTACTS)) { ?>
                <li class="dropdown pull-right">
                    <a href="<?php echo site_url('personal/edit_my_emergency_contacts'); ?>" class="popup_full">Edit Now</a>
                </li>
            <?php } ?>
        </ul>
    </div>
    <div class="box-content">
        
        <div style="margin-top:20px;">
            <table class="table">
                <tr>
                    <?php foreach ($e_contacts[1] as $title => $value) { ?>
                        <td width="14%"><strong><?php echo $title; ?>:</strong></td>

                    <?php } ?>
                </tr>
                <tr>
                    <?php foreach ($e_contacts[1] as $title => $value) { ?>
                        <td><?php echo $value; ?></td>
                    <?php } ?>
                </tr>
                <tr>
                    <?php foreach ($e_contacts[2] as $title => $value) { ?>
                        <td><?php echo $value; ?></td>
                    <?php } ?>
                </tr>

            </table>
        </div>
    </div>
</div>
