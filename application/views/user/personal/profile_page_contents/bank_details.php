<div class="box box-color box-bordered primary employee_info_box" id="bank_details_container" style="display:none;">
	<div class="box-title">
		<h3>Bank details</h3>
	</div>
	<div class="box-content">
		<div style="border-bottom:1px solid #CCCCCC;">
			<ul class="nav nav-pills" style="margin-bottom:5px;">
				<li class="active">
					<h4>Bank details</h4>
				</li>
				<?php if($this->user_auth->have_perm(EDIT_MY_BANK_DETAILS)) { ?>
				<li class="dropdown pull-right">
					<a href="<?php echo site_url('personal/edit_my_bank_details'); ?>">Edit Now</a>
				</li>
				<?php } ?>
			</ul>
		</div>
		<div style="margin-top:20px;">
			<table cellpadding="3">
				<?php foreach($bank_details as $title => $value) { ?>
					<tr>
						<td align="right"><strong><?php echo $title; ?>:</strong></td>
						<td><?php echo $value; ?></td>
					</tr>
				<?php } ?>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</div>
	</div>
</div>
