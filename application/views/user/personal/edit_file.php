<style>
	.control-group {
		padding:15px !important;
	}
	h4, h5 {
		padding-left:15px;
	}
</style>
<div class="box">
	<div class="page-header">
		<div class="pull-left">
			<h1>Edit file</h1>
		</div>
    </div>
	<div class="breadcrumbs">
		<ul>
			<li><a href="<?php echo site_url('employee/view_records/'.$this->uri->segment(4).'/'.$this->uri->segment(5)); ?>">Employee</a><i class="icon-angle-right"></i></li>
			<li><a href="#">Edit file</a></li>
		</ul>
		<div class="close-bread"></div>
	</div>
	<?php if($success_message != '') { ?>
	<div class="alert alert-success" style="margin-top:20px;">
			<button data-dismiss="alert" class="close" type="button">×</button>
			<strong>Success!</strong> <?php echo $success_message; ?>
	</div>
	<?php } ?>
	<?php 
	if(!empty($error_messages)) { ?>
		<div class="alert alert-danger" style="margin-top:20px;">
			<button data-dismiss="alert" class="close" type="button">×</button>
		<?php
		foreach($error_messages as $err_message) {
	?>
		<strong><?php echo $err_message; ?></strong><br />
	<?php } ?>
		</div>
	<?php
	}
	?>
	<div class="box-content">
		<form action="" method="POST" class="form-horizontal form-wizard form-wysiwyg form-striped" id="ss" enctype="multipart/form-data" style="border:1px solid #DDDDDD">
			<input type="hidden" value="upload_now_tab" name="form_type" id="form_type" />
			<h4>Edit file</h4>
			<div id="upload_now_tab" class="form-file-tabs">
				<div style="display: block;" id="file_now_template">	
						<div class="control-group">
							<label class="control-label" for="">File name</label>
							<div class="controls">
								<input type="text" value="<?php echo $file_title; ?>" data-rule-required="true" name="upload_file_name" style="width:500px;">
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="">File Description</label>
							<div class="controls">
								<textarea data-rule-required="true" name="upload_file_description" style="width:500px;"><?php echo $file_description; ?></textarea>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for=""></label>
							<div class="controls file_now_container">
								<input type="file" data-rule-required="true" name="upload_file">
								<?php 
								if($file_name != '') { 
									$container = $this->user_auth->get('cdn_container');
									$file_get_url = site_url('cdn_file_url/file_temp_url') . '/' . $container . '/' . $file_name;
								?>
								<a href="<?php echo $file_get_url; ?>" target="_blank"><?php echo $file_title; ?></a>
								<?php } ?>
							</div>
						</div>
				</div>
			</div>
			<div class="control-group">
				<label for="" class="control-label"></label>
				<div class="controls file_now_container">
					<button type="submit" class="btn btn-primary" name="update" id="update">Update</button> 
				</div>
			</div>
		</form>
	</div>	
</div>
