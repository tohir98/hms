<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
?>
<body>
	<div class="container">
		<div class="masthead">
			<div class="row-fluid">
				<div class="span6">
					<h3 class="muted" style=" vertical-align: bottom">
						<!-- <a href="<?php echo $login_url; ?>"><img src="/img/talentbase-logo.png"></a> Careers -->
						<?php if($logo_path != NULL) { ?>
							<img src="<?php $file_url = $this->cdn_api->temporary_url($logo_path); echo $file_url; ?>" width="220px" height="220px">
						<?php } elseif($logo_text != NULL) { 
							echo $logo_text;
						} else {
						?>
							<img src="/img/talentbase-logo.png">
						<?php } ?>
					</h3>
				</div>
				<div class="span6">
				</div>
			</div>
			<hr style=" margin-top: 0">
		</div>
		
<div class="row-fluid">
    <div class="span2">
        <a class="btn btn-inverse btn-small pull-left" href="<?php echo site_url(); ?>">
            <i class="icon-arrow-left icon-white"></i> Back To All Jobs
        </a>
    </div>
    <div class="span10">
	
</div>
</div>
<div class="alert alert-info" style="margin-top:20px;">
    Please enter your E-mail address to reset password
</div>
<?php if($error_message != '') { ?>
<div class="alert alert-danger" style="margin-top:20px;">
    <?php echo $error_message; ?>
</div>
<?php } ?>
<?php if($success_message != '') { ?>
<div class="alert alert-success" style="margin-top:20px;">
    <?php echo $success_message; ?>
</div>
<?php } ?>
<div class="row-fluid">
    <div class="span6">
        <div class="box box-bordered">
            <div class="box-title">
                <h3>Forgot password</h3><br />
            </div>
			<div class="box-content nopadding">
    <form id="applicant_login_frm" class="form-horizontal form-bordered" method="post">
        <div class="control-group">
                        <label for="textfield" class="control-label">Email</label>
                        <div class="controls">
                            <input type="email" id="email" placeholder="E-mail address" class="input-xlarge" name="email" required />
                        </div>
        </div>
        <input type="submit" id="send_button" class="btn btn-warning btn-large pull-right" value="Send" name="send_button" />
    </form>
            </div>
        </div>
    </div>
</div>
<hr style=" margin-top: 50px;">
<script>
    mixpanel.track("Job description page");
</script>
<div class="footer">
<p>Powered by TalentBase &copy; <?php echo date('Y'); ?></p>
</div>
</body>