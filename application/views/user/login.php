<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

if ($email_message != '') {
    $email_error_class = ' error';
} else {
    $email_error_class = '';
}

if ($password_message != '') {
    $password_error_class = ' error';
} else {
    $password_error_class = '';
}
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <!-- Apple devices fullscreen -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <!-- Apple devices fullscreen -->
        <meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />

        <title>HMS - login</title>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo site_url(); ?>css/bootstrap.min.css">
        <!-- Bootstrap responsive -->
        <link rel="stylesheet" href="<?php echo site_url(); ?>css/bootstrap-responsive.min.css">
        <!-- icheck -->
        <link rel="stylesheet" href="<?php echo site_url(); ?>css/plugins/icheck/all.css">
        <!-- Theme CSS -->
        <link rel="stylesheet" href="<?php echo site_url(); ?>css/style.css">
        <!-- Color CSS -->
        <link rel="stylesheet" href="<?php echo site_url(); ?>css/themes.css">


        <!-- jQuery -->
        <script src="<?php echo site_url(); ?>js/jquery.min.js"></script>

        <!-- Nice Scroll -->
        <script src="<?php echo site_url(); ?>js/plugins/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- Validation -->
        <script src="<?php echo site_url(); ?>js/plugins/validation/jquery.validate.min.js"></script>
        <script src="<?php echo site_url(); ?>js/plugins/validation/additional-methods.min.js"></script>
        <!-- icheck -->
        <script src="<?php echo site_url(); ?>js/plugins/icheck/jquery.icheck.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?php echo site_url(); ?>js/bootstrap.min.js"></script>
        <script src="<?php echo site_url(); ?>js/eakroko.js"></script>


        <!-- Favicon -->
        <link rel="shortcut icon" href="<?php echo site_url(); ?>img/favicon.ico" />
        <!-- Apple devices Homescreen icon -->
        <link rel="apple-touch-icon-precomposed" href="<?php echo site_url(); ?>img/apple-touch-icon-precomposed.png" />

    </head>

    <body class='login' <?php if($company): ?>style="background:  <?= $company->bg_color; ?>"<?php endif;?> >
        <div class="wrapper">

            <?php if ($login_tpl && file_exists($login_tpl)): ?>
                <?php include $login_tpl; ?>
            <?php else: ?>
                <div class="login-body">
                    <center>
                        <img src="/img/hms-logo.jpg" width="220px" height="220px">
                    </center>
                    <br/>
                    <form method='POST' class='form-validate' id="test">
                        <div class="control-group <?php echo $email_error_class; ?>">
                            <div class="email controls">
                                <input type="text" name='uemail' placeholder="Email address" class='input-block-level' data-rule-required="true" data-rule-email="true">
                                <?php if ($email_message != '') { ?>
                                    <span for="uemail" class="help-block error"><?php echo $email_message; ?></span>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="control-group <?php echo $password_error_class; ?>">
                            <div class="pw controls">
                                <input type="password" name="upw" placeholder="Password" class='input-block-level' data-rule-required="true">
                                <?php if ($password_message != '') { ?>
                                    <span for="uemail" class="help-block error"><?php echo $password_message; ?></span>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="submit">
                            <input type="submit" value="Sign me in" class='btn btn-primary'>
                        </div>
                    </form>
                    <?php if (count($providers)): ?>
                        <div class="row-fluid">
                            <?php foreach ($providers as $id => $provider): ?>
                                <div class="span1">&nbsp;</div>
                                <div class="span10">
                                    <a class="btn btn-block btn-large btn-warning" href="<?= $auth_factory->signinUrl($id) ?>">
                                        Login using <?= $provider->getName(); ?>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <div class="forget">
                        <a href="<?php echo site_url("forgot_password"); ?>"><span>Forgot password?</span></a>
                    </div>

                </div>
            <?php endif; ?>
        </div>
    </body>
</html>
