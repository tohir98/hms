<!-- Masked inputs -->
<script src="/js/plugins/maskedinput/jquery.maskedinput.min.js"></script>
<style>
	.control-group {
		padding:15px !important;
	}
	h4, h5 {
		padding-left:15px;
	}
</style>
<div class="box">
	<div class="page-header">
		<div class="pull-left">
            <a href="<?= site_url('employee/view_records/'.$this->uri->segment(3).'/'.$this->uri->segment(4)); ?>" class="btn btn-warning"><i class="icon-chevron-left"></i> Back</a>
			<h1>Edit Profile / Personal info</h1>
		</div>
    </div>
	<div class="breadcrumbs">
		<ul>
			<li><a href="<?php echo site_url('employee/view_records/'.$this->uri->segment(3).'/'.$this->uri->segment(3)); ?>">Employee</a><i class="icon-angle-right"></i></li>
			<li><a href="#">Edit Profile / Personal info</a></li>
		</ul>
		<div class="close-bread"></div>
	</div>
    <br>
	<?php if($success_message != '') { ?>
	<div class="alert alert-success" style="margin-top:20px;">
			<button data-dismiss="alert" class="close" type="button">×</button>
			<strong>Success!</strong> <?php echo $success_message; ?>
	</div>
	<?php } ?>
	<?php 
	if(!empty($error_messages)) { ?>
		<div class="alert alert-danger" style="margin-top:20px;">
			<button data-dismiss="alert" class="close" type="button">×</button>
		<?php
		foreach($error_messages as $err_message) {
	?>
		<strong><?php echo $err_message; ?></strong><br />
	<?php } ?>
		</div>
	<?php
	}
	?>
	<div class="box-content-padless">
		<form action="" method="POST" class="form-horizontal form-wizard form-wysiwyg form-striped" id="edit_profile" enctype="multipart/form-data" style="border:1px solid #DDDDDD">
			<h4>Personal info</h4>
			<div class="control-group">
				<label class="control-label" for="first_name">Gender:</label>
				<div class="controls">
					<select name="id_gender" id="id_gender">
						<?php 
						if($profile->id_gender == '1') { 
							$sel_male = ' selected="selected" ';
						} else {
							$sel_male = '';
						}
						if($profile->id_gender == '2') { 
							$sel_female = ' selected="selected" ';
						} else {
							$sel_female = '';
						}
						?>
						<option value="1" <?php echo $sel_male; ?>>Male</option>
						<option value="2" <?php echo $sel_female; ?>>Female</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label for="id_marital_status" class="control-label">Marital Status:</label>
				<div class="controls">
					<select name="id_marital_status" id="id_marital_status">
						<?php 
						if($profile['id_marital_status'] == '1') {
							$sel_simgle = ' selected="selected" ';
						} else {
							$sel_simgle = '';
						}
						if($profile['id_marital_status'] == '2') {
							$sel_married = ' selected="selected" ';
						} else {
							$sel_married = '';
						}
						?>
						<option value="1" <?php echo $sel_simgle; ?>>Single</option>
						<option value="2" <?php echo $sel_married; ?>>Married</option>
					</select>
				</div>
			</div>
            <div class="control-group">
				<label for="dob" class="control-label">Personal Number:</label>
				<div class="controls">
					<select id='phone_code' name="phone_code" class="select-2-me input-medium">
						<?php if(!empty($countries)) { 
							foreach($countries as $c) { 
								if($c->code == $profile['id_country_code']) { ?>
									<option value="<?php echo $c->code ?>" selected><?php echo $c->country.' - '.$c->code ?></option>
							<?php } else { ?>
								<option value="<?php echo $c->code ?>"><?php echo $c->country.' - '.$c->code ?></option>
						<?php } } } ?>
					</select>
                    <?php 
                        $p_1 = substr($profile['personal_number'], 0,3);
                        $p_2 = substr($profile['personal_number'], 3,3);
                        $p_3 = substr($profile['personal_number'], 6,4);   //- (999) - (999) - (9999)
                        $number_per = '- ('.$p_1.') - ('.$p_2.') - ('.$p_3.')';
                     ?>
					<input type="text" data-rule-required="true" class="input-xlarge" id="personal_number" name="personal_number" value="<?php echo $number_per ?>" />
                     <div class="check-line" style="margin-top:10px">
                        <input type="checkbox" id="default_acct" name="primary_phone_number" class='icheck-me' data-skin="square" data-color="blue" <?php if ($profile['is_primary_number'] == 1) { echo "checked"; } ?>> <label class='inline'>Set as primary contact number</label> 
                    </div>
				</div>
			</div>
			<div class="control-group">
				<label for="dob" class="control-label">Birth Date:</label>
				<div class="controls">
					<input type="text" id="dob" name="dob" class="datepick" data-rule-required="true" value="<?php echo $profile['dob']; ?>" />
				</div>
			</div>
			<div class="control-group">
				<label for="id_nationality" class="control-label">Country of Origin:</label>
				<div class="controls">
					<select name="id_nationality" id="id_nationality">
						<?php if(!empty($countries)) { 
							foreach($countries as $c) { 
								if($profile['id_nationality'] == $c->id_country) {
									$sel = ' selected="selected" ';
								} else {
									$sel = '';
								}	
								?>
								<option value="<?php echo $c->id_country; ?>" <?php echo $sel; ?>><?php echo $c->country; ?></option>
							<?php }
						} ?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label for="address" class="control-label">Address:</label>
				<div class="controls">
					<input type="text" id="address" name="address" class="" data-rule-required="true" value="<?php echo $profile['address']; ?>" />
				</div>
			</div>
			<div class="control-group">
				<label for="city" class="control-label">City:</label>
				<div class="controls">
					<input type="text" id="city" name="city" class="" data-rule-required="true" value="<?php echo $profile['city']; ?>" />
				</div>
			</div>
			<div class="control-group">
				<label for="country" class="control-label">Country:</label>
				<div class="controls">
					<select name="country" id="country">
						<?php if(!empty($countries)) { 
							foreach($countries as $c) { 
								if($profile['id_country'] == $c->id_country) {
									$sel = ' selected="selected" ';
								} else {
									$sel = '';
								}	
								?>
								<option value="<?php echo $c->id_country; ?>" <?php echo $sel; ?>><?php echo $c->country; ?></option>
							<?php }
						} ?>	
					</select>
				</div>
			</div>
			<div class="control-group">
				<label for="state" class="control-label">State:</label>
				<div class="controls" id="states_container">
					<select name="id_state" id="id_state">
						<option>[State]</option>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label for="managers" class="control-label"></label>
				<div class="controls">
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</div>
		</form>
	</div>	
</div>
			
<script type="text/javascript">

$(document).ready(function() {
	
	select_country(<?php echo $profile['id_country']; ?>);
	
	$('#country').change(function() {
		var id_country = $(this).val();
		select_country(id_country);
	});

	function select_country(id_country) {
		
		var countries_url = '<?php echo site_url('recruiting/recruiting/states_select/'); ?>'; 
		var ajax_url = countries_url + '/' + id_country + '/single/id_state';
		
		$.ajax({
		  type: 'POST',
		  url: ajax_url,
		  cache: false,
		  dataType: "html",
		  
		  beforeSend: function(html) {
			$('#states_container').html('Loading states...'); 
		  },
		  		  
	      success: function(html){
			$('#states_container').html(html); 
		  },	
		  error: function(html) {
			$('#states_container').html('Error: cannot load data.'); 
		  }
		 
		});
		
	}
	
	$('#edit_profile').validate({
		rules: {
			first_name: {
				required: true
			},
			last_name: {
				required: true
			},
			dob: {
				required: true
			},
			address : {
				required:true
			},
			city : {
				required:true
			}
		},
		messages: {
			first_name: {
				required: "Enter First name"
			},
			last_name: {
				required: "Enter Last name"
			},
			dob: {
				required: "Enter Birth Date"
			},
			address : {
				required: "Enter address"
			},
			city : {
				required: "Enter city"
			}
		}
	});
    
    $("#personal_number").mask("- (999) - (999) - (9999)");
    //$("#personal_number").val('<?php echo $profile['personal_number']; ?>');
	
});



</script>