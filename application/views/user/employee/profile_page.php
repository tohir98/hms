<?= flash_message_display(); ?>
<div class="box">
    <div class="box-content-padless">
        <div class="row-fluid">
            <div class="span12" style="margin-top: 10px;">
                <a class="btn btn-warning" href="<?php echo site_url('employee/view_staff_directory'); ?>"><i class="icon-angle-left"></i> Back</a>
                <a class="btn btn-primary" href="#"><i class="icon-print"></i> Print</a>
                <a class="btn btn-primary" href="#"><i class="icon-file"></i> PDF</a>
                <?php if (ALL_MODULE_TOUR > 0 AND STAFF_RECORDS_TOUR > 0) { ?>
                    <a class="btn btn-small btn-danger" href="javascript:void(0);" onclick="startIntro();">Take a tour</a>
                <?php } ?>
            </div>
        </div>
        <div class="row-fluid" style="margin-top:20px;">

            <div class="span12">
                <select class="select2-me input-xxlarge employee">
                    <option selected="selected"> &nbsp;</option>
                    <?php
                    if (!empty($employees)) {
                        $sel = '';
                        foreach ($employees as $e) {
                            if ($e->id_string == $this->uri->segment(3)) {
                                ?>
                                <option class="employee" value="<?php echo $e->id_string; ?>" data-first_name="<?php echo $e->first_name ?>" data-last_name="<?php echo $e->last_name ?>" selected="selected"> <?php echo $e->first_name . ' ' . $e->last_name . ' (' . $e->roles . ', ' . $e->departments . ')'; ?></option>
                            <?php } else { ?>
                                <option class="employee" value="<?php echo $e->id_string; ?>" data-first_name="<?php echo trim(preg_replace('/[^A-Z0-9]+/i', '-', $e->first_name), '-') ?>" data-last_name="<?php echo trim(preg_replace('/[^A-Z0-9]+/i', '-', $e->last_name), '-') ?>" > <?php echo $e->first_name . ' ' . $e->last_name . ' (' . $e->roles . ', ' . $e->departments . ')'; ?></option> 



                            <?php
                            }
                        }
                    }
                    ?>
                </select>
                <!--<h4><?php echo $profile->first_name . ' ' . $profile->last_name; ?>, <?php echo $profile->roles; ?>, <?php echo $profile->departments; ?></h4>-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="span2">
                <div class="box">
                    <div class="box-title" style="border:1px solid #CCCCCC;height:210px;padding:0;">
                        <div class="row-fluid" style="height:180px;overflow:hidden">
                            <?php
                            if ($profile->profile_picture_name != '') {
                                $profile_pic_url = $this->cdn_api->temporary_url($profile->profile_picture_name);
                            } else {
                                $profile_pic_url = '/img/profile-default.jpg';
                            }
                            ?>
                            <img src="<?php echo $profile_pic_url; ?>" />
                        </div>
                        <div class="row-fluid">
                            <div class="btn-group span12">
                                <a class="btn btn-primary dropdown-toggle span12" data-toggle="dropdown" href="#"><span class="caret"></span> Edit </a>
                                <ul class="dropdown-menu dropdown-primary">
                                    <li>
                                        <a href="<?php echo site_url('employee/edit_profile_picture/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)); ?>">Edit Now</a>
                                    </li>
<?php if ($send_request == true) {
    if ($profile->status == USER_STATUS_ACTIVE || $profile->status == USER_STATUS_PROBATION_ACCESS) {
        ?>
                                            <li>
                                                <a href="#" id="request_profile_pic">Request From Employee</a>
                                            </li>
    <?php }
} ?>
                                </ul>
                            </div>
                        </div>	
                    </div>
                    <div class="_box-content">

                        <div class="span12">
                            <p></p>
                            <ul class="nav nav-tabs nav-stacked left_menu">
                                <li class="active"><a href="#work_info_personal_info_container" class="left_menu_button" data-container="work_info_personal_info_container">Profile</a></li>
                                <li><a href="#resume_credentials_container" class="left_menu_button" data-container="resume_credentials_container">Document Uploads</a></li>
                                <li><a class="left_menu_button" href="#emergency_container">Emergency contacts</a></li>
                                <li><a class="left_menu_button" href="#internal_notes_container">Internal notes</a></li>
                            </ul>
                            <p></p>
                        </div>

                        <div class="span12">
                            <h6>Other</h6>
                            <p></p>
                            <ul class="nav nav-tabs nav-stacked">
                                <li><a class="left_menu_button" href="#bank_details_container">Bank details</a></li>
                                <li><a class="left_menu_button" href="#benefits_details_container">Benefits details</a></li>
                                <!--                                <li><a href="#" class="left_menu_button" data-container="employee_queries_container">Employee queries</a></li>
                                                                <li><a href="#" class="left_menu_button" data-container="vacations_records_container">Vacations records</a></li>
                                                                <li><a href="#" class="left_menu_button" href="#payroll_records_container">Payroll records</a></li>
                                                                <li><a href="#" class="left_menu_button" data-container="appraisal_records_container">Appraisal records</a></li>-->
                            </ul>
                            <p></p>
                        </div>

                    </div>
                </div>

            </div>
            <div class="span10">
                <?php
                echo $profile_content;
                echo $resume_content;
                echo $emergency_content;
                echo $internal_notes_content;
                echo $bank_details_content;
                //echo $employee_queries_content;
                echo $benefits_details_content;
                //echo $vacations_records_content;
                //echo $payroll_records_content;
                //echo $appraisal_records_content;
                ?>
            </div>
        </div>

    </div>
</div>
<div class="modal hide fade" id="request_from_employee_profile_pic_model" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="myModalLabel">Request from Employee</h4>
    </div>
    <div class="modal-body">
        <?php if ($request_message != '') { ?>
            <div class="alert alert-info">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <strong><?php echo $request_message; ?></strong>
            </div>
<?php } ?>
        <form method="POST" action="<?php echo site_url('employee/request_profile_picture/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)); ?>" id="profile_pic_request_frm">
            <p>
                Instruction
                <br />
                <textarea data-rule-required="true" id="profile_pic_request_description" name="profile_pic_request_description" style="width:500px;" class="ui-wizard-content"></textarea>
                <span class="error" id="error_profile_pic_request_description" style="display:none;">
                    <br />Enter Instruction
                </span>
                <br />
                Due date
                <br />
                <input type="text" name="profile_pic_due_date" id="profile_pic_due_date" class="datepick" value="" />
                <span class="error" id="error_profile_pic_due_date" style="display:none;">
                    <br />Enter due date
                </span>
            </p>
        </form>
    </div>
    <div class="modal-footer">
        <button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
        <button data-id_dept="" class="btn btn-primary" id="request_from_employee_profile_pic_btn">Send request</button>
    </div>
</div>

<div class="modal hide fade" id="popup_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" >

</div>

<div class="modal hide fade modal-full" id="popup_modal_full" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" >

</div>

<script type="text/javascript">

    $(document).ready(function () {

        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
        $('.pickDate').datepicker({
            startDate: now
        });

        $('#request_profile_pic').click(function () {
            $('#request_from_employee_profile_pic_model').modal('show');
            return false;
        });

        $('#request_from_employee_profile_pic_btn').click(function () {

            var send = true;

            if ($('#profile_pic_request_description').val() == '') {
                $('#error_profile_pic_request_description').show();
                send = false;
            } else {
                $('#error_profile_pic_request_description').hide();
            }

            if ($('#profile_pic_due_date').val() == '') {
                $('#error_profile_pic_due_date').show();
                send = false;
            } else {
                $('#error_profile_pic_due_date').hide();
            }

            if (send == true) {
                $('#profile_pic_request_frm').submit();
            } else {
                return false;
            }

        });

        $('.left_menu_button').click(function () {
            $('.left_menu_button').parent().removeClass('active');
            $(this).parent().addClass('active');

            $('.employee_info_box').hide();

            var container_id = $(this).attr('href');
            $(container_id).show();
            window.location.href = container_id;
            return false;
        });

        if (window.location.hash !== '' && window.location.hash !== '#') {
            var k = window.location.hash.substring();
            $('.left_menu_button[href="' + k + '"]').click();
        }


        $('.employee').change(function () {
            var id_string = $(this).val();
            var first_name = $(this).find(':selected').attr('data-first_name');
            var last_name = $(this).find(':selected').attr('data-last_name');
            window.location.href = '<?php echo site_url('employee/view_records'); ?>/' + id_string + '/' + first_name + '-' + last_name;
        });

        $('.popup').click(function (eve) {
            eve.preventDefault();
            $('#popup_modal').modal('show').fadeIn();
            $('#popup_modal').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

            var page = $(this).attr("href");
            $.get(page, function (html) {

                $('#popup_modal').html('');
                $('#popup_modal').html(html).show();
                icheck();

            });
        });

        $('.popup_full').click(function (eve) {
            eve.preventDefault();
            $('#popup_modal_full').modal('show').fadeIn();
            $('#popup_modal_full').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

            var page = $(this).attr("href");
            $.get(page, function (html) {

                $('#popup_modal_full').html('');
                $('#popup_modal_full').html(html).show();
                icheck();

            });
        });


    });


</script>


<script>
    function startIntro() {
        var intro = introJs();
        intro.setOptions({
            steps: [
                {
                    element: ".employee",
                    intro: "Simply select an employee from this drop-down to view employee's profile.",
                    position: "buttom"
                },
                {
                    element: ".left_menu",
                    intro: "<strong>Profile</strong> - Shows employees work and personal information. </br>" +
                            "<strong>Document Uploads</strong> - Click here to view uploaded documents or upload/request documents .</br>" +
                            "<strong>Emergency Contacts</strong> - Create/Edit your Company Location.</br>" +
                            "<strong>Internal Notes</strong> - Add/View Internal notes.</br>" +
                            "<strong>Bank Details</strong> - Click here to view employee's bank information.</br>" +
                            "<strong>Benefit Details</strong> - Click here to view employee's benefit/pension information.</br>",
                    position: "right"
                },
                {
                    element: "#sec_work_info",
                    intro: "This shows a quick overview an employee's work information.",
                    position: "top"
                },
                {
                    element: "#sec_edit_work_info",
                    intro: "The Edit drop-down allows you to edit employee information.",
                    position: "left"
                },
                {
                    element: "#sec_personal_info",
                    intro: "This shows a quick overview an employee's personal information.",
                    position: "top"
                },
                {
                    element: "#sec_edit_pers_info",
                    intro: "The Edit drop-down allows you to edit/request employee's personal information.",
                    position: "left"
                }
            ]
        });

        intro.start();
    }
</script>