<?php
//var_dump($banks);  
$account_default = array(
    1 => 'Yes',
    0 => 'No',
    2 => 'No'
);

$phone_type = array(
    1 => 'Work Number',
    2 => 'Personal Number',
);
?>
<style>
    .control-group {
        padding:15px !important;
    }
    h4, h5 {
        padding-left:15px;
    }
    .solid_border{
        border:1px solid #DDDDDD;
    }
</style>
<!-- Display error/success message -->
<br/>
<?php if ($this->session->userdata('errors') != '') { ?>
    <div class="alert alert-danger">
        <button data-dismiss="alert" class="close" type="button">x</button>
        <?php echo $this->session->userdata('errors'); ?>
    </div>
    <?php
    $this->session->unset_userdata('errors');
}
if ($this->session->userdata('confirm') != '') {
    ?>
    <div class="alert alert-success">
        <button data-dismiss="alert" class="close" type="button">x</button>
        <?php echo $this->session->userdata('confirm'); ?>
    </div>
    <?php
    $this->session->unset_userdata('confirm');
}
?>

<!-- Page Header -->
<div class="page-header">
    <div class="pull-left">
        <h1>Pending Approvals</h1>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">

    </div>
</div>

<div class="breadcrumbs">
    <ul>
        <li>
            <a href="<?php echo site_url('employee') ?>">Staff Records</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="<?php echo site_url('employee/pending_approval') ?>">Pending Approvals</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Pending Approvals Details</a>
        </li>
    </ul>
</div>

<div class="page-header">
    <a href="<?php echo site_url('employee/pending_approval') ?>" class="btn btn-warning"><i class="icons icon-arrow-left"></i> Back</a>
</div>


<div class="box box-bordered">
    <div class="box-title" style="padding-right:5px;">
        <h3>
<?php echo $pending_approvals[0]['first_name'] . ' ' . $pending_approvals[0]['last_name']; ?>
        </h3>
    </div>

    <div class="box-content">

        <?php
        $sub = $pending_approvals[0]['data_submitted'];
        foreach ($sub as $b) {

            if ($b->request_type == $request_types['home_address']) {
                include 'requestEntries/_home_address.php';
            }
            ?>

            <?php if ($b->request_type == $request_types['birthday']) : ?>
                <?php include 'requestEntries/_birthday.php'; ?>
            <?php endif; ?>

            <?php
            if ($b->request_type == $request_types['gender']) :
                include 'requestEntries/_gender.php';
            endif;
            ?>
        
            <?php
            if ($b->request_type == $request_types['marital_status']) :
                include 'requestEntries/_marital.php';
            endif;
            ?>
        
            <?php
            if ($b->request_type == $request_types['city']) :
                include 'requestEntries/_city.php';
            endif;
            ?>
        
            <?php
            if ($b->request_type == $request_types['state']) :
                include 'requestEntries/_state.php';
            endif;
            ?>
        
            <?php
            if ($b->request_type == $request_types['personal_phone']) :
                include 'requestEntries/_personal_phone.php';
            endif;
            ?>
        
            <?php
            if ($b->request_type == $request_types['country']) :
                include 'requestEntries/_country.php';
            endif;
            ?>
        
            <?php
            if ($b->request_type == $request_types['nationality']) :
                include 'requestEntries/_nationality.php';
            endif;
            ?>

            <?php if ($b->request_type == $request_types['bank_account']) { ?>
                <?php include 'requestEntries/_bank_details.php'; ?>
            <?php } ?>

            <?php if ($b->request_type == $request_types['pension_details']) { ?>
                <?php include 'requestEntries/_benefit_details.php'; ?>
            <?php } // benefits  ?>

            <?php if ($b->request_type == $request_types['emergency1']) { ?>
                <?php include 'requestEntries/_emergency1.php'; ?>
            <?php } ?>

            <?php if ($b->request_type == $request_types['emergency2']) { ?>
                <?php include 'requestEntries/_emergency2.php'; ?>
    <?php } ?>




<?php } // end forech   ?>


    </div>
</div>

<!-- Error Modal -->
<div id="modal-error" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-body">
        <p>Sorry, we are unable to complete your request at the moment, pls try again later</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>		
        <button data-dismiss="modal" id="btn_reset_password" data-dismiss="modal" class="btn btn-danger closeme">Yes, reset </button>
    </div>
</div>

<div id="reasonModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <h4>Decline Entry</h4>
    </div>
    <form method="post" class="form">

        <div class="modal-body">
            <div class="control-group">
                <label class="control-label">Reason:</label>
                <div class="controls">
                    <textarea name="reason" id="reason" rows="3" style="width: 100%"></textarea>
                    <div id="reason_err" class="error hide">Pls supply a reason</div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" id="request_type_"/>
            <input type="hidden" id="form_id_"/>
            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>		
            <button id="btn_decline" class="btn btn-primary decline_request_now">Confirm</button>
        </div>
    </form>
</div>

<div id="reWorkModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <h4>Re-Work Entry</h4>
    </div>
    <form method="post" class="form">

        <div class="modal-body">
            <div class="control-group">
                <label class="control-label">Reason:</label>
                <div class="controls">
                    <textarea id="rework_reason" rows="3" style="width: 100%"></textarea>
                    <div id="rework_reason_err" class="error hide">Pls supply a reason</div>
                </div>
            </div>

            <div class="control-group">
                <label>Due date</label>
                <div class="controls">
                    <input type="text" name="request_due_date" id="request_due_date" class="datepick" />
                    <span class="error" id="error_request_due_date" style="display:none;">
                        Enter due date
                    </span>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <input type="hidden" id="rework_request_type_"/>
            <input type="hidden" id="rework_form_id_"/>
            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>		
            <button class="btn btn-primary rework_request_now">Confirm</button>
        </div>
    </form>
</div>

<div id="modal-approve" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-body">
        <p>Are you sure you want to approve this?</p>
    </div>
    <div class="modal-footer">
        <input type="hidden" id="inp_data-request" />
        <input type="hidden" id="inp_data-form_id" />
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">No</button>
        <button class="btn btn-primary approve_request" data-dismiss="modal" aria-hidden="true">Yes, Approve</button>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        var id_temp_request = '<?php echo $this->uri->segment(3); ?>';

        $('.approve_modal').click(function () {
            var request_type_ = $(this).attr('data-request_type');
            var form_id_ = $(this).attr('data-form_id');
            $('#inp_data-request').val(request_type_);
            $('#inp_data-form_id').val(form_id_);

            $('#modal-approve').modal('show').fadeIn();
        });

        $('body').delegate('.approve_request', 'click', function (evt) {
            evt.preventDefault();
            $(this).attr('disabled', true);
            var request_type_ = $('#inp_data-request').val();
            var form_id_ = $('#inp_data-form_id').val();

            show_loader();
            var url_ = '<?php echo site_url('user/employee/approve_request'); ?>';
            $.post(
                    url_,
                    {
                        'request_type': request_type_,
                        'id_temp_request': id_temp_request,
                    },
                    function (res) {
                        hide_loader();
                        if (res !== '') {
                            $('#' + form_id_).html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">x</button>' + res + '</div>');
                        } else {
                            $('#modal-error').modal('show');
                        }
                    }
            );

            return false;

        });

        $('body').delegate('.decline_request', 'click', function (evt) {
            evt.preventDefault();
//            $(this).attr('disabled', true);
            var request_type_ = $(this).attr('data-request_type');
            $('#request_type_').val(request_type_);
            var form_id_ = $(this).attr('data-form_id');
            $('#form_id_').val(form_id_);

            $('#reasonModal').modal('show').fadeIn();
        });

        $('body').delegate('.rework_request', 'click', function (evt) {
            evt.preventDefault();
//            $(this).attr('disabled', true);
            var request_type_ = $(this).attr('data-request_type');
            $('#rework_request_type_').val(request_type_);
            var form_id_ = $(this).attr('data-form_id');
            $('#rework_form_id_').val(form_id_);

            $('#reWorkModal').modal('show').fadeIn();
        });



        $('body').delegate('.decline_request_now', 'click', function (evt) {
            evt.preventDefault();
            $('.decline_request').attr('disabled', false);
            var request_type_ = $('#request_type_').val();
            var form_id_ = $('#form_id_').val();
            var reason_ = $('#reason').val();
            if (reason_ == '') {
                $('#reason_err').show();
                return false;
            } else {
                $('#reason_err').hide();
            }

            $('#reasonModal').modal('hide').fadeIn();

            show_loader();
            var url_ = '<?php echo site_url('user/employee/decline_request'); ?>';
            $.post(
                    url_,
                    {
                        'request_type': request_type_,
                        'id_temp_request': id_temp_request,
                        'reason': reason_,
                    },
                    function (res) {
                        hide_loader();
                        if (res !== '') {
                            $('#' + form_id_).html(res);
                        } else {
                            $('#modal-error').modal('show');
                        }
                    }
            );

            return false;

        });

        $('body').delegate('.rework_request_now', 'click', function (evt) {
            evt.preventDefault();
            $('.decline_request').attr('disabled', false);
            var request_type_ = $('#rework_request_type_').val();
            var form_id_ = $('#rework_form_id_').val();
            var reason_ = $('#rework_reason').val();
            var due_date_ = $('#request_due_date').val();

            if (reason_ == '') {
                $('#rework_reason_err').show();
                return false;
            } else {
                $('#rework_reason_err').hide();
            }

            if (due_date_ == '') {
                $('#error_request_due_date').show();
                $('#request_due_date').focus();
                return false;
            } else {
                $('#error_request_due_date').hide();
            }

            console.log(request_type_);
            console.log(form_id_);
            console.log(reason_);
            console.log(due_date_);

            $('#reWorkModal').modal('hide').fadeIn();

            show_loader();
            var url_ = '<?php echo site_url('user/employee/rework_request'); ?>';
            $.post(
                    url_,
                    {
                        'request_type': request_type_,
                        'id_temp_request': id_temp_request,
                        'reason': reason_,
                    },
                    function (res) {
                        hide_loader();
                        if (res !== '') {
                            $('#' + form_id_).html(res);
                        } else {
                            $('#modal-error').modal('show');
                        }
                    }
            );

            return false;

        });


    }); // end of document.ready
</script>
