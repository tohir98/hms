<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h4 class="modal-title" id="myModalLabel">Upload / Request file</h4>
</div>


<form action="<?php echo site_url('employee/upload_document/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)); ?>" method="POST" class="form-horizontal form-wizard form-wysiwyg form-striped" id="uploadfrm" enctype="multipart/form-data" style="border:1px solid #DDDDDD" >
    <div class="modal-body">
        <input type="hidden" value="upload_now_tab" name="form_type" id="form_type" />
        
            <div style="display: block;" id="file_now_template">	
                <div class="control-group">
                    <label class="control-label" for="">File name</label>
                    <div class="controls">
                        <input required type="text" value="" data-rule-required="true" id="upload_file_name" name="upload_file_name" style="width:500px;">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="">File Description</label>
                    <div class="controls">
                        <textarea required name="upload_file_description" style="width:500px;"></textarea>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for=""></label>
                    <div class="controls file_now_container">
                        <input required type="file" data-rule-required="true" id="upload_file" name="upload_file[]" multiple />
                        <label class="inline"> Use Ctrl + Click to select multiple files </label>
                    </div>
                </div>
            </div>
       
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary" name="update">Save</button> 
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        </div>
</form>
</div>


<script type="text/javascript">

    $(document).ready(function () {
        
        $('#upload_now_tab').show();

        $('#update').click(function () {
            var type = $('#form_type').val();
            
            $('#upload_file_name').unbind();
            $('#upload_file').unbind();
            
            if (type === 'upload_now_tab') {
                if ($('#upload_file_name').val() === '') {
                    alert('Enter file name');
                    $('#upload_file_name').focus();
                    return false;
                }

                if ($('#upload_file').val() == '')
                {
                    alert('Pls upload a file');
                    $('#upload_file').focus();
                    return false;
                }
            }

            $('#uploadfrm').submit();
        });

        $('.tab-form-item').click(function () {
            $('.tab-form-item').removeClass('btn-primary');
            $('.tab-form-item').addClass('btn-default');

            $(this).addClass('btn-primary');

            $('.form-file-tabs').hide();
            var tab_id = $(this).attr('data-tab');
            $('#' + tab_id).show();

            $('#form_type').val(tab_id);

        });

        var _type = '<?php echo $this->input->post('form_type'); ?>';
        if (_type == 'request_employee') {
            $('.tab-form-item').removeClass('btn-primary');
            $('.tab-form-item').addClass('btn-default');

            $('#request_btn').addClass('btn-primary');

            $('.form-file-tabs').hide();
            $('#request_employee').show();

            $('#form_type').val(_type);
        }

    });
</script>