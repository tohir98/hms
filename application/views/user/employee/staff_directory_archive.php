<div class="box">
	<div class="page-header">
		<div class="pull-left">
			<h1>
				Staff Directory
				<?php if(ALL_MODULE_TOUR > 0 AND STAFF_RECORDS_TOUR > 0){ ?>
				<a class="btn btn-small btn-danger" href="javascript:void(0);" onclick="startIntro();">Take a tour</a>
				<?php } ?>
			</h1>
		</div>
    </div>
	<div class="breadcrumbs">
		<ul>
			<li><a href="#">Manage Staff</a><i class="icon-angle-right"></i></li>
            <li><a href="#">Staff Directory</a><i class="icon-angle-right"></i></li>
			<li><a href="#">Archive</a></li>
		</ul>
		<div class="close-bread"></div>
	</div>
	
	<?php if ($this->session->userdata('errors') != '') { ?>
	<br/>
	<div class="alert alert-danger">
		 <button data-dismiss="alert" class="close" type="button">x</button>
		 <?php echo $this->session->userdata('errors'); ?>
	</div>
	<?php $this->session->unset_userdata('errors'); } ?>
	<?php if($success_message != '') { ?>
	<div class="alert alert-success" style="margin-top:20px;">
			<button data-dismiss="alert" class="close" type="button">×</button>
			<strong>Success!</strong> <?php echo $success_message; ?>
	</div>
	<?php } ?>
	<?php 
	if(!empty($error_messages)) { ?>
		<div class="alert alert-danger" style="margin-top:20px;">
			<button data-dismiss="alert" class="close" type="button">×</button>
		<?php
		foreach($error_messages as $err_message) {
	?>
		<strong><?php echo $err_message; ?></strong><br />
	<?php } ?>
		</div>
	<?php
	}
	?>
    
    <!-- -->
    <div class="box-content nopadding">
        <p>
            <ul class="nav nav-tabs" id="myTab">
                <li>
                    <a href="<?php echo site_url('employee/view_staff_directory') ?>">Current</a>
                </li>
                <li class="active">
                    <a href="#">Archive</a>
                </li>
            </ul>
        </p>
                                        
<!--        <div class="tab-content"> 
            <div class="tab-pane active" id="overview">
            </div>
            
             
        </div>-->
    </div>
    <!-- -->
    
	<div class="box-content nopadding">
		<div class="box box-bordered">
			<div class="box-title" style="padding:10px !important;">
				<form method="post" action="" id="sel" style="margin:0 !important;">
					
					<h3><i class="icon-reorder"></i></h3>
					
				</form>
			</div>
			<div class="box-content nopadding" data-height="600">
				<table class="table table-user table-hover table-nomargin dataTable">
					<thead>
						<tr>
							<th></th>
							<th>Name</th>
							<?php if($this->user_auth->have_perm(VIEW_EMPLOYEE_RECORDS)) { ?>
							<th class='hidden-1024'>Status</th>
							<?php } ?>
							<th class='hidden-480'>Work Details</th>
							<th class='hidden-480'>Contact</th>
							<?php if($actions == true) { ?>
								<th class='hidden-480' nowrap id='action_status'>Action</th>
							<?php } ?>
						</tr>
					</thead>
				<tbody>
                <?php
				if(!empty($profiles)) {
					foreach($profiles as $profile) {
						$name_link = strtolower($profile->first_name) . '-' . strtolower($profile->last_name);
                    ?>
					<tr>
						<td class='img'>
							<?php 
							$profile_pic_src = '';
							if($profile->profile_picture_name != '') { 
								$profile_pic_src = $this->cdn_api->temporary_url($profile->profile_picture_name);
							} 	 
							
							if($profile_pic_src == '') {
								$profile_pic_src = '/img/profile-default.jpg';
							}
							?>
							<img src="<?php echo $profile_pic_src; ?>" width="40" height="40" />
                        </td>
                        <td>
							<strong>
								<?php if($view_records == true) { ?>
								<a href="<?php echo site_url("employee/view_records/" . $profile->id_string . '/' . $name_link); ?>">
									<?php echo $profile->first_name . " " . $profile->last_name; ?>
								</a>
								<?php } else { ?>
									<?php echo $profile->first_name . " " . $profile->last_name; ?>
								<?php } ?>
							</strong>
						</td>
						<?php if($this->user_auth->have_perm(VIEW_EMPLOYEE_RECORDS)) { ?>
						<td class='hidden-1024'>
							<?php echo $statuses[$profile->status]; ?>
							<?php 
                            $job_types_str = '';
							if($profile->emp_job_types != '') {
								

								$ujts = explode(',', $profile->emp_job_types);
								foreach($ujts as $ujt) {
									if(isset($job_types[$ujt])) {
										$job_types_str .= $job_types[$ujt] . ', ';
									}
								}

								$job_types_str = rtrim($job_types_str, ', ');

							} else {
								$job_types_str = 'N/A';
							}

							?>
						</td>
						<?php } ?>
					<td class='hidden-480'>
						<?php if($profile->roles != '') { ?>
							<span class="muted"><?php echo $profile->roles; ?></span><br />
						<?php } ?>
						<?php if($profile->departments != '') { ?>
							<span class="muted"><?php echo $profile->departments; ?></span><br />
						<?php } ?>
						<?php if($profile->location != '') { ?>
							<span class="muted"><?php echo $profile->location; ?></span><br>
						<?php } ?>
						<?php if($job_types_str != '') { ?>
							<span class="muted"><?php echo $job_types_str; ?></span>
						<?php } ?>
					</td>                                           
                    <td>
						<?php $arr = explode('.', $profile->email);
							if(strtolower($arr[1]) == 'user@talentbase' and strtolower($arr[2]) == 'ng'){
								echo 'N/A<br/>';
							} else {
						?>
						<a href="#"><?php echo $profile->email; ?></a><br />
						<?php } ?>
						<?php if($profile->work_number != '') { ?>
							<span class='muted'><?php echo '('. $profile->id_country_code .') - '. $profile->work_number; ?></span><br />
						<?php } ?>
						<!--
                        <?php if($profile->address != '') { ?>
							<span class='muted'>Home Address: <?php echo $profile->address; ?></span><br />
						<?php } ?>	
						-->
					</td>
					<?php if($actions == true) { ?>
					<td nowrap>
						<div class="btn-group">
							<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Action <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<?php if($this->user_auth->have_perm(VIEW_EMPLOYEE_RECORDS)) { ?>
								<li>
									<a href="<?php echo site_url("employee/view_records/" . $profile->id_string . '/' . $name_link); ?>">View Records</a>
								</li>
								<?php } ?>
							</ul>
						</div>
					</td>
					<?php } ?>
				</tr>
			<?php	
            }
			}
            ?>
		</tbody>
	</table>
</div>
</div>

	</div>
</div>


<script type="text/javascript">
var current_id_user = '';
var current_profile_name = '';
var current_profile_email = '';

$(document).ready(function() {
	
	//$('.phone_format').mask("(999) - (999) - (999) - (9999)");
	
	$('#id_statuses').change(function() {
		$('#sel').submit();
	});
	
	$('.dataTables_paginate a').click(function() {
		init_status_menu();
		init_reset_password();
	});
	
	init_status_menu();
	init_reset_password();
});

function init_reset_password() {
	
	$('.reset-password-link').unbind();
	$('.reset-password-link').click(function() {
		
		current_id_user = $(this).attr('data-id_user');
		current_profile_name = $(this).attr('data-profile_name');
		current_profile_email = $(this).attr('data-profile_email');
		current_id_string = $(this).attr('data-id_string');
		
		$('#id_user_strr').val(current_id_user);
		$('#employee_namer').html(current_profile_name);
		$('#employee_full_namer').val(current_profile_name);
		$('#employee_emailr').val(current_profile_email);
		$('#employee_id_string').val(current_id_string);
		
		$('#reset_employee_password_modal').modal('show');
	});	
	
	$('#reset_password_btn').click(function() {
		$('#reset_password_frm').submit();
	});
	
}

function init_status_menu() {
	
	$('#no_email').unbind();
	$('#no_email').click(function() {
	   if($(this).is(':checked')) {
		   $('#email').attr('disabled', 'disabled');
	   } else {
		   $('#email').removeAttr('disabled');
	   }
    });	
	
	// Add by Chuks -- Start
	$('#no_email_status').hide();
	// End
   
	$('.change-status-link').unbind();
	$('.change-status-link').click(function() {
		
		current_id_user = $(this).attr('data-id_user');
		current_profile_name = $(this).attr('data-profile_name');
		current_profile_email = $(this).attr('data-profile_email');
		
		var current_status = $(this).attr('data-cur_st');
		
		if(current_status == 1 || current_status == 4) {
			$('#email_container').show();
			$('#email').val(current_profile_email);
		} else {
			$('#email_container').hide();
			$('#email').val('');
		}
		
		$('#id_user_st').val(current_id_user);
		$('#employee_name').html(current_profile_name);
		$('#employee_full_name').val(current_profile_name);
		$('#employee_email').val(current_profile_email);
		//$('#email').val(current_profile_email);
				
		$('._status').show();
		$('._status').removeAttr('selected');
		$('#status_' + current_status).attr('selected', 'selected');
		
		$('#change_employee_status_modal').modal('show');
		return false;
	});
	
	$('#change_status_btn').unbind();
	$('#change_status_btn').click(function() {
		
		var st = $('#status').val();
		if(st == 1 || st == 4) {
			var email = $('#email').val();
			if(email == '') {
				$('#email_err').show();
				return false;
			} else {
				$('#email_err').hide();
			}	
		}
		$('#change_status_frm').submit();
	});
	
	$('#status').unbind();
	$('#status').change(function() {
	   
	   var id_status = $(this).val();
	   
	   $('#probation_expire_date_container').hide();
	   $('#probation_message_container').hide();
	   $('#probation_message_container_txt').html('');
	   $('#terminated_expire_date_container').hide();
	   
	   if(id_status == '<?php echo USER_STATUS_PROBATION_NO_ACCESS; ?>') {
		   $('#probation_expire_date_container').show();
		   $('#probation_message_container').show();
		   $('#probation_message_container_txt').html('This employee is on probation and has no access to employee Portal. Please set probation end-date below');
	   } 
	   
	   if(id_status == '<?php echo USER_STATUS_PROBATION_ACCESS; ?>') {
		   $('#probation_expire_date_container').show();
		   $('#probation_message_container').show();
		   $('#probation_message_container_txt').html('This employee is on probation and has access to employee Portal. Please set probation end-date below');
	   } 
	   
	   if(id_status == '<?php echo USER_STATUS_ACTIVE_NO_ACCESS; ?>') {
		   $('#probation_message_container').show();
		   $('#probation_message_container_txt').html('This employee will not have access to the employee portal. Recommended for semi/unskilled employees');
	   }
	   
	   if(id_status == '<?php echo USER_STATUS_TERMINATED; ?>') {
		   $('#terminated_expire_date_container').show();
	   }
	   
	   if(id_status == 1 || id_status == 4) {
			$('#email_container').show();
			$('#email').val(current_profile_email);
	   } else {
			$('#email_container').hide();
			$('#email').val(current_profile_email);
	   }
		
	   if(id_status != '<?php echo USER_STATUS_ACTIVE; ?>' && id_status != '<?php echo USER_STATUS_PROBATION_ACCESS; ?>') {
		   $('.user_request_data').each(function() {
			   $(this).find('input:radio').removeAttr("checked");
			   $(this).hide();
			   $(this).prev().find('input:radio').click();
		   });
	   } else {
		   $('.user_request_data').each(function() {
			   $(this).show();
		   });
	   }
	   
   });
}


function startIntro(){
        var intro = introJs();
			intro.setOptions({
			steps: [
					  { 
					    element: ".subnav-menu",
					    intro: "<strong>Add New Employee</strong> - Click here to add a new employee to your records. </br>" +
					    "<strong>Manager View</strong> - Here's a summary view of employees within your purview. You can send SMS messages to each employee from this page.</br>" +
					    "<strong>Company Location</strong> - Create/Edit your Company Location.</br>" +
					    "<strong>Department</strong> - Create/Edit your  Departments.</br>" +
					    "<strong>Employee Roles</strong> - Create/Edit your Employee Roles.</br>" +
					    "<strong>Organogram</strong> - Click here to view your org structure. You will need to fully assign managers and locations to all employees to view the entire structure.</br>",
					    position: "right"
					  },
					  { 
					    element: "#sel",
					    intro: "You can filter the list of employees by selecting or removing the status options.",
					    position: "left"
					  },
					  { 
					    element: "#action_status",
					    intro: "View Records allows you to access detailed personnel records on this employee, Edit Status and Reset Password",
					    position: "left"
					  }
					]
			});

			intro.start();
    }

// Active/Probation Access on Display of Email
$('#status').change(function(){
	alert('hello');
});
</script>