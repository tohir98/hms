<!-- Masked inputs -->
<script src="/js/plugins/maskedinput/jquery.maskedinput.min.js"></script>

<div class="box">
    <div class="page-header">
        <div class="pull-left">
            <h1>Add New Employee</h1>
        </div>
    </div>

    <?php if ($success_message != '') { ?>
        <div class="alert alert-success" style="margin-top:20px;">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <strong>Success!</strong> <?php echo $success_message; ?>
        </div>
    <?php } ?>
    <?php if (!empty($error_messages)) { ?>
        <div class="alert alert-danger" style="margin-top:20px;">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php
            foreach ($error_messages as $err_message) {
                ?>
                <strong><?php echo $err_message; ?></strong><br />
            <?php } ?>
        </div>
        <?php
    }
    ?>
    <div class="box-content-padless">
       
        <ul class="nav nav-tabs" id="myTab">
            <li class="active">
                <a href="#create_account" data-toggle="tab">New Employee</a>
            </li>
            <li class="">
                <a href="#bulk_users" data-toggle="tab" >Employee Bulk Upload</a>
            </li>
        </ul>

        <div class="tab-content"> 
            
            <div class="tab-pane active" id="create_account">
                <div class="box">
                    <div class="box-content">
                        <?php include '_employee_form.php'; ?> 
                    </div>
                </div>
            </div>
            
            <div class="tab-pane" id="bulk_users">
                <div class="row-fluid">
                    <div class="span12">
                        <ul class="tiles">

                            <li class="orange long" id="import_excel">
                                <a href="#"><span style=" font-size: 18px">Import from a .xls file</span></a>
                            </li>

                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>
    $('#import_excel').click(function () {
            self.location = "<?php echo site_url('employee/upload/excel'); ?>";
        });
</script>