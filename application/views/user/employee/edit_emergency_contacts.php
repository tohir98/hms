
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h4 class="modal-title" id="myModalLabel">Edit Emergency contacts</h4>
</div>

<form action="<?php echo site_url('employee/edit_profile_emergency_contacts/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)); ?>" method="POST" class="form-horizontal form-wizard form-wysiwyg form-striped" id="edit_emergency_contacts" enctype="multipart/form-data" style="border:1px solid #DDDDDD">
    <div class="modal-body">
        
        <table class="table table-striped">
            <tr>
                <th><h5>Contact 1</h5></th>
                <th><h5>Contact 2</h5></th>
            </tr>
            <tr>
                <td>
                    <div class="control-group">
                        <label class="control-label" for="emergency_first_name">First name:</label>
                        <div class="controls">
                            <input type="text" name="emergency_first_name" id="emergency_first_name" value="<?php echo $profile['emergency_first_name']; ?>" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="emergency_last_name">Last name:</label>
                        <div class="controls">
                            <input type="text" name="emergency_last_name" id="emergency_last_name" value="<?php echo $profile['emergency_last_name']; ?>" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="emergency_address">Address:</label>
                        <div class="controls">
                            <input type="text" name="emergency_address" id="emergency_address" value="<?php echo $profile['emergency_address']; ?>" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="emergency_email">Email:</label>
                        <div class="controls">
                            <input type="text" name="emergency_email" id="emergency_email" value="<?php echo $profile['emergency_email']; ?>" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="emergency_phone_number">Phone:</label>
                        <div class="controls">
                            <input type="text" name="emergency_phone_number" id="emergency_phone_number" value="<?php echo $profile['emergency_phone_number']; ?>" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="emergency_employer">Employer:</label>
                        <div class="controls">
                            <input type="text" name="emergency_employer" id="emergency_employer" value="<?php echo $profile['emergency_employer']; ?>" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="emergency_work_address">Work address:</label>
                        <div class="controls">
                            <input type="text" name="emergency_work_address" id="emergency_work_address" value="<?php echo $profile['emergency_work_address']; ?>" />
                        </div>
                    </div>
                </td>
                <td>
                    <div class="control-group">
                        <label class="control-label" for="first_name">First name:</label>
                        <div class="controls">
                            <input type="text" name="emergency_first_name2" id="emergency_first_name2" value="<?php echo $profile['emergency_first_name2']; ?>" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="emergency_last_name2">Last name:</label>
                        <div class="controls">
                            <input type="text" name="emergency_last_name2" id="emergency_last_name2" value="<?php echo $profile['emergency_last_name2']; ?>" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="emergency_address2">Address:</label>
                        <div class="controls">
                            <input type="text" name="emergency_address2" id="emergency_address2" value="<?php echo $profile['emergency_address2']; ?>" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="emergency_email2">Email:</label>
                        <div class="controls">
                            <input type="text" name="emergency_email2" id="emergency_email2" value="<?php echo $profile['emergency_email2']; ?>" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="emergency_phone_number2">Phone:</label>
                        <div class="controls">
                            <input type="text" name="emergency_phone_number2" id="emergency_phone_number2" value="<?php echo $profile['emergency_phone_number2']; ?>" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="emergency_employer2">Employer:</label>
                        <div class="controls">
                            <input type="text" name="emergency_employer2" id="emergency_employer2" value="<?php echo $profile['emergency_employer2']; ?>" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="emergency_work_address2">Work address:</label>
                        <div class="controls">
                            <input type="text" name="emergency_work_address2" id="emergency_work_address2" value="<?php echo $profile['emergency_work_address2']; ?>" />
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>	
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Update</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    </div>
</form>


<script type="text/javascript">

    $(document).ready(function () {

        $('#edit_emergency_contacts').validate({
            rules: {
                emergency_first_name: {
                    required: true
                },
                emergency_last_name: {
                    required: true
                },
                emergency_address: {
                    required: true
                },
                emergency_email: {
                    required: true,
                    email: true
                },
                emergency_phone_number: {
                    required: true,
                    rangelength: [13, 13],
                    digits: true
                },
                emergency_employer: {
                    required: true
                },
                emergency_work_address: {
                    required: true
                },
                /////////////////////////
                emergency_first_name2: {
                    required: true
                },
                emergency_last_name2: {
                    required: true
                },
                emergency_address2: {
                    required: true
                },
                emergency_email2: {
                    required: true,
                    email: true
                },
                emergency_phone_number2: {
                    required: true,
                    rangelength: [13, 13],
                    digits: true
                },
                emergency_employer2: {
                    required: true
                },
                emergency_work_address2: {
                    required: true
                }
            },
            messages: {
                emergency_first_name: {
                    required: "Enter First name"
                },
                emergency_last_name: {
                    required: "Enter last name"
                },
                emergency_address: {
                    required: "Enter address"
                },
                emergency_email: {
                    required: "Enter email",
                    email: "Enter valid email address"
                },
                emergency_phone_number: {
                    required: "Enter Phone",
                    rangelength: "Enter valid number (13 digits)",
                    digits: "Enter valid number (13 digits)"
                },
                emergency_employer: {
                    required: "Enter Employer name"
                },
                emergency_work_address: {
                    required: "Enter Work address"
                },
                ///////////////////////////
                emergency_first_name2: {
                    required: "Enter First name"
                },
                emergency_last_name2: {
                    required: "Enter last name"
                },
                emergency_address2: {
                    required: "Enter address"
                },
                emergency_email2: {
                    required: "Enter email",
                    email: "Enter valid email address"
                },
                emergency_phone_number2: {
                    required: "Enter Phone",
                    rangelength: "Enter valid number (13 digits)",
                    digits: "Enter valid number (13 digits)"
                },
                emergency_employer2: {
                    required: "Enter Employer name"
                },
                emergency_work_address2: {
                    required: "Enter Work address"
                }
            }
        });

    });

</script>