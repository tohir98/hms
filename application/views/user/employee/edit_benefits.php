
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h4 class="modal-title" id="myModalLabel">Edit Benefits details</h4>
</div>

<?php if ($success_message != '') { ?>
    <div class="alert alert-success" style="margin-top:20px;">
        <button data-dismiss="alert" class="close" type="button">×</button>
        <strong>Success!</strong> <?php echo $success_message; ?>
    </div>
<?php } ?>
<?php if (!empty($error_messages)) { ?>
    <div class="alert alert-danger" style="margin-top:20px;">
        <button data-dismiss="alert" class="close" type="button">×</button>
        <?php
        foreach ($error_messages as $err_message) {
            ?>
            <strong><?php echo $err_message; ?></strong><br />
        <?php } ?>
    </div>
    <?php
}
?>
<form action="<?php echo site_url('employee/edit_benefits/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)); ?>" method="POST" class="form-horizontal form-wizard form-wysiwyg form-striped" id="edit_profile" enctype="multipart/form-data" style="border:1px solid #DDDDDD">
    <div class="modal-body">
        <div class="control-group">
            <label class="control-label" for="rsa_pin">RSA PIN:</label>
            <div class="controls">
                <input type="text" data-rule-required="true" class="input-xlarge" id="rsa_pin" name="rsa_pin" value="<?php echo $profile->rsa_pin; ?>" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="pfs">Pension Fund Administrator:</label>
            <div class="controls">
                <select name="pfs" id="pfs" data-rule-required="true" style="width:300px;">
                    <?php
                    foreach ($pfas as $pfa) {
                        if ($pfa->id_pension_fund_administrator == $profile->id_pfs) {
                            $sel = ' selected="selected" ';
                        } else {
                            $sel = '';
                        }
                        ?>
                        <option value="<?php echo $pfa->id_pension_fund_administrator; ?>" <?php echo $sel; ?>><?php echo $pfa->name; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Update</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    </div>

</form>


<script type="text/javascript">

    $(document).ready(function () {

        $('#edit_profile').validate({
            rules: {
                rsa_pin: {
                    required: true
                },
                pfs: {
                    required: true
                }
            },
            messages: {
                rsa_pin: {
                    required: "Enter RSA PIN"
                },
                pfs: {
                    required: "Select Pension Fund Administrator"
                }
            }
        });

    });

</script>