<!-- Display error/success message -->
<?php if ($this->session->userdata('errors') != '') { ?>
    <div class="alert alert-danger">
        <button data-dismiss="alert" class="close" type="button">x</button>
        <?php echo $this->session->userdata('errors'); ?>
    </div>
    <?php
    $this->session->unset_userdata('errors');
}
if ($this->session->userdata('confirm') != '') {
    ?>
    <div class="alert alert-success">
        <button data-dismiss="alert" class="close" type="button">x</button>
        <?php echo $this->session->userdata('confirm'); ?>
    </div>
    <?php
    $this->session->unset_userdata('confirm');
}
?>

<!-- Page Header -->
<div class="page-header">
    <div class="pull-left">
        <h1>Settings</h1>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">

    </div>
</div>

<div class="box">

    <div class="box-content nopadding">

        <?php $this->load->view('common/employee_settings_tab'); ?>

        <div class="tab-content"> 
            <div class="tab-pane active" id="approval">



                <div class="row-fluid">
                    <div class="span12">
                        <div class="box box-bordered">
                            <div class="box-title">
                                <h3>
                                    <i class="icon-bar-chart"></i>
                                    Approval Workflow
                                </h3>
                                <a href="#add_approver_modal" data-toggle='modal' class="btn btn-warning pull-right" style=" margin-right: 10px;">Add New</a>
                            </div>
                            <div class="box-content">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <?php
                                                foreach ($approvers as $a) {
                                                    ?><span class='label'><?php echo ucwords($a->first_name . ' ' . $a->last_name) ?></span><?php
                                                }
                                                ?>
                                            <td><a href="#" class="edit_approval" data-desc="Edit approver" >Edit</a></td>

                                        </tr>


                                        <?php
                                        if (!empty($approver_workflows)) {
                                            $j = 0;
                                            $cnt = count($approver_workflows) - 1;
                                            foreach ($approver_workflows as $a) {
                                                ?>
                                                <tr>
                                                    <td> <?php echo $approver_steps[$j]; ?> Approver 
                                                        &nbsp;
                                                        <?php echo '<span class="label">' . $a->first_name . ' ' . $a->last_name . '</span>'; ?> </td>
                                                    <td> 
                                                        <?php if ($j == $cnt) { ?>
                                                            <a class="btn btn-danger modal_remove_approver" href="<?php echo site_url('employee/remove_approver/' . $a->id_workflow_approver); ?>">Remove </a>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <?php
                                                ++$j;
                                            }
                                        }
                                        ?>
                                        <tr>
                                            <td><a href="#" class="add_workflow_approver" > [ Add Approver ] </a> </td>
                                            <td> &nbsp; </td>
                                            <td> &nbsp; </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div><!-- End row-fluid -->


            </div><!-- End Div Overview -->
        </div>
    </div>    
</div>

<div class="modal hide fade" id="add_approver_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" >
    <div class="row_fluid"> 
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h4 class="modal-title"> Add New Approver </h4>
        </div>

        <div id="loading_msg" class="alert alert-info" style="display:none;"><i>processing....</i></div>
        <div id="success_msg" class="alert alert-success" style="display:none;"><i>Approver added successfully</i>.</div>

        <form name="add_approver_frm" id="add_approver_frm" method="post" action="" class="form-horizontal form-bordered">
            <div class="modal-body nopadding" style="">
                <div class="control-group" style="margin-left: 30px">
                    <label for="select" class="control-label">Select Employee</label>
                    <div class="controls">
                        <select id="id_approver" name="id_approver" class="select2-me input-large">
                            <option value="" selected="selected"> select employee</option>
                            <?php
                            if (!empty($employees)) {
                                foreach ($employees as $e) {
                                    ?>
                                    <option value="<?php echo $e->id_user ?>"><?php echo $e->first_name . ' ' . $e->last_name ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <span class="error" id="error_" style="display:none;">
                            Select an employee
                        </span>
                    </div>
                </div>
            </div>
            <div class="modal-footer" id="footer_modal">
                <button data-dismiss="modal" class="btn btn-default" aria-hidden="true">Cancel</button>
                <button type="button" id="btn_add_approver" class="btn btn-primary"> Add </button>
            </div>
        </form>
    </div>
</div>

<div id="approval_modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel">Edit Data Approver</h4>
    </div>

    <form method="post" action="<?= site_url('employee/edit_data_approver') ?>" class="form-horizontal">
        <div class="modal-body">

            <div class="control-group">
                <label for="frequency" class="control-label">Employees</label>
                <div class="controls">
                    <select name="employees4[]" id="employees4" class="select2-me input-large">
                        <?php
                        $employee_ = array();
                        foreach ($approvers as $cp) {
                            ?>
                            <option value="<?php echo $cp->id_user; ?>" selected><?php echo ucwords($cp->first_name . ' ' . $cp->last_name); ?></option>
                            <?php
                            $employee_[] = $cp->id_user;
                        }
                        ?>

                        <?php
                        foreach ($employees as $em) {
                            if (!in_array($em->id_user, $employee_)) {
                                ?>
                                <option value="<?php echo $em->id_user; ?>"><?php echo ucwords($em->first_name . ' ' . $em->last_name); ?></option>
                                <?php
                            }
                        }
                        ?>

                    </select>
                </div>
            </div>

        </div>

        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <input type="submit" value="Update" class="btn btn-primary pull-right" />
        </div>

    </form>
</div>

<!-- Approver process modal  -->
<div class="modal hide fade" id="add_workflow_approver_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" >
    <div class="row_fluid"> 
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h4 class="modal-title"> Approver Workflow </h4>
        </div>
        <div id="loading_msg" class="alert alert-info" style="display:none;"><i>processing....</i></div>
        <div id="success_msg" class="alert alert-success" style="display:none;"><i>Approver added successfully</i>.</div>
        <form name="add_approver_frm" id="add_approver_frm" method="post" action="" class="form-horizontal form-bordered">
            <div class="modal-body nopadding" style="">
                <div class="control-group" style="margin-left: 30px">
                    <label for="select" class="control-label">Select Employee</label>
                    <div class="controls">
                        <select id="id_workflow_approver" name="id_workflow_approver">
                            <option value="" selected="selected"> select employee</option>
                            <?php
                            if (!empty($employees)) {
                                foreach ($employees as $e) {
                                    ?>
                                    <option value="<?php echo $e->id_user ?>"><?php echo $e->first_name . ' ' . $e->last_name ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer" id="footer_modal">
                <button data-dismiss="modal" class="btn btn-default" aria-hidden="true">Cancel</button>
                <button type="button" id="btn_workflow_approver" class="btn btn-primary"> Add </button>
            </div>
        </form>
    </div>
</div>

<div id="modal-remove_approver" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-body">
        <p>Are you sure you want to delete this?</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
        <a href="#" class="btn btn-danger closeme">Yes, remove</a>
    </div>
</div>

<script>
    $('#btn_add_approver').click(function () {

        var send = true;

        if ($('#id_approver').val() === '') {
            $('#error_').show();
            $('#id_approver').focus();
            send = false;
        } else {
            $('#error_').hide();
        }

        if (send === true) {

            var form_data = {
                id_user: $('#id_approver').val()
            };

            $.ajax({
                url: '<?php echo base_url() ?>employee/settings',
                type: 'POST',
                data: form_data,
                success: function (msg) {
                    $('#loading_msg').hide();
                    $('#success_msg').show();
                    location.reload();
                }
            });
            return false;
        }

    });

    $('.edit_approval').click(function (eve) {

        eve.preventDefault();
        var id_role = $(this).attr("data-id");
        $('input[name=id_role]').val(id_role);
        $('#approval_modal').modal('show');
        $('.chzn-container').css('width', '100%');
        $('.chzn-drop').css('width', '100%');
        $('.modal-body').css('overflow-y', 'visible');

    });

    $('.add_workflow_approver').unbind();
    $('.add_workflow_approver').click(function () {
        $('#add_workflow_approver_modal').modal('show');
    });

    $('#btn_workflow_approver').click(function () {
        $('#btn_workflow_approver').attr('disabled', true);
        $('#loading_msg').show();

        var form_data = {
            id_user: $("#id_workflow_approver").val(),
        };


        $.ajax({
            url: '<?php echo base_url() ?>user/employee/process_workflow_approver',
            type: 'POST',
            data: form_data,
            success: function (msg) {
                $('#loading_msg').hide();
                $('#success_msg').show();
                location.reload();
            }
        });
        return false;
    });

    $('.modal_remove_approver').click(function (eve) {
        eve.preventDefault();
        $('#modal-remove_approver').modal('show').fadeIn();

        var page = $(this).attr("href");
        $('a.closeme').attr('href', page);
    });

</script>
