<div class="page-header">
	<div class="pull-left">
		<h1>Manager View</h1>
	</div>
</div>

<!--  Bread crumbs  -->
<div class="breadcrumbs">
	<ul>
		<li><a href="#">Manage Staff</a><i class="icon-angle-right"></i></li>
		<li><a href="#">Staff Directory</a></li>
	</ul>
	<div class="close-bread"></div>
</div>

<!-- Display error/success message -->
<?php if ($this->session->userdata('errors') != '') { ?>
<br/>
<div class="alert alert-danger">
	 <button data-dismiss="alert" class="close" type="button">x</button>
	 <?php echo $this->session->userdata('errors'); ?>
</div>
<?php $this->session->unset_userdata('errors'); } 
if ($this->session->userdata('confirm') != '') { ?>
<br/>
<div class="alert alert-success">
	 <button data-dismiss="alert" class="close" type="button">x</button>
	 <?php echo $this->session->userdata('confirm'); ?>
</div>
<?php $this->session->unset_userdata('confirm'); }  ?>

<p></p><br/>

<ul class="nav nav-tabs" id="myTab">
	<li class="active"><a href="#direct" data-toggle="tab">Direct Report </a></li>
	<?php if($is_head) { ?>
	<li><a href="#location" data-toggle="tab">By Location </a></li>
	<?php } ?>
</ul>

<div class="tab-content">
	<!-- Direct Report -->
	<div class="tab-pane active" id="direct">
		<div class="box box-bordered">
			<div class="box-title" style="padding:10px !important;">
				<h3>Employees under Manager</h3>
				<!--<a href="#" class='btn btn-warning pull-right'>Add New Employee</a>-->
			</div>
			<div class="box-content nopadding" data-height="600">
				<table class="table table-user table-hover table-nomargin dataTable">
					<thead>
						<tr>
							<th></th>
							<th>Name</th>
							<th class='hidden-480'>Work Details</th>
							<th class='hidden-1024'>Status</th>
							<th class='hidden-480' nowrap></th>
						</tr>
					</thead>
				<tbody>

                <?php
				if(!empty($employees)) {
					foreach($employees as $profile) {
						$name_link = strtolower($profile->first_name) . '-' . strtolower($profile->last_name);
						$name = strtolower($profile->first_name) . ' ' . strtolower($profile->last_name);
                    ?>
					<tr>
						<td class='img'>
							<?php 
							$profile_pic_src = '';
							
							if($profile->profile_picture_name != '') { 
								
								$profile_img = $this->cdn_api->temporary_url($profile->profile_picture_name);
								$profile_pic_src = $profile_img;
							} 	 
							
							if($profile_pic_src == '') {
								$profile_pic_src = '/img/profile-default.jpg';
							}
							?>
							<img src="<?php echo $profile_pic_src; ?>" width="40" height="40" />
                        </td>
                        <td>
							<strong>
								<a href="<?php echo site_url("employee/manager_view_records/" . $profile->id_string . '/' . $name_link); ?>">
									<?php echo ucwords($name); ?>
								</a>
							</strong>
						</td>
					<td class='hidden-480'>
						<?php if($profile->roles != '') { ?>
							<span class="muted"><?php echo $profile->roles; ?></span><br />
						<?php } ?>
						<?php if($profile->departments != '') { ?>
							<span class="muted"><?php echo $profile->departments; ?></span><br />
						<?php } ?>
					</td>
                    <td class='hidden-1024'>
						<?php echo $statuses[$profile->status]; ?>
						<?php //echo $types[$profile->status]; ?>
					</td>					
					<td nowrap>
						
						<div class="btn-group">
							<a class="btn dropdown-toggle btn-primary" data-toggle="dropdown" href="#">Action <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="#" class='add_note' data-id='<?php echo $profile->id_user;?>'>Add Note</a></li>
								<li><a href="<?php echo site_url("employee/manager_view_records/" . $profile->id_string . '/' . $name_link); ?>" class='' data-id='<?php echo $profile->id_user;?>'>View Note</a></li>
								<li><a href="#" class="submit_form_button" data-id_user='<?php echo $profile->id_user;?>'>Upload Records</a></li>
								<li><a href="#" class="sms_form_button" data-phone="<?php echo $profile->work_number; ?>" data-name='<?php echo $name; ?>' data-id_user='<?php echo $profile->id_user;?>'>Send SMS</a></li>
							</ul>
						</div>
					</td>
				</tr>
				<?php } } ?>
				</tbody>
			</table>
			</div>
		</div>
	</div>
	
	<!-- By Location -->
	<div class="tab-pane" id="location">
		<?php //var_dump($locations); ?>
		
		<div class="box box-bordered">
			<div class="box-title" style="padding:10px !important;">
				<h3>Employees under Location Head</h3>
				<!--<a href="#" class='btn btn-warning pull-right'>Add New Employee</a>-->
				<div class="control-group">
					<div class="controls">
						<div class="input-xlarge pull-right">
							<select name="assigned_location" id="assigned_location" class='chosen-select '>
								<option value="">Select Location</option>
								<?php if(!empty($locations)) { $l = array();
									foreach ($locations as $location) { 
										if(!in_array($location->id_location, $l)) { 
											if($location->id_location == $id_location){
								?>
								<option value="<?php echo $location->id_location; ?>" selected><?php echo $location->location_tag ?></option>
								<?php } else { ?>
								<option value="<?php echo $location->id_location; ?>"><?php echo $location->location_tag ?></option>
							    <?php } $l[] = $location->id_location; } } } ?>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="box-content nopadding" data-height="600">
				<table class="table table-user table-hover table-nomargin dataTable">
					<thead>
						<tr>
							<th></th>
							<th>Name</th>
							<th class='hidden-480'>Work Details</th>
							<th class='hidden-1024'>Status</th>
							<th class='hidden-480' nowrap></th>
						</tr>
					</thead>
				<tbody>

                <?php
				if(!empty($locations)) {
					foreach($locations as $profile) {
						$name_link = strtolower($profile->first_name) . '-' . strtolower($profile->last_name);
						$name = strtolower($profile->first_name) . ' ' . strtolower($profile->last_name);
                    ?>
					<tr>
						<td class='img'>
							<?php 
							$profile_pic_src = '';
							
							if($profile->profile_picture_name != '') { 
								
								$profile_img = $this->cdn_api->temporary_url($profile->profile_picture_name);
								$profile_pic_src = $profile_img;
							} 	 
							
							if($profile_pic_src == '') {
								$profile_pic_src = '/img/profile-default.jpg';
							}
							?>
							<img src="<?php echo $profile_pic_src; ?>" width="40" height="40" />
                        </td>
                        <td>
							<strong>
								<a href="<?php echo site_url("employee/manager_view_records/" . $profile->id_string . '/' . $name_link); ?>">
									<?php echo ucwords($name); ?>
								</a>
							</strong>
						</td>
					<td class='hidden-480'>
						<?php if($profile->roles != '') { ?>
							<span class="muted"><?php echo $profile->roles; ?></span><br />
						<?php } ?>
						<?php if($profile->departments != '') { ?>
							<span class="muted"><?php echo $profile->departments; ?></span><br />
						<?php } ?>
					</td>
                    <td class='hidden-1024'>
						<?php echo $statuses[$profile->status]; ?>
						<?php //echo $types[$profile->status]; ?>
					</td>					
					<td nowrap>
						
						<div class="btn-group">
							<a class="btn dropdown-toggle btn-primary" data-toggle="dropdown" href="#">Action <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="#" class='add_note' data-id='<?php echo $profile->id_user;?>'>Add Note</a></li>
								<li><a href="<?php echo site_url("employee/manager_view_records/" . $profile->id_string . '/' . $name_link); ?>" class='' data-id='<?php echo $profile->id_user;?>'>View Note</a></li>
								<li><a href="#" class="submit_form_button" data-id_user='<?php echo $profile->id_user;?>'>Upload Records</a></li>
								<li><a href="#" class="sms_form_button" data-phone="<?php echo $profile->work_number; ?>" data-name='<?php echo $name; ?>' data-id_user='<?php echo $profile->id_user;?>'>Send SMS</a></li>
							</ul>
						</div>
					</td>
				</tr>
				<?php } } ?>
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>

<!-- Note modal -->
<div class="modal hide fade" id="add_note_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h4 class="modal-title" id="myModalLabel">Add Note</h4>
	</div>
	<div class="modal-body">
		<?php $options = array(); ?>
		<div style="padding-right:10px;">
			<input type="hidden" name="note_id_user" id="note_id_user" value="" />
			<textarea name="note" id="note" class="form-control" style="width:100%"></textarea>
			<div id="enter_note_msg" class="error" style="display:none;">Enter Note.</div>
			<div id="loading_note_msg" class="alert alert-info" style="display:none;"><i>Note processing....</i></div>
		<div id="success_note_msg" class="alert alert-success" style="display:none;"><i>Note added successfully</i>.</div>
		</div>
		<div style="padding-right:10px;">
			<input type="checkbox" name='priority' id='priority'> High Priority (Email and SMS alert will be sent to HR)
		</div>
		<div style="padding-right:10px; margin-top:10px;">
			Who can see this note 
			<select name="who_sees" id="who_sees">
				<option value="1">Admin only</option>	
				<option value="2">Admin and Manager</option>
				<option value="3">Admin, Manager and Employee</option>
			</select>
		</div>
	</div>
	<div class="modal-footer">
		<button data-dismiss="modal" class="btn btn-default" aria-hidden="true">Cancel</button>
		<button class="btn btn-primary" aria-hidden="true" id="add_note_btn">Add Note</button>
	</div>
</div>

<!-- Modal note response box -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="note_confirm_box" style="display: none;">
	<div class="modal-header">
		<button aria-hidden="true" data-dismiss="modal" class="close" type="button">x</button>
		<h4 id="myModalLabel">Manager View</h4>
	</div>
	<div class="modal-body">
		<p id="id_note_msg"></p>
	</div>
	<div class="modal-footer">
		<button data-dismiss="modal" class="btn btn-primary" id="submit_dialog_sms_ok">Ok</button>
	</div>
</div>


<!-- Modal box for prompt -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="update_box" style="display: none;">
	<div class="modal-header">
		<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
		<h3 id="myModalLabel">Upload Document</h3>
	</div>
	<form id="upload" name="upload" method="post" action="<?php echo site_url('user/employee/manager_upload_document');?>" enctype="multipart/form-data">
		<div class="modal-body">
			<div class="control-group">
				<label for="filename" class="control-label">Document Name:</label>
				<div class="controls">
					<input type="text" name="file_name" id="file_name" placeholder="Document Name" class="input-xxlarge">
				</div>
			</div>
			<div class="control-group">
				<label for="filepath" class="control-label">Location:</label>
				<div class="controls">
					<input type="file" name="file" id="file" accept="application/msword, application/pdf, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-powerpoint, application/vnd.openxmlformats-officedocument.presentationml.presentation"/>
					<input type="hidden" name='id_user' id='id_user' value=''>
				</div>
			</div>
			<div class="control-group">
				<label for="description" class="control-label">Description:</label>
				<div class="controls">
					<textarea class="input-xxlarge" name="file_description" id="file_description" rows="2" placeholder="Description"></textarea>
				</div>
			</div>
		</div>
	</form>
	<div class="modal-footer">
		<button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
		<button data-dismiss="modal" class="btn btn-primary" id="submit_dialog_button">Upload</button>
	</div>
</div>


<!-- Modal box for prompt -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="sms_box" style="display: none;">
	<div class="modal-header">
		<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
		<h3 id="myModalLabel">Send SMS</h3>
	</div>
	<div class="modal-body">
		<div class="box-content">
			<form class='form-horizontal'>
				<div class="control-group">
					<label for="filename" class="control-label">To:</label>
					<div class="controls">
						<span id='em_name'></span>
					</div>
				</div>
				<div class="control-group">
					<label for="description" class="control-label">Message:</label>
					<div class="controls">
						<textarea class="input-block-level" name="sms_details" id="sms_details" rows="5" placeholder="Message" maxlength="140"></textarea>
						<input type="hidden" name='id_user2' id='id_user2' value=''>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<label id="cntr" class="control-label">140 words left</label>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="modal-footer">
		<button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
		<button data-dismiss="modal" class="btn btn-primary" id="send_sms_button">Send SMS</button>
	</div>
</div>

<!-- Modal SMS confirmation box -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="sms_request_box" style="display: none;">
	<div class="modal-header">
		<button aria-hidden="true" data-dismiss="modal" class="close" type="button">x</button>
		<h4 id="myModalLabel">Manager View</h4>
	</div>
	<div class="modal-body">
		<p id="">Do you wish to send SMS?</p>
	</div>
	<div class="modal-footer">
		<button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
		<button data-dismiss="modal" class="btn btn-primary" id="submit_dialog_sms_send">Yes</button>
	</div>
</div>

<!-- Modal SMS response box -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="sms_confirm_box" style="display: none;">
	<div class="modal-header">
		<button aria-hidden="true" data-dismiss="modal" class="close" type="button">x</button>
		<h4 id="myModalLabel">Manager View</h4>
	</div>
	<div class="modal-body">
		<p id="id_sms_msg"></p>
	</div>
	<div class="modal-footer">
		<button data-dismiss="modal" class="btn btn-primary" id="submit_dialog_sms_ok">Ok</button>
	</div>
</div>

<!-- Modal SMS confirmation box -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="doc_upload_box" style="display: none;">
	<div class="modal-header">
		<button aria-hidden="true" data-dismiss="modal" class="close" type="button">x</button>
		<h4 id="myModalLabel">Manager View</h4>
	</div>
	<div class="modal-body">
		<p id="">Do you wish to upload document?</p>
	</div>
	<div class="modal-footer">
		<button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
		<button data-dismiss="modal" class="btn btn-primary" id="submit_dialog_doc_send">Yes</button>
	</div>
</div>

<script type="text/javascript">
	
	$('.submit_form_button').click(function() {
		var id_user = $(this).attr('data-id_user');
		$('#id_user').val(id_user);
		$('#update_box').modal('show');
	});

	$('#submit_dialog_button').click(function() {
		$('#doc_upload_box').modal('show');
	});
	
	$('#submit_dialog_doc_send').click(function(){
		$('form#upload').submit();
		$('#doc_upload_box').modal('hide');
	});

	/* SEND SMS TO EMPLOYEE - EDITED BY CHUKS. */
	var phone;
	var id_user;
	var msg;
	var url_1;
	
	$('.sms_form_button').click(function(){
		$('#sms_details').val('');  // CLEAR MESSAGE BOX
		$('#cntr').html('140 words left'); // SET DEFAULT COUNTER
		
		id_user = $(this).attr('data-id_user');
		var name = $(this).attr('data-name');
		phone = $(this).data('phone');
		
		$('#id_user2').val(id_user);
		$('#em_name').html('<strong>'+name+'</strong>');
		$('#sms_box').modal('show');
	
	});

	$('#send_sms_button').click(function() {
		$('#sms_request_box').modal('show');
	});
	
	$('#submit_dialog_sms_send').click(function(){
		msg = $('#sms_details').val();
		url_1 = "<?php echo site_url('user/employee/send_sms_to_employee'); ?>";
		
		$.ajax({
			type: "POST",
			url: url_1,
			data: {message:msg, phone:phone, id_user:id_user},
			beforeSend:function(){},
			success:function(result){
				$('#id_sms_msg').html(result);
				$('#sms_confirm_box').modal('show');
				$('#sms_details').val('');
			}
		});
	});
	
	/* SMS CHARACTER COUNTER */
	var limit = 140;

	$('#sms_details').unbind('keyup change input paste').bind('keyup change input paste', function(e){
		var chars = $(this).val().length;
		$('#cntr').html(limit - chars + ' words left');
	});

	
	// --------------- ADD NOTE ---------------------- //

	$('.add_note').click(function() {

		$('#enter_note_msg').hide();
		var id_user = $(this).attr('data-id');
		$('#note_id_user').val(id_user);
		$('#loading_note_msg').hide();
		$('#success_note_msg').hide();
		$('#add_note_modal').modal('show');
	});
	
	var url_note = "<?php echo site_url('user/employee/add_note'); ?>";
	
	$('#add_note_btn').click(function(){
		
		var note = $('#note').val();
		var id_user = $('#note_id_user').val();
		var who_sees = $('#who_sees').val();
		var priority = $('#priority').val();
		
		$('#add_note_btn').attr("disabled", true);
		
		$.ajax({
			type: 'post',
			url: url_note,
			data: {id_user : id_user, note : note, who_sees : who_sees, priority : priority },
			dataType: 'html',
			beforeSend:function(){ },
			success:function(result){ 
				$('#id_note_msg').html(result);
				$('#note_confirm_box').modal('show');
				$('#add_note_modal').modal('hide');
				
				$('#add_note_btn').attr("disabled", false);
			}
		});
	});
	
	/* CHANGE LOCATION - UNIT HEAD */
	$('#assigned_location').change(function(){
		var i = $('#assigned_location').val();
		// url redirect
		var url = "<?php echo site_url('employee/manager_view/') ?>";
		window.location = url+'/'+i;
	});
</script>

<script>
  $(function () {
    $('#myTab a:first').tab('show');
  })
</script>

