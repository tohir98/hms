<!-- Masked inputs -->
<script src="/js/plugins/maskedinput/jquery.maskedinput.min.js"></script>
<style>
	.control-group {
		padding:15px !important;
	}
	h4, h5 {
		padding-left:15px;
	}
</style>
<div class="box">
	<div class="page-header">
		<div class="pull-left">
            <a href="<?= site_url('employee/view_records/'.$this->uri->segment(3).'/'.$this->uri->segment(4)); ?>" class="btn btn-warning"><i class="icon-chevron-left"></i> Back</a>
			<h1>Edit Profile / Work info</h1>
		</div>
    </div>
	<div class="breadcrumbs">
		<ul>
			<li><a href="<?php echo site_url('employee/view_records/'.$this->uri->segment(3).'/'.$this->uri->segment(3)); ?>">Employee</a><i class="icon-angle-right"></i></li>
			<li><a href="#">Edit Profile / Work info</a></li>
		</ul>
		<div class="close-bread"></div>
	</div>
    <br>
	<?php if($success_message != '') { ?>
	<div class="alert alert-success" style="margin-top:20px;">
			<button data-dismiss="alert" class="close" type="button">×</button>
			<strong>Success!</strong> <?php echo $success_message; ?>
	</div>
	<?php } ?>
	<?php 
	if(!empty($error_messages)) { ?>
		<div class="alert alert-danger" style="margin-top:20px;">
			<button data-dismiss="alert" class="close" type="button">×</button>
		<?php
		foreach($error_messages as $err_message) {
	?>
		<strong><?php echo $err_message; ?></strong><br />
	<?php } ?>
		</div>
	<?php
	}
	?>
	<div class="box-content-padless">
		<form action="" method="POST" class="form-horizontal form-wizard form-wysiwyg form-striped" id="edit_profile" enctype="multipart/form-data" style="border:1px solid #DDDDDD">
			<h4>Work info</h4>
			<?php 
			$termination_date_style = 'display:none;';
			$probation_expire_date_style = 'display:none;';
			?>
			<div class="control-group" style="display:none;" id="probation_message_container">
				<label for="managers" class="control-label"></label>
				<div class="controls" id="probation_message_container_txt">
					
				</div>
			</div>
			<div class="control-group" style="<?php echo $probation_expire_date_style; ?>" id="probation_expire_date_container">
				<label for="managers" class="control-label">Probation due date:</label>
				<div class="controls">
					<input type="text" name="probation_expire_date" id="probation_expire_date" class="datepick" value="<?php echo $profile['probation_expire_date']; ?>" />
				</div>
			</div>
			<div class="control-group" style="<?php echo $termination_date_style; ?>" id="terminated_expire_date_container">
				<label for="managers" class="control-label">Effective as of:</label>
				<div class="controls">
					<input type="text" name="termination_date" id="termination_date" value="<?php echo $profile['date_termination']; ?>" class="datepick" />
				</div>
			</div>
			
			<div class="control-group" id="div_no_email">
				<label class="control-label"></label>
				<div class="controls">
					<label>
						<input class="ui-wizard-content" type="checkbox" name="no_email" id="no_email" value="1" />
						<span style="margin-left:5px;margin-top:8px;">Does Not have Email</span>
					</label>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="first_name">Work Number:</label>
				<div class="controls">
					<select id='phone_code' name="phone_code" class="select-2-me input-medium">
						<?php if(!empty($countries)) { 
							foreach($countries as $c) { 
								if($c->code == $profile['id_country_code']) { ?>
									<option value="<?php echo $c->code ?>" selected><?php echo $c->country.' - '.$c->code ?></option>
							<?php } else { ?>
								<option value="<?php echo $c->code ?>"><?php echo $c->country.' - '.$c->code ?></option>
						<?php } } } ?>
					</select>
                     <?php 
                        $p_1 = substr($profile['work_number'], 0,3);
                        $p_2 = substr($profile['work_number'], 3,3);
                        $p_3 = substr($profile['work_number'], 6,4);   //- (999) - (999) - (9999)
                        $number_wor = '- ('.$p_1.') - ('.$p_2.') - ('.$p_3.')';
                     ?>
					<input type="text" data-rule-required="true" class="input-xlarge" id="work_number" name="work_number" value="<?php echo $number_wor ?>" />
                     <div class="check-line" style="margin-top:10px">
                        <input type="checkbox" id="default_acct" name="primary_phone_number" value="1" class='icheck-me' data-skin="square" data-color="blue" <?php if ($profile['is_primary_number'] == 1) { echo "checked"; } ?>> <label class='inline'>Set as primary contact number</label> 
                    </div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="first_name">Department:</label>
				<div class="controls">
					<select name="department" id="department" data-rule-required="true">
						<?php 
						if(!empty($departments)) {
							foreach($departments as $d) { 
								if($profile['id_dept'] == $d->deptid) {
									$sel = ' selected';
								} else {	
									$sel = '';
								}	
								?>
								<option value="<?php echo $d->deptid; ?>" <?php echo $sel; ?>><?php echo $d->departments; ?></option>
						<?php }
						}
						?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="first_name">Role:</label>
				<div class="controls">
					<select name="role" id="role" data-rule-required="true">
							<?php if(!empty($roles)) {
								foreach($roles as $r) { 
									if($profile['id_role'] == $r->id_role) {
										$sel = ' selected';
									} else {
										$sel = '';
									}	
									?>
									<option value="<?php echo $r->id_role; ?>" <?php echo $sel; ?>><?php echo $r->roles; ?></option>
								<?php }
							}
							?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="first_name">Job type:</label>
				<div class="controls">
					<div class="input-xlarge">
						<select name="job_type[]" id="job_type" multiple="multiple" class="chosen-select wizard-ignore input-xxlarge" data-rule-required="true">
							<?php if(!empty($job_types)) {
								foreach($job_types as $j) { 
									if(in_array($j->id_type, $profile['ext_job_types'])) {
										$sel = ' selected="selected" ';
									} else {
										$sel = '';
									}
									?>
									<option value="<?php echo $j->id_type; ?>" <?php echo $sel; ?>><?php echo $j->name; ?></option>
								<?php }
							}
							?>
						</select>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label for="company_location" class="control-label">Company location:</label>
				<div class="controls">
					<select name="company_location" id="company_location" data-rule-required="true">
						<?php 
						if(!empty($company_locations)) {
							foreach($company_locations as $item) { 
								if($profile['id_company_location'] == $item->id_location) {
									$sel = ' selected="selected" ';
								} else {
									$sel = '';
								}	
								?> 
							<option value="<?php echo $item->id_location; ?>" <?php echo $sel; ?>><?php echo $item->location_tag; ?></option>
						<?php }
						}
						?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label for="managers" class="control-label">Manager:</label>
				<div class="controls">
					<select name="manager" id="manager">
						<option value="0">N/A</option>
						<?php if(!empty($managers)) { 
							foreach($managers as $m) { 
								if($profile['id_manager'] == $m->id_user) {
									$sel = ' selected="selected" ';
								} else {
									$sel = '';
								}
								?>
								<option value="<?php echo $m->id_user; ?>" <?php echo $sel; ?>><?php echo $m->first_name . ' ' . $m->last_name; ?></option>
							<?php }
						} ?>	
					</select>
				</div>
			</div>
			<div class="control-group">
				<label for="managers" class="control-label">Employment date:</label>
				<div class="controls">
					<input type="text" id="employment_date" name="employment_date" class="datepick" data-rule-required="true" 
					value="<?php if($profile['employment_date'] == '01/01/1970'){ echo date('d-m-Y'); } else { echo $profile['employment_date']; } ?>" /> 
				</div>
			</div>
			
			<div class="control-group">
				<label for="managers" class="control-label"></label>
				<div class="controls">
					<?php 
					if($profile['is_manager'] == 1) { 
						$is_manager_checked = ' checked="checked" ';
						$is_ceo_checked = ' ';
						$is_none_checked = ' ';
					} elseif($profile['is_manager'] == 2) {
						$is_manager_checked = '';
						$is_ceo_checked = ' checked="checked" ';
						$is_none_checked = ' ';
					} else {
						$is_manager_checked = '';
						$is_ceo_checked = ' ';
						$is_none_checked = ' checked="checked" ';
					}
					?>
					
					<label for="is_none"><input type="radio" value="0" name="is_manager" id="is_none" <?php echo $is_none_checked; ?> /> None </label> 
					<label for="is_manager"><input type="radio" value="1" name="is_manager" id="is_manager" <?php echo $is_manager_checked; ?> /> Is manager </label>
					<?php if($allow_ceo == true) { ?>
					<label for="is_ceo"><input type="radio" value="2" name="is_manager" id="is_ceo" <?php echo $is_ceo_checked; ?> /> Is CEO/Root </label> 
					<?php } ?>
						
				</div>
			</div>
			<div class="control-group">
				<label for="managers" class="control-label"></label>
				<div class="controls">
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</div>
		</form>
	</div>	
</div>
			
<script type="text/javascript">

$(document).ready(function() {
	
	$('#no_email').click(function() {
	   if($(this).is(':checked')) {
		   $('#email').attr('disabled', 'disabled');
	   } else {
		   $('#email').removeAttr('disabled');
	   }
   });	
   
	$('#edit_profile').validate({
		rules: {
			email: {
				required: true,
				email: true
			},
			work_number : {
				required:true,
				rangelength:[24,24]
			}
		},
		messages: {
			email: {
				required: "Enter email",
				email: "Enter valid email"
			},
			work_number : {
				required:"Enter work number",
				rangelength:"Enter valid number",
				digits: "Enter valid number"
			}
		}
	});
	
	$('#employment_status').change(function() {
	   
	   var id_status = $(this).val();
	   change_status_by_type(id_status);
	});   
	
	function change_status_by_type(id_status) {
		
	   $('#probation_expire_date_container').hide();
	   $('#probation_message_container').hide();
	   $('#probation_message_container_txt').html('');
	   $('#terminated_expire_date_container').hide();
	   
	   if(id_status == '<?php echo USER_STATUS_PROBATION_NO_ACCESS; ?>') {
		   $('#probation_expire_date_container').show();
		   $('#probation_message_container').show();
		   $('#probation_message_container_txt').html('This employee is on probation and has no access to employee Portal. Please set probation end-date below');
	   } 
	   
	   if(id_status == '<?php echo USER_STATUS_PROBATION_ACCESS; ?>') {
		   $('#probation_expire_date_container').show();
		   $('#probation_message_container').show();
		   $('#probation_message_container_txt').html('This employee is on probation and has access to employee Portal. Please set probation end-date below');
		   //$('#msg_status').show();
		   //$('#msg_status').html('This employee is on probation and has access to employee Portal. Please set probation end-date below');
	   } 
	   
	   if(id_status == '<?php echo USER_STATUS_ACTIVE_NO_ACCESS; ?>') {
		   $('#probation_message_container').show();
		   $('#probation_message_container_txt').html('This employee will not have access to the employee portal. Recommended for semi/unskilled employees');
	   }
	   
	   if(id_status == '<?php echo USER_STATUS_TERMINATED; ?>') {
		   $('#terminated_expire_date_container').show();
	   }
	   
	   /* CHECK IF STATUS IS ACTIVE OR PROBATION ACCESS */
	   if(id_status == '<?php echo USER_STATUS_PROBATION_ACCESS; ?>' || id_status == '<?php echo USER_STATUS_ACTIVE; ?>'){
		   $('#div_no_email').hide();
	   } else {
		   $('#div_no_email').show();
	   }
	   
	   if(id_status != '<?php echo USER_STATUS_ACTIVE; ?>' && id_status != '<?php echo USER_STATUS_PROBATION_ACCESS; ?>') {
		   $('.user_request_data').each(function() {
			   $(this).find('input:radio').removeAttr("checked");
			   $(this).hide();
			   $(this).prev().find('input:radio').click();
		   });
	   } else {
		   $('.user_request_data').each(function() {
			   $(this).show();
		   });
	   }
	   
   } // End func change_status_by_type
   
   change_status_by_type(<?php echo $profile['status']; ?>);
   
   $("#work_number").mask("- (999) - (999) - (9999)");
   //$("#personal_number").val('<?php echo $profile['work_number']; ?>');
   
});

</script>