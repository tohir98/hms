<script>
    window.staffDirConfig = window.staffDirConfig || {};
    window.staffDirConfig.managerView = <?= json_encode(!!$this->user_auth->have_perm(VIEW_EMPLOYEE_RECORDS)); ?>;
    window.staffDirConfig.statuses = <?= json_encode($current_statuses); ?>;
    window.staffDirConfig.perms = <?= json_encode($perms); ?>;

</script>
<link href="/css/angular-busy.min.css" rel="stylesheet" type="text/css"/>
<script src="/js/angular-busy.min.js"></script>
<script src="/js/angular-datatables-all.js" type="text/javascript"></script>
<script src="/js/staff-directory.js" type="text/javascript"></script>
<div class="page-header">
    <div class="pull-left">
        <h1>
            Staff Directory
            <?php if (ALL_MODULE_TOUR > 0 && STAFF_RECORDS_TOUR > 0) { ?>
                <a class="btn btn-small btn-danger" href="javascript:void(0);" onclick="startIntro();">Take a tour</a>
            <?php } ?>
        </h1>
    </div>
</div>
<div ng-app="staffDir" >
    <div ng-controller="ListCtrl" cg-busy="employeesPromise">
        <div class="box">

            <?php if ($this->session->userdata('errors') != '') { ?>
                <br/>
                <div class="alert alert-danger">
                    <button data-dismiss="alert" class="close" type="button">x</button>
                    <?php echo $this->session->userdata('errors'); ?>
                </div>
                <?php
                $this->session->unset_userdata('errors');
            }
            ?>
            <?php if ($this->session->userdata('confirm') != '') {
                ?>
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close" type="button">x</button>
                    <?php echo $this->session->userdata('confirm'); ?>
                </div>
                <?php
                $this->session->unset_userdata('confirm');
            }
            ?>

            <!-- -->
            <div class="box-content nopadding">
                <p>
                <ul class="nav nav-tabs" id="myTab">
                    <li class="active">
                        <a href="#" data-toggle="tab">Current</a>
                    </li>
                    <li ng-if="config.perms.view_records">
                        <a href="<?php echo site_url('employee/view_staff_directory/archive') ?>">Archive</a>
                    </li>
                </ul>
                </p>


            </div>
            <div class="alert alert-error" id="err_div" ng-if="noSelect" ng-cloak="">
                <strong>Select Employee(s)!</strong>
            </div>
            <div class="box-content nopadding">
                <div class="box box-bordered">
                    <div class="box-title" style="padding:10px !important;">
                        <div class="btn-group bulk_action" ng-if="config.managerView">
                            <a class="btn btn-warning dropdown-toggle" data-toggle="dropdown" href="#">Bulk Action <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <?php echo anchor('#', 'Activate Accounts', 'class="activate_employees" eatclick="" ng-click="bulkAction(null, \'#modal-activate\')"'); ?>
                                </li>
                                <!--                                <li>
                                <?php //echo anchor('#', 'Deactivate Accounts', 'class="deactivate_employees"'); ?>
                                                                </li>-->
                                <!--<li><a href="#" id="bulk_action_modal">Reset Passwords</a></li>-->
                                <li ng-if="config.perms.assign_checklist">
                                    <a href="#" ng-click="bulkAction(null, '#checklitst-assign')" eatclick="">Assign Checklist</a>
                                </li>

                            </ul>
                        </div>

                        <form method="post" action="" id="sel" style="margin:0 !important;" class="pull-right">
                            <select chosenfy="" ng-if="config.managerView" ng-model="filter.status_id">
                                <option value="" selected="selected"> All Employees </option>
                                <option ng-repeat="(status, statusId) in config.statuses" value="{{statusId}}">{{status|capitalize}}</option>
                            </select>
                            <h3 ng-if="!config.managerView"><i class="icon-reorder"></i></h3>
                        </form>
                    </div>
                    <div class="box-content nopadding" data-height="600">
                        <table class="table table-user table-hover table-nomargin" datatable="ng" dt-options="dtOptions" dt-column-defs='dtColumnDefs'>
                            <thead>
                                <tr>

                                    <th ng-if="config.managerView" class="with-checkbox sorting_disabled" role="columnheader" style="width: 17px">
                                        <input type="checkbox" ng-click="toggleAll()">
                                    </th>
                                    <th></th>
                                    <th>Name</th>
                                    <th ng-if="config.managerView" class='hidden-1024'>Status</th>
                                    <th class='hidden-480'>Work Details</th>
                                    <th class='hidden-480'>Contact</th>
                                    <th ng-if="config.perms.actions" nowrap id='action_status'>Action</th>
                                </tr>
                            </thead>
                            <tbody ng-cloak="">
                                <tr ng-repeat="employee in employees|filter:filter">
                                    <td ng-if="config.managerView">
                                        <input type="checkbox" value="{{employee.id_user}}" data-id-string="{{employee.id_string}}" name="employees[]" ng-model="employee.is_selected"/>
                                    </td>
                                    <td class="img">
                                        <img ng-src="{{employee.profile_pic}}" width="40" height="40" />
                                    </td>
                                    <td>
                                        <span ng-if="!config.managerView">
                                            {{employee.first_name}} {{employee.last_name}}
                                        </span>
                                        <span ng-if="config.managerView">
                                            <a href="{{employee.view_record_url}}">{{employee.first_name}} {{employee.last_name}}</a>
                                        </span>
                                    </td>
                                    <td class='hidden-1024' ng-if="config.managerView">
                                        {{employee.status}}
                                    </td>
                                    <td class='hidden-480'>
                                        <span class="muted">{{employee.roles ? employee.roles : 'N/A'}}</span><br />
                                        <span class="muted">{{employee.department ? employee.department : 'N/A'}}</span><br />
                                        <span class="muted">{{employee.location ? employee.location : 'N/A'}}</span><br>
                                        <span class="muted">{{employee.job_type ? employee.job_type : 'N/A'}}</span>
                                    </td>
                                    <td class='hidden-480'>
                                        {{employee.email}}
                                        <br/>
                                        <span class="muted" ng-if="validPhone(employee.work_number)">{{employee.work_number}}</span>
                                    </td>
                                    <td >
                                        <div class="btn-group" ng-if="config.perms.actions && employee.id_string !== '<?= $this->user_auth->get('id_string') ?>'">
                                            <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Action <span class="caret"></span></a>
                                            <ul class="dropdown-menu pull-right">
                                                <li><a href="{{employee.view_record_url}}">View Records</a></li>
                                                <li><a href="#" class="change-status-link" ng-click="editStatus(employee)">Edit Status</a></li>
                                                <li ng-if="employee.is_valid_email">
                                                    <a href="#" class="reset-password-link" ng-click="resetPassword(employee)">Reset/Send Password</a>
                                                </li>
                                                <li ng-if="employee.is_valid_email && config.perms.assign_checklist && (employee.status_id == config.statuses.active || config.statuses.probation_access)">
                                                    <a href="#" eatclick="" ng-click="bulkAction([employee], '#checklitst-assign')">Assign Checklist</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <?php if ($this->user_auth->have_perm(CHANGE_EMPLOYEE_STATUS)) { ?>
            <div class="modal hide fade" id="change_employee_status_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                <form method="POST" action="<?php echo site_url('user/employee/change_status'); ?>" id="change_status_frm">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                        <h4 class="modal-title" id="myModalLabel">Edit account status</h4>
                    </div>
                    <div class="modal-body">

                        <input type="hidden" name="id_user_st" id="id_user_st" value="{{currentEmployee.id_string}}" />
                        <input type="hidden" name="employee_full_name" id="employee_full_name" value="{{currentEmployee.first_name}} {{currentEmployee.last_name}}" />
                        <input type="hidden" name="employee_email" id="employee_email" value="{{currentEmployee.email}}" />
                        <p>
                            Change account status for <span id="employee_name">{{currentEmployee.first_name}}</span> 
                        </p>
                        <p>
                            <select name="status" id="status" ng-model="currentEmployee.status_id">
                                <option value=""></option>
                                <option ng-repeat="(status, statusId) in config.statuses" value="{{statusId}}">{{status|capitalize}}</option>
                            </select>
                        </p>	
                        <div class="control-group" ng-if="statusMessages[currentEmployee.status_id]" id="probation_message_container">
                            <div class="controls" id="probation_message_container_txt">
                                {{statusMessages[currentEmployee.status_id]}}
                            </div>
                        </div>

                        <div class="control-group" ng-if="currentEmployee.status_id == config.statuses.probation_no_access || currentEmployee.status_id == config.statuses.probation_access" id="probation_expire_date_container">
                            <label for="" class="control-label">Probation due date:</label>
                            <div class="controls">
                                <input type="text" name="probation_expire_date" id="probation_expire_date" value="" datepick="" />
                            </div>
                        </div>
                        <div ng-if="currentEmployee.status_id == config.statuses.terminated" class="control-group" id="terminated_expire_date_container">
                            <label for="" class="control-label">Effective as of:</label>
                            <div class="controls">
                                <input type="text" name="termination_date" id="termination_date" value="" datepick="" />
                            </div>
                        </div>
                        <div id="email_container" ng-if="currentEmployee.status_id == config.statuses.active || currentEmployee.status_id == config.statuses.probation_access">
                            <div class="control-group">
                                <label for="" class="control-label">Email:</label>
                                <div class="controls">
                                    <input type="text" name="email" id="email" class="" required="" ng-model="currentEmployee.email"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
                        <button data-id_dept="" type="submit" class="btn btn-primary" id="change_status_btn">Change status</button>
                    </div>
                </form>
            </div>
        <?php } ?>

        <?php if ($this->user_auth->have_perm(RESET_EMPLOYEE_PASSWORD)) { ?>
            <div class="modal hide fade" id="reset_employee_password_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                <form method="POST" action="<?php echo site_url('user/employee/reset_password'); ?>" id="reset_password_frm">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                        <h4 class="modal-title" id="myModalLabel">Reset/Send Password</h4>
                    </div>
                    <div class="modal-body">

                        <input type="hidden" name="id_user_strr" id="id_user_strr" value="{{currentEmployee.id_user}}" />
                        <input type="hidden" name="employee_full_namer" id="employee_full_namer" value="{{currentEmployee.first_name}} {{currentEmployee.last_name}}" />
                        <input type="hidden" name="employee_emailr" id="employee_emailr" value="{{currentEmployee.email}}" />
                        <input type="hidden" name="employee_id_string" id="employee_id_string" value="{{currentEmployee.id_string}}" />
                        <p>
                            Reset/Send Password for <span id="employee_namer">{{currentEmployee.first_name}}</span> 
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
                        <button data-id_dept="" class="btn btn-primary" id="reset_password_btn">Reset/Send Password</button>
                    </div>
                </form>
            </div>
        <?php } ?>
        <!-- Activate Modal -->
        <div id="modal-activate" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <form id="bulkActivationFrm" method="post" action="<?= site_url('user/employee/bulkActivateEmployee') ?>">
                <div class="modal-body">
                    <p>Are you sure you want to activate selected employee(s)?</p>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id_users" id="id_users" value="{{buildIdList(selectedEmployees, 'id_user', ',')}}"/>
                    <button class="btn btn-danger" type="button" data-dismiss="modal" aria-hidden="true">No</button>
                    <button type="submit" id="btn_activate" class="btn btn-primary closeme">Yes, activate </button>
                </div>
            </form>
        </div>

        <!-- Checklists Modal -->
        <div id="checklitst-assign" class="modal hide" tabindex="-1" role="dialog" aria-hidden="true" ng-if="config.perms.assign_checklist">
            <form method="get">
                <div class="modal-body">
                    <div class="control-group">
                        <label>Selected Employee(s): </label>
                        <div >
                            <span ng-repeat="employee in selectedEmployees|limitTo:20" style="margin-right: 4px;" class="label">{{employee.first_name}} {{employee.last_name}}</span> <span ng-if="selectedEmployees.length > 20"> and {{selectedEmployees.length - 20}} more</span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label>Select Checklist</label>
                        <div>
                            <select required="" ng-model="data.selectedChecklist" name="ref">
                                <option value=""></option>
                                <?php foreach ($checklists as $checklist): ?>
                                    <option value="<?= $checklist->ref ?>"><?= $checklist->title ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button class="btn btn-warning" type="button" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <button ng-disabled="data.selectedChecklist === ''" type="button"  ng-click="assignChecklist()" id="btn_activate" class="btn btn-primary">Assign Checklist</button>
                </div>
            </form>
        </div>
    </div>

</div>


<?php if ($this->user_auth->have_perm(VIEW_EMPLOYEE_RECORDS)) : ?>
    <script>
        function startIntro() {
            var intro = introJs();
            intro.setOptions({
                steps: [
                    {
                        element: ".table",
                        intro: "This is the Staff Directory which shows a quick overview of current employees within this enterprise. </br>",
                        position: "top"
                    },
                    {
                        element: ".hidden-1024",
                        intro: "The status bar indicates the status of each employee. Probation, Active, Suspended, etc.",
                        position: "right"
                    },
                    {
                        element: "#sel",
                        intro: "You can choose to see selected employees based on employment status.",
                        position: "left"
                    },
                    {
                        element: ".dataTables_filter",
                        intro: "Or start typing a name or location to find a specific employee who meets that criteria.",
                        position: "left"
                    },
                    {
                        element: "#action_status",
                        intro: "Each Action button allows you re/set access passwords. You can also grant or remove access for an employee.",
                        position: "left"
                    },
                    {
                        element: ".bulk_action",
                        intro: "Use this to perform the same action, password reset, activate or deactivate access, for multiple employees at once.",
                        position: "right"
                    }
                ]
            });

            intro.start();
        }
    </script>
<?php else: ?>
    <script>
        function startIntro() {
            var intro = introJs();
            intro.setOptions({
                steps: [
                    {
                        element: ".table",
                        intro: "This is the Staff Directory which shows a quick overview of current employees within this enterprise. </br>",
                        position: "top"
                    },
                    {
                        element: ".dataTables_filter",
                        intro: "Or start typing a name or location to find a specific employee who meets that criteria.",
                        position: "left"
                    }
                ]
            });

            intro.start();
        }
    </script>
<?php endif; ?>