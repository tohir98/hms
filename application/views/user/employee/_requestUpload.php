
<div style="display: block;" id="request_new_template">
    <div class="control-group">
        <label class="control-label" for="">Due date</label>
        <div class="controls">
            <input type="text" class="datepick" name="request_file_date" value="" data-rule-required="true">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="">File name</label>
        <div class="controls">
            <input type="text" value="" data-rule-required="true" name="request_file_name" style="width:500px;">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="">Request description</label>
        <div class="controls">
            <textarea data-rule-required="true" name="request_file_description" style="width:500px;"></textarea>
        </div>
    </div>
</div>	
