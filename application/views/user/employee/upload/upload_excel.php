<?php
/* Check if session message as message */
if ($this->session->userdata('errors') != '') { ?>
<div class="alert alert-danger">
     <button data-dismiss="alert" class="close" type="button">×</button>
     <strong><?php echo $this->session->userdata('errors'); ?></strong>
</div>
<?php $this->session->unset_userdata('errors'); } 
if ($this->session->userdata('confirm') != '') { ?>
<div class="alert alert-success">
     <button data-dismiss="alert" class="close" type="button">×</button>
     <strong><?php echo $this->session->userdata('confirm'); ?></strong>
</div>
<?php $this->session->unset_userdata('confirm'); }  ?>

<div class="page-header">
	<div class="pull-left">
		<h1>Add New Employee(s)</h1>
	</div>
</div>

<div class="row-fluid">
	<div class="span12">
		<div class="box-content">
			<div class="row-fluid">
				<div class="span12">
                    <ul class="nav nav-tabs" id="myTab">
                        <li class="">
                            <a href="<?= site_url('employee/add_new_employee') ?>">New Employee</a>
                        </li>
                        <li class="active">
                            <a href="#" >Employee Bulk Upload</a>
                        </li>
                    </ul>
                    
					<div class="tab-content">
						<!-- Company profile -->
						
						<!-- Bulk users upload -->
						<div class="tab-pane active" id="bulk_users">
                            
							<?php //echo $profile->id_company ?>
							<form id="upload" name="upload" method="post" enctype="multipart/form-data" action="<?php echo site_url('employee/process_upload') ?>" class='form-horizontal form-bordered'>
								<div class="control-group">
									<label for="textfield" class="control-label">File upload</label>
									<div class="controls">
										<div class="fileupload fileupload-new" data-provides="fileupload">
											<div class="input-append">
												<div class="uneditable-input span3"><i class="icon-file fileupload-exists"></i> <span class="fileupload-preview"></span></div><span class="btn btn-file"><span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span><input type="file" name="userfile" /></span><a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
											</div>
										</div>
									</div>
								</div>
                                
                                
                                
								<input type="hidden" name="id_company" id="id_company" value="<?php echo $profile->id_company ?>"/>
								<input type="hidden" name="url" id="url" value="organizations/organizations/manage"/>
								<div class="form-actions">
									<input type="submit" name="submit" value="Upload File" class="btn btn-primary"/>
								</div>
							</form>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>





<script>
  $(function () {
    $('#myTab a:first').tab('show');
  });
  
</script>