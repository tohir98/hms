<style>
    .placeholder{
        height: 300px;
    }
</style>
<!-- Display error/success message -->
<br/>
<?php if ($this->session->userdata('errors') != '') { ?>
<div class="alert alert-danger">
     <button data-dismiss="alert" class="close" type="button">x</button>
     <?php echo $this->session->userdata('errors'); ?>
</div>
<?php $this->session->unset_userdata('errors'); } 
if ($this->session->userdata('confirm') != '') { ?>
<div class="alert alert-success">
     <button data-dismiss="alert" class="close" type="button">x</button>
     <?php echo $this->session->userdata('confirm'); ?>
</div>
<?php $this->session->unset_userdata('confirm'); }  ?>

<!-- Page Header -->
<div class="page-header">
    <div class="pull-left">
        <h1>Pending Approvals</h1>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">
        
    </div>
</div>

<div class="breadcrumbs">
    <ul>
        <li>
            <a href="<?php echo site_url('employee')?>">Staff Records</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Pending Approvals</a>
        </li>
    </ul>
</div>



    
<div class="box box-bordered">
		<div class="box-title" style="padding-right:5px;">
			<h3>
				<div class="btn-group">
                    <a class="btn btn-warning dropdown-toggle" data-toggle="dropdown" href="#">Bulk Action <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><?php echo anchor('#', 'Approve', 'class="approve"'); ?></li>
                        <li><?php echo anchor('#', 'Decline', 'class="decline"'); ?></li>
                    </ul>
                </div>
			</h3>
		</div>
        <?php //var_dump($pending_approvals);?>
		<div class="box-content-padless">
            <table class='table table-striped dataTable '>
                <thead>
                    <tr class='thefilter'>
                        <th class='with-checkbox'><input type="checkbox" name="check_all" id="check_all"></th>
                        <th>Employee</th>
                        <th>Data Submitted</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                
                <?php if (!empty($pending_approvals)) { ?>
                <tbody>
                    <?php foreach ($pending_approvals as $p_approval) { ?>
                    <tr>
                        <td><input type="checkbox" class="_item_checkbox" name="check"></td>
                        <td><?php echo $p_approval['first_name'] .' '. $p_approval['last_name']; ?></td>
                        <td>
                            <?php $sub = $p_approval['data_submitted']; 
 
                                foreach ($sub as $b) { ?>
                            <span class='label'><?= $requests[$b->request_type]; ?> </span>
                             <?php   } ?>
                        </td>
                        <td>Pending</td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-warning dropdown-toggle" data-toggle="dropdown" href="#">Action <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><?php echo anchor('employee/request_details/'.$p_approval['id_temp_request'], 'Review Details', ''); ?></li>
<!--                                    <li><?php echo anchor('#', 'Approve', 'class="approve"'); ?></li>
                                    <li><?php echo anchor('#', 'Decline', 'class="decline"'); ?></li>-->
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
                <?php } ?>
            </table>
		</div>
	</div>
