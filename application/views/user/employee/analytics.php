<style>
    .placeholder{
        height: 300px;
    }
</style>
<!-- Display error/success message -->
<br/>
<?php if ($this->session->userdata('errors') != '') { ?>
    <div class="alert alert-danger">
        <button data-dismiss="alert" class="close" type="button">x</button>
        <?php echo $this->session->userdata('errors'); ?>
    </div>
    <?php $this->session->unset_userdata('errors');
}
if ($this->session->userdata('confirm') != '') {
    ?>
    <div class="alert alert-success">
        <button data-dismiss="alert" class="close" type="button">x</button>
    <?php echo $this->session->userdata('confirm'); ?>
    </div>
    <?php $this->session->unset_userdata('confirm');
} ?>

<!-- Page Header -->
<div class="page-header">
    <div class="pull-left">
        <h1>Analytics</h1>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">

    </div>
</div>

<div class="box">

    <div class="box-content nopadding">
        <p>
        <ul class="nav nav-tabs" id="myTab">
            <li class="active">
                <a href="#overview" data-toggle="tab">Overview</a>
            </li>
            <li>
                <a href="#report" data-toggle="tab">Reports</a>
            </li>
        </ul>
        </p>

        <div class="tab-content"> 
            <div class="tab-pane active" id="overview">
                <div class="box-content-padless">

                    <form class="form-inline" id="frm_upload" name="frm_upload" method="post">
                        <div class="row-fluid">
                            <div class="span3">
                                <div class="control-group">
                                    <div class="controls controls-row">
                                        <select class='select2-me input-xlarge' name="cbo_dept" id="cbo_dept">
                                            <option value="0" selected="selected"> All Departments</option>
                                            <?php
                                            if (!empty($departments)) {
                                                foreach ($departments as $d) {
                                                    ?>
                                                    <option value="<?php echo $d->deptid ?>"><?php echo $d->departments ?></option>
    <?php }
} ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="span3">
                                <div class="control-group">
                                    <div class="controls controls-row">
                                        <select id="cbo_location" name="cbo_location" class='select2-me input-xlarge'>
                                            <option value="0">All Locations</option>
<?php
if (!empty($locations)) {
    foreach ($locations as $l) {
        ?>
                                                    <option value="<?php echo $l->id_location ?>"><?php echo $l->location_tag ?></option>
    <?php }
} ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="span2">
                                <div class="control-group">
                                    <select id="cbo_year" name="cbo_year" class='select2-me input'>
<?php for ($y = date('Y'); $y >=2014; --$y) { ?>
                                            <option value="<?php echo $y ?>"><?php echo $y ?></option>
<?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="span2">
                                <div class="control-group">
                                    <div class="controls controls-row">
                                        <button type="button" name="btn_go" id="btn_go" class="btn btn-primary ladda-button pull-left" data-style="contract">Go</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div> <!-- End of search -->

                <div class="row-fluid">
                    <div class="span6">
                        <div class="box box-bordered">
                            <div class="box-title">
                                <h3>
                                    <i class="icon-bar-chart"></i>
                                    Total Head Count
                                </h3>
                            </div>
                            <div class="box-content">
                                <div class="statistic-big">
                                    <div class="bottom">
                                        <div id="emp_count_container" class="placeholder"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="span6">
                        <div class="box box-bordered">
                            <div class="box-title">
                                <h3>
                                    <i class="icon-bar-chart"></i>
                                    Employee Retention
                                </h3>
                            </div>
                            <div class="box-content">
                                <div class="statistic-big">
                                    <div class="bottom">
                                        <div id="emp_retention_container" class="placeholder"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- End span6 -->
                </div><!-- End row-fluid -->

                <div class="row-fluid">
                    <div class="span6">
                        <div class="box box-bordered">
                            <div class="box-title">
                                <h3>
                                    <i class="icon-bar-chart"></i>
                                    New Hires
                                </h3>
                            </div>
                            <div class="box-content">
                                <div class="statistic-big">
                                    <div class="bottom">
                                        <div id="new_hire_container" class="placeholder"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="span6">
                        <div class="box box-bordered">
                            <div class="box-title">
                                <h3>
                                    <i class="icon-bar-chart"></i>
                                    Gender Diversity
                                </h3>
                            </div>
                            <div class="box-content">
                                <div class="statistic-big">
                                    <div class="bottom">
                                        <div id="gender_diversity_container" class="placeholder"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- End span6 -->
                </div><!-- End row-fluid -->


            </div><!-- End Div Overview -->

            <div class="tab-pane" id="report">

                <div class="box box-bordered">
                    <div class="box-title">
                        <h3>Reports</h3>
                    </div>

                    <div class="box-content nopadding" id="data_container">
                        <table class="table table-hover table-nomargin dataTable" id="tabjgh">
                            <thead>
                                <tr class=''>
                                    <th>Report</th>
                                    <th class='hidden-480'>&nbsp;</th>
                                </tr>

                            </thead>
                            <tbody>

                                <tr>
                                    <td>Personnel Record Audit</td>
                                    <td> 
                                        <div class="btn-group">
                                            <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">Action <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo site_url('employee/personnel_report') ?>">Run Report</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>

                        </table>
                    </div>
                </div>	

            </div> <!-- End Div report -->

        </div>
    </div>    
</div>


<script>

    var ticks = [[0, "Jan"], [1, "Feb"], [2, "Mar"], [3, "Apr"], [4, "May"], [5, "Jun"], [6, "Jul"], [7, "Aug"], [8, "Sep"], [9, "Oct"], [10, "Nov"], [11, "Dec"]];

    var options = {
                    series: {
                            bars: {
                                    show: true
                            }
                    },
                    bars: {
                            align: "center",
                            barWidth: 0.5
                    },
                    xaxis: {
                            axisLabel: "World Cities",
                            axisLabelUseCanvas: true,
                            axisLabelFontSizePixels: 12,
                            axisLabelFontFamily: 'Verdana, Arial',
                            axisLabelPadding: 10,
                            ticks: ticks
                    },
                    yaxis: {
                            //axisLabel: "Average Temperature",
            min: 0,
                            axisLabelUseCanvas: true,
                            axisLabelFontSizePixels: 12,
                            axisLabelFontFamily: 'Verdana, Arial',
                            axisLabelPadding: 3,
                            tickFormatter: function (v, axis) {
                                    return v + '%';
                            }
                    },
                    legend: {
                            noColumns: 0,
                            labelBoxBorderColor: "#000000",
                            position: "nw"
                    },
                    grid: {
                            hoverable: true,
                            borderWidth: 2,
                            backgroundColor: {colors: ["#ffffff", "#EDF5FF"]}
                    },
        colors: ["#5482FF", "#FF0000"]
            };

    var options_count = {
                    series: {
                            bars: {
                                    show: true
                            }
                    },
                    bars: {
                            align: "center",
                            barWidth: 0.5
                    },
                    xaxis: {
                            axisLabelUseCanvas: true,
                            axisLabelFontSizePixels: 12,
                            axisLabelFontFamily: 'Verdana, Arial',
                            axisLabelPadding: 10,
                            ticks: ticks
                    },
                    yaxis:
                {
                    min: 0,
                    max: Math.round('<?php echo $emp_count + 5 ?>'),
                    //max: 24,  
                    tickSize: 5
                },
                    legend: {
                            noColumns: 0,
                            labelBoxBorderColor: "#000000",
                            position: "nw"
                    },
                    grid: {
                            hoverable: true,
                            borderWidth: 2,
                            backgroundColor: {colors: ["#ffffff", "#EDF5FF"]}
                    },
        colors: ["#5482FF", "#FF0000"]
            };

    var opti = {
        xaxis: {
            minTickSize: 1,
            ticks: ticks
        },
                    yaxis: {
                            //axisLabel: "Average Temperature",
            min: 0,
                            axisLabelUseCanvas: true,
                            axisLabelFontSizePixels: 12,
                            axisLabelFontFamily: 'Verdana, Arial',
                            axisLabelPadding: 3,
                            tickFormatter: function (v, axis) {
                                    return v + '%';
                            }
                    },
                    grid: {
                            hoverable: true,
                            borderWidth: 2,
                            backgroundColor: {colors: ["#ffffff", "#EDF5FF"]}
                    },
        series: {
            bars: {
                show: true,
                barWidth: .9,
                align: "center"
            },
            stack: true
        },
        colors: ["#5482FF", "#FF0000"]
    };




    function gd(year, month, day) {
        return new Date(year, month, day).getTime();
    }

    var previousPoint = null,
            previousLabel = null;



    $.fn.UseTooltipp = function () {
        $(this).bind("plothover", function (event, pos, item) {
            if (item) {
                if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
                    previousPoint = item.dataIndex;
                    previousLabel = item.series.label;
                    $("#tooltip").remove();

                    var x = item.datapoint[0];
                    var y = item.datapoint[1];

                    var color = item.series.color;

                    showTooltip(item.pageX,
                            item.pageY,
                            color, "<strong>" + item.series.xaxis.ticks[x].label + " : <strong>" + y + "</strong>");
                }
            } else {
                $("#tooltip").remove();
                previousPoint = null;
            }
        });
    };  // End UseTooltipp

    $.fn.UseTooltip = function () {
        $(this).bind("plothover", function (event, pos, item) {
            if (item) {
                if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
                    previousPoint = item.dataIndex;
                    previousLabel = item.series.label;
                    $("#tooltip").remove();

                    var x = item.datapoint[0];
                    var y = item.datapoint[1];

                    var color = item.series.color;

                    showTooltip(item.pageX,
                            item.pageY,
                            color, "<strong>" + item.series.xaxis.ticks[x].label + " : <strong>" + Math.round(y) + '%' + "</strong>");
                }
            } else {
                $("#tooltip").remove();
                previousPoint = null;
            }
        });
    };

    function showTooltip(x, y, color, contents) {
        $('<div id="tooltip">' + contents + '</div>').css({
            position: 'absolute',
            display: 'none',
            top: y - 40,
            left: x - 120,
            border: '2px solid ' + color,
            padding: '3px',
            'font-size': '9px',
            'border-radius': '5px',
            'background-color': '#fff',
            'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
            opacity: 0.9
        }).appendTo("body").fadeIn(200);
    }

    function headCount(department, location, year) {

        $.ajax({
            url: '<?php echo site_url('employee/emp_head_count_json'); ?>/' + department + '/' + location + '/' + year,
            method: 'POST',
            beforeSend: function (html) {
                $('#emp_count_container').html('<div class="loaderBox" style="margin-top:10px; margin-buttom:10px"><p align="center"><img src="/img/gif-load.gif" ></p></div>');
            },
            success: function (data1) {

                var data3 = $.parseJSON(data1);
                 
                $.plot($('#emp_count_container'), data3, options_count);
                $("#emp_count_container").UseTooltipp();

            }
        });
    }   // End headCount

    function newHires(department, location, year) {

        $.ajax({
            url: '<?php echo site_url('employee/new_hires_json'); ?>/' + department + '/' + location + '/' + year,
            method: 'POST',
            beforeSend: function (html) {
                $('#new_hire_container').html('<div class="loaderBox" style="margin-top:10px; margin-buttom:10px"><p align="center"><img src="/img/gif-load.gif" ></p></div>');
            },
            success: function (data1) {

                var data3 = $.parseJSON(data1);
                 
                $.plot($('#new_hire_container'), data3, options_count);
                $("#new_hire_container").UseTooltipp();

            }
        });
    }   // End newHires


    function empRetention(department, location, year) {

        $.ajax({
            url: '<?php echo site_url('employee/emp_retention_json'); ?>/' + department + '/' + location + '/' + year,
            method: 'POST',
            beforeSend: function (html) {
                $('#emp_retention_container').html('<div class="loaderBox" style="margin-top:10px; margin-buttom:10px"><p align="center"><img src="/img/gif-load.gif" ></p></div>');
            },
            success: function (data1) {

                var data3 = $.parseJSON(data1);
                 
                $.plot($('#emp_retention_container'), data3, options);
                $("#emp_retention_container").UseTooltip();

            }
        });
    }   // End empRetention

    function genderDiversity(department, location, year) {


        $.ajax({
            url: '<?php echo site_url('employee/gender_diversity_json'); ?>/' + department + '/' + location + '/' + year,
            method: 'POST',
            beforeSend: function (html) {
                $('#gender_diversity_container').html('<div class="loaderBox" style="margin-top:10px; margin-buttom:10px"><p align="center"><img src="/img/gif-load.gif" ></p></div>');
            },
            success: function (data1) {

                var series = $.parseJSON(data1);
                 
                $.plot("#gender_diversity_container", series, opti);
                $("#gender_diversity_container").UseTooltipp();

            }
        });
    }   // End genderDiversity



    $(document).ready(function () {

        var yy = '<?php echo date('Y') ?>';
        setTimeout(function () {

            headCount(0, 0, yy);
            empRetention(0, 0, yy);
            newHires(0, 0, yy);
            genderDiversity(0, 0, yy);

        }, 200);


        $('#btn_go').click(function () {

            var dept = $('#cbo_dept').val();
            var loc = $('#cbo_location').val();
            var yy = $('#cbo_year').val();

            $('#btn_go').attr('disabled', true);
            headCount(dept, loc, yy);
            empRetention(dept, loc, yy);
            newHires(dept, loc, yy);
            genderDiversity(dept, loc, yy);
            $('#btn_go').attr('disabled', false);

        });

    });
</script>
