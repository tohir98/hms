<div class="box box-color box-bordered primary employee_info_box" id="benefits_details_container" style="display:none;">
    <div class="box-title">
        <h3>Benefits details</h3>
        <ul class="nav nav-pills pull-right" style="margin-bottom:5px;">
            <?php if ($this->user_auth->have_perm(EDIT_REQUEST_EMPLOYEE_RECORDS)) { ?>
                <li class="dropdown pull-right">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">Edit <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo site_url('employee/edit_benefits/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)); ?>" class="popup">Edit Now</a></li>
                        <?php if ($send_request == true) { ?>
                            <li><a href="#" id="request_employee_benefits">Request from Employee</a></li>
                        <?php } ?>	
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </div>
    <div class="box-content">
        <div style="margin-top:20px;">
            <table class="table">
                <thead>
                    <tr>
                        <?php foreach ($benefits as $title => $value) { ?>
                            <th align="right"><strong><?php echo $title; ?>:</strong></th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <?php foreach ($benefits as $title => $value) { ?>
                            <td><?php echo $value; ?></td>
                        <?php } ?>
                    </tr>
                </tbody>                    
            </table>
        </div>

    </div>
</div>
<?php if ($this->user_auth->have_perm(EDIT_REQUEST_EMPLOYEE_RECORDS)) { ?>
    <div class="modal hide fade" id="request_from_employee_benefits_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h4 class="modal-title" id="myModalLabel">Request from Employee</h4>
        </div>
        <div class="modal-body">
            <?php if ($request_message != '') { ?>
                <div class="alert alert-info">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <strong><?php echo $request_message; ?></strong>
                </div>
            <?php } ?>
            <form method="POST" action="<?php echo site_url('employee/request_benefits/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)); ?>" id="benefits_request_frm">
                <p>
                    Instruction
                    <br />
                    <textarea data-rule-required="true" id="benefits_request_description" name="benefits_request_description" style="width:500px;" class="ui-wizard-content"></textarea>
                    <span class="error" id="error_benefits_request_description" style="display:none;">
                        <br />Enter Instruction
                    </span>
                    <br />
                    Due date
                    <br />
                    <input type="text" name="benefits_due_date" id="benefits_due_date" class="datepick" value="" />
                    <span class="error" id="error_benefits_due_date" style="display:none;">
                        <br />Enter due date
                    </span>
                </p>
            </form>
        </div>
        <div class="modal-footer">
            <button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
            <button data-id_dept="" class="btn btn-primary" id="request_from_employee_benefits_btn">Send request</button>
        </div>
    </div>
    <script>
        $(document).ready(function () {

            $('#request_employee_benefits').click(function () {
                $('#request_from_employee_benefits_modal').modal('show');
                return false;
            });

            $('#request_from_employee_benefits_btn').click(function () {

                var send = true;

                if ($('#benefits_request_description').val() == '') {
                    $('#error_benefits_request_description').show();
                    send = false;
                } else {
                    $('#error_benefits_request_description').hide();
                }

                if ($('#benefits_due_date').val() == '') {
                    $('#error_benefits_due_date').show();
                    send = false;
                } else {
                    $('#error_benefits_due_date').hide();
                }

                if (send == true) {
                    $('#benefits_request_frm').submit();
                } else {
                    return false;
                }

            });

        });
    </script>	
<?php } ?>