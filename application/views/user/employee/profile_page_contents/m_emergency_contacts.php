<div class="box box-color box-bordered primary employee_info_box" id="emergency_container" style="display:none;">
	<div class="box-title">
		<h3>Emergency contacts</h3>
	</div>
	<div class="box-content">
		<div style="border-bottom:1px solid #CCCCCC;">
			<ul class="nav nav-pills" style="margin-bottom:5px;">
				<li class="active">
					<h4>Contacts</h4>
				</li>
			</ul>
		</div>
		<div style="margin-top:20px;">
			<h5>Contact 1</h5>
			<table cellpadding="3">
				<?php foreach($e_contacts[1] as $title => $value) { ?>
					<tr>
						<td align="right"><strong><?php echo $title; ?>:</strong></td>
						<td><?php echo $value; ?></td>
					</tr>
				<?php } ?>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<h5>Contact 2</h5>
			<table cellpadding="3">
				<?php foreach($e_contacts[2] as $title => $value) { ?>
					<tr>
						<td align="right"><strong><?php echo $title; ?>:</strong></td>
						<td><?php echo $value; ?></td>
					</tr>
				<?php } ?>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</div>
	</div>
</div>