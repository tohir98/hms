<div class="box box-color box-bordered primary employee_info_box" id="work_info_personal_info_container">
    <div class="box-title">
        <h3>
            Profile
        </h3>
    </div>
    <div class="box-content">

        <div style="border-bottom:1px solid #CCCCCC;">
            <ul class="nav nav-pills" style="margin-bottom:5px;">
                <li class="active">
                    <h4>Required info</h4>
                </li>
            </ul>
        </div>
        <div style="margin-top:20px;" id="sec_work_info">
            <table cellpadding="3">
                <tr>
                    <td align="right"><strong>Employee Name:</strong></td>
                    <td><?php echo $ri['employee_name']; ?></td>
                    <td align="right">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?= site_url('employee/edit_employee_name/' . $ri['staff_id']) ?>" class="edit_employee">
                            edit
                        </a>
                    </td>
                </tr>

                <tr>
                    <td align="right"><strong>Email:</strong></td>
                    <td>
                        <?php
                        $arr = explode('.', $ri['email']);
                        if (count($arr) > 1) :
                            if (strtolower($arr[1]) != 'user@talentbase') :
                                echo $ri['email'];
                            endif;
                        endif;
                        ?>
                    </td>
                    <td align="right">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php if ($ri['isParent'] == FALSE and $ri['isCompanyAdmin'] == TRUE): ?>

                        <?php else: ?>
                            <a href="<?= site_url('employee/edit_staff_email/' . $ri['staff_id']) ?>" class="edit_employee">
                                edit
                            </a>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td align="right"><strong>Staff ID:</strong></td>
                    <td><?php
                        if ($ri['id_staff'] !== '0' && $ri['id_staff'] !== '') {
                            echo $ri['id_staff'];
                        }
                        ?></td>
                    <td align="right">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?= site_url('employee/edit_staff_id/' . $ri['staff_id']) ?>" class="edit_employee">
                            edit
                        </a>
                    </td>
                </tr>
                <tr>
                    <td align="right"><strong>Access level:</strong></td>
                    <td><?php echo $ri['access_level']; ?></td>
                    <td align="right">&nbsp; </td>
                </tr>
                <tr>
                    <td align="right"><strong>Employment Status:</strong></td>
                    <td><?php echo $ri['employement_status']; ?></td>
                    <td align="right">&nbsp; </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
        <div style="border-bottom:1px solid #CCCCCC;">
            <ul class="nav nav-pills" style="margin-bottom:5px;">
                <li class="active">
                    <h4>Work info</h4>
                </li>
                <?php if ($this->user_auth->have_perm(EDIT_REQUEST_EMPLOYEE_RECORDS)) { ?>
                    <li class="dropdown pull-right" id="sec_edit_work_info">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">Edit <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo site_url('employee/edit_profile_work_info/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)); ?>">Edit Now</a></li>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <div style="margin-top:20px;" id="sec_work_info">
            <table cellpadding="3">
                <?php foreach ($wi as $title => $value) { ?>
                    <tr>
                        <td align="right"><strong><?php echo $title; ?>:</strong></td>
                        <td><?php echo $value; ?></td>
                    </tr>
                <?php } ?>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
        <div style="border-bottom:1px solid #CCCCCC;">
            <ul class="nav nav-pills" style="margin-bottom:5px;">
                <li class="active">
                    <h4>Personal info</h4>
                </li>
                <?php if ($this->user_auth->have_perm(EDIT_REQUEST_EMPLOYEE_RECORDS)) { ?>
                    <li class="dropdown pull-right" id="sec_edit_pers_info">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">Edit <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo site_url('employee/edit_profile_personal_info/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)); ?>">Edit Now</a></li>
                            <?php if ($send_request == true) { ?>
                                <li><a href="#" id="request_employee_profile_pers_info">Request from Employee</a></li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <div style="margin-top:20px;" id="sec_personal_info">
            <table cellpadding="3">
                <?php foreach ($pi as $title => $value) { ?>
                    <tr>
                        <td align="right"><strong><?php echo $title; ?>:</strong></td>
                        <td><?php echo $value; ?></td>
                    </tr>
                <?php } ?>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
    </div>
</div>
<?php if ($this->user_auth->have_perm(EDIT_REQUEST_EMPLOYEE_RECORDS)) { ?>
    <div class="modal hide fade" id="request_from_employee_profile_pers_info_model1" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h4 class="modal-title" id="myModalLabel">Request from Employee</h4>
        </div>
        <div class="modal-body">
            <?php if ($request_message != '') { ?>
                <div class="alert alert-info">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <strong><?php echo $request_message; ?></strong>
                </div>
            <?php } ?>
            <form method="POST" action="<?php echo site_url('employee/request_profile_personal_info/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)); ?>" id="profile_pers_info_request_frm">
                <p>
                    Description
                    <br />
                    <textarea data-rule-required="true" id="request_from_employee_profile_desc" name="request_from_employee_profile_desc" style="width:500px;" class="ui-wizard-content"></textarea>
                    <span class="error" id="error_request_from_employee_profile_desc" style="display:none;">
                        <br />Enter description
                    </span>
                    <br />
                    Due date
                    <br />
                    <input type="text" name="request_from_employee_profile_due_date" id="request_from_employee_profile_due_date" class="datepick" />
                    <span class="error" id="error_request_from_employee_profile_due_date" style="display:none;">
                        <br />Enter due date
                    </span>
                </p>
            </form>
        </div>
        <div class="modal-footer">
            <button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
            <button data-id_dept="" class="btn btn-primary" id="request_from_employee_info_btn">Send request</button>
        </div>
    </div>

    <div class="modal hide fade" id="request_from_employee_profile_pers_info_model" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h4 class="modal-title" id="myModalLabel">Request from Employee</h4>
        </div>
        <div class="modal-body">
            <form id="request_frm" class="form" method="POST" action="<?= site_url('user/employee/send_request') ?>">
                <input type="hidden" name="id_string" id="id_string" value="<?= $this->uri->segment(3) ?>" />


                <div class="control-group">
                    <div class="controls">
                        <div class="check-line">
                            <input type="checkbox" id="gender" name="gender" class='icheck-me' data-skin="square" data-color="blue" value="<?= $request_fields[0]['id_gender_'] ?>" checked=""> <label class='inline' for="gender">&nbsp;Gender</label> 
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <div class="controls">
                        <div class="check-line">
                            <input type="checkbox" id="gender" name="marital_status" class='icheck-me' data-skin="square" data-color="blue" value="<?= $request_fields[0]['id_marital_status_'] ?>" checked=""> <label class='inline' for="marital_status">&nbsp;Marital Status</label> 
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <div class="controls">
                        <div class="check-line">
                            <input type="checkbox" id="birthday" name="birthday" class='icheck-me' data-skin="square" data-color="blue" value="<?= $request_fields[0]['birthday_'] ?>" checked=""> <label class='inline' for="c6">&nbsp;Birthday</label> 
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <div class="controls">
                        <div class="check-line">
                            <input type="checkbox" id="id_country" name="id_country" class='icheck-me' data-skin="square" data-color="blue" value="<?= $request_fields[0]['id_country_'] ?>" checked=""> <label class='inline' for="id_country">&nbsp;Country of Origin</label> 
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <div class="controls">
                        <div class="check-line">
                            <input type="checkbox" id="personal_phone" name="personal_phone" class='icheck-me' data-skin="square" data-color="blue" value="<?= $request_fields[0]['personal_phone_'] ?>" checked=""> <label class='inline' for="personal_phone">&nbsp;Personal Phone</label> 
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <div class="controls">
                        <div class="check-line">
                            <input type="checkbox" id="home_address" name="home_address" class='icheck-me' data-skin="square" data-color="blue" value="<?= $request_fields[0]['home_address_'] ?>" checked=""> <label class='inline' for="home_address">&nbsp;Home Address</label> 
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <div class="controls">
                        <div class="check-line">
                            <input type="checkbox" id="city" name="city" class='icheck-me' data-skin="square" data-color="blue" value="<?= $request_fields[0]['city_'] ?>" checked=""> <label class='inline' for="city">&nbsp;City</label> 
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <div class="controls">
                        <div class="check-line">
                            <input type="checkbox" id="id_state" name="id_state" class='icheck-me' data-skin="square" data-color="blue" value="<?= $request_fields[0]['id_state_'] ?>" checked=""> <label class='inline' for="id_state">&nbsp;State</label> 
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <div class="controls">
                        <div class="check-line">
                            <input type="checkbox" id="id_nationality" name="id_nationality" class='icheck-me' data-skin="square" data-color="blue" value="<?= $request_fields[0]['id_nationality_'] ?>" checked> <label class='inline' for="id_nationality">&nbsp;Nationality</label> 
                        </div>
                    </div>
                </div>

                <!--            <div class="control-group">
                                <div class="controls">
                                    <div class="check-line">
                                        <input type="checkbox" id="emergency1" name="emergency1" class='icheck-me' data-skin="square" data-color="blue" value="<?= $request_fields[0]['emergency1_'] ?>" checked=""> <label class='inline' for="emergency1">&nbsp;Emergency Contact 1</label> 
                                    </div>
                                </div>
                            </div>
                           
                            <div class="control-group">
                                <div class="controls">
                                    <div class="check-line">
                                        <input type="checkbox" id="emergency2" name="emergency2" class='icheck-me' data-skin="square" data-color="blue" value="<?= $request_fields[0]['emergency2_'] ?>" checked=""> <label class='inline' for="emergency2">&nbsp;Emergency Contact 2</label> 
                                    </div>
                                </div>
                            </div>
                
                            <div class="control-group">
                                <div class="controls">
                                    <div class="check-line">
                                        <input type="checkbox" id="pension_details" name="pension_details" class='icheck-me' data-skin="square" data-color="blue" value="<?= $request_fields[0]['pension_details_'] ?>" checked=""> <label class='inline' for="c6">&nbsp;Benefit Details</label> 
                                    </div>
                                </div>
                            </div>-->


                <div class="control-group">
                    <label>Due date</label>
                    <div class="controls">
                        <input type="text" name="request_due_date" id="request_due_date" class="pickDate" />
                        <span class="error" id="error_request_due_date" style="display:none;">
                            Enter due date
                        </span>
                    </div>
                </div>


            </form>
        </div>



        <div class="modal-footer">
            <button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
            <button data-id_dept="" class="btn btn-primary" id="request_from_employee_info_btn1">Send request</button>
        </div>

    </div>

    <div class="modal hide fade" id="edit_employee_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" >

    </div>

    <script>
        $(document).ready(function () {

            $('#request_employee_profile_pers_info').click(function () {
                $('#request_from_employee_profile_pers_info_model').modal('show');
                return false;
            });

            $('#request_from_employee_info_btn').click(function () {
                var content = $.trim($('#request_from_employee_profile_desc').val());
                var due_date = $.trim($('#request_from_employee_profile_due_date').val());
                var send = true;

                if (content == '') {
                    $('#error_request_from_employee_profile_desc').show();
                    send = false;
                } else {
                    $('#error_request_from_employee_profile_desc').hide();
                }

                if (due_date == '') {
                    $('#error_request_from_employee_profile_due_date').show();
                    send = false;
                } else {
                    $('#error_request_from_employee_profile_due_date').hide();
                }

                if (send == true) {
                    $('#profile_pers_info_request_frm').submit();
                } else {
                    return false;
                }

            });


            $('#request_from_employee_info_btn1').click(function () {

                var due_date = $.trim($('#request_due_date').val());
                var send = true;

                if (due_date === '') {
                    $('#error_request_due_date').show();
                    send = false;
                } else {
                    $('#error_request_due_date').hide();
                }

                if (send === true) {
                    $('#request_from_employee_info_btn1').attr('disabled', true);
                    $('#request_frm').submit();
                } else {
                    return false;
                }

            });

            $('.edit_employee').click(function (eve) {
                eve.preventDefault();
                $('#edit_employee_modal').modal('show').fadeIn();
                $('#edit_employee_modal').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

                var page = $(this).attr("href");
                $.get(page, function (html) {

                    $('#edit_employee_modal').html('');
                    $('#edit_employee_modal').html(html).show();

                });
            });




        });
    </script>	

<?php } ?>