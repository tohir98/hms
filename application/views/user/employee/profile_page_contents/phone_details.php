<div class="box box-color box-bordered primary employee_info_box" id="phone_details_container" style="display:none;">
	<div class="box-title">
		<h3>Phone details</h3>
	</div>
	<div class="box-content">
		<div style="border-bottom:1px solid #CCCCCC;">
			<ul class="nav nav-pills" style="margin-bottom:5px;">
				<li class="active">
					<h4>Phone details</h4>
				</li>
				<?php if($this->user_auth->have_perm(EDIT_REQUEST_EMPLOYEE_RECORDS)) { ?>
				<li class="dropdown pull-right">
					<a data-toggle="dropdown" class="dropdown-toggle" href="#">Edit <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo site_url('employee/edit_phone_details/'.$this->uri->segment(3).'/'.$this->uri->segment(4)); ?>">Edit Now</a></li>
                        <li><a href="#" id="add_employee_phone_details" >Add Phone </a></li>
						<?php if($send_request == true) { ?>
						<li><a href="#" id="request_employee_bank_details">Request from Employee</a></li>
						<?php } ?>	
					</ul>
				</li>
				<?php } ?>
			</ul>
		</div>
		<div style="margin-top:20px;">
            <?php foreach($phone_details as $phone ) { ?>
			<table cellpadding="3">
				<?php
                foreach($phone as $title => $value) { ?>
					<tr>
						<td align="right"><strong><?php echo $title; ?>:</strong></td>
						<td><?php echo $value; ?></td>
					</tr>
				<?php } ?>
				<tr>
					<td colspan="2"><hr/></td>
				</tr>
			</table>
            <?php } ?>
		</div>
	</div>
</div>
<?php if($this->user_auth->have_perm(EDIT_REQUEST_EMPLOYEE_RECORDS)) { ?>
<div class="modal hide fade" id="request_from_employee_bank_details_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h4 class="modal-title" id="myModalLabel">Request from Employee</h4>
	</div>
	<div class="modal-body">
		<?php if($request_message != '') { ?>
			<div class="alert alert-info">
				<button data-dismiss="alert" class="close" type="button">×</button>
				<strong><?php echo $request_message; ?></strong>
			</div>
		<?php } ?>
		<form method="POST" action="<?php echo site_url('employee/request_bank_details/'.$this->uri->segment(3).'/'.$this->uri->segment(4)); ?>" id="bank_request_frm">
		<p>
			Instruction
			<br />
			<textarea data-rule-required="true" id="bank_request_description" name="bank_request_description" style="width:500px;" class="ui-wizard-content"></textarea>
			<span class="error" id="error_bank_request_description" style="display:none;">
				<br />Enter Instruction
			</span>
			<br />
			Due date
			<br />
			<input type="text" name="bank_due_date" id="bank_due_date" class="datepick" value="" />
			<span class="error" id="error_bank_due_date" style="display:none;">
				<br />Enter due date
			</span>
		</p>
		</form>
	</div>
	<div class="modal-footer">
		<button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
		<button data-id_dept="" class="btn btn-primary" id="request_from_employee_bank_btn">Send request</button>
	</div>
</div>
<script>
$(document).ready(function() {

$('#request_employee_bank_details').click(function() {
	$('#request_from_employee_bank_details_modal').modal('show');
	return false;
});

$('#request_from_employee_bank_btn').click(function() {
	
	var send = true;
	
	if($('#bank_request_description').val() == '') {
		$('#error_bank_request_description').show();
		send = false;
	} else {
		$('#error_bank_request_description').hide();
	}
	
	if($('#bank_due_date').val() == '') { 
		$('#error_bank_due_date').show();
		send = false;
	} else {
		$('#error_bank_due_date').hide();
	}
	
	if(send == true) {
		$('#bank_request_frm').submit();
	} else {
		return false;
	}
		
});

});
</script>	
<?php } ?>


<div class="modal hide fade" id="insert_new_employee_phone_details_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h4 class="modal-title" id="myModalLabel">Add New Employee Phone</h4>
	</div>
	<div class="modal-body">
		
		<form method="POST" class="form-horizontal form-bordered" action="<?php echo site_url('employee/insert_employee_phone_details/'.$this->uri->segment(3).'/'.$this->uri->segment(4)); ?>" id="phone_details_frm">
		<p>
			Phone Type
			<br />
            <select id="phone_type">
                <option> Work </option>
                <option> Home </option>
            </select>
			<span class="error" id="error_phone_type" style="display:none;">
				<br />
			</span>
			<br />
            Phone Number
            <input type="text" value="" id="phone_number"/>
			<span class="error" id="error_phone_number" style="display:none;">
				<br />
			</span>
            <br />
			Primary?
			<br />
                <div class="check-line">
                    <input type="checkbox" id="yes" class='icheck-me' data-skin="square" data-color="blue" checked> <label class='inline' for="c6">Yes</label> 
                </div>
                <div class="check-line">
                    <input type="checkbox" id="no" class='icheck-me' data-skin="square" data-color="blue" checked> <label class='inline' for="c6">No</label> 
                </div>
		</p>
		</form>
	</div>
	<div class="modal-footer">
		<button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
		<button data-id_dept="" class="btn btn-primary" id="add_employee_phone_details_btn">Save</button>
	</div>
</div>
<script>


$('#add_employee_phone_details').click(function() {
	$('#insert_new_employee_phone_details_modal').modal('show');
	return false;
});

$('#add_employee_phone_details_btn').click(function() {
	
	var send = true;
	
	if($('#bank_request_description').val() == '') {
		$('#error_bank_request_description').show();
		send = false;
	} else {
		$('#error_bank_request_description').hide();
	}
	
	if($('#bank_due_date').val() == '') { 
		$('#error_bank_due_date').show();
		send = false;
	} else {
		$('#error_bank_due_date').hide();
	}
	
	if(send == true) {
		$('#bank_request_frm').submit();
	} else {
		return false;
	}
		
});


</script>	
