<div class="box box-color box-bordered primary employee_info_box" id="emergency_container" style="display:none;">
    <div class="box-title">
        <h3>Emergency contacts</h3>
        <ul class="nav nav-pills pull-right" style="margin-bottom:5px;">
            <?php if ($this->user_auth->have_perm(EDIT_REQUEST_EMPLOYEE_RECORDS)) { ?>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">Edit <span class="caret"></span></a>
                    <ul class="pull-right dropdown-menu">
                        <li><a href="<?php echo site_url('employee/edit_profile_emergency_contacts/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)); ?>" class="popup_full">Edit Now</a></li>
                        <?php if ($send_request == true) { ?>
                            <li><a href="#" id="request_employee_profile_emergency_contacts">Request from Employee</a></li>
                        <?php } ?>	
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </div>
	<div class="box-content">
		
		
		<div style="margin-top:20px;">
            <table class="table">
                <tr>
                    <?php foreach ($e_contacts[1] as $title => $value) { ?>
                        <td width="14%"><strong><?php echo $title; ?>:</strong></td>

                    <?php } ?>
                </tr>
                <tr>
                    <?php foreach ($e_contacts[1] as $title => $value) { ?>
                        <td><?php echo $value; ?></td>
                    <?php } ?>
                </tr>
                <tr>
                    <?php foreach ($e_contacts[2] as $title => $value) { ?>
                        <td><?php echo $value; ?></td>
                    <?php } ?>
                </tr>

            </table>
		</div>
	</div>
</div>
<?php if($this->user_auth->have_perm(EDIT_REQUEST_EMPLOYEE_RECORDS)) { ?>
<div class="modal hide fade" id="request_from_employee_emergency_model" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h4 class="modal-title" id="myModalLabel">Request from Employee</h4>
	</div>
	<div class="modal-body">
		<?php if($request_message != '') { ?>
			<div class="alert alert-info">
				<button data-dismiss="alert" class="close" type="button">×</button>
				<strong><?php echo $request_message; ?></strong>
			</div>
		<?php } ?>
		<form method="POST" action="<?php echo site_url('employee/request_emergency_contacts/'.$this->uri->segment(3).'/'.$this->uri->segment(4)); ?>" id="emergency_request_frm">
		<p>
			Instruction
			<br />
			<textarea data-rule-required="true" id="emergency_request_description" name="emergency_request_description" style="width:500px;" class="ui-wizard-content"></textarea>
			<span class="error" id="error_emergency_request_description" style="display:none;">
				<br />Enter Instruction
			</span>
			<br />
			Due date
			<br />
			<input type="text" name="emergency_due_date" id="emergency_due_date" class="datepick" value="" />
			<span class="error" id="error_emergency_due_date" style="display:none;">
				<br />Enter due date
			</span>
		</p>
		</form>
	</div>
	<div class="modal-footer">
		<button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
		<button data-id_dept="" class="btn btn-primary" id="request_from_employee_emergency_btn">Send request</button>
	</div>
</div>

<div class="modal hide fade" id="request_from_employee_emergency_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h4 class="modal-title" id="myModalLabel">Request from Employee</h4>
	</div>
    <div class="modal-body">
        <form id="request_emer_frm" class="form" method="POST" action="<?= site_url('user/employee/send_request') ?>">
            <input type="hidden" name="id_string" id="id_string" value="<?= $this->uri->segment(3) ?>" />
            
            <div class="control-group">
                <div class="controls">
                    <div class="check-line">
                        <input type="checkbox" id="emergency1" name="emergency1" class='icheck-me' data-skin="square" data-color="blue" value="<?= $request_fields[0]['emergency1_'] ?>" checked=""> <label class='inline' for="emergency1">&nbsp;Emergency Contact 1</label> 
                    </div>
                </div>
            </div>
           
            <div class="control-group">
                <div class="controls">
                    <div class="check-line">
                        <input type="checkbox" id="emergency2" name="emergency2" class='icheck-me' data-skin="square" data-color="blue" value="<?= $request_fields[0]['emergency2_'] ?>" checked=""> <label class='inline' for="emergency2">&nbsp;Emergency Contact 2</label> 
                    </div>
                </div>
            </div>
            
            <div class="control-group">
                <label>Due date</label>
                <div class="controls">
                    <input type="text" name="request_due_date" id="request_emergency_due_date" class="pickDate" />
                    <span class="error" id="error_request_emergency_due_date" style="display:none;">
                        Enter due date
                    </span>
                </div>
            </div>

        </form>
    </div>
    
    <div class="modal-footer">
		<button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
		<button data-id_dept="" class="btn btn-primary" id="request_from_employee_emer_btn">Send request</button>
	</div>
    
</div>

<script>
$(document).ready(function() {

$('#request_employee_profile_emergency_contacts').click(function() {
//	$('#request_from_employee_emergency_model').modal('show');
	$('#request_from_employee_emergency_modal').modal('show');
	return false;
});

$('#request_from_employee_emer_btn').click(function() {
    
    var due_date = $.trim($('#request_emergency_due_date').val());
	var send = true;
    
    if(due_date === '') { 
		$('#error_request_emergency_due_date').show();
		send = false;
	} else {
		$('#error_request_emergency_due_date').hide();
	}
    
    if(send === true) { 
        $('#request_from_employee_emer_btn').attr('disabled', true);
		$('#request_emer_frm').submit();
	} else {
		return false;
	}	
    
});


$('#request_from_employee_emergency_btn').click(function() {
	
	var send = true;
	
	if($('#emergency_request_description').val() == '') {
		$('#error_emergency_request_description').show();
		send = false;
	} else {
		$('#error_emergency_request_description').hide();
	}
	
	if($('#emergency_due_date').val() == '') { 
		$('#error_emergency_due_date').show();
		send = false;
	} else {
		$('#error_emergency_due_date').hide();
	}
	
	if(send == true) {
		$('#emergency_request_frm').submit();
	} else {
		return false;
	}	
		
});

});
</script>	
<?php } ?>