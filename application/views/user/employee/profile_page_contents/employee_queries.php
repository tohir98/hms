<div class="box box-color box-bordered primary employee_info_box" id="employee_queries_container" style="display:none;">
	<div class="box-title">
		<h3>Employee queries</h3>
	</div>
	<div class="box-content">
		<div style="border-bottom:1px solid #CCCCCC;">
			<ul class="nav nav-pills" style="margin-bottom:5px;">
				<li class="active">
					<h4>Employee queries</h4>
				</li>
			</ul>
		</div>
		<div style="margin-top:20px;">
			
			<?php if($message != '') { ?>
			<div class="alert alert-danger" style="margin-top:20px;">
				<button data-dismiss="alert" class="close" type="button">×</button>
				<?php echo $message; ?>
			</div>
			<?php } ?>
			
			<?php 

			$status = array(
				1 => 'Outstanding', 
				2 => 'Completed',
				3 => 'Draft'
			);

			?>
			
			<div class="-box-content nopadding">

            <table class="table table-hover table-nomargin dataTable table-bordered">
                <thead>
                    <th>Date Issued</th>
                    <th>Title</th>
                    <th>Issued By</th>
                    <th>Status</th>
					<th>Action</th>
                </thead>
                <tbody>
                    <?php 

                    foreach ($queries as $q) {
                        ?>
                        <tr>
                            <td><?php echo $q->date_due; ?></td>
                            <td><?php echo $q->title; ?></td>
                            <td><?php echo ucwords($q->name_from); ?></td>
                            <td><?php echo $status[$q->status]; ?></td>
							<td><?php echo anchor('employee_queries/query/'.$q->id_employee_query, 'view'); ?></td>
                        </tr>
                        <?php
                    }

                     ?>
                    
                </tbody>
            </table>
        </div>
			
		</div>
	</div>
</div>