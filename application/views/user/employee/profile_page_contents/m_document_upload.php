<div class="box box-color box-bordered primary employee_info_box" id="resume_credentials_container" style="display:none;">
	<div class="box-title">
		<h3>
			Document Uploads
		</h3>
		<a id="submit_form_button" type="button" name="_submit" class="btn btn-warning pull-right"> Add New Document</a>
	</div>
	<div class="box-content no-padding">
		
		<?php 
			if(!empty($uploads)) { ?>
				<table cellpadding="3" class="table table-hover table-nomargin">
					<thead>
						<tr>
							<th><strong>File name</strong></th>
							<th><strong>Description</strong></th>
							<th><strong>Status</strong></th>
							<th><strong>Admin, Employee</strong></th>
						</tr>
					</thead>
					<tbody>
				<?php
				$container = $this->user_auth->get('cdn_container');
				
				foreach($uploads as $r) { 
					
					if($r->type == 0) {
						$status = 'Outstanding';
					} else {
						$status = 'Submitted';
					}
					
					if($r->file_name != '') {
						$file_get_url = site_url('cdn_file_url/file_temp_url') . '/' . $container . '/' . $r->file_name;
					} else {
						$file_get_url = '';
					}
					?>
					<tr>
						<td><?php echo $r->file_title; ?></td>
						<td><?php echo $r->description; ?></td>
						<td><?php echo $status; ?></td>
						<td><?php echo $r->first_name . ' ' . $r->last_name; ?></td>
					</tr>
					<?php		
				} ?>
				</table>
			<?php
			} else { ?>
			<div class="alert alert-info" style="margin-top:20px;">
				<strong>There are no uploads.</strong>
			</div>
			<?php } ?>
		
	</div>
</div>



<!-- Modal box for prompt -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="update_box" style="display: none;">
	<div class="modal-header">
		<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
		<h3 id="myModalLabel">Upload Document</h3>
	</div>
	<form id="upload" name="upload" method="post" action="<?php echo site_url('employee/document_upload');?>" enctype="multipart/form-data">
		<div class="modal-body">
			<div class="control-group">
				<label for="filename" class="control-label">Document Name:</label>
				<div class="controls">
					<input type="text" name="upload_file_name" id="upload_file_name" placeholder="Document Name" class="input-xxlarge">
				</div>
			</div>
			<div class="control-group">
				<label for="filepath" class="control-label">Location:</label>
				<div class="controls">
					<input type="file" name="upload_file" id="upload_file" accept="application/msword, application/pdf, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-powerpoint, application/vnd.openxmlformats-officedocument.presentationml.presentation"/>
					<input type="hidden" name='id_user' value='<?php echo $id_user; ?>'>
				</div>
			</div>
			<div class="control-group">
				<label for="description" class="control-label">Description:</label>
				<div class="controls">
					<textarea class="input-xxlarge" name="upload_file_description" id="upload_file_description" rows="2" placeholder="Description"></textarea>
				</div>
			</div>
		</div>
	</form>
	<div class="modal-footer">
		<button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
		<button data-dismiss="modal" class="btn btn-primary" id="submit_dialog_button">Upload</button>
	</div>
</div>


<script>
	
var current_id_file = 0;
var current_file_name = '';
var delete_file_url = '<?php echo site_url('downloads/download/delete_shared_file/'); ?>';

$(document).ready(function() {
	
	$('#submit_form_button').click(function() {
		
		$('#update_box').modal('show');
	
	});
	
	$('#submit_dialog_button').click(function() {
		$('form#upload').submit();
		$('#update_box').modal('hide');
	});
	
	// $('.delete_shared_file').click(function() {
         
	// 	 current_id_file = 0;
	// 	 current_file_name = '';

	// 	 current_id_file = $(this).attr('data-id_file');
	// 	 current_file_name = $(this).attr('data-filename');
		 
	// 	 $('#delete_modal').modal('show');
		 
	// 	 $('#delete_file_button').unbind();
	// 	 $('#delete_file_button').click(function() {
			 
	// 		 var this_delete_file_url = delete_file_url + '/' + current_file_name + '/' + current_id_file;
	// 		 /* NOW YOU CAN REDIRECT TO THIS LINK. IT IS DELETE LINK ! */
	// 		 window.location.href = this_delete_file_url;
	// 	 });
		 
	// 	 return false;
 //    });
	
});
</script>