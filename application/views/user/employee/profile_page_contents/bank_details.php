<div class="box box-color box-bordered primary employee_info_box" id="bank_details_container" style="display:none;">
    <div class="box-title">
        <h3>Bank details</h3>
        <ul class="nav nav-pills pull-right" style="margin-bottom:5px;">
            <?php if ($this->user_auth->have_perm(EDIT_REQUEST_EMPLOYEE_RECORDS)) { ?>
                <li class="dropdown pull-right">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">Action <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo site_url('employee/edit_bank_details/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)); ?>" class="popup">Add New</a></li>
                        <?php if ($send_request == true) { ?>
                            <li><a href="#" id="request_employee_bank_details">Request from Employee</a></li>
                        <?php } ?>	
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </div>
    <div class="box-content">
        <div style="margin-top:20px;">
            <?php if (!empty($bank_details)) : ?>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Account name</th>
                            <th>Bank</th>
                            <th>Account type</th>
                            <th>Account number</th>
                            <th>Default</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                <?php foreach ((array) $bank_details as $bank) : ?>
                        <tbody>
                            <tr>

                            <?php foreach ((array)$bank as $title => $value) { ?>
                                    <td><?php echo $value; ?></td>
                                <?php } ?>

                            </tr>
                        </tbody>
                    <?php endforeach; ?>
                </table>
            <?php else: ?>
            <div>
                <?php echo 'No bank information has been added yet'; ?>
            </div>
            <?php endif; ?>

        </div>
    </div>
</div>
<?php if ($this->user_auth->have_perm(EDIT_REQUEST_EMPLOYEE_RECORDS)) { ?>
    <div class="modal hide fade" id="request_from_employee_bank_details_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h4 class="modal-title" id="myModalLabel">Request from Employee</h4>
        </div>
        <div class="modal-body">
            <?php if ($request_message != '') { ?>
                <div class="alert alert-info">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <strong><?php echo $request_message; ?></strong>
                </div>
            <?php } ?>
            <form method="POST" action="<?php echo site_url('employee/request_bank_details/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)); ?>" id="bank_request_frm">
                <p>
                    Instruction
                    <br />
                    <textarea data-rule-required="true" id="bank_request_description" name="bank_request_description" style="width:500px;" class="ui-wizard-content"></textarea>
                    <span class="error" id="error_bank_request_description" style="display:none;">
                        <br />Enter Instruction
                    </span>
                    <br />
                    Due date
                    <br />
                    <input type="text" name="bank_due_date" id="bank_due_date" class="pickDate" value="" />
                    <span class="error" id="error_bank_due_date" style="display:none;">
                        <br />Enter due date
                    </span>
                </p>
            </form>
        </div>
        <div class="modal-footer">
            <button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
            <button data-id_dept="" class="btn btn-primary" id="request_from_employee_bank_btn">Send request</button>
        </div>
    </div>

    <div class="modal hide fade" id="request_from_employee_bank_info_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h4 class="modal-title" id="myModalLabel">Request from Employee</h4>
        </div>
        <div class="modal-body">
            <form id="request_bank_frm" class="form" method="POST" action="<?= site_url('user/employee/send_request') ?>">
                <input type="hidden" name="id_string" id="id_string" value="<?= $this->uri->segment(3) ?>" />

                <div class="control-group">
                    <div class="controls">
                        <div class="check-line">
                            <input type="checkbox" id="bank_account" name="bank_account" class='icheck-me' data-skin="square" data-color="blue" value="<?= $request_fields[0]['bank_account_'] ?>" checked=""> <label class='inline' for="c6">&nbsp;Bank Details</label> 
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label>Due date</label>
                    <div class="controls">
                        <input type="text" name="request_due_date" id="request_bank_info_due_date" class="pickDate" />
                        <span class="error" id="error_request_bank_info_due_date" style="display:none;">
                            Enter due date
                        </span>
                    </div>
                </div>

            </form>
        </div>

        <div class="modal-footer">
            <button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
            <button data-id_dept="" class="btn btn-primary" id="request_from_employee_bank_btn1">Send request</button>
        </div>

    </div>

    <script>
        $(document).ready(function () {

            $('#request_employee_bank_details').click(function () {
                //	$('#request_from_employee_bank_details_modal').modal('show');
                $('#request_from_employee_bank_info_modal').modal('show');
                return false;
            });

            $('#request_from_employee_bank_btn1').click(function () {

                var due_date = $.trim($('#request_bank_info_due_date').val());
                var send = true;

                if (due_date === '') {
                    $('#error_request_bank_info_due_date').show();
                    send = false;
                } else {
                    $('#error_request_bank_info_due_date').hide();
                }

                if (send === true) {
                    $('#request_from_employee_bank_btn1').attr('disabled', true);
                    $('#request_bank_frm').submit();
                } else {
                    return false;
                }

            });

            $('#request_from_employee_bank_btn').click(function () {

                var send = true;

                if ($('#bank_request_description').val() == '') {
                    $('#error_bank_request_description').show();
                    send = false;
                } else {
                    $('#error_bank_request_description').hide();
                }

                if ($('#bank_due_date').val() == '') {
                    $('#error_bank_due_date').show();
                    send = false;
                } else {
                    $('#error_bank_due_date').hide();
                }

                if (send == true) {
                    $('#bank_request_frm').submit();
                } else {
                    return false;
                }

            });

        });
    </script>	
<?php } ?>