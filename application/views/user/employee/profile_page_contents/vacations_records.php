<div class="box box-color box-bordered primary employee_info_box" id="vacations_records_container" style="display:none;">
	<div class="box-title">
		<h3>Vacations records</h3>
	</div>
	<div class="box-content">
		<div style="border-bottom:1px solid #CCCCCC;">
			<ul class="nav nav-pills" style="margin-bottom:5px;">
				<li class="active">
					<h4>Vacations records</h4>
				</li>
			</ul>
		</div>
		<div style="margin-top:20px;">
		<?php if($message != '') { ?>
			<div class="alert alert-danger" style="margin-top:20px;">
				<button data-dismiss="alert" class="close" type="button">×</button>
				<?php echo $message; ?>
			</div>
		<?php } ?>
		
			<!-- Vacation data -->
			<div class="box box-bordered ">
    <div class="box-title">
        
        <ul class="tabs">
            <li class="active">
                <a href="#entitlement" data-toggle="tab">Entitlement</a>
            </li>
            <li>
                <a href="#history" data-toggle="tab">History</a>
            </li>
            <li>
                <a href="#approval" data-toggle="tab">Pending Approval</a>
            </li>
        </ul>
    </div>
    <div class="box-content nopadding">
        <div class="tab-content">
            <?php foreach ($holidays as $h) {
                $holi[] = $h->holiday_date;
            } ?>
            
            <div class="tab-pane active" id="entitlement">
                
                <table class="table">
                    <thead>
                        <th>Type</th>
                        <th>Days Allocated</th>
                        <th>Carried Over</th>
                        <th>Approved: Taken</th>
                        <th>Approved: Upcoming</th>
                        <th>Pending Approval</th>
                        <th>Balance</th>
                    </thead>
                    <tbody>
                        <?php 
                            if(!empty($entitlements)) {
                                $count = 0;
                                $balance = array();
                                
                                foreach($entitlements as $ent) {
                        ?>
                        <tr>
                            <td><?php echo $ent->name?></td>
                            <td><?php echo $ent->no_of_days?></td>
                            <?php 
                            
                                $used = 0;
                                $used2 = 0;
                                $pending = 0;
                                $upc = 0;
                                if(!empty($taken[$ent->id_type])) {
                                    
                                    foreach($taken[$ent->id_type] as $t) {
                                        $date_start = date('F jS, Y', strtotime($t[0]));
                                        $date_end = date('F jS, Y', strtotime($t[1]));
                                        $date_end2 = strtotime($t[1]);
                                        $date_now = date('F jS, Y', strtotime('now'));
                                        $date_now2 = strtotime('now');
                                         
                                        $diff = get_working_days($date_start, $date_end, $holi);   
                                        $up = round($diff);
                                        if($t[2] == 'Pending') {
                                            if($diff > 0){ $pending = $pending + $diff; }
                                        }

                                        if($t[2] == 'Approved'){
                                            $used2 += $diff;
                                            $wd = get_working_days($date_now, $date_end, $holi);
                                            if($wd > 0){ 
                                                $upc = $wd - 1; 
                                                $diff = $diff - $wd;
                                            }

                                            
                                            //$diff ++;
                                            
                                            $used += $diff;
                                        }

                                        /*if($t[2] = 'Declined'){
                                            $used += 0;
                                        }*/
                                        
                                    }
                                }
                                //$ongoing = $upc - 1;
                                $balance[$count] = $ent->no_of_days - $used2;
                                $used++;
                            ?>
                            <td><?php echo 0?></td>
                            <td><?php echo $used?></td>
                            <td><?php echo $upc;?></td>
                            <td><?php echo $pending?></td>
                            <td><span id='my_balance<?php echo $ent->id_type;?>'><?php echo $balance[$count]?></span></td>                          
                        </tr>
                        <?php $count++; }}else {?>
                        <tr>
                            <td colspan="5">No entitled to any vacation.</td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane" id="history">
                <table class="table">
                    <thead>
                        <th>Type</th>
                        <th>Description</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>Status</th>
                    </thead>
                    <tbody>
                        <?php 
                            if(!empty($history)) {
                                foreach($history as $hist) {
                        ?>
                        <tr>
                            <td><?php echo $hist->name?></td>
                            <td><?php echo $hist->description?></td>
                            <td><?php echo date('jS F, Y', strtotime($hist->date_start))?></td>
                            <td><?php echo date('jS F, Y', strtotime($hist->date_end))?></td>
                            <td><?php echo $hist->status?></td>
                        </tr>
                        <?php }}else {?>
                        <tr>
                            <td colspan="5">There are no requests for any vacation.</td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane" id="approval">
                <table class="table">
                    <thead>
                        <th>Type</th>
                        <th>Description</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>No. of Days</th>
                        <th>Status</th>
                    </thead>
                    <tbody>
                        <?php 
                            if(!empty($pending_approvals)) {
                                foreach($pending_approvals as $pending) {
                                    $date_start = date('F jS, Y', strtotime($pending->date_start));
                                    $date_end = date('F jS, Y', strtotime($pending->date_end));
                                                                        
                        ?>
                        <tr>
                            <td><span id="id_name<?php echo $pending->id_vacation?>" data-id="<?php echo $pending->id_type?>"><?php echo $pending->name?></span></td>
                            <td><span id="id_desc<?php echo $pending->id_vacation?>"><?php echo $pending->description?></span></td>
                            <td><span id="id_start<?php echo $pending->id_vacation?>"><?php echo $date_start;?></td>
                            <td><span id="id_end<?php echo $pending->id_vacation?>"><?php echo $date_end;?></td>
                            <td>
                                <span id="id_days<?php echo $pending->id_vacation?>">
                                <?php 
                                    
                                    $diff = get_working_days($date_start, $date_end, $holi);
                                    echo round($diff);
                                ?> 
                                </span>
                            </td>
                            <td><span id="id_status<?php echo $pending->id_vacation?>"><?php echo $pending->status?></span></td>
                        </tr>
                        <?php }}else {?>
                        <tr>
                            <td colspan="7">There are no any request awaiting approval.</td>                            
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>    
</div>
			<!-- end vacation data -->
		</div>
	</div>
</div>