<div class="box box-color box-bordered primary employee_info_box" id="work_info_personal_info_container">
	<div class="box-title">
		<h3>
			Profile
		</h3>
	</div>
	<div class="box-content">
		
		<div style="border-bottom:1px solid #CCCCCC;">
			<ul class="nav nav-pills" style="margin-bottom:5px;">
				<li class="active">
					<h4>Work info</h4>
				</li>
			</ul>
		</div>
		<div style="margin-top:20px;">
			<table cellpadding="3">
				<?php foreach($wi as $title => $value) { ?>
				<tr>
					<td align="right"><strong><?php echo $title; ?>:</strong></td>
					<td><?php echo $value; ?></td>
				</tr>
				<?php } ?>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</div>
		<div style="border-bottom:1px solid #CCCCCC;">
			<ul class="nav nav-pills" style="margin-bottom:5px;">
				<li class="active">
					<h4>Personal info</h4>
				</li>
			</ul>
		</div>
		<div style="margin-top:20px;">
			<table cellpadding="3">
				<?php foreach($pi as $title => $value) { ?>
				<tr>
					<td align="right"><strong><?php echo $title; ?>:</strong></td>
					<td><?php echo $value; ?></td>
				</tr>
				<?php } ?>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</div>
	</div>
</div>
