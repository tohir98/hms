<div class="box box-color box-bordered primary employee_info_box" id="internal_notes_container" style="display:none;">
	<div class="box-title">
		<h3>Internal notes</h3>
		
        <a href="#" class='add_note btn btn-warning pull-right' style="margin-right: 10px">Add note</a>
	</div>
	<div class="box-content">
		
		<ul class="messages">
		<?php 
			if(!empty($notes)){
				foreach ($notes as $n) {
					
		?>
					<li class="left" id="note<?php echo $n->id_employee_note;?>">
						<div class="image">
							<?php 
								if($n->profile_picture_name != '') { 
									$profile_pic_url = $this->cdn_api->temporary_url($n->profile_picture_name);
								} else {
									$profile_pic_url = '/img/profile-default.jpg';
								}
							?>
							<img src="<?php echo $profile_pic_url; ?>" />
							
						</div>
						<div class="message">
							<span class="caret"></span>
							<span class="name"><?php echo ucwords($n->first_name." ".$n->last_name); ?></span>
							<span class="time dept"><?php echo ucwords($n->departments."/".$n->roles); ?></span>
							<?php 
							if($this->user_auth->get('id_user') == $n->id_creator){
							?>
							<a href="#" class='pull-right edit_note' data-id='<?php echo $n->id_user;?>' data-id_note='<?php echo $n->id_employee_note;?>'>edit</a>
							<?php
								}
							?>
							<p class='note'><?php echo $n->note; ?> </p>
							<span class="time">
								<?php echo $n->date_added ?>
							</span>
						</div>
					</li>
		<?php 	
				}
			}
			else{
		?>
			
			<li class="left empty_notes">
				<p>There are no notes for this employee</p>
			</li>
		<?php
			}
		?>	
		</ul>
		
	</div>
</div>


<div class="modal hide fade" id="edit_note_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h4 class="modal-title" id="myModalLabel">Edit Note</h4>
	</div>
	<div class="modal-body">
		
		<div id="enter_note_msg" class="alert alert-error" style="display:none;">Enter Note.</div>
		<div id="loading_note_msg" class="alert alert-info" style="display:none;"><i>Processing....</i></div>
		<div id="success_note_msg" class="alert alert-success" style="display:none;"><i>Note added successfully</i>.</div>
		<div style="padding-right:10px;">
			<input type="hidden" name="note_id_user" id="note_id_user" value="<?php echo $notes_id_user;?>" />
			<input type="hidden" name="note_id" id="note_id" value="" />
			<textarea name="note" id="note" class="form-control" style="width:100%"></textarea>
		</div>
		<div style="padding-right:10px;">
			<input type="checkbox" name='priority' id='priority'> High Priority (Email and SMS alert will be sent to HR)
		</div>
		<div style="padding-right:10px; margin-top:10px;">
			Who can see this note 
			<select name="who_sees" id="who_sees">
				<option value="1">Admin only</option>	
				<option value="2">Admin and Manager</option>
				<option value="3">Admin, Manager and Employee</option>
			</select>
		</div>
		
	</div>
	<div class="modal-footer">
		<button class="btn btn-primary" aria-hidden="true" id="edit_note_btn">Save Changes</button>
		<button data-dismiss="modal" class="btn btn-default close-modal" aria-hidden="true">Cancel</button>
	</div>
</div>


<div class="modal hide fade" id="add_note_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h4 class="modal-title" id="myModalLabel">Add Note</h4>
	</div>
	<div class="modal-body">
		
		<div id="enter_note_msg1" class="error" style="display:none;">Enter Note.</div>
		<div id="loading_note_msg1" class="alert alert-info" style="display:none;"><i>Note processing....</i></div>
		<div id="success_note_msg1" class="alert alert-success" style="display:none;"><i>Note added successfully</i>.</div>
		<div style="padding-right:10px;">
			<input type="hidden" name="note_id_user1" id="note_id_user1" value="<?php echo $notes_id_user;?>" />
			<input type="hidden" name="note_id1" id="note_id1" value="" />
			<textarea name="note1" id="note1" class="form-control" style="width:100%"></textarea>
		</div>
		<div style="padding-right:10px;">
			<input type="checkbox" name='priority1' id='priority1'> High Priority (Email and SMS alert will be sent to HR)
		</div>
		<div style="padding-right:10px; margin-top:10px;">
			Who can see this note 
			<select name="who_sees1" id="who_sees1">
				<option value="1">Admin only</option>	
				<option value="2">Admin and Manager</option>
				<option value="3">Admin, Manager and Employee</option>
			</select>
		</div>
		
	</div>
	<div class="modal-footer">
		<button class="btn btn-primary bot" aria-hidden="true" id="add_note_btn">Submit</button>
		<button data-dismiss="modal" class="btn btn-default close-modal" aria-hidden="true">Cancel</button>
	</div>
</div>




<script type="text/javascript">
	
	// --------------- ADD NOTE ---------------------- //

	$('.add_note').click(function() {

		$('#enter_note_msg1').hide();
		$('#loading_note_msg1').hide();
		$('#success_note_msg1').hide();
		$('#add_note_modal').modal('show');
	});


	$('#add_note_btn').click(function() {
		
		var note = $('#note1').val();
		var id_user = $('#note_id_user1').val();
		var who_sees = $('#who_sees1').val();
		var priority = $('#priority1').prop('checked');

		if(priority > 0){
			priority = 1;
		}else{
			priority = 0;
		}
	
		if($.trim(note) == '') {
			$('#enter_note_msg1').show();
			return true;
		} else {
			$('#enter_note_msg1').hide();

			$('#note1').attr('disabled', true);
			$('#note_id_user1').attr('disabled', true);
			$('#who_sees1').attr('disabled', true);
			$('#priority1').attr('disabled', true);
			$('#add_note_btn').attr('disabled', true);
			$('.close-modal').attr('disabled', true);
			$('#loading_note_msg1').show();
		}
		
		var url_ = '<?php echo site_url('employee/add_note'); ?>';
		$.ajax({

          type: "POST",
          url: url_,
          data: {
				'note': note, 
				'id_user':id_user,
				'who_sees':who_sees,
				'priority':priority
			},
          dataType: "JSON",
          success: function(result){
              console.log(result);
          	$('#loading_note_msg1').hide();
			$('#success_note_msg1').show();
			//append new data
			var new_part = '<li class="left">' +
								'<div class="image">' +
									'<img src="/img/profile-default.jpg" />' +
								'</div>' +
								'<div class="message">' +
									'<span class="caret"></span>' +
									'<span class="name">'+ result.first_name +' '+ result.last_name +'</span>' +
									'<span class="time dept">'+ result.department +'/'+ result.role +'</span>' +
									'<p class="note">'+ result.note +' </p>' +
									'<span class="time">' +
										result.date_added +
									'</span>' +
								'</div>' +
							'</li>';

			$("ul.messages").prepend(new_part);
			$('#add_note_modal').modal('hide');  
			$('#note1').attr('disabled', false);
			$('#note_id_user1').attr('disabled', false);
			$('#who_sees1').attr('disabled', false);
			$('#priority1').attr('disabled', false);
			$('#add_note_btn').attr('disabled', false);
			$('.close-modal').attr('disabled', false);


			$('#note1').val('');
			$('.empty_notes').hide();               
              
           }
         });
				
		return true;
		
	});


	$('.edit_note').click(function() {

		$('#enter_note_msg').hide();
		$('#loading_note_msg').hide();
		$('#success_note_msg').hide();

		//set form field value
		var note = $(this).next('.note').text();
		var id_user = $(this).attr('data-id');
		var id_note = $(this).attr('data-id_note');
		$('#edit_note_modal').modal('show');
		$('#note').val(note);
		$('#note_id_user').val(id_user);
		$('#note_id').val(id_note);
		
	});


	$('body').on('click', '#edit_note_btn', (function() {
		
		var note = $('#note').val();
		var id_user = $('#note_id_user').val();
		var who_sees = $('#who_sees').val();
		var note_id = $('#note_id').val();
		var priority = $('#priority').prop('checked');

		if($.trim(note) == '') {
			$('#enter_note_msg').show();
			return true;
		} else {
			$('#enter_note_msg').hide();

			$('#note').attr('disabled', true);
			$('#note_id_user').attr('disabled', true);
			$('#who_sees').attr('disabled', true);
			$('#priority').attr('disabled', true);
			$('#edit_note_btn').attr('disabled', true);
			$('.close-modal').attr('disabled', true);
			$('#loading_note_msg').show();
		}
		
		var url_ = '<?php echo site_url('employee/update_note'); ?>';

		$.ajax({

          type: "POST",
          url: url_,
          data: {
				'note': note, 
				'id_user':id_user,
				'who_sees':who_sees,
				'priority':priority,
				'note_id':note_id
			},
          dataType: "JSON",
          success: function(result){

          	$('#loading_note_msg').hide();
			$('#success_note_msg').show();
			//append new data
			var new_part = '<li class="left">' +
								'<div class="image">' +
									'<img src="/img/profile-default.jpg" />' +
								'</div>' +
								'<div class="message">' +
									'<span class="caret"></span>' +
									'<span class="name">'+ result.first_name +' '+ result.last_name + '  </span>' +
									'<span class="time">'+ result.department +'/'+ result.role +'</span>' +
									'<a href="#" class="pull-right edit_note" data-id="'+ result.id_user +'" data-id_note="'+ note_id +'">edit</a>' +
									'<p>'+ result.note +' </p>' +
									'<span class="time">' +
										result.date_added +
									'</span>' +
								'</div>' +
							'</li>';

			$('#note'+note_id).remove();
			$("ul.messages").prepend(new_part);
			$('#edit_note_modal').modal('hide');  
			$('#note').attr('disabled', false);
			$('#note_id_user').attr('disabled', false);
			$('#who_sees').attr('disabled', false);
			$('#priority').attr('disabled', false);
			$('#edit_note_btn').attr('disabled', false);
			$('.close-modal').attr('disabled', false);  

			$('#note').val('');
			$('.empty_notes').hide();            
              
           }
         });
				
		return true;
		
	}));



</script>