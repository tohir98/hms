<div class="box box-color box-bordered primary employee_info_box" id="resume_credentials_container" style="display:none;">
						<div class="box-title">
							<h3>
								Document Uploads
							</h3>
							<?php if($this->user_auth->have_perm(EDIT_REQUEST_EMPLOYEE_RECORDS)) { ?>
								<a href="<?php echo site_url('employee/upload_document/'.$this->uri->segment(3).'/'.$this->uri->segment(4)); ?>" class="pull-right btn popup_full" style="margin-right:10px;">Upload New File</a>
								<a href="#request_from_employee_credentials_model" data-toggle="modal" class="pull-right btn" style="margin-right:10px;">Request From Employee</a>
							<?php } ?>	
						</div>
						<div class="box-content">
							<div style="margin-top:20px;">
								<?php 
								if(!empty($uploads)) { ?>
									<table cellpadding="3" class="table table-hover table-nomargin">
										<thead>
											<tr>
												<th><strong>File name</strong></th>
												<th><strong>Link</strong></th>
												<th><strong>Status</strong></th>
												<!--<th><strong>From</strong></th>-->
												<th>&nbsp;</th>
											</tr>
										</thead>
										<tbody>
									<?php
									$container = $this->user_auth->get('cdn_container');
									foreach($uploads as $r) { 
										if($r->type == 0) {
											$status = 'Outstanding';
										} else {
											$status = 'Submitted ';
										}
										
										if($r->file_name != '') {
											$file_get_url = site_url('cdn_file_url/file_temp_url') . '/' . $container . '/' . $r->file_name;
										} else {
											$file_get_url = '';
										}
										?>
                                            <tr>
                                                        <?php if ($file_get_url == '') { ?>
                                                            <td>
                                                                <?php echo $r->file_title; ?>
                                                                <div class="muted"><?php echo $r->description; ?></div>
                                                            </td>
                                                        <?php } else { ?>	
                                                            <td><a href="<?php echo $file_get_url; ?>" target="_blank"><?php echo $r->file_title; ?></a>
                                                                <div class="muted"><?php echo $r->description; ?></div>
                                                            </td>
                                                        <?php } ?>
                                                        <td>
                                                            <?php $docKount = 1; if (!empty($files)) : foreach ($files as $f) : if ($r->id_cred == $f->id_cred) : ?>
                                                            <div>
                                                                <a href="<?= site_url('cdn_file_url/file_temp_url') . '/' . $container . '/' . $f->file_name ?>" target="_blank">
                                                                <?= 'Document '.$docKount ?>
                                                                </a>
                                                            </div>
                                                            <?php ++$docKount; endif; endforeach; endif; ?>
                                                           
                                                        </td>
                                                        <td><?php echo $status; ?> </td>
                                                        <!--<td><?php //echo $r->first_name . ' ' . $r->last_name; ?></td>-->
                                                        <td>
                                                            <?php if ($this->user_auth->have_perm(EDIT_REQUEST_EMPLOYEE_RECORDS) and $r->type == 1) { ?>
                                                                <ul class="nav nav-pills" style="margin-bottom:5px;">
                                                                    <li class="dropdown pull-right">
                                                                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">Action <span class="caret"></span></a>
                                                                        <ul class="dropdown-menu">
                                                                            <li><a href="<?php echo site_url('employee/edit_document/' . $r->id_cred . '/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)); ?>">Edit file</a></li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            <?php }elseif ($this->user_auth->have_perm(EDIT_REQUEST_EMPLOYEE_RECORDS) && $r->type == 0 && date('Y-m-d') > $r->due_date) { ?>
                                                            <a href="#request_from_employee_document_modal" data-toggle="modal" class="pull-right">
                                                                Request From Employee
                                                            </a>
                                                            <?php } else { ?>
                                                                &nbsp;
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
										<?php		
									} ?>
									</table>
								<?php
								} else { ?>
								<div class="alert alert-info" style="margin-top:20px;">
									<strong>There are no uploads.</strong>
								</div>
								<?php } ?>
							</div>
							
						</div>
					</div>
<?php if($this->user_auth->have_perm(EDIT_REQUEST_EMPLOYEE_RECORDS)) { ?>
<div class="modal hide fade" id="request_from_employee_credentials_model" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h4 class="modal-title" id="myModalLabel">Request from Employee</h4>
	</div>
    <form method="POST" action="<?php echo site_url('employee/request_credentials/'.$id_user_string.'/'.$this->uri->segment(4)); ?>" id="credentials_request_frm">
	<div class="modal-body">
		<?php if($request_message != '') { ?>
			<div class="alert alert-info">
				<button data-dismiss="alert" class="close" type="button">×</button>
				<strong><?php echo $request_message; ?></strong>
			</div>
		<?php } ?>
		
		<p>
			File Name
			<br />
            <input type="text" required id="cred_request_file_title" name="cred_request_file_title" title="Enter File name" style="width:500px;" class="ui-wizard-content">
			
			<br />
			File Description
			<br />
            <textarea required id="cred_request_description" name="cred_request_description" style="width:500px;" class="ui-wizard-content" title="Enter description"></textarea>
			
			<br />
			Due date
			<br />
            <input required type="text" name="cred_request_due_date" id="cred_request_due_date" class="pickDate" value="" title="Enter due date" />
			
		</p>
		
	</div>
	<div class="modal-footer">
		<button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
        <button type="submit" data-id_dept="" class="btn btn-primary" id="">Send request</button>
	</div>
    </form>
</div>

<div class="modal hide fade" id="request_from_employee_document_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <form method="post" action="<?= site_url('employee/upload_document'.'/'.$this->uri->segment(3).'/'.$this->uri->segment(4))?>">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title" id="myModalLabel">Request from Employee</h4>
            </div>

            <div class="modal-body">
                <?php include APPPATH . 'views/user/employee/_requestUpload.php'; ?>
            </div>

            <div class="modal-footer">
                <button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
                <input type="hidden" value="request_now_tab" name="form_type" id="form_type" />
                <button class="btn btn-primary" type="submit">Send request</button>
            </div>
        </form>
    
</div>
<script>
$(document).ready(function() {

$('#request_employee_credentials').click(function() {
	$('#request_from_employee_credentials_model').modal('show');
	return false;
});

$('#request_from_employee_credentials_btn').click(function() {
	
	var send = true;
	
	if($('#cred_request_file_title').val() == '') { 
		$('#error_credentials__file_name_txt').show();
		send = false;
	} else {
		$('#error_credentials__file_name_txt').hide();
	}
	
	if($('#cred_request_description').val() == '') {
		$('#error_credentials_txt').show();
		send = false;
	} else {
		$('#error_credentials_txt').hide();
	}
	
	if($('#cred_request_due_date').val() == '') { 
		$('#error_credentials_date_txt').show();
		send = false;
	} else {
		$('#error_credentials_date_txt').hide();
	}
	
	if(send == true) {
		$('#credentials_request_frm').submit();
	} else {
		return false;
	}	
		
});

});
</script>	
<?php } ?>