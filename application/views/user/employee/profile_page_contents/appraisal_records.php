<div class="box box-color box-bordered primary employee_info_box" id="appraisal_records_container" style="display:none;">
	<div class="box-title">
		<h3>Appraisal records</h3>
	</div>
	<div class="box-content">
		<div style="border-bottom:1px solid #CCCCCC;">
			<ul class="nav nav-pills" style="margin-bottom:5px;">
				<li class="active">
					<h4>Appraisal records</h4>
				</li>
			</ul>
		</div>
		<table class="table table-hover table-nomargin dataTable table-bordered">
					<thead>
						<tr>
							<th>Appraisal Type</th>
							<th>Period Under Review</th>
							<th>Review Period</th>
							<th>Status</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php if(!empty($appraisals)) { foreach ($appraisals as $a) { ?>
						<tr>
							<td>Self</td>
							<td><?php echo date('d, M, y', strtotime($a->date_period_start)).' - '. date('d, M, y', strtotime($a->date_period_end)) ?></td>
							<td><?php echo date('d, M, y', strtotime($a->date_start)).' - '. date('d, M, y', strtotime($a->date_due)) ?></td>
							<td>
							<?php if($a->status == APPRAISAL_REVIEW_STATUS_ACTIVE) {
								echo 'Completed';
							} elseif ($a->status == APPRAISAL_REVIEW_STATUS_INACTIVE) {
								echo 'Pending';
							}
							?>
							</td>
							<td>
								<?php if($a->rack_file_name != '') { ?>
								<div class="btn-group">
									<a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Action<span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li>
											<?php $file_url = $this->cdn_api->temporary_url($a->rack_file_name); ?>
											<a href="<?php echo $file_url ?>">View</a>
										</li>
									</ul>
								</div>
								<?php } ?>
							</td>
						</tr>
						<?php } } ?>
					</tbody>
				</table>
	</div>
</div>