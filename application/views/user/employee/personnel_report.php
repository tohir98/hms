<style>
    .placeholder{
        height: 300px;
    }
</style>
<!-- Display error/success message -->
<br/>
<?php if ($this->session->userdata('errors') != '') { ?>
    <div class="alert alert-danger">
        <button data-dismiss="alert" class="close" type="button">x</button>
        <?php echo $this->session->userdata('errors'); ?>
    </div>
    <?php $this->session->unset_userdata('errors');
}
if ($this->session->userdata('confirm') != '') {
    ?>
    <div class="alert alert-success">
        <button data-dismiss="alert" class="close" type="button">x</button>
    <?php echo $this->session->userdata('confirm'); ?>
    </div>
    <?php $this->session->unset_userdata('confirm');
} ?>

<!-- Page Header -->
<div class="page-header">
    <div class="pull-left">
        <h1>Analytics</h1>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">

    </div>
</div>

<div class="box">

    <div class="box-content nopadding">
        <p>
        <ul class="nav nav-tabs" id="myTab">
            <li>
                <a href="<?php echo site_url('employee/employee_analytics') ?>">Overview</a>
            </li>
            <li  class="active">
                <a href="#report" data-toggle="tab">Reports</a>
            </li>
        </ul>
        </p>

        <div class="tab-content"> 
            <div class="tab-pane active" id="report">
                <a href="<?php echo site_url('employee/employee_analytics') ?>" class="btn btn-warning"><i class=" icon-chevron-left"></i> Back</a>
                <div class="header">
                    <h3>Personnel Records</h3>
                </div>

                <div class="box box-bordered">
                    <div class="box-title" style="padding-right:5px;">
                        <h3>
                            <div class="btn-group pull-left">
                                <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">Bulk Action <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><?php echo anchor('#SendRequest', 'Send Request', 'class="send_request" data-toggle="modal"'); ?></li> 
                                </ul>
                            </div>
                        </h3>
                        <a href="<?php echo site_url('user/employee/download_excel') ?>" class="btn btn-primary pull-right">Download Excel <i class="icons icon-download-alt"></i></a>
                    </div>

                    <div class="box-content-padless">
                        <div class="alert alert-error" style="display:none" id="err_msg">
                            <strong>Select Employee(s)!</strong>
                        </div>
                        <form method="post" id="frm_approve">
                            <table class='table table-bordered dataTable dataTable-scroll-y dataTable-scroll-x'>
                                <thead>
                                    <tr class='thefilter'>
                                        <th class='with-checkbox'><input type="checkbox" name="check_all" id="check_all"></th>
                                        <th>Staff ID</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Status</th>
                                        <th>Department</th>
                                        <th>Location</th>
                                        <!--<th>Work Email</th>-->
                                        <th>Work Number</th>
                                        <th>Birthday</th>
                                        <th>Home Address</th>
                                        <th>Personal Number</th>
                                        <th>Emergency I</th>
                                        <th>Emergency II </th>
                                        <th>Available Documents</th>
                                        <th>Bank Account</th>
                                        <th>Pension Details</th>
                                    </tr>
                                </thead>
<?php if (!empty($report)) { ?>
                                    <tbody>
    <?php for ($c = 0; $c <= count($report) - 1; ++$c) { ?>
                                            <tr>
                                                <td class="with-checkbox">
                                                    <input type="checkbox" class="_item_checkbox" name="check" value="1" data-id_string="<?php echo $report[$c]['id_string']; ?>" data-work_phone="<?php echo $report[$c]['work_phone_'] ?>" data-birthday="<?php echo $report[$c]['birthday_'] ?>" data-home_address="<?php echo $report[$c]['home_address_'] ?>" data-personal_phone="<?php echo $report[$c]['personal_phone_'] ?>" data-emergency1="<?php echo $report[$c]['emergency1_'] ?>" data-emergency2="<?php echo $report[$c]['emergency2_'] ?>" data-uploaded_docs="<?php echo $report[$c]['uploaded_docs'] ?>" data-bank_account="<?php echo $report[$c]['bank_account_'] ?>" data-pension_details="<?php echo $report[$c]['pension_details_'] ?>" />
                                                </td>
                                                <td title="Staff ID"><?php echo $report[$c]['id_string']; ?></td>
                                                <td title="First Name"><?php echo $report[$c]['first_name']; ?></td>
                                                <td title="Last Name"><?php echo $report[$c]['last_name']; ?></td>
                                                <td title="Status"><?php echo $statuses[$report[$c]['status']]; ?></td>
                                                <td title="Department"><?php echo $report[$c]['departments']; ?></td>
                                                <td title="Location"><?php echo $report[$c]['location_tag']; ?></td>
                                                <!--<td title="Work Email" ><h4><?php // echo $report[$c]['work_email']  ?></h4></td>-->
                                                <td title="Work Number" ><h4><?php echo $report[$c]['work_phone'] ?></h4></td>
                                                <td title="Birthday" ><h4><?php echo $report[$c]['birthday'] ?></h4></td>
                                                <td title="Home Address"><h4><?php echo $report[$c]['home_address'] ?></h4></td>
                                                <td title="Personal Number" ><h4><?php echo $report[$c]['personal_phone'] ?></h4></td>

                                                <td title="Emergency I"><h4><?php echo $report[$c]['emergency1'] ?></h4></td>

                                                <td title="Emergency II" ><h4><?php echo $report[$c]['emergency2'] ?></h4></td>

                                                <td title="Available Documents" ><?php echo $report[$c]['uploaded_docs'] . ' of ' . $report[$c]['documents']; ?></td>

                                                <td title="Bank Account"  ><h4><?php echo $report[$c]['bank_account'] ?></h4></td>

                                                <td title="Pension Details"><h4><?php echo $report[$c]['pension_details'] ?></h4></td>

                                            </tr>
    <?php } ?>
                                    </tbody>
<?php } ?>
                            </table>
                            <input type="hidden" name="hidden_ref_id" value="<?php echo $this->uri->segment(3); ?>" />
                        </form>
                        <div style="text-align:right;clear:both;">

                        </div>
                    </div>

                </div>

            </div> <!-- End Div report -->

        </div>
    </div>    
</div>


<!--Request from employee modal-->
<div class="modal hide fade" id="request_from_employee_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="myModalLabel">Request from Employee</h4>
    </div>
    <div class="modal-body">
        <div class="alert alert-success loading1" style=" display: none; ">
            Loading... Please wait
        </div>
        <form method="POST" action="<?php echo site_url('employee/request_profile_personal_info/' . $this->uri->segment(3) . '/' . $this->uri->segment(4)); ?>" id="profile_pers_info_request_frm">
            <p>

                <input type="hidden" id="request_from_employee_profile_desc" name="request_from_employee_profile_desc" />

                Due date
                <br />
                <input type="text" required name="request_due_date" id="request_due_date" class="datepick" />
                <span class="error" id="error_request_due_date" style="display:none;">
                    <br />Enter due date
                </span>
            </p>
        </form>
    </div>
    <div class="modal-footer">
        <button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
        <input type="button" class="btn btn-primary" id="request_from_employee_info_btn" value="Send request" />
    </div>
</div>


<script>
    function get_selected_items() {

        var items = [];
        var i = 0;
        $('._item_checkbox').each(function () {
            if ($(this).is(':checked')) {
                items[i] = $(this).attr('data-id_string');

                i++;
            }
        });

        return items;
    }

    function get_selected_stuffs() {

        var stuffs = [];
        var i = 0;
        $('._item_checkbox').each(function () {
            if ($(this).is(':checked')) {
                stuffs[i] = $(this).attr('data-id_string') + '|' + $(this).attr('data-work_phone') + '|' + $(this).attr('data-birthday') + '|' + $(this).attr('data-home_address') + '|' + $(this).attr('data-personal_phone') + '|' + $(this).attr('data-emergency1') + '|' + $(this).attr('data-emergency2') + '|' + $(this).attr('data-bank_account') + '|' + $(this).attr('data-pension_details');
                i++;
            }
        });

        return stuffs;
    }

    $('.send_request').click(function () {

        var items = get_selected_items();

        if (!items.length) {
            $('#err_msg').show();
            return true;
        }

        $('#request_from_employee_modal').modal('show');
        return true;

    });

    $('#request_from_employee_info_btn').click(function () {

        var request_due_date_ = $('#request_due_date').val();
        var items = get_selected_items();
        var stuffs = get_selected_stuffs();

        if ($.trim(request_due_date_) === '') {
            $('#error_request_due_date').show();
            $('#request_due_date').focus();
            return true;
        }

        $('#request_from_employee_info_btn').attr('disabled', true);
        $('.loading1').show();
        var url_ = '<?php echo site_url('user/employee/send_my_request'); ?>';
        $.post(
                url_,
                {
                    'id_strings[]': items,
                    'values[]': stuffs,
                    'due_date': request_due_date_
                },
        function () {
            $('.loading1').hide();
            $('#request_from_employee_modal').modal('hide');
            $('#request_from_employee_info_btn').attr('disabled', false);
        }
        );

        return true;

    });
</script>