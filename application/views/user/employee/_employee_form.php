<script>
    var Const_NIGERIA = '<?= NIGERIA ?>';
</script>
<div ng-app="EmployeeApp">
    <form action="" method="POST" enctype="multipart/form-data" class='form-horizontal form-wizard form-wysiwyg' id="ss" ng-controller="formCtrl">
        <div>
            <h4 class="light-title" style="padding: 0px 0 15px;">Personal Information</h4>
            <div class="control-group">
                <label for="first_name" class="control-label">First name:</label>
                <div class="controls">
                    <input type="text" name="first_name" id="first_name" class="input-xlarge" required />
                </div>
            </div>
            <div class="control-group">
                <label for="last_name" class="control-label">Last name:</label>
                <div class="controls">
                    <input type="text" name="last_name" id="last_name" class="input-xlarge" required />
                </div>
            </div>
            <div class="control-group">
                <label for="id_gender" class="control-label">Gender:</label>
                <div class="controls">
                    <select name="id_gender" id="id_gender" required class="select2-me input-xlarge">
                        <option value="" selected>Select Gender</option>
                        <option value="1">Male</option>
                        <option value="2">Female</option>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label for="id_marital_status" class="control-label">Marital Status:</label>
                <div class="controls">
                    <select name="id_marital_status" id="id_marital_status" required class="select2-me input-xlarge">
                        <option value="" selected>Select Status</option>
                        <option value="1">Single</option>
                        <option value="2">Married</option>
                    </select>
                </div>
            </div>	
            <div class="control-group">
                <label for="additionalfield" class="control-label">Personal Number:</label>
                <div class="controls">
                    <select required id='personal_phone_code' name="personal_phone_code" class="select2-me input-medium">
                        <option value="" selected>Select</option>
                        <?php if (!empty($countries)) : ?>
                            <?php foreach ($countries as $c) : ?>
                                <option value="<?php echo $c->code ?>"><?php echo $c->country . ' - ' . $c->code ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    &nbsp;
                    <input type="text" name="personal_number" id="personal_number" required />
                    <div class="check-line" style="margin-top:-25px; margin-left: 400px;">
                        <input type="radio" id="default_acct" name="primary_phone_number" value="1" checked> 
                        <label class='inline' for="default_acct">Set as primary contact number</label> 
                    </div>
                </div>

            </div> 
            <div class="control-group">
                <label for="dob" class="control-label">Birth Date:</label>
                <div class="controls">
                    <input type="text" id="dob" name="dob" class="datepick" required />
                </div>
            </div>
            <div class="control-group">
                <label for="id_nationality" class="control-label">Country of Origin:</label>
                <div class="controls">
                    <select name="id_nationality" id="id_nationality" class="select2-me input-large">
                        <option value="" selected>Select</option>
                        <?php if (!empty($countries)) : ?>
                            <?php foreach ($countries as $c) : ?>
                                <option value="<?php echo $c->id_country; ?>"><?php echo $c->country; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label for="address" class="control-label">Address:</label>
                <div class="controls">
                    <input type="text" id="address" name="address" class="" required />
                </div>
            </div>
            <div class="control-group">
                <label for="city" class="control-label">City:</label>
                <div class="controls">
                    <input type="text" id="city" name="city" class="" required />
                </div>
            </div>
            <div class="control-group">
                <label for="country" class="control-label">Country:</label>
                <div class="controls">
                    <select name="country" id="country" ng-model="employee.id_country" ng-change="loadStates();" class="select2-me input-large">
                        <?php if (!empty($countries)) : ?>
                            <?php foreach ($countries as $c) : ?>
                                <option value="<?php echo $c->id_country; ?>"><?php echo $c->country; ?></option>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </select> &nbsp; State: &nbsp;
                    <!--<div class="" ng-if="statesStatus === ''">-->
                    <select name="id_state" id="id_state" class="select2-me input-medium" data-rule-required="true" ng-model="employee.id_state">
                        <option ng-repeat="state in states" value="{{state.id}}" ng-selected="employee.id_state.indexOf(state.id + '') > -1">
                            {{state.name}}
                        </option>
                    </select>

                    <!--</div>-->
                </div>
            </div>
            <!--            <div class="control-group">
                            <label for="state" class="control-label">State:</label>
            
                        </div>-->
        </div>


        <div>
            <h4 class="light-title" style="padding: 5px 0 15px;">Work Information</h4>
            <div class="control-group">
                <label for="employment_status" class="control-label">Employment Status:</label>
                <div class="controls">
                    <select name="employment_status" id="employment_status" ng-model="employee.status" required>
                        <option value="">[Select status]</option>
                        <option value="<?php echo USER_STATUS_INACTIVE; ?>">Inactive</option>
                        <option value="<?php echo USER_STATUS_ACTIVE; ?>">Active</option>
                        <option value="<?php echo USER_STATUS_ACTIVE_NO_ACCESS; ?>">Active/No access</option>
                        <option value="<?php echo USER_STATUS_SUSPENDED; ?>">Suspended</option>
                        <option value="<?php echo USER_STATUS_TERMINATED; ?>">Terminated</option>
                        <option value="<?php echo USER_STATUS_PROBATION_ACCESS; ?>">Probation/Access</option>
                        <option value="<?php echo USER_STATUS_PROBATION_NO_ACCESS; ?>">Probation/No access</option>
                    </select>
                </div>
            </div>

            <div class="control-group" style="display:none;" id="probation_message_container">
                <label for="managers" class="control-label"></label>
                <div class="controls" id="probation_message_container_txt">

                </div>
            </div>

            <div class="control-group" id="probation_expire_date_container" style="display:none;" >
                <label for="managers" class="control-label">Probation due date:</label>
                <div class="controls">
                    <input type="text" name="probation_expire_date" id="probation_expire_date" value="" class="datepick" />
                </div>
            </div>
            <div class="control-group" id="terminated_expire_date_container" style="display:none;">
                <label for="managers" class="control-label">Effective as of:</label>
                <div class="controls">
                    <input type="text" name="termination_date" id="termination_date" value="" class="datepick" />
                </div>
            </div>
            <div class="control-group">
                <label for="additionalfield" class="control-label">Work Email:</label>
                <div class="controls">
                    <input type="text" name="work_email" id="work_email" required />

                </div>
                <div class="check-line" id="div_no_email" style="margin-left: 400px; margin-top: -30px">
                    <input type="checkbox" name="no_email" id="no_email" value="1" />
                    <label class='inline' for="no_email">Does Not have Email</label> 

                </div>

            </div>

            <div class="control-group">
                <label for="additionalfield" class="control-label">Work Number:</label>
                <div class="controls">
                    <select id='phone_code' name="phone_code" class="select2-me input-medium">
                        <option value="" selected>Select</option>
                        <?php if (!empty($countries)) : ?>
                            <?php foreach ($countries as $c) : ?>
                                <option value="<?php echo $c->code ?>"><?php echo $c->country . ' - ' . $c->code ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    &nbsp;
                    <input type="text" name="work_number" id="work_number" required />
                    <div class="check-line" style="margin-top:-25px; margin-left: 400px">
                        <input type="radio" id="default_acct-2" name="primary_phone_number" value="2" checked> 
                        <label class='inline' for="default_acct-2">Set as primary contact number</label> 
                    </div>
                </div>
            </div> 
            <div class="control-group">
                <label for="additionalfield" class="control-label">Staff ID:</label>
                <div class="controls">
                    <input type="text" name="id_staff" id="id_staff" data-rule-required="false" />
                </div>
            </div>
            <div class="control-group">
                <label for="additionalfield" class="control-label">Department:</label>
                <div class="controls">
                    <select name="department" id="department" required class="select2-me input-large">
                        <option value="">Select</option>
                        <?php if (!empty($departments)) : ?>
                            <?php foreach ($departments as $d) :
                                ?>
                                <option value="<?php echo $d->deptid; ?>"><?php echo $d->departments; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
                <div style="float: left; margin-left: 240px; margin-top:-25px;">
                    <?php if(empty($departments)): ?>
                    <a href="<?= site_url('employee/view_departments') ?>" target="_blank" title="Click here to setup departments">Add Department</a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="control-group">
                <label for="role" class="control-label">Role:</label>
                <div class="controls">
                    <select name="role" id="role" required  class="select2-me input-large">
                        <option value="" selected> Select </option>
                        <?php if (!empty($roles)) : ?>
                            <?php foreach ($roles as $r) : ?>
                                <option value="<?php echo $r->id_role; ?>"><?php echo $r->roles; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
                <div style="float: left; margin-left: 240px; margin-top:-25px;">
                    <?php if(empty($roles)): ?>
                    <a href="<?= site_url('employee/view_roles') ?>" target="_blank" title="Click here to setup roles">Add Roles</a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="control-group">
                <label for="level" class="control-label">Level:</label>
                <div class="controls">
                    <select name="level" id="level" class="select2-me input-large">
                        <option value="" selected> Select </option>
                        <?php if (!empty($levels)) : ?>
                            <?php foreach ($levels as $l): ?>
                                <option value="<?php echo $l->id_company_level; ?>"><?php echo $l->level_name; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
                <div style="float: left; margin-left: 240px; margin-top:-25px;">
                    <?php if(empty($levels)): ?>
                    <a href="<?= site_url('employee/view_roles') ?>" target="_blank" title="Click here to setup levels">Add Level</a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="control-group">
                <label for="level" class="control-label">Pay Grade:</label>
                <div class="controls">
                    <select name="paygrade" id="paygrade" class="select2-me input-large">
                        <option value="" selected> Select </option>
                        <?php if (!empty($payGrades)) : ?>
                            <?php foreach ($payGrades as $pg): ?>
                                <option value="<?php echo $pg->id_payroll_paygrade; ?>"><?= ucfirst($pg->pay_grade); ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
                <div style="float: left; margin-left: 240px; margin-top:-25px;">
                    <?php if(empty($payGrades)): ?>
                    <a href="<?= site_url('payroll/paygrades') ?>" target="_blank" title="Click here to setup paty grades">Add Pay Grade</a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="control-group">
                <label for="job_type" class="control-label">Job Type:</label>
                <div class="controls">
                    <div class="input-xlarge">
                        <select name="job_type[]" id="job_type" multiple="multiple" class="chosen-select wizard-ignore input-xxlarge">
                            <?php
                            if (!empty($job_types)) {
                                foreach ($job_types as $j) {
                                    ?>
                                    <option value="<?php echo $j->id_type; ?>"><?php echo $j->name; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label for="company_location" class="control-label">Company location:</label>
                <div class="controls">
                    <select name="company_location" id="company_location" required class="select2-me input-large">
                        <option value="" selected> Select </option>
                        <?php if (!empty($company_locations)) : ?>
                            <?php foreach ($company_locations as $item) : ?>
                                <option value="<?php echo $item->id_location; ?>"><?php echo $item->location_tag; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label for="managers" class="control-label">Manager:</label>
                <div class="controls">
                    <select name="manager" id="manager" class="select2-me input-large">
                        <option value="0" selected>N/A</option>
                        <?php
                        if (!empty($managers)) {
                            foreach ($managers as $m) {
                                ?>
                                <option value="<?php echo $m->id_user; ?>"><?php echo $m->first_name . ' ' . $m->last_name; ?></option>
                                <?php
                            }
                        }
                        ?>	
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label for="managers" class="control-label">Employment date:</label>
                <div class="controls">
                    <input type="text" id="employment_date" name="employment_date" class="datepick" required />
                </div>
            </div>
            <div class="control-group">
                <label for="text" class="control-label">Access level</label>
                <div class="controls">
                    <select name="access_level" id="access_level" required class="select2-me input-large">
                        <option value="" selected>Select</option>
                        <?php if (!empty($accounts)) : ?>
                            <?php foreach ($accounts as $a) : ?>
                                <?php if ($a->name !== 'Administrator') : ?>
                                    <option value="<?php echo $a->accounttype; ?>"><?php echo $a->name; ?></option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>	
                    </select>

                </div>
                <div class="check-line" id="div_no_email" style="margin-left: 400px; margin-top: -30px">
                    <label class="inline" for="is_ceo"><input type="checkbox" value="2" name="is_manager" id="is_ceo" /> Is CEO/Root </label> 

                </div>
            </div>

        </div>

        <div>
            <h4 class="light-title" style="padding: 5px 0 15px;">Profile Picture</h4>
            <div class="control-group">
                <div class="controls">
                    <div class="check-demo-col">
                        <div class="check-line">
                            <input type="radio" id="complete_now" ng-model="picture" value="now" name="profile_pic_select" > 
                            <label class='inline' for="complete_now">Complete This Now</label>
                        </div>
                        <div class="check-line">
                            <input type="radio" id="complete_later" ng-model="picture" value="later" name="profile_pic_select"  > 
                            <label class='inline' for="complete_later">Complete this later</label>
                        </div>
                        <div class="check-line">
                            <input type="radio" id="request_from_employee" ng-model="picture" value="request" name="profile_pic_select" > 
                            <label class='inline' for="request_from_employee">Request From Employee</label>
                        </div>
                    </div>
                </div>
            </div>
            <!--            <div class="control-group">
                            <div class="controls">
                                <label>
                                    <input type="radio" name="profile_pic_select" ng-model="picture" class="profile_pic_rd" value="now" checked> Complete This Now
                                </label>
                                <label>
                                    <input type="radio" name="profile_pic_select" ng-model="picture" class="profile_pic_rd" value="later"> Complete this later
                                </label>
                                <label class="user_request_data">
                                    <input type="radio" name="profile_pic_select" ng-model="picture" class="profile_pic_rd" value="request"> Request From Employee
                                </label>
                            </div>
                        </div>-->
            <div id="profile_pic_now_container" ng-show="showPicContainer('now')">
                <div class="control-group">
                    <label for="textfield" class="control-label"></label>
                    <div class="controls">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" />
                            </div>
                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                            <div>
                                <span class="btn btn-file">
                                    <span class="fileupload-new">Select file</span>
                                    <span class="fileupload-exists">Change</span>
                                    <input type="file" name="profile_picture" title="Pls upload a profile picture" />
                                </span>
                                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="profile_pic_later_container" ng-show="showPicContainer('later')"></div>	
            <div id="profile_pic_request_container" ng-show="showPicContainer('request')">
                <div class="control-group">
                    <label for="" class="control-label"></label>
                    <div class="controls">
                        Instructions<br />
                        <textarea style="width:500px;" name="profile_pic_request_instructions" data-rule-required="true"></textarea>
                    </div>
                </div>
                <div class="control-group">
                    <label for="" class="control-label"></label>
                    <div class="controls">
                        Due date:<br />
                        <input type="text" id="profile_pic_request_date" data-rule-required="true" value="" name="profile_pic_request_date" class="datepick ui-wizard-content" />
                    </div>
                </div>
            </div>	

        </div>


        <div class="control-group">
            <div class="controls">
                <input type="submit" class="btn btn-success btn-large" value="Submit" id="next">
            </div>
        </div>
    </form>
</div>

<script src="<?= site_url('/js/employee.js') ?>"></script>
<script>
                            $('#no_email').click(function () {
                            if ($(this).is(':checked')) {
                            $('#work_email').attr('disabled', 'disabled');
                    } else {
                            $('#work_email').removeAttr('disabled');
                    }
                            });

                            $('#employment_status').change(function () {

                    var id_status = $(this).val();

                                $('#probation_expire_date_container').hide();
                                $('#probation_message_container').hide();
                                $('#probation_message_container_txt').html('');
                    $('#terminated_expire_date_container').hide();

                    if (id_status == '<?php echo USER_STATUS_PROBATION_NO_ACCESS; ?>') {
                            $('#probation_expire_date_container').show();
                                $('#probation_message_container').show();
                                $('#probation_message_container_txt').html('This employee is on probation and has no access to employee Portal. Please set probation end-date below');
                    }

                    if (id_status == '<?php echo USER_STATUS_PROBATION_ACCESS; ?>') {
                            $('#probation_expire_date_container').show();
                                $('#probation_message_container').show();
                                $('#probation_message_container_txt').html('This employee is on probation and has access to employee Portal. Please set probation end-date below');
                            }

                            if (id_status == '<?php echo USER_STATUS_ACTIVE_NO_ACCESS; ?>') {
                                        $('#probation_message_container').show();
                                    $('#probation_message_container_txt').html('This employee will not have access to the employee portal. Recommended for semi/unskilled employees');
                    }

                    if (id_status == '<?php echo USER_STATUS_TERMINATED; ?>') {
                                $('#terminated_expire_date_container').show();
                    }

                            /* CHECK IF STATUS IS ACTIVE OR PROBATION ACCESS */
                    if (id_status == '<?php echo USER_STATUS_PROBATION_ACCESS; ?>' || id_status == '<?php echo USER_STATUS_ACTIVE; ?>') {
                                    $('#div_no_email').hide();
                    } else {
                                    $('#div_no_email').show();
                    }

                                                if (id_status != '<?php echo USER_STATUS_ACTIVE; ?>' && id_status != '<?php echo USER_STATUS_PROBATION_ACCESS; ?>') {
                                                                $('.user_request_data').each(function () {
                                                        $(this).find('input:radio').removeAttr("checked");
                                                    $(this).hide();
                            $(this).prev().find('input:radio').click();
                        });
                    } else {
                        $('.user_request_data').each(function () {
                            $(this).show();
                        });
                    }

                });
</script>
