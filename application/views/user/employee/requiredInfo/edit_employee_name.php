<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h4 class="modal-title" id="myModalLabel"><?php echo $page_title; ?></h4>
</div>

<form class="form-horizontal" name="add" id="add" method="POST" action="<?= $form_action ?>">
    <div class="modal-body">
        <div id="success_msg_p1" class="alert alert-warning hide"> Processing... please wait</div>
        <div id="success_msg_p" class="alert alert-success hide"> Record updated successfully</div>

        <div class="control-group">
            <label class="control-label" for="first_name">First Name</label>
            <div class="controls">
                <input type="text" required id="first_name" name="first_name" value="<?= $first_name ?>" title="First Name" />
                <div class="error hide" id="first_name_err">First name is required</div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="middle_name">Middle Name</label>
            <div class="controls">
                <input type="text" id="middle_name" name="middle_name" value="<?= $middle_name ?>" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="last_name">Last Name</label>
            <div class="controls">
                <input required type="text" id="last_name" name="last_name" value="<?= $last_name ?>" title="Last Name" />
                <div class="error hide" id="last_name_err">Last name is required</div>
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <input type="button" class="btn btn-primary pull-right" id="update_btn" value="Update">
    </div>
</form>

<script>
    $('#update_btn').click(function () {
        if ($('#first_name').val().trim() === '') {
            $('#first_name_err').show();
            $('#first_name').focus();
            return false;
        } else {
            $('#first_name_err').hide();
        }

        if ($('#last_name').val().trim() === '') {
            $('#last_name_err').show();
            $('#last_name').focus();
            return false;
        } else {
            $('#last_name_err').hide();
        }
        
        $('#update_btn').attr('disabled', true);

        $('#success_msg_p1').show();

        var form_data = {
            first_name: $("#first_name").val(),
            middle_name: $("#middle_name").val(),
            last_name: $("#last_name").val()
        };

        $.ajax({
            url: '<?php echo base_url() ?>employee/edit_employee_name/<?= $id_string ?>',
                        type: 'POST',
                        data: form_data,
                        success: function (msg) {
                            if (msg == 'success')
                            {
                                $('#success_msg_p1').hide();
                                $('#success_msg_p').show();
                                location.reload();
                                return false;
                            } else {
                                $('#success_msg_p1').html('An Error occur while processing your request');
                                $('#success_msg_p1').show();
                                $('#update_btn').attr('disabled', false);
                                return false;
                            }
                        }
                    });
                });


</script>