<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h4 class="modal-title" id="myModalLabel"><?php echo $page_title; ?></h4>
</div>

<form class="form-horizontal" name="add" id="add" method="POST" action="<?= $form_action ?>">
    <div class="modal-body">
        <div id="success_msg_p1" class="alert alert-warning hide">Processing... please wait</div>
        <div id="success_msg_p" class="alert alert-success hide"> Record updated successfully </div>
        <div id="success_msg_p3" class="alert alert-error hide">Staff ID already exists </div>

        <div class="control-group">
            <label class="control-label" for="id_string">Staff ID</label>
            <div class="controls">
                <input type="text" id="staff_id_string" name="staff_id_string" value="<?= $id_staff ?>" title="Staff ID" />
                <div class="error hide" id="id_string_err">Staff ID is required</div>
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <input type="button" class="btn btn-primary pull-right" id="update_id_btn" value="Update">
    </div>
</form>

<script>
    $('#update_id_btn').click(function () {
        if ($('#staff_id_string').val() === '') {
            $('#id_string_err').show();
            $('#staff_id_string').focus();
            return false;
        } else {
            $('#id_string_err').hide();
        }
        
        $('#update_id_btn').attr('disabled', true);

        $('#success_msg_p1').show();

        var new_staff_id = $("#staff_id_string").val();
        var form_data = {
            id_string: new_staff_id
        };

        $.ajax({
            url: '<?php echo base_url() ?>user/requiredInfo/update_staff_id/<?= $id_user ?>',
                        type: 'POST',
                        data: form_data,
                        success: function (msg) {
                            if (msg == 'success')
                            {
                                $('#success_msg_p1').hide();
                                $('#success_msg_p').show();
                                location.reload();
                                return false;
                            } else if (msg == 'exist') {
                                $('#success_msg_p1').hide();;
                                $('#success_msg_p').hide();
                                $('#success_msg_p3').show();
                                $('#update_id_btn').attr('disabled', false);
                                return false;

                            } else {
                                $('#success_msg_p1').html('An Error occur while processing your request');
                                $('#success_msg_p1').show();
                                $('#update_id_btn').attr('disabled', false);
                                return false;
                            }
                        }
                    });
                });



</script>