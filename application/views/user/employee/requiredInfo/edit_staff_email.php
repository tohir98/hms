<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h4 class="modal-title" id="myModalLabel"><?php echo $page_title; ?></h4>
</div>

<form class="form-horizontal" name="add" id="add" method="POST" action="<?= $form_action ?>">
    <div class="modal-body">
        <div id="success_msg_p1" class="alert warning hide"> </div>
        <div id="success_msg_p" class="alert alert-success hide"> </div>

        <div class="control-group">
            <label class="control-label" for="staff_email">Email</label>
            <div class="controls">
                <input type="text" id="staff_email_add" name="staff_email_add" value="<?= $email ?>" title="Email Address" />
                <div class="error hide" id="staff_email_err">Email is required</div>
                <div class="error hide" id="staff_email_valid_err">Invaid email address</div>
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <input type="button" class="btn btn-primary pull-right" id="update_email_btn" value="Update">
    </div>
</form>

<script>
    $('#update_email_btn').click(function () {

        if ($('#staff_email_add').val() == '') {
            $('#staff_email_err').show();
            $('#staff_email_add').focus();
            return false;
        } else {
            $('#staff_email_err').hide();
        }

        if (!validateEmail($('#staff_email_add').val())) {
            $('#staff_email_valid_err').show();
            $('#staff_email_add').focus();
            return false;
        } else {
            $('#staff_email_valid_err').hide();
        }
        
        $('#update_email_btn').attr('disabled', true);

        $('#success_msg_p1').html('Processing... please wait');
        $('#success_msg_p1').show();

        var form_data = {
            email: $("#staff_email_add").val()
        };

        $.ajax({
            url: '<?php echo base_url() ?>user/requiredInfo/update_staff_email/<?= $id_user ?>',
                        type: 'POST',
                        data: form_data,
                        success: function (msg) {
                            if (msg == 'success')
                            {
                                $('#success_msg_p1').hide();
                                $('#success_msg_p').html('Record updated successfully');
                                $('#success_msg_p').show();
                                location.reload();
                                return false;
                            } else if (msg == 'exist') {
                                $('#success_msg_p1').html('Email already exists');
                                $('#success_msg_p1').show();
                                $('#update_id_btn').attr('disabled', false);
                                return false;

                            } else {
                                $('#success_msg_p1').html('An Error occur while processing your request');
                                $('#success_msg_p1').show();
//                                location.reload();
                            }
                        }
                    });
                });

                function validateEmailEntries() {


                }

                function validateEmail($email) {
                    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                    return emailReg.test($email);
                }

</script>