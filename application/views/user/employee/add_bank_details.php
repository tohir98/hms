
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h4 class="modal-title" id="myModalLabel">Add Bank details</h4>
</div>

<?php if ($success_message != '') { ?>
    <div class="alert alert-success" style="margin-top:20px;">
        <button data-dismiss="alert" class="close" type="button">×</button>
        <strong>Success!</strong> <?php echo $success_message; ?>
    </div>
<?php } ?>
<?php if (!empty($error_messages)) { ?>
    <div class="alert alert-danger" style="margin-top:20px;">
        <button data-dismiss="alert" class="close" type="button">×</button>
        <?php
        foreach ($error_messages as $err_message) {
            ?>
            <strong><?php echo $err_message; ?></strong><br />
        <?php } ?>
    </div>
    <?php
}
?>

<form action="<?= site_url('employee/edit_bank_details/'.$this->uri->segment(3).'/'.$this->uri->segment(4)) ?>" method="POST" class="form-horizontal form-wizard form-wysiwyg form-striped" id="edit_profile" enctype="multipart/form-data" style="border:1px solid #DDDDDD">
    <div class="modal-body">

        <div class="control-group">
            <label class="control-label" for="first_name">Account name:</label>
            <div class="controls">
                <input type="text" data-rule-required="true" class="input-xlarge" id="account_name" name="account_name" value="<?php echo $profile->bank_account_name; ?>" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="first_name">Bank:</label>
            <div class="controls">
                <select name="bank" id="bank" data-rule-required="true">
                    <option value="">Select Bank</option>
                    <?php
                    foreach ($banks as $bank) {
                        if ($bank->id_bank == $profile->id_bank) {
                            $sel = ' selected="selected" ';
                        } else {
                            $sel = '';
                        }
                        ?>
                        <option value="<?php echo $bank->id_bank; ?>" <?php echo $sel; ?>><?php echo $bank->name; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="first_name">Account type:</label>
            <div class="controls">
                <select name="account_type" id="account_type" data-rule-required="true">
                    <option value="">Select Account Type</option>
                    <?php
                    foreach ($account_types as $index => $type) {
                        if ($index == $profile->id_bank_account_type) {
                            $sel = ' selected="selected" ';
                        } else {
                            $sel = '';
                        }
                        ?>
                        <option value="<?php echo $index; ?>" <?php echo $sel; ?>><?php echo $type; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="first_name">Account number:</label>
            <div class="controls">
                <input type="text" id="account_number" name="account_number" data-rule-required="true" value="<?php echo $profile->bank_account_number; ?>" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="first_name">Set Default:</label>
            <div class="controls">
                <div class="check-line">
                    <input type="checkbox" id="default_acct" name="default_acct" class='icheck-me' data-skin="square" data-color="blue"> <label class='inline' for="c6">&nbsp;</label> 
                </div>
            </div>
        </div>

    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    </div>
</form>

<script type="text/javascript">

    $(document).ready(function () {

        $('#edit_profile').validate({
            rules: {
                account_name: {
                    required: true
                },
                bank: {
                    required: true
                },
                account_type: {
                    required: true
                },
                account_number: {
                    required: true
                }
            },
            messages: {
                account_name: {
                    required: "Enter Account name"
                },
                bank: {
                    required: "Select Bank"
                },
                account_type: {
                    required: "Select Account type"
                },
                account_number: {
                    required: "Enter Account number"
                }
            }
        });

    });

</script>