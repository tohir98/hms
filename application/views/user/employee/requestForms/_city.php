<?php if ($this->user_auth->have_perm(EDIT_MY_HOME_ADDRESS)) { ?>
    <p>&nbsp;</p>
    <form action="" method="POST" class="form-horizontal form-wysiwyg form-striped solid_border" id="edit_city_frm" >
        <h4>Edit City</h4>
        <div class="control-group">
            <label for="city" class="control-label">City:</label>
            <div class="controls">
                <input type="text" id="city" name="city" class="" data-rule-required="true" />
            </div>
            <div class="error" style="display:none; margin-top: 5px;" id="city_err_msg">
                <strong>Field is required!</strong>
            </div>
        </div>
        <div class="control-group">
            <label for="managers" class="control-label"></label>
            <div class="controls">
                <input type="hidden" name="city_id_temp_request" id="city_id_temp_request" value="<?= $r->id_temp_request; ?>" />
                <input type="hidden" name="city_id_task" id="city_id_task" value="<?php echo $id_task; ?>"  />
                <input type="button" id="update_city_btn" class="btn btn-primary" value="Update" />
            </div>
        </div>
    </form>
<?php } ?>

    <script>
     $('#update_city_btn').click(function () {

            var city_ = $('#city').val();
            var id_task_ = $('#city_id_task').val();

            if (city_ === '') {
                $('#city_err_msg').show();
                return false;
            } else {
                $('#city_err_msg').hide();
                show_loader();
            }

            var url_ = '<?php echo site_url('user/personal/update_city'); ?>';
            $.post(
                    url_,
                    {
                        'city': city_,
                        'id_task': id_task_,
                        'id_temp_request': $('#city_id_temp_request').val()
                    },
            function (res) {
                hide_loader();
                $('#edit_city_frm').html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">x</button>' + res + '</div>');
            }
            );

            return false;

        });
    </script>
        