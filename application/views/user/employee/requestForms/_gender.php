<?php if ($this->user_auth->have_perm(EDIT_MY_HOME_ADDRESS)) { ?>
    <p>&nbsp;</p>
    <form action="" method="POST" class="form-horizontal form-wysiwyg form-striped solid_border" id="edit_gender_frm" >
        <h4>Edit Gender</h4>
        <div class="control-group">
            <label class="control-label" for="id_gender">Gender:</label>
            <div class="controls">
                <select name="id_gender" id="id_gender">
                    <option value="" selected>Select gender</option>
                    <option value="1">Male</option>
                    <option value="2">Female</option>
                </select>
                <div class="error" style="display:none; margin-top: 5px;" id="gender_err_msg">
                    <strong>Field is required!</strong>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label for="managers" class="control-label"></label>
            <div class="controls">
                <input type="hidden" name="id_gender_id_temp_request" id="id_gender_id_temp_request" value="<?= $r->id_temp_request; ?>" />
                <input type="hidden" name="id_gender_id_task" id="id_gender_id_task" value="<?php echo $id_task; ?>"  />
                <input type="button" id="update_gender_btn" class="btn btn-primary" value="Update" />
            </div>
        </div>
    </form>
<?php } ?>

    <script>
     $('#update_gender_btn').click(function () {

            var id_gender_ = $('#id_gender').val();
            var id_task_ = $('#id_gender_id_task').val();

            if (id_gender_ === '') {
                $('#gender_err_msg').show();
                return false;
            } else {
                $('#gender_err_msg').hide();
                show_loader();
            }

            var url_ = '<?php echo site_url('user/personal/update_gender'); ?>';
            $.post(
                    url_,
                    {
                        'gender': id_gender_,
                        'id_task': id_task_,
                        'id_temp_request': $('#id_gender_id_temp_request').val()
                    },
            function (res) {
                hide_loader();
                $('#edit_gender_frm').html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">x</button>' + res + '</div>');
            }
            );

            return false;

        });
    </script>
        