<?php if ($this->user_auth->have_perm(EDIT_MY_HOME_ADDRESS)) { ?>
    <p>&nbsp;</p>
    <form action="" method="POST" class="form-horizontal form-wysiwyg form-striped solid_border" id="edit_state_frm" >
        <h4>Edit State</h4>
        <div class="control-group">
            <label class="control-label" for="id_state">State:</label>
            <div class="controls">
                <select name="id_state" id="id_state">
                    <?php
                    if (!empty($states)) :
                        foreach ($states as $s) : ?>
                            
                                <option value="<?php echo $s->id_state; ?>"><?php echo $s->state; ?></option>
                            
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
                <div class="error" style="display:none; margin-top: 5px;" id="id_state_err_msg">
                    <strong>Field is required!</strong>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label for="managers" class="control-label"></label>
            <div class="controls">
                <input type="hidden" name="id_state_id_temp_request" id="id_state_id_temp_request" value="<?= $r->id_temp_request; ?>" />
                <input type="hidden" name="id_state_id_task" id="id_state_id_task" value="<?php echo $id_task; ?>"  />
                <input type="button" id="update_state_btn" class="btn btn-primary" value="Update" />
            </div>
        </div>
    </form>
<?php } ?>

<script>
    $('#update_state_btn').click(function () {

        var id_state_ = $('#id_state').val();
        var id_task_ = $('#id_state_id_task').val();

        if (id_state_ === '') {
            $('#id_state_err_msg').show();
            return false;
        } else {
            $('#id_state_err_msg').hide();
            show_loader();
        }

        var url_ = '<?php echo site_url('user/personal/update_state'); ?>';
        $.post(
                url_,
                {
                    'id_state': id_state_,
                    'id_task': id_task_,
                    'id_temp_request': $('#id_state_id_temp_request').val()
                },
        function (res) {
            hide_loader();
            $('#edit_state_frm').html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">x</button>' + res + '</div>');
        }
        );

        return false;

    });
</script>
