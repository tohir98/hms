<?php if ($this->user_auth->have_perm(EDIT_MY_HOME_ADDRESS)) { ?>
    <p>&nbsp;</p>
    <form action="" method="POST" class="form-horizontal form-wysiwyg form-striped solid_border" id="edit_country_frm" >
        <h4>Edit Country</h4>
        <div class="control-group">
            <label class="control-label" for="id_country">Country:</label>
            <div class="controls">
                <select name="id_country" id="id_country">
                    <?php
                    if (!empty($countries)) :
                        foreach ($countries as $c) :
                            if ($c->id_country == 115) :
                                ?>
                                <option value="<?php echo $c->id_country; ?>" selected><?php echo $c->country; ?></option>
                            <?php else: ?>
                                <option value="<?php echo $c->id_country; ?>"><?php echo $c->country; ?></option>
                            <?php endif; ?>
                            <?php
                        endforeach;
                    endif;
                    ?>
                </select>
                <div class="error" style="display:none; margin-top: 5px;" id="id_country_err_msg">
                    <strong>Field is required!</strong>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label for="managers" class="control-label"></label>
            <div class="controls">
                <input type="hidden" name="id_country_id_temp_request" id="id_country_id_temp_request" value="<?= $r->id_temp_request; ?>" />
                <input type="hidden" name="id_country_id_task" id="id_country_id_task" value="<?php echo $id_task; ?>"  />
                <input type="button" id="update_country_btn" class="btn btn-primary" value="Update" />
            </div>
        </div>
    </form>
<?php } ?>

<script>
    $('#update_country_btn').click(function () {

        var id_country_ = $('#id_country').val();
        var id_task_ = $('#id_country_id_task').val();

        if (id_country_ === '') {
            $('#id_country_err_msg').show();
            return false;
        } else {
            $('#id_country_err_msg').hide();
            show_loader();
        }

        var url_ = '<?php echo site_url('user/personal/update_country'); ?>';
        $.post(
                url_,
                {
                    'country': id_country_,
                    'id_task': id_task_,
                    'id_temp_request': $('#id_country_id_temp_request').val()
                },
        function (res) {
            hide_loader();
            $('#edit_country_frm').html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">x</button>' + res + '</div>');
        }
        );

        return false;

    });
</script>
