<?php if ($this->user_auth->have_perm(EDIT_MY_BIRTHDAY)) { ?>

    <form action="" method="POST" class="form-horizontal form-wysiwyg form-striped solid_border" id="edit_birth_day">
        <h4>Edit Birthday</h4>

        <div class="control-group">
            <label class="control-label" for="date_of_birth">Date of birth:</label>
            <div class="controls">
                <input ng-required="true" class="datepick" type="text" name="dob" id="dob" value="<?= date('m/d/Y', strtotime($profile->dob)); ?>" />
                <div class="error" style="display:none; margin-top: 5px;" id="dob_err_msg">
                    <strong>Field is required!</strong>
                </div>
            </div>

        </div>
        <div class="control-group">
            <label for="managers" class="control-label"></label>
            <div class="controls">
                <input type="hidden" name="birth_day_id_task" id="birth_day_id_task" value="<?= $id_task; ?>"  />
                <input type="hidden" name="birth_day_id_temp_request" id="birth_day_id_temp_request" value="<?= $r->id_temp_request; ?>" />
                <input type="button" id="update_dob_btn" class="btn btn-primary" value="Update" />
            </div>
        </div>
    </form>
<?php } ?>

<script>
    $('#update_dob_btn').click(function () {

        var dob_ = $('#dob').val();
        var id_task_ = $('#birth_day_id_task').val();
        if (dob_ === '') {
            $('#dob_err_msg').show();
            return false;
        } else {
            $('#dob_err_msg').hide();
            show_loader();
        }

        var url_ = '<?php echo site_url('user/personal/update_birthday'); ?>';
        $.post(
                url_,
                {
                    'dob': dob_,
                    'id_task': id_task_,
                    'id_temp_request': $('#birth_day_id_temp_request').val()
                },
        function (res) {
            hide_loader();
            $('#edit_birth_day').html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">x</button>' + res + '</div>');
        }
        );

        return false;

    });
</script>