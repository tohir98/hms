<?php if ($this->user_auth->have_perm(EDIT_MY_HOME_ADDRESS)) { ?>
    <p>&nbsp;</p>
    <form action="" method="POST" class="form-horizontal form-wysiwyg form-striped solid_border" id="edit_home_address_frm" >
        <h4>Edit Home Address</h4>
        <div class="control-group">
            <label class="control-label" for="address">Home address:</label>
            <div class="controls">
                <input ng-required="true" type="text" id="address" value="<?php echo $profile->adress; ?>" name="address" class="" />
                <div class="error" style="display:none; margin-top: 5px;" id="address_err_msg">
                    <strong>Field is required!</strong>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label for="managers" class="control-label"></label>
            <div class="controls">
                <input type="hidden" name="address_id_temp_request" id="address_id_temp_request" value="<?= $r->id_temp_request; ?>" />
                <input type="hidden" name="address_id_task" id="address_id_task" value="<?php echo $id_task; ?>"  />
                <input type="button" id="update_address_btn" class="btn btn-primary" value="Update" />
            </div>
        </div>
    </form>
<?php } ?>

    <script>
     $('#update_address_btn').click(function () {

            var address_ = $('#address').val();
            var id_task_ = $('#address_id_task').val();
            if (address_ === '') {
                $('#address_err_msg').show();
                return false;
            } else {
                $('#address_err_msg').hide();
                show_loader();
            }

            var url_ = '<?php echo site_url('user/personal/update_address'); ?>';
            $.post(
                    url_,
                    {
                        'address': address_,
                        'id_task': id_task_,
                        'id_temp_request': $('#address_id_temp_request').val()
                    },
            function (res) {
                hide_loader();
                $('#edit_home_address_frm').html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">x</button>' + res + '</div>');
            }
            );

            return false;

        });

    </script>
        