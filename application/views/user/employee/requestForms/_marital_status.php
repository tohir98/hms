<?php if ($this->user_auth->have_perm(EDIT_MY_HOME_ADDRESS)) { ?>
    <p>&nbsp;</p>
    <form action="" method="POST" class="form-horizontal form-wysiwyg form-striped solid_border" id="edit_marital_status_frm" >
        <h4>Edit Marital Status</h4>
        <div class="control-group">
            <label class="control-label" for="id_marital_status">Marital Status:</label>
            <div class="controls">
                <select name="id_marital_status" id="id_marital_status">
                    <option value="" selected>Select Status</option>
                    <option value="1">Single</option>
                    <option value="2">Married</option>
                </select>
                <div class="error" style="display:none; margin-top: 5px;" id="id_marital_status_err_msg">
                    <strong>Field is required!</strong>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label for="managers" class="control-label"></label>
            <div class="controls">
                <input type="hidden" name="id_marital_status_id_temp_request" id="id_marital_status_id_temp_request" value="<?= $r->id_temp_request; ?>" />
                <input type="hidden" name="id_marital_status_id_task" id="id_marital_status_id_task" value="<?php echo $id_task; ?>"  />
                <input type="button" id="update_marital_btn" class="btn btn-primary" value="Update" />
            </div>
        </div>
    </form>
<?php } ?>

<script>
    $('#update_marital_btn').click(function () {

        var id_marital_status_ = $('#id_marital_status').val();
        var id_task_ = $('#id_marital_status_id_task').val();

        if (id_marital_status_ === '') {
            $('#id_marital_status_err_msg').show();
            return false;
        } else {
            $('#id_marital_status_err_msg').hide();
            show_loader();
        }

        var url_ = '<?php echo site_url('user/personal/update_marital_status'); ?>';
        $.post(
                url_,
                {
                    'marital_status': id_marital_status_,
                    'id_task': id_task_,
                    'id_temp_request': $('#id_marital_status_id_temp_request').val()
                },
        function (res) {
            hide_loader();
            $('#edit_marital_status_frm').html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">x</button>' + res + '</div>');
        }
        );

        return false;

    });
</script>
