<form action="" method="POST" class="form-horizontal form-wysiwyg form-striped solid_border" id="pension_details_frm" >
    <h4><?php echo $requests[$b->request_type]; ?></h4>

    <?php
    foreach ($entries as $e) {
        if ($e->request_type == $b->request_type) {
            $pos = strpos($e->field_name, '.');
            $field1 = substr($e->field_name, $pos + 1, strlen($e->field_name));
            $field_name = ucfirst(str_replace('_', ' ', $field1));

            if ($e->field_name == 'users.id_pfs') {
                $field_name = 'PFA';
                foreach ($pfa as $pf) {
                    if ($pf->id_pension_fund_administrator == $e->field_value) {
                        $field_val = $pf->name;
                    }
                }
                ?>
                <div class="control-group">
                    <label class="control-label" for="address"><?php echo $field_name ?>:</label>
                    <div class="controls">
                        <input ng-required="true" type="text" id="address" value="<?php echo $field_val; ?>" readonly="" />
                    </div>
                </div>
            <?php } else { ?>
                <div class="control-group">
                    <label class="control-label" for="address"><?php echo $field_name ?>:</label>
                    <div class="controls">
                        <input ng-required="true" type="text" id="address" value="<?php echo $e->field_value; ?>" readonly="" />
                    </div>
                </div>
            <?php
            }
        }
    }
    ?>

    <div class="controls" style=" margin-top: 5px; margin-bottom: 5px;">
        <a type="button" data-request_type='<?php echo $b->request_type; ?>' data-form_id='pension_details_frm' class="btn btn-primary approve_modal">Approve</a>
        <a type="button" data-request_type='<?php echo $b->request_type; ?>' data-form_id='pension_details_frm' class="btn btn-warning rework_request">Re-Work</a>
        <a type="button" data-request_type='<?php echo $b->request_type; ?>' data-form_id='pension_details_frm' class="btn btn-danger decline_request">Decline</a>
    </div>
</form>
<p> &nbsp;</p>