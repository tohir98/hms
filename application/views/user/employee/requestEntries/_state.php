<form action="" method="POST" class="form-horizontal form-wysiwyg form-striped solid_border" id="state_frm" >
    <h4><?php echo $requests[$b->request_type]; ?></h4>

    <?php
    foreach ($entries as $e) :
        if ($e->request_type == $b->request_type) :
            $pos = strpos($e->field_name, '.');
            $field1 = substr($e->field_name, $pos + 1, strlen($e->field_name));
            $field1 = ucfirst(str_replace('_', ' ', $field1));
            ?>
            <div class="control-group">
                <label class="control-label" for="<?= $field1 ?>">State:</label>
                <div class="controls">
                    <?php foreach ($states as $s) {
                                    if ($s->id_state == $e->field_value) :
                                        $state = $s->state;
                                    endif;
                                } ?>
                    <input ng-required="true" type="text" id="<?= $field1 ?>" value="<?php echo $state; ?>" readonly="" />
                </div>
            </div>
            <?php
        endif;
    endforeach;
    ?>

    <div class="controls" style=" margin-top: 5px; margin-bottom: 5px;">
        <a type="button" data-request_type='<?php echo $b->request_type; ?>' data-form_id='state_frm' class="btn btn-primary approve_modal">Approve</a>
        <a type="button" data-request_type='<?php echo $b->request_type; ?>' data-form_id='state_frm' class="btn btn-warning rework_request">Re-Work</a>
        <a type="button" data-request_type='<?php echo $b->request_type; ?>' data-form_id='state_frm' class="btn btn-danger decline_request">Decline</a>
    </div>
</form>
<p> &nbsp;</p>