<link href="/css/angular-busy.min.css" rel="stylesheet" type="text/css"/>
<!--<link href="/css/datatables.bootstrap.min.css" rel="stylesheet" type="text/css"/>-->

<script src="/js/dirPagination.js"></script>
<script src="/js/personnel.js"></script>
<script src="/js/dirPagination.js"></script>
<script src="/js/angular-busy.min.js"></script>
<script src="/js/angular-datatables.min.js" type="text/javascript"></script>
<style>
    .placeholder{
        height: 300px;
    }
    .avail-0 {
        color: red;
        font-weight: bold
    }
    .avail-1 {
        color: green;
        font-weight: bold
    }
</style>
<!-- Display error/success message -->
<br/>
<?php if ($this->session->userdata('errors') != '') { ?>
<div class="alert alert-danger">
     <button data-dismiss="alert" class="close" type="button">x</button>
     <?php echo $this->session->userdata('errors'); ?>
</div>
<?php $this->session->unset_userdata('errors'); } 
if ($this->session->userdata('confirm') != '') { ?>
<div class="alert alert-success">
     <button data-dismiss="alert" class="close" type="button">x</button>
     <?php echo $this->session->userdata('confirm'); ?>
</div>
<?php $this->session->unset_userdata('confirm'); }  ?>

<!-- Page Header -->
<div class="page-header">
    <div class="pull-left">
        <h1>Analytics</h1>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">
        
    </div>
</div>

<div class="box" ng-app="PersonnelReport">
    
    <div class="box-content nopadding">
        <p>
            <ul class="nav nav-tabs" id="myTab">
                <li>
                    <a href="<?php echo site_url('employee/employee_analytics') ?>">Overview</a>
                </li>
                <li  class="active">
                    <a href="#report" data-toggle="tab">Reports</a>
                </li>
            </ul>
        </p>
                                        
        <div class="tab-content"> 
            <div class="tab-pane active" id="report">
                <a href="<?php echo site_url('employee/employee_analytics') ?>" class="btn btn-warning"><i class=" icon-chevron-left"></i> Back</a>
                <div class="header">
                    <h3>Personnel Records</h3>
                </div>
                
                <div class="box box-bordered">
                    <div class="box-title" style="padding-right:5px;">
                        <h3>
                            <div class="btn-group pull-left">
                                <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">Bulk Action <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><?php echo anchor('#SendRequest', 'Send Request', 'class="send_request" data-toggle="modal"'); ?></li> 
                                </ul>
                            </div>
                        </h3>
                        <a href="<?php echo site_url('user/employee/download_excel') ?>" class="btn btn-primary pull-right">Download Excel <i class="icons icon-download-alt"></i></a>
                    </div>

                    <div class="box-content-padless">
                        <div class="alert alert-error" style="display:none" id="err_msg">
                            <strong>Select Employee(s)!</strong>
                        </div>
                        
                        <form method="post" id="frm_approve" ng-controller="AddController" cg-busy="myPromise">
                            <!-- Use the simple syntax -->
                            
                            <table datatable="ng">
                                <thead>
                                    <tr class='thefilter'>
                                        <th class='with-checkbox'><input type="checkbox" name="check_all" id="check_all"></th>
                                        <th>Staff ID</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Status</th>
                                        <th>Department</th>
                                        <th>Location</th>
                                        <th>Work Email</th>
                                        <th>Work Number</th>
                                        <th>Birthday</th>
                                        <th>Home Address</th>
                                        <th>Personal Number</th>
                                        <th>Emergency I</th>
                                        <th>Emergency II </th>
                                        <th>Available Documents</th>
                                        <th>Bank Account</th>
                                        <th>Pension Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="employee in employees | orderBy : 'first_name' ">
                                        <td> <input type="checkbox" class="_item_checkbox" name="check" value="1" /></td>
                                        <td>{{ employee.id_string }}</td>
                                        <td>{{ employee.first_name | uppercase }}</td>
                                        <td>{{ employee.last_name | uppercase }}</td>
                                        <td>{{ statuses[employee.status] }}</td>
                                        <td>{{ employee.departments }}</td>
                                        <td>{{ employee.location_tag }}</td>
                                        <!--<td>{{ employee.work_email }}</td>-->
                                        <td class="avail-{{employee.work_email}}">{{ employee.work_email ? '✔' : '✖'}}</td>
                                        <td class="avail-{{employee.work_phone}}">{{ employee.work_phone ? '✔' : '✖'}}</td>
                                        <td class="avail-{{employee.home_address}}">{{ employee.home_address ? '✔' : '✖'}}</td>
                                        <td class="avail-{{employee.birthday}}">{{ employee.birthday ? '✔' : '✖'}}</td>
                                        <td class="avail-{{employee.personal_phone}}">{{ employee.personal_phone ? '✔' : '✖'}}</td>
                                        <td class="avail-{{employee.emergency1}}">{{ employee.emergency1 ? '✔' : '✖'}}</td>
                                        <td class="avail-{{employee.emergency2}}">{{ employee.emergency2 ? '✔' : '✖'}}</td>
                                        <td>{{ employee.uploaded_docs }} of {{ employee.documents }}</td>
                                        <td class="avail-{{employee.bank_account}}">{{ employee.bank_account ? '✔' : '✖'}}</td>
                                        <td class="avail-{{employee.pension_details}}">{{ employee.pension_details ? '✔' : '✖'}}</td>
<!--                                        <td>{{ employee.home_address }}</td>-->
                                        <!--<td>{{ employee.birthday }}</td>-->
                                        <!--<td>{{ employee.personal_phone }}</td>-->
                                        <!--<td>{{ employee.emergency1 }}</td>-->
                                        <!--<td>{{ employee.emergency2 }}</td>-->
                                        
<!--                                        <td>{{ employee.bank_account }}</td>
                                        <td>{{ employee.pension_details }}</td>-->
                                    </tr>
                                </tbody>
                            </table>
                            
<!--                            <dir-pagination-controls></dir-pagination-controls>-->
                            
                            <input type="hidden" name="hidden_ref_id" value="<?php echo $this->uri->segment(3); ?>" />
                        </form>
                        <div style="text-align:right;clear:both;">

                        </div>
                    </div>
                  
                </div>
                
            </div> <!-- End Div report -->

        </div>
    </div>    
</div>


<!--Request from employee modal-->
<div class="modal hide fade" id="request_from_employee_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h4 class="modal-title" id="myModalLabel">Request from Employee</h4>
	</div>
	<div class="modal-body">
		<?php if($request_message != '') { ?>
        <div class="alert alert-info">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <strong><?php echo $request_message; ?></strong>
			</div>
		<?php } ?>
		<form method="POST" action="<?php echo site_url('employee/request_profile_personal_info/'.$this->uri->segment(3).'/'.$this->uri->segment(4)); ?>" id="profile_pers_info_request_frm">
		<p>
			
			<input type="hidden" id="request_from_employee_profile_desc" name="request_from_employee_profile_desc" />
			
			Due date
			<br />
			<input type="text" required name="request_due_date" id="request_due_date" class="datepick" />
			<span class="error" id="error_request_due_date" style="display:none;">
				<br />Enter due date
			</span>
		</p>
		</form>
	</div>
	<div class="modal-footer">
		<button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
		<button data-id_dept="" type="button" class="btn btn-primary" id="request_from_employee_info_btn">Send request</button>
	</div>
</div>


<script>
function get_selected_items() {
	
	var items = [];
	var i = 0;
	$('._item_checkbox').each(function() {
		if($(this).is(':checked')) {
			items[i] = $(this).attr('data-id_string');
            
			i++;
		}
	});
	
	return items;
}

function get_selected_stuffs() {
    var stuffs = {};
    //stuffs = [];
	$('._item_checkbox').each(function() {
		if($(this).is(':checked')) {
            var data = $(this).data();
            var id_string = data.id_string;
            delete data.id_string;
            stuffs[id_string] = data;
		}
	});
	console.log(stuffs);
    throw new Error('ford');
	return stuffs;
}

$('.send_request').click(function() {
	
	var items = get_selected_items();
	
	if(!items.length) {
		$('#err_msg').show();
		return true;
	}

    $('#request_from_employee_modal').modal('show');
    return true;
	
});

$('#request_from_employee_info_btn').click(function() {
	
	var request_due_date_ = $('#request_due_date').val();
	var items = get_selected_items();
    var stuffs = get_selected_stuffs();
    
	if($.trim(request_due_date_) == '') {
		$('#error_request_due_date').show();
        $('#request_due_date').focus();
		return true;
	}
	
	var url_ = '<?php echo site_url('user/employee/send_my_request'); ?>';
	$.post(
		url_, 
		{
			'id_strings[]': items, 
			'values[]':stuffs,
			'due_date':request_due_date_
		},
		function() {
			$('#request_from_employee_modal').modal('hide');
		}
	);
	
	return true;
	
});

</script>