<div class="box">
	<div class="box-content">
			<div class="row-fluid">
				<div class="span12">
					<a class="btn btn-warning" href="<?php echo site_url('employee/manager_view'); ?>"><i class="icon-angle-left"></i> Back</a>
					<a class="btn btn-primary" href="#"><i class="icon-print"></i> Print</a>
					<a class="btn btn-primary" href="#"><i class="icon-file"></i> PDF</a>
				</div>
			</div>
			<div class="row-fluid" style="margin-top:20px;">
				<div class="span12">
					<h4><?php echo $profile->first_name . ' ' . $profile->last_name; ?>, <?php echo $profile->roles; ?>, <?php echo $profile->departments; ?></h4>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span2">
					<div class="box">
						<div class="box-title" style="border:1px solid #CCCCCC;height:180px;padding:0;">
							<div class="row-fluid" style="height:180px;overflow:hidden">
								<?php 
								if($profile->profile_picture_name != '') { 
									$profile_pic_url = $this->cdn_api->temporary_url($profile->profile_picture_name);
								} else {
									$profile_pic_url = '/img/profile-default.jpg';
								}
								?>
								<img src="<?php echo $profile_pic_url; ?>" />
							</div>
							<div class="row-fluid">
								<!-- <div class="btn-group span12">
									<a class="btn btn-primary dropdown-toggle span12" data-toggle="dropdown" href="#"><span class="caret"></span> Edit </a>
									<ul class="dropdown-menu dropdown-primary">
										<li>
											<a href="<?php echo site_url('employee/edit_profile_picture/'.$this->uri->segment(3).'/'.$this->uri->segment(4)); ?>">Edit Now</a>
										</li>

									</ul>
								</div> -->
							</div>	
						</div>
						<div class="_box-content">
							
							<div class="span12">
								<p></p>
								<ul class="nav nav-tabs nav-stacked left_menu">
									<li class="active"><a href="#" class="left_menu_button" data-container="work_info_personal_info_container">Profile</a></li>
									<li><a href="#" class="left_menu_button" data-container="emergency_container">Emergency contacts</a></li>
									<li><a href="#" class="left_menu_button" data-container="internal_notes_container">Internal notes</a></li>
									<li><a href="#" class="left_menu_button" data-container="resume_credentials_container">Document Uploads</a></li>
								</ul>
								<p></p>
							</div>
														
						</div>
					</div>
					
				</div>
				<div class="span10">
					<?php
					echo $profile_content;
					echo $emergency_content;
					echo $internal_notes_content;
					echo $resume_content;
					?>
				</div>
			</div>
		
	</div>
</div>
<div class="modal hide fade" id="request_from_employee_profile_pic_model" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h4 class="modal-title" id="myModalLabel">Request from Employee</h4>
	</div>
	<div class="modal-body">
		<?php if($request_message != '') { ?>
			<div class="alert alert-info">
				<button data-dismiss="alert" class="close" type="button">×</button>
				<strong><?php echo $request_message; ?></strong>
			</div>
		<?php } ?>
		<form method="POST" action="<?php echo site_url('employee/request_profile_picture/'.$this->uri->segment(3).'/'.$this->uri->segment(4)); ?>" id="profile_pic_request_frm">
		<p>
			Instruction
			<br />
			<textarea data-rule-required="true" id="profile_pic_request_description" name="profile_pic_request_description" style="width:500px;" class="ui-wizard-content"></textarea>
			<span class="error" id="error_profile_pic_request_description" style="display:none;">
				<br />Enter Instruction
			</span>
			<br />
			Due date
			<br />
			<input type="text" name="profile_pic_due_date" id="profile_pic_due_date" class="datepick" value="" />
			<span class="error" id="error_profile_pic_due_date" style="display:none;">
				<br />Enter due date
			</span>
		</p>
		</form>
	</div>
	<div class="modal-footer">
		<button data-dismiss="modal" class="btn" aria-hidden="true">Cancel</button>
		<button data-id_dept="" class="btn btn-primary" id="request_from_employee_profile_pic_btn">Send request</button>
	</div>
</div>
<script type="text/javascript">

$(document).ready(function() {

$('#request_profile_pic').click(function() {
	$('#request_from_employee_profile_pic_model').modal('show');
	return false;
});

$('#request_from_employee_profile_pic_btn').click(function() {
	
	var send = true;
	
	if($('#profile_pic_request_description').val() == '') {
		$('#error_profile_pic_request_description').show();
		send = false;
	} else {
		$('#error_profile_pic_request_description').hide();
	}
	
	if($('#profile_pic_due_date').val() == '') { 
		$('#error_profile_pic_due_date').show();
		send = false;
	} else {
		$('#error_profile_pic_due_date').hide();
	}
	
	if(send == true) {
		$('#profile_pic_request_frm').submit();
	} else {
		return false;
	}	
		
});

$('.left_menu_button').click(function() {
	
	$('.left_menu_button').parent().removeClass('active');
	$(this).parent().addClass('active');
	
	$('.employee_info_box').hide();
	
	var container_id = $(this).attr('data-container');
	$('#' + container_id).show();
	
	return false;
});	
});	


</script>