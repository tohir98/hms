<?=
form_bs_control('Task Description', function() {
    ?>
    <span>
        {{item.params.description}} 
    </span>

    <?php
})
?>

<?=
form_bs_control('Approvers', function() {
    ?>
    <div ng-init="data[item.id].approvers = data[item.id].approvers || [];"></div>
    <div ng-repeat="approver in data[item.id].approvers">
        <div class="input-append">
            <select style="width: 300px;" name="assign[{{item.id}}][approvers][]" chosenfy='' ng-model="data[item.id].approvers[$index]">
                <option ng-if="employee.id_user === approver || data[item.id].approvers.indexOf(employee.id_user) < 0" ng-selected="approver === employee.id_user" value="{{employee.id_user}}" ng-repeat="employee in employees| orderBy:last_name">
                    {{employee.last_name}} {{employee.first_name}} {{employee.department ? ('/' + employee.department) : ''}}
                </option>
            </select> 
            <a href="#" onclick="return false;" ng-click="data[item.id].approvers.splice($index, 1)" class="btn">
                <i class="icon-trash"></i> remove
            </a>
        </div>
    </div>

    <div>
        <br/>
        <button type="button" ng-click="data[item.id].approvers.push('')" class="btn btn-small" ng-if="data[item.id].approvers.indexOf('') < 0">
            <i class="icon-plus"></i> add approver
        </button>
    </div>

    <?php
})
?>