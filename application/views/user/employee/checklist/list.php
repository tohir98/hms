<?php
$sn = 0; /* @var $checklists employee\Checklist[] */
$this->helper(['text']);
?>
<div class="page-header">
    <div class="pull-left">
        <h1>Settings</h1>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">

    </div>
</div>
<?= flash_message_display(); ?>

<div class="box">

    <div class="box-content nopadding">

        <?php $this->load->view('common/employee_settings_tab'); ?>

        <div class="tab-content"> 
            <div class="tab-pane active" id="approval">



                <div class="row-fluid">
                    <div class="span12">
                        <div class="box box-bordered">
                            <div class="box-title">
                                <h3>
                                    <i class="icon-tasks"></i>
                                    Checklists
                                </h3>
                                <a href="<?= site_url('employee/checklist/create'); ?>" data-toggle='modal' class="btn btn-warning pull-right" style=" margin-right: 10px;">Add New Checklist</a>
                            </div>
                            <div class="box-content box-content-padless">
                                <table class="table table-bordered dataTable">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Items</th>
                                            <th>Employees</th>
                                            <th>Created</th>
                                            <th>Status</th>
                                            <th>Last Assigned</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($checklists as $checklist): ?>
                                            <tr>
                                                <td><?= ++$sn; ?></td>
                                                <td><?= $checklist->title ?></td>
                                                <td><?= word_limiter($checklist->description, 50) ?></td>
                                                <td><?= $checklist->getItemsCount() ?></td>
                                                <td><?= $checklist->getEmployeesCount() ?></td>
                                                <td><?= date_format_strtotime($checklist->created_on); ?></td>
                                                <td>
                                                    <?php if ($checklist->enabled): ?>
                                                        <span class="label label-success">enabled</span>
                                                    <?php else: ?>
                                                        <span class="label label-warning">disabled</span>
                                                    <?php endif; ?>
                                                </td>
                                                <td><?= date_format_strtotime($checklist->date_last_assigned) ? : '-Never-'; ?></td>
                                                <td>
                                                    <div class="dropdown tright">
                                                        <a class="btn btn-primary" href="#" data-toggle="dropdown">
                                                            Actions <span class="caret"></span>
                                                        </a>
                                                        <ul class="dropdown-menu pull-right tleft">


                                                            <?php if ($this->user_auth->have_perm('employee:checklist_edit')): ?>
                                                                <li>
                                                                    <a class="checklistToggle" data-enabled="<?= intval($checklist->enabled) ?>" href="<?= site_url('/employee/checklist/toggle/' . $checklist->ref); ?>">
                                                                        <?= $checklist->enabled ? 'Disable' : 'Enable' ?>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="<?= site_url('/employee/checklist/edit/' . $checklist->ref); ?>">
                                                                        Edit Checklist
                                                                    </a>
                                                                </li>
                                                            <?php endif; ?>
                                                            <?php if ($this->user_auth->have_perm('employee:checklist_assigned') && $checklist->getEmployeesCount() > 0): ?> 
                                                                <li>
                                                                    <a href="<?= site_url('/employee/checklist/assigned/' . $checklist->ref); ?>" title="View employees that have been assigned this checklist">
                                                                        View Employees
                                                                    </a>
                                                                </li>
                                                            <?php endif; ?>
                                                        </ul>
                                                    </div>

                                                </td>

                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div><!-- End row-fluid -->


            </div><!-- End Div Overview -->
        </div>
    </div>    
</div>
<script>
    $(function () {
        $('.checklistToggle').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = parseInt($(this).data('enabled')) ? 'If you disable this checklist, you\'ll no longer be able to assign it to employees, Are you sure you want to continue? ' : 'Are you sure you want to enable this checklist?';
            TalentBase.doConfirm({
                title: 'Confirm Status Change',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });
</script>