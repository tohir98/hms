<?php include '_checklist_js.php'; ?>
<?php
$sn = 0; /* @var $checklists employee\Checklist[] */
$this->helper(['text']);
?>
<div class="page-header">
    <div class="pull-left">
        <h1>Checklist </h1>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left"></div>
</div>
<?= flash_message_display(); ?>

<div class="box" ng-app="checklistAssign">
    <div class="row-fluid" ng-controller="AssignedCtrl">
        <div class="span12">
            <div class="box box-bordered">
                <div class="box-title">
                    <h3>
                        <i class="icon-tasks"></i> Assigned Employees <?= $checklist ? ": {$checklist->title}" : "" ?>
                    </h3>
                </div>
                <div class="box-content box-content-padless" cg-busy="employeePromise" >
                    <table class="table table-bordered" datatable="ng">
                        <thead>
                            <tr>
                                <th>Staff ID</th>
                                <th>Employee</th>
                                <?php if (!$checklist): ?>
                                    <th>Checklist</th>
                                <?php endif; ?>
                                <th>Status</th>
                                <th>Approval</th>
                                <th>Assigned On</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr ng-repeat="assigned in assignments ">
                                <td>{{assigned.id_string}}</td>
                                <td>
                                    <?php if ($this->user_auth->have_perm('employee:view_records')): ?>
                                        <a href="<?= site_url('employee/view_records/{{assigned.id_string}}/{{assigned.first_name}}-{{assigned.last_name}}') ?>">
                                            {{assigned.first_name}} {{assigned.last_name}}
                                        </a>
                                    <?php else: ?>
                                        {{assigned.first_name}} {{assigned.last_name}}
                                    <?php endif; ?>
                                </td>
                                <?php if (!$checklist): ?>
                                    <td>
                                        <a href="#" data-trigger="hover" popover="" data-placement="top" data-content="{{assigned.description}}" title="{{assigned.title}}">
                                            {{assigned.title}}
                                        </a>
                                    </td>
                                <?php endif; ?>
                                <td>
                                    {{assigned.completion_status}}
                                </td>
                                <td>
                                    <div ng-repeat="(status, c) in assigned.approval_status">
                                        <span ng-if="status !== 'total'">{{status}} <b class="badge badge-info">{{c}} of {{assigned.approval_status.total}}</b></span> 
                                    </div>
                                </td>
                                <td>
                                    {{assigned.date_assigned|date}}
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div><!-- End row-fluid -->

</div>