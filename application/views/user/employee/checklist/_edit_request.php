<div class="control-group">
    <label class="control-label {{idx=$index}}">Requests</label>
    <div class="controls" ng-init="item.params.requests = item.params.requests || [];">
        <div ng-int="item.params.requests_selection=item.params.requests_selection||{};" class="row-fluid">
            <label>Please select the type of information you wish to request.</label>
            <div ng-repeat="(tag, name) in requestTypeNames" class="span4 {{idx}}" ng-class="{noLeftMargin: ($index % 3) === 0}">
                <label class="checkbox">
                    <input ng-model="item.params.requests_selection[tag]" ng-change="changedReq(item.params.requests, item.params.requests_selection)" type="checkbox" name="items[{{idx}}][params][requests][]" ng-checked="item.params.requests.indexOf(tag) > -1" value="{{tag}}"/> {{name}}
                </label>
            </div>

        </div>
    </div>
</div>