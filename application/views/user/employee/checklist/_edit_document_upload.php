<label>Please supply document details</label>
<div class="control-group">
    <label class="control-label">
        Document Title
    </label>
    <div class="controls">
        <input type="text" class="input-xlarge" name="items[{{$index}}][params][title]" ng-model="item.params.title"/>
    </div>
</div>
<div class="control-group">
    <label class="control-label">
        Document Description
    </label>
    <div class="controls">
        <textarea class="input-xlarge" name="items[{{$index}}][params][description]" ng-model="item.params.description" placeholder="Document Description"></textarea>
    </div>
</div>