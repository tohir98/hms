<?php include '_checklist_js.php'; ?>
<?php $isEditing = !empty($checklist); ?>
<div class="page-header">
    <div class="pull-left">
        <h1>Settings</h1>
    </div>
</div>
<?= flash_message_display(); ?>

<div class="box" ng-app="checklistApp">

    <div class="box-content nopadding" ng-controller="EditCtrl">

        <?php $this->load->view('common/employee_settings_tab'); ?>

        <div class="tab-content" cg-busy='employeePromise'> 
            <div class="tab-pane active" id="approval">



                <div class="row-fluid">
                    <div class="span12">
                        <div class="box box-bordered">
                            <div class="box-title">
                                <h3>
                                    <i class="icon-tasks"></i>
                                    <?= empty($checklist) ? 'Create' : 'Edit' ?> Checklist  <span ng-if="checklist.title && checklist.title !== ''">: {{checklist.title}}</span>
                                </h3>
                            </div>
                            <div class="box-content">
                                <?php include 'checklist_form.php'; ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>    
</div>
