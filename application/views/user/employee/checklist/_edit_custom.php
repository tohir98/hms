<div ng-init="item.params.approvers = item.params.approvers || [];">
    <div class="control-group">
        <label class="control-label">
            Task Description
        </label>
        <div class="controls">
            <textarea class="input-block-level" placeholder="Task Description" name="items[{{$index}}][params][description]" ng-model="item.params.description"></textarea>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Task Approvers</label>
        <div class="controls">
            <select class="input-xxlarge" ng-model="item.params.approvers" name="items[{{$index}}][params][approvers][]" multiple="" chosenfy=''>
                <option ng-repeat="employee in employees|orderBy:last_name|orderBy:first_name" value="{{employee.id_user}}" ng-selected="item.params.approvers.indexOf(employee.id_user) > -1">
                    {{employee.last_name}} {{employee.first_name}} {{employee.department ? '/' + employee.department : ''}}
                </option>
            </select>
        </div>
    </div>
</div>