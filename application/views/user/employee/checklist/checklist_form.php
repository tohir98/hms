<?php $this->helper(['form']) ?>
<script>
    window.checklist = <?= json_encode($checklist); ?>;
    window.taskTypes = <?= json_encode($taskTypes); ?>;
    window.requestTypeNames = <?= json_encode($requestTypeNames); ?>;
</script>
<form class="form-horizontal" method="post" action="">
    <?php form_bs_control('Checklist Title', function() {
        ?>
        <input name="title" ng-model="checklist.title" required="" type="text" class="input-xlarge"/>
    <?php })
    ?>
    <?php form_bs_control('Description', function() {
        ?>
        <textarea name="description" ng-model="checklist.description" required="" class="input-xlarge" rows="4"></textarea>
    <?php })
    ?>


    <?php form_bs_control('Checklist Items', function() use ($taskTypes) {
        ?>
        <div class="checklist-items well well-small" ng-repeat="item in checklist.items" ng-class="{
                            error: !itemValid(item)
                        }" ng-init="item.duration = int(item.duration)" >
            <div class="pull-right" style="margin-bottom: 5px;">
                <a  href="#" onclick="return false;" ng-click="editItemPopup($index)">
                    <i class="icon-edit"></i> edit
                </a>

                <a  href="#" onclick="return false;" ng-click="move($index, 1)" ng-if="$index > 0">
                    <i class="icon-caret-up"></i> move up
                </a>

                <a  href="#" onclick="return false;" ng-click="move($index, -1)" ng-if="$index < checklist.items.length - 1">
                    <i class="icon-caret-down"></i> move down
                </a>
                <a  href="#" onclick="return false;" confirm-on-accept="removeItem($index)" tb-confirm="tb-confirm" confirm-message="Are you sure you want to delete item : <b>{{item.title}}</b> ?<br/>This action cannot be undone.">
                    <i class="fa icon-remove"></i> remove
                </a>
            </div>
            <div class="clearfix"></div>
            <div style="color: #ff3333;" ng-if="!itemValid(item)">
                This checklist item is not valid.
            </div>
            <div class="row-fluid">
                <div class="span1" style="width: 20px;">
                    {{$index + 1}}
                </div>
                <div class="span11">
                    <h3 class="checklist_item_title">
                        {{item.title}}
                    </h3>
                    <div class="checklist_item_summary">
                        {{item.summary}}
                    </div>

                    <div class="checklist_item_type">
                        <div>
                            <strong>Type</strong> {{taskTypes[item.item_type].typeName}}
                        </div>
                        <div ng-if="item.item_type === 'custom'" class="custom">
                            <strong>Description</strong> {{item.params.description}}<br/>
                            <div ng-if="item.params.approvers.length > 0">
                                <strong>Approver(s):</strong> 
                                <span ng-repeat="approver in item.params.approvers" class="label label-default" style="margin-right: 5px;">
                                    {{(emp = findEmployee(approver)).last_name}} {{emp.first_name}}
                                </span>
                            </div>
                        </div>

                        <div ng-if="item.item_type === 'request'" class="request">
                            <strong>Requested Fields</strong> <span class="label label-default" ng-repeat="req in item.params.requests" style="margin-left: 5px;">{{requestTypeNames[req]}}</span>
                        </div>

                        <div ng-if="item.item_type === 'document_upload'" class="document_upload">
                            <strong>Title</strong> {{item.params.title}}<br/>
                            <strong>Description</strong> {{item.params.description}}<br/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal hide modal-full " id="addItemPopup_{{$index}}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss='modal'>&times;</button>
                    <h3 class="modal-title">&nbsp;</h3>
                </div>
                <div class="modal-body" style="overflow-y: hidden; height: auto; min-height: 70%;">
                    <div  style="padding: 10px;">
                        <div class="control-group">
                            <label class="control-label">
                                Title    
                            </label>
                            <div class="controls">
                                <input type="text" required="" name="items[{{$index}}][title]" ng-model="item.title" placeholder="Item Title" title="Item Title" class="input-block-level"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Summary
                            </label>
                            <div class="controls">
                                <textarea name="items[{{$index}}][summary]" ng-model="item.summary" placeholder="Summary" required="" class="input-block-level"></textarea>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">
                                Timeline
                            </label>
                            <div class="controls">
                                <div class="input-append input-prepend">
                                    <input type="number" name="items[{{$index}}][duration]" ng-model="item.duration" required="" class="input-small"/>
                                    <span class="add-on">Days</span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">

                            <label class="control-label">
                                Task Type
                            </label>
                            <div class="controls">
                                <select class="input-block-level" required="" name="items[{{$index}}][item_type]" ng-options="type as typeObj.typeName for (type, typeObj) in taskTypes" ng-model="item.item_type">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="items[{{$index}}][id]" value="{{item.id}}"/>
                        <input type="hidden" name="items[{{$index}}][item_order]" value="{{$index * 100}}"/>
                        <?php foreach ($taskTypes as $t => $tObj): ?>
                            <div ng-if="item.item_type === '<?= $t; ?>'">
                                <?php
                                if (file_exists($fname = __DIR__ . "/_edit_{$t}.php")) {
                                    include $fname;
                                }
                                ?>
                            </div>
                        <?php endforeach; ?>
                        <div ng-if="taskTypes[item.item_type].supportsAttachment" class="control-group">
                            <label class="control-label">Attached File</label>
                            <div class="controls">
                                <div>
                                    {{item.attachment_description ? item.attachment_description : 'None'}}
                                    <span ng-if="item.attached_file_name">
                                        <a href="#" onclick="return false;" ng-click="item.attached_file_name = null;
                                                    item.attachment_description = null;">remove
                                        </a> &middot;
                                    </span>


                                    <a href="#" onclick="return false;" ng-click="fileChange(item);">
                                        change
                                    </a> 
                                </div>
                            </div>
                            <input type="hidden" name="items[{{$index}}][attached_file_name]" value="{{item.attached_file_name}}"/>
                            <input type="hidden" name="items[{{$index}}][attachment_description]" value="{{item.attachment_description}}"/>
                        </div>
                    </div>
                    <div class="tright">
                        <hr/>
                        <button class="btn btn-success" data-dismiss='modal'>Done Editing</button>
                    </div>
                </div>
            </div>
        </div>

        <div ng-if="checklist.items.length < 1" class="alert alert-info">
            There are not items on this checklist at the moment, you must add an item to complete checklist.
        </div>
        <div class="tleft">
            <button type="button" ng-click="addItem();" class="btn btn-info">Add Item</button>
        </div>
    <?php })
    ?>

    <?php form_bs_control('Checklist Approvers', function() {
        ?>
        <div ng-repeat="approver in checklist.approvers" >
            <select style="width: 300px;" name="approvers[]" chosenfy='' ng-model="checklist.approvers[$index]">
                <option ng-if="employee.id_user === approver || checklist.approvers.indexOf(employee.id_user) < 0" ng-selected="approver === employee.id_user" value="{{employee.id_user}}" ng-repeat="employee in employees| orderBy:last_name">
                    {{employee.last_name}} {{employee.first_name}} {{employee.department ? ('/' + employee.department) : ''}}
                </option>
            </select> 
            <label>
                <a href="#" onclick="return false;" ng-click="checklist.approvers.splice($index, 1)">
                    <i class="icon-trash"></i> remove
                </a>
            </label>
        </div>
        <div>
            <br/>
            <button type="button" ng-click="checklist.approvers.push('')" class="btn btn-success" ng-if="checklist.approvers.indexOf('') < 0">
                <i class="icon-plus"></i> add approver
            </button>
        </div>
    <?php })
    ?>

    <div class="tleft">
        <button class="btn btn-primary btn-large" type="submit" ng-disabled="checklist.items.length < 1 || !allItemsValid()">Save Checklist</button>
    </div>
</form>