<?php include '_checklist_js.php'; ?>
<script>
    window.checklist = <?= json_encode($checklist); ?>;
    window.assignData = <?= json_encode($data['assign']); ?>;
    window.taskTypes = <?= json_encode($taskTypes); ?>;
    window.requestTypeNames = <?= json_encode($requestTypeNames); ?>;
</script>
<?php
/* @var $checklist employee\Checklist */
$this->helper(['text', 'form']);
?>
<div class="page-header">
    <div class="pull-left">
        <h1>Assign Checklist</h1>
    </div>
    <div class="clearfix"></div>
</div>
<?= flash_message_display(); ?>
<?php if ($error): ?>
    <div class="alert alert-error">
        <?= $error ?>
    </div>
<?php endif; ?>
<div class="box box-bordered" ng-app="checklistApp">
    <div class="box-title">
        <h3><?= $checklist->title ?></h3>
    </div>

    <div class="box-content" ng-controller="AssignCtrl" cg-busy="employeePromise" ng-cloak="">
        <form class="form-horizontal" method="POST" action="" id="checklistAssignForm" >
            <div class="control-group">
                <label style="font-weight: bold;" class="control-label">Employees</label>
                <div class="controls">
                    <?php
                    foreach ($users as $user) {
                        ?>
                        <span class="label label-default"><?= $user->first_name ?> <?= $user->last_name ?></span> &nbsp;
                        <?php
                    }
                    ?>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label">Checklist item(s)</label>
                <div class="controls">
                    <ol>
                        <li class="well well-small" ng-repeat="item in checklist.items">
                            <label>
                                <strong>{{item.title}}</strong>
                                <div style="font-size: 0.85em; font-style: italic;">
                                    {{item.summary}}
                                </div>
                            </label>
                            <?php
                            echo form_bs_control('Due Date', function() {
                                ?>
                                <input type="text" pattern="\d\d-\d\d-\d\d\d\d" title="Due Date for this item" name="assign[{{item.id}}][due_date]" ng-model="data[item.id].due_date" class="datepick"/>
                                <?php
                            });

                            foreach ($taskTypes as $taskType) {
                                $script = __DIR__ . "/_assign_{$taskType->getId()}.php";
                                if (file_exists($script)) {
                                    ?>
                                    <div ng-if="item.item_type === '<?= $taskType->getId() ?>'">
                                        <?php include $script; ?>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </li>
                    </ol>
                </div>
            </div>

            <div class="control-group">
                <label style="font-weight: bold;" class="control-label">
                    Checklist Approvers
                </label>
                <div class="controls">
                    <div ng-repeat="approver in data.approvers" >
                        <div class="input-append">
                            <select style="width: 300px;" name="assign[approvers][]" chosenfy='' ng-model="data.approvers[$index]">
                                <option ng-if="employee.id_user === approver || data.approvers.indexOf(employee.id_user) < 0" ng-selected="approver === employee.id_user" value="{{employee.id_user}}" ng-repeat="employee in employees| orderBy:last_name">
                                    {{employee.last_name}} {{employee.first_name}} {{employee.department ? ('/' + employee.department) : ''}}
                                </option>
                            </select> 
                            <a href="#" onclick="return false;" ng-click="data.approvers.splice($index, 1)" class="btn">
                                <i class="icon-trash"></i> remove
                            </a>
                        </div>
                    </div>
                    <div>
                        <br/>
                        <button type="button" ng-click="data.approvers.push('')" class="btn btn-success btn-small" ng-if="data.approvers.indexOf('') < 0">
                            <i class="icon-plus"></i> add approver
                        </button>
                    </div>
                </div>
            </div>

            <div>
                <button type="submit" class="btn btn-primary btn-large">Assign Checklist</button>
            </div>
        </form>
    </div>
</div>
<script>
    $(function () {
        var confirmed = false;
        $('#checklistAssignForm').submit(function (e) {
            if (!confirmed) {
                e.preventDefault();
                TalentBase.doConfirm({
                    title: 'Confirm Checklist Assignment',
                    message: 'Are you sure you want to assign this checklist to the specified employees?',
                    acceptText: 'Yes, Continue',
                    onAccept: function () {
                        confirmed = true;
                        $('#checklistAssignForm').submit();
                    }
                });
            }
        });
    });
</script>