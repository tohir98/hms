<style>
	.control-group {
		padding:15px !important;
	}
	h4, h5 {
		padding-left:15px;
	}
</style>
<div class="box">
	<div class="page-header">
		<div class="pull-left">
			<h1>Edit Resume/Credentials</h1>
		</div>
    </div>
	<div class="breadcrumbs">
		<ul>
			<li><a href="<?php echo site_url('employee/view_records/'.$this->uri->segment(3).'/'.$this->uri->segment(3)); ?>">Employee</a><i class="icon-angle-right"></i></li>
			<li><a href="#">Edit Resume/Credentials</a></li>
		</ul>
		<div class="close-bread"></div>
	</div>
	<?php if($success_message != '') { ?>
	<div class="alert alert-success" style="margin-top:20px;">
			<button data-dismiss="alert" class="close" type="button">×</button>
			<strong>Success!</strong> <?php echo $success_message; ?>
	</div>
	<?php } ?>
	<?php 
	if(!empty($error_messages)) { ?>
		<div class="alert alert-danger" style="margin-top:20px;">
			<button data-dismiss="alert" class="close" type="button">×</button>
		<?php
		foreach($error_messages as $err_message) {
	?>
		<strong><?php echo $err_message; ?></strong><br />
	<?php } ?>
		</div>
	<?php
	}
	?>
	<div class="box-content">
		<form action="" method="POST" class="form-horizontal form-wizard form-wysiwyg form-striped" id="ss" enctype="multipart/form-data" style="border:1px solid #DDDDDD">
			<h4>Resume/Credentials</h4>
			<div id="credentials_now_container">
					<?php if(!empty($files)) { 
						$i = 0;
						?>
					<?php 
					$container = $this->user_auth->get('cdn_container');
					foreach($files as $file) { ?>
					<div>	
						<input type="hidden" name="edit_id_cred[<?php echo $i; ?>]" value="<?php echo $file->id_cred; ?>" />
						<div class="control-group">
							<label for="" class="control-label">File name</label>
							<div class="controls">
								<input type="text" style="width:500px;" name="old_attachment_file_name[<?php echo $i; ?>]" data-rule-required="true" value="<?php echo $file->file_title; ?>">
							</div>
						</div>
						<div class="control-group">
							<label for="" class="control-label">
								<?php if($file->type == 0) { ?>
									Request description<br />
								<?php } else { ?>
									File Description<br />
								<?php } ?>
							</label>
							<div class="controls">
								<textarea style="width:500px;" name="old_attachment_description[<?php echo $i; ?>]" data-rule-required="true"><?php echo $file->description; ?></textarea>
							</div>
						</div>
						<?php if($file->type == 1) { ?>
						<div class="control-group">
							<label for="" class="control-label"></label>
							<div class="controls file_now_container">
								<input type="file" name="new_resume[<?php echo $i; ?>]" id="resume" data-rule-required="true" /> <br />
								<?php
								if($file->file_name != '') {
									$file_get_url = site_url('cdn_file_url/file_temp_url') . '/' . $container . '/' . $file->file_name;
								} else {
									$file_get_url = '';
								}
								?>
								<?php if($file_get_url != '') { ?>
									Existing file: <a href="<?php echo $file_get_url; ?>" target="_blank"><?php echo $file->file_name; ?></a>
								<?php } ?>	
							</div>
						</div>
						<?php } ?>
						<div class="control-group">
							<label for="" class="control-label"></label>
							<div class="controls">
								<a href="#" class="remove_this_item" data-id_cred="<?php echo $file->id_cred; ?>">Remove</a>
							</div>
						</div>
					</div>	
					<?php 
					$i++;
					
					} ?>
					<?php } ?>
				
					<div>
						<div class="control-group">
							<label for="" class="control-label"></label>
							<div class="controls">
								<button type="button" id="add_new_file_btn" class="btn btn-primary" name="update" id="update">Add new file</button>
								<?php if($send_request == true) { ?>
									<button type="button" id="add_new_request_btn" class="btn btn-primary" name="update" id="update">Add new request</button> 
								<?php } ?>
							</div>
						</div>
					</div>	
				
					<div id="new_items_container">
					</div>
					
					<div id="request_new_template" style="display:none">
						<div class="control-group">
							<label for="" class="control-label">Due date</label>
							<div class="controls">
								<input type="text" data-rule-required="true" value="" name="request_date[]" class="datepick">
							</div>
						</div>
						<div class="control-group">
							<label for="" class="control-label">File name</label>
							<div class="controls">
								<input type="text" style="width:500px;" name="request_file_name[]" data-rule-required="true" value="">
							</div>
						</div>
						<div class="control-group">
							<label for="" class="control-label">Request description</label>
							<div class="controls">
								<textarea style="width:500px;" name="request_description[]" data-rule-required="true"></textarea>
							</div>
						</div>
						<div class="control-group">
							<label for="" class="control-label"></label>
							<div class="controls">
								<a href="#" class="remove_this_item">Remove</a>
							</div>
						</div>
					</div>	
					
					<div id="file_now_template" style="display:none">	
						<div class="control-group">
							<label for="" class="control-label">File name</label>
							<div class="controls">
								<input type="text" style="width:500px;" name="attachment_file_name[]" data-rule-required="true" value="">
							</div>
						</div>
						<div class="control-group">
							<label for="" class="control-label">File Description</label>
							<div class="controls">
								<textarea style="width:500px;" name="attachment_description[]" data-rule-required="true"></textarea>
							</div>
						</div>
						<div class="control-group">
							<label for="" class="control-label"></label>
							<div class="controls file_now_container">
								<input type="file" name="resume[]" data-rule-required="true" />
							</div>
						</div>
						<div class="control-group">
							<label for="" class="control-label"></label>
							<div class="controls">
								<a href="#" class="remove_this_item">Remove</a>
							</div>
						</div>
					</div>
				
					<div>
						<div class="control-group">
							<label for="" class="control-label"></label>
							<div class="controls file_now_container">
								<button type="submit" class="btn btn-primary" name="update" id="update">Update</button> 
							</div>
						</div>
					</div>	
			</div>
		</form>
	</div>	
</div>
			
<script type="text/javascript">

$(document).ready(function() {
	
   $('#add_new_request_btn').click(function() {
	   var item = $('#request_new_template').clone();
	   item.css({'display':'block'});
	   item.find('input').val('');
	   $('#new_items_container').append(item);
	   remove_this_item_init();
   });	
	
   $('#add_new_file_btn').click(function() {
	   var item = $('#file_now_template').clone();
	   item.css({'display':'block'});
	   item.find('input').val('');
	   $('#new_items_container').append(item);
	   remove_this_item_init();
   });
   
   remove_this_item_init();
   
   function remove_this_item_init() {
	   $('.remove_this_item').unbind();
	   $('.remove_this_item').click(function() {
		   var id_cred = $(this).attr('data-id_cred');
		   $(this).parent().parent().parent().remove();
		   $('#new_items_container').append('<input type="hidden" name="remove_file[]" value="'+id_cred+'" />');
		   return false;
	   });
   }
       
});

</script>