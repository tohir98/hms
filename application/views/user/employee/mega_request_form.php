<style>
    .control-group {
        padding:15px !important;
    }
    h4, h5 {
        padding-left:15px;
    }
    .solid_border{
        border:1px solid #DDDDDD;
    }
</style>
<!-- Masked inputs -->
<script src="/js/plugins/maskedinput/jquery.maskedinput.min.js"></script>

<div class="box">
    <div class="page-header">
        <div class="pull-left">
            <h1>My Profile</h1>
        </div>
    </div>
    <div class="breadcrumbs">
        <ul>
            <li><a href="<?php echo site_url('employee/view_records/' . $this->uri->segment(3) . '/' . $this->uri->segment(3)); ?>">Employee</a><i class="icon-angle-right"></i></li>
            <li><a href="#">Edit Request</a></li>
        </ul>
        <div class="close-bread"></div>
    </div>
    <?php if ($success_message != '') { ?>
        <div class="alert alert-success" style="margin-top:20px;">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <strong>Success!</strong> <?php echo $success_message; ?>
        </div>
    <?php } ?>
    <?php if (!empty($error_messages)) { ?>
        <div class="alert alert-danger" style="margin-top:20px;">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php
            foreach ($error_messages as $err_message) {
                ?>
                <strong><?php echo $err_message; ?></strong><br />
            <?php } ?>
        </div>
        <?php
    }
    ?>

    <p>
    <ul class="nav nav-tabs" id="myTab">
        <li class="">
            <a href="<?= site_url('personal/my_profile') ?>">Profile</a>
        </li>
        <li class="active">
            <a href="<?= site_url('personal/my_profile_info') ?>" >My Request</a>
        </li>
    </ul>
</p>

<div class="box-content">
    <?php
    if (!empty($requests)) {
        foreach ($requests as $r) {

            $k = substr($kk, 0, strpos($kk, '_'));
            $id_task = $r->id_task;
            if ($request_type['birthday'] == $r->request_type) {  ?>
                <?php include 'requestForms/_birthday.php'; ?>
                
    
            <?php } if ($request_type['home_address'] == $r->request_type) { ?>
                <?php include 'requestForms/_address.php'; ?>
                    
            <?php } if ($request_type['gender'] == $r->request_type) { ?>
                <?php include 'requestForms/_gender.php'; ?>
                    
            <?php } if ($request_type['marital_status'] == $r->request_type) { ?>
                <?php include 'requestForms/_marital_status.php'; ?>
                    
            <?php } if ($request_type['nationality'] == $r->request_type) { ?>
                <?php include 'requestForms/_nationality.php'; ?>
                    
            <?php } if ($request_type['country'] == $r->request_type) { ?>
                <?php include 'requestForms/_country.php'; ?>
                    
            <?php } if ($request_type['city'] == $r->request_type) { ?>
                <?php include 'requestForms/_city.php'; ?>
    
            <?php } if ($request_type['state'] == $r->request_type) { ?>
                <?php include 'requestForms/_state.php'; ?>

            <?php } if ($request_type['work_phone'] == $r->request_type) { ?>
                <?php if ($this->user_auth->have_perm(EDIT_MY_WORK_NUMBER)) { ?>
                    <p>&nbsp;</p>
                    <form action="" method="POST" class="form-horizontal form-wysiwyg form-striped solid_border" id="edit_work_phone_frm">
                        <h4>Edit Work Phone</h4>
                        <div class="control-group">
                            <label class="control-label" for="work_phone">Work phone:</label>
                            <div class="controls">
                                <select id='work_phone_code' name="work_phone_code" class="select-2-me input-medium">
                                    <?php
                                    if (!empty($countries)) {
                                        foreach ($countries as $c) {
                                            if ($c->code == PHONE_CODE_NIGERIA) {
                                                ?>
                                                <option value="<?php echo $c->code ?>" selected><?php echo $c->country . ' - ' . $c->code ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $c->code ?>"><?php echo $c->country . ' - ' . $c->code ?></option>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </select>
                                <input ng-required="true" type="text" id="work_phone" name="work_phone" class="" />
                                <div class="error" style="display:none; margin-top: 5px;" id="work_phone_err_msg">
                                    <strong>All Fields are required!</strong>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="managers" class="control-label"></label>
                            <div class="controls">
                                <input type="hidden" name="work_phone_id_temp_request" id="work_phone_id_temp_request" value="<?= $r->id_temp_request; ?>" />
                                <input type="hidden" name="work_phone_id_task" id="work_phone_id_task" value="<?php echo $id_task; ?>"  />
                                <input type="button" id="update_work_phone_btn" class="btn btn-primary" value="Update" />
                            </div>
                        </div>
                    </form>
                <?php } ?>

            <?php } if ($request_type['personal_phone'] == $r->request_type) { ?>
                <?php if ($this->user_auth->have_perm(EDIT_MY_PERSONAL_NUMBER)) { ?>
                    <p>&nbsp;</p>
                    <form action="" method="POST" class="form-horizontal form-wysiwyg form-striped solid_border" id="edit_personal_phone_frm">
                        <h4>Edit Personal Phone</h4>
                        <div class="control-group">
                            <label for="additionalfield" class="control-label">Personal Number:</label>
                            <div class="controls">
                                <select id='personal_phone_code' name="personal_phone_code" class="select-2-me input-medium">
                                    <?php
                                    if (!empty($countries)) {
                                        foreach ($countries as $c) {
                                            if ($c->code == PHONE_CODE_NIGERIA) {
                                                ?>
                                                <option value="<?php echo $c->code ?>" selected><?php echo $c->country . ' - ' . $c->code ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $c->code ?>"><?php echo $c->country . ' - ' . $c->code ?></option>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </select>
                                <input type="text" name="personal_phone" id="personal_phone" ng-required="true" />
                                <div class="error" style="display:none; margin-top: 5px;" id="personal_phone_err_msg">
                                    <strong>All Fields are required!</strong>
                                </div>
                            </div>

                        </div> 
                        <div class="control-group">
                            <label for="managers" class="control-label"></label>
                            <div class="controls">
                                <input type="hidden" name="personal_phone_id_temp_request" id="personal_phone_id_temp_request" value="<?= $r->id_temp_request; ?>" />
                                <input type="hidden" name="personal_phone_id_task" id="personal_phone_id_task" value="<?php echo $id_task; ?>"  />
                                <input type="button" id="update_personal_phone_btn" class="btn btn-primary" value="Update" />
                            </div>
                        </div>
                    </form>
                <?php } ?>

            <?php } if ($request_type['bank_account'] == $r->request_type) { ?>
                <?php if ($this->user_auth->have_perm(EDIT_MY_BANK_ACCOUNT)) { ?>
                    <p>&nbsp;</p>
                    <form action="" method="POST" class="form-horizontal form-wysiwyg form-striped solid_border" id="edit_bank_details_frm">
                        <h4>Edit Bank Details </h4>
                        <div class="control-group">
                            <label class="control-label" for="account_name">Account name:</label>
                            <div class="controls">
                                <input type="text" class="input-xlarge" id="account_name" name="account_name" value="<?php echo $bank_details->bank_account_name; ?>" />
                                <div class="error" style="display:none; margin-top: 5px;" id="account_name_err_msg">
                                    <strong>Account name is required</strong>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="bank">Bank:</label>
                            <div class="controls">
                                <select name="id_bank" id="id_bank">
                                    <option value="">Select Bank</option>
                                    <?php
                                    foreach ($banks as $bank) {
                                        if ($bank->id_bank == $bank_details->id_bank) {
                                            $sel = ' selected="selected" ';
                                        } else {
                                            $sel = '';
                                        }
                                        ?>
                                        <option value="<?php echo $bank->id_bank; ?>" <?php echo $sel; ?>><?php echo $bank->name; ?></option>
                                    <?php } ?>
                                </select>
                                <div class="error" style="display:none; margin-top: 5px;" id="id_bank_err_msg">
                                    <strong>Select your bank</strong>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="id_bank_account_type">Account type:</label>
                            <div class="controls">
                                <select name="account_type" id="account_type">
                                    <option value=""> Select </option>
                                    <?php
                                    foreach ($account_types as $index => $type) {
                                        if ($index == $bank_details->id_bank_account_type) {
                                            $sel = ' selected="selected" ';
                                        } else {
                                            $sel = '';
                                        }
                                        ?>
                                        <option value="<?php echo $index; ?>" <?php echo $sel; ?>><?php echo $type; ?></option>
                                    <?php } ?>
                                </select>
                                <div class="error" style="display:none; margin-top: 5px;" id="account_type_err_msg">
                                    <strong>Select your account type</strong>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="account_number">Account number:</label>
                            <div class="controls">
                                <input type="text" id="account_number" name="account_number" maxlength="10" data-rule-required="true" value="<?php echo $bank_details->bank_account_number; ?>" />
                                <div class="error" style="display:none; margin-top: 5px;" id="account_number_err_msg">
                                    <strong>Enter your account number</strong>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="managers" class="control-label"></label>
                            <div class="controls">
                                <input type="hidden" name="bank_id_temp_request" id="bank_id_temp_request" value="<?= $r->id_temp_request; ?>" />
                                <input type="hidden" name="bank_id_task" id="bank_id_task" value="<?php echo $id_task; ?>"  />
                                <input type="button" class="btn btn-primary" value="Update" id="update_bank_btn" />
                            </div>
                        </div>
                    </form>
                <?php } ?>

            <?php } if ($request_type['pension_details'] == $r->request_type) { ?>
                <?php if ($this->user_auth->have_perm(EDIT_MY_PENSION_DETAILS)) { ?>
                    <p>&nbsp;</p>
                    <form action="" method="POST" class="form-horizontal form-wysiwyg form-striped solid_border" id="edit_benefit_frm">
                        <h4>Edit Benefit</h4>

                        <div class="control-group">
                            <label class="control-label" for="rsa_pin">RSA PIN:</label>
                            <div class="controls">
                                <input type="text" class="input-xlarge" id="rsa_pin" name="rsa_pin" value="<?php echo $profile->rsa_pin; ?>" />
                                <div class="error" style="display:none; margin-top: 5px;" id="rsa_pin_err_msg">
                                    <strong>Enter RSA PIN</strong>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="pfs">Pension Fund Administrator:</label>
                            <div class="controls">
                                <select name="pfs" id="pfs" style="width:300px;">
                                    <?php
                                    foreach ($pfas as $pfa) {
                                        if ($pfa->id_pension_fund_administrator == $profile->id_pfs) {
                                            $sel = ' selected="selected" ';
                                        } else {
                                            $sel = '';
                                        }
                                        ?>
                                        <option value="<?php echo $pfa->id_pension_fund_administrator; ?>" <?php echo $sel; ?>><?php echo $pfa->name; ?></option>
                                    <?php } ?>
                                </select>
                                <div class="error" style="display:none; margin-top: 5px;" id="pfs_err_msg">
                                    <strong>Select Pension Fund Administrator</strong>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="managers" class="control-label"></label>
                            <div class="controls">

                                <input type="hidden" name="benefit_id_temp_request" id="benefit_id_temp_request" value="<?= $r->id_temp_request; ?>" />
                                <input type="hidden" name="benefit_id_task" id="benefit_id_task" value="<?php echo $id_task; ?>"  />
                                <input type="button" class="btn btn-primary" value="Update" id="update_benefit_btn"/>
                            </div>
                        </div>
                    </form>
                <?php } ?>
            <?php } if ($request_type['emergency1'] == $r->request_type) { ?>
                <?php if ($this->user_auth->have_perm(EDIT_MY_EMERGENCY_CONTACT_1)) { ?>
                    <p>&nbsp;</p>
                    <form action="" method="POST" class="form-horizontal form-wysiwyg form-striped solid_border" id="edit_emergency_contact1_frm">
                        <h4>Edit Emergency contact 1</h4>
                        <div class="error_em alert error" style="display:none; margin-top: 5px;" id="address_err_msg">
                            <strong>All Fields are required!</strong>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="emergency_first_name">First name:</label>
                            <div class="controls">
                                <input type="text" name="emergency_first_name" id="emergency_first_name" value="<?php echo $profile->emergency_first_name; ?>" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="emergency_last_name">Last name:</label>
                            <div class="controls">
                                <input type="text" name="emergency_last_name" id="emergency_last_name" value="<?php echo $profile->emergency_last_name; ?>" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="emergency_address">Address:</label>
                            <div class="controls">
                                <input type="text" name="emergency_address" id="emergency_address" value="<?php echo $profile->emergency_address; ?>" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="emergency_email">Email:</label>
                            <div class="controls">
                                <input type="text" name="emergency_email" id="emergency_email" value="<?php echo $profile->emergency_email; ?>" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="emergency_phone_number">Phone:</label>
                            <div class="controls">
                                <input type="text" name="emergency_phone_number" id="emergency_phone_number" value="<?php echo $profile->emergency_phone_number; ?>" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="emergency_employer">Employer:</label>
                            <div class="controls">
                                <input type="text" name="emergency_employer" id="emergency_employer" value="<?php echo $profile->emergency_employer; ?>" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="emergency_work_address">Work address:</label>
                            <div class="controls">
                                <input type="text" name="emergency_work_address" id="emergency_work_address" value="<?php echo $profile->emergency_work_address; ?>" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="managers" class="control-label"></label>
                            <div class="controls">
                                <input type="hidden" name="contact1_id_temp_request" id="contact1_id_temp_request" value="<?= $r->id_temp_request; ?>" />
                                <input type="hidden" name="contact1_id_task" id="contact1_id_task" value="<?php echo $id_task; ?>"  />
                                <input type="button" class="btn btn-primary" value="Update" id="update_contact1_btn">
                            </div>
                        </div>
                    </form>
                <?php } ?>

            <?php } if ($request_type['emergency2'] == $r->request_type) { ?>
                <?php if ($this->user_auth->have_perm(EDIT_MY_EMERGENCY_CONTACT_2)) { ?>
                    <p>&nbsp;</p>
                    <form action="" method="POST" class="form-horizontal form-wysiwyg form-striped solid_border" id="edit_emergency_contact2_frm">
                        <h4>Edit Emergency contacts 2</h4>
                        <div class="error_em alert error" style="display:none; margin-top: 5px;" id="address_err_msg">
                            <strong>All Fields are required!</strong>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="first_name">First name:</label>
                            <div class="controls">
                                <input type="text" name="emergency_first_name2" id="emergency_first_name2" value="<?php echo $profile->emergency_first_name2; ?>" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="emergency_last_name2">Last name:</label>
                            <div class="controls">
                                <input type="text" name="emergency_last_name2" id="emergency_last_name2" value="<?php echo $profile->emergency_last_name2; ?>" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="emergency_address2">Address:</label>
                            <div class="controls">
                                <input type="text" name="emergency_address2" id="emergency_address2" value="<?php echo $profile->emergency_address2; ?>" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="emergency_email2">Email:</label>
                            <div class="controls">
                                <input type="text" name="emergency_email2" id="emergency_email2" value="<?php echo $profile->emergency_email2; ?>" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="emergency_phone_number2">Phone:</label>
                            <div class="controls">
                                <input type="text" name="emergency_phone_number2" id="emergency_phone_number2" value="<?php echo $profile->emergency_phone_number2; ?>" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="emergency_employer2">Employer:</label>
                            <div class="controls">
                                <input type="text" name="emergency_employer2" id="emergency_employer2" value="<?php echo $profile->emergency_employer2; ?>" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="emergency_work_address2">Work address:</label>
                            <div class="controls">
                                <input type="text" name="emergency_work_address2" id="emergency_work_address2" value="<?php echo $profile->emergency_work_address2; ?>" />
                            </div>
                        </div>

                        <div class="control-group">
                            <label for="managers" class="control-label"></label>
                            <div class="controls">
                                <input type="hidden" name="contact2_id_temp_request" id="contact2_id_temp_request" value="<?= $r->id_temp_request; ?>" />
                                <input type="hidden" name="contact2_id_task" id="contact2_id_task" value="<?php echo $id_task; ?>"  />
                                <input type="button" class="btn btn-primary" value="Update" id="update_contact2_btn">
                            </div>
                        </div>
                    </form>
                <?php } ?>
                <?php
            }
        }
    } else {
        ?>
        <ul class="messages">
            <li class="left">
                <div class="image">
                    <img src="/img/tb_logo.png" alt="">
                </div>
                <div class="message">
                    <span class="caret"></span>
                    <span class="name">TalentBase HelpDesk</span>
                    <p>
                        Hi, you have no pending request at moment.</p>
                </div>
            </li>
        </ul>
    <?php }
    ?>

</div>	
</div>

<script type="text/javascript">

    $(document).ready(function () {

        $('#work_phone').mask("- (999) - (999) - (9999)");
        $('#personal_phone').mask("- (999) - (999) - (9999)");

        //////////////////// update_work_phone_btn ///////////
        $('#update_work_phone_btn').click(function () {

            var work_phone_ = $('#work_phone').val();
            var work_phone_code_ = $('#work_phone_code').val();
            var id_task_ = $('#work_phone_id_task').val();

            if (work_phone_ === '' || work_phone_code_ === '') {
                $('#work_phone_err_msg').show();
                return false;
            } else {
                $('#work_phone_err_msg').hide();
                show_loader();
            }

            var url_ = '<?php echo site_url('user/personal/update_work_phone'); ?>';
            $.post(
                    url_,
                    {
                        'phone': work_phone_,
                        'id_country_code': work_phone_code_,
                        'id_task': id_task_,
                        'id_temp_request': $('#work_phone_id_temp_request').val()
                    },
            function (res) {
                hide_loader();
                $('#edit_work_phone_frm').html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">x</button>' + res + '</div>');
            }
            );

            return false;

        });

        //////////////////// Personal phone ///////////
        $('#update_personal_phone_btn').click(function () {

            var personal_phone_ = $('#personal_phone').val();
            var personal_phone_code_ = $('#personal_phone_code').val();
            var id_task_ = $('#personal_phone_id_task').val();

            if (personal_phone_ === '' || personal_phone_code_ === '') {
                $('#personal_phone_err_msg').show();
                return false;
            } else {
                $('#personal_phone_err_msg').hide();
                show_loader();
            }

            var url_ = '<?php echo site_url('user/personal/update_personal_phone'); ?>';
            $.post(
                    url_,
                    {
                        'phone': personal_phone_,
                        'id_country_code': personal_phone_code_,
                        'id_task': id_task_,
                        'id_temp_request': $('#personal_phone_id_temp_request').val()
                    },
            function (res) {
                hide_loader();
                $('#edit_personal_phone_frm').html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">x</button>' + res + '</div>');
            }
            );

            return false;

        });

        ///////////////////////////update_benefit_btn ////////////
        $('#update_benefit_btn').click(function () {

            var rsa_pin_ = $('#rsa_pin').val();
            var pfs_ = $('#pfs').val();
            var id_task_ = $('#benefit_id_task').val();

            if (rsa_pin_ === '') {
                $('#rsa_pin_err_msg').show();
                return false;
            } else if (pfs_ === '') {
                $('#rsa_pfs_err_msg').show();
                return false;
            } else {
                $('#rsa_pin_err_msg').hide();
                $('#rsa_pfs_err_msg').hide();
                show_loader();
            }

            var url_ = '<?php echo site_url('user/personal/update_benefit'); ?>';
            $.post(
                    url_,
                    {
                        'rsa_pin': rsa_pin_,
                        'pfs': pfs_,
                        'id_task': id_task_,
                        'id_temp_request': $('#benefit_id_temp_request').val()
                    },
            function (res) {
                hide_loader();
                $('#edit_benefit_frm').html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">x</button>' + res + '</div>');
            }
            );

            return false;

        });

        /////////////////////////////// update_bank_btn //////////////////////
        $('#update_bank_btn').click(function () {

            var account_name_ = $('#account_name').val();
            var id_bank_ = $('#id_bank').val();
            var account_type_ = $('#account_type').val();
            var account_number_ = $('#account_number').val();
            var id_task_ = $('#bank_id_task').val();

            if (account_name_ === '') {
                $('#account_name_err_msg').show();
                return false;
            } else if (id_bank_ === '') {
                $('#id_bank_err_msg').show();
                return false;
            } else if (account_type_ === '') {
                $('#account_type_err_msg').show();
                return false;
            } else if (account_number_ === '') {
                $('#account_number_err_msg').show();
                return false;
            } else {
                $('.error').hide();
                show_loader();
            }

            var url_ = '<?php echo site_url('user/personal/update_bank'); ?>';
            $.post(
                    url_,
                    {
                        'account_name': account_name_,
                        'id_bank': id_bank_,
                        'account_type': account_type_,
                        'account_number': account_number_,
                        'id_task': id_task_,
                        'id_temp_request': $('#bank_id_temp_request').val()
                    },
            function (res) {
                hide_loader();
                $('#edit_bank_details_frm').html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">x</button>' + res + '</div>');
            }
            );

            return false;

        });

        //////////////////////// update_contact1_btn /////
        $('#update_contact1_btn').click(function () {

            var emergency_first_name_ = $('#emergency_first_name').val();
            var emergency_last_name_ = $('#emergency_last_name').val();
            var emergency_address_ = $('#emergency_address').val();
            var emergency_email_ = $('#emergency_email').val();
            var emergency_phone_number_ = $('#emergency_phone_number').val();
            var emergency_employer_ = $('#emergency_employer').val();
            var emergency_work_address_ = $('#emergency_work_address').val();
            var id_task_ = $('#contact1_id_task').val();

            if (emergency_first_name_ === '' || emergency_last_name_ === '' || emergency_phone_number_ === '' || emergency_employer_ === '') {
                $('.error_em').show();
                return false;
            } else {
                $('.error_em').hide();
                show_loader();
            }

            var url_ = '<?php echo site_url('user/personal/update_emergency_contact1'); ?>';
            $.post(
                    url_,
                    {
                        'emergency_first_name': emergency_first_name_,
                        'emergency_last_name': emergency_last_name_,
                        'emergency_address': emergency_address_,
                        'emergency_email': emergency_email_,
                        'emergency_phone_number': emergency_phone_number_,
                        'emergency_employer': emergency_employer_,
                        'emergency_work_address': emergency_work_address_,
                        'id_task': id_task_,
                        'id_temp_request': $('#contact1_id_temp_request').val()
                    },
            function (res) {
                hide_loader();
                $('#edit_emergency_contact1_frm').html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">x</button>' + res + '</div>');
            }
            );

            return false;

        });

        //////////////////////// update_contact2_btn /////
        $('#update_contact2_btn').click(function () {

            var emergency_first_name_ = $('#emergency_first_name2').val();
            var emergency_last_name_ = $('#emergency_last_name2').val();
            var emergency_address_ = $('#emergency_address2').val();
            var emergency_email_ = $('#emergency_email2').val();
            var emergency_phone_number_ = $('#emergency_phone_number2').val();
            var emergency_employer_ = $('#emergency_employer2').val();
            var emergency_work_address_ = $('#emergency_work_address2').val();
            var id_task_ = $('#contact2_id_task').val();

            if (emergency_first_name_ === '' || emergency_last_name_ === '' || emergency_phone_number_ === '' || emergency_employer_ === '') {
                $('.error_em').show();
                return false;
            } else {
                $('.error_em').hide();
                show_loader();
            }

            var url_ = '<?php echo site_url('user/personal/update_emergency_contact2'); ?>';
            $.post(
                    url_,
                    {
                        'emergency_first_name2': emergency_first_name_,
                        'emergency_last_name2': emergency_last_name_,
                        'emergency_address2': emergency_address_,
                        'emergency_email2': emergency_email_,
                        'emergency_phone_number2': emergency_phone_number_,
                        'emergency_employer2': emergency_employer_,
                        'emergency_work_address2': emergency_work_address_,
                        'id_task': id_task_,
                        'id_temp_request': $('#contact2_id_temp_request').val()
                    },
            function (res) {
                hide_loader();
                $('#edit_emergency_contact2_frm').html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">x</button>' + res + '</div>');
            }
            );

            return false;

        });

    }); // end doc ready

</script>