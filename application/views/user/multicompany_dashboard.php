<div class="span12" style="margin-left:0;">
    <?php
    if (!empty($companies)) {
        $jey = 1;
        $i = 1;
        foreach ($companies as $c) {

            if (!$c['company']->id_own_company) {
                $head = ' box-color ';
            } else {
                $head = '';
            }

            if ($c['tasks'] > 0) {
                $tasks = $c['tasks'] . ' Tasks';
                $tbg = ' badge-warning ';
            } else {
                $tasks = 'No Pending Tasks';
                $tbg = '';
            }
            ?>
            <div class="span4">
                <div class="box <?php echo $head; ?> box-bordered">
                    <div class="box-title">
                        <h3>
                            <?php echo $c['company']->company_name; ?>
                        </h3>
                    </div>
                    <div class="box-content">
                        <div class="span12">
                            <p class="lead"><?php echo $c['role']; ?></p>
                            <p class="badge <?php echo $tbg; ?>"><?php echo $tasks; ?></p>
                            <p>
                                <a href="<?php echo site_url('user/access_to/' . $c['company']->c_id_string . '/' . $c['company']->u_id_string); ?>" class="btn btn-success accnt">Access Account</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            if ($jey >= 3) {
                $jey = 0;
                ?></div><div class="span12" style="margin-left:0;">
                <?php
            }

            $jey++;
            $i++;
        }
    }
    ?>
</div>
