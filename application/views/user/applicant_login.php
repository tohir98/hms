<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
?>
<body>
	<div class="container">
		<div class="masthead">
			<div class="row-fluid">
				<div class="span6">
					<h3 class="muted" style=" vertical-align: bottom">
						<!-- <a href="<?php echo $login_url; ?>"><img src="/img/talentbase-logo.png"></a> Careers -->
						<?php if($logo_path != NULL) { ?>
							<img src="<?php $file_url = $this->cdn_api->temporary_url($logo_path); echo $file_url; ?>" width="220px" height="220px">
						<?php } elseif($logo_text != NULL) { 
							echo $logo_text;
						} else {
						?>
							<img src="/img/talentbase-logo.png">
						<?php } ?>
					</h3>
				</div>
				<div class="span6">
				</div>
			</div>
			<hr style=" margin-top: 0">
		</div>
		
<div class="row-fluid">
    <div class="span2">
        <a class="btn btn-inverse btn-small pull-left" href="<?php echo site_url(); ?>">
            <i class="icon-arrow-left icon-white"></i> Back To All Jobs
        </a>
    </div>
    <div class="span10">
	
</div>
</div>
<div class="alert alert-info" style="margin-top:20px;">
    You have to be logged in to continue. Please login or register to complete the job application.
</div>
<?php if($error_message != '') { ?>
<div class="alert alert-danger" style="margin-top:20px;">
    <?php echo $error_message; ?>
</div>
<?php } ?>
<?php if($success_message != '') { ?>
<div class="alert alert-success" style="margin-top:20px;">
    <?php echo $success_message; ?>
</div>
<?php } ?>
<div class="row-fluid">
    <div class="span6">
        <div class="box box-bordered">
            <div class="box-title">
                <h3>Returning Applicant</h3>
            </div>
            <div class="box-content nopadding">
    <form id="applicant_login_frm" class="form-horizontal form-bordered" method="post">
        <div class="control-group">
                        <label for="textfield" class="control-label">Email</label>
                        <div class="controls">
                            <input type="email" id="email" placeholder="E-mail address" class="input-xlarge" name="email" required />
                        </div>
        </div>
        <div class="control-group">
                        <label for="password" class="control-label">Password</label>
                        <div class="controls">
                            <input type="password" id="password" placeholder="Password" class="input-xlarge" name="password" required />
                        </div>
                    </div>
        <div class="control-group">
                        <label for="textfield" class="control-label"></label>
                        <div class="controls">
                            <div class="forget">
                                <a href="<?php echo $forgot_password_url; ?>"><span>Forgot password?</span></a>
                            </div>
                        </div>
                    </div>
        <input type="submit" id="login_button" class="btn btn-warning btn-large pull-right" value="Login" name="login_button" />
    </form>
            </div>
        </div>
    </div>

<div class="span6">
    <div class="box box-bordered">
            <div class="box-title">
                <h3>New Applicant</h3>
            </div>
            <div class="box-content nopadding">
    <form id="register" class="form-horizontal form-bordered" method="post">
        <div class="control-group">
                        <label for="textfield" class="control-label">Email</label>
                        <div class="controls">
                            <input type="email" id="register_email" placeholder="E-mail address" class="input-xlarge" name="register_email" required />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="textfield" class="control-label">First name</label>
                        <div class="controls">
                            <input type="text" id="fname" placeholder="First name" class="input-xlarge" name="fname" required />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="textfield" class="control-label">Last name</label>
                        <div class="controls">
                            <input type="text" id="lname" placeholder="Last name" class="input-xlarge" name="lname" required />
                        </div>
                    </div>
        <input type="submit" id="register_button" class="btn btn-primary btn-large pull-right" value="Register" name="register_button" />
    </form>
            </div>
    </div>
</div>
</div>
<hr style=" margin-top: 50px;">
<script>
    mixpanel.track("Job description page");
</script>
<div class="footer">
<p>Powered by TalentBase &copy; <?php echo date('Y'); ?></p>
</div>
</body>