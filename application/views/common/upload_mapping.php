


<style>

    #import-map {
        width: inherit;
        max-width: 1400px;
    }

    #import-map th.map-activecol {
        border-top-color: #52bad5;
    }

    #import-map .map-activecol {
        border-color: #52bad5;
        border-bottom: none;
        color: #333;
        text-align: left;
    }

    #import-map th {
        height: 130px;
        /*min-width: 250px;*/
        text-align: middle;
        vertical-align: middle;
        border-top: 2px solid #d0d0d0;
    }

    #import-map td, #import-map th {
        /*padding: 6px 10px;*/
        font-size: 13px;
        border-left: 2px solid #d0d0d0;
        border-right: 2px solid #d0d0d0;
    }

    #import-map .map-activecol {
        border-color: #52bad5;
        border-bottom: none;
        color: #333;
        text-align: left;
    }

    .box_enabled .pagination ul>li>a{

        background-color:  #1e74c5;
    }

    .box_disabled .pagination ul>li>a{
        background-color:  #red;
    }

    .pagination {
        margin: 10px 0;
    }
    .pagination ul>li, .pagination ul>li>span {
        float: left;
        padding: 4px 12px;
        line-height: 20px;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
        border-left-width: 0;
    }
    
    .bootstrap-switch-container{
        width: 110px
    }
</style>

<?php
//var_dump($res);
/* Check if session message as message */
if ($this->session->userdata('errors') != '') {
    ?>
    <div class="alert alert-danger">
        <button data-dismiss="alert" class="close" type="button">×</button>
        <strong><?php echo $this->session->userdata('errors'); ?></strong>
    </div>
    <?php
    $this->session->unset_userdata('errors');
}
if ($this->session->userdata('confirm') != '') {
    ?>
    <div class="alert alert-success">
        <button data-dismiss="alert" class="close" type="button">×</button>
        <strong><?php echo $this->session->userdata('confirm'); ?></strong>
    </div>
    <?php
    $this->session->unset_userdata('confirm');
}
?>

<div class="page-header">
    <div class="pull-left">
        <h1> Add New Employee(s)</h1>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box-content">
            <div class="row-fluid">
                <div class="span12">
                    <ul class="nav nav-tabs" id="myTab">
                        <li class="">
                            <a href="<?= site_url('employee/add_new_employee') ?>">New Employee</a>
                        </li>
                        <li class="active">
                            <a href="#" >Employee Bulk Upload</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <!-- Company profile -->

                        <!-- Bulk users upload -->
                        <div class="tab-pane active" id="bulk_users">

                            <form action="<?= site_url('organizations/organizations/update_backend') ?>" id="upload_frm" name="upload_frm" method="post" action="" class='form-horizontal form-bordered'>
                                <div class="panel panel-default">
                                    <div class="table-responsive" style=" overflow: scroll; max-height: 400px">
                                        <table id="import-map" class="table table-hover table-nomargin table-bordered" style=" max-width: 1300px">
                                            <thead>
                                                <tr>

                                                    <?php for ($k = 0; $k <= $colCount; $k++) { ?>
                                                        <th id="header-<?php echo $k; ?>" class="header-cell">
                                                            <select name="<?php echo $k; ?>_head" id="<?php echo $k; ?>_head" class="select2-me input-medium col-name-field">
                                                                <?php
                                                                foreach ($mapping_cols as $key => $value) {
                                                                    if ($key == '-1') {
                                                                        $sel = 'selected="selected" ';
                                                                    } else {
                                                                        $sel = '';
                                                                    }
                                                                    ?>
                                                                    <option value="<?php echo $key; ?>" <?php echo $sel; ?>> <?php echo $value ?></option>
                                                                <?php } ?>
                                                            </select>
                                                <div style="margin-top:5px; display: <?php //echo 'none'   ?>;"> 
                                                    <?php if ($k > 0) { ?>
                                                    <?php } ?>

                                                    <a href="#" class="btn btn-primary next-importmap" id="ok-<?php echo $k; ?>" data-id="<?php echo $k; ?>" style=" display: none">Save <i class="icon-arrow-right"></i></a> 
                                                    <a href="#" class="btn btn-warning edit-importmap" id="edit-<?php echo $k; ?>" data-id="<?php echo $k; ?>" style=" display: none">Edit</a> 
                                                    Skip this column? &nbsp;
                                                    <div class="pagination">
                                                        <ul >
                                                            <li style=" cursor: pointer" value="<?php echo $k; ?>" data-value="on" class="delete-importmap">Yes</li>
                                                            <li style=" cursor: pointer" value="<?php echo $k; ?>" data-value="off" class="delete-importmap switch-off">No</li>
                                                        </ul>
                                                    </div>
<!--                                                    <div style="margin-top: 5px;">
                                                         <input type="checkbox" name="my-checkbox" value="<?php echo $k; ?>" data-size="small" data-on-text="Yes" data-off-text="No" data-on-color="danger" data-off-color="success" data-size="110" class="inline" >
                                                    </div>-->
                                                </div> 
                                                </th>
                                            <?php } ?>
                                            </tr>
                                            </thead>


                                            <?php foreach ($res as $r) { ?>
                                                <tr>
                                                    <?php for ($k = 0; $k <= $colCount; $k++) : ?>

                                                        <td class="header-<?php echo $k; ?>-content">
                                                            <?php echo $r[$k] ?>
                                                        </td>

                                                    <?php endfor; ?>
                                                </tr>
                                            <?php } ?>

                                        </table>

                                        <input type="hidden" name="value" id="value" value=""/>
                                        <input type="hidden" name="id_company" id="id_company" value="<?php echo $id_company ?>"/>
                                        <input type="hidden" name="url" id="url" value="<?=$redirect_url;?>"/>

                                    </div>
                                    <div class="form-actions">
                                        <label class="control-label">
                                            Update Employee Records ? <input type="checkbox" name="updateRecord" value="yes" />
                                        </label>
                                        <input type="button" id="complete_import_btn" value="Complete Import" class="btn btn-primary"/>
                                        <a href="<?= $cancel_redirect_url; ?>" class="btn btn-warning cancel"/>Cancel</a>
                                    </div>
                                </div>
                            </form>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Error Modal -->
<div id="err_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-body">
        <p>Your request could not be completed at moment.</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">OK</button>
    </div>
</div>
<!-- End Confirm upload -->

<div id="cancel-modal-popup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-body">
        <p>Are you sure you want to cancel the import process?</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
        <a href="#" class="btn btn-danger closeme">Yes, Cancel</a>
    </div>
</div>




<script>


    $(document).ready(function () {

        $("[name='my-checkbox']").bootstrapSwitch();

        $('input[name="my-checkbox"]').on('switchChange.bootstrapSwitch', function (event, state) {
            if (state) {
                $('#' + $(this).val() + '_head').prop('disabled', true);
            } else {
                $('#' + $(this).val() + '_head').prop('disabled', false);
            }
        });


        $('.cancel').click(function (eve) {
            eve.preventDefault();
            $('#cancel-modal-popup').modal('show').fadeIn();

            var page = $(this).attr("href");
            $('a.closeme').attr('href', page);

        });

        $(".switch-off").css("background-color", "green");
        var _current_focus = '';
        var post_data = '';
        
       

        $('#import-map').delegate('.delete-importmap', 'change click', function (e) {
            e.preventDefault();
            var sel_val = $(this).val();

            var id = $(this).attr('data-value');
            if (id == 'on') {
                //$(this).addClass('box_enabled');
                $(this).css("background-color", "red");
                $(this).next().css("background-color", "white");
                $('#' + sel_val + '_head').prop('disabled', true);
            } else {
                $('#' + sel_val + '_head').prop('disabled', false);
                $(this).css("background-color", "green");
                $(this).prev().css("background-color", "white");
            }

            return false;

        });


       
        $('#import-map').delegate('.next-importmap', 'click', function (e) {
            e.preventDefault();
            var emp_str = '';
            var a = $(this).attr('data-id');
            var sel_head = $('#' + a + '_head').val();          // value of selected field
            //alert(sel_head);
            $(".col-name-field option[value=" + sel_head + "]").attr('disabled', 'disabled');
            $('.' + _current_focus + '-content').each(function () {
                emp_str = emp_str + '|' + $(this).html().trim();
            });
            $("#ok-" + a).hide();
            $("#edit-" + a).show();
            update_backend(sel_head, emp_str);
            return false;
        }); //

        $('#import-map').delegate('.col-name-field', 'change', function (e) {
            e.preventDefault();
            var emp_str = '';
            var sel_head = $(this).val();          // value of selected field
            $(".col-name-field option[value=" + sel_head + "]").attr('disabled', 'disabled');
            $('.' + _current_focus + '-content').each(function () {
                emp_str = emp_str + '|' + $(this).html().trim();
            });
            update_backend(sel_head, emp_str);
            return false;
        });


        $("#import-map").delegate(".header-cell", "focus mouseover", function () {
            var elem = $(this);
            elem.addClass('map-activecol');
            _current_focus = elem.attr('id');
        });

        function update_backend(col, values) {
            post_data = post_data + col + '*' + values + '*';
        }   // end func update_backend

        //function complete_import(col, values){
        $("#complete_import_btn").click(function (eve) {
            eve.preventDefault();
            
            $("#complete_import_btn").attr('disabled', true);
            $('#value').val(post_data);
            $('#upload_frm').submit();
            
            return false;
            
        })   // end func complete import


    });



</script>