<?php if ($receipt): /* @var $receipt payment\Receipt */ ?>
    <div class="box box-bordered">
        <div class="box-title">
            <h2>Bill Payment</h2>
        </div>

        <div class="box-content">

            <div class="row-fluid">
                <div class="span4 tleft">Reference:</div>
                <div class="span6"><?= $receipt->getRef(); ?></div>
            </div>
            <div class="row-fluid">
                <div class="span4 tleft">Summary: </div>
                <div class="span6"><?= $receipt->getDescription(); ?></div>
            </div>
            <div>
                <h4>Invoiced To: </h4>
                <p>
                    <?= $user->first_name ?> <?= $user->last_name ?>
                    <br/>
                    <?= $this->user_auth->get('company_name') ?>
                </p>
                <br/>
                <br/>
            </div>

            <div>
                <table class="table table-invoice">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Description</th>
                            <th>Unit Price</th>
                            <th>Qty</th>
                            <th class="tright">Discount</th>
                            <th class="tright">Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($receipt->getItems() as $i => $item): ?>
                            <tr>
                                <td><?= $i + 1 ?></td>
                                <td ><?= $item->getDescription() ?> </td>
                                <td class="price"><?= number_format($item->getUnitAmount(), 2) ?> </td>
                                <td class="qty"><?= $item->getQuantity() ?> </td>
                                <td class="tright"><?= $item->getDiscount() > 0 ? number_format($item->getDiscount(), 2) . '%' : '' ?> </td>
                                <td class="total"><?= number_format($item->getUnitAmountDiscounted() * $item->getQuantity(), 2) ?> </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                        <?php if (!$receipt->isVatInclusive()): ?>
                            <tr>
                                <td class="tright" colspan="5">
                                    Sub-Total
                                </td>
                                <td class="tright">
                                    <?= number_format($receipt->getItemTotal(), 2) ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="tright" colspan="5">
                                    VAT (<?= $receipt->getVatRate() ?>%)
                                </td>
                                <td class="tright" colspan="">
                                    <?= number_format($receipt->getVatAmount(), 2) ?>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <tr>
                            <th class="tright" colspan="5">
                                Grand Total
                            </th>
                            <th class="tright" colspan="">
                                <?= number_format($receipt->getTotalAmount(), 2) ?>
                            </th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    </div>
<?php else: ?>
    <div class="alert alert-warning">
        Bill details not found.
    </div>
<?php endif; ?>
