<?php $path = $this->input->server('REQUEST_URI'); ?>
<ul class="nav nav-tabs" id="myTab">
    <li class="<?= strstr($path, 'employee/settings') ? 'active' : '' ?>">
        <a href="<?= site_url('employee/settings') ?>">Approval process</a>
    </li>
    <li class="<?= strstr($path, 'employee/view_company_location') ? 'active' : '' ?>">
        <a href="<?= site_url('employee/view_company_location') ?>" >Locations</a>
    </li>
    <li class="<?= strstr($path, 'employee/view_departments') ? 'active' : '' ?>">
        <a href="<?= site_url('employee/view_departments') ?>" >Departments</a>
    </li>
    <li class="<?= strstr($path, 'employee/view_roles') ? 'active' : '' ?>">
        <a href="<?= site_url('employee/view_roles') ?>" >Employee Roles/Levels</a>
    </li>
    <?php if ($this->user_auth->have_perm('employee:checklist')): ?>
        <li class="<?= strstr($path, 'employee/checklist') ? 'active' : '' ?>">
            <a href="<?= site_url('employee/checklist') ?>" >Checklist</a>
        </li>
    <?php endif; ?>
</ul>
