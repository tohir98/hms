<body>
    <div class="container">
        <div class="masthead">
            <div class="row-fluid">
                <div class="span6">
                    <h3 style=" vertical-align: bottom" class="muted">
                        <img src="/img/hms-logo.jpg" width="220px" height="220px">
                        <!--</a> Careers-->
                    </h3>
                </div>
                <div class="span6">

                </div>
            </div>
        </div>
    </div>

    <div class="mainHeader homeHeader" style="background: <?php echo $bg_color; ?>;">
        <div class="container">
            <div class="row-fluid">
                <p><?php echo $banner_text; ?></p>
                <div class="btnGroup">
                    <a class="btn btn-lg" style="background: <?php echo $bg_color; ?>;" href="<?php echo $company_url; ?>">About Us</a>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row-fluid">
            <div class="span5" > 
                <h3>Current Vacancies</h3>





            </div>
            <div class="span6 offset1">
                <h3>About <?php echo $company_name; ?></h3>
                <p class="muted">
                    The hospital management system or the hospital billing software incorporates the features which are targeted at dealing with each and every aspects of any hospital and this software also covers the areas like OPD, reception, outpatients, inpatients, inventory of the materials and the medicines, medical records, scheduling of the duties of the doctors, appointments, accounts and laboratories. Besides, this software also offers the security feature that prevents the stored information of the hospitals from getting misused. 
                    <!--The common features of this type of software are:-->
<!--                <ul>
                    <li>This software facilitates complete and smooth running of the reception</li>
                    <li>It manages all the bed allocation systems and the wards of the hospitals</li>
                    <li>It manages the laboratory and the equipments</li>
                    <li>It manages the treatments as well as the entire history of the patients</li>
                    <li>This software manages the management with the timely and regular refurbishment of stocks of the instruments and the medicines</li>
                    <li>It also manages the timely and proper accounting to make sure a proper and current billing</li>
                    <li>The biggest benefit of the hospital management system is that it can be used at the same time via a number of users</li>
                </ul>-->
                   
                </p>
                <p><a href="<?php echo $company_url; ?>" class="btn btn-inverse">Learn More»</a></p>
            </div>
        </div>
        <hr style=" margin-top: 20;">

        <div class="footer">
            <p>Powered by CleanSoft Lab &copy; <?php echo date('Y'); ?>
                <a href="<?php echo site_url('login'); ?>" class="pull-right">Login</a>
            </p>
        </div>
    </div> <!-- /container -->

    <script>
        $(document).ready(function () {
            $('.jobs_tab').click(function () {
                var letter = $(this).attr('data-item');
                $('.jobs_tab_container').hide();
                $('#' + letter + '_container').show();
                $('.tp').removeClass('active');
                $(this).parent().addClass('active');
            });
        });
    </script>	

</body>