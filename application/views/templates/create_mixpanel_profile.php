<?php 
	$statuses = $this->user_auth->statuses(); 
	$account_type = $this->user_auth->get_account_type($this->session->userdata('account_type'));
?>

<script>
	
	mixpanel.identify("<?php echo $this->session->userdata('id_string'); ?>");
	mixpanel.people.set({
	    "$first_name" : "<?php echo $this->session->userdata('display_name'); ?>",
	    "$last_name" : "<?php echo $this->session->userdata('last_name'); ?>",
	    "$email" : "<?php echo $this->session->userdata('email');?>",
	    "$phone" : "",
	    "$access_level" : "<?php echo $account_type->name;?>",
	    "$company_name" : "<?php echo $this->session->userdata('company_name');?>",
	    "$company_id" : "<?php echo $this->session->userdata('id_company');?>",
	    "$current_status" : "<?php echo $statuses[$this->session->userdata('status')];?>"

	});
</script>