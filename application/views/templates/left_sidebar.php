<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>
<div id="left">
    <div class="subnav">
        <div class="subnav-title" style="white-space: normal;">
            <span style="white-space: initial; width: 100%; display: block "><?php echo $company_name; ?></span>
        </div>
    </div>
    <?php echo $side_nav_menu; ?>
</div>