<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!isset($activity_bar)) {
    $activity_bar = true;
}

if ($activity_bar == true) {
    $content_vlass = 'span9';
} else {
    $content_vlass = 'span12';
}

if (!isset($isMinimal)) {
    $isMinimal = false;
}



?>
<?php include 'header.php'; ?>
<body style="overflow-x: hidden">
    <?php if (isset($GLOBALS['osLicenseError']) && $this->user_auth->get('account_type') == USER_TYPE_ADMIN): ?>
        <div style="width: 100%; border-bottom: solid 1px #ff9933; background: #ffffcc; text-align: center;">
            <div style="padding: 10px;">
                <?= $GLOBALS['osLicenseError']; ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="wrap" id="loader">
        <div class="bar"></div>
        <div class="bar"></div>
        <div class="bar"></div>
        <div class="bar"></div>
        <div class="bar"></div>
        <div class="bar"></div>
        <div class="bar"></div>
    </div>

    <?php
    include 'top_menu.php';
    ?>


    <div class="container-fluid" id="content">
       <?php include 'left_sidebar.php'; ?>
        <div id="main">
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="<?php echo $content_vlass; ?>">
                        <!-- Check for Internet connectivity -->
                        <div class="show_content"> </div>
                        <?php
                        echo $page_content;
                        if (is_callable($content_cb)) {
                            call_user_func($content_cb);
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if (!$isMinimal): ?>
        <?php include 'footer.php'; ?>
    <?php endif; ?>
</body>
</html>