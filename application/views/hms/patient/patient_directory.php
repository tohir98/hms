<div class="page-header">
    <div class="pull-left">
        <h1>Patient Directory</h1>
    </div>
</div>

<div class="breadcrumbs">
    <ul>
        <li>
            <a href="<?= site_url('admin/dashboard') ?>">Dashboard</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Patient Directory</a>
        </li>
    </ul>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="box box-bordered">
            <div class="box-title">
                <h3>
                    <i class="icons icon-calendar"></i>Patient List
                </h3>
                <a href="<?= site_url('patient/addPatient') ?>" class="btn btn-warning pull-right" style="margin-right: 5px">New Patient</a>
            </div>
            <div class="box-content-padless">
                <?php if (!empty($patients)) : ?>
                    <table class="table table-striped dataTable">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($patients as $patient) : ?>
                                <tr>
                                    <td class="img">
                                        <img src="/img/40x40.gif" width="40" height="40" />
                                    </td>
                                    <td><?= $patient->firstName . ' ' . $patient->lastName; ?></td>
                                    <td>
                                        <?= $patient->phone; ?><br>
                                        <span class="muted"><?= $patient->address; ?></span><br>
                                        <span class="muted"><?= $patient->city; ?></span>
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Action <span class="caret"></span></a>
                                            <ul class="dropdown-menu pull-right">
                                                <li><a href="<?=$patient->patient_id?>">View Records</a></li>
                                                
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php
                else:
                    echo show_no_data('No patients available at moment');
                    ?>
<?php endif; ?>
            </div>
        </div>

    </div>

</div>