<script>
    var Const_NIGERIA = '<?= NIGERIA ?>';
</script>
<div ng-app="EmployeeApp">
    <form action="" method="POST" enctype="multipart/form-data" class='form-horizontal form-wizard form-wysiwyg' id="ss" ng-controller="formCtrl">
        <div>
            <h4 class="light-title" style="padding: 0px 0 15px;">Personal Information</h4>
            <div class="control-group">
                <label for="first_name" class="control-label">First name:</label>
                <div class="controls">
                    <input type="text" name="firstName" id="firstName" class="input-xlarge" required />
                </div>
            </div>
            <div class="control-group">
                <label for="last_name" class="control-label">Middle name:</label>
                <div class="controls">
                    <input type="text" name="middleName" id="middleName" class="input-xlarge" required />
                </div>
            </div>
            <div class="control-group">
                <label for="last_name" class="control-label">Last name:</label>
                <div class="controls">
                    <input type="text" name="lastName" id="lastName" class="input-xlarge" required />
                </div>
            </div>
            <div class="control-group">
                <label for="id_gender" class="control-label">Gender:</label>
                <div class="controls">
                    <select name="id_gender" id="id_gender" required class="select2-me input-xlarge">
                        <option value="" selected>Select Gender</option>
                        <option value="1">Male</option>
                        <option value="2">Female</option>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label for="id_marital_status" class="control-label">Marital Status:</label>
                <div class="controls">
                    <select name="id_marital_status" id="id_marital_status" required class="select2-me input-xlarge">
                        <option value="" selected>Select Status</option>
                        <option value="1">Single</option>
                        <option value="2">Married</option>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label for="dob" class="control-label">Birth Date:</label>
                <div class="controls">
                    <input type="text" id="dob" name="dob" class="datepick" required />
                </div>
            </div>
            <div class="control-group">
                <label for="height" class="control-label">Height:</label>
                <div class="controls">
                    <input type="number" type="number" step="0.1" id="height" name="height" class="" />
                </div>
            </div>
            <div class="control-group">
                <label for="height" class="control-label">Weight:</label>
                <div class="controls">
                    <input type="number" step="0.1" id="weight" name="weight" class="" />
                </div>
            </div>
            <div class="control-group">
                <label for="height" class="control-label">Blood Group:</label>
                <div class="controls">
                    <select id="blood_group_id" name="blood_group_id" class="select2-me input-small">
                        <?php foreach ($bloodGroups as $val => $bloodgroup): ?>
                            <option value="<?= $val; ?>"><?= $bloodgroup; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label for="id_nationality" class="control-label">Country of Origin:</label>
                <div class="controls">
                    <select name="id_nationality" id="id_nationality" class="select2-me input-large">
                        <option value="" selected>Select</option>
                        <?php if (!empty($countries)) : ?>
                            <?php foreach ($countries as $c) : ?>
                                <option value="<?php echo $c->id_country; ?>"><?php echo $c->country; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>
        </div>


        <div>
            <h4 class="light-title" style="padding: 5px 0 15px;">Contact Information</h4>
            <div class="control-group">
                <label for="additionalfield" class="control-label">Phone Number:</label>
                <div class="controls">
                    <input type="text" name="phone" id="phone" required />
                </div>
            </div> 
            
            <div class="control-group">
                <label for="additionalfield" class="control-label">Email:</label>
                <div class="controls">
                    <input type="text" name="email" id="email" />
                </div>
            </div> 
            
             <div class="control-group">
                <label for="address" class="control-label">Address:</label>
                <div class="controls">
                    <input type="text" id="address" name="address" class="" required />
                </div>
            </div>
            <div class="control-group">
                <label for="city" class="control-label">City:</label>
                <div class="controls">
                    <input type="text" id="city" name="city" class="" required />
                </div>
            </div>
            <div class="control-group">
                <label for="country" class="control-label">Country:</label>
                <div class="controls">
                    <select name="country" id="country" ng-model="employee.id_country" ng-change="loadStates();" class="select2-me input-large">
                        <?php if (!empty($countries)) : ?>
                            <?php foreach ($countries as $c) : ?>
                                <option value="<?php echo $c->id_country; ?>"><?php echo $c->country; ?></option>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </select> &nbsp; State: &nbsp;
                    <!--<div class="" ng-if="statesStatus === ''">-->
                    <select name="id_state" id="id_state" class="select2-me input-medium" data-rule-required="true" ng-model="employee.id_state">
                        <option ng-repeat="state in states" value="{{state.id}}" ng-selected="employee.id_state.indexOf(state.id + '') > -1">
                            {{state.name}}
                        </option>
                    </select>

                    <!--</div>-->
                </div>
            </div>

        </div>

        <div>
            <h4 class="light-title" style="padding: 5px 0 15px;">Profile Picture</h4>
            <div class="control-group">
                <div class="controls">
                    <div class="check-demo-col">
                        <div class="check-line">
                            <input type="radio" id="complete_now" ng-model="picture" value="now" name="profile_pic_select" > 
                            <label class='inline' for="complete_now">Complete This Now</label>
                        </div>
                        <div class="check-line">
                            <input type="radio" id="complete_later" ng-model="picture" value="later" name="profile_pic_select"  > 
                            <label class='inline' for="complete_later">Complete this later</label>
                        </div>
                    </div>
                </div>
            </div>
            <div id="profile_pic_now_container" ng-show="showPicContainer('now')">
                <div class="control-group">
                    <label for="textfield" class="control-label"></label>
                    <div class="controls">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" />
                            </div>
                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                            <div>
                                <span class="btn btn-file">
                                    <span class="fileupload-new">Select file</span>
                                    <span class="fileupload-exists">Change</span>
                                    <input type="file" name="profile_picture" title="Pls upload a profile picture" />
                                </span>
                                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="profile_pic_later_container" ng-show="showPicContainer('later')"></div>	
        </div>


        <div class="control-group">
            <div class="controls">
                <input type="submit" class="btn btn-success btn-large" value="Submit" id="next">
            </div>
        </div>
    </form>
</div>

<script src="<?= site_url('/js/employee.js') ?>"></script>
<script>
                $('#no_email').click(function () {
                    if ($(this).is(':checked')) {
                        $('#work_email').attr('disabled', 'disabled');
                    } else {
                        $('#work_email').removeAttr('disabled');
                    }
                });

                $('#employment_status').change(function () {

                    var id_status = $(this).val();

                    $('#probation_expire_date_container').hide();
                    $('#probation_message_container').hide();
                    $('#probation_message_container_txt').html('');
                    $('#terminated_expire_date_container').hide();

                    if (id_status == '<?php echo USER_STATUS_PROBATION_NO_ACCESS; ?>') {
                        $('#probation_expire_date_container').show();
                        $('#probation_message_container').show();
                        $('#probation_message_container_txt').html('This employee is on probation and has no access to employee Portal. Please set probation end-date below');
                    }

                    if (id_status == '<?php echo USER_STATUS_PROBATION_ACCESS; ?>') {
                        $('#probation_expire_date_container').show();
                        $('#probation_message_container').show();
                        $('#probation_message_container_txt').html('This employee is on probation and has access to employee Portal. Please set probation end-date below');
                    }

                    if (id_status == '<?php echo USER_STATUS_ACTIVE_NO_ACCESS; ?>') {
                        $('#probation_message_container').show();
                        $('#probation_message_container_txt').html('This employee will not have access to the employee portal. Recommended for semi/unskilled employees');
                    }

                    if (id_status == '<?php echo USER_STATUS_TERMINATED; ?>') {
                        $('#terminated_expire_date_container').show();
                    }

                    /* CHECK IF STATUS IS ACTIVE OR PROBATION ACCESS */
                    if (id_status == '<?php echo USER_STATUS_PROBATION_ACCESS; ?>' || id_status == '<?php echo USER_STATUS_ACTIVE; ?>') {
                        $('#div_no_email').hide();
                    } else {
                        $('#div_no_email').show();
                    }

                    if (id_status != '<?php echo USER_STATUS_ACTIVE; ?>' && id_status != '<?php echo USER_STATUS_PROBATION_ACCESS; ?>') {
                        $('.user_request_data').each(function () {
                            $(this).find('input:radio').removeAttr("checked");
                            $(this).hide();
                            $(this).prev().find('input:radio').click();
                        });
                    } else {
                        $('.user_request_data').each(function () {
                            $(this).show();
                        });
                    }

                });
</script>
