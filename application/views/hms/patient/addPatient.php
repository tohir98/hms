<!-- Masked inputs -->
<script src="/js/plugins/maskedinput/jquery.maskedinput.min.js"></script>

<!-- Display error/success message -->
<br/>
<?php if ($error_message != '') { ?>
    <div class="alert alert-danger">
        <button data-dismiss="alert" class="close" type="button">x</button>
        <?php echo $error_message; ?>
    </div>
<?php
}
if ($success_message != '') {
    ?>
    <div class="alert alert-success">
        <button data-dismiss="alert" class="close" type="button">x</button>
    <?php echo $success_message; ?>
    </div>
<?php } ?>

<div class="page-header">
    <div class="pull-left">
        <a class="btn btn-warning" href="<?= site_url('patient') ?>"><i class="icons icon-chevron-left"></i> Back</a>
        <h1>New Patient</h1>
    </div>
</div>

<!--  Bread crumbs  -->
<div class="breadcrumbs">
    <ul>
        <li>
            <a href="<?= site_url('admin/dashboard') ?>">Dashboard</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="<?= site_url('patient') ?>">Patient Directory</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">New Patient</a>
        </li>
    </ul>
</div>


<div class="row-fluid">
    <div class="box">
        <div class="box-content-padless">
<?php include '_patient_form.php'; ?> 
        </div>
    </div>
</div>
