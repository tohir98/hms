<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once BASEPATH . 'helpers/form_helper.php';

if (!function_exists('form_bs_control')) {

    function form_bs_control($label, $control, $attributes = array(), $control_id = '') {
        $attributes_arr = array();
        foreach ($attributes as $key => $val) {
            $attributes_arr[] = "{$key} = '" . addslashes($val) . "'";
        }
        ?>
        <div class="control-group" <?= join(' ', $attributes_arr) ?>>

            <label <?php if ($control_id): ?>for="<?= $control_id ?>" <?php endif; ?>  class="control-label"><?= $label ?></label>
            <div class="controls">
                <?php
                if (is_callable($control)) {
                    $control();
                } else {
                    echo $control;
                }
                ?>               
            </div>
        </div>
        <?php
    }

}


if (!function_exists('form_checkbox_unchecked')) {

    function form_checkbox_unchecked($name, $value, $unchecked_value, $data = '', $checked = false) {

        return form_hidden($name, $unchecked_value)
                . PHP_EOL
                . form_checkbox(array_merge(['name' => $name], (array) $data), $value, $checked);
    }

}

if (!function_exists('form_field_to_label')) {

    function form_field_to_label($field, array $map = []) {

        if (array_key_exists($field, $map)) {
            return $map[$field];
        }

        $parts = explode('_', $field);
        foreach ($parts as $i => $prt) {
            $parts[$i] = ucfirst($prt);
        }

        return join(' ', $parts);
    }

}