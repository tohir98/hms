<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!function_exists('upload_validate')) {

    function upload_validate($fieldName, $config = [], array &$upload = []) {
        if (!isset($_FILES[$fieldName])) {
            return 'FIle upload is not present';
        }

        $upload = $_FILES[$fieldName];

        if ($upload['error']) {
            $errors = array(
                UPLOAD_ERR_INI_SIZE => 'File size is larger than system allowed',
                UPLOAD_ERR_FORM_SIZE => 'File size is larger than allowed by this application',
                UPLOAD_ERR_PARTIAL => 'File was not completely uploaded',
                UPLOAD_ERR_NO_FILE => 'No valid file uplaoded',
                UPLOAD_ERR_NO_TMP_DIR => 'Could not upload file to server',
                UPLOAD_ERR_CANT_WRITE => 'Could not save uploaded file on server',
                UPLOAD_ERR_EXTENSION => 'Internal server error prevented the upload.',
            );

            return $errors[$upload['error']];
        }

        if (isset($config['max_size']) && $upload['size'] > $config['max_size']) {
            return 'File size is larger than allowed';
        }

        $upload['extension'] = get_file_extension($upload['name']);
        if (isset($config['allowed_types']) && !in_array($upload['extension'], is_array($config['allowed_types']) ? $config['allowed_types'] : preg_split('/[,\s\|]+/', $config['allowed_types']))) {
            return 'File extension is not allowed';
        }

        return null;
    }

}

if (!function_exists('get_file_extension')) {

    function get_file_extension($name) {
        $tmp = explode('.', $name);
        return strtolower(array_pop($tmp));
    }

}
