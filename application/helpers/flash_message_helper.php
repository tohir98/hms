<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!function_exists('flash_message_display')) {

    function flash_message_display($ns = '') {
        $ci = get_instance();
        $msg = $ci->session->flashdata($ns . 'msg');
        if ($msg != NULL):
            ?>
            <div class="alert alert-<?= $ci->session->flashdata($ns . 'type') ?>">
            <?= $msg; ?>
            </div>
            <?php
        endif;
    }

}
if (!function_exists('flash_message_create')) {

    function flash_message_create($message = '', $type = 'info', $ns = '') {
        get_instance()->session->set_flashdata(array(
            $ns . 'msg' => $message,
            $ns . 'type' => $type
        ));
    }

}
