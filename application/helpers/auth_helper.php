<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!function_exists('auth_login_error')) {

    function auth_login_error($userdata) {
        if ($userdata['account_blocked'] == 1) {
            return 'For security reasons, this account has been locked after multiple failed login attempts. Please check the registered email for password reset instructions.';
        } elseif ($userdata['company_status'] != USER_STATUS_ACTIVE) {
            return 'Company account suspended. Please contact HR Administrator or Call TalentBase Support on <br />' . TALENTBASE_PHONE_NUMBER;
        } elseif ($userdata["status"] == USER_STATUS_INACTIVE) {
            return 'Account inactive.';
        } elseif ($userdata["status"] == USER_STATUS_SUSPENDED) {
            return 'Account suspended.';
        } elseif ($userdata["status"] == USER_STATUS_TERMINATED) {
            return 'Account terminated.';
        } elseif ($userdata["status"] == USER_STATUS_ACTIVE_NO_ACCESS || $userdata["status"] == USER_STATUS_PROBATION_NO_ACCESS) {
            return 'You have not been granted access to the employee portal. Please see your HR Manager.';
        }
    }

}

if (!function_exists('auth_next_url')) {

    function auth_next_url() {
        $ci = get_instance();
        /* @var $ci Auth */
        $ci->load->library(['input', 'session']);
        if ($ci->input->get('next_url')) {
            return $ci->input->get('next_url');
        } else if ($ci->input->post('next_url')) {
            return $ci->input->post('next_url');
        } else if (($next = $ci->session->userdata('next_url'))) {
            $ci->session->unset_userdata('next_url');
            return $next;
        } else {
            return '';
        }
    }

}

if (!function_exists('auth_save_next_url')) {

    function auth_save_next_url($next = '') {

        $ci = get_instance();
        /* @var $ci Auth */
        $ci->load->library(['input', 'session']);
        if (!$next) {
            $next = $ci->input->get('next_url') ? : $ci->input->post('next_url');
        }

        if ($next) {
            $ci->session->set_userdata(['next_url' => $next]);
        }
    }

}

if (!function_exists('auth_redirect_by_user_type')) {

    function auth_redirect_by_user_type($account_type, $redirect_url = '') {
        if ($redirect_url) {
            redirect($redirect_url);
        } else {
            if ($account_type == USER_TYPE_ADMIN) {
                // check if module_setup completed
                check_if_setup();
                redirect(site_url('admin/dashboard'));
            } elseif ($account_type == USER_TYPE_EMPLOYEE) {
                check_if_setup();
                redirect(site_url('employee/dashboard'));
            } else {
                trigger_error('Unknown Account type!', E_USER_ERROR);
            }
        }
    }

}

if (!function_exists('auth_log_user_action')) {

    function auth_log_user_action($id_user, $message, $type) {
        $ci = get_instance();
        $ci->load->model('user/user_model', 'u_model');
        $ci->u_model->log_db($id_user, $message, $type);
    }

}

if (!function_exists('check_if_setup')) {

    function check_if_setup() {
        if (!_check_setup()) {
            $CI = & get_instance();
            $CI->load->model('payroll/payroll_model');
            $CI->load->library('user_auth');

//            if (!$this->_is_admin($CI->session->userdata('id_user'))) {
//                redirect(site_url('user/access_denied'), 'refesh');
//            }
            if (!$CI->user_auth->is_admin()) {
                redirect(site_url('user/access_denied'), 'refesh');
            }

            //GET max step attatined
            $s = $CI->payroll_model->get_max_setup_step(1);

            if (!empty($s->step)) {
                redirect(site_url('setup/run_setup/step' . $s->step), 'refesh');
            }
            redirect(site_url('setup/run_setup/step1'), 'refesh');
        }
    }

}

if (!function_exists('_check_setup')) {

    function _check_setup() {
        
        $CI = & get_instance();
        $CI->load->model('payroll/payroll_model');

        $check_where = array(
            'id_company' => $CI->session->userdata('id_company'),
            'status' => MODULE_SETUP_ACTIVE,
            'id_module' => 1
        );

        $check = $CI->payroll_model->check('module_setup', $check_where);

        if ($check) {
            return true;
        } else {
            return false;
        }
    }

}

