<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!function_exists("process_attached_email_to_be_sent_to_recipient")) {
	
	function process_attached_email_to_be_sent_to_recipient(array $data) {
		
		$ci = & get_instance();
		$ci->load->library('pdf_lib_ext');
		
		if(!empty($data)) {
			
			$file_name = random_string('alnum', 12);
			
			$ci->pdf_lib_ext->build_pdf([
				'html' => $data['content'],
				'file_name' => $file_name
			]);

			send_email_to_recipient($file_name, $data);
		}
	}
}

if (!function_exists("send_email_to_recipient")) {
	
	function send_email_to_recipient($file_name, $data) {
		
		$full_name = $file_name . '.pdf';
		$file_path = FILE_PATH . $full_name;
		
		$ci = & get_instance();
		$ci->load->library("mailgun_api");
		
		$ci->mailgun_api->send_mail(array(
			'from' => $data['sender'],
			'to' => $data['recipient'],
			'subject' => $data['subject'],
			'html' => '<html>'. $data['message'] .'</html>',
			'attachment' => "@$file_path",
			'enctype' => 'multipart/form-data'
		));
	}
}

if(!function_exists("send_general_mail")) {
	
	function send_general_mail($from, $recipient, $message, $subject) {
		
		$ci = & get_instance();
		$ci->load->library("mailgun_api");
		
		$ci->mailgun_api->send_mail(array(
			'from' => $from,
			'to' => $recipient,
			'subject' => $subject,
			'html' => $message
		));
	}
}

if (!function_exists("get_image_icon")) {
	
	function get_image_icon($image_type) {
		
		$icon = strtolower($image_type);

		if ($icon == 'pdf') {
			return site_url('img/doc_icons/pdf_icon.png');
		} elseif ($icon == 'docx' || $icon == 'doc') {
			return site_url('img/doc_icons/word_icon.png');
		} elseif ($icon == 'ppt' || $icon == 'pptx') {
			return site_url('img/doc_icons/ppt_icon.png');
		} elseif ($icon == 'xls' || $icon == 'xlsx') {
			return site_url('img/doc_icons/excel_icon.png');
		}
	}
}

if (!function_exists("upload_file_to_storage")) {
	
	function upload_file_to_storage($path, $file_name) {
		
		$ci = & get_instance();
		$ci->load->library("storage");
		
		$str = $ci->storage->upload_file(array("file_path" => $path, "file_name" => $file_name));		
		return $str != '' ? true : false;
	}
}

if (!function_exists("upload_document_signature_images")) {
	
	function upload_document_signature_images($pics) {
		
	}
}
