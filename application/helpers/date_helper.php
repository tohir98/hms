<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once BASEPATH . 'helpers/date_helper.php';

if (!function_exists('date_format_strtotime')) {

    function date_format_strtotime($dateOrTime, $format = 'd-m-Y', $from_format = null) {
        $dateOrTime = trim($dateOrTime);
        
        if ($dateOrTime && strstr($dateOrTime, '0000-00-00') === false) {
            if (preg_match('/^\d+$/', $dateOrTime)) {
                $ts = DateTime::createFromFormat('U', $dateOrTime);
            } else if ($from_format) {
                $ts = DateTime::createFromFormat($from_format, $dateOrTime);
            } else {
                try {
                    $ts = new DateTime($dateOrTime);
                } catch (Exception $ex) {
                    $ts = false;
                }
            }
        }

        return $ts ? $ts->format($format) : null;
    }

}