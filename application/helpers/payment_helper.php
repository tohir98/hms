<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!function_exists('payment_get_back_url')) {

    function payment_get_back_url($payout) {
        $paymentType = $payout->payment_type;
        $ci = get_instance();
        $ci->load->model('payroll/payroll_model', 'payroll_model');
        //return get_class($ci->payroll_model);
        $payrollTypes = [Payroll_model::PAY_TYPE_TAKE_HOME,
            Payroll_model::PAY_TYPE_PAYE,
            Payroll_model::PAY_TYPE_PENSION,
            Payroll_model::PAY_TYPE_NHF];

        if (($idx = array_search($paymentType, $payrollTypes)) !== false) {
            $str = $ci->payroll_model->getPaymentTypes()[$idx];
            return site_url('payroll/disburse_funds/' . $payout->ref . '/' . rawurldecode($str));
            
            //add for other payment types later.
        } else {
            return 'javascript:window.history.back(-1);';
        }
    }

}

if(!function_exists('payment_receipt_pay_url')){
    function payment_receipt_pay_url($ref, $id){
        return site_url("/payment/receipt/{$id}/{$ref}");
    }
}