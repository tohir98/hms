<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!function_exists('bs2_modal')) {

    /**
     * 
     * @param array $params config with:
     *  - title : string
     *  - fade : boolean
     *  - id : string
     *  - content : string/callback
     *  - footer : string / callback
     *  - hidden: boolean
     *  - show_close_icon: boolean
     * 
     */
    function bs2_modal(array $params) {
        //$title = '', $id = 'bsModal', $content = null, $footer = null, $fade = true
        $params = array_merge(array(
            'fade' => true,
            'hidden' => true,
            'content' => null,
            'footer' => null,
            'id' => 'myModal',
            'title' => '',
            'show_close_icon' => true,
                ), $params);
        ?>
        <div class="modal <?= $params['fade'] ? 'fade in' : '' ?> <?= $params['hidden'] ? 'hide' : '' ?> " id="<?= $params['id'] ?>">
            <div class="modal-header">
                <?php if ($params['show_close_icon']) : ?>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <?php endif; ?>
                <h3 class="modal-title"><?= $params['title'] ?></h3>
            </div>
            <div class="modal-body">
                <?php
                if (is_callable($params['content'])) {
                    call_user_func($params['content']);
                } else {
                    echo $params['content'];
                }
                ?>
            </div>
            <?php if (!is_null($params['footer'])): ?>
                <div class="modal-footer">
                    <?php
                    if (is_callable($params['footer'])) {
                        call_user_func($params['footer']);
                    } else {
                        echo $params['footer'];
                    }
                    ?>
                </div>
            <?php endif; ?>
        </div>
        <?php
    }

}

if (!function_exists('bs2_init_pagination')) {

    function bs2_init_pagination() {
        $ci = get_instance();
        $ci->load->library('pagination');
        $pagination = $ci->pagination;
        /* @var $pagination CI_Pagination */
        $pagination->initialize(array(
            'use_page_numbers' => true,
            'page_query_string' => true,
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'full_tag_open' => '<div class="pagination pagination-centered"><ul>',
            'full_tag_close' => '</ul></div>',
            'cur_tag_open' => '<li class="active"><a href="#">',
            'cur_tag_close' => '</a></li>',
            'per_page' => PAGINATION_PER_PAGE,
        ));

        $pagination->first_tag_open = $pagination->prev_tag_open = $pagination->next_tag_open = $pagination->last_tag_open = '<li>';
        $pagination->first_tag_close = $pagination->prev_tag_close = $pagination->next_tag_close = $pagination->last_tag_close = '</li>';
        $pagination->last_link = ' &raquo;';
        $pagination->next_link = ' Next';
        $pagination->query_string_segment = 'page';

        $parts = explode('?', $_SERVER['REQUEST_URI']);
        if (count($parts) > 1) {
            $base = $parts[0];
            $c = $ci->input->get();
            if (isset($c['page'])) {
                unset($c['page']);
            }
            $query = http_build_query($c);
        } else {
            $query = '';
            $base = $parts[0];
        }
        $pagination->base_url = $base . '?' . $query;
    }

}