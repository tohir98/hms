<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
if (!function_exists('calculate_taxable'))
{
    function calculate_taxable($pay, $period)
    {
        $pay = $pay * $period;

        if ($pay <= 300000) {

        	$tax = $pay * 0.07;
        	return $tax;

        } 
        elseif($pay <= 600000) {

        	$pay = $pay - 300000;
        	$tax = 300000 * 0.07;
        	$tax2 = $pay * 0.11;

        	return $tax + $tax2;
        }
        elseif($pay <= 1100000) {
        	$pay = $pay - 300000;
        	$pay = $pay - 300000;
        	$tax = 300000 * 0.07;
        	$tax2 = 300000 * 0.11;
        	$tax3 = $pay * 0.15;

        	return $tax + $tax2 + $tax3;
        }
        elseif($pay <= 1600000) {

        	$pay = $pay - 300000;
        	$pay = $pay - 300000;
        	$pay = $pay - 500000;
        	$tax = 300000 * 0.07;
        	$tax2 = 300000 * 0.11;
        	$tax3 = 500000 * 0.15;
        	$tax4 = $pay * 0.19;

        	return $tax + $tax2 + $tax3 + $tax4;

        }
        elseif($pay <= 3200000) {

        	$pay = $pay - 300000;
        	$pay = $pay - 300000;
        	$pay = $pay - 500000;
        	$pay = $pay - 500000;
        	$tax = 300000 * 0.07;
        	$tax2 = 300000 * 0.11;
        	$tax3 = 500000 * 0.15;
        	$tax4 = 500000 * 0.19;
        	$tax5 = $pay * 0.21;

        	return $tax + $tax2 + $tax3 + $tax4 + $tax5;

        }

        elseif($pay > 3200000) {

        	$pay = $pay - 300000;
        	$pay = $pay - 300000;
        	$pay = $pay - 500000;
        	$pay = $pay - 500000;
        	$pay = $pay - 1600000;
        	$tax = 300000 * 0.07;
        	$tax2 = 300000 * 0.11;
        	$tax3 = 500000 * 0.15;
        	$tax4 = 500000 * 0.19;
        	$tax5 = 1600000 * 0.21;
        	$tax6 = $pay * 0.24;

        	return $tax + $tax2 + $tax3 + $tax4 + $tax5 + $tax6;

        }
        
    }
 
}



if (!function_exists('gross_income_relief')) {

    function gross_income_relief($amount) {
        return 0.2 * $amount;
    }

}

if (!function_exists('deduction_value')) {

    function deduction_value($rate, $amount) {

        return ($rate / 100) * $amount;
    }

}

if (!function_exists('taxable')) {

    function taxable($pay, $deductions, $period) {
        $cr = consolidated_relief($pay, $period);
        $gr = gross_income_relief($pay);

        $total_deduction = $deductions + $cr + $gr;
        $taxable = $pay - $total_deduction;

        return $taxable;
    }

}
if (!function_exists('non_taxable')) {

    function non_taxable($pay, $deductions, $period) {
        $cr = consolidated_relief($pay, $period);
        $gr = gross_income_relief($pay);

        $total_deduction = $deductions + $cr + $gr;

        return $total_deduction;
    }

}

if (!function_exists('calculate_tax')) {

    function calculate_tax($pay, $deductions, $period) {

        $taxable = taxable($pay, $deductions, $period);

        $tax_wf = calculate_taxable($taxable, $period);

        $tax_alt = 0.01 * $pay;

        if ($tax_wf > $tax_alt) {
            $tax = $tax_wf;
        } else {
            $tax = $tax_alt;
        }

        return $tax;
    }

}

if (!function_exists('calculate_paydate_period')) {

    function calculate_paydate_period($pay_date, $period_ending, $pay_schedule) {

        $current_month = date('F', strtotime('today'));
        $current_month_end = date('t', strtotime('today'));
        $pay_day = date('j', strtotime($pay_date));
        $pay_day_end = date('t', strtotime($pay_date));
        $period_end_day = date('j', strtotime($period_ending));


        if ($pay_schedule == 'monthly') {
            if ($pay_day == $current_month_end) {
                $period_start = date('F jS, Y', strtotime("1 $current_month"));
                $period_end = date('F jS, Y', strtotime("$current_month_end $current_month"));
                $pay_me = $period_end = date('F jS, Y', strtotime("$current_month_end $current_month"));
            } else {
                $period_start = date('F jS, Y', strtotime("- 1 month $period_end_day $current_month"));
                $period_end = date('F jS, Y', strtotime("$period_end_day $current_month"));
                $pay_me = $period_end = date('F jS, Y', strtotime("$pay_day $current_month"));
            }
        }

        if ($pay_schedule == 'biweekly') {
            $period_start = date('F jS, Y', strtotime("- 2 weeks " . $period_end_day . " " . $current_month));
            $period_end = date('F jS, Y', strtotime("$period_end_day $current_month"));
            $pay_me = $period_end = date('F jS, Y', strtotime("$pay_day $current_month"));
        }

        if ($pay_schedule == 'weekly') {
            $period_start = date('F jS, Y', strtotime("- 1 week " . $period_end_day . " " . $current_month));
            $period_end = date('F jS, Y', strtotime("$period_end_day $current_month"));
            $pay_me = $period_end = date('F jS, Y', strtotime("$pay_day $current_month"));
        }

        $array = array(
            'period_start' => $period_start,
            'period_end' => $period_end,
            'pay_date' => $pay_me
        );

        return $array;
    }

}

if (!function_exists('calculate_paydate_period_list')) {

    function calculate_paydate_period_list($pay_date, $period_ending, $pay_schedule) {

        $pay_date = date('F jS, Y', strtotime($pay_date));
        $pay_period_ending = date('F jS, Y', strtotime($period_ending));
        $pay_period_s = array();
        $pay_period_e = array();
        $next_pay_date = array();
        if ($pay_schedule == 'monthly') {

            $count = 1;
            $period_end = date('F jS, Y', strtotime($pay_period_ending . "- 1 month"));
            $last_date = date('t', strtotime($pay_date));
            $pay_day = date('j', strtotime($pay_date));
            $pay_period_day = date('j', strtotime($pay_period_ending));
            $month = date('F', strtotime($pay_date));
            $year = date('Y', strtotime($pay_date));

            if ($last_date == $pay_day) {
                $pay_period_s[] = date('F jS, Y', strtotime("1 $month"));
                $pay_period_e[] = date('F jS, Y', strtotime($period_end . " + 1 month"));
                $monthly = TRUE;
            } else {
                $pay_period_s[] = date('F jS, Y', strtotime($period_end . " + 1 day"));
                $pay_period_e[] = date('F jS, Y', strtotime($period_end . " + 1 month"));
                $monthly = FALSE;
            }
            $next_pay_date[] = $pay_date;


            for ($i = 0; $i < 11; $i++) {

                $month = date('F', strtotime($month . ' + 1 month'));
                $year = date('Y', strtotime($pay_date));
                $last_date = date('t', strtotime($month));

                $nxt_pay = date('F jS, Y', strtotime($pay_date . " + " . $count . " month "));
                $next_pay_day = $last_date = date('t', strtotime($nxt_pay));

                if ($monthly) {
                    if ($last_date == $next_pay_day) {
                        $pay_period_s[] = date('F jS, Y', strtotime("1-" . $month . "-" . $year));
                        $pay_period_e[] = date('F jS, Y', strtotime($last_date . "-" . $month . "-" . $year));
                        $next_pay_date[] = $nxt_pay;
                    } else {
                        $pay_period_s[] = date('F jS, Y', strtotime($pay_period_s . " + " . $count . " month"));
                        $pay_period_e[] = date('F jS, Y', strtotime($pay_period_e . " + " . $count . " month " . $year));
                        $next_pay_date[] = date('F jS, Y', strtotime($last_date . " " . $month . " " . $year));
                    }
                } else {
                    $pay_period_s[] = date('F jS, Y', strtotime($pay_period_s . " + " . $count . " month"));
                    $pay_period_e[] = date('F jS, Y', strtotime($pay_period_e . " + " . $count . " month " . $year));
                    $next_pay_date[] = date('F jS, Y', strtotime($pay_date . " + " . $count . " month " . $year));
                }


                $count++;
            }
        } elseif ($pay_schedule == 'weekly') {
            $count = 1;

            $period_start = date('F jS, Y', strtotime($pay_period_ending . "+ 1 days"));
            $period_end = date('F jS, Y', strtotime($pay_period_ending . "+ 7 days"));
            $next_pay_date2 = date('F jS, Y', strtotime($pay_date . "+ 7 days"));

            for ($i = 0; $i < 11; $i++) {

                $pay_period_s[] = date('F jS, Y', strtotime($period_start . " + " . $count . " week"));
                $pay_period_e[] = date('F jS, Y', strtotime($period_end . " + " . $count . " week"));
                $next_pay_date[] = date('F jS, Y', strtotime($next_pay_date2 . " + " . $count . " week"));

                $count++;
            }
        } elseif ($pay_schedule == 'biweekly') {
            $count = 2;

            $period_start = date('F jS, Y', strtotime($pay_period_ending . "+ 1 days"));
            $period_end = date('F jS, Y', strtotime($pay_period_ending . "+ 2 weeks"));
            $next_pay_date2 = date('F jS, Y', strtotime($pay_date . "+ 2 weeks"));

            for ($i = 0; $i < 11; $i++) {

                $pay_period_s[] = date('F jS, Y', strtotime($period_start . " + " . $count . " week"));
                $pay_period_e[] = date('F jS, Y', strtotime($period_end . " + " . $count . " week"));
                $next_pay_date[] = date('F jS, Y', strtotime($next_pay_date2 . " + " . $count . " week"));

                $count+=2;
            }
        }

        $pay_array[] = $pay_period_s;
        $pay_array[] = $pay_period_e;
        $pay_array[] = $next_pay_date;
        return $pay_array;
    }

}

if (!function_exists('get_pension')) {

    function get_pension($id_pay_structure, $percent, $period) {

        $CI = & get_instance();
        $CI->load->model('compensation/compensation_model');

        $result = $CI->compensation_model->get_pension_cal($id_pay_structure, $CI->session->userdata('id_company'));
        $q = ($percent / 100) * ($result->amount / $period);
        return $q;
    }

}

if (!function_exists('get_pension_employee')) {

    function get_pension_employee($id_pay_structure, $percent, $period) {

        $CI = & get_instance();
        $CI->load->model('compensation/compensation_model');
        $result = $CI->compensation_model->get_pension_cal($id_pay_structure);
        $q = ($percent / 100) * ($result->amount * $period);
        return $q;
    }

}

if (!function_exists('get_nhf')) {

    function get_nhf($id_pay_structure, $percent, $period) {

        $CI = & get_instance();
        $CI->load->model('compensation/compensation_model');
        $result = $CI->compensation_model->get_nhf_cal($id_pay_structure, $CI->session->userdata('id_company'));
        $q = ($percent / 100) * ($result->amount * $period);
        return $q;
    }

}

if (!function_exists('get_user_deduction')) {

    function get_user_deduction($id_user, $id_schedule) {
        $CI = & get_instance();
        $CI->load->model('payroll/payroll_model');
        $basic = $CI->payroll_model->get_my_basic_salary($id_user, $id_schedule, $CI->session->userdata('id_company'));
        $regular = $CI->payroll_model->get_my_regular_pay($id_user, $id_schedule, $CI->session->userdata('id_company'));
        $gross = $CI->payroll_model->get_my_gross_pay($id_user, $id_schedule, $CI->session->userdata('id_company'));

        $nhf_percent = $CI->payroll_model->get_my_nhf_percent($id_user, $id_schedule, $CI->session->userdata('id_company'));
        $pension_percent = $CI->payroll_model->get_my_pension_pay($id_user, $id_schedule, $CI->session->userdata('id_company'));
        $my_other_deduction = $CI->payroll_model->get_my_other_deduction($id_user, $id_schedule, $CI->session->userdata('id_company'));
        $loan_deduction = $CI->payroll_model->get_my_loan_deduction($id_user, $id_schedule, $CI->session->userdata('id_company'));

        $nhf = 0;
        $pension = 0;
        $other_deduction = 0;
        $total_deduction = 0;
        $other_deduction = 0;
        $loan_amount = 0;

        if (!empty($nhf_percent)) {
            $nhf = ($nhf_percent->percent / 100) * $basic->amount;
        }

        if (!empty($pension_percent)) {
            $pension = ($pension_percent->percent / 100) * $regular->amount;
        }

        if (!empty($my_other_deduction)) {
            $other_deduction = ($my_other_deduction->percent / 100) * $gross->amount;
        }

        if (!empty($loan_deduction)) {
            $loan_amount = $loan_deduction->amount;
        }

        $total_deduction = $gross->amount - ($nhf + $pension + $other_deduction + $loan_amount);

        return $nhf + $pension + $other_deduction + $loan_amount;
    }

}

if (!function_exists('update_module_setup_step')) {

    function update_module_setup_step($id_module, $step, $status) {
        $CI = & get_instance();
        $CI->load->model('payroll/payroll_model');
        $data = array(
            'id_company' => $CI->session->userdata('id_company'),
            'status' => $status,
            'step' => $step,
            'id_module' => $id_module,
            'created_by' => $CI->session->userdata('id_user'),
            'date_created' => date('Y-m-d h:i:s'),
        );

        return $CI->payroll_model->add('module_setup', $data);
    }

}

if (!function_exists('calculate_personal')) {

    function calculate_personal($gross, $period) {
        return consolidated_relief($gross, $period) + gross_income_relief($gross);
    }

}

if (!function_exists('personal_relief')) {

    function personal_relief($gross, $period) {
        return consolidated_relief($gross, $period) + gross_income_relief($gross);
    }

}

if (!function_exists('total_relief')) {

    function total_relief($gross, $deduction, $period) {
        return consolidated_relief($gross, $period) + gross_income_relief($gross) + $deduction;
    }

}

if (!function_exists('consolidated_relief')) {

    function consolidated_relief($pay, $period) {

        $relief = 0.01 * $pay;

        if ($relief > 200000) {
            return $relief / $period;
        } else {
            return (200000 / $period);
        }
    }

}


if (!function_exists('number_unformat')) {

    function number_unformat($figure = '') {
        return str_replace(array(',', '.00'), '', $figure);
    }

}

if (!function_exists('update_run_status')) {

    function update_run_status($id_run, $stage) {
        $CI = & get_instance();
        $CI->load->model('payroll/payroll_model');

        $where_array = array('id_run' => $id_run,
            'id_company' => $CI->session->userdata('id_company'));

        if ($CI->payroll_model->check('payroll_run_schedule', $where_array)) {
            $CI->payroll_model->update('payroll_run_schedule', ['status' => $stage], $where_array);
            return true;
        } else {
            return false;
        }
    }

}

if (!function_exists('update_run_status_otherpay')) {

    function update_run_status_otherpay($ref_id, $stage) {
        $CI = & get_instance();
        $CI->load->model('payroll/payroll_model');

        $where_array = array('ref_id' => $ref_id,
            'id_company' => $CI->session->userdata('id_company'));

        if ($CI->payroll_model->check('payroll_run_otherpay', $where_array)) {
            $CI->payroll_model->update('payroll_run_otherpay', ['status' => $stage], $where_array);
            return true;
        } else {
            return false;
        }
    }

}

if (!function_exists('update_incidental')) {

    function update_incidental($id_period, $status) {
        $CI = & get_instance();
        $CI->load->model('payroll/payroll_model');

        $qq = array('id_company' => $CI->session->userdata('id_company'), 'id_period' => $id_period);

        $check = $CI->payroll_model->check('payroll_incidental', $qq);
        if ($check) {
            $data_db = array('status' => $status);
            $CI->payroll_model->update('payroll_incidental', $data_db, $qq);
        }
    }

}

if (!function_exists('update_json')) {

    function update_json(array $params) {
        $CI = & get_instance();
        $CI->load->model('payroll/payroll_model');

        $CI->payroll_model->update('payroll_schedule_period', ['status' => $params['status']], 
            array('id_payroll_schedule_period' => $params['id_payroll_schedule_period']
            ));
    }

}

if (!function_exists('update_deduction_component')) {

    function update_deduction_component(array $incidental, array $params) {
        $CI = & get_instance();
        $CI->load->model('payroll/payroll_model');

        $record = $CI->payroll_model->get('payroll_compensation_employee_allowance', array(
            'id_run_ref' => $params['id_run_ref'],
            'id_company' => $CI->session->userdata('id_company'),
            'id_user' => $params['id_user'])
        );
        if (empty($record)) {
            return;
        }

        $components = json_encode(array_merge(json_decode($record[0]->deductions), (array) $incidental));

        $CI->payroll_model->update('payroll_compensation_employee_allowance', ['deductions' => $components], ['id_payroll_compensation_employee_allowance' => $record[0]->id_payroll_compensation_employee_allowance]);

        return;
    }

}

if (!function_exists('send_sms')) {

    function send_sms($message, $id_user, $work_num, $sender=null) {
        $CI = & get_instance();
        if ($work_num != '') {
            $str = str_replace(array('(', ')', ' ', '-'), '', $work_num);
            

            $params = array(
                'id_module' => 10,
                'message' => $message,
                'recipients' => array(
                    'id_user' => $id_user,
                    'number' => '+' . $str
                ),
                'sender' => $sender ? : $CI->session->userdata('company_name')
            );

            $CI->load->library('nexmo');
            $CI->nexmo->send_sms($params);
        }
    }

}

if (!function_exists('send_email_now')) {

    function send_email_now($sender, $email, $subject, $msg) {

        $CI = & get_instance();
        $CI->load->library('mailgun_api');

        $CI->mailgun_api->send_mail(
            array(
                'from' => $sender . ' <noreply@testbase.com.ng>',
                'to' => $email,
                'subject' => $subject,
                'html' => '<html>' . $msg . '</html>'
        ));
    }

}

if (!function_exists('insertDefaultAllowance')) {

    function insertDefaultAllowance(array $allowances) {
        $CI = & get_instance();
        $CI->load->model('payroll/payroll_model');

        foreach ($allowances as $all) {
            $db = ['allowance' => $all, 'id_company' => $CI->session->userdata('id_company')];
            if (!$CI->payroll_model->check('payroll_allowance_default', $db)) {
                $CI->payroll_model->add('payroll_allowance_default', $db);
            }
        }
    }

}

if (!function_exists('insertDefaultDeductions')) {

    function insertDefaultDeductions(array $deduction) {
        $CI = & get_instance();
        $CI->load->model('payroll/payroll_model');
        
        foreach ($deduction as $d) {
            if ($d == 'NHF') {
                $percentage = 2.5;
                $component = $CI->payroll_model->getBasicAllowanceID();
            }elseif ( $d == 'Pension'){
                $percentage = 8.0;
                $component = $CI->payroll_model->getBHTID();
            }
            
            $db = ['deduction' => $d, 'id_company' => $CI->session->userdata('id_company')];
            
            if (!$CI->payroll_model->check('payroll_deduction_settings', $db)) {
                $CI->payroll_model->add('payroll_deduction_settings', array(
                    'deduction' => $d,
                    'id_company' => $CI->session->userdata('id_company'),
                    'id_payroll_deduction_type' => 1,
                    'percentage' => $percentage,
                    'type' => COMPENSATION_VARIABLE_TYPE,
                    'component' => $component,
                    'amount' => 0,
                    'ref_id' => strtoupper(random_string('alnum', 10))
                ));
            }
        }
    }

}

if (!function_exists('insertDefaultContributions')) {

    function insertDefaultContributions(array $contributions) {
        $CI = & get_instance();
        $CI->load->model('payroll/payroll_model');
        
        $component = $CI->payroll_model->getBHTID();
        $percentage = 10;
        
        foreach ($contributions as $c) {
            $db = ['contribution' => $c, 'id_company' => $CI->session->userdata('id_company')];
            if (!$CI->payroll_model->check('payroll_contribution_settings', $db)) {
                $CI->payroll_model->add('payroll_contribution_settings', array(
                    'contribution' => $c,
                    'id_company' => $CI->session->userdata('id_company'),
                    'id_payroll_contribution_type' => 1,
                    'percentage' => $percentage,
                    'type' => COMPENSATION_VARIABLE_TYPE,
                    'component' => $component,
                    'amount' => 0,
                    'ref_id' => strtoupper(random_string('alnum', 10))
                ));
            }
        }
    }

}

if (!function_exists('getUserInfo')) {

    function getUserInfo($id_string, $id_company) {
        $CI = & get_instance();
        $CI->load->model('payroll/payroll_model');

        if ($id_string === '' or $id_company === '') {
            return FALSE;
        }

        $user = $CI->payroll_model->get('users', array(
            'id_string' => $id_string,
            'id_company' => $id_company
        ));

        if (empty($user)) {
            return FALSE;
        }

        return $user[0];
    }

}

if (!function_exists('getWorkingDays')) {

    function getWorkingDays($startDate, $endDate) {
        $CI = & get_instance();
        $CI->load->model('payroll/payroll_model');

        $from = $startDate;
        $to = $endDate;

        $diff = ((strtotime($to) - strtotime($from)) / (60 * 60 * 24)) + 1;
        $working_days = 0;

        for ($j = 1; $j <= $diff; $j++) {
            if (date('D', strtotime($from)) == 'Sun' or date('D', strtotime($from)) == 'Sat' && $CI->payroll_model->is_public($from, $CI->session->userdata('id_company'))) {
                
            } else {
                $working_days++;
            }
            $from = date('Y-m-d', (strtotime($from) + 86400));
        }

        return $working_days;
    }

}


if (!function_exists('isTaxableDeduction')) {

    function isTaxableDeduction($deduction) {
        $CI = & get_instance();
        $CI->load->model('payroll/payroll_model');

        $chk = $CI->payroll_model->check('payroll_deduction_settings', [
            'deduction' => $deduction,
            'id_company' => $CI->session->userdata('id_company'),
            'taxable' => TAXABLE
        ]);

        if ($chk) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

if (!function_exists('isTaxablePay')) {

    function isTaxablePay($otherpay) {
        $CI = & get_instance();
        $CI->load->model('payroll/payroll_model');

        $chk = $CI->payroll_model->check('payroll_otherpay_settings', [
            'otherpay' => $otherpay,
            'id_company' => $CI->session->userdata('id_company'),
            'taxable' => TAXABLE
        ]);

        if ($chk) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}


if (!function_exists('insert_next_schedule')) {

    function insert_next_schedule($id_schedule, $status) {
        $CI = & get_instance();
        $CI->load->model('payroll/payroll_model');
        
        $CI->payroll_model->add('payroll_schedule_period', array(
                'start_date' => date('d-M-Y', strtotime('+1 month', strtotime($d->from))),
                'end_date' => date('t-M-Y', strtotime('+1 month', strtotime($d->from))),
                'pay_date' => date('d-M-Y', strtotime('+1 month', strtotime($d->pay_date))),
                'status' => $status
            ));
    }

}


if (!function_exists('mixPanelTrack')){
    function mixPanelTrack($moduleID, $moduleTitle, $pageTitle, $pageID){
        
        $CI = & get_instance();
        $CI->load->library('mixpanel');
        
        $CI->mixpanel->track('page load', array(
            "distinct_id" => $CI->session->userdata('id_string'),
            "module id" => $moduleID,
            "module title" => $moduleTitle,
            "page title" => $pageTitle,
            "page id" => $pageID
        ));
    }
}
