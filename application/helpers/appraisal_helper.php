<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!function_exists("get_next_appraisal_cycle")) {
	
	function get_next_appraisal_cycle ($type, $prev_date) {
		
		if ($type == "" || $prev_date == "") {
			return false;
		} else {
			if ($type == 1) {
				return get_week(date("d-m-Y", strtotime($prev_date)), "+1 day", "+6 day");
			} elseif ($type == 2) {
				return get_date(date("d-m-Y", strtotime($prev_date)), "+1 month", null);
			} elseif ($type == 3) {
				return get_date(date("d-m-Y", strtotime($prev_date)), "+1 month", "+2 month");
			} elseif ($type == 4) {
				return get_date(date("d-m-Y", strtotime($prev_date)), "+1 month", "+5 month");
			} elseif ($type == 5) {
				return get_date(date("d-m-Y", strtotime($prev_date)), "+1 month", "+11 month");
			} else {
				return false;
			}
		}
	}
}

if (!function_exists("get_date")) {
	
	function get_date($date, $start, $incr = null) {
		
		if($date != "") {
			$start = date("Y-m-d", strtotime($start, strtotime($date)));
			
			if ($incr != null) {
				$end = date("Y-m-t", strtotime($incr, strtotime($start)));
			} else {
				$end = date("Y-m-t", strtotime($start, strtotime($date)));
			}
			return array ("start" => $start, "end" => $end); 
		}
	}
}

if (!function_exists("get_week")) {
	
	function get_week($date, $start, $incr) {

		if($date != "") {
			
			$start = date("Y-m-d", strtotime($start, strtotime($date)));
			
			return array(
				"start" => $start,
				"end" => date("Y-m-d", strtotime($incr, strtotime($start)))
			);
		}
	}
}

if (!function_exists("notify_via_email")) {
	
	function notify_via_email($sender = '', $recipient = '', $subject = '', $message = '') {
		$ci = & get_instance();
		$ci->load->library("mailgun_api");
		
		return $ci->mailgun_api->send_mail(array(
			'from' => $sender,
			'to' => $recipient,
			'subject' => $subject,
			'html' => '<html><body>'. $message .'</body></html>'
		));
	}
}

if (!function_exists("upload_completed_appraisal_forms_to_storage")) {
	
	function upload_completed_appraisal_forms_to_storage($file_name = null) {
		
		if ($file_name == null or $file_name == '') {
			return false;
		} else {
		
			$ci = & get_instance();
			$ci->load->library('storage');

			return $ci->storage->upload_file(array('file_path' => FILE_PATH, 'file_name' => $file_name)) ? true : false;
		}
	}
}

if (!function_exists("create_appraisal_form_pdf")) {
	
	function create_appraisal_form_pdf($content, $company_name) {
		
		$file_name = random_string('alnum', 12);
		
		$ci = & get_instance();
		$ci->load->library('pdf_lib_ext');
		
		$ci->pdf_lib_ext->build_pdf([
			'header' => $company_name,
			'footer' => date('Y') .' | {PAGENO} | '. $company_name,
			'html' => $content,
			'file_name' => $file_name
		]);
		
		if(FILE_PATH . $file_name .'.pdf') {
			return $file_name;
		} else {
			return false;
		}
	}
}