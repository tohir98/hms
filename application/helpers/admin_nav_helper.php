<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Admin Navigation helper
 */

if ( ! function_exists('build_top_nav_part'))
{
	function build_top_nav_part($nav_arr, $main_link = NULL)
	{
		// $CI =& get_instance();
		
		if(empty($nav_arr)) {
			return '';
		}
		
		$buffer = '<li>
				       <a href="'.site_url($main_link.'/dashboard').'">Dashboard</a>
				   </li>';
		
		if(!is_array($nav_arr) or empty($nav_arr)) {
			return $buffer;
		}
		
		foreach($nav_arr as $label => $menu) {
		
			$buffer .= '<li>
					<a href="#" data-toggle="dropdown" class="dropdown-toggle">
						<span>'.$label.'</span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">';
			
			foreach($menu as $id => $item) {
			
				if($item['in_menu'] == 1) {
					$buffer .= '<li><a href="'.site_url('tbadmin/'.$item['menu_id_string'].'/'.$item['perm_id_string']).'">'.$item['perm_subject'].'</a></li>';
				}	
				
				
			}
			
			$buffer .= '</ul>';
			
		} // End modules
		
		$buffer .= '</ul>';
		$buffer .= '</li>';
			
		return $buffer;
	}
}

if ( ! function_exists('build_sidebar_nav_part'))
{
	function build_sidebar_nav_part($nav_arr)
	{
		
		if(!is_array($nav_arr) or empty($nav_arr)) {
			return false;
		}
		
		// Load CI object
		$CI = get_instance();
		
		// Load libraries
        $CI->load->library('uri');
		
		$segment1 = $CI->uri->segment(1);
		$segment2 = $CI->uri->segment(2);
		
		if($segment2 == 'dashboard') {
			return build_sidebar_nav_part_all($nav_arr);
		} else {
			return build_sidebar_nav_part_module($nav_arr, $segment2);
		}
		
	}
}

if ( ! function_exists('build_sidebar_nav_part_module')) 
{
	function build_sidebar_nav_part_module($nav_arr, $menu_id_string) {
		
		$buffer = '';
		$base_buffer = '<div class="subnav">
				<div class="subnav-title">
					<a href="#" class="toggle-subnav"><i class="icon-angle-down"></i><span>{label}</span></a>
				</div>
				<ul class="subnav-menu">';
		
		foreach($nav_arr as $label => $menu) {
			/*
			$buffer .= '<div class="subnav">
				<div class="subnav-title">
					<a href="#" class="toggle-subnav"><i class="icon-angle-down"></i><span>'.$label.'</span></a>
				</div>
				<ul class="subnav-menu">';
			*/
			
			foreach($menu as $id => $item) {
				
				if($buffer == '' and $item['menu_id_string'] == $menu_id_string) {
					$buffer .= $base_buffer;
					$buffer = str_replace('{label}', $label, $buffer);
				}
				
				if($item['in_menu'] == 1 and $item['menu_id_string'] == $menu_id_string) {
					$buffer .= '<li>
								<a href="'.site_url('tbadmin/'.$item['menu_id_string'].'/'.$item['perm_id_string']).'">'.$item['perm_subject'].'</a>
								</li>';
				}	
			}
			
			}
			$buffer .= '</ul></div>';	
		
		return $buffer;
		
	} // End func build_sidebar_nav_part_all
}

if ( ! function_exists('build_sidebar_nav_part_all')) 
{
	function build_sidebar_nav_part_all($nav_arr) {
		return '';
		$buffer = '';
		
		foreach($nav_arr as $label => $menu) {
			
			$buffer .= '<div class="subnav">
				<div class="subnav-title">
					<a href="#" class="toggle-subnav"><i class="icon-angle-down"></i><span>'.$label.'</span></a>
				</div>
				<ul class="subnav-menu">';
			
			foreach($menu as $id => $item) {
			
				if($item['in_menu'] == 1) {
					$buffer .= '<li>
								<a href="'.site_url('tbadmin/'.$item['menu_id_string'].'/'.$item['perm_id_string']).'">'.$item['perm_subject'].'</a>
							</li>';
				}	
			}
					
			$buffer .= '</ul></div>';		
	}
			
	return $buffer;
		
	} // End func build_sidebar_nav_part_all
}