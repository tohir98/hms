<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Talentbase
 * Employee controller
 * 
 * @category   Controller
 * @package    Users
 * @subpackage Employee
 * @author     Tohir O. <ocleantech@gmail.com>
 * @copyright  Copyright (c) 2015 CleanSoft Lab.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 * 
 * @property common_model $c_model common model
 * @property patient_model $p_model Description
 * @property user_auth $user_auth user auth
 */
class Patient extends CI_Controller {

    public function __construct() {
        parent::__construct();

        /*
         * Loaded automatically in app.
         * 
         * lib - user_auth, session
         * helper - url
         */
        $this->load->library(array(
            'user_nav',
            'cdn_api'
        ));

        $this->load->helper(['user_nav', 'notification_helper']);
        $this->load->model('hms/common_model', 'c_model');
        $this->load->model('hms/patient_model', 'p_model');
    }

    public function index() {
        $this->user_auth->check_login();
        $data = array(
            'patients' => $this->p_model->fetchPatients()
        );

        $buffer = $this->load->view('hms/patient/patient_directory', $data, true);
        $this->user_nav->run_page($buffer, 'Patient Directory | HMS', false);
    }

    public function addPatient() {
        $this->user_auth->check_login();
        if (request_is_post()){
            $res = $this->p_model->addPatient(request_post_data());
            
            if($res){
                $this->session->set_userdata('confirm', 'Patient Added successfully');
            }else{
                $this->session->set_userdata('errors', 'Eror: Unable to save patient information');
            }
            redirect(site_url('patient'));
        }

        $data = array(
            'bloodGroups' => $this->c_model->getBloodGroups(),
            'countries' => $this->c_model->get('countries', [])
        );

        $buffer = $this->load->view('hms/patient/addPatient', $data, true);
        $this->user_nav->run_page($buffer, 'New patient', false);
    }

    public function states_select() {

        $id_country = $this->uri->segment(3);
        $single = $this->uri->segment(4);
        $el_name = $this->uri->segment(5);
        $id_state = $this->uri->segment(6);
        $isJson = false;

        if ($el_name == '') {
            $el_name = 'id_state';
        }

        $tmp = explode('.', $id_country);
        if (count($tmp) > 1) {
            $id_country = (int) $tmp[0];
            $isJson = $tmp[1] === 'json';
        }

        if (!$isJson) {
            if ($single == 'single') {
                $buffer = '<select name="' . $el_name . '" id="' . $el_name . '" data-rule-required="true">';
            } else {
                $buffer = '<div class="input-xlarge">
				<select name="' . $el_name . '[]" id="' . $el_name . '" multiple="multiple" class="chosen-select input-xxlarge" data-rule-required="true">';
            }

            if ($single == 'single') {
                $end_buffer = '</select>';
            } else {
                $end_buffer = '</select></div>';
            }
        }

        if (!is_numeric($id_country)) {
            if ($isJson) {
                request_bad();
            }
            echo 'Not applicable';
            return false;
        }

        $states = $this->c_model->fetch_states_by_id($id_country);

        if ($isJson) {
            $this->output->set_content_type('application/json');
            $data = array();
            if (!$states) {
                $data[] = ['id' => '-1', 'name' => "[States not found]"];
            } else {
                foreach ($states as $state) {
                    $data[] = ['id' => $state->id_state, 'name' => $state->state];
                }
            }
            $this->output->_display(json_encode($data));
            return;
        }

        if (!$states) {
            echo $buffer;
            echo '<option value="0">[States not found]</option>';
            echo $end_buffer;
            return false;
        }

        foreach ($states as $s) {

            if ($id_state == $s->id_state) {
                $sel = 'selected="selected"';
            } else {
                $sel = '';
            }

            $buffer .= '<option value="' . $s->id_state . '" ' . $sel . '>' . $s->state . '</option>';
        }

        if ($single == 'single') {
            $buffer .= $end_buffer;
        } else {
            $buffer .= $end_buffer;
        }

        echo $buffer;
    }
    
    

}
