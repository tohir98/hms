<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Talentbase
 * Employee controller
 * 
 * @category   Controller
 * @package    Users
 * @subpackage Employee
 * @author     Tohir O. <ocleantech@gmail.com>
 * @copyright  Copyright (c) 2015 CleanSoft Lab.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 * @property-read Employee_model $e_model employeemodel
 * 
 * @property User_model $u_model Description
 * @property Employee_info_request $employee_info_request Description
 * @property Employee_model $e_model employee model
 * @property Organizations_model $o_model orgnization
 * @property Checklist_Model $checklist_model 
 */
class Employee extends CI_Controller {

   

    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();

        /*
         * Loaded automatically in app.
         * 
         * lib - user_auth, session
         * helper - url
         */
        $this->load->library(array(
            'user_nav',
            'sms_lib',
            'bugsnag',
            'cdn_api',
            'employee_info_request',
        ));

        $this->load->helper('user_nav', 'notification_helper');
    }

// End func __construct

    public function cli_cleanup_employee_temp_perms() {

        if (PHP_SAPI != 'cli') {
            show_404('page', false);
        }

        echo "\nStarting Cleanup\n";

        $date = date('Y-m-d');

        $this->load->model('user/user_model', 'u_model');

        $count = $this->u_model->cleanup_temp_perms($date);

        if (!$count) {
            echo "There are no permissions to delete.\n";
        } else {
            echo $count . " permissions deleted.\n";
        }

        echo "Done.\n";
    }

// End func cli_cleanup_employee_record_requests

    public function request_emergency_contacts($id_string, $name_surname) {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(EDIT_REQUEST_EMPLOYEE_RECORDS);

        if ($name_surname == '') {
            show_404('page', false);
        }

        if ($this->input->server('REQUEST_METHOD') != 'POST') {
            show_404('page', false);
        }

        $data = $this->input->post();

        $id_company = $this->user_auth->get('id_company');
        $id_string = $this->uri->segment(3);

        if (!isset($data['emergency_request_description'])) {
            show_404('page', false);
        }

        if ($data['emergency_request_description'] == '') {
            show_404('page', false);
        }

        $due_date = date('Y-m-d', strtotime($data['emergency_due_date']));

        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('tasks/tasks_model', 't_model');

        $record = $this->u_model->fetch_account(array('id_string' => $id_string, 'id_company' => $id_company));

        if (!$record) {
            show_404('page', false);
        }

        $record = $record[0];

        $link = anchor('personal/my_profile', 'Emergency contact request from ' . $this->user_auth->get('display_name') . ' ' .
            $this->user_auth->get('last_name'));

        // Add temp perm

        $id_company = $this->user_auth->get('id_company');

        $perms_temp = array(
            'id_user' => $record->id_user,
            'id_creator' => $this->user_auth->get('id_user'),
            'date_expire' => $due_date,
            'id_module' => 1,
            'date_created' => date('Y-m-d'),
            'id_company' => $id_company,
            'id_perm' => EDIT_MY_EMERGENCY_CONTACTS
        );

        $y = $this->u_model->replace_perm_temp(EDIT_MY_EMERGENCY_CONTACTS, $perms_temp);

        $tasks = array(
            0 => array(
                'id_user_from' => $this->user_auth->get('id_user'),
                'id_user_to' => $record->id_user,
                'id_module' => 1,
                //'subject' => $data['emergency_request_description'],
                'subject' => $link,
                'status' => 0,
                'date_will_start' => date('Y-m-d'),
                'date_will_end' => $due_date,
                'tb_name' => 'user_perms_temp',
                'tb_column_name' => 'id_perms_temp',
                'tb_id_record' => $y[0],
                'notes' => $data['emergency_request_description']
            )
        );

        $this->t_model->add_task($tasks);

        // Send request notification via email to employee -- Chuks
        $this->load->library('mailgun_api');

        $i = array(
            'first_name' => $record->first_name,
            'company_name' => $this->user_auth->get('company_name'),
            'company_string' => $this->user_auth->get('company_id_string')
        );

        $msg = $this->load->view('email_templates/request_edit_emergency_contacts', $i, true);

        $this->mailgun_api->send_mail(
            array(
                'from' => $this->user_auth->get('company_name') . ' <noreply@talentbase.ng>',
                'to' => $record->email,
                'subject' => 'Request From ' . $this->user_auth->get('company_name'),
                'html' => '<html>' . $msg . '</html>'
        ));

        // End

        echo 'Redirecting ... ';

        redirect(
            site_url('employee/view_records/' . $id_string . '/' . $name_surname . '#emergency_container')
        );
    }

// End func request_emergency_contacts

    public function edit_profile_emergency_contacts($id_string, $name_surname) {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(EDIT_REQUEST_EMPLOYEE_RECORDS);

        if ($name_surname == '') {
            show_404('page', false);
        }

        $id_company = $this->user_auth->get('id_company');
        $id_string = $this->uri->segment(3);

        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('reference_model', 'ref');

        $record = $this->u_model->fetch_account(array('id_string' => $id_string, 'id_company' => $id_company));

        if (!$record) {
            show_404('page', false);
        }

        $record = $record[0];

        $success_message = '';
        $error_messages = '';

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $user = array(
                'emergency_first_name' => $this->input->post('emergency_first_name'),
                'emergency_last_name' => $this->input->post('emergency_last_name'),
                'emergency_address' => $this->input->post('emergency_address'),
                'emergency_email' => $this->input->post('emergency_email'),
                'emergency_phone_number' => $this->input->post('emergency_phone_number'),
                'emergency_employer' => $this->input->post('emergency_employer'),
                'emergency_work_address' => $this->input->post('emergency_work_address'),
                'emergency_first_name2' => $this->input->post('emergency_first_name2'),
                'emergency_last_name2' => $this->input->post('emergency_last_name2'),
                'emergency_address2' => $this->input->post('emergency_address2'),
                'emergency_email2' => $this->input->post('emergency_email2'),
                'emergency_phone_number2' => $this->input->post('emergency_phone_number2'),
                'emergency_employer2' => $this->input->post('emergency_employer2'),
                'emergency_work_address2' => $this->input->post('emergency_work_address2')
            );

            $this->u_model->save($user, array('id_user' => $record->id_user, 'id_company' => $id_company));

            redirect(site_url('employee/view_records/' . $id_string . '/' . $name_surname));
        } else {

            $profile = array(
                'emergency_first_name' => $record->emergency_first_name,
                'emergency_last_name' => $record->emergency_last_name,
                'emergency_address' => $record->emergency_address,
                'emergency_email' => $record->emergency_email,
                'emergency_phone_number' => $record->emergency_phone_number,
                'emergency_employer' => $record->emergency_employer,
                'emergency_work_address' => $record->emergency_work_address,
                'emergency_first_name2' => $record->emergency_first_name2,
                'emergency_last_name2' => $record->emergency_last_name2,
                'emergency_address2' => $record->emergency_address2,
                'emergency_email2' => $record->emergency_email2,
                'emergency_phone_number2' => $record->emergency_phone_number2,
                'emergency_employer2' => $record->emergency_employer2,
                'emergency_work_address2' => $record->emergency_work_address2,
            );
        } // End POST



        $data = array(
            'success_message' => $success_message,
            'error_messages' => $error_messages,
            'profile' => $profile
        );

        $this->load->view('user/employee/edit_emergency_contacts', $data);
    }

// End func edit_profile_emergency_contacts

    public function request_benefits($id_string) {
        $this->user_auth->check_login();
        $this->user_auth->check_perm(EDIT_REQUEST_EMPLOYEE_RECORDS);

        if (!request_is_post()) {
            show_404('page', false);
        }

        $data = $this->input->post();
        $due_date = $data['benefits_due_date'];
        $notes = array(
            'pension_details' => $data['benefits_request_description'],
        );

        $user = null;

        if ($this->employee_info_request->sendRequest($id_string, $due_date, ['pension_details'], $user, $notes)) {
            flash_message_create('Your request for benefit details from employee was sent successfully', 'success');
        } else {
            flash_message_create('We could not send your request at the moment.', 'error');
        }

        $tag = preg_replace('/[^A-Za-z0-9\-]+/', '-', strtolower($user->first_name . '-' . $user->last_name));
        $nextUrl = $this->input->server('HTTP_REFERER') ? : site_url('employee/view_records/' . $data['id_string'] . '/' . $tag);

        redirect($nextUrl);
    }

// End func request_benefits

    public function request_profile_picture($id_string, $name_surname) {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(EDIT_REQUEST_EMPLOYEE_RECORDS);

        if ($name_surname == '') {
            show_404('page', false);
        }

        if ($this->input->server('REQUEST_METHOD') != 'POST') {
            show_404('page', false);
        }

        $data = $this->input->post();

        $id_company = $this->user_auth->get('id_company');
        $id_string = $this->uri->segment(3);

        if (!isset($data['profile_pic_request_description'])) {
            show_404('page', false);
        }

        if ($data['profile_pic_request_description'] == '') {
            show_404('page', false);
        }

        $due_date = date('Y-m-d', strtotime($data['profile_pic_due_date']));

        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('tasks/tasks_model', 't_model');

        $record = $this->u_model->fetch_account(array('id_string' => $id_string, 'id_company' => $id_company));

        if (!$record) {
            show_404('page', false);
        }

        $record = $record[0];

        // Add temp perm

        $id_company = $this->user_auth->get('id_company');

        $perms_temp = array(
            'id_user' => $record->id_user,
            'id_creator' => $this->user_auth->get('id_user'),
            'date_expire' => $due_date,
            'id_module' => 1,
            'date_created' => date('Y-m-d'),
            'id_company' => $id_company,
            'id_perm' => EDIT_MY_PROFILE_PICTURE
        );

        $y = $this->u_model->replace_perm_temp(EDIT_MY_PROFILE_PICTURE, $perms_temp);

        $link = anchor('personal/my_profile', 'Personal picture request from ' . $this->user_auth->get('display_name') . ' ' .
            $this->user_auth->get('last_name'));

        $tasks = array(
            0 => array(
                'id_user_from' => $this->user_auth->get('id_user'),
                'id_user_to' => $record->id_user,
                'id_module' => 1,
                //'subject' => 'Personal picture request',
                'subject' => $link,
                'status' => 0,
                'date_will_start' => date('Y-m-d'),
                'date_will_end' => $due_date,
                'tb_name' => 'user_perms_temp',
                'tb_column_name' => 'id_perms_temp',
                'tb_id_record' => $y[0],
                'notes' => $data['profile_pic_request_description']
            )
        );

        $this->t_model->add_task($tasks);

        echo 'Redirecting ... ';

        redirect(
            site_url('employee/view_records/' . $id_string . '/' . $name_surname)
        );
    }

// End func request_profile_picture

    public function requestBankDetails() {

        $this->load->model('user/user_model', 'u_model');

        $users = explode(',', $this->input->post('id_user_list'));

        if (empty($users)) {
            echo 'Error';
            return;
        }

        foreach ($users as $id_user) {
            $record = $this->u_model->fetch_account(array('id_user' => $id_user, 'id_company' => $this->user_auth->get('id_company')));

            $this->_processBankRequest(array(
                'id_string' => $record[0]->id_string,
                'description' => $this->input->post('bank_request_description'),
                'due_date' => $this->input->post('request_due_date')
            ));
        }

        echo count($users);
    }

    private function _processBankRequest(array $params) {

        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('tasks/tasks_model', 't_model');

        $record = $this->u_model->fetch_account(array('id_string' => $params['id_string'], 'id_company' => $this->user_auth->get('id_company')));

        if (!$record) {
//			show_404('page', false);
            return;
        }

        $record = $record[0];

        $link = anchor('personal/my_profile', 'Bank request from ' .
            $this->user_auth->get('display_name') . ' ' . $this->user_auth->get('last_name')); // Chuks
        // Add temp perm

        $perms_temp = array(
            'id_user' => $record->id_user,
            'id_creator' => $this->user_auth->get('id_user'),
            'date_expire' => $params['due_date'],
            'id_module' => 1,
            'date_created' => date('Y-m-d'),
            'id_company' => $this->user_auth->get('id_company'),
            'id_perm' => EDIT_MY_BANK_DETAILS
        );

        $y = $this->u_model->replace_perm_temp(EDIT_MY_BANK_DETAILS, $perms_temp);

        $tasks = array(
            0 => array(
                'id_user_from' => $this->user_auth->get('id_user'),
                'id_user_to' => $record->id_user,
                'id_module' => 1,
                'subject' => $link, // Chuks
                'status' => 0,
                'date_will_start' => date('Y-m-d'),
                'date_will_end' => $params['due_date'],
                'tb_name' => 'user_perms_temp',
                'tb_column_name' => 'id_perms_temp',
                'tb_id_record' => $y[0],
                'notes' => $params['description']
            )
        );

        $this->t_model->add_task($tasks);

        // Send request notification via email to employee -- Chuks
        $this->load->library('mailgun_api');

        $i = array(
            'first_name' => $record->first_name,
            'company_name' => $this->user_auth->get('company_name'),
            'company_string' => $this->user_auth->get('company_id_string')
        );

        $msg = $this->load->view('email_templates/request_edit_bank_details', $i, true);

        $this->mailgun_api->send_mail(
            array(
                'from' => $this->user_auth->get('company_name') . ' <noreply@talentbase.ng>',
                'to' => $record->email,
                'subject' => 'Request From ' . $this->user_auth->get('company_name'),
                'html' => '<html>' . $msg . '</html>'
        ));
    }

    public function request_bank_details($id_string, $name_surname) {

        $this->user_auth->check_perm(EDIT_REQUEST_EMPLOYEE_RECORDS);

        if ($name_surname == '') {
            show_404('page', false);
        }

        if ($this->input->server('REQUEST_METHOD') != 'POST') {
            show_404('page', false);
        }

        $data = $this->input->post();

        if (!isset($data['bank_request_description']) || $data['bank_request_description'] == '') {
            show_404('page', false);
        }

        $this->_processBankRequest(array(
            'id_string' => $this->uri->segment(3),
            'description' => $data['bank_request_description'],
            'due_date' => date('Y-m-d', strtotime($data['bank_due_date']))
        ));

        redirect(
            site_url('employee/view_records/' . $id_string . '/' . $name_surname)
        );
    }

// End func request_bank_details

    public function request_credentials($id_string, $name_surname) {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(EDIT_REQUEST_EMPLOYEE_RECORDS);

        if (!request_is_post()) {
            show_404('page', false);
        }

        $data = $this->input->post();

        if ($this->employee_info_request->requestCredentials($id_string, $data['cred_request_file_title'], $data['cred_request_description'], $data['cred_request_due_date'])) {
            flash_message_create('Credential upload request has been sent.', 'success');
        } else {
            flash_message_create('Credentials could not be requested, please ensure you supplied all required information.', 'error');
        }

        redirect(site_url('employee/view_records/' . $id_string . '/' . $name_surname . '#resume_credentials_container'));
    }

// End func request_credentials

    /**
     * View employee profile record 
     * 
     * @access public
     * @param int $id_user
     * @return void
     */
    public function view_records($id_string, $name_surname) {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(VIEW_EMPLOYEE_RECORDS);

        $this->load->model('reference_model', 'ref');
        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');

        $this->load->library('cdn_api');

        // Create a connection
        $this->cdn_api->connect(
            array(
                'username' => RACKSPACE_USERNAME,
                'api_key' => RACKSPACE_API_KEY,
                'container' => $this->user_auth->get('cdn_container')
            )
        );

        if ($name_surname == '') {
            show_404('page', false);
        }

        $id_company = $this->user_auth->get('id_company');

        $success_message = '';
        $error_messages = '';

        $profile = $this->e_model->fetch_global(array('u.id_string' => $id_string, 'u.id_company' => $id_company));

        if (!$profile) {
            show_404('page', false);
        }

        $profile = $profile[0];

        $user = $this->u_model->fetch_account(array('id_user' => $profile->id_user, 'id_company' => $id_company));

        if (!$user) {
            show_404('page', false);
        }

        $user = $user[0];

        $requests = $this->u_model->fetch_perms_temp(array('id_company' => $id_company, 'id_user' => $user->id_user));

        // Profile tab
        // Work info
        $job_type = '';
        if ($profile->emp_job_types != '') {
            $job_types = $this->e_model->fetch_job_types($profile->id_user);
            if (!empty($job_types)) {
                foreach ($job_types as $j) {
                    $job_type .= $j->name . ', ';
                }
                $job_type = rtrim($job_type, ', ');
            }
        }

        $job_function = '';
        if ($profile->id_role != '') {
            $job_functions = $this->e_model->fetch_job_functions($profile->id_role, $this->user_auth->get('id_company'));
            if (!empty($job_functions)) {
                foreach ($job_functions as $jf) {
                    $job_function .= $jf->name . ', ';
                }
                $job_function = rtrim($job_function, ', ');
            }
        }

        if ($profile->employment_date != '' and $profile->employment_date != '0000-00-00 00:00:00') {
            $employment_date = date('m/d/Y', strtotime($profile->employment_date));
        } else {
            $employment_date = '';
        }


        $statuses = $this->user_auth->statuses();
        $emp_status = $statuses[$profile->status];

        if ($profile->status == USER_STATUS_PROBATION_ACCESS or $profile->status == USER_STATUS_PROBATION_NO_ACCESS) {
            $emp_status .= ' - ' . date('m/d/Y', strtotime($profile->date_probation_expire));
        }

        $url_role = '';

        // Build role description link
        if ($profile->job_description != '') {
            //$url_ = $this->cdn_api->temporary_url($profile->job_description);
            //$url_role = '<a href='.$url_.'>See Description</a>';
            $url_role = '<a href=' . site_url('employee/view_roles') . '>See Description</a>';
        }

        if (empty($profile->id_staff)) {
            $id_staff = 'N/A';
        } else {
            $id_staff = $profile->id_staff;
        }

        $primary_str = '';
        if ($this->e_model->is_primary_contact($profile->id_user, WORK_PHONE_TYPE)) {
            $primary_str = ' (Primary)';
        }

        $ri = array(
            'employee_name' => $profile->first_name . ' ' . $profile->middle_name . ' ' . $profile->last_name,
            'email' => $profile->email,
            'staff_id' => $profile->id_string,
            'id_staff' => $profile->id_staff,
            'access_level' => $profile->access_level_name,
            'employement_status' => $emp_status,
            'isParent' => $this->u_model->isParent($this->user_auth->get('id_company')),
            'isCompanyAdmin' => $this->u_model->isCompanyAdmin($profile->id_user)
        );
        $wi = array(
            'Company name' => $this->user_auth->get('company_name'),
            'Work Number' => $this->e_model->fetch_employee_phone_number($profile->id_user, WORK_PHONE_TYPE) . $primary_str,
            'Department' => $profile->departments,
            'Role' => $profile->roles . ' - ' . $url_role, // Add description link if exists
            'Job Type' => $job_type,
            'Company location' => $profile->location,
            'Manager' => $profile->manager_first_name . ' ' . $profile->manager_last_name,
            'Employment date' => date('d-M-Y', strtotime($employment_date)),
        );

        $primary_str = '';
        if ($this->e_model->is_primary_contact($profile->id_user, PERSONAL_PHONE_TYPE)) {
            $primary_str = ' (Primary)';
        }
        $pi = array(
            'Gender' => $this->genders[$profile->id_gender],
            'Marital Status' => $this->marital[$profile->id_marital_status],
            'Birth Date' => date_format_strtotime($profile->dob, 'd-M-Y') ? : 'N/A',
            'Country of Origin' => $profile->country_origin,
            'Personal Number' => $this->e_model->fetch_employee_phone_number($profile->id_user, PERSONAL_PHONE_TYPE) . $primary_str,
            'Address' => $profile->address,
            'City' => $profile->city ? : '',
            'State' => $profile->state,
            'Country' => $profile->country
        );

        // Resume / Credentials
        $rc = $this->e_model->fetch_credentials($profile->id_user);
        $rcfiles = $this->e_model->fetch_credential_files($profile->id_user);

        if ($profile->status == USER_STATUS_ACTIVE or $profile->status == USER_STATUS_PROBATION_ACCESS) {
            $send_request = true;
        } else {
            $send_request = false;
        }

        // Emergency contacts
        $e_contacts = $this->_compute_emergency_contact($user);

        // New bank Details : Tohir
        $bank_details = $this->_compute_bank_details($profile->id_user);

        $pfa_name = '';

        $pfa = $this->ref->fetch_all_records('pension_fund_administrators', array('id_pension_fund_administrator' => $user->id_pfs));

        if ($pfa != '') {
            $pfa_name = $pfa[0]->name;
        }

        $benefits = array(
            'RSA PIN' => $user->rsa_pin,
            'Pension Fund Administrator' => $pfa_name
        );

        // ---- Load content views ----

        $base_request_messge = 'A request was sent to Employee ' .
            $user->first_name . ' ' .
            $user->last_name . ' on {request_created} to submit this information by {request_expire}.';

        $r = $this->perm_from_perms($requests, EDIT_MY_PROFILE_PERSONAL_INFO);
        $request_message = '';

        $canEditRecord = $this->user_auth->have_perm(EDIT_REQUEST_EMPLOYEE_RECORDS);
        if (!empty($r) and $canEditRecord) {
            $request_message = $base_request_messge;
            $request_message = str_replace('{request_created}', date('d F, Y', strtotime($r->date_created)), $request_message);
            $request_message = str_replace('{request_expire}', date('d F, Y', strtotime($r->date_expire)), $request_message);
        }

        $profile_content = $this->load->view('user/employee/profile_page_contents/profile', array(
            'ri' => $ri,
            'wi' => $wi,
            'pi' => $pi,
            'send_request' => $send_request,
            'request_message' => $request_message,
            'request_fields' => $this->e_model->reporting_data($id_company, $id_string)
            ), true);

        $r = $this->perm_from_perms($requests, EDIT_MY_CREDENTIALS);
        $request_message = '';

        if (!empty($r) and $canEditRecord) {
            $request_message = $base_request_messge;
            $request_message = str_replace('{request_created}', date('d F, Y', strtotime($r->date_created)), $request_message);
            $request_message = str_replace('{request_expire}', date('d F, Y', strtotime($r->date_expire)), $request_message);
        }

        $resume_content = $this->load->view('user/employee/profile_page_contents/resume_credentials', array(
            'uploads' => $rc,
            'id_user_string' => $id_string,
            'send_request' => $send_request,
            'request_message' => $request_message,
            'files' => $rcfiles
            ), true);

        $r = $this->perm_from_perms($requests, EDIT_MY_EMERGENCY_CONTACTS);
        $request_message = '';

        if (!empty($r) and $canEditRecord) {
            $request_message = $base_request_messge;
            $request_message = str_replace('{request_created}', date('d F, Y', strtotime($r->date_created)), $request_message);
            $request_message = str_replace('{request_expire}', date('d F, Y', strtotime($r->date_expire)), $request_message);
        }

        $emergency_content = $this->load->view('user/employee/profile_page_contents/emergency_contacts', array(
            'e_contacts' => $e_contacts,
            'send_request' => $send_request,
            'request_message' => $request_message
            ), true);

        $request_message = '';
        $notes_data['notes'] = $this->e_model->fetch_notes($profile->id_company, $profile->id_user);
        $notes_data['note_id_user'] = $profile->id_user;
        $internal_notes_content = $this->load->view('user/employee/profile_page_contents/internal_notes', $notes_data, true);

        $r = $this->perm_from_perms($requests, EDIT_MY_BANK_DETAILS);
        $request_message = '';

        if (!empty($r) and $canEditRecord) {
            $request_message = $base_request_messge;
            $request_message = str_replace('{request_created}', date('d F, Y', strtotime($r->date_created)), $request_message);
            $request_message = str_replace('{request_expire}', date('d F, Y', strtotime($r->date_expire)), $request_message);
        }

        $bank_details_content = $this->load->view('user/employee/profile_page_contents/bank_details', array(
            'bank_details' => $bank_details,
            'send_request' => $send_request,
            'request_message' => $request_message
            ), true);



        $r = $this->perm_from_perms($requests, EDIT_MY_PENSION_DETAILS);
        $request_message = '';
        //$employee_queries_content = $this->get_employee_record_queries_page_content($profile->id_user);echo $id_string.' '.$name_surname; die();

        $request_message = '';

        if (!empty($r) and $canEditRecord) {
            $request_message = $base_request_messge;
            $request_message = str_replace('{request_created}', date('d F, Y', strtotime($r->date_created)), $request_message);
            $request_message = str_replace('{request_expire}', date('d F, Y', strtotime($r->date_expire)), $request_message);
        }

        $benefits_details_content = $this->load->view('user/employee/profile_page_contents/benefits_details', array(
            'benefits' => $benefits,
            'send_request' => $send_request,
            'request_message' => $request_message
            ), true);

        //$payroll_records_content = $this->load->view('user/employee/profile_page_contents/payroll_records', array(), true);
        //$appraisal_records_content = $this->get_appraisal_record_page_content($profile->id_user);

        $r = $this->perm_from_perms($requests, EDIT_MY_PROFILE_PICTURE);
        $request_message = '';

        if (!empty($r) and $canEditRecord) {
            $request_message = $base_request_messge;
            $request_message = str_replace('{request_created}', date('d F, Y', strtotime($r->date_created)), $request_message);
            $request_message = str_replace('{request_expire}', date('d F, Y', strtotime($r->date_expire)), $request_message);
        }

        $data = array(
            'success_message' => $success_message,
            'error_messages' => $error_messages,
            'profile' => $profile,
            // Containers
            'profile_content' => $profile_content,
            'resume_content' => $resume_content,
            'emergency_content' => $emergency_content,
            'internal_notes_content' => $internal_notes_content,
            'bank_details_content' => $bank_details_content,
            //'employee_queries_content' => $employee_queries_content,
            'benefits_details_content' => $benefits_details_content,
            //'vacations_records_content' => $this->get_vacation_record_page_content($profile->id_user),
            //'payroll_records_content' => $payroll_records_content,
            //'appraisal_records_content' => $appraisal_records_content,
            'send_request' => $send_request,
            'request_message' => $request_message,
            'employees' => $this->e_model->fetch_global(array('u.id_company' => $id_company)),
        );

        $buffer = $this->load->view('user/employee/profile_page', $data, true);

        $this->user_nav->run_page($buffer, 'TalentBase - Staff records / Profile', false);
    }

    private function _compute_emergency_contact($user) {
        return array(
            1 => array(
                'First name' => $user->emergency_first_name,
                'Last name' => $user->emergency_last_name,
                'Address' => $user->emergency_address,
                'Email' => $user->emergency_email,
                'Phone' => $user->emergency_phone_number,
                'Employer' => $user->emergency_employer,
                'Work address' => $user->emergency_work_address
            ),
            2 => array(
                'First name' => $user->emergency_first_name2,
                'Last name' => $user->emergency_last_name2,
                'Address' => $user->emergency_address2,
                'Email' => $user->emergency_email2,
                'Phone' => $user->emergency_phone_number2,
                'Employer' => $user->emergency_employer2,
                'Work address' => $user->emergency_work_address2
            )
        );
    }

    private function _compute_bank_details($id_user) {

        $pp = $this->e_model->fetch_bank_details($id_user);
        $kount = 0;
        $bank_details = array();

        if (empty($pp)) {
            return FALSE;
        }
        foreach ($pp as $p) {

            $bank = $this->ref->fetch_all_records('banks', array('id_bank' => $p->id_bank));
            $bank_name = '';

            if (!empty($bank)) {
                $bank = $bank[0];
                $bank_name = $bank->name;
            }

            $bank_account_type = '';

            if (isset($this->account_types[$p->id_bank_account_type])) {
                $bank_account_type = $this->account_types[$p->id_bank_account_type];
            }

            if ($p->bank_account_name != '' or $p->bank_account_name != 0) {
                $bank_account_name = $p->bank_account_name;
            } else {
                $bank_account_name = '';
            }

            if ($p->bank_account_number != '' and $p->bank_account_number != 0) {
                $bank_account_number = $p->bank_account_number;
            } else {
                $bank_account_number = '';
            }

            $defaultAccount = 'No';
            if ($p->default == 1) {
                $defaultAccount = 'Yes';
            }

            $bank_details[$kount] = array(
                'Account name' => $bank_account_name,
                'Bank' => $bank_name,
                'Account type' => $bank_account_type,
                'Account number' => $bank_account_number,
                'Default' => $defaultAccount,
                '' => '<a class=popup href=' . site_url() . 'employee/edit_bank_detail/' . $profile->id_string . '/' . $p->ref_id . ' > Edit </a>'
            );

            ++$kount;
        }

        return $bank_details;
    }

    private function perm_from_perms($requests, $id_perm) {

        if (!empty($requests)) {
            foreach ($requests as $req) {
                if ($req->id_perm == $id_perm) {
                    return $req;
                }
            }
        }

        return false;
    }

// End func perm_from_perms

    protected function get_vacation_record_page_content($id_user) {

        $this->load->model('company/company_model', 'c_model');
        $this->load->model('vacation/vacation_model', 'v_model');

        $this->load->helper('vacation_helper');

        $data['vacation_types'] = NULL;
        $data['entitlements'] = NULL;
        $data['taken'] = NULL;
        $data['history'] = NULL;
        $data['pending_approvals'] = NULL;
        $data['holidays'] = NULL;

        $id_company = $this->user_auth->get('id_company');
        $id_module = 2;

        $vacation_module_enabled = $this->c_model->module_enabled($id_company, $id_module);

        if (!$vacation_module_enabled) {
            $message = 'This module is currently unavailable on this account. Please contact clientdesk@talentbase.ng to add this module.';
            $vacations_records_content = $this->load->view('user/employee/profile_page_contents/vacations_records', array('message' => $message, 'data' => NULL), true);
            return $vacations_records_content;
        }

        $data['vacation_types'] = $this->v_model->get_employee_vacation_types($id_user);
        $data['entitlements'] = $this->v_model->get_employee_entitlements($id_user);
        $data['taken'] = $this->v_model->get_taken_vacations($id_user);
        $data['history'] = $this->v_model->get_employee_vacation_history($id_user);
        $data['pending_approvals'] = $this->v_model->get_employee_pending_vacation($id_user);
        $data['holidays'] = $this->v_model->get_holidays($id_company);

        //$data['pending'] = $this->v_model->get_employee_pending_requests($this->user_auth->get('id_user'));
        //$buffer = $this->load->view('vacation/my_vacation', $data, true);

        $data['message'] = '';

        $vacations_records_content = $this->load->view('user/employee/profile_page_contents/vacations_records', $data, true);

        return $vacations_records_content;
    }

// End func get_vacation_record_page_content

    protected function get_employee_record_queries_page_content($id_user) {

        $this->load->model('company/company_model', 'c_model');

        $id_company = $this->user_auth->get('id_company');
        $id_module = 12;

        $query_module_enabled = $this->c_model->module_enabled($id_company, $id_module);

        $data = array();

        if (!$query_module_enabled) {
            $message = 'This module is currently unavailable on this account. Please contact clientdesk@talentbase.ng to add this module.';
            $content = $this->load->view('user/employee/profile_page_contents/employee_queries', array('message' => $message, 'data' => NULL), true);
            return $content;
        }

        $this->load->model('employee_query/employee_query_model', 'query');

        $data = array(
            'queries' => $this->query->get_employee_query($id_user),
            'message' => ''
        );

        $content = $this->load->view('user/employee/profile_page_contents/employee_queries', $data, true);

        return $content;
    }

// End func get_employee_record_queries_page_content

    protected function get_appraisal_record_page_content($id_user) {

        $this->load->model('company/company_model', 'c_model');

        $id_company = $this->user_auth->get('id_company');
        $id_module = 3;

        $kpi_module_enabled = $this->c_model->module_enabled($id_company, $id_module);

        $data = array();

        if (!$kpi_module_enabled) {
            $message = 'This module is currently unavailable on this account. Please contact clientdesk@talentbase.ng to add this module.';
            $content = $this->load->view('user/employee/profile_page_contents/appraisal_records', array('message' => $message, 'data' => NULL), true);
            return $content;
        }

        $this->load->model('kpi/kpi_model', 'k_model');

        $data = array(
            'kpis' => $this->k_model->fetch_current_appraisal_info($id_user, $id_company),
            'staffs' => $this->k_model->fetch_all_employees_by_id_company($id_company),
            'appraisals' => $this->k_model->fetch_staff_appraisal_history(
                array(
                    'id_user' => $id_user,
                    'id_company' => $id_company
            ))
        );

        $data['message'] = '';

        $content = $this->load->view('user/employee/profile_page_contents/appraisal_records', $data, true);

        return $content;
    }

// End func get_appraisal_record_page_content

    /**
     * View Staff directory
     * 
     * @access public
     * @return void 
     */
    public function view_staff_directory() {

        $this->user_auth->check_perm(VIEW_STAFF_DIRECTORY);

        $this->load->model('reference_model', 'ref');
        $this->load->model('user/checklist_model', 'checklist_model');
        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');

        //$id_company = $this->user_auth->get('id_company');

        $current_statuses = array(
            'inactive' => USER_STATUS_INACTIVE, // Added by Tohir
            'active' => USER_STATUS_ACTIVE,
            'active_no_access' => USER_STATUS_ACTIVE_NO_ACCESS,
            'probation_access' => USER_STATUS_PROBATION_ACCESS,
            'probation_no_access' => USER_STATUS_PROBATION_NO_ACCESS,
            'suspended' => USER_STATUS_SUSPENDED, // Added by Tohir
            'terminated' => USER_STATUS_TERMINATED, // Added by Tohir
        );

        //$profiles = $this->e_model->fetch_global(array('id_company' => $id_company), $current_statuses);

        $actions = $this->user_auth->have_perm(VIEW_EMPLOYEE_RECORDS) //manager view
            || $this->user_auth->is_admin()  // is admin
            || $this->user_auth->have_perm(CHANGE_EMPLOYEE_STATUS) //can change employee status
            || $this->user_auth->have_perm(RESET_EMPLOYEE_PASSWORD); //can reset password

        $canAssignChecklist = !!$this->user_auth->have_perm('employee:checklist_assign');
        $checklists = $canAssignChecklist ? $this->checklist_model->getChecklists($this->user_auth->get('id_company'), ['enabled' => 1]) : [];
        $data = array(
            'statuses' => $this->user_auth->statuses_without_terminated(),
            'current_statuses' => $current_statuses,
            'perms' => array(
                'view_records' => !!$this->user_auth->have_perm(VIEW_EMPLOYEE_RECORDS),
                'change_status' => !!$this->user_auth->have_perm(CHANGE_EMPLOYEE_STATUS),
                'actions' => $actions,
                'reset_password' => !!$this->user_auth->have_perm(RESET_EMPLOYEE_PASSWORD),
                'assign_checklist' => $canAssignChecklist
            ),
            'checklists' => $checklists,
        );

        $this->user_nav->run_page_with_view('user/employee/staff_directory', $data, 'TalentBase - Staff records / Directory', false);
    }

// End func view_staff_directory

    public function reset_password() {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(RESET_EMPLOYEE_PASSWORD);

        $id_user = $this->input->post('id_user_strr');
        $full_name = $this->input->post('employee_full_namer');
        $email = $this->input->post('employee_emailr');
        $id_string = $this->input->post('employee_id_string');

        $this->load->model('user/employee_model', 'e_model');
        $this->e_model->activate_account($full_name, $id_string, $email);

        $this->session->set_userdata("confirm", $full_name . "'s password successfully re/set");
        redirect(site_url('employee/view_staff_directory'));
    }

// End func reset_password

    public function change_status() {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(CHANGE_EMPLOYEE_STATUS);

        $id_user = $this->input->post('id_user_st');
        $status = $this->input->post('status');
        $full_name = $this->input->post('employee_full_name');
        $email = $this->input->post('employee_email');
        $email_2 = $this->input->post('email');

        $termination_date = $this->input->post('termination_date');
        $probation_expire_date = $this->input->post('probation_expire_date');

        $send_email = $this->input->post('send_email');
        $no_email = $this->input->post('no_email');

        $this->load->model('user/user_model', 'u_model');

        // Check if email address already exists - Chuks
        if ($email_2 != $email) {
            $user_email_exists = $this->u_model->email_exists($email_2, $id_user, $this->user_auth->get('id_company'));
            // Redirect to staff directory
            if ($user_email_exists) {
                $this->session->set_userdata('errors', 'Error. You cannot assign an existing email to employee.');
                redirect(site_url('employee/view_staff_directory'));
                return;
            }
        }

        // Get user record for id_string
        $user_record = $this->u_model->fetch_account(array('id_user' => $id_user));
        $id_string = $user_record[0]->id_string;

        $data = array(
            'status' => $status,
            'account_blocked' => 0
        );

        if ($no_email == 1) {
            $data['email'] = strtolower($id_string) . '.user@talentbase.ng';
        } else {
            $data['email'] = $this->input->post('email');
        }

        if ($status == USER_STATUS_TERMINATED) {
            $data['date_termination'] = date('Y-m-d', strtotime($termination_date));
        }

        if ($status == USER_STATUS_PROBATION_ACCESS or $status == USER_STATUS_PROBATION_NO_ACCESS) {
            $data['date_probation_expire'] = date('Y-m-d', strtotime($probation_expire_date));
        }

        if (($status == USER_STATUS_ACTIVE or $status == USER_STATUS_PROBATION_ACCESS) and ! $no_email) {
            $send_email = true;

            /* REJECT DEFAULT EMAIL ADDRESS FOR ACTIVE STATUS - CHUKS */
            if ($data['email'] == strtolower($id_string) . '.user@talentbase.ng') {
                $this->session->set_userdata('errors', 'Error! Activation of account cannot exists with default email: ' . $email . '. '
                    . 'Please reset email address');
                redirect(site_url('employee/view_staff_directory'));
                return;
            }
            /* END */
        }

        $this->u_model->save(
            $data, array(
            'id_user' => $id_user,
            'email' => $email
            )
        );

        if (!$send_email) {
            redirect(site_url('employee/view_staff_directory'));
        }

        if ($email_2 != '') {

            // Company name
            $sender = $this->user_auth->get('company_name');

            $reset_password_url = site_url('user/reset_password') . '/' . $id_string . '/' . sha1($email_2 . $id_string);

            $i = $this->u_model->fetch_user_role_dept_details(
                array(
                    'id_user' => $this->user_auth->get('id_user'),
                    'id_company' => $this->user_auth->get('id_company')
            ));

            $bdata = array(
                'full_name' => $full_name,
                'reset_password_url' => $reset_password_url,
                'email' => $email_2,
                'company_name' => $this->user_auth->get('company_name'),
                //'company_url' => 'https://'.$this->user_auth->get('company_id_string').'.talentbase.ng/login',
                'company_url' => site_url('login'),
                'sender_full_name' => $this->user_auth->get('display_name') . ' ' . $this->user_auth->get('last_name'),
                'role' => $i->roles,
                'dept' => $i->departments
            );

            //$msg = $this->load->view('email_templates/user_account_status_changed', $bdata, true);
            $msg = $this->load->view('email_templates/user_reset_password_after_register', $bdata, true);

            // Load libr.
            $this->load->library('mailgun_api');
            $this->mailgun_api->send_mail(
                array(
                    'from' => $sender . ' <' . TALENTBASE_SUPPORT_EMAIL . '>',
                    'to' => $email_2,
                    'bcc' => 'helpdesk@talentbase.ng',
                    'subject' => 'Access Granted: ' . $this->user_auth->get('company_name') . ' Employee Portal',
                    'html' => '<html>' . $msg . '</html>'
                )
            );
        }

        redirect(site_url('employee/view_staff_directory'));
    }

// End func change_status

    public function edit_benefits($id_string, $name_surname) {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(EDIT_REQUEST_EMPLOYEE_RECORDS);

        if ($name_surname == '') {
            show_404('page', false);
        }

        $id_company = $this->user_auth->get('id_company');
        $id_string = $this->uri->segment(3);

        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('reference_model', 'ref');

        $record = $this->u_model->fetch_account(array('id_string' => $id_string, 'id_company' => $id_company));

        if (!$record) {
            show_404('page', false);
        }

        $record = $record[0];

        $employee = $this->e_model->fetch_profile(array('id_user' => $record->id_user, 'id_company' => $id_company));

        if (!$employee) {
            show_404('page', false);
        }

        $employee = $employee[0];

        $success_message = '';
        $error_messages = '';

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $profile = array(
                'rsa_pin' => $this->input->post('rsa_pin'),
                'id_pfs' => $this->input->post('pfs')
            );

            // UPDATE RECORDS

            $this->u_model->save($profile, array('id_user' => $record->id_user, 'id_company' => $id_company));

            redirect(site_url('employee/view_records/' . $id_string . '/' . $name_surname . '#benefits_details_container'));
        } // End POST

        $data = array(
            'success_message' => $success_message,
            'error_messages' => $error_messages,
            'profile' => $record,
            'pfas' => $this->ref->fetch_all_records('pension_fund_administrators'),
        );

        $this->load->view('user/employee/edit_benefits', $data);
    }

// End func edit_benefits

    public function edit_profile_picture($id_string, $name_surname) {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(EDIT_REQUEST_EMPLOYEE_RECORDS);

        if ($name_surname == '') {
            show_404('page', false);
        }

        $id_company = $this->user_auth->get('id_company');
        $id_string = $this->uri->segment(3);

        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('reference_model', 'ref');

        $record = $this->u_model->fetch_account(array('id_string' => $id_string, 'id_company' => $id_company));

        if (!$record) {
            show_404('page', false);
        }

        $record = $record[0];

        $employee = $this->e_model->fetch_profile(array('id_user' => $record->id_user, 'id_company' => $id_company));

        if (!$employee) {
            show_404('page', false);
        }

        $employee = $employee[0];

        $success_message = '';
        $error_messages = '';

        // Create a connection
        $this->cdn_api->connect(
            array(
                'username' => RACKSPACE_USERNAME,
                'api_key' => RACKSPACE_API_KEY,
                'container' => $this->user_auth->get('cdn_container')
            )
        );

        if ($record->profile_picture_name != '') {
            $profile_picture_src = $this->cdn_api->temporary_url($record->profile_picture_name);
        } else {
            $profile_picture_src = '';
        }

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $user['profile_picture_name'] = '';
            $user['profile_picture_uri'] = '';

            if (isset($_FILES['profile_picture']['name'])) {

                // Generate a new name for file
                $n = basename($_FILES['profile_picture']['name']);
                $x = pathinfo($n, PATHINFO_EXTENSION);
                $new_file_name = md5($n) . '.' . $x;

                $uri = $this->cdn_api->upload_file(
                    array(
                        'file_name' => $new_file_name,
                        'file_path' => $_FILES['profile_picture']['tmp_name']
                    )
                );

                $user['profile_picture_name'] = $new_file_name;
                //$user['profile_picture_uri'] = $uri;
            }

            $this->u_model->save($user, array('id_user' => $record->id_user, 'id_company' => $id_company));

            redirect(site_url('employee/view_records/' . $id_string . '/' . $name_surname));
        } // End POST

        $data = array(
            'success_message' => $success_message,
            'error_messages' => $error_messages,
            'profile' => $record,
            'profile_picture_src' => $profile_picture_src
        );

        $buffer = $this->load->view('user/employee/edit_profile_picture', $data, true);

        $this->user_nav->run_page($buffer, 'TalentBase - Edit Profile picture', false);
    }

// End func edir_	profile_picture

    public function edit_bank_details($id_string, $name_surname) {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(EDIT_REQUEST_EMPLOYEE_RECORDS);

        if ($name_surname == '') {
            show_404('page', false);
        }

        $id_company = $this->user_auth->get('id_company');
        $id_string = $this->uri->segment(3);

        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('reference_model', 'ref');

        $record = $this->u_model->fetch_account(array('id_string' => $id_string, 'id_company' => $id_company));

        if (!$record) {
            show_404('page', false);
        }

        $record = $record[0];

        $employee = $this->e_model->fetch_profile(array('id_user' => $record->id_user, 'id_company' => $id_company));

        if (!$employee) {
            show_404('page', false);
        }

        $employee = $employee[0];

        $success_message = '';
        $error_messages = '';

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            /* $profile = array(
              'bank_account_name' => $this->input->post('account_name'),
              'id_bank' => $this->input->post('bank'),
              'id_bank_account_type' => $this->input->post('account_type'),
              'bank_account_number' => $this->input->post('account_number')
              );

              // UPDATE RECORDS

              $this->e_model->update_profile($profile, array('id_user' => $record->id_user, 'id_company' => $id_company));

             * 
             */
            $default = 0;
            if ($this->input->post('default_acct') != '') {
                $this->e_model->update_bank_details(array('default' => $default), array('id_user' => $record->id_user, 'id_company' => $id_company));
                $default = 1;
            }

            $bank_details = array(
                'bank_account_name' => $this->input->post('account_name'),
                'id_bank' => $this->input->post('bank'),
                'id_bank_account_type' => $this->input->post('account_type'),
                'bank_account_number' => $this->input->post('account_number'),
                'id_user' => $record->id_user,
                'id_company' => $id_company,
                'default' => $default,
                'ref_id' => strtoupper(random_string('alnum', 10)),
            );

            //  INSERT RECORDS
            $this->e_model->insert_bank_details($bank_details);


            redirect(site_url('employee/view_records/' . $id_string . '/' . $name_surname . '#bank_details_container'));
        } // End POST

        $data = array(
            'success_message' => $success_message,
            'error_messages' => $error_messages,
            'profile' => $employee,
            'banks' => $this->ref->fetch_all_records('banks'),
            'account_types' => $this->account_types
        );

        $this->load->view('user/employee/add_bank_details', $data);
    }

// End func edit_bank_details

    public function edit_profile_work_info($id_string, $name_surname) {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(EDIT_REQUEST_EMPLOYEE_RECORDS);

        if ($name_surname == '') {
            show_404('page', false);
        }

        $select_access_level = false;

        $id_company = $this->user_auth->get('id_company');
        $id_string = $this->uri->segment(3);

        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('reference_model', 'ref');

        $can_edit_email = $this->u_model->can_edit_email($id_company);

        $record = $this->u_model->fetch_account(array('id_string' => $id_string, 'id_company' => $id_company));

        if (!$record) {
            show_404('page', false);
        }

        $record = $record[0];

        $employee = $this->e_model->fetch_profile(array('id_user' => $record->id_user, 'id_company' => $id_company));

        if (!$employee) {
            show_404('page', false);
        }

        $employee = $employee[0];

        $success_message = '';
        $error_messages = '';

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            if ($this->input->post('employment_date') == '') {
                $employment_date = '';
            } else {
                $employment_date = date('m/d/Y', strtotime($this->input->post('employment_date')));
            }

            if ($this->input->post('probation_expire_date') == '') {
                $probation_expire_date = '';
            } else {
                $probation_expire_date = date('m/d/Y', strtotime($this->input->post('probation_expire_date')));
            }

            if ($this->input->post('termination_date') == '') {
                $termination_date = '';
            } else {
                $termination_date = date('m/d/Y', strtotime($this->input->post('termination_date')));
            }

            $is_manager = $this->input->post('is_manager');

            if ($is_manager != 1 and $is_manager != 2) {
                $is_manager = 0;
            }

//            $no_email = $this->input->post('no_email');
//
//            if ($no_email == 1) {
//                $email = strtolower($id_string) . '.user@talentbase.ng';
//            } else {
//                $email = $this->input->post('email');
//            }

            $profile = array(
                // 'email' => $email, // See this bellow
                'work_number' => $this->input->post('work_number'),
                'id_dept' => $this->input->post('department'),
                'id_role' => $this->input->post('role'),
                'ext_job_types' => $this->input->post('job_type'),
                'id_company_location' => $this->input->post('company_location'),
                'id_manager' => $this->input->post('manager'),
                'employment_date' => $employment_date,
//                'status' => $this->input->post('employment_status'),
                'account_blocked' => 0,
                'probation_expire_date' => $probation_expire_date,
                'date_termination' => $termination_date,
                'is_manager' => $is_manager,
                'id_staff' => $this->input->post('id_staff')
            );

//            $email_exists = false;
//
//            if ($can_edit_email) {
//                $profile['email'] = $email;
//                $email_exists = $this->u_model->email_exists($this->input->post('email'), $record->id_user, $this->user_auth->get('id_company'));
//
//                if (!$email_exists) {
//                    $email_exists = $this->u_model->email_exists_in_sub_accounts($this->input->post('email'), $record->id_user, $this->user_auth->get('id_company'));
//                }
//            }
//
//            if ($email_exists == true and ! $no_email) {
//                $error_messages[] = 'This email address is already registered.';
//            } else {
            // UPDATE RECORDS

            if ($probation_expire_date != '') {
                $probation_expire_date = date('Y-m-d', strtotime($probation_expire_date));
            }

            if ($termination_date != '') {
                $termination_date = date('Y-m-d', strtotime($termination_date));
            }

            if ($employment_date != '') {
                $employment_date = date('Y-m-d', strtotime($employment_date));
            }

            $user = array(
                // 'email' => $email, // See beloow
                'id_company_location' => $this->input->post('company_location'),
//                    'status' => $this->input->post('employment_status'),
                'date_probation_expire' => $probation_expire_date,
                'date_termination' => $termination_date
            );

//                if ($can_edit_email) {
//                    $user['email'] = $email;
//                }

            $w_number = str_replace(array('(', ')', ' ', '-'), '', $this->input->post('work_number')); // Clear all special characters (-, _)

            $employee = array(
                //'work_number' => $w_number,                               disabled by Tohir
                //'id_country_code' => $this->input->post('phone_code'),
                'id_dept' => $this->input->post('department'),
                'id_role' => $this->input->post('role'),
                'id_manager' => $this->input->post('manager'),
                'employment_date' => $employment_date,
                'is_manager' => $is_manager,
//                    'id_staff' => $this->input->post('id_staff')
            );

            // UPDATE employee phone with personal -- Tohir
            $primary = 0;
            if ($this->e_model->check('employee_phone', array('id_user' => $record->id_user, 'id_company' => $id_company, 'id_phone_type' => WORK_PHONE_TYPE))) {

                if ($this->input->post('primary_phone_number') != '') {
                    $this->e_model->update_phone_details(array('primary' => $primary), array('id_user' => $record->id_user, 'id_company' => $id_company));
                    $primary = 1;
                }

                $this->e_model->update_phone_details(array(
                    'phone_number' => $w_number,
                    'id_country_code' => $this->input->post('phone_code'),
                    'primary' => $primary,
                    ), array('id_user' => $record->id_user, 'id_company' => $id_company, 'id_phone_type' => WORK_PHONE_TYPE));
            } else {

                if ($this->input->post('primary_phone_number') != '') {
                    $primary = 1;
                }
                $this->_insert_phone_number($record->id_user, $w_number, $this->input->post('phone_code'), WORK_PHONE_TYPE, $primary);
            }   // End if check


            $this->u_model->save($user, array('id_user' => $record->id_user, 'id_company' => $id_company));

//                if ($can_edit_email) {
//                    $id_own_company = $this->user_auth->get('id_company');
//                    $current_email = $record->email;
//                    $new_email = $email;
//                    $this->u_model->update_sub_company_emails($id_own_company, $current_email, $new_email);
//                }

            $this->e_model->update_profile($employee, array('id_user' => $record->id_user, 'id_company' => $id_company));

            $job_types = array();

            $this->e_model->delete_job_types($record->id_user);

            $post_job_types = $this->input->post('job_type');

            if (!empty($post_job_types)) {
                foreach ($post_job_types as $id_job) {
                    $job_types[] = array(
                        'id_job' => $id_job,
                        'id_user' => $record->id_user
                    );
                }

                $this->e_model->insert_job_types($job_types);
            }

            // Compare user status and send email if status = ACTIVE -- Chuks
            if ($record->status != USER_STATUS_ACTIVE and $this->input->post('employment_status') == USER_STATUS_ACTIVE) {

                $sender = $this->user_auth->get('company_name');
                $reset_password_url = site_url('user/reset_password') . '/' . $id_string . '/' . sha1($email . $id_string);

                $i = $this->u_model->fetch_user_role_dept_details(
                    array(
                        'id_user' => $this->user_auth->get('id_user'),
                        'id_company' => $this->user_auth->get('id_company')
                ));

                $bdata = array(
                    'full_name' => $record->first_name . ' ' . $record->last_name,
                    'reset_password_url' => $reset_password_url,
                    'email' => $email,
                    'company_name' => $this->user_auth->get('company_name'),
                    'company_url' => 'https://' . $this->user_auth->get('company_id_string') . '.talentbase.ng/login',
                    'sender_full_name' => $this->user_auth->get('display_name') . ' ' . $this->user_auth->get('last_name'),
                    'role' => $i->roles,
                    'dept' => $i->departments
                );

                //$msg = $this->load->view('email_templates/user_account_status_changed', $bdata, true);
                $msg = $this->load->view('email_templates/user_reset_password_after_register', $bdata, true);

                // Load libr.
                $this->load->library('mailgun_api');
                $this->mailgun_api->send_mail(
                    array(
                        'from' => $sender . ' <' . TALENTBASE_SUPPORT_EMAIL . '>',
                        'to' => $email,
                        'bcc' => 'helpdesk@talentbase.ng',
                        'subject' => 'Access Granted: ' . $this->user_auth->get('company_name') . ' Employee Portal',
                        'html' => '<html>' . $msg . '</html>'
                    )
                );
            }
            // End -- Chuks

            redirect(site_url('employee/view_records/' . $id_string . '/' . $name_surname));
            //}
        } else {

            $jts = $this->e_model->fetch_job_types($record->id_user);
            $employee_job_types = array();

            if (!empty($jts)) {
                foreach ($jts as $jt) {
                    $employee_job_types[] = $jt->id_type;
                }
            }

            if ($record->date_probation_expire != '' and $record->date_probation_expire != '0000-00-00 00:00:00') {
                $probation_expire_date = date('m/d/Y', strtotime($record->date_probation_expire));
            } else {
                $probation_expire_date = '';
            }

            if ($record->date_termination != '' and $record->date_termination != '0000-00-00 00:00:00') {
                $date_termination = date('m/d/Y', strtotime($record->date_termination));
            } else {
                $date_termination = '';
            }

            $w_number = '';
            $id_country_code = '234';
            $is_primary = 0;
            $emp_phone_details = $this->e_model->fetch_employee_phone_details($record->id_user, WORK_PHONE_TYPE);
            if (!empty($emp_phone_details)) {
                $w_number = $emp_phone_details->phone_number;
                $id_country_code = $emp_phone_details->id_country_code;
                $is_primary = $emp_phone_details->primary;
            }

            $profile = array(
                'email' => $record->email,
                //'work_number' => $employee->work_number,
                'work_number' => $w_number,
                'id_country_code' => $id_country_code,
                'is_primary_number' => $is_primary,
                'id_dept' => $employee->id_dept,
                'id_role' => $employee->id_role,
                'ext_job_types' => $employee_job_types,
                'id_company_location' => $record->id_company_location,
                'id_manager' => $employee->id_manager,
                'employment_date' => date('d-m-Y', strtotime($employee->employment_date)),
                'status' => $record->status,
                'probation_expire_date' => $probation_expire_date,
                'date_termination' => $date_termination,
                'is_manager' => $employee->is_manager,
                'id_staff' => $employee->id_staff
            );
        } // End POST
        // 2 = CEO
        // 1 = MANAGER
        // 0 = None
        $CEO = $this->ref->fetch_all_records('employees', array('id_company' => $id_company, 'is_manager' => 2));

        if (!$CEO) {
            $allow_ceo = true;
        } else {
            $allow_ceo = false;
        }

        if ($employee->is_manager == 2) {
            $allow_ceo = true;
        }

        $data = array(
            'success_message' => $success_message,
            'error_messages' => $error_messages,
            'profile' => $profile,
            'departments' => $this->ref->fetch_all_records('departments', array('id_company' => $id_company)),
            'roles' => $this->ref->fetch_all_records('roles', array('id_company' => $id_company)),
            'job_types' => $this->ref->fetch_all_records('job_types'),
            'company_locations' => $this->ref->fetch_all_records('company_locations', array('id_company' => $id_company)),
            'managers' => $this->e_model->fetch_managers($id_company, $record->id_user),
            'statuses' => $this->user_auth->statuses(),
            'allow_ceo' => $allow_ceo,
            'select_access_level' => $select_access_level,
            'can_edit_email' => $can_edit_email,
            'countries' => $this->ref->fetch_all_records('countries')
        );

        $buffer = $this->load->view('user/employee/edit_profile_work_info', $data, true);

        $this->user_nav->run_page($buffer, 'TalentBase - Edit Profile / Work info', false);
    }

// End func edit_profile_work_info

    public function edit_profile_personal_info($id_string, $name_surname) {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(EDIT_REQUEST_EMPLOYEE_RECORDS);

        if ($name_surname == '') {
            show_404('page', false);
        }

        $id_company = $this->user_auth->get('id_company');
        $id_string = $this->uri->segment(3);

        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('reference_model', 'ref');

        $record = $this->u_model->fetch_account(array('id_string' => $id_string, 'id_company' => $id_company));

        if (!$record) {
            show_404('page', false);
        }

        $record = $record[0];

        $employee = $this->e_model->fetch_profile(array('id_user' => $record->id_user, 'id_company' => $id_company));

        if (!$employee) {
            show_404('page', false);
        }

        $employee = $employee[0];

        $success_message = '';
        $error_messages = '';

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            if ($this->input->post('dob') == '') {
                $dob = '';
            } else {
                $dob = date('m/d/Y', strtotime($this->input->post('dob')));
            }

            $id_state = $this->input->post('id_state');

            if ($id_state == '') {
                $id_state = 0;
            }

            $profile = array(
                'id_gender' => $this->input->post('id_gender'),
                'id_marital_status' => $this->input->post('id_marital_status'),
                'dob' => $dob,
                'id_nationality' => $this->input->post('id_nationality'),
                'address' => $this->input->post('address'),
                'city' => $this->input->post('city'),
                'id_country' => $this->input->post('country'),
                'id_state' => $id_state,
            );

            // UPDATE RECORDS

            if ($dob != '') {
                $dob = date('Y-m-d', strtotime($dob));
            }

            $user = array(
                'id_gender' => $this->input->post('id_gender'),
                'id_marital_status' => $this->input->post('id_marital_status'),
                'dob' => $dob,
                'id_nationality' => $this->input->post('id_nationality'),
                'address' => $this->input->post('address'),
                'city' => $this->input->post('city'),
                'id_country' => $this->input->post('country'),
                'id_state' => $id_state,
            );

            // UPDATE employee phone with personal -- Tohir
            $primary = 0;
            $p_number = str_replace(array('(', ')', ' ', '-'), '', $this->input->post('personal_number')); // Clear all special characters (-, _)
            if ($this->e_model->check('employee_phone', array('id_user' => $record->id_user, 'id_company' => $id_company, 'id_phone_type' => PERSONAL_PHONE_TYPE))) {

                if ($this->input->post('primary_phone_number') != '') {
                    $this->e_model->update_phone_details(array('primary' => $primary), array('id_user' => $record->id_user, 'id_company' => $id_company));
                    $primary = 1;
                }

                $this->e_model->update_phone_details(array(
                    'phone_number' => $p_number,
                    'id_country_code' => $this->input->post('phone_code'),
                    'primary' => $primary,
                    ), array('id_user' => $record->id_user, 'id_company' => $id_company, 'id_phone_type' => PERSONAL_PHONE_TYPE));
            } else {

                if ($this->input->post('primary_phone_number') != '') {
                    $primary = 1;
                }
                $this->_insert_phone_number($record->id_user, $p_number, $this->input->post('phone_code'), PERSONAL_PHONE_TYPE, $primary);
            }   // End if check

            $this->u_model->save($user, array('id_user' => $record->id_user, 'id_company' => $id_company));

            redirect(site_url('employee/view_records/' . $id_string . '/' . $name_surname));
        } else {

            if ($record->dob != '' and $record->dob != '0000-00-00 00:00:00') {
                $dob = date('d-m-Y', strtotime($record->dob));
            } else {
                $dob = '';
            }

            $p_number = '';
            $id_country_code = '234';
            $is_primary = 0;
            $emp_phone_details = $this->e_model->fetch_employee_phone_details($record->id_user, PERSONAL_PHONE_TYPE);
            if (!empty($emp_phone_details)) {
                $p_number = $emp_phone_details->phone_number;
                $id_country_code = $emp_phone_details->id_country_code;
                $is_primary = $emp_phone_details->primary;
            }

            $profile = array(
                'id_gender' => $record->id_gender,
                'id_marital_status' => $record->id_marital_status,
                'personal_number' => $p_number,
                'id_country_code' => $id_country_code,
                'is_primary_number' => $is_primary,
                'dob' => $dob,
                'id_nationality' => $record->id_nationality,
                'address' => $record->address,
                'city' => $record->city,
                'id_country' => $record->id_country,
                'id_state' => $record->id_state,
//                'first_name' => $record->first_name,
//                'last_name' => $record->last_name,
            );
        } // End POST

        $data = array(
            'success_message' => $success_message,
            'error_messages' => $error_messages,
            'profile' => $profile,
            'countries' => $this->ref->fetch_all_records('countries'),
        );

        $buffer = $this->load->view('user/employee/edit_profile_pers_info', $data, true);

        $this->user_nav->run_page($buffer, 'TalentBase - Edit Profile / Personal info', false);
    }

// End func edit_profile_personal_info

    public function request_profile_personal_info($id_string, $name_surname) {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(EDIT_REQUEST_EMPLOYEE_RECORDS);

        if ($name_surname == '') {
            show_404('page', false);
        }

        if ($this->input->server('REQUEST_METHOD') != 'POST') {
            show_404('page', false);
        }

        $data = $this->input->post();

        $id_company = $this->user_auth->get('id_company');
        $id_string = $this->uri->segment(3);

        if (!isset($data['request_from_employee_profile_desc'])) {
            show_404('page', false);
        }

        if ($data['request_from_employee_profile_desc'] == '') {
            show_404('page', false);
        }

        $due_date = date('Y-m-d', strtotime($data['request_from_employee_profile_due_date']));

        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('tasks/tasks_model', 't_model');

        $record = $this->u_model->fetch_account(array('id_string' => $id_string, 'id_company' => $id_company));

        if (!$record) {
            show_404('page', false);
        }

        $record = $record[0];

        // Add temp perm

        $id_company = $this->user_auth->get('id_company');

        $perms_temp = array(
            'id_user' => $record->id_user,
            'id_creator' => $this->user_auth->get('id_user'),
            'date_expire' => $due_date,
            'id_module' => 1,
            'date_created' => date('Y-m-d'),
            'id_company' => $id_company,
            'id_perm' => EDIT_MY_PROFILE_PERSONAL_INFO
        );

        $y = $this->u_model->replace_perm_temp(EDIT_MY_PROFILE_PERSONAL_INFO, $perms_temp);

        $link = anchor('personal/my_profile', 'Personal profile request from '
            . $this->user_auth->get('display_name') . ' ' . $this->user_auth->get('last_name'));

        $tasks = array(
            0 => array(
                'id_user_from' => $this->user_auth->get('id_user'),
                'id_user_to' => $record->id_user,
                'id_module' => 1,
                //'subject' => $data['request_from_employee_profile_desc'],
                'subject' => $link,
                'status' => 0,
                'date_will_start' => date('Y-m-d'),
                'date_will_end' => $due_date,
                'tb_name' => 'user_perms_temp',
                'tb_column_name' => 'id_perms_temp',
                'tb_id_record' => $y[0],
                'notes' => $data['request_from_employee_profile_desc']
            )
        );

        $this->t_model->add_task($tasks);

        // Send request notification via email to employee -- Chuks
        $this->load->library('mailgun_api');

        $i = array(
            'first_name' => $record->first_name,
            'company_name' => $this->user_auth->get('company_name'),
            'company_string' => $this->user_auth->get('company_id_string')
        );

        $msg = $this->load->view('email_templates/request_edit_personal_profile', $i, true);

        $this->mailgun_api->send_mail(
            array(
                'from' => $this->user_auth->get('company_name') . ' <noreply@talentbase.ng>',
                'to' => $record->email,
                'subject' => 'Request From ' . $this->user_auth->get('company_name'),
                'html' => '<html>' . $msg . '</html>'
        ));

        // End

        echo 'Redirecting ... ';

        redirect(
            site_url('employee/view_records/' . $id_string . '/' . $name_surname)
        );
    }

// End func request_profile_personal_info

    public function edit_document($id_doc, $id_string, $name_surname) {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(EDIT_REQUEST_EMPLOYEE_RECORDS);

        if ($name_surname == '') {
            show_404('page', false);
        }

        if ($id_doc == '') {
            show_404('page', false);
        }

        if ($id_string == '') {
            show_404('page', false);
        }

        $id_company = $this->user_auth->get('id_company');

        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('tasks/tasks_model', 't_model');

        $record = $this->u_model->fetch_account(array('id_string' => $id_string, 'id_company' => $id_company));

        if (!$record) {
            show_404('page', false);
        }

        $record = $record[0];

        $doc = $this->e_model->fetch_document(array('id_cred' => $id_doc));

        if (!$doc) {
            show_404('page', false);
        }

        $doc = $doc[0];

        $success_message = '';
        $error_messages = '';

        $data = array();

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $data['file_title'] = $this->input->post('upload_file_name');
            $data['file_description'] = $this->input->post('upload_file_description');
            $data['file_name'] = $doc->file_name;

            if (trim($data['file_title']) == '') {
                $error_messages[] = 'Enter file name.';
            }

            if (trim($data['file_description']) == '') {
                $error_messages[] = 'Enter file description.';
            }

            $uri = '';
            $new_file_name = '';

            if (isset($_FILES['upload_file']) and $_FILES['upload_file']['name'] != '') {

                $file = $_FILES['upload_file'];

                if (empty($error_messages)) {

                    // Create a connection
                    $this->cdn_api->connect(
                        array(
                            'username' => RACKSPACE_USERNAME,
                            'api_key' => RACKSPACE_API_KEY,
                            'container' => $this->user_auth->get('cdn_container')
                        )
                    );

                    // Generate a new name for file
                    $n = basename($file['name']);
                    $x = pathinfo($n, PATHINFO_EXTENSION);
                    $new_file_name = md5($n) . '.' . $x;

                    $uri = $this->cdn_api->upload_file(
                        array(
                            'file_name' => $new_file_name,
                            'file_path' => $file['tmp_name']
                        )
                    );

                    // $data['file_name'] = $new_file_name;
                }
            }

            if (empty($error_messages)) {

                $ins_data = array(
                    'file_uri' => $uri,
                    'description' => $data['file_description'],
                    'type' => 1,
                    'id_user' => $record->id_user,
                    'id_uploader_user' => $this->user_auth->get('id_user'),
                    'file_title' => $data['file_title']
                );

                if ($new_file_name != '') {
                    $ins_data['file_name'] = $new_file_name;
                }

                $this->e_model->update_document($ins_data, array('id_cred' => $id_doc));

                redirect(site_url('employee/view_records/' . $id_string . '/' . $name_surname));
            }
        } else {
            $data['file_name'] = $doc->file_name;
            $data['file_title'] = $doc->file_title;
            $data['file_description'] = $doc->description;
        }

        $data['success_message'] = $success_message;
        $data['error_messages'] = $error_messages;

        $buffer = $this->load->view('user/employee/edit_file', $data, true);

        $this->user_nav->run_page($buffer, 'TalentBase - Edit Resume / Credentials', false);
    }

// End func edit_document

    public function upload_document($id_string, $name_surname) {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(EDIT_REQUEST_EMPLOYEE_RECORDS);

        if ($name_surname == '') {
            show_404('page', false);
        }

        $id_company = $this->user_auth->get('id_company');
        $id_string = $this->uri->segment(3);

        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('tasks/tasks_model', 't_model');

        $record = $this->u_model->fetch_account(array('id_string' => $id_string, 'id_company' => $id_company));

        if (!$record) {
            show_404('page', false);
        }

        $record = $record[0];

        if ($record->status == USER_STATUS_ACTIVE or $record->status == USER_STATUS_PROBATION_ACCESS) {
            $send_request = true;
        } else {
            $send_request = false;
        }

        $success_message = '';
        $error_messages = '';

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $form_type = $this->input->post('form_type');

            if ($form_type == 'upload_now_tab') {
                // Upload file
                $result = $this->_upload_employee_document($record);
            } else {
                // Request employee
                $result = $this->_send_request_employee_document($record);
            }

            if ($result === true) {
                redirect(site_url('employee/view_records/' . $id_string . '/' . $name_surname . '#resume_credentials_container'));
            }

            $error_messages = $result;
        }

        $data = array(
            'success_message' => $success_message,
            'error_messages' => $error_messages,
            'send_request' => $send_request
        );

        $this->load->view('user/employee/upload_file', $data);
    }

// End func upload_document

    private function _upload_employee_document($record) {

        $file_name = $this->input->post('upload_file_name');
        $file_description = $this->input->post('upload_file_description');

        $id_cred = $this->e_model->insert_data(array(
            'table' => 'employee_credentials',
            'vals' => array(
                'id_user' => $record->id_user,
                'type' => 1,
                'description' => $file_description,
                'file_title' => $file_name,
                'id_uploader_user' => $this->user_auth->get('id_user'),
            )
        ));



        if (isset($_FILES['upload_file'])) {
            $file = $_FILES['upload_file'];
        } else {
            $file = NULL;
        }

        for ($i = 0; $i <= count($_FILES['upload_file']['name']) - 1; ++$i) {
            $errors = array();

            if (trim($file_name) == '') {
                $errors[] = 'Enter file name.';
            }

            if (trim($file_description) == '') {
                $errors[] = 'Enter file description.';
            }

            if (!isset($file['name'][$i])) {
                $errors[] = 'Include file.';
            }

            if (!empty($errors)) {
                return $errors;
            }

            // Create a connection
            $this->cdn_api->connect(
                array(
                    'username' => RACKSPACE_USERNAME,
                    'api_key' => RACKSPACE_API_KEY,
                    'container' => $this->user_auth->get('cdn_container')
                )
            );

            // Generate a new name for file
            $n = basename($file['name'][$i]);
            $x = pathinfo($n, PATHINFO_EXTENSION);
            $new_file_name = md5($n) . '.' . $x;

            $uri = $this->cdn_api->upload_file(
                array(
                    'file_name' => $new_file_name,
                    'file_path' => $file['tmp_name'][$i]
                )
            );

            $ins_data = array(
                'file_name' => $new_file_name,
                'id_cred' => $id_cred
            );

            $this->e_model->insert_data(array(
                'table' => 'employee_credential_files',
                'vals' => $ins_data));
        }


        return true;
    }

// End func _upload_employee_document

    private function _send_request_employee_document($record) {

        $file_date = $this->input->post('request_file_date');
        $file_name = $this->input->post('request_file_name');
        $file_description = $this->input->post('request_file_description');

        $errors = array();

        if (trim($file_date) == '') {
            $errors[] = 'Select due date.';
        }

        if (trim($file_name) == '') {
            $errors[] = 'Enter file name.';
        }

        if (trim($file_description) == '') {
            $errors[] = 'Enter file description.';
        }

        if (!empty($errors)) {
            return $errors;
        }

        $ins_data = array(
            'file_name' => '',
            'file_uri' => '',
            'description' => $file_description,
            'type' => 0,
            'id_user' => $record->id_user,
            'id_uploader_user' => $this->user_auth->get('id_user'),
            'file_title' => $file_name,
            'due_date' => date('Y-m-d', strtotime($file_date))
        );

        $this->e_model->insert_credentials(array($ins_data));

        $tasks[] = array(
            'id_user_from' => $this->user_auth->get('id_user'),
            'id_user_to' => $record->id_user,
            'id_module' => 1,
            'subject' => $file_description,
            'status' => 0,
            'date_will_start' => date('Y-m-d', strtotime($file_date)),
            'date_will_end' => date('Y-m-d', strtotime($file_date))
        );

        $this->t_model->add_task($tasks);

        $id_company = $this->user_auth->get('id_company');

        /*
          $perms_temp = array(
          'id_user' => $record->id_user,
          'id_creator' => $this->user_auth->get('id_user'),
          'date_expire' => date('Y-m-d', strtotime($file_date)),
          'id_module' => 1,
          'date_created' => date('Y-m-d'),
          'id_company' => $id_company,
          'id_perm' => EDIT_MY_CREDENTIALS
          );

          $this->u_model->replace_perm_temp(EDIT_MY_CREDENTIALS, $perms_temp);
         */

        return true;
    }

// End func _send_request_employee_document

    public function edit_credentials($id_string, $name_surname) {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(EDIT_REQUEST_EMPLOYEE_RECORDS);

        if ($name_surname == '') {
            show_404('page', false);
        }

        $id_company = $this->user_auth->get('id_company');
        $id_string = $this->uri->segment(3);

        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('tasks/tasks_model', 't_model');

        $record = $this->u_model->fetch_account(array('id_string' => $id_string, 'id_company' => $id_company));

        if (!$record) {
            show_404('page', false);
        }

        $record = $record[0];

        $success_message = '';
        $error_messages = '';

        // Create a connection
        $this->cdn_api->connect(
            array(
                'username' => RACKSPACE_USERNAME,
                'api_key' => RACKSPACE_API_KEY,
                'container' => $this->user_auth->get('cdn_container')
            )
        );

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $tasks = array();
            $ins_data = array();

            $data = $this->input->post();

            // Add new requests
            if (isset($data['request_date']) and is_array($data['request_date'])) {

                foreach ($data['request_date'] as $index => $rd) {

                    if ($rd != '' and $data['request_description'][$index] != '' and $data['request_file_name'][$index] != '') {

                        $date_due = date('Y-m-d', strtotime($rd));

                        $ins_data[] = array(
                            'file_name' => '',
                            'file_uri' => '',
                            'description' => $data['request_description'][$index],
                            'type' => 0,
                            'id_user' => $record->id_user,
                            'id_uploader_user' => $this->user_auth->get('id_user'),
                            'file_title' => $data['request_file_name'][$index]
                        );

                        $tasks[] = array(
                            'id_user_from' => $this->user_auth->get('id_user'),
                            'id_user_to' => $record->id_user,
                            'id_module' => 1,
                            'subject' => $data['request_description'][$index],
                            'status' => 0,
                            'date_will_start' => date('Y-m-d'),
                            'date_will_end' => $date_due
                        );
                    }
                }
            }

            // Add new files
            if (isset($data['attachment_description']) and is_array($data['attachment_description'])) {

                foreach ($data['attachment_description'] as $index => $at_description) {

                    if ($at_description != '' and isset($_FILES['resume']['name'][$index]) and $_FILES['resume']['name'][$index] != '' and $data['attachment_file_name'][$index] != '') {

                        // Generate a new name for file
                        $n = basename($_FILES['resume']['name'][$index]);
                        $x = pathinfo($n, PATHINFO_EXTENSION);
                        $new_file_name = md5($n) . '.' . $x;

                        $uri = $this->cdn_api->upload_file(
                            array(
                                'file_name' => $new_file_name,
                                'file_path' => $_FILES['resume']['tmp_name'][$index]
                        ));

                        $ins_data[] = array(
                            'file_name' => $new_file_name,
                            'file_uri' => $uri,
                            'description' => $at_description,
                            'type' => 1,
                            'id_user' => $record->id_user,
                            'id_uploader_user' => $this->user_auth->get('id_user'),
                            'file_title' => $data['attachment_file_name'][$index]
                        );
                    }
                }
            }

            if (!empty($ins_data)) {
                $this->e_model->insert_credentials($ins_data);
            }

            if (!empty($tasks)) {
                $this->t_model->add_task($tasks);
            }

            // Remove 

            if (isset($data['remove_file']) and is_array($data['remove_file'])) {
                foreach ($data['remove_file'] as $id_cred) {
                    $this->e_model->remove_credentials($id_cred, $record->id_user);
                }
            }

            // Update existing
            if (isset($data['edit_id_cred']) and is_array($data['edit_id_cred'])) {
                foreach ($data['edit_id_cred'] as $index => $id_cred) {

                    $update_data = array();

                    if (isset($data['old_attachment_description']) and is_array($data['old_attachment_description'])) {

                        $description = $data['old_attachment_description'][$index];

                        if ($description != '' and $data['old_attachment_file_name'][$index] != '') {

                            if (isset($_FILES['new_resume']['name'][$index]) and $_FILES['new_resume']['name'][$index] != '') {

                                // Generate a new name for file
                                $n = basename($_FILES['new_resume']['name'][$index]);
                                $x = pathinfo($n, PATHINFO_EXTENSION);
                                $new_file_name = md5($n) . '.' . $x;

                                $uri = $this->cdn_api->upload_file(
                                    array(
                                        'file_name' => $new_file_name,
                                        'file_path' => $_FILES['new_resume']['tmp_name'][$index]
                                ));

                                $update_data['file_name'] = $new_file_name;
                                $update_data['file_uri'] = $uri;
                            }

                            $update_data['file_title'] = $data['old_attachment_file_name'][$index];
                            $update_data['id_uploader_user'] = $this->user_auth->get('id_user');
                            $update_data['description'] = $description;

                            $this->e_model->update_credentials($update_data, array('id_cred' => $id_cred));
                        }
                    }
                }
            }

            redirect(site_url('employee/view_records/' . $id_string . '/' . $name_surname));

            return true;
        } // End if POST

        if ($record->status == USER_STATUS_ACTIVE or $record->status == USER_STATUS_PROBATION_ACCESS) {
            $send_request = true;
        } else {
            $send_request = false;
        }

        $data = array(
            'success_message' => $success_message,
            'error_messages' => $error_messages,
            'files' => $this->e_model->fetch_credentials($record->id_user),
            'send_request' => $send_request
        );

        $buffer = $this->load->view('user/employee/edit_credentials', $data, true);

        $this->user_nav->run_page($buffer, 'TalentBase - Edit Resume / Credentials', false);
    }

// End func edit_credentials

    /**
     * Create account page
     *
     * @access public
     * @return void
     * */
    public function create_account() {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(ADD_NEW_EMPLOYEE);

        $this->load->model('reference_model', 'ref');
        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('tasks/tasks_model', 't_model');


        $tmp = $this->ref->fetch_all_records('accounts');
        $_access_levels = array();

        foreach ($tmp as $l) {
            $_access_levels[$l->accounttype] = $l->name;
        }

        $select_access_level = true;

        $tasks = array();

        $success_message = '';

        $this->load->helper('string_helper');

        $id_company = $this->user_auth->get('id_company');
        $id_admin = $this->user_auth->get('id_user');

        $error_messages = NULL;

        $perms_temp = array();

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $data = $this->input->post();

            $employee = array();
            $user = array();

            // Special fields
            $employee['id_company'] = $id_company;

            $user['id_company'] = $id_company;
            $user['id_creator'] = $this->user_auth->get('id_user');
            $user['id_string'] = str_replace(' ', '', $this->user_auth->create_id_string());
            $user['account_type'] = USER_TYPE_EMPLOYEE;
            $user['password'] = sha1(md5(time()));

            if (isset($data['is_manager']) and $data['is_manager'] != 0) {
                /* ... */
            } else {
                $data['is_manager'] = 0;
            }

            $employee['is_manager'] = $data['is_manager'];

            // form fields
            $user['first_name'] = $data['first_name'];
            if ($user['first_name'] == '') {
                $error_messages[] = 'Enter first name.';
            }

            $user['last_name'] = $data['last_name'];
            if ($user['last_name'] == '') {
                $error_messages[] = 'Enter last name.';
            }

            $user['id_gender'] = $data['id_gender'];
            $user['id_marital_status'] = $data['id_marital_status'];

            $user['dob'] = date('Y-m-d', strtotime($data['dob']));
            if ($user['dob'] == '') {
                $error_messages[] = 'Enter Birth Date.';
            }

            $user['id_nationality'] = $data['id_nationality'];

            $user['address'] = $data['address'];
            if ($user['address'] == '') {
                $error_messages[] = 'Enter Address.';
            }

            $user['city'] = $data['city'];
            if ($user['city'] == '') {
                $error_messages[] = 'Enter city.';
            }

            $user['id_company_location'] = $data['company_location'];
            $user['id_country'] = $data['country'];
            $user['id_state'] = $data['id_state'];

            if (isset($data['no_email']) and $data['no_email'] == 1) {
                $user['email'] = strtolower($user['id_string']) . '.user@talentbase.ng';
            } else {
                $user['email'] = $data['work_email'];
            }

            if ($user['email'] == '') {
                $error_messages[] = 'Enter email.';
            }

            $user['id_pfs'] = '';
            $user['rsa_pin'] = '';

            // NOW
            if ($data['benefits_select'] == 'now') {
                $user['id_pfs'] = $data['pfs'];
                $user['rsa_pin'] = $data['rsa_pin'];
            }

            // LATER
            if ($data['benefits_select'] == 'later') {
                /* ... do nothing ... */
            }

            if ($data['benefits_select'] == 'request') {
                $benefits_request_date = date('Y-m-d', strtotime($data['benefits_request_date']));

                $tasks[] = array(
                    'id_user_from' => $this->user_auth->get('id_user'),
                    'id_user_to' => NULL,
                    'id_module' => 1,
                    'subject' => $data['benefits_request_instructions'],
                    'status' => 0,
                    'date_will_start' => date('Y-m-d'),
                    'date_will_end' => $benefits_request_date
                );

                $perms_temp[] = array(
                    'id_user' => NULL,
                    'id_creator' => $this->user_auth->get('id_user'),
                    'date_expire' => $benefits_request_date,
                    'id_module' => 1,
                    'date_created' => date('Y-m-d'),
                    'id_company' => $id_company,
                    'id_perm' => EDIT_MY_BENEFITS_DETAILS
                );
            }

            $user['status'] = $data['employment_status'];

            $user['date_probation_expire'] = '';
            $user['date_termination'] = '';

            if ($user['status'] == USER_STATUS_PROBATION_ACCESS or $user['status'] == USER_STATUS_PROBATION_NO_ACCESS) {
                $user['date_probation_expire'] = date('Y-m-d', strtotime($data['probation_expire_date']));
            } elseif ($user['status'] == USER_STATUS_TERMINATED) {
                $user['date_termination'] = date('Y-m-d', strtotime($data['termination_date']));
            }

            // Contact 1
            $user['emergency_first_name'] = '';
            $user['emergency_last_name'] = '';
            $user['emergency_address'] = '';
            $user['emergency_email'] = '';
            $user['emergency_phone_number'] = '';
            $user['emergency_employer'] = '';
            $user['emergency_work_address'] = '';

            // Contact 2
            $user['emergency_first_name2'] = '';
            $user['emergency_last_name2'] = '';
            $user['emergency_address2'] = '';
            $user['emergency_email2'] = '';
            $user['emergency_phone_number2'] = '';
            $user['emergency_employer2'] = '';
            $user['emergency_work_address2'] = '';

            $user['emergency_request_date'] = '';
            $user['emergency_request_instructions'] = '';

            if (!isset($data['emergency_info_select'])) {
                $data['emergency_info_select'] = 'later';
            }

            // now
            if ($data['emergency_info_select'] == 'now') {

                // Contact 1
                $user['emergency_first_name'] = $data['emergency_first_name'];
                $user['emergency_last_name'] = $data['emergency_last_name'];
                $user['emergency_address'] = $data['emergency_address'];
                $user['emergency_email'] = $data['emergency_email'];
                $user['emergency_phone_number'] = $data['emergency_phone_number'];
                $user['emergency_employer'] = $data['emergency_employer'];
                $user['emergency_work_address'] = $data['emergency_work_address'];

                // Contact 2
                $user['emergency_first_name2'] = $data['emergency_first_name2'];
                $user['emergency_last_name2'] = $data['emergency_last_name2'];
                $user['emergency_address2'] = $data['emergency_address2'];
                $user['emergency_email2'] = $data['emergency_email2'];
                $user['emergency_phone_number2'] = $data['emergency_phone_number2'];
                $user['emergency_employer2'] = $data['emergency_employer2'];
                $user['emergency_work_address2'] = $data['emergency_work_address2'];
            }

            // request
            if ($data['emergency_info_select'] == 'request') {

                $user['emergency_request_date'] = date('Y-m-d', strtotime($data['emergency_request_date']));
                $user['emergency_request_instructions'] = $data['emergency_request_instructions'];

                $tasks[] = array(
                    'id_user_from' => $this->user_auth->get('id_user'),
                    'id_user_to' => NULL,
                    'id_module' => 1,
                    'subject' => $user['emergency_request_instructions'],
                    'status' => 0,
                    'date_will_start' => date('Y-m-d'),
                    'date_will_end' => $user['emergency_request_date']
                );

                $perms_temp[] = array(
                    'id_user' => NULL,
                    'id_creator' => $this->user_auth->get('id_user'),
                    'date_expire' => $user['emergency_request_date'],
                    'id_module' => 1,
                    'date_created' => date('Y-m-d'),
                    'id_company' => $id_company,
                    'id_perm' => EDIT_MY_EMERGENCY_CONTACTS
                );
            }

            // later
            if ($data['emergency_info_select'] == 'later') {

                // Do nothing
            }

            $set_work_as_primary = 0;
            $set_personal_as_primary = 0;

            $personal_number = str_replace(array('(', ')', ' ', '-'), '', $data['personal_number']); // Clear all special characters (-, _)
            $personal_phone_code = $data['personal_phone_code'];

            $work_number = str_replace(array('(', ')', ' ', '-'), '', $data['work_number']); // Clear all special characters (-, _)
            $id_country_code = $data['phone_code'];
            $employee['id_dept'] = $data['department'];
            $employee['id_role'] = $data['role'];

            if ($data['primary_phone_number'] == 1) {
                $set_personal_as_primary = 1;
            } else if ($data['primary_phone_number'] == 2) {
                $set_work_as_primary = 1;
            }

            // Staff ID
            $employee['id_staff'] = $data['id_staff'];

            if (!isset($data['manager'])) {
                $data['manager'] = 0;
            }

            $employee['id_manager'] = $data['manager'];
            $employee['id_manager_vacation'] = $data['manager'];
            $employee['employment_date'] = date('Y-m-d', strtotime($data['employment_date']));
            $employee['id_access_level'] = $data['access_level'];
            $employee['id_company_level'] = $data['level'];

            $access_level_name = $_access_levels[$data['access_level']];

            if (!isset($data['bank_info_select'])) {
                $data['bank_info_select'] = 'later';
            }

            if ($data['bank_info_select'] == 'now') {

                // now
                $employee['bank_account_name'] = $data['bank_account_name'];
                $employee['id_bank'] = $data['id_bank'];
                $employee['id_bank_account_type'] = $data['id_account_type'];
                $employee['bank_account_number'] = $data['account_number'];

                // request
                $employee['bank_request_date'] = '';
                $employee['bank_request_instructions'] = '';
            }

            if ($data['bank_info_select'] == 'request') {

                // now
                $employee['bank_account_name'] = '';
                $employee['id_bank'] = 0;
                $employee['id_bank_account_type'] = 0;
                $employee['bank_account_number'] = '';

                // request
                $employee['bank_request_date'] = date('Y-m-d', strtotime($data['bank_request_date']));
                $employee['bank_request_instructions'] = $data['bank_request_instructions'];

                $tasks[] = array(
                    'id_user_from' => $this->user_auth->get('id_user'),
                    'id_user_to' => NULL,
                    'id_module' => 1,
                    'subject' => $employee['bank_request_instructions'],
                    'status' => 0,
                    'date_will_start' => date('Y-m-d'),
                    'date_will_end' => $employee['bank_request_date']
                );

                $perms_temp[] = array(
                    'id_user' => NULL,
                    'id_creator' => $this->user_auth->get('id_user'),
                    'date_expire' => $employee['bank_request_date'],
                    'id_module' => 1,
                    'date_created' => date('Y-m-d'),
                    'id_company' => $id_company,
                    'id_perm' => EDIT_MY_BANK_DETAILS
                );
            }

            if ($data['bank_info_select'] == 'later') {

                // now
                $employee['bank_account_name'] = '';
                $employee['id_bank'] = 0;
                $employee['id_bank_account_type'] = 0;
                $employee['bank_account_number'] = '';

                // request
                $employee['bank_request_date'] = '';
                $employee['bank_request_instructions'] = '';
            }

            // Create a connection
            $this->cdn_api->connect(
                array(
                    'username' => RACKSPACE_USERNAME,
                    'api_key' => RACKSPACE_API_KEY,
                    'container' => $this->user_auth->get('cdn_container')
                )
            );

            // Profile picture

            $user['profile_picture_name'] = '';
            $user['profile_picture_uri'] = '';

            if ($data['profile_pic_select'] === 'now') {

                if (isset($_FILES['profile_picture']['name'])) {

                    // Generate a new name for file
                    $n = basename($_FILES['profile_picture']['name']);
                    $x = pathinfo($n, PATHINFO_EXTENSION);
                    $new_file_name = md5($n) . '.' . $x;

                    $uri = $this->cdn_api->upload_file(
                        array(
                            'file_name' => $new_file_name,
                            'file_path' => $_FILES['profile_picture']['tmp_name']
                        )
                    );

                    $user['profile_picture_name'] = $new_file_name;
                    $user['profile_picture_uri'] = $uri;
                }
            }

            if ($data['profile_pic_select'] == 'request') {

                $profile_pic_request_date = date('Y-m-d', strtotime($data['profile_pic_request_date']));

                $tasks[] = array(
                    'id_user_from' => $this->user_auth->get('id_user'),
                    'id_user_to' => NULL,
                    'id_module' => 1,
                    'subject' => $data['profile_pic_request_instructions'],
                    'status' => 0,
                    'date_will_start' => date('Y-m-d'),
                    'date_will_end' => $profile_pic_request_date
                );

                $perms_temp[] = array(
                    'id_user' => NULL,
                    'id_creator' => $this->user_auth->get('id_user'),
                    'date_expire' => $profile_pic_request_date,
                    'id_module' => 1,
                    'date_created' => date('Y-m-d'),
                    'id_company' => $id_company,
                    'id_perm' => EDIT_MY_PROFILE_PICTURE
                );
            }

            if ($data['profile_pic_select'] == 'later') {
                /* ... do nothing ... */
            }

            // 1. insert user data
            // 1.1 Check, if user exists.
            $user_record = $this->u_model->fetch_account(array('email' => $user['email'], 'id_company' => $this->user_auth->get('id_company')));

            $id_user = false;

            if (!empty($user_record)) {
                $error_messages[] = 'Account with email "' . $user['email'] . '" is already registered.';
            } else {
                $id_user = $this->u_model->save($user);

                if (!$id_user) {
                    trigger_error('Cannot insert user data', E_USER_ERROR);
                }

                // Insert phone number to employee_phone table: Tohir
                $this->_insert_phone_number($id_user, $personal_number, $personal_phone_code, PERSONAL_PHONE_TYPE, $set_personal_as_primary);
                $this->_insert_phone_number($id_user, $work_number, $id_country_code, WORK_PHONE_TYPE, $set_work_as_primary);

                if (isset($data['paygrade']) && $data['paygrade'] != '') {
                    $this->load->model('payroll/compensation_model', 'comp');
                    $this->comp->assignPaygrade($id_user, $data['paygrade']);
                }
            }

            // 1.1 Insert user default perms
            if ($access_level_name == 'Secondary Admin') {
                $this->u_model->setSecondaryPermissions($id_user);
            } else {
                $user_default_perms = $this->user_auth->get_default_perms($access_level_name);
                $this->u_model->insert_default_perms($id_company, $id_user, $user_default_perms);
            }

            // 2. insert employee data
            if ($id_user) {
                $employee['id_user'] = $id_user;
                $employee_inserted = $this->e_model->insert_profile($employee);

                if (!$employee_inserted) {
                    trigger_error('Cannot insert employee data', E_USER_ERROR);
                }
            }

            // 3. insert job types and job function 

            if ($id_user) {

                $job_types = array();

                if (!empty($data['job_type'])) {
                    foreach ($data['job_type'] as $id_job) {
                        $job_types[] = array(
                            'id_job' => $id_job,
                            'id_user' => $id_user
                        );
                    }

                    $this->e_model->insert_job_types($job_types);
                }
            }



            // 4. insert attachment request | files with descriptions.

            if ($id_user) {

                $credentials = array();

                if (!isset($data['credentials_info_select'])) {
                    $data['credentials_info_select'] = 'later';
                }

                if ($data['credentials_info_select'] == 'now') {
                    if (is_array($data['attachment_description'])) {

                        foreach ($data['attachment_description'] as $index => $at_description) {
                            if ($at_description != '' and isset($_FILES['resume']['name'][$index]) and $data['attachment_file_name'][$index] != '') {

                                // Upload to CDN
                                // Generate a new name for file
                                $n = basename($_FILES['resume']['name'][$index]);
                                $x = pathinfo($n, PATHINFO_EXTENSION);
                                $new_file_name = md5($n) . '.' . $x;

                                $uri = $this->cdn_api->upload_file(
                                    array(
                                        'file_name' => $new_file_name,
                                        'file_path' => $_FILES['resume']['tmp_name'][$index]
                                ));

                                $credentials[] = array(
                                    'file_name' => $new_file_name,
                                    'file_uri' => $uri,
                                    'description' => $at_description,
                                    'type' => 1,
                                    'id_user' => $id_user,
                                    'id_uploader_user' => $id_admin,
                                    'file_title' => $data['attachment_file_name'][$index]
                                );
                            }
                        }
                    }
                }

                $this->cdn_api->close();

                if ($data['credentials_info_select'] == 'request') {
                    if (is_array($data['attachment_description'])) {
                        foreach ($data['attachment_description'] as $index => $at_description) {
                            if ($at_description != '' and $data['attachment_file_name'][$index] != '') {

                                $credentials[] = array(
                                    'file_name' => '',
                                    'file_uri' => '',
                                    'description' => $at_description,
                                    'type' => 0,
                                    'id_user' => $id_user,
                                    'id_uploader_user' => $id_admin,
                                    'file_title' => $data['attachment_file_name'][$index]
                                );

                                $date_due = date('Y-m-d', strtotime($data['attachment_description_request_date'][$index - 1]));

                                $tasks[] = array(
                                    'id_user_from' => $this->user_auth->get('id_user'),
                                    'id_user_to' => NULL,
                                    'id_module' => 1,
                                    'subject' => $at_description,
                                    'status' => 0,
                                    'date_will_start' => date('Y-m-d'),
                                    'date_will_end' => $date_due
                                );
                                /*
                                  $perms_temp[] = array(
                                  'id_user' => NULL,
                                  'id_creator' => $this->user_auth->get('id_user'),
                                  'date_expire' => $date_due,
                                  'id_module' => 1,
                                  'date_created' => date('Y-m-d'),
                                  'id_company' => $id_company,
                                  'id_perm' => EDIT_CREDENTIALS
                                  );
                                 */
                            }
                        }
                    }
                }

                if ($data['credentials_info_select'] == 'later') {
                    // do nothing...
                }

                if (!empty($credentials)) {
                    $this->e_model->insert_credentials($credentials);
                }

                if (!$error_messages) {
                    $success_message = 'Employee account created successfully.';
                }

                // Send reset password email.
                if (!$error_messages and ( $user['status'] == USER_STATUS_ACTIVE or $user['status'] == USER_STATUS_PROBATION_ACCESS)) {

                    $this->load->library('mailgun_api');

                    //$sender = 'TalentBase';
                    $sender = ucwords($this->user_auth->get('company_name'));

                    //$reset_password_url = site_url('user/reset_password') . '/' . $id_user . '/' . sha1($user['email'] . $id_user);

                    $ret = $this->u_model->fetch_account(array('id_user' => $id_user));
                    $ret = $ret[0];

                    $reset_password_url = site_url('user/reset_password') . '/' . $ret->id_string . '/' . sha1($user['email'] . $ret->id_string);

                    $bdata = array(
                        'full_name' => $user['first_name'] . ' ' . $user['last_name'],
                        'reset_password_url' => $reset_password_url,
                        'email' => $user['email'],
                        'company_name' => $this->user_auth->get('company_name'),
                        'company_url' => site_url(),
                    );

                    $msg = $this->load->view('email_templates/user_reset_password_after_register', $bdata, true);

                    $this->mailgun_api->send_mail(
                        array(
                            'from' => $sender . ' <' . TALENTBASE_SUPPORT_EMAIL . '>',
                            'to' => $user['email'],
                            'subject' => 'Access Granted: ' . $this->user_auth->get('company_name') . ' Employee Portal',
                            'html' => '<html>' . $msg . '</html>'
                        )
                    );
                } // End reset password email	 
                // Inserting tasks
                // Add tasks only if user status is Active or Probation/Access
                if ($data['employment_status'] == USER_STATUS_ACTIVE or $data['employment_status'] == USER_STATUS_PROBATION_ACCESS) {

                    if (!empty($tasks)) {
                        $t = array();
                        foreach ($tasks as $task) {
                            $task['id_user_to'] = $id_user;
                            $t[] = $task;
                        }
                        $this->t_model->add_task($t);
                    }

                    if (!empty($perms_temp)) {
                        $p = array();
                        foreach ($perms_temp as $perm) {
                            $perm['id_user'] = $id_user;
                            $p[] = $perm;
                        }
                        $this->u_model->insert_perm_temp($p);
                    }
                } // End status	 

                $this->u_model->trans_complete();
            } // End if id_user
            // Done.
        } // END post request
        // 2 = CEO
        // 1 = MANAGER
        // 0 = None
        $CEO = $this->ref->fetch_all_records('employees', array('id_company' => $id_company, 'is_manager' => 2));

        if (!$CEO) {
            $allow_ceo = true;
        } else {
            $allow_ceo = false;
        }

        $data = array(
            'success_message' => $success_message,
            'error_messages' => $error_messages,
            'countries' => $this->ref->fetch_all_records('countries'),
            'departments' => $this->ref->fetch_all_records('departments', array('id_company' => $id_company)),
            'roles' => $this->ref->fetch_all_records('roles', array('id_company' => $id_company)),
            'levels' => $this->ref->fetch_all_records('company_levels', array('id_company' => $id_company)),
            'payGrades' => $this->ref->fetch_all_records('payroll_paygrade', array('id_company' => $id_company)),
            'job_types' => $this->ref->fetch_all_records('job_types'),
            'managers' => $this->e_model->fetch_managers($id_company),
            'accounts' => $this->ref->fetch_all_records('accounts'),
            'banks' => $this->ref->fetch_all_records('banks'),
            'account_types' => $this->account_types,
            'company_locations' => $this->ref->fetch_all_records('company_locations', array('id_company' => $id_company)),
            'pfa' => $this->ref->fetch_all_records('pension_fund_administrators'),
            'allow_ceo' => $allow_ceo,
            'select_access_level' => $select_access_level
        );

        $buffer = $this->load->view('user/employee/create_account', $data, true);

        $this->user_nav->run_page($buffer, 'TalentBase - Add new Employee', false);
    }

// End func create_account

    
    public function show_dashboardp5($companies) {

        $c_arr = array();
        $this->load->model('user/user_model', 'usr_model');
        $this->load->model('tasks/tasks_model');

        /*
          $role = $this->usr_model->get_user_role_in_company($this->user_auth->get('id_user'), $this->user_auth->get('id_company'));

          if(empty($role)) {
          $r = 'N/A';
          } else {
          $r = $role[0]->roles;
          }
         */

//        if ($this->user_auth->get('account_type') == USER_TYPE_ADMIN) {
//            $r = 'Admin';
//        } else {
//            $r = 'Employee';
//        }

        if ($this->user_auth->get('access_level') == ACCOUNT_TYPE_ADMINISTRATOR) {
            $r = 'Admin';
        } else if ($this->user_auth->get('access_level') == ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR) {
            $r = 'Secondary Admin';
        } else if ($this->user_auth->get('access_level') == ACCOUNT_TYPE_UNIT_MANAGER) {
            $r = 'Unit Manager';
        } else {
            $r = 'Employee';
        }

        $c = new stdClass();
        $c->id_company = $this->user_auth->get('id_company');
        $c->company_name = $this->user_auth->get('company_name');
        $c->c_id_string = $this->user_auth->get('company_id_string');
        $c->u_id_string = $this->user_auth->get('id_string');
        $c->id_user = $this->user_auth->get('id_user');

        $c_arr[] = array(
            'role' => $r,
            'company' => $c,
            'tasks' => $this->tasks_model->get_user_tasks_count($c->id_user)
        );

        if (!empty($companies)) {
            foreach ($companies as $c) {


                /*
                  $role = $this->usr_model->get_user_role_in_company($c->id_user, $c->id_company);

                  if(empty($role)) {
                  $r = 'N/A';
                  } else {
                  $r = $role[0]->roles;
                  }
                 */

//                if ($c->account_type == USER_TYPE_ADMIN) {
//                    $r = 'Admin';
//                } else {
//                    $r = 'Employee';
//                }

                if ($c->id_access_level == ACCOUNT_TYPE_ADMINISTRATOR) {
                    $r = 'Admin';
                } else if ($c->id_access_level == ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR) {
                    $r = 'Secondary Admin';
                } else if ($c->id_access_level == ACCOUNT_TYPE_UNIT_MANAGER) {
                    $r = 'Unit Manager';
                } else {
                    $r = 'Employee';
                }

                $c_arr[] = array(
                    'role' => $r,
                    'company' => $c,
                    'tasks' => $this->tasks_model->get_user_tasks_count($c->id_user)
                );
            }
        }

        $data = array(
            'companies' => $c_arr
        );

        $buffer = $this->load->view('user/multicompany_dashboard', $data, true);

        $this->user_nav->run_page($buffer, 'Dashboard', false, false, null, false);
    }

// End func show_dashboardp5

    /**
     * Add Employee Note
     * 
     * @access public
     * @return void
     */
    public function add_note() {

        $this->user_auth->check_login();

        $id_user = $this->input->post('id_user');
        $note = trim($this->input->post('note'));
        $who = $this->input->post('who_sees');
        $priority = $this->input->post('priority');

        if ($note == '') {
            echo 'Please compose note to be sent.';
            return;
        }

        $data = array(
            'note' => $note,
            'date_added' => date('Y-m-d H:i:s'),
            'id_user' => $id_user,
            'id_company' => $this->user_auth->get('id_company'),
            'id_creator' => $this->user_auth->get('id_user'),
            'access' => $who,
            'creator_access_level' => $this->user_auth->get('account_type')
        );

        $this->load->model('user/employee_model', 'e_model');
        $this->e_model->insert_employee_note($data);

        if ($priority) {

            if ($who == 1) {
                $send_to = $this->e_model->get_company_admin($this->user_auth->get('id_company'));
            } else if ($who == 2) {
                $send_to = $this->e_model->get_company_admin_manager($this->user_auth->get('id_company'), $id_user);
            } else if ($who == 3) {
                $send_to = $this->e_model->get_company_admin_manager_user($this->user_auth->get('id_company'), $id_user);
            }

            for ($s = 0; $s < count($send_to); $s++) {

                $first_name = $this->user_auth->get('display_name');
                $last_name = $this->user_auth->get('last_name');
                $company_string = $this->user_auth->get('company_id_string');

                // BUILD USER FULL NAME
                $sender = substr($this->user_auth->get('display_name'), 0, 1) . '.' . $this->user_auth->get('last_name');

                $message = "You've just received a high-priority HR notification from ." . $sender
                    . ". Please check your email or log-in to " . $company_string . ".talentbase.ng to view the note.";

                // Clear phone number of special characters such as ( _ - )
                $str = str_replace(array('(', ')', ' ', '-'), '', $send_to[$s]['work_number']);

                $params = array(
                    'id_module' => 1,
                    'message' => $message,
                    'recipients' => array(
                        'id_user' => $send_to[$s]['id_user'],
                        'number' => '+' . $str
                    ),
                    'sender' => $this->user_auth->get('company_name')
                );

                $this->sms_lib->send_sms($params);

                $subject = "Priority note from " . ucwords($first_name . " " . $last_name);

                $k = array(
                    'manager_name' => ucwords($send_to[$s]['first_name'] . " " . $send_to[$s]['last_name']),
                    'note' => $note,
                    'company_name' => $this->user_auth->get('company_name')
                );
                $msg = $this->load->view('email_templates/priority_note', $k, true);

                $this->load->library('mailgun_api');

                $this->mailgun_api->send_mail(
                    array(
                        'from' => $this->user_auth->get('company_name') . ' <noreply@testbase.com.ng>',
                        'to' => $send_to[$s]['email'],
                        'subject' => $subject,
                        'html' => '<html>' . $msg . '</html>'
                ));
            }

            echo 'Note created and user(s) notified via email and sms';
            return;
        }

        echo 'Note created successfully.';
        return;
    }

// End func add_note

    private function send_mail(array $params) {

        if (empty($params)) {
            return false;
        }

        $this->load->library('mailgun_api');

        $subject = "Priority note from " . ucwords($params['first_name'] . " " . $params['last_name']);

        $k = array(
            'manager_name' => ucwords($params['first_name'] . " " . $params['last_name']),
            'note' => $params['note'],
            'company_name' => $this->user_auth->get('company_name')
        );

        $msg = $this->load->view('email_templates/priority_note', $k, true);

        $ret = $this->mailgun_api->send_mail(
            array(
                'from' => $this->user_auth->get('company_name') . ' <noreply@talentbase.ng>',
                'to' => $params['email'],
                'subject' => $subject,
                'html' => '<html>' . $msg . '</html>'
        ));

        return $ret;
    }

// End func send_mail

    private function send_sms(array $data) {

        if (empty($data)) {
            return false;
        }

        // Clear phone number of special characters such as ( _ - )
        $str = str_replace(array('(', ')', ' ', '-'), '', $data['work_number']);

        $params = array(
            'id_module' => $data['id_module'],
            'message' => $data['message'],
            'recipients' => array(
                'id_user' => $data['id_user'],
                'number' => '+' . $str
            ),
            'sender' => $this->user_auth->get('company_name')
        );

        $ret = $this->sms_lib->send_sms($params);

        return $ret['type'];
    }

// End func send_sms

    /**
     * Update Employee Note
     * 
     * @access public
     * @return void
     */
    public function update_note() {

        $this->user_auth->check_login();

        $id_user = $this->input->post('id_user', true);
        $note = $this->input->post('note', true);
        $who = $this->input->post('who_sees', true);
        $priority = $this->input->post('priority', true);
        $id_company = $this->user_auth->get('id_company', true);
        $id_note = $this->input->post('note_id', true);

        $data = array(
            'note' => $note,
            'access' => $who
        );

        $this->load->model('user/employee_model', 'e_model');

        $where = array('id_employee_note' => $id_note);
        $q = $this->e_model->update_employee_note($data, $where);

        if ($q) {
            $em = $this->e_model->get_employee($id_user, $id_company);

            // Create a connection
            $this->cdn_api->connect(
                array(
                    'username' => RACKSPACE_USERNAME,
                    'api_key' => RACKSPACE_API_KEY,
                    'container' => $this->user_auth->get('cdn_container')
                )
            );

            if ($em->profile_picture_name != '') {
                $profile_pic_url = $this->cdn_api->temporary_url($em->profile_picture_name);
            } else {
                $profile_pic_url = '/img/profile-default.jpg';
            }

            $data['id_user'] = $em->id_user;
            $data['first_name'] = $em->first_name;
            $data['last_name'] = $em->last_name;
            $data['department'] = $em->departments;
            $data['role'] = $em->roles;
            $data['profile_picture_name'] = $em->profile_picture_name;
            $data['date_added'] = '-';

            echo json_encode($data);
        }
    }

// End func add_no

    /**
     * Manager View
     * 
     * @access public
     * @return void
     */
    public function manager_view($id_location = null) {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(MANAGER_VIEW);

        // Load model
        $this->load->model('user/employee_model', 'e_model');

        $ret = true;

        if ($id_location != null) {
            $ret = $this->e_model->is_location_head(
                array(
                    'id_head' => $this->user_auth->get('id_user'),
                    'id_location' => $id_location,
                    'id_company' => $this->user_auth->get('id_company')
            ));
        }

        if ($ret == false) {
            redirect('employee/manager_view');
            return;
        }

        // Load library
        $this->load->library('cdn_api');
        // Create a connection
        $this->cdn_api->connect(
            array(
                'username' => RACKSPACE_USERNAME,
                'api_key' => RACKSPACE_API_KEY,
                'container' => $this->user_auth->get('cdn_container')
            )
        );

        //Check if manager
        $id_user = $this->user_auth->get('id_user');
        $id_company = $this->user_auth->get('id_company');
        $is_manager = $this->e_model->is_manager($id_user, $id_company);

        if (!$is_manager) {
            redirect('employee/view_staff_directory', 'refresh');
            return;
        }

        //get employee under manager
        $data['employees'] = $this->e_model->get_manager_employees($id_user, $id_company);
        $data['statuses'] = $this->user_auth->statuses();
        $data['m'] = $this->e_model->get_company_admin($this->user_auth->get('id_company'));

        // Employees by location
        $data['locations'] = $this->e_model->get_manager_employees_location(
            array(
                'id_user' => $id_user,
                'id_company' => $id_company,
                'id_location' => $id_location
        ));
        $data['id_location'] = $id_location;
        $data['is_head'] = $this->e_model->is_location_head(
            array(
                'id_head' => $this->user_auth->get('id_user'),
                'id_company' => $id_company
        ));

        $buffer = $this->load->view('user/employee/manager_view', $data, true);
        $this->user_nav->run_page($buffer, 'TalentBase - Manager\' View', false);
    }

// End func manager_view

    /**
     * Manager View
     * 
     * @access public
     * @return void
     */
    public function manager_view_records() {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(MANAGER_VIEW);

        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('user/user_model', 'u_model');

        // Create a connection
        $this->cdn_api->connect(
            array(
                'username' => RACKSPACE_USERNAME,
                'api_key' => RACKSPACE_API_KEY,
                'container' => $this->user_auth->get('cdn_container')
            )
        );

        //check if manager
        $id_user = $this->user_auth->get('id_user');
        $id_company = $this->user_auth->get('id_company');
        $this->load->model('user/employee_model', 'e_model');
        $is_manager = $this->e_model->is_manager($id_user, $id_company);

        if (!$is_manager) {
            redirect('employee/manager_view', 'refresh');
            return;
        }

        $employee = $this->e_model->fetch_profile(array('id_user' => $id_user, 'id_company' => $id_company));

        $id_company = $this->user_auth->get('id_company');
        $id_user_string = $this->uri->segment(3);

        $profile = $this->e_model->fetch_global(array('u.id_string' => $id_user_string, 'u.id_company' => $id_company));

        if (!$profile) {
            show_404('page', false);
        }

        $profile = $profile[0];

        if ($id_user !== $profile->id_manager) {
            redirect('employee/manager_view', 'refresh');
            return;
        }

        $user = $this->u_model->fetch_account(array('id_user' => $profile->id_user, 'id_company' => $id_company));

        if (!$user) {
            show_404('page', false);
        }

        $user = $user[0];

        $requests = $this->u_model->fetch_perms_temp(array('id_company' => $id_company, 'id_user' => $user->id_user));

        // Profile tab
        // Work info
        $job_type = '';
        if ($profile->emp_job_types != '') {
            $job_types = $this->e_model->fetch_job_types($profile->id_user);
            if (!empty($job_types)) {
                foreach ($job_types as $j) {
                    $job_type .= $j->name . ', ';
                }
                $job_type = rtrim($job_type, ', ');
            }
        }

        if ($profile->employment_date != '' and $profile->employment_date != '0000-00-00 00:00:00') {
            $employment_date = date('m/d/Y', strtotime($profile->employment_date));
        } else {
            $employment_date = '';
        }

        $statuses = $this->user_auth->statuses();
        $emp_status = $statuses[$profile->status];

        if ($profile->status == USER_STATUS_PROBATION_ACCESS or $profile->status == USER_STATUS_PROBATION_NO_ACCESS) {
            $emp_status .= ' - ' . date('m/d/Y', strtotime($profile->date_probation_expire));
        }

        $wi = array(
            'Company name' => $this->user_auth->get('company_name'),
            'Work email' => $profile->email,
            'Work Number' => $profile->work_number,
            'Department' => $profile->departments,
            'Role' => $profile->roles,
            'Job Type' => $job_type,
            'Company location' => $profile->location,
            'Manager' => $profile->manager_first_name . ' ' . $profile->manager_last_name,
            'Employment date' => $employment_date,
            'Access level' => $profile->access_level_name,
            'Employment Status' => $emp_status
        );

        $pi = array(
            'Gender' => $this->genders[$profile->id_gender],
            'Marital Status' => $this->marital[$profile->id_marital_status],
            'Birth Date' => date('m/d/Y', strtotime($profile->dob)),
            'Country of Origin' => $profile->country_origin,
            'Address' => $profile->address,
            'City' => $profile->city,
            'State' => $profile->state,
            'Country' => $profile->country
        );

        // Resume / Credentials
        $rc = $this->e_model->fetch_credentials($profile->id_user);


        if ($profile->status == USER_STATUS_ACTIVE or $profile->status == USER_STATUS_PROBATION_ACCESS) {
            $send_request = true;
        } else {
            $send_request = false;
        }

        // Emergency contacts
        $e_contacts = array(
            1 => array(
                'First name' => $user->emergency_first_name,
                'Last name' => $user->emergency_last_name,
                'Address' => $user->emergency_address,
                'Email' => $user->emergency_email,
                'Phone' => $user->emergency_phone_number,
                'Employer' => $user->emergency_employer,
                'Work address' => $user->emergency_work_address
            ),
            2 => array(
                'First name' => $user->emergency_first_name2,
                'Last name' => $user->emergency_last_name2,
                'Address' => $user->emergency_address2,
                'Email' => $user->emergency_email2,
                'Phone' => $user->emergency_phone_number2,
                'Employer' => $user->emergency_employer2,
                'Work address' => $user->emergency_work_address2
            )
        );

        $profile_content = $this->load->view('user/employee/profile_page_contents/m_profile', array(
            'wi' => $wi,
            'pi' => $pi,
            ), true);


        $emergency_content = $this->load->view('user/employee/profile_page_contents/m_emergency_contacts', array(
            'e_contacts' => $e_contacts,
            ), true);


        $notes_data['notes'] = $this->e_model->fetch_notes($profile->id_company, $profile->id_user, $this->user_auth->get('id_user'));
        $notes_data['notes_id_user'] = $profile->id_user;
        $internal_notes_content = $this->load->view('user/employee/profile_page_contents/internal_notes', $notes_data, true);


        $resume_content = $this->load->view('user/employee/profile_page_contents/m_document_upload', array(
            'uploads' => $rc,
            'id_user' => $profile->id_user,
            ), true);

        $success_message = '';
        $error_messages = '';
        $request_message = '';

        $data = array(
            'success_message' => $success_message,
            'error_messages' => $error_messages,
            'profile' => $profile,
            // Containers
            'profile_content' => $profile_content,
            'emergency_content' => $emergency_content,
            'internal_notes_content' => $internal_notes_content,
            'resume_content' => $resume_content,
            'send_request' => $send_request,
            'request_message' => $request_message
        );

        $buffer = $this->load->view('user/employee/manager_view_profile', $data, true);

        $this->user_nav->run_page($buffer, 'TalentBase - Staff records / Profile', false);
    }

// End func manager_view

    /**
     * SEND COMPOSED SMS TO EMPLOYEE -- CHUKS
     * 
     * @access public
     * @return void 
     */
    public function send_sms_to_employee() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $data = $this->input->post();

            $this->load->model('user/employee_model', 'e_model');

            // REPLACE CHARACTERS WITH BLANK SPACE.
            $str = str_replace(array('(', ')', ' ', '-'), '', $data['phone']);

            $code = $this->e_model->employee_phone_code(array(
                'id_user' => $data['id_user'],
                'id_company' => $this->user_auth->get('id_company')
            ));

            // CALL SMS LIB.
            $this->load->library('sms_lib');

            $sender = substr($this->user_auth->get('display_name'), 0, 1) . '.' . $this->user_auth->get('last_name');

            $params = array(
                'id_module' => 2,
                'message' => $data['message'] . ' ' . $sender,
                'recipients' => array(
                    'id_user' => $data['id_user'],
                    'number' => '+' . $code . $str
                ),
                'sender' => $this->user_auth->get('company_name')
            );

            $ret = $this->sms_lib->send_sms($params);

            echo $ret['msg'];
        }
    }

// End func send_sms_to employee

    public function manager_upload_document() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $data = $this->input->post();

            if (empty($data)) {
                $this->session->set_userdata('errors', 'Error! Ensure all fields are filled and document attached');
                redirect(site_url('employee/manager_view'));
                return;
            }

            // Call Lib.
            $this->load->library('docs_upload');

            $ret = $this->docs_upload->upload_docs(
                array(
                    'file' => array(
                        'name' => $_FILES["file"]["name"],
                        'type' => $_FILES["file"]["type"],
                        'size' => $_FILES["file"]["size"],
                        'path' => $_FILES["file"]["tmp_name"],
                    ),
                    'name' => $data['file_name']
            ));

            if ($ret['status'] == 200) {

                $this->load->model('user/employee_model', 'e_model');

                $res = $this->e_model->insert_employee_credentials(
                    array(
                        'id_user' => $data['id_user'],
                        'type' => 1,
                        'description' => $data['file_description'],
                        'file_name' => $ret['info']['rackspace_file_name'],
                        'id_uploader_user' => $this->user_auth->get('id_user'),
                        'file_title' => $data['file_name'],
                        'container' => $this->user_auth->get('cdn_container')
                ));

                if ($res > 0) {
                    $this->session->set_userdata('confirm', 'Document upload successful');
                    redirect(site_url('employee/manager_view'));
                } else {
                    $this->session->set_userdata('errors', 'Error! Upload completed but failed to post upload details to database.');
                    redirect(site_url('employee/manager_view'));
                }
            } else {
                redirect(site_url('employee/manager_view'));
            }
        } else {
            redirect(site_url('employee/manager_view'));
        }
    }

// End function upload_document

    public function employee_analytics() {

        $this->user_auth->check_login();
        $this->load->model('user/employee_model', 'e_model');

        $where = array('id_company' => $this->user_auth->get('id_company'));

        $data = array(
            'departments' => $this->e_model->get_where('departments', $where),
            'locations' => $this->e_model->get_where('company_locations', $where),
            'emp_count' => $this->e_model->get_total_emps(),
            'report' => $this->e_model->reporting_data($this->user_auth->get('id_company')),
            'statuses' => $this->user_auth->statuses(),
        );

        $buffer = $this->load->view('user/employee/analytics', $data, true);
        $this->user_nav->run_page($buffer, 'Analytics', false);
    }

// End func employee_analytics

    /*     * ************************* Start: Tohir ************************************************     */

    public function view_terminated_staff_directory() {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(VIEW_STAFF_DIRECTORY);

        $this->load->library('cdn_api');

        // Create a connection
        $this->cdn_api->connect(
            array(
                'username' => RACKSPACE_USERNAME,
                'api_key' => RACKSPACE_API_KEY,
                'container' => $this->user_auth->get('cdn_container')
            )
        );

        $this->load->model('reference_model', 'ref');
        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');

        $id_company = $this->user_auth->get('id_company');

        $success_message = '';
        $error_messages = '';

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $data = $this->input->post();
            $current_statuses = $data['id_statuses'];
            if (empty($current_statuses)) {
                $current_statuses = NULL;
            }
        } else {
            $current_statuses = array(
                USER_STATUS_TERMINATED
            );
        }

        $profiles = $this->e_model->fetch_global(array('id_company' => $id_company), $current_statuses);

        $job_types_tmp = $this->ref->fetch_all_records('job_types');
        $job_types = array();

        if (!empty($job_types_tmp)) {
            foreach ($job_types_tmp as $jt) {
                $job_types[$jt->id_type] = $jt->name;
            }
        }

        if ($this->user_auth->have_perm(VIEW_EMPLOYEE_RECORDS)) {
            $view_records = true;
        } else {
            $view_records = false;
        }

        if ($this->user_auth->have_perm(CHANGE_EMPLOYEE_STATUS)) {
            $change_status = true;
        } else {
            $change_status = false;
        }

        if (!$this->user_auth->have_perm(VIEW_EMPLOYEE_RECORDS) and ! $this->user_auth->is_admin() and ! $this->user_auth->have_perm(CHANGE_EMPLOYEE_STATUS) and ! $this->user_auth->have_perm(RESET_EMPLOYEE_PASSWORD)) {
            $actions = false;
        } else {
            $actions = true;
        }

        $data = array(
            'success_message' => $success_message,
            'error_messages' => $error_messages,
            'employee_count' => count($profiles),
            'profiles' => $profiles,
            'job_types' => $job_types,
            'statuses' => $this->user_auth->statuses(),
            'current_statuses' => $current_statuses,
            'view_records' => $view_records,
            'change_status' => $change_status,
            'actions' => $actions,
            'statuses' => $this->user_auth->statuses()
        );

        $buffer = $this->load->view('user/employee/staff_directory_archive', $data, true);

        $this->user_nav->run_page($buffer, 'TalentBase - Staff records / Directory', false);
    }

// End func view_terminated_staff_directory

    public function view_staff_directory_json() {

        $this->load->model('reference_model', 'ref');
        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('payroll/payroll_model', 'p_model');

        $this->load->library('cdn_api');

        // Create a connection
        $this->cdn_api->connect(
            array(
                'username' => RACKSPACE_USERNAME,
                'api_key' => RACKSPACE_API_KEY,
                'container' => $this->user_auth->get('cdn_container')
            )
        );

        $this->p_model->logit('Start ' . __FUNCTION__ . '()', 'Ajax Main');

        $id_company = $this->user_auth->get('id_company');

        if (!$this->uri->segment(4) or ( $this->uri->segment(4) < 0)) {
            $current_statuses = array(
                USER_STATUS_INACTIVE,
                USER_STATUS_ACTIVE,
                USER_STATUS_ACTIVE_NO_ACCESS,
                USER_STATUS_PROBATION_ACCESS,
                USER_STATUS_PROBATION_NO_ACCESS,
                USER_STATUS_SUSPENDED,
            );
        } else {
            $current_statuses = array($this->uri->segment(4));
        }


        $job_types_tmp = $this->ref->fetch_all_records('job_types');
        $job_types = array();

        if (!empty($job_types_tmp)) {
            foreach ($job_types_tmp as $jt) {
                $job_types[$jt->id_type] = $jt->name;
            }
        }

        $sta = $this->user_auth->statuses();

        $profiles = $this->e_model->fetch_global(array('id_company' => $id_company), $current_statuses);
        // var_dump($profiles);
        if (empty($profiles)) {

            $result = array();

            $package[] = $result;

            $b = array('aaData' => $package);

            echo json_encode($b);

            return;
        }

        foreach ($profiles as $p) {

            $name_link = strtolower(trim(preg_replace('/[^A-Z0-9]+/i', '-', $p->first_name), '-')) . '-' . strtolower(trim(preg_replace('/[^A-Z0-9]+/i', '-', $p->last_name), '-'));
            $is_valid_email = 0;

            $result = array();

            $result['id_user'] = $p->id_user;
            $result['id_string'] = $p->id_string;
            $result['first_name'] = trim($p->first_name);
            $result['last_name'] = trim($p->last_name);
            $result['email'] = '';
            $result['status'] = $sta[$p->status];
            $result['status_id'] = $p->status;

            // Check if email is valid
            $arr = explode('.', $p->email);
            if (count($arr) > 1) {
                if (strtolower($arr[1]) != 'user@talentbase') {
                    $is_valid_email = 1;
                    $result['email'] = $p->email;
                }
            }


            $result['is_valid_email'] = $is_valid_email;

            //  Job type
            if ($p->emp_job_types != '') {
                $job_types_str = '';

                $ujts = explode(',', $p->emp_job_types);
                foreach ($ujts as $ujt) {
                    if (isset($job_types[$ujt])) {
                        $job_types_str .= $job_types[$ujt] . ', ';
                    }
                }

                $job_types_str = rtrim($job_types_str, ', ');
            } else {
                $job_types_str = 'N/A';
            }

            $result['job_type'] = $job_types_str;

            //  Profile pic
            $profile_pic_src = '';
            if ($p->profile_picture_name != '') {
                $profile_pic_src = $this->cdn_api->temporary_url($p->profile_picture_name);
            }

            if ($profile_pic_src == '') {
                $profile_pic_src = '/img/profile-default.jpg';
            }

            $result['profile_pic'] = $profile_pic_src;


            $result['view_record_url'] = site_url("employee/view_records/" . $p->id_string . '/' . $name_link);
            $result['location'] = $p->location ? $p->location : 'N/A';
            $result['department'] = $p->departments ? $p->departments : 'N/A';
            $result['roles'] = $p->roles ? $p->roles : 'N/A';
            $result['id_country_code'] = $p->id_country_code;
            $result['work_number'] = $this->user_auth->get_employee_phone_number($p->id_user); // $result['work_number'] = $this->e_model->fetch_employee_primary_number( $p->id_user, EMPLOYEE_PRIMARY_PHONE);
            $result['address'] = $p->address ? $p->address : 'N/A';

            $package[] = $result;
        }

        $b = array('aaData' => $package);
        $this->p_model->logit('End ' . __FUNCTION__ . '()', 'Ajax Main');

        echo json_encode($b);
    }

// End func view_staff_directory_json

    public function emp_head_count_json() {

        $this->user_auth->check_login();
        $this->load->model('user/employee_model', 'e_model');

        $department = $this->uri->segment(3);
        $location = $this->uri->segment(4);
        $yy = $this->uri->segment(5);

        $res = '';
        $res2 = '';
        $sum = $this->e_model->get_employess_uptill($yy . '-' . '01-01 00:00:00', $department, $location);

        for ($m = 1; $m <= 12; $m++) {


            $bb = array();
            $bb = $this->e_model->get_newly_hired($yy . '-' . $m . '-01 00:00:00', $yy . '-' . $m . '-30 00:00:00', $department, $location);
            $terminated = $this->e_model->get_total_terminated_emps($yy . '-' . $m . '-01 00:00:00', $yy . '-' . $m . '-30 00:00:00', $department, $location);

            if (!empty($bb)) {

                foreach ($bb as $b) {
                    $sum += $b->users;
                }

                $sum = $sum - $terminated;
                $res .= '[' . ($m - 1) . ',' . $sum . ']' . ',';
            } else if ($m <= date('m') - 1) {
                $res .= '[' . ($m - 1) . ',' . $sum . ']' . ',';
            } else {
                $res2 .= '[' . ($m - 1) . ',' . $sum . ']' . ',';
            }
        }
        $res = '[' . rtrim($res, ',') . ']';
        $res2 = '[' . rtrim($res2, ',') . ']';


        echo '[' . $res . ',' . $res2 . ']';
    }

// End func emp_head_count_json

    public function new_hires_json() {

        $this->user_auth->check_login();
        $this->load->model('user/employee_model', 'e_model');

        $department = $this->uri->segment(3);
        $location = $this->uri->segment(4);
        $yy = $this->uri->segment(5);

        $res = '';
        $res2 = '';

        for ($m = 1; $m <= 12; $m++) {
            $sum = 0;

            $bb = array();
            $bb = $this->e_model->get_newly_hired($yy . '-' . $m . '-01 00:00:00', $yy . '-' . $m . '-30 00:00:00', $department, $location);

            if (!empty($bb)) {
                foreach ($bb as $b) {
                    $sum += $b->users;
                }
                $res .= '[' . ($m - 1) . ',' . $sum . ']' . ',';
            } else if ($m <= date('m') - 1) {
                $res .= '[' . ($m - 1) . ',' . 0 . ']' . ',';
            } else {
                $res2 .= '[' . ($m - 1) . ',' . 0 . ']' . ',';
            }
        }
        $res = '[' . rtrim($res, ',') . ']';
        $res2 = '[' . rtrim($res2, ',') . ']';


        echo '[' . $res . ',' . $res2 . ']';
    }

// End func new_hires_json

    public function emp_retention_json() {

        $this->user_auth->check_login();
        $this->load->model('user/employee_model', 'e_model');

        $department = $this->uri->segment(3);
        $location = $this->uri->segment(4);
        $yy = $this->uri->segment(5);
        $emp_count = $this->e_model->get_total_emps();

        $res = '';
        $res2 = '';

        for ($m = 1; $m <= 12; $m++) {

            $terminated = $this->e_model->get_total_terminated_emps($yy . '-' . $m . '-01 00:00:00', $yy . '-' . $m . '-30 00:00:00', $department, $location);

            $f = ( ($emp_count - $terminated) / $emp_count) * 100;

            if ($m >= date('m') - 1) {
                $res2 .= '[' . ($m - 1) . ',' . $f . ']' . ',';
            } else {
                $res .= '[' . ($m - 1) . ',' . $f . ']' . ',';
            }
        }   // End m

        $res = '[' . rtrim($res, ',') . ']';
        $res2 = '[' . rtrim($res2, ',') . ']';


        echo '[' . $res . ',' . $res2 . ']';
    }

// End func emp_retention_json

    public function gender_diversity_json() {

        $this->user_auth->check_login();
        $this->load->model('user/employee_model', 'e_model');

        $department = $this->uri->segment(3);
        $location = $this->uri->segment(4);
        $yy = $this->uri->segment(5);
        $emp_count = $this->e_model->get_total_emps();

        $res = '';
        $str_m = '';
        $str_f = '';
        $sum = 0;
        for ($m = 1; $m <= 12; $m++) {

            $bb = array();
            $bb = $this->e_model->get_newly_hired($yy . '-' . $m . '-01 00:00:00', $yy . '-' . $m . '-30 00:00:00', $department, $location);
            $terminated = $this->e_model->get_total_terminated_emps($yy . '-' . $m . '-01 00:00:00', $yy . '-' . $m . '-30 00:00:00', $department, $location);

            if (!empty($bb)) {

                foreach ($bb as $b) {
                    $sum += $b->users;
                }

                $sum = $sum - $terminated;
            }

            //  If employee count = 0
            if ($sum == 0) {

                $m_percent = $sum;
                $f_percent = $sum;
            } else {

                $males = $this->e_model->get_emps_by_gender($yy . '-' . $m . '-01 00:00:00', $yy . '-' . $m . '-30 00:00:00', $department, $location, '1');
                $female = $this->e_model->get_emps_by_gender($yy . '-' . $m . '-01 00:00:00', $yy . '-' . $m . '-30 00:00:00', $department, $location, '2');

                $m_percent = number_format((($males / $sum) * 100), 0);
                $f_percent = 100 - $m_percent;
                //$f_percent = number_format( (($female/$sum) * 100) ,0);
                //$res .= '['. ($m-1) .',' . 100 . ']' .',' ;
            }

            $str_m = $str_m .= '[' . ($m - 1) . ',' . $m_percent . ']' . ',';
            $str_f = $str_f .= '[' . ($m - 1) . ',' . $f_percent . ']' . ',';
        }

        //$res = rtrim($res, ',');
        $str_m = rtrim($str_m, ',');
        $str_f = rtrim($str_f, ',');


        // echo '[ ['.$res. '] , ['.$str_m. '] ,['.$str_f. ']  ]';
        echo '[ [' . $str_m . '] ,[' . $str_f . ']  ]';
    }

// End func gender_diversity_json

    public function edit_bank_detail($id_string, $ref_id) {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(EDIT_REQUEST_EMPLOYEE_RECORDS);

        if ($ref_id == '') {
            show_404('page', false);
        }

        $id_company = $this->user_auth->get('id_company');
        $id_string = $this->uri->segment(3);

        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('reference_model', 'ref');

        $record = $this->u_model->fetch_account(array('id_string' => $id_string, 'id_company' => $id_company));

        if (!$record) {
            show_404('page', false);
        }

        $record = $record[0];

        $employee = $this->e_model->fetch_profile(array('id_user' => $record->id_user, 'id_company' => $id_company));

        if (!$employee) {
            show_404('page', false);
        }

        $employee = $employee[0];

        $success_message = '';
        $error_messages = '';

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $default = 0;
            if ($this->input->post('default_acct') != '') {
                $this->e_model->update_bank_details(array('default' => $default), array('id_user' => $record->id_user, 'id_company' => $id_company));
                $default = 1;
            }

            $profile = array(
                'bank_account_name' => $this->input->post('account_name'),
                'id_bank' => $this->input->post('bank'),
                'id_bank_account_type' => $this->input->post('account_type'),
                'bank_account_number' => $this->input->post('account_number'),
                'default' => $default,
            );

            // UPDATE RECORDS

            $this->e_model->update_bank_details($profile, array('ref_id' => $ref_id, 'id_company' => $id_company));


            redirect(site_url('employee/view_records/' . $id_string . '/' . $ref_id . '#bank_details_container'));
        } // End POST

        $data = array(
            'success_message' => $success_message,
            'error_messages' => $error_messages,
            'profile' => $employee,
            'banks' => $this->ref->fetch_all_records('banks'),
            'account_types' => $this->account_types,
            'bank_details' => $this->e_model->fetch_bank_details_by_ref_id($ref_id)
        );

        $this->load->view('user/employee/edit_bank_details', $data);
    }

// End func edit_bank_detail

    private function _insert_phone_number($id_user, $work_number, $id_country_code, $phone_type, $primary) {

        $this->load->model('user/employee_model', 'e_model');

        $this->e_model->insert_data(
            array(
                'table' => 'employee_phone',
                'vals' => array(
                    'id_user' => $id_user,
                    'id_company' => $this->user_auth->get('id_company'),
                    'id_phone_type' => $phone_type,
                    'phone_number' => $work_number,
                    'primary' => $primary,
                    'id_country_code' => $id_country_code,
                    'ref_id' => strtoupper(random_string('alnum', 10)),
                )
            )
        );
    }

// End func. insert_phone_number

    public function migrate_phones() {

        $this->load->model('reference_model', 'ref');
        $this->load->model('user/employee_model', 'e_model');

        $emps = $this->ref->fetch_all_records('employees', null);
        $data_db = array();
        $data_bank = array();

        foreach ($emps as $e) {

            $data_db[] = array(
                'id_user' => $e->id_user,
                'id_phone_type' => WORK_PHONE_TYPE,
                'phone_number' => $e->work_number,
                'id_country_code' => $e->id_country_code,
                'id_company' => $e->id_company,
                'ref_id' => strtoupper(random_string('alnum', 10)),
            );

            if ($e->bank_account_number != '') {
                $data_bank[] = array(
                    'id_user' => $e->id_user,
                    'id_bank' => $e->id_bank,
                    'bank_account_name' => $e->bank_account_name,
                    'bank_account_number' => $e->bank_account_number,
                    'id_bank_account_type' => $e->id_bank_account_type,
                    'id_company' => $e->id_company,
                    'default' => 1,
                    'ref_id' => strtoupper(random_string('alnum', 10)),
                );
            }
        }   // end employee

        if (!empty($data_bank)) {
            $this->e_model->insert_migrated_data('employee_bank_details', $data_bank);
        }
        $this->e_model->insert_migrated_data('employee_phone', $data_db);

        echo "success";
    }

// End migrate_phones

    public function process_bulk_action() {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(CHANGE_EMPLOYEE_STATUS);

        $todo = $this->input->post('todo', true);
        $id_strs = $this->input->post('emp', true);

        $users = array(explode("-", $id_strs));
        $users = $users[0];

        $this->load->model('user/user_model', 'u_model');

        $i = $this->u_model->fetch_user_role_dept_details(
            array(
                'id_user' => $this->user_auth->get('id_user'),
                'id_company' => $this->user_auth->get('id_company')
        ));

        foreach ($users as $id_user) {

            if ($todo == 'activate') {
                $status = USER_STATUS_ACTIVE;
            } else if ($todo == 'deactivate') {
                $status = USER_STATUS_INACTIVE;
            }

            // Get user record for id_string
            $user_record = $this->u_model->fetch_account(array('id_user' => $id_user));
            $id_string = $user_record[0]->id_string;
            $email = $user_record[0]->email;

            $data = array(
                'status' => $status,
                'account_blocked' => 0
            );

            $this->u_model->save(
                $data, array(
                'id_user' => $id_user,
                'email' => $email
                )
            );

            if ($status == USER_STATUS_ACTIVE) {

                $sender = $this->user_auth->get('company_name');

                $reset_password_url = site_url('user/reset_password') . '/' . $id_string . '/' . sha1($email . $id_string);

                $bdata = array(
                    'full_name' => $user_record[0]->first_name . ' ' . $user_record[0]->last_name,
                    'reset_password_url' => $reset_password_url,
                    'email' => $email,
                    'company_name' => $this->user_auth->get('company_name'),
                    'company_url' => 'https://' . $this->user_auth->get('company_id_string') . '.talentbase.ng/login',
                    'sender_full_name' => $this->user_auth->get('display_name') . ' ' . $this->user_auth->get('last_name'),
                    'role' => $i->roles,
                    'dept' => $i->departments
                );

                //$msg = $this->load->view('email_templates/user_account_status_changed', $bdata, true);
                $msg = $this->load->view('email_templates/user_reset_password_after_register', $bdata, true);

                // Load libr.
                $this->load->library('mailgun_api');
                $this->mailgun_api->send_mail(
                    array(
                        'from' => $sender . ' <' . TALENTBASE_SUPPORT_EMAIL . '>',
                        'to' => $email,
                        'bcc' => 'helpdesk@talentbase.ng',
                        'subject' => 'Access Granted: ' . $this->user_auth->get('company_name') . ' Employee Portal',
                        'html' => '<html>' . $msg . '</html>'
                    )
                );
            }   // End if status = active
        }   // End foreach
    }

// End func process_bulk_action

    public function reset_bulk_password() {

        $todo = $this->input->post('todo', true);
        $id_strs = $this->input->post('emp', true);

        $users = array(explode("-", $id_strs));
        $users = $users[0];

        $this->load->library('mailgun_api');
        $sender = 'TalentBase Help Desk';

        $this->load->model('user/user_model', 'u_model');

        $i = $this->u_model->fetch_user_role_dept_details(
            array(
                'id_user' => $this->user_auth->get('id_user'),
                'id_company' => $this->user_auth->get('id_company')
        ));



        if (!empty($users)) {
            foreach ($users as $id_user) {

                $user_record = $this->u_model->fetch_account(array('id_user' => $id_user));
                $id_string = $user_record[0]->id_string;
                $email = $user_record[0]->email;
                $full_name = $user_record[0]->first_name . ' ' . $user_record[0]->last_name;

                $reset_password_url = site_url('user/reset_password') . '/' . $id_string . '/' . sha1($email . $id_string);

                $bdata = array(
                    'full_name' => $full_name,
                    'reset_password_url' => $reset_password_url,
                    'email' => $email,
                    'company_name' => $this->user_auth->get('company_name'),
                    'company_url' => 'https://' . $this->user_auth->get('company_id_string') . '.talentbase.ng/login',
                    'role' => $i->roles,
                    'dept' => $i->departments,
                    'sender_full_name' => $this->user_auth->get('display_name') . ' ' . $this->user_auth->get('last_name')
                );

                $msg = $this->load->view('email_templates/admin_reset_employee_password', $bdata, true);

                $this->mailgun_api->send_mail(
                    array(
                        'from' => $sender . ' <' . TALENTBASE_SUPPORT_EMAIL . '>',
                        'to' => $email,
                        'bcc' => 'helpdesk@talentbase.ng',
                        'subject' => 'Reset Your Password (' . $this->user_auth->get('company_name') . ') Staff Portal',
                        'html' => '<html>' . $msg . '</html>'
                    )
                );
            }
        }   // End !empty($users)
    }

// End func reset_bulk_password

    public function download_excel() {

        $this->user_auth->check_login();
        $this->load->model('user/employee_model', 'e_model');

        $column_data = array();
        $query = $this->e_model->reporting_data($this->user_auth->get('id_company'));
        $column_data[] = array(
            'Fname',
            'Lname',
            'Department',
            'Location',
            'Work Email',
            'Work Number',
            'Birthday',
            'Home Address',
            'Personal Number',
            'Emergency 1',
            'Emergency 2',
            'Available Documents',
            'Bank Account',
            'Pension details'
        );

        for ($i = 0; $i < count($query); $i++) {
            $column_data[] = array($query[$i]['first_name'],
                $query[$i]['last_name'],
                $query[$i]['id_string'],
                $query[$i]['location_tag'],
                strip_tags($query[$i]['work_email']),
                strip_tags($query[$i]['work_phone']),
                strip_tags($query[$i]['birthday']),
                strip_tags($query[$i]['home_address']),
                strip_tags($query[$i]['personal_phone']),
                strip_tags($query[$i]['emergency1']),
                strip_tags($query[$i]['emergency2']),
                $query[$i]['uploaded_docs'] . ' of ' . $query[$i]['documents'],
                strip_tags($query[$i]['bank_account']),
                strip_tags($query[$i]['pension_details']));
        }


        $column_count = sizeof($column_data[0]);

        $download_name = $this->user_auth->get('company_name') . "_PersonnelRecordAudit_" . date('dMY');

        $this->load->library('csv_helper');
        $this->csv_helper->personnel_audit_record($column_data, $column_count, $download_name);

        redirect(site_url('employee/employee_analytics'), 'refresh');
    }

// end func download_excel

    public function personnel_report() {

        $this->user_auth->check_login();
        $this->load->model('user/employee_model', 'e_model');

        $data = array(
            'report' => $this->e_model->reporting_data($this->user_auth->get('id_company')),
            'statuses' => $this->user_auth->statuses(),
            'request_message' => ''
        );

        $buffer = $this->load->view('user/employee/personnel_report', $data, true);
        $this->user_nav->run_page($buffer, 'Analytics', false);
    }

// end func. personnel_report

    public function send_my_request() {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(EDIT_REQUEST_EMPLOYEE_RECORDS);

        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('payroll/payroll_model', 'p_model');

        $due_date = $this->input->post('due_date');
        $employeeStatuses = $this->input->post('values');

        foreach ($employeeStatuses as $empStatus) {

            $request = [];
            list ($id_string, $request['work_phone'], $request['birthday'], $request['home_address'], $request['personal_phone'], $request['emergency1'], $request['emergency2'], $request['bank_account'], $request['pension_details']) = explode('|', $empStatus);
            $data = array_filter($request, function($v) {
                return !is_numeric($v) || intval($v) === 0;
            });

            $this->employee_info_request->sendRequest($id_string, $due_date, array_keys($data));
        }   // end foreach
    }

// end func. send_my_request

    private function _send_request_notification($msg, $email) {
        return $this->employee_info_request->send_request_notification($msg, $email);
    }

// end func. _send_request_notification

    public function send_request() {

        if ($this->input->server('REQUEST_METHOD') != 'POST') {
            show_404('page', false);
        }

        $data = $this->input->post();

        $user = null;
        if ($this->employee_info_request->sendRequest($data['id_string'], $data['request_due_date'], array_keys($data), $user)) {
            flash_message_create('Your request for employee information was sent successfully', 'success');
        } else {
            flash_message_create('We could not send your request, ensure you selected at least on field to request', 'error');
        }

        $tag = preg_replace('/[^A-Za-z0-9\-]+/', '-', strtolower($user->first_name . '-' . $user->last_name));
        $nextUrl = $this->input->server('HTTP_REFERER') ? : site_url('employee/view_records/' . $data['id_string'] . '/' . $tag);
        redirect($nextUrl);
    }

// end func. _insert_temp_request

    public function settings() {

        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('payroll/payroll_model', 'p_model');
        $this->user_auth->check_perm('employee:settings');

        if ($this->input->post()) {
            $where = array('id_company' => $this->user_auth->get('id_company'), 'id_user' => $this->input->post('id_user'));

            if (!$this->p_model->check('employee_data_approver', $where)) {

                $this->p_model->add('employee_data_approver', $where);
            }

            return;
        }


        $this->id_company = $this->user_auth->get('id_company');

        $admin = $this->p_model->get_where('users', array('account_type' => ACCOUNT_TYPE_ADMINISTRATOR, 'id_company' => $this->id_company));

        $approvers = $this->e_model->access_approver($this->id_company);

        if (empty($approvers)) {
            $this->p_model->add('employee_data_approver', array('id_company' => $this->id_company, 'id_user' => $admin[0]->id_user));
        }

        $data = array(
            'approvers' => $this->e_model->access_approver($this->id_company),
            'approver_workflows' => $this->p_model->fetch_approvers_from_table('employee_workflow_approvers', array('id_company' => $this->id_company)),
            'employees' => $this->p_model->get_where('users', array('id_company' => $this->id_company, 'status' => USER_STATUS_ACTIVE OR USER_STATUS_PROBATION_ACCESS)),
            'approver_steps' => $this->approver_steps,
        );

        $this->user_nav->run_page_with_view('user/employee/settings', $data, 'Settings - Employee Directory');
    }

// end func. settings

    public function edit_data_approver() {

        $this->load->library('form_validation');
        $this->load->model('payroll/payroll_model', 'p_model');
        $this->user_auth->check_perm('employee:settings');

        $this->form_validation->set_rules('employees4', 'Required', 'required|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_userdata('errors', 'Error! Please select employees');
            redirect('employee/settings', 'refresh');
            return;
        }

        $this->p_model->remove('employee_data_approver', array('id_company' => $this->user_auth->get('id_company')));

        $employee = $this->input->post('employees4', true);
        foreach ($employee as $em) {
            $this->p_model->add('employee_data_approver', array('id_user' => $em,
                'id_company' => $this->user_auth->get('id_company'),
            ));
        }

        $this->session->set_userdata('confirm', 'Operation was successful');
        redirect(site_url('employee/settings'), 'refresh');
    }

    public function pending_approval() {
        $this->user_auth->check_login();

        $this->load->model('user/employee_model', 'e_model');

        $data = array(
            'pending_approvals' => $this->e_model->fetch_pending_approvals(),
            'requests' => $this->employee_info_request->getRequestTypeNames(),
        );

        $this->user_nav->run_page_with_view('user/employee/pending_approval', $data, 'Pending Approvals - Employee Directory');
    }

// end func pending_approval

    public function request_details($id_temp_request) {

        $this->user_auth->check_login();
        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('reference_model', 'ref');

        if ($id_temp_request == '') {
            show_404();
        }

        $data = array(
            'pending_approvals' => $this->e_model->fetch_pending_approvals_details($id_temp_request),
            'requests' => $this->employee_info_request->getRequestTypeNames(),
            'request_types' => $this->employee_info_request->getRequestTypes(),
            'banks' => $this->ref->fetch_all_records('banks'),
            'states' => $this->ref->fetch_all_records('states'),
            'gender' => $this->genders,
            'marital' => $this->marital,
            'pfa' => $this->ref->fetch_all_records('pension_fund_administrators'),
            'account_types' => $this->account_types,
            'countries' => $this->ref->fetch_all_records('countries'),
            'entries' => $this->e_model->get_where('employee_request_entries', array(
                'id_temp_request' => $id_temp_request,
                'id_company' => $this->user_auth->get('id_company')
            ))
        );

        $this->user_nav->run_page_with_view('user/employee/request_details', $data, 'Request Details - Employee Directory');
    }

// end func. request_details

    public function decline_request() {
        $this->user_auth->check_login();
        $data = $this->input->post();

        if ($data['request_type'] != '' or $data['id_temp_request'] != '') {

            $this->load->model('payroll/payroll_model', 'p_model');
            $this->load->model('user/employee_model', 'e_model');

            $set = array('status' => EMPLOYEE_REQUEST_DECLINED);
            $q = $this->p_model->update('employee_temp_request_task', $set, array(
                'request_type' => $data['request_type'],
                'id_temp_request' => $data['id_temp_request'],
            ));

            //notify employee via email
            $this->e_model->sendDeclineEmail(array(
                'id_temp_request' => $data['id_temp_request'],
                'reason' => $data['reason'],
                'requestSubject' => $this->employee_info_request->getRequestTypeNames()[$data['request_type']],
            ));

            // delete entries made by employee
            $this->p_model->delete('employee_request_entries', array(
                'request_type' => $data['request_type'],
                'id_temp_request' => $data['id_temp_request'],
            ));

            if ($q) {
                echo '<h3 align="center">Employee data declined successfully</h3>';
            } else {
                echo '<h3 align="center">Sorry, we are unable to complete your request at the moment. Pls try again later</h3>';
            }
        }
    }

    public function rework_request() {
        $data = $this->input->post();

        if ($data['request_type'] != '' or $data['id_temp_request'] != '') {

            $this->load->model('payroll/payroll_model', 'p_model');
            $this->load->model('user/employee_model', 'e_model');

            $set = array('status' => EMPLOYEE_REQUEST_PENDING);
            $q = $this->p_model->update('employee_temp_request_task', $set, array(
                'request_type' => $data['request_type'],
                'id_temp_request' => $data['id_temp_request'],
            ));

            //notify employee via email
            $this->e_model->sendDeclineEmail(array(
                'id_temp_request' => $data['id_temp_request'],
                'reason' => $data['reason'],
                'requestSubject' => $this->employee_info_request->getRequestTypeNames()[$data['request_type']],
            ));

            // delete entries made by employee
            $this->p_model->delete('employee_request_entries', array(
                'request_type' => $data['request_type'],
                'id_temp_request' => $data['id_temp_request'],
            ));

            if ($q) {
                echo '<h3 align="center">Employee data declined successfully</h3>';
            } else {
                echo '<h3 align="center">Sorry, we are unable to complete your request at the moment. Pls try again later</h3>';
            }
        }
    }

    public function approve_request() {
        $this->user_auth->check_login();
        $data = $this->input->post();

        if ($data['request_type'] != '' or $data['id_temp_request'] != '') {

            $this->load->model('payroll/payroll_model', 'p_model');
            $set = array('status' => EMPLOYEE_REQUEST_APPROVED);
            $q = $this->p_model->update('employee_temp_request_task', $set, array(
                'request_type' => $data['request_type'],
                'id_temp_request' => $data['id_temp_request'],
            ));

            // fetch filled entries
            $where = array(
                'request_type' => $data['request_type'],
                'id_temp_request' => $data['id_temp_request'],
            );
            $entries = $this->p_model->get('employee_request_entries', $where);

            $chk = array(
                'id_user' => $entries[0]->id_user,
                'id_company' => $entries[0]->id_company,
            );

            $table_name = substr($entries[0]->field_name, 0, strpos($entries[0]->field_name, '.'));

            $d_ar = array();
            $d_ar['id_user'] = $entries[0]->id_user;
            $d_ar['id_company'] = $entries[0]->id_company;

            foreach ($entries as $ent) {
                $field_ = substr($ent->field_name, strpos($ent->field_name, '.') + 1, strlen($ent->field_name));
                $d_ar[$field_] = $ent->field_value;
            }

            // if record doesn't exist, then add new
            if (!$this->p_model->check($table_name, $chk)) {
                $this->p_model->add($table_name, $d_ar);
            }

            // update record in table
            $this->p_model->update($table_name, $d_ar, array(
                'id_user' => $ent->id_user,
                'id_company' => $ent->id_company,
            ));

            // delete entries made by employee
            $this->p_model->delete('employee_request_entries', $where);


            if ($q) {
                echo '<h4 align="center">Employee data approved successfully</h4>';
            } else {
                echo '<h4 align="center">Sorry, we are unable to complete your request at the moment. Pls try again later</h4>';
            }
        }
    }

    /*
     * Employee bulk upload
     * 
     */

    public function upload_excel() {
        $data = array();

        $this->user_nav->run_page_with_view('user/employee/upload/upload_excel', $data, 'Bulk Upload - Employee Directory');
    }

    public function process_upload() {

        $this->load->model('organizations/organizations_model', 'o_model');
        $error_redirect = 'employee/upload/excel';

        $res = $this->o_model->processUpload($this->user_auth->get('id_company'), $error_redirect, $this->input->post());

        $dat = array(
            'id_company' => $this->user_auth->get('id_company'),
            'res' => $res,
            'colCount' => count($res[2]) - 1,
            'mapping_cols' => $this->o_model->get_mapping_cols(),
            'redirect_url' => 'employee/view_staff_directory',
            'cancel_redirect_url' => site_url('employee/upload/excel'),
        );

        $this->user_nav->run_page_with_view('common/upload_mapping', $dat, 'Bulk Upload - Employee Directory');
    }

    public function process_workflow_approver() {
        $this->load->model('payroll/payroll_model', 'p_model');
        $data_db = array(
            'id_user' => $this->input->post('id_user'),
            'id_company' => $this->user_auth->get('id_company')
        );

        if ($this->p_model->check('employee_workflow_approvers', $data_db)) {
            $this->session->set_userdata('errors', 'Approver already exist');
            return;
        }

        $this->p_model->add('employee_workflow_approvers', $data_db);
    }

    public function remove_approver($id_workflow_approver) {
        $this->load->model('payroll/payroll_model', 'p_model');

        if ($id_workflow_approver === '') {
            $this->session->set_userdata('errors', 'Unable to complete your request at moment');
            redirect(site_url('employee/settings'), refresh);
        }

        $this->p_model->delete('employee_workflow_approvers', array('id_workflow_approver' => $id_workflow_approver));
        $this->session->set_userdata('confirm', 'Approver removed successfully');
        redirect(site_url('employee/settings'), refresh);
    }

    /*     * **************************** End: Tohir************************************************************** */

    public function search() {
        $this->user_auth->check_login();
        $results = [];
        if (($term = $this->input->get('term')) && strlen(trim($term)) > 2) {
            $fields = ['first_name', 'last_name', 'middle_name'];
            $term = $this->u_model->db->escape_like_str($term);
            $likes = [];
            foreach ($fields as $fld) {
                $likes[] = "{$fld} LIKE '%{$term}%'";
            }
            $str = '(' . join(' OR ', $likes) . ')';
            $this->u_model->db->where($str);
        }

        $results = $this->u_model->fetch_account_info($this->user_auth->get('id_company'), [], 5000, 0, \employee\UserBasicInfo::class);


        $this->output->set_content_type('application/json');
        $this->output->_display(json_encode($results));
    }

    public function bulkActivateEmployee() {

        $users = explode(',', $this->input->post('id_users'));
        $this->load->model('user/employee_model', 'e_model');

        foreach ($users as $u) {

            $has_valid_email = FALSE;

            if ($u != '') {

                $userInfo = $this->e_model->get_employee($u, $this->user_auth->get('id_company'));

                $arr = explode('.', $userInfo->email);
                if (count($arr) > 1) {
                    if (strtolower($arr[1]) != 'user@talentbase') {
                        $has_valid_email = TRUE;
                    }
                }

                if ($has_valid_email) {
                    $this->e_model->activate_account($userInfo->first_name . ' ' . $userInfo->last_name, $userInfo->id_string, $userInfo->email);
                }
            }
        }
        $this->session->set_userdata('confirm', 'Employee(s) account activated successfully');
        redirect(site_url('employee/view_staff_directory'));
    }

}

// End class Employee