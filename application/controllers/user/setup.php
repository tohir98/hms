<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Talentbase
 * Employee controller
 * 
 * @category   Controller
 * @package    Users
 * @subpackage Employee
 * @author     Tohir Omoloye. <thohiru.omoloye@talentbase.ng>
 * @copyright  Copyright (c) 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * 
 * @property Locations_model $l_model Description
 * @property  Departments_model $d_model Departments Model
 * @property Employee_roles_model $er_model Description
 * @property Payroll_model $p_model payroll_model
 * @property Company_level_model $cl_model Description
 * 
 * @since      File available since Release 1.0.0
 */
class setup extends CI_Controller {
    //put your code here
    const MODULE_ID = 1;
    
    public function __construct() {

        parent::__construct();

        /*
         * Loaded automatically in app.
         * 
         * lib - user_auth, session
         * helper - url
         */
        
        $this->load->model('departments/departments_model', 'd_model');
        $this->load->model('locations/locations_model', 'l_model');
        $this->load->model('reference_model', 'ref');
        $this->load->model('user/employee_model', 'e_model');
        $this->load->library(['user_nav', 'bugsnag', 'form_validation']);
        $this->load->helper(['payroll_helper', 'form', 'string', 'notification_helper']);

        $this->id_company = $this->user_auth->get('id_company');
        $this->id_user = $this->user_auth->get('id_user');
        $this->company_name = $this->user_auth->get('company_name');
        $this->company_id_string = $this->user_auth->get('company_id_string');
    }
    
    public function run_setup($step = 'step1', $status = 0) { // 0 is temporary

        if ($step == 'step1') {
            $this->_run_setup1($status);
        }

        if ($step == 'step2') {
            $this->_run_setup2($status);
        }

        if ($step == 'step3') {
            $this->_run_setup3($status);
        }

        if ($step == 'step4') {
            $this->_run_setup4($status);
        }
        
        if ($step == 'step5') {
            $this->_run_setup5($status);
        }
    }
    
     public function _run_setup1() {
         
         $this->user_auth->check_login();
        
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $dat = $this->input->post();
            
            $this->l_model->insert_locations($dat);

            redirect(site_url('setup/run_setup/step2'), refresh);
            
        }
        
        $data = array(
            'locations' => $this->l_model->fetch_all_by_id_comapny($this->id_company),
            'heads' => $this->e_model->fetch_managers($this->id_company),
            'countries' => $this->ref->fetch_all_records('countries'),
        );

        $page_content = $this->load->view('user/setup/step1', $data, true);
        $this->user_nav->run_setup_page($page_content, 'Setup - Step 1 of 4', false);
    }
    
     public function _run_setup2() {
         
         $this->user_auth->check_login();
        
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $dat = $this->input->post();
            
            $this->d_model->insert_departments($dat);

            redirect(site_url('setup/run_setup/step3'), refresh);
            
        }
        
        $data = array(
            'depts' => $this->d_model->fetch_all_by_id_comapny($this->id_company),
            'heads' => $this->e_model->fetch_managers($this->id_company),
        );

        $page_content = $this->load->view('user/setup/step2', $data, true);
        $this->user_nav->run_setup_page($page_content, 'Setup - Step 2 of 4', false);
    }
    
     public function _run_setup3() {
         
         $this->user_auth->check_login();
         
         $this->load->model('employee_roles/employee_roles_model', 'er_model');
        
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $dat = $this->input->post();
            
            $this->er_model->insert_roles($dat, $_FILES["roles"]);
            
            update_module_setup_step(self::MODULE_ID, 4, MODULE_SETUP_ACTIVE);

            redirect(site_url('employee'), refresh);
            
        }
        
        
        $data = array(
            'roles' => $this->er_model->fetch_all_by_id_comapny($this->id_company),
            'job_functions' => $this->ref->fetch_all_records('job_functions')
        );

        $page_content = $this->load->view('user/setup/step3', $data, true);
        $this->user_nav->run_setup_page($page_content, 'Setup - Step 3 of 4', false);
    }
    
     public function _run_setup4() {
         
         $this->user_auth->check_login();
         
         $this->load->model('employee_roles/company_level_model', 'cl_model');
        
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $dat = $this->input->post();
            
            $this->er_model->insert_roles($dat, $_FILES["roles"]);
            
            update_module_setup_step(self::MODULE_ID, 4, MODULE_SETUP_ACTIVE);

            redirect(site_url('employee'), refresh);
            
        }
        
        
        $data = array(
            'levels' => $this->cl_model->fetch_all_levels($this->id_company)
        );

        $page_content = $this->load->view('user/setup/step4', $data, true);
        $this->user_nav->run_setup_page($page_content, 'Setup - Step 4 of 4', false);
    }
    
    public function restart_setup() {
        
        $this->load->model('payroll/payroll_model', 'p_model');
        
        $where = array(
            'id_module' => self::MODULE_ID,
            'id_company' => $this->user_auth->get('id_company')
        );

        $q = $this->p_model->delete('module_setup', $where);

        if ($q) {
            redirect(site_url('setup/run_setup/step1'), 'refesh');
        }
    }
    
    public function finishSetup() {
        update_module_setup_step(self::MODULE_ID, 3, MODULE_SETUP_ACTIVE);
        $data = [];
        $page_content = $this->load->view('user/setup/setupCompleted', $data, true);
        $this->user_nav->run_setup_page($page_content, 'Setup Completed', false);

//        $this->user_nav->run_page_with_view('user/setup/setupCompleted', $data, 'Setup Completed');
    }

}
