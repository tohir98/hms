<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Talentbase
 * Personal controller
 * 
 * @category   Controller
 * @package    Users
 * @subpackage Personal
 * @author     David A. <tokernel@gmail.com>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 * @property Tasks_model $t_model Task_Model
 * @property Employee_model $e_model employees model
 */
class Personal extends CI_Controller {

    private $account_types = array(
        1 => 'Savings',
        2 => 'Current',
        3 => 'Not Sure'
    );
    private $request_types = array(
        'work_email' => 1,
        'work_phone' => 2,
        'birthday' => 3,
        'home_address' => 4,
        'personal_phone' => 5,
        'emergency1' => 6,
        'emergency2' => 7,
        'uploaded_docs' => 8,
        'bank_account' => 9,
        'pension_details' => 10,
        'gender' => 11,
        'marital_status' => 12,
        'country' => 13,
        'city' => 14,
        'state' => 15,
        'nationality' => 16,
    );

    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();

        /*
         * Loaded automatically in app.
         * 
         * lib - user_auth, session
         * helper - url
         */

        $this->load->library('user_nav');
        $this->load->helper('user_nav', 'payroll_helper');

        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('reference_model', 'ref');
        $this->load->model('payroll/payroll_model', 'p_model');
        $this->load->model('tasks/tasks_model', 't_model');
    }

// End func __construct

    public function edit_my_profile_personal_info() {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(EDIT_MY_PROFILE_PERSONAL_INFO);

        $id_company = $this->user_auth->get('id_company');
        $id_user = $this->user_auth->get('id_user');

        $record = $this->u_model->fetch_account(array('id_user' => $id_user, 'id_company' => $id_company));

        if (!$record) {
            show_404('page', false);
        }

        $record = $record[0];

        $employee = $this->e_model->fetch_profile(array('id_user' => $record->id_user, 'id_company' => $id_company));

        if (!$employee) {
            show_404('page', false);
        }

        $employee = $employee[0];

        $success_message = '';
        $error_messages = '';

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            if ($this->input->post('dob') == '') {
                $dob = '';
            } else {
                $dob = date('m/d/Y', strtotime($this->input->post('dob')));
            }

            $id_state = $this->input->post('id_state');

            if ($id_state == '') {
                $id_state = 0;
            }

            $profile = array(
                'id_gender' => $this->input->post('id_gender'),
                'id_marital_status' => $this->input->post('id_marital_status'),
                'dob' => $dob,
                'id_nationality' => $this->input->post('id_nationality'),
                'address' => $this->input->post('address'),
                'city' => $this->input->post('city'),
                'id_country' => $this->input->post('country'),
                'id_state' => $id_state,
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name')
            );

            // UPDATE RECORDS

            if ($dob != '') {
                $dob = date('Y-m-d', strtotime($dob));
            }

            $user = array(
                'id_gender' => $this->input->post('id_gender'),
                'id_marital_status' => $this->input->post('id_marital_status'),
                'dob' => $dob,
                'id_nationality' => $this->input->post('id_nationality'),
                'address' => $this->input->post('address'),
                'city' => $this->input->post('city'),
                'id_country' => $this->input->post('country'),
                'id_state' => $id_state,
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name')
            );

            $this->u_model->save($user, array('id_user' => $record->id_user, 'id_company' => $id_company));
            $this->u_model->delete_perm_temp(array('id_user' => $record->id_user, 'id_perm' => EDIT_MY_PROFILE_PERSONAL_INFO));

            redirect(site_url('personal'));
        } else {

            if ($record->dob != '' and $record->dob != '0000-00-00 00:00:00') {
                $dob = date('m/d/Y', strtotime($record->dob));
            } else {
                $dob = '';
            }

            $profile = array(
                'id_gender' => $record->id_gender,
                'id_marital_status' => $record->id_marital_status,
                'dob' => $dob,
                'id_nationality' => $record->id_nationality,
                'address' => $record->address,
                'city' => $record->city,
                'id_country' => $record->id_country,
                'id_state' => $record->id_state,
                'first_name' => $record->first_name,
                'last_name' => $record->last_name
            );
        } // End POST

        $data = array(
            'success_message' => $success_message,
            'error_messages' => $error_messages,
            'profile' => $profile,
            'countries' => $this->ref->fetch_all_records('countries'),
        );

        $buffer = $this->load->view('user/personal/edit_profile_pers_info', $data, true);

        $this->user_nav->run_page($buffer, 'TalentBase - Edit Profile / Personal', false);
    }

// E nd func edit_my_profile_personal_info

    public function edit_my_profile_picture() {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(EDIT_MY_PROFILE_PICTURE);

        $id_company = $this->user_auth->get('id_company');
        $id_user = $this->user_auth->get('id_user');

        $record = $this->u_model->fetch_account(array('id_user' => $id_user, 'id_company' => $id_company));

        if (!$record) {
            show_404('page', false);
        }

        $record = $record[0];

        $employee = $this->e_model->fetch_profile(array('id_user' => $record->id_user, 'id_company' => $id_company));

        if (!$employee) {
            show_404('page', false);
        }

        $employee = $employee[0];

        $this->load->library('cdn_api');

        // Create a connection
        $this->cdn_api->connect(
            array(
                'username' => RACKSPACE_USERNAME,
                'api_key' => RACKSPACE_API_KEY,
                'container' => $this->user_auth->get('cdn_container')
            )
        );

        if ($record->profile_picture_name != '') {
            $picture_src = $this->cdn_api->temporary_url($record->profile_picture_name);
        } else {
            $picture_src = '';
        }

        $success_message = '';
        $error_messages = '';

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $user['profile_picture_name'] = '';
            $user['profile_picture_uri'] = '';

            if (isset($_FILES['profile_picture']['name'])) {

                // Generate a new name for file
                $n = basename($_FILES['profile_picture']['name']);
                $x = pathinfo($n, PATHINFO_EXTENSION);
                $new_file_name = md5($n) . '.' . $x;

                $picture_src = $this->cdn_api->upload_file(
                    array(
                        'file_name' => $new_file_name,
                        'file_path' => $_FILES['profile_picture']['tmp_name']
                    )
                );

                $user['profile_picture_name'] = $new_file_name;
                // $user['profile_picture_uri'] = $uri;
            }

            $this->u_model->save($user, array('id_user' => $record->id_user, 'id_company' => $id_company));

            $this->u_model->delete_perm_temp(array('id_user' => $record->id_user, 'id_perm' => EDIT_MY_PROFILE_PICTURE));

            redirect(site_url('personal'));
        } // End POST

        $data = array(
            'success_message' => $success_message,
            'error_messages' => $error_messages,
            'profile' => $record,
            'profile_picture_src' => $picture_src
        );

        $buffer = $this->load->view('user/personal/edit_profile_picture', $data, true);

        $this->user_nav->run_page($buffer, 'TalentBase - Edit Profile picture', false);
    }

// E nd func edit_my_profile_picture

    public function edit_my_emergency_contacts() {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(EDIT_MY_EMERGENCY_CONTACTS);

        $id_company = $this->user_auth->get('id_company');
        $id_user = $this->user_auth->get('id_user');

        $record = $this->u_model->fetch_account(array('id_user' => $id_user, 'id_company' => $id_company));

        if (!$record) {
            show_404('page', false);
        }

        $record = $record[0];

        $success_message = '';
        $error_messages = '';

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $user = array(
                'emergency_first_name' => $this->input->post('emergency_first_name'),
                'emergency_last_name' => $this->input->post('emergency_last_name'),
                'emergency_address' => $this->input->post('emergency_address'),
                'emergency_email' => $this->input->post('emergency_email'),
                'emergency_phone_number' => $this->input->post('emergency_phone_number'),
                'emergency_employer' => $this->input->post('emergency_employer'),
                'emergency_work_address' => $this->input->post('emergency_work_address'),
                'emergency_first_name2' => $this->input->post('emergency_first_name2'),
                'emergency_last_name2' => $this->input->post('emergency_last_name2'),
                'emergency_address2' => $this->input->post('emergency_address2'),
                'emergency_email2' => $this->input->post('emergency_email2'),
                'emergency_phone_number2' => $this->input->post('emergency_phone_number2'),
                'emergency_employer2' => $this->input->post('emergency_employer2'),
                'emergency_work_address2' => $this->input->post('emergency_work_address2')
            );

            $this->u_model->save($user, array('id_user' => $record->id_user, 'id_company' => $id_company));

            $this->u_model->delete_perm_temp(array('id_user' => $record->id_user, 'id_perm' => EDIT_MY_EMERGENCY_CONTACTS));

            redirect(site_url('personal'));
        } else {

            $profile = array(
                'emergency_first_name' => $record->emergency_first_name,
                'emergency_last_name' => $record->emergency_last_name,
                'emergency_address' => $record->emergency_address,
                'emergency_email' => $record->emergency_email,
                'emergency_phone_number' => $record->emergency_phone_number,
                'emergency_employer' => $record->emergency_employer,
                'emergency_work_address' => $record->emergency_work_address,
                'emergency_first_name2' => $record->emergency_first_name2,
                'emergency_last_name2' => $record->emergency_last_name2,
                'emergency_address2' => $record->emergency_address2,
                'emergency_email2' => $record->emergency_email2,
                'emergency_phone_number2' => $record->emergency_phone_number2,
                'emergency_employer2' => $record->emergency_employer2,
                'emergency_work_address2' => $record->emergency_work_address2,
            );
        } // End POST

        $data = array(
            'success_message' => $success_message,
            'error_messages' => $error_messages,
            'profile' => $profile
        );

        $this->load->view('user/personal/edit_emergency_contacts', $data);
    }

// E nd func edit_my_emergency_contacts

    public function edit_my_bank_details() {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(EDIT_MY_BANK_DETAILS);

        $id_company = $this->user_auth->get('id_company');
        $id_user = $this->user_auth->get('id_user');

        $record = $this->u_model->fetch_account(array('id_user' => $id_user, 'id_company' => $id_company));

        if (!$record) {
            show_404('page', false);
        }

        $record = $record[0];

        $employee = $this->e_model->fetch_profile(array('id_user' => $record->id_user, 'id_company' => $id_company));

        if (!$employee) {
            show_404('page', false);
        }

        $employee = $employee[0];

        $success_message = '';
        $error_messages = '';

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $profile = array(
                'bank_account_name' => $this->input->post('account_name'),
                'id_bank' => $this->input->post('bank'),
                'id_bank_account_type' => $this->input->post('account_type'),
                'bank_account_number' => $this->input->post('account_number')
            );

            // UPDATE RECORDS

            $this->e_model->update_profile($profile, array('id_user' => $record->id_user, 'id_company' => $id_company));

            $this->u_model->delete_perm_temp(array('id_user' => $record->id_user, 'id_perm' => EDIT_MY_BANK_DETAILS));

            redirect(site_url('personal'));
        } // End POST

        $data = array(
            'success_message' => $success_message,
            'error_messages' => $error_messages,
            'profile' => $employee,
            'banks' => $this->ref->fetch_all_records('banks'),
            'account_types' => $this->account_types
        );

        $buffer = $this->load->view('user/personal/edit_bank_details', $data, true);
        //$buffer = '';
        $this->user_nav->run_page($buffer, 'TalentBase - Edit Bank details', false);
    }

// E nd func edit_my_bank_details

    public function edit_my_benefits_details() {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(EDIT_MY_BENEFITS_DETAILS);

        $id_company = $this->user_auth->get('id_company');
        $id_user = $this->user_auth->get('id_user');

        $record = $this->u_model->fetch_account(array('id_user' => $id_user, 'id_company' => $id_company));

        if (!$record) {
            show_404('page', false);
        }

        $record = $record[0];

        $employee = $this->e_model->fetch_profile(array('id_user' => $record->id_user, 'id_company' => $id_company));

        if (!$employee) {
            show_404('page', false);
        }

        $employee = $employee[0];

        $success_message = '';
        $error_messages = '';

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $profile = array(
                'rsa_pin' => $this->input->post('rsa_pin'),
                'id_pfs' => $this->input->post('pfs')
            );

            // UPDATE RECORDS

            $this->u_model->save($profile, array('id_user' => $record->id_user, 'id_company' => $id_company));

            $this->u_model->delete_perm_temp(array('id_user' => $record->id_user, 'id_perm' => EDIT_MY_BENEFITS_DETAILS));

            redirect(site_url('personal'));
        } // End POST

        $data = array(
            'success_message' => $success_message,
            'error_messages' => $error_messages,
            'profile' => $record,
            'pfas' => $this->ref->fetch_all_records('pension_fund_administrators'),
        );

        $buffer = $this->load->view('user/personal/edit_benefits', $data, true);
        //$buffer = '';
        $this->user_nav->run_page($buffer, 'TalentBase - Edit Benefits details', false);
    }

// E nd func edit_my_benefits_details

    public function my_profile() {

        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VIEW_MY_PROFILE);

        $id_company = $this->user_auth->get('id_company');

        $success_message = '';
        $error_messages = '';

        $profile = $this->e_model->fetch_global(array('u.id_user' => $this->user_auth->get('id_user'), 'u.id_company' => $id_company));

        if (!$profile) {
            show_404('page', false);
        }

        $profile = $profile[0];

        $user = $this->u_model->fetch_account(array('id_user' => $this->user_auth->get('id_user'), 'id_company' => $id_company));

        if (!$user) {
            show_404('page', false);
        }

        $user = $user[0];

        $this->load->library('cdn_api');

        // Create a connection
        $this->cdn_api->connect(
            array(
                'username' => RACKSPACE_USERNAME,
                'api_key' => RACKSPACE_API_KEY,
                'container' => $this->user_auth->get('cdn_container')
            )
        );

        if ($user->profile_picture_name != '') {
            $_profile_picture_src = $this->cdn_api->temporary_url($user->profile_picture_name);
        } else {
            $_profile_picture_src = '';
        }

        // Profile tab
        // WORK INFO
        //if($this->user_auth->have_perm(VIEW_MY_PROFILE_WORK_INFO)) {

        $job_type = '';
        if ($profile->emp_job_types != '') {
            $job_types = $this->e_model->fetch_job_types($profile->id_user);
            if (!empty($job_types)) {
                foreach ($job_types as $j) {
                    $job_type .= $j->name . ', ';
                }
                $job_type = rtrim($job_type, ', ');
            }
        }

        if ($profile->employment_date != '' and $profile->employment_date != '0000-00-00 00:00:00') {
            $employment_date = date('m/d/Y', strtotime($profile->employment_date));
        } else {
            $employment_date = '';
        }

        $statuses = $this->user_auth->statuses();
        $emp_status = $statuses[$profile->status];

        if ($profile->status == USER_STATUS_PROBATION_ACCESS or $profile->status == USER_STATUS_PROBATION_NO_ACCESS) {
            $emp_status .= ' - ' . date('m/d/Y', strtotime($profile->date_probation_expire));
        }

        $wi = array(
            'Company name' => $this->user_auth->get('company_name'),
            'Work email' => $profile->email,
            'Work Number' => $profile->work_number,
            'Department' => $profile->departments,
            'Role' => $profile->roles,
            'Job Type' => $job_type,
            'Company location' => $profile->location,
            'Manager' => $profile->manager_first_name . ' ' . $profile->manager_last_name,
            'Employment date' => $employment_date,
            'Access level' => $profile->access_level_name,
            'Employment Status' => $emp_status
        );

//		} else {
//			$wi = array();
//		}
        //if($this->user_auth->have_perm(VIEW_MY_PROFILE_PERSONAL_INFO)) {
        // Personal info
        $genders = array(
            '' => 'N/A',
            0 => 'N/A',
            1 => 'Male',
            2 => 'Female'
        );

        $marital = array(
            '' => 'N/A',
            0 => 'N/A',
            1 => 'Single',
            2 => 'Married'
        );

        // PERSONAL INFO
        $pi = array(
            'Gender' => $genders[$profile->id_gender],
            'Marital Status' => $marital[$profile->id_marital_status],
            'Birth Date' => date('m/d/Y', strtotime($profile->dob)),
            'Country of Origin' => $profile->country_origin,
            'Address' => $profile->address,
            'City' => $profile->city,
            'State' => $profile->state,
            'Country' => $profile->country
        );

//		} else {
//			$pi = array();
//		}
        //if($this->user_auth->have_perm(VIEW_MY_EMERGENCY_CONTACTS)) {
        // Emergency contacts
        $e_contacts = array(
            1 => array(
                'First name' => $user->emergency_first_name,
                'Last name' => $user->emergency_last_name,
                'Address' => $user->emergency_address,
                'Email' => $user->emergency_email,
                'Phone' => $user->emergency_phone_number,
                'Employer' => $user->emergency_employer,
                'Work address' => $user->emergency_work_address
            ),
            2 => array(
                'First name' => $user->emergency_first_name2,
                'Last name' => $user->emergency_last_name2,
                'Address' => $user->emergency_address2,
                'Email' => $user->emergency_email2,
                'Phone' => $user->emergency_phone_number2,
                'Employer' => $user->emergency_employer2,
                'Work address' => $user->emergency_work_address2
            )
        );

//		} else {
//			$e_contacts = array();
//		}
        //if($this->user_auth->have_perm(VIEW_MY_BANK_DETAILS)) {

        $bank = $this->ref->fetch_all_records('banks', array('id_bank' => $profile->id_bank));
        $bank_name = '';

        if (!empty($bank)) {
            $bank = $bank[0];
            $bank_name = $bank->name;
        }

        $bank_account_type = '';

        if (isset($this->account_types[$profile->id_bank_account_type])) {
            $bank_account_type = $this->account_types[$profile->id_bank_account_type];
        }

        if ($profile->bank_account_name != '') {
            $bank_account_name = $profile->bank_account_name;
        } else {
            $bank_account_name = '';
        }

        if ($profile->bank_account_number != '' and $profile->bank_account_number != 0) {
            $bank_account_number = $profile->bank_account_number;
        } else {
            $bank_account_number = '';
        }

        $bank_details = array(
            'Account name' => $bank_account_name,
            'Bank' => $bank_name,
            'Account type' => $bank_account_type,
            'Account number' => $bank_account_number
        );

//		} else {
//			$bank_details = array();
//		}
        //if($this->user_auth->have_perm(VIEW_MY_BENEFITS_DETAILS)) {

        $pfa_name = '';

        $pfa = $this->ref->fetch_all_records('pension_fund_administrators', array('id_pension_fund_administrator' => $user->id_pfs));

        if ($pfa != '') {
            $pfa_name = $pfa[0]->name;
        }

        $benefits = array(
            'RSA PIN' => $user->rsa_pin,
            'Pension Fund Administrator' => $pfa_name
        );

//		} else {
//			$benefits = array();
//		}
        // ---- Load content views ----

        $profile_content = $this->load->view('user/personal/profile_page_contents/profile', array(
            'wi' => $wi,
            'pi' => $pi
            ), true);


        // ------ Resume credentiaals
        // Resume / Credentials
        $rc = array();
        $docs_menu = false;

        $docs_requests = $this->e_model->fetch_docs_requests($profile->id_user);

        if (!empty($docs_requests)) {
            $rc = array_merge($rc, $docs_requests);
            $docs_menu = true;
        }

        //if($this->user_auth->have_perm(EDIT_MY_CREDENTIALS) or $this->user_auth->have_perm(VIEW_MY_CREDENTIALS)) {
        if ($this->user_auth->have_perm(EDIT_MY_CREDENTIALS)) {

            $docs_uploaded = $this->e_model->fetch_docs_uploaded($profile->id_user);
            if (!empty($docs_uploaded)) {
                $rc = array_merge($rc, $docs_uploaded);
            }

            $view_edit_perms = true;
            $docs_menu = true;
        } else {
            $view_edit_perms = false;
        }

        if (!empty($rc) or $view_edit_perms == true) {

            $resume_content = $this->load->view('user/personal/profile_page_contents/resume_credentials', array(
                'uploads' => $rc,
                ), true);
        } else {
            $resume_content = '';
        }

        //if($this->user_auth->have_perm(VIEW_MY_EMERGENCY_CONTACTS)) {

        $emergency_content = $this->load->view('user/personal/profile_page_contents/emergency_contacts', array(
            'e_contacts' => $e_contacts
            ), true);

//		} else {
//			$emergency_content = '';
//		}
        //if($this->user_auth->have_perm(VIEW_MY_BANK_DETAILS)) {

        $bank_details_content = $this->load->view('user/personal/profile_page_contents/bank_details', array(
            'bank_details' => $bank_details
            ), true);

//		} else {
//			$bank_details_content = '';
//		}
        //if($this->user_auth->have_perm(VIEW_MY_BENEFITS_DETAILS)) {

        $benefits_details_content = $this->load->view('user/personal/profile_page_contents/benefits_details', array(
            'benefits' => $benefits
            ), true);

//		} else {
//			$benefits_details_content = '';
//		}

        $data = array(
            'success_message' => $success_message,
            'error_messages' => $error_messages,
            'profile' => $profile,
            // Containers
            'profile_content' => $profile_content,
            'resume_content' => $resume_content,
            'emergency_content' => $emergency_content,
            'bank_details_content' => $bank_details_content,
            'benefits_details_content' => $benefits_details_content,
            'profile_picture_src' => $_profile_picture_src,
            'docs_menu' => $docs_menu
        );

        $buffer = $this->load->view('user/personal/profile_page', $data, true);

        $this->user_nav->run_page($buffer, 'TalentBase - Personal / Profile', false);
    }

// End func my_profile

    public function edit_document($id_cred) {

        $this->user_auth->check_login();
        $this->user_auth->check_perm(EDIT_MY_CREDENTIALS);

        if ($id_cred == '') {
            show_404('page', false);
        }

        $doc = $this->e_model->fetch_document(array('id_cred' => $id_cred));

        if (!$doc) {
            show_404('page', false);
        }

        $doc = $doc[0];

        if ($doc->type != 1) {
            show_404('page', false);
        }

        $success_message = '';
        $error_messages = '';

        $data = array();

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $this->load->library('cdn_api');

            $data['file_title'] = $this->input->post('upload_file_name');
            $data['file_description'] = $this->input->post('upload_file_description');
            $data['file_name'] = $doc->file_name;

            if (trim($data['file_title']) == '') {
                $error_messages[] = 'Enter file name.';
            }

            if (trim($data['file_description']) == '') {
                $error_messages[] = 'Enter file description.';
            }

            $uri = '';
            $new_file_name = '';

            if (isset($_FILES['upload_file']) and $_FILES['upload_file']['name'] != '') {

                $file = $_FILES['upload_file'];

                if (empty($error_messages)) {

                    // Create a connection
                    $this->cdn_api->connect(
                        array(
                            'username' => RACKSPACE_USERNAME,
                            'api_key' => RACKSPACE_API_KEY,
                            'container' => $this->user_auth->get('cdn_container')
                        )
                    );

                    // Generate a new name for file
                    $n = basename($file['name']);
                    $x = pathinfo($n, PATHINFO_EXTENSION);
                    $new_file_name = md5($n) . '.' . $x;

                    $uri = $this->cdn_api->upload_file(
                        array(
                            'file_name' => $new_file_name,
                            'file_path' => $file['tmp_name']
                        )
                    );

                    // $data['file_name'] = $new_file_name;
                }
            }

            if (empty($error_messages)) {

                $ins_data = array(
                    'description' => $data['file_description'],
                    'id_uploader_user' => $this->user_auth->get('id_user'),
                    'file_title' => $data['file_title']
                );

                if ($new_file_name != '') {
                    $this->e_model->update_document_file(['file_name' => $new_file_name], array('id_cred' => $id_cred));
                }
                
                $this->e_model->update_document($ins_data, array('id_cred' => $id_cred));

                redirect(site_url('personal/my_profile'));
            }
        } else {
            $data['file_name'] = $doc->file_name;
            $data['file_title'] = $doc->file_title;
            $data['file_description'] = $doc->description;
        }

        $data['success_message'] = $success_message;
        $data['error_messages'] = $error_messages;

        $buffer = $this->load->view('user/personal/edit_file', $data, true);

        $this->user_nav->run_page($buffer, 'TalentBase - Edit Resume / Credentials', false);
    }

// End func edit_document

    public function upload_document($id_cred) {

        $this->user_auth->check_login();

        if ($id_cred == '') {
            show_404('page', false);
        }

        $id_user = $this->user_auth->get('id_user');

        $doc = $this->e_model->fetch_document(array('id_cred' => $id_cred, 'id_user' => $id_user));

        if (!$doc) {
            show_404('page', false);
        }

        $doc = $doc[0];

        if ($doc->type != 0) {
            show_404('page', false);
        }

        $success_message = '';
        $error_messages = '';

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $result = true;

            if (isset($_FILES['upload_file']['name']) and $_FILES['upload_file']['name'] != '') {
                /* ... */
            } else {
                $error_messages[] = 'Include file.';
                $result = false;
            }

            if (empty($error_messages) and $result == true) {

                $file = $_FILES['upload_file'];

                $this->load->library('cdn_api');

                // Create a connection
                $this->cdn_api->connect(
                    array(
                        'username' => RACKSPACE_USERNAME,
                        'api_key' => RACKSPACE_API_KEY,
                        'container' => $this->user_auth->get('cdn_container')
                    )
                );

                // Generate a new name for file
                $n = basename($file['name']);
                $x = pathinfo($n, PATHINFO_EXTENSION);
                $new_file_name = md5($n) . '.' . $x;

                $uri = $this->cdn_api->upload_file(
                    array(
                        'file_name' => $new_file_name,
                        'file_path' => $file['tmp_name']
                    )
                );

                $ins_data = array(
                    'type' => 1,
                    'id_uploader_user' => $id_user,
                );

                $this->e_model->insert_data(array(
                    'table' => 'employee_credential_files',
                    'vals' => array(
                        'file_name' => $new_file_name,
                        'id_cred' => $id_cred
                    )
                ));


                $this->e_model->update_credentials($ins_data, array('id_cred' => $id_cred, 'id_user' => $id_user));

                /* Check and update perms, if there are no more requests */
                $docs_requests = $this->e_model->fetch_docs_requests($id_user);
                if (empty($docs_requests)) {
                    $this->u_model->delete_perm_temp(array('id_user' => $id_user, 'id_perm' => EDIT_MY_CREDENTIALS));
                }
            } // End if errors.

            if ($result == true) {
                redirect(site_url('personal/my_profile'));
            }
        }

        $data = array(
            'success_message' => $success_message,
            'error_messages' => $error_messages,
            'doc' => $doc
        );

        $buffer = $this->load->view('user/personal/upload_file', $data, true);

        $this->user_nav->run_page($buffer, 'TalentBase - Upload Resume / Credentials', false);
    }

// End func upload_document

    public function my_tasks() {

        $this->user_auth->check_login();

        $this->load->library('tasks');

        $nav_menu = $this->user_nav->get_assoc();
        $dashboard_buttons = build_dashboard_buttons_nav_part($nav_menu, 'admin');

        $tasks = $this->tasks->get_todo_inprogress_tasks_by_user_id($this->user_auth->get('id_user'));

        $task_statuses = $this->tasks->get_ref_arr();

        $tasks_completed = $this->tasks->get_completed_tasks_by_user_id($this->user_auth->get('id_user')); // Chuks

        $tasks_sent = $this->tasks->fetch_tasks_sent_by_user_id($this->user_auth->get('id_user'));

        $tasks_data = array(
            'title' => 'My tasks',
            'tasks' => $tasks,
            'task_statuses' => $task_statuses,
            'tasks_sent' => $tasks_sent,
            'tasks_completed' => $tasks_completed
        );

        $buffer = $this->load->view('tasks/employee_tasks', $tasks_data, true);

        $this->user_nav->run_page($buffer, 'My Task', false);
    }

// End func my_tasks

    public function my_profile_info() {
        $url = 'personal/my_profile_info';
        $this->user_auth->check_login($url);

        $profile = $this->e_model->fetch_global(array('u.id_user' => $this->user_auth->get('id_user'), 'u.id_company' => $this->user_auth->get('id_company')));


        if (!$profile) {
            show_404('page', false);
        }

        $profile = $profile[0];


        $user = $this->e_model->get_where('employee_temp_request', array(
            'id_string' => $this->user_auth->get('id_user'),
            'id_company' => $this->user_auth->get('id_company'),
        ));

        $data = array(
            'requests' => $this->e_model->fetch_employee_requests(),
            'success_message' => '',
            'request_type' => $this->request_types,
            'profile' => $profile,
            'countries' => $this->ref->fetch_all_records('countries'),
            'banks' => $this->ref->fetch_all_records('banks'),
            'states' => $this->ref->fetch_all_records('states'),
            'account_types' => $this->account_types,
            'pfas' => $this->ref->fetch_all_records('pension_fund_administrators'),
        );

        $buffer = $this->load->view('user/employee/mega_request_form', $data, true);
        $this->user_nav->run_page($buffer, 'My profile', false);
    }

    public function update_birthday() {

        $this->user_auth->check_login();

        if (!$this->input->post('dob')) {
            echo '<h3>Unable to complete your request at the moment. Pls try again later</h3>';
        } else {

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.dob',
                'field_value' => date('Y-m-d h:i:s', strtotime($this->input->post('dob'))),
                'request_type' => $this->request_types['birthday']
            ));

            $this->_update_emp_temp_request(array(
                'request_type' => $this->request_types['birthday'],
                'id_temp_request' => $this->input->post('id_temp_request'),
                'id_task' => $this->input->post('id_task')
            ));

            // Update task
            $this->t_model->change_task_status($this->input->post('id_task'), TASK_STATUS_COMPLETE);

            $this->_notify_admin(array(
                'request' => 'Birthday'
            ));

            echo '<h3 align="center">Birthday updated successfully</h3>';
        }
    }

// end fuuc. update_birthday

    public function update_address() {

        $this->user_auth->check_login();

        if (!$this->input->post('address')) {
            echo '<h3>Unable to complete your request at the moment. Pls try again later</h3>';
        } else {

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.address',
                'field_value' => $this->input->post('address'),
                'request_type' => $this->request_types['home_address']
            ));

            $this->_update_emp_temp_request(array(
                'request_type' => $this->request_types['home_address'],
                'id_temp_request' => $this->input->post('id_temp_request'),
                'id_task' => $this->input->post('id_task')
            ));

            $this->t_model->change_task_status($this->input->post('id_task'), TASK_STATUS_COMPLETE);

            $this->_notify_admin(array(
                'request' => 'Home Address'
            ));

            echo '<h3 align="center">Home address updated successfully</h3>';
        }
    }

    private function _get_admin() {
        $this->load->model('payroll/payroll_model', 'p_model');
        return $this->p_model->get('users', array('account_type' => ACCOUNT_TYPE_ADMINISTRATOR,
                'id_company' => $this->user_auth->get('id_company')));
    }

    private function _notify_admin(array $params) {
        $first_name = $this->user_auth->get('display_name');
        $last_name = $this->user_auth->get('last_name');

        $admin = $this->_get_admin();

        $message = '<p> Hi ' . $admin[0]->first_name .
            ',</p><b> ' . $first_name . ' ' . $last_name . '</b> has filled in/completed his/her ' .
            $params['request'] . ' Information. <br>' .
            'kindly log in to <a href="' . site_url('employee/pending_approval') . '">' . site_url('employee/pending_approval') . '</a> to approve/decline ';

        $k = array(
            'note' => $message,
            'company_name' => $this->user_auth->get('company_name')
        );

        $msg = $this->load->view('email_templates/request_entry_notification', $k, true);

        send_email_now($this->user_auth->get('company_name'), $admin[0]->email, 'Information Update from Employee', $msg);
    }

    public function update_work_phone() {

        $this->user_auth->check_login();

        if (!$this->input->post('phone')) {
            echo '<h3>Unable to complete your request at the moment. Pls try again later</h3>';
        } else {

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'employee_phone.phone_number',
                'field_value' => str_replace(array('(', ')', ' ', '-'), '', $this->input->post('phone')),
                'request_type' => $this->request_types['work_phone']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'employee_phone.id_country_code',
                'field_value' => $this->input->post('id_country_code'),
                'request_type' => $this->request_types['work_phone']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'employee_phone.id_phone_type',
                'field_value' => WORK_PHONE_TYPE,
                'request_type' => $this->request_types['work_phone']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'employee_phone.ref_id',
                'field_value' => strtoupper(random_string('alnum', 10)),
                'request_type' => $this->request_types['work_phone']
            ));


            $this->_update_emp_temp_request(array(
                'request_type' => $this->request_types['work_phone'],
                'id_temp_request' => $this->input->post('id_temp_request'),
                'id_task' => $this->input->post('id_task')
            ));

            // delete task
            $this->t_model->delete_task($this->input->post('id_task'));

            $this->_notify_admin(array(
                'request' => 'Work Phone'
            ));

            echo '<h3 align="center">Work phone updated successfully</h3>';
        }
    }

    public function update_personal_phone() {

        $this->user_auth->check_login();

        if (!$this->input->post('phone')) {
            echo '<h3>Unable to complete your request at the moment. Pls try again later</h3>';
        } else {

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'employee_phone.phone_number',
                'field_value' => str_replace(array('(', ')', ' ', '-'), '', $this->input->post('phone')),
                'request_type' => $this->request_types['personal_phone']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'employee_phone.id_country_code',
                'field_value' => $this->input->post('id_country_code'),
                'request_type' => $this->request_types['personal_phone']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'employee_phone.id_phone_type',
                'field_value' => PERSONAL_PHONE_TYPE,
                'request_type' => $this->request_types['personal_phone']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'employee_phone.ref_id',
                'field_value' => strtoupper(random_string('alnum', 10)),
                'request_type' => $this->request_types['personal_phone']
            ));

            $this->_update_emp_temp_request(array(
                'request_type' => $this->request_types['personal_phone'],
                'id_temp_request' => $this->input->post('id_temp_request'),
                'id_task' => $this->input->post('id_task'),
                'request_type' => $this->request_types['personal_phone']
            ));

            $this->t_model->change_task_status($this->input->post('id_task'), TASK_STATUS_COMPLETE);

            $this->_notify_admin(array(
                'request' => 'Personal Phone'
            ));

            echo '<h3 align="center">Personal number updated successfully</h3>';
        }
    }

    public function update_benefit() {
        $this->user_auth->check_login();

        if (!$this->input->post('rsa_pin')) {
            echo '<h3>Unable to complete your request at the moment. Pls try again later</h3>';
        } else {

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.rsa_pin',
                'field_value' => $this->input->post('rsa_pin'),
                'request_type' => $this->request_types['pension_details']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.id_pfs',
                'field_value' => $this->input->post('pfs'),
                'request_type' => $this->request_types['pension_details']
            ));

            $this->_update_emp_temp_request(array(
                'request_type' => $this->request_types['pension_details'],
                'id_temp_request' => $this->input->post('id_temp_request'),
                'id_task' => $this->input->post('id_task')
            ));

            $this->t_model->change_task_status($this->input->post('id_task'), TASK_STATUS_COMPLETE);

            $this->_notify_admin(array(
                'request' => 'Benefit'
            ));

            echo '<h4 align="center">Pension details updated successfully</h4>';
        }
    }

    public function update_bank() {

        $this->user_auth->check_login();

        if (!$this->input->post('account_name')) {
            echo '<h3>Unable to complete your request at the moment. Pls try again later</h3>';
        } else {

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'employee_bank_details.bank_account_name',
                'field_value' => $this->input->post('account_name'),
                'request_type' => $this->request_types['bank_account']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'employee_bank_details.id_bank',
                'field_value' => $this->input->post('id_bank'),
                'request_type' => $this->request_types['bank_account']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'employee_bank_details.bank_account_number',
                'field_value' => $this->input->post('account_number'),
                'request_type' => $this->request_types['bank_account']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'employee_bank_details.id_bank_account_type',
                'field_value' => $this->input->post('account_type'),
                'request_type' => $this->request_types['bank_account']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'employee_bank_details.ref_id',
                'field_value' => strtoupper(random_string('alnum', 10)),
                'request_type' => $this->request_types['bank_account']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'employee_bank_details.default',
                'field_value' => 1,
                'request_type' => $this->request_types['bank_account']
            ));

            $this->_update_emp_temp_request(array(
                'request_type' => $this->request_types['bank_account'],
                'id_temp_request' => $this->input->post('id_temp_request'),
                'id_task' => $this->input->post('id_task')
            ));

            $this->t_model->change_task_status($this->input->post('id_task'), TASK_STATUS_COMPLETE);

            $this->_notify_admin(array(
                'request' => 'Bank'
            ));

            echo '<h4 align="center">Bank details updated successfully</h4>';
        }
    }

    public function update_emergency_contact1() {

        $this->user_auth->check_login();

        if (!$this->input->post()) {
            echo '<h3>Unable to complete your request at the moment. Pls try again later</h3>';
        } else {


            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.emergency_first_name',
                'field_value' => $this->input->post('emergency_first_name'),
                'request_type' => $this->request_types['emergency1']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.emergency_last_name',
                'field_value' => $this->input->post('emergency_last_name'),
                'request_type' => $this->request_types['emergency1']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.emergency_address',
                'field_value' => $this->input->post('emergency_address'),
                'request_type' => $this->request_types['emergency1']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.emergency_email',
                'field_value' => $this->input->post('emergency_email'),
                'request_type' => $this->request_types['emergency1']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.emergency_phone_number',
                'field_value' => $this->input->post('emergency_phone_number'),
                'request_type' => $this->request_types['emergency1']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.emergency_employer',
                'field_value' => $this->input->post('emergency_employer'),
                'request_type' => $this->request_types['emergency1']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.emergency_work_address',
                'field_value' => $this->input->post('emergency_work_address'),
                'request_type' => $this->request_types['emergency1']
            ));

            $this->_update_emp_temp_request(array(
                'request_type' => $this->request_types['emergency1'],
                'id_temp_request' => $this->input->post('id_temp_request'),
                'id_task' => $this->input->post('id_task')
            ));

            $this->t_model->change_task_status($this->input->post('id_task'), TASK_STATUS_COMPLETE);

            $this->_notify_admin(array(
                'request' => 'Emergency Contact 1'
            ));

            echo '<h4 align="center">Emergency contact updated successfully</h4>';
        }
    }

    public function update_emergency_contact2() {

        $this->user_auth->check_login();

        if (!$this->input->post()) {
            echo '<h3>Unable to complete your request at the moment. Pls try again later</h3>';
        } else {

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.emergency_first_name2',
                'field_value' => $this->input->post('emergency_first_name2'),
                'request_type' => $this->request_types['emergency2']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.emergency_last_name2',
                'field_value' => $this->input->post('emergency_last_name2'),
                'request_type' => $this->request_types['emergency2']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.emergency_address2',
                'field_value' => $this->input->post('emergency_address2'),
                'request_type' => $this->request_types['emergency2']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.emergency_email2',
                'field_value' => $this->input->post('emergency_email2'),
                'request_type' => $this->request_types['emergency2']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.emergency_phone_number2',
                'field_value' => $this->input->post('emergency_phone_number2'),
                'request_type' => $this->request_types['emergency2']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.emergency_employer2',
                'field_value' => $this->input->post('emergency_employer2'),
                'request_type' => $this->request_types['emergency2']
            ));

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.emergency_work_address2',
                'field_value' => $this->input->post('emergency_work_address2'),
                'request_type' => $this->request_types['emergency2']
            ));

            $this->_update_emp_temp_request(array(
                'request_type' => $this->request_types['emergency2'],
                'id_temp_request' => $this->input->post('id_temp_request'),
                'id_task' => $this->input->post('id_task')
            ));

            $this->t_model->change_task_status($this->input->post('id_task'), TASK_STATUS_COMPLETE);

            $this->_notify_admin(array(
                'request' => 'Emergency Contact 2'
            ));

            echo '<h4 align="center">Emergency contact 2 updated successfully</h4>';
        }
    }

    private function _update_emp_temp_request(array $params) {

        $set = array('status' => EMPLOYEE_REQUEST_FILLED);

        $this->p_model->update('employee_temp_request_task', $set, array(
            'id_temp_request' => $params['id_temp_request'],
            'request_type' => $params['request_type'],
            'id_task' => $params['id_task']
        ));
    }

    private function _insert_request_entries(array $params) {
        $this->load->model('payroll/payroll_model', 'p_model');

        $this->p_model->add('employee_request_entries', array(
            'id_temp_request' => $params['id_temp_request'],
            'id_user' => $this->user_auth->get('id_user'),
            'id_company' => $this->user_auth->get('id_company'),
            'field_name' => $params['field_name'],
            'field_value' => $params['field_value'],
            'request_type' => $params['request_type'],
            'timestamp' => date('Y-m-d h:i:s')
        ));
    }

    public function update_gender() {

        $this->user_auth->check_login();

        if (!$this->input->post('gender')) {
            echo '<h3>Unable to complete your request at the moment. Pls try again later</h3>';
        } else {

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.id_gender',
                'field_value' => $this->input->post('gender'),
                'request_type' => $this->request_types['gender']
            ));

            $this->_update_emp_temp_request(array(
                'request_type' => $this->request_types['gender'],
                'id_temp_request' => $this->input->post('id_temp_request'),
                'id_task' => $this->input->post('id_task')
            ));

            $this->t_model->change_task_status($this->input->post('id_task'), TASK_STATUS_COMPLETE);

            $this->_notify_admin(array(
                'request' => 'Gender'
            ));

            echo '<h3 align="center">Gender updated successfully</h3>';
        }
    }

    public function update_marital_status() {

        $this->user_auth->check_login();

        if (!$this->input->post('marital_status')) {
            echo '<h3>Unable to complete your request at the moment. Pls try again later</h3>';
        } else {

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.id_marital_status',
                'field_value' => $this->input->post('marital_status'),
                'request_type' => $this->request_types['marital_status']
            ));

            $this->_update_emp_temp_request(array(
                'request_type' => $this->request_types['marital_status'],
                'id_temp_request' => $this->input->post('id_temp_request'),
                'id_task' => $this->input->post('id_task')
            ));

            $this->t_model->change_task_status($this->input->post('id_task'), TASK_STATUS_COMPLETE);

            $this->_notify_admin(array(
                'request' => 'Marital Status'
            ));

            echo '<h3 align="center">Marital status updated successfully</h3>';
        }
    }

    public function update_nationality() {

        $this->user_auth->check_login();

        if (!$this->input->post('nationality')) {
            echo '<h3>Unable to complete your request at the moment. Pls try again later</h3>';
        } else {

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.id_nationality',
                'field_value' => $this->input->post('nationality'),
                'request_type' => $this->request_types['nationality']
            ));

            $this->_update_emp_temp_request(array(
                'request_type' => $this->request_types['nationality'],
                'id_temp_request' => $this->input->post('id_temp_request'),
                'id_task' => $this->input->post('id_task')
            ));

            $this->t_model->change_task_status($this->input->post('id_task'), TASK_STATUS_COMPLETE);

            $this->_notify_admin(array(
                'request' => 'Nationality'
            ));

            echo '<h3 align="center">Nationallity updated successfully</h3>';
        }
    }

    public function update_country() {

        $this->user_auth->check_login();

        if (!$this->input->post('country')) {
            echo '<h3>Unable to complete your request at the moment. Pls try again later</h3>';
        } else {

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.id_country',
                'field_value' => $this->input->post('country'),
                'request_type' => $this->request_types['country']
            ));

            $this->_update_emp_temp_request(array(
                'request_type' => $this->request_types['country'],
                'id_temp_request' => $this->input->post('id_temp_request'),
                'id_task' => $this->input->post('id_task')
            ));

            $this->t_model->change_task_status($this->input->post('id_task'), TASK_STATUS_COMPLETE);

            $this->_notify_admin(array(
                'request' => 'Country'
            ));

            echo '<h3 align="center">Country updated successfully</h3>';
        }
    }

    public function update_city() {

        $this->user_auth->check_login();

        if (!$this->input->post('city')) {
            echo '<h3>Unable to complete your request at the moment. Pls try again later</h3>';
        } else {

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.city',
                'field_value' => $this->input->post('city'),
                'request_type' => $this->request_types['city']
            ));

            $this->_update_emp_temp_request(array(
                'request_type' => $this->request_types['city'],
                'id_temp_request' => $this->input->post('id_temp_request'),
                'id_task' => $this->input->post('id_task')
            ));

            $this->t_model->change_task_status($this->input->post('id_task'), TASK_STATUS_COMPLETE);

            $this->_notify_admin(array(
                'request' => 'City'
            ));

            echo '<h3 align="center">City updated successfully</h3>';
        }
    }

    public function update_state() {

        $this->user_auth->check_login();

        if (!$this->input->post('id_state')) {
            echo '<h3>Unable to complete your request at the moment. Pls try again later</h3>';
        } else {

            $this->_insert_request_entries(array(
                'id_temp_request' => $this->input->post('id_temp_request'),
                'field_name' => 'users.id_state',
                'field_value' => $this->input->post('id_state'),
                'request_type' => $this->request_types['state']
            ));

            $this->_update_emp_temp_request(array(
                'request_type' => $this->request_types['state'],
                'id_temp_request' => $this->input->post('id_temp_request'),
                'id_task' => $this->input->post('id_task')
            ));

            $this->t_model->change_task_status($this->input->post('id_task'), TASK_STATUS_COMPLETE);

            $this->_notify_admin(array(
                'request' => 'State'
            ));

            echo '<h3 align="center">State updated successfully</h3>';
        }
    }

}

// End class Employee

// End file employee.php