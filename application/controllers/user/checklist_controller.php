<?php

/**
 * @property CI_Input $input 
 * @property CI_Output $output
 * @property CI_Loader $load Loader
 * @property user_auth $user_auth Auth
 * @property User_nav $user_nav
 * @property Checklist_Model $checklist_model
 * @property Checklist_Lib $checklist_lib
 * @property Employee_info_request $employee_info_request
 * @property User_model $user_model
 * 
 */
class Checklist_Controller extends CI_Controller {

    private $id_company;

    public function __construct() {
        parent::__construct();
        $this->load->library(['user_nav', 'employee_info_request', 'checklist_lib']);
        $this->load->model('user/checklist_model', 'checklist_model');
        $this->load->model('user/user_model', 'user_model');
        if ($this->user_auth) {
            $this->id_company = $this->user_auth->get('id_company');
        }
    }

    public function index() {
        $this->user_auth->check_perm('employee:checklist');

        //load onboarding / exit
        //get checklist
        $this->checklist_model->loadDefaultChecklists($this->id_company);

        $data = [];
        $data['checklists'] = $this->checklist_model->getChecklists($this->id_company);
        $this->user_nav->run_page_with_view('user/employee/checklist/list', $data, 'Company Checklist');
    }

    public function toggle($ref) {
        $this->user_auth->check_perm('employee:checklist_edit');
        if ($this->checklist_model->toggleStatus($this->id_company, $ref)) {
            flash_message_create('Status change was successful.', 'success');
        } else {
            flash_message_create('Status change could not be completed, ensure the .', 'success');
        }

        redirect($this->input->server('HTTP_REFERER') ? : site_url('/employee/checklist'));
    }

    public function create() {
        $this->edit(null);
    }

    public function edit($ref) {
        $this->user_auth->check_perm('employee:checklist_edit');
        $pageData = ['error' => null];

        if ($ref && ($checklist = $this->checklist_model->getByRef($this->id_company, $ref))) {
            $pageData['checklist'] = $checklist;
        } else {
            $pageData['checklist'] = null;
        }

        if (request_is_post()) {
            $data = request_post_data();

            $idChecklist = $checklist ? $checklist->id : null;
            if ($this->checklist_lib->validate($data, $pageData['error']) && $this->checklist_model->saveChecklist($data, $idChecklist, $this->id_company, $ref)) {
                //save
                flash_message_create('Your checklist changes were successfully saved.', 'success');
                redirect(site_url('employee/checklist/edit/' . $ref));
            }

            if (!$pageData['error']) {
                $pageData['error'] = 'Checklist details could not be saved.';
            }

            //var_dump($data);exit;
            $pageData['checklist'] = $data;
            $pageData['checklist']['ref'] = $ref;
        }

        $taskTypes = $this->checklist_lib->getTaskTypes();
        $pageData['taskTypes'] = $taskTypes;
        foreach ($taskTypes as $taskType) {
            $taskType->editBootstrap($pageData);
        }

        $this->user_nav->run_page_with_view('user/employee/checklist/edit', $pageData, $checklist ? 'Edit Checklist' : 'Create Checklist');
    }

    public function delete_item($ref) {
        $this->user_auth->check_perm('employee:checklist_edit');
        if (!request_is_post()) {
            request_bad();
            return;
        }

        $itemId = $this->input->post('item_id');

        if (!$itemId) {
            $this->output->set_status_header(400);
            die('Item ID not found');
        }

        if (!$ref || !($checklist = $this->checklist_model->getByRef($this->id_company, $ref))) {
            $this->output->set_status_header(404);
            return;
        }

        $this->output->set_content_type('application/json');
        $this->output->_display(json_encode($this->checklist_model->deleteChecklistItems($checklist->id, [$itemId])));
    }

    public function assign($ref = '') {
        $this->user_auth->check_perm('employee:checklist_assign');
        if (!$ref) {
            $ref = $this->input->get('ref');
        }
        $ids = $this->input->get('ids');

        $idRefs = is_array($ids) ? $ids : preg_split('/[,\|]+/', $this->input->get('ids'));
        if (empty($idRefs)) {
            show_404('We could not process your request.', true);
        }

        $users = $this->user_model->fetch_account_info($this->id_company, ['u.id_string' => $idRefs], null, 0, stdClass::class, true);
        if (empty($users)) {
            show_404('The selected users were not found.', true);
        }

        $checklist = $this->checklist_model->getByRef($this->id_company, $ref);

        if (!$checklist || !$checklist->enabled) {
            show_404('The checklist is not enabled.', true);
        }

        //check if POST, blah blah


        $pageData = ['checklist' => $checklist, 'users' => $users, 'taskTypes' => $this->checklist_lib->getTaskTypes()];

        if (request_is_post()) {
            $dCopy = $data = $this->input->post();
            $approvers = $dCopy['assign']['approvers'];
            unset($dCopy['assign']['approvers']);
            if ($this->checklist_lib->assign($checklist, $users, $dCopy['assign'], $approvers)) {
                flash_message_create('Checklist has been successfully assigned to the selected employee(s)', 'success');
                redirect('employee/checklist');
            } else {
                $pageData['error'] = 'Checklist assignment failed.';
            }
        } else {
            $data = [];
            foreach ($checklist->getItems() as $checklistItem) {
                $itemData = [];
                $this->checklist_lib->getTaskType($checklistItem->item_type)->assignBootstrap($checklistItem, $itemData);
                $data['assign'][$checklistItem->id] = $itemData;
            }
            $data['assign']['approvers'] = $checklist->approverIds();
        }

        $pageData['data'] = $data;

        foreach ($this->checklist_lib->getTaskTypes() as $taskType) {
            $taskType->editBootstrap($pageData);
        }

        $this->user_nav->run_page_with_view('user/employee/checklist/assign', $pageData, 'Assign Checklist');
    }

    public function assigned($ref = '') {
        $this->user_auth->check_perm('employee:checklist_assigned');
        $checklist = null;
        if ($ref) {
            $checklist = $this->checklist_model->getByRef($this->id_company, $ref);
            if (!$checklist) {
                show_404('Checklist not found.', true);
            }
        }

        if ($this->input->get('json')) {
            $this->output->set_content_type('application/json');
            $checklists = $this->checklist_model
                    ->getEmployeeChecklists($this->id_company, $checklist ? $checklist->id : null, null, null, employee\EmployeeChecklistLite::class);
            $this->output->_display(json_encode($checklists));
            return;
        }

        $pageData = ['checklist' => $checklist, $ref => ''];
        $this->user_nav->run_page_with_view('user/employee/checklist/assigned', $pageData, 'Employee Checklist' . ($checklist ? (' - ' . $checklist->title) : ''), false);
    }

}
