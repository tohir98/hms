<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Talentbase
 * Applicant controller
 * 
 * @category   Controller
 * @package    Applicant
 * @subpackage Main
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

class Applicant extends CI_Controller {
	
	/**
	 * Class constructor
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		
		/*
		 * Loaded automatically in app.
		 * 
		 * lib - user_auth, session
		 * helper - url
		 */
		
		$this->load->library(array('user_nav', 'applicant_auth', 'bugsnag', 'cdn_api'));
		$this->load->model('user/applicant_model', 'a_model');
						
	} // End func __construct
	
	public function logout() {
		
		// Call logout function from user_auth library
        $this->applicant_auth->logout();
        redirect(site_url(), 'refresh');
		
	} // End func logout
	
	/**
	 * Applicant login or register
	 * 
	 * @access public
	 * @param string $id_reference
	 * @param string $id_string
	 * @return void
	 */
	public function login($id_reference = NULL, $id_string = NULL) {
		
		if($this->user_auth->logged_in()) {
			
			if($this->user_auth->get('account_type') == USER_TYPE_ADMIN) {
				$login_url = site_url('admin/dashboard');
			} else {
				$login_url = site_url('employee/dashboard');
			}
			
			$login_title = 'Dashboard';
			
		} else {
			$login_url = site_url('login');
			$login_title = 'Employee Login';
		}
		
		$register_message = NULL;
		$login_message = NULL;
		
		$success_message = '';
		$error_message = '';
		
		if($this->input->server('REQUEST_METHOD') == 'POST') {
			
			$login_message = $this->_login();
			$register_message = $this->_register($id_reference, $id_string);
			
			if($register_message === true) {
				$success_message = 'You should receive email with activation link to reset your password.';
			} elseif($register_message != '') {
				$error_message = $register_message;
			}
		
			if($login_message === true) {
				redirect(site_url('jobs/apply/'. $id_reference . '/' . $id_string));
			} elseif($login_message != '') {
				$error_message = $login_message;
			}
		
		}
		
		$hdata = array(
			'page_title' => 'Login or create account'
		);
		
		$company = $this->subdomain->company();
		
		$bdata = array(
			'forgot_password_url' => site_url('applicant/forgot_password'),
			'login_url' => $login_url,
			'login_title' => $login_title,
			'error_message' => $error_message,
			'success_message' => $success_message,
			'logo_path' => $company[0]->logo_path,
			'logo_text' => $company[0]->logo_text
		);
		
		$this->cdn_api->connect(array(
			'username' => RACKSPACE_USERNAME,
			'api_key' => RACKSPACE_API_KEY,
			'container' => STORAGE_CONTAINER_ASSETS
		));
		
		$this->load->view('company_templates/header', $hdata);
		$this->load->view('user/applicant_login', $bdata);
		$this->load->view('company_templates/footer');
		
	} // End func login
	
	protected function _login() {
		
		if($this->input->post("login_button", TRUE) == '') {
			return NULL;
		}
		
		$email = $this->input->post("email", TRUE);
        $password = $this->input->post("password", TRUE);
		
		$result = $this->applicant_auth->login($email, $password);
		
		if(!$result) {
			return 'Invalid Email or password.';
		}
		
		if($result['status'] != 'Active') {
			return 'Your account is not active.';
		}
		
		return true;
		
	} // End func _login

    /**
     * Check is valid e-mail address
     *
     * @access public
     * @param string $value
     * @return mixed
     */
    public function is_email($data) {

        if(preg_match("/^[a-z0-9_\.-]+@([a-z0-9]+([\-]+[a-z0-9]+)*\.)+[a-z]{2,6}$/i", $data) and strlen($data) < 100) {
            return $data;
        } else {
            return false;
        }
    } // End func email
	
	protected function _register($id_reference, $id_string) {
		
		if($this->input->post("register_button", TRUE) == '') {
			return NULL;
		}
		
		$email = $this->input->post("register_email", TRUE);
        $fname = $this->input->post("fname", TRUE);
		$lname = $this->input->post("lname", TRUE);

        if(!$this->is_email($email)) {
            return 'Invalid Email address.';
        }

		$appl = $this->a_model->fetch_account(array('email' => $email));
		
		if($appl) {
			return 'This Email address already registered.';
		}	
		
		$new_password = time() . $email;
		$new_password = $this->applicant_auth->encrypt($new_password);
		
		$applicant_id = $this->a_model->insert_account(
			array(
				'email' => $email,
				'password' => $this->applicant_auth->encrypt($new_password),
				'status' => 'Inactive',
				'created' => date('Y-m-d H:i:s'),
				'firstname' => $fname,
				'lastname' => $lname
			)
		);
		
		$this->_send_registration_email(
			array(
				'full_name' => $fname . ' ' . $lname,
				'email' => $email,
				'password' => $new_password,
				'activation_url' => site_url('applicant/activate_account/' . $applicant_id . '/' . sha1($email . $applicant_id) . '/' . $id_reference .'/'. $id_string)
			)
		);
		
		return true;
		
	} // End func _register
	
	protected function _send_registration_email($data) {
		
		$this->load->library('mailgun_api');
		
		$sender = 'TalentBase';
		
		$msg_data = array(
			'full_name' => $data['full_name'],
			'activation_url' => $data['activation_url'],
			'email' => $data['email'],
			'password' => $data['password']
		);
		
		$msg_content = $this->load->view('email_templates/applicant_reset_password_after_register', $msg_data, true);
		
		$r = $this->mailgun_api->send_mail (
			array (
				'from' => $sender.' <'.TALENTBASE_SUPPORT_EMAIL.'>',
				'to' => $data['email'],
				'subject' => 'Registration Confirmation',
				'html' => $msg_content
			)
		);
		
	}
	
	public function activate_account($id_applicant, $key, $id_reference, $id_string) {
		
		$applicant = $this->a_model->fetch_account(array('candidateid' => $id_applicant));
		
		if(!$applicant) {
			show_404('page', false);
		}
		
		$db_key = sha1($applicant[0]->email . $id_applicant);
		
		if($db_key != $key) {
			show_404('page', false);
		}
		
		$success_message = '';
		$error_message = '';
		
		if($this->input->server('REQUEST_METHOD') == 'POST') {
			
			$new_password = $this->input->post("new_password", TRUE);
			$confirm_password = $this->input->post("confirm_password", TRUE);
		
			if(strlen(trim($new_password)) < 6) {
				$error_message = 'Enter valid password (minimum 6 chars).';
			} elseif($new_password != $confirm_password) {
				$error_message = 'Invalid confirmation.';
			} else {
				$this->a_model->update_account (
					array(
						'password' => $this->applicant_auth->encrypt($new_password),
						'status' => 'Active'
					),	
					array(
						'candidateid' => $id_applicant
					)
				);
				
				$success_message = 'Password changed successfully.';
			}
			
			redirect(
				site_url('applicant/login/' . $id_reference .'/'. $id_string)
			);
				
		}
		
		$hdata = array(
			'page_title' => 'Reset password'
		);
		
		$login_url = '';
		$login_title = 'Login';
		
		$bdata = array(
			'login_url' => $login_url,
			'login_title' => $login_title,
			'error_message' => $error_message,
			'success_message' => $success_message
		);
		
		$this->load->view('company_templates/header', $hdata);
		$this->load->view('user/applicant_reset_password', $bdata);
		$this->load->view('company_templates/footer');
		
		
	} // End func activate_account
    
    private function _get_company_name($url){

        $parsedUrl = parse_url($url);

        $host = explode('.', $parsedUrl['host']);

        $subdomain = $host[0];
        
        return $this->a_model->get_company_name($subdomain);
    }
   
	
	public function forgot_password() {
        
        
		$login_url = site_url('login');
		$login_title = 'Employee Login';
		
		$success_message = '';
		$error_message = '';
		
		if($this->input->server('REQUEST_METHOD') == 'POST') {
			
			$email = $this->input->post("email", TRUE);
        
			$appl = $this->a_model->fetch_account(array('email' => $email));
		
			if(!$appl) {
				$error_message = 'E-mail address not exists in our database.';
			} else {
				$this->_send_forgot_password_email(
					array(
						'full_name' => $appl[0]->firstname . ' ' . $appl[0]->lastname,
						'email' => $email,
						'reset_password_url' => site_url('applicant/reset_password/' . $appl[0]->candidateid . '/' . sha1($email . $appl[0]->candidateid))
					)
				);
				$success_message = 'You should receive email with password reset link within 5 mins.';
			}	
		}
		
		$hdata = array(
			'page_title' => 'Forgot password'
		);
		
		$bdata = array(
			'login_url' => $login_url,
			'login_title' => $login_title,
			'error_message' => $error_message,
			'success_message' => $success_message
		);
		
		$this->load->view('company_templates/header', $hdata);
		$this->load->view('user/applicant_forgot_password', $bdata);
		$this->load->view('company_templates/footer');
		
		return true;
		
	} // End func forgot_password
	
	protected function _send_forgot_password_email($data) {
		
		$this->load->library('mailgun_api');
		
		//$sender = 'TalentBase';
        $sender = $this->_get_company_name(site_url());
		
		$msg_data = array(
			'full_name' => $data['full_name'],
			'reset_password_url' => $data['reset_password_url']
		);
		
		$msg_content = $this->load->view('email_templates/applicant_forgot_password', $msg_data, true);
		
		$this->mailgun_api->send_mail (
			array (
				'from' => $sender.' <support@talentbase.ng>',
				'to' => $data['email'],
				'subject' => 'Reset password',
				'html' => $msg_content
			)
		);
		
	} // End func _send_forgot_password_email
	
	public function reset_password($id_applicant, $key) {
		
		$applicant = $this->a_model->fetch_account(array('candidateid' => $id_applicant));
		
		if(!$applicant) {
			show_404('page', false);
		}
		
		$db_key = sha1($applicant[0]->email . $id_applicant);
		
		if($db_key != $key) {
			show_404('page', false);
		}
		
		$login_url = site_url('login');
		$login_title = 'Employee Login';
		
		$success_message = '';
		$error_message = '';
		
		if($this->input->server('REQUEST_METHOD') == 'POST') {
			
			$new_password = $this->input->post("new_password", TRUE);
			$confirm_password = $this->input->post("confirm_password", TRUE);
		
			if(strlen(trim($new_password)) < 6) {
				$error_message = 'Enter valid password (minimum 6 chars).';
			} elseif($new_password != $confirm_password) {
				$error_message = 'Invalid confirmation.';
			} else {
				$this->a_model->update_account (
					array(
						'password' => $this->applicant_auth->encrypt($new_password)
					),	
					array(
						'candidateid' => $id_applicant
					)
				);
				$success_message = 'Password changed successfully.';
			}
				
		}
		
		$hdata = array(
			'page_title' => 'Reset password'
		);
		
		$bdata = array(
			'login_url' => $login_url,
			'login_title' => $login_title,
			'error_message' => $error_message,
			'success_message' => $success_message
		);
		
		$this->load->view('company_templates/header', $hdata);
		$this->load->view('user/applicant_reset_password', $bdata);
		$this->load->view('company_templates/footer');
		
		return true;
		
	} // End func reset_password
	
} // End class Applicant

// End file applicant