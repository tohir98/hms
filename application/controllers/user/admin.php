<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Talentbase
 * Admin (employer) controller
 * 
 * @category   Controller
 * @package    Users
 * @subpackage Admin
 * @author     David A. <tokernel@gmail.com>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */
class Admin extends CI_Controller {

    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();

        /*
         * Loaded automatically in app.
         * 
         * lib - user_auth, session
         * helper - url
         */

        $this->load->library('user_nav');
        $this->load->helper('user_nav');
    }

// End func __construct

    /**
     * Admin dashboard 
     * 
     * @access public
     * @return void
     */
    public function dashboard() {

        $this->user_auth->check_login();

        // Check, if user is admin in current company and admin in another sub company, than load special dashboard for admin.
        $this->load->model('user/user_model', 'u_model');

        if (!$this->user_auth->get('company_switch_selected')) {
            $companies = $this->u_model->get_all_companies($this->user_auth->get('email'), $this->user_auth->get('id_company'));
            if (count($companies) > 1) {
                $this->show_dashboardp5($companies);
                return;
            }
        }


        $this->load->library('tasks');

        $nav_menu = $this->user_nav->get_assoc();
        $dashboard_buttons = build_dashboard_buttons_nav_part($nav_menu, 'admin');

        $tasks = $this->tasks->get_todo_inprogress_tasks_by_user_id($this->user_auth->get('id_user'));

        $task_statuses = $this->tasks->get_ref_arr();

        $tasks_data = array(
            'title' => 'My tasks',
            'tasks' => $tasks,
            'task_statuses' => $task_statuses
        );

        $tasks_buffer = $this->load->view('tasks/dashboard_box', $tasks_data, true);
        $mixpanel = $this->load->view('templates/create_mixpanel_profile', '', true);
        $buffer = $dashboard_buttons . $tasks_buffer . $mixpanel;

        $this->user_nav->run_page($buffer, 'Dashboard', false);
    }

// End func dashboard

    public function show_dashboardp5($companies) {

        $c_arr = array();
        $this->load->model('user/user_model', 'usr_model');
        $this->load->model('tasks/tasks_model');

        if ($this->user_auth->get('access_level') == ACCOUNT_TYPE_ADMINISTRATOR) {
            $r = 'Admin';
        } else if ($this->user_auth->get('access_level') == ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR) {
            $r = 'Secondary Admin';
        } else if ($this->user_auth->get('access_level') == ACCOUNT_TYPE_UNIT_MANAGER) {
            $r = 'Unit Manager';
        } else {
            $r = 'Employee';
        }

        if (!empty($companies)) {

            foreach ($companies as $c) {

                if ($c->id_access_level == ACCOUNT_TYPE_ADMINISTRATOR) {
                    $r = 'Admin';
                } else if ($c->id_access_level == ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR) {
                    $r = 'Secondary Admin';
                } else if ($c->id_access_level == ACCOUNT_TYPE_UNIT_MANAGER) {
                    $r = 'Unit Manager';
                } else {
                    $r = 'Employee';
                }

                $c_arr[] = array(
                    'role' => $r,
                    'company' => $c,
                    'tasks' => $this->tasks_model->get_user_tasks_count($c->id_user)
                );
            }
        }

        $data = array(
            'companies' => $c_arr
        );

        $buffer = $this->load->view('user/multicompany_dashboard', $data, true);

        $this->user_nav->run_page($buffer, 'Dashboard', false, false, null, false);
    }

// End func show_dashboardp5

    /**
     * Registration page
     *
     * @access public
     * @return void
     * */
    public function register() {
        
    }

// End func register	

    /**
     * Manage profile page
     *
     * @access public
     * @return void
     * */
    public function manage_profile() {
        
    }

// End func manage_profile

    /**
     * View account details page
     *
     * @access public
     * @param string $id_user
     * @return void
     * */
    public function details($id_user) {
        
    }

// End func details
}

// End class Admin
// End file admin.php
?>