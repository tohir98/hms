<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Talentbase
 * Permissions controller
 * 
 * @category   Controller
 * @package    Users
 * @subpackage Permissions
 * @author     David A. <tokernel@gmail.com>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

class Permissions extends CI_Controller {

	/**
	 * Class constructor
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		
		/*
		 * Loaded automatically in app.
		 * 
		 * lib - user_auth, session
		 * helper - url
		 */
		
	} // End func __construct
	
	/**
	 * Manage employee permissions page
	 * 
	 * @access public
	 * @param string $id_employee
	 * @return void
	 */
	public function manage($id_employee) {
		
	} // End func manage
	
} // End class Permissions	

// End file permissions.php
?>
