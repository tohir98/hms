<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Talentbase
 * Groups controller
 * 
 * @category   Controller
 * @package    Users
 * @subpackage Groups
 * @author     David A. <tokernel@gmail.com>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

class Groups extends CI_Controller {

	/**
	 * Class constructor
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		
		/*
		 * Loaded automatically in app.
		 * 
		 * lib - user_auth, session
		 * helper - url
		 */
		
	} // End func __construct
	
	/**
	 * Create group page
	 *
	 * @access public
	 * @return void
	 **/
	public function create_group() {
		
	} // End func create_group
	
	/**
	 * Manage group page
	 *
	 * @access public
	 * @param string $id_group
	 * @return void
	 **/
	public function manage_group($id_group) {
		
	} // End func manage_group
	
	/**
	 * Manage group  - will redirect to browse page.
	 *
	 * @access public
	 * @param string $id_group
	 * @return void
	 **/
	public function delete_group($id_group) {
		
	} // End func delete_group
	
	/**
	 * Browse groups page
	 *
	 * @access public
	 * @return void
	 **/
	public function browse() {
		
	} // End func browse
	
} // End class Groups	

// End file groups.php
?>