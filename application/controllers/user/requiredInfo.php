<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Talentbase
 * Employee controller
 * 
 * @category   Controller
 * @package    Users
 * @subpackage Employee
 * @author     Tohir O. <thohiru.omoloye@talentbase.ng>
 * @copyright  Copyright (c) 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 * @property-read Employee_model $e_model employeemodel
 * @property User_model $u_model Description
 */

class RequiredInfo extends CI_Controller {
      /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();

        /*
         * Loaded automatically in app.
         * 
         * lib - user_auth, session
         * helper - url
         */
        $this->load->library(array(
            'user_nav',
            'sms_lib',
            'bugsnag',
            'cdn_api'));

        $this->load->helper('user_nav');
    }
    
    public function edit_employee_name($id_string){
        
        $id_company = $this->user_auth->get('id_company');
        $id_string = $this->uri->segment(3);

        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');
        
        if ($this->input->post()){
            
            $data_db = array(
                'first_name' => $this->input->post(first_name),
                'middle_name' => $this->input->post(middle_name),
                'last_name' => $this->input->post(last_name),
            );
            
            $where = ['id_string' => $id_string, 'id_company' => $this->user_auth->get('id_company')];
            
            if ($this->e_model->update_employee_data($data_db,$where )){
                echo 'success';
            }else{
                echo 'err';
            }
            return;
        }

        $record = $this->u_model->fetch_account(array('id_string' => $id_string, 'id_company' => $id_company));
        
        $data = array(
            'first_name' => $record[0]->first_name,
            'middle_name' => $record[0]->middle_name,
            'last_name' => $record[0]->last_name,
            'id_user' => $record[0]->id_user,
            'id_string' => $record[0]->id_string,
            'page_title' => 'Edit Employee Name',
        );
        
        $this->load->view('user/employee/requiredInfo/edit_employee_name', $data);
        
    }
    
    public function edit_staff_id($id_string){
        
        $id_company = $this->user_auth->get('id_company');
        $id_string = $this->uri->segment(3);

        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');

        $record = $this->u_model->fetch_account(array('id_string' => $id_string, 'id_company' => $id_company));
        
        $data = array(
             'first_name' => $record[0]->first_name,
            'last_name' => $record[0]->last_name,
            'id_user' => $record[0]->id_user,
            'id_string' => $record[0]->id_string,
            'id_staff' => $this->e_model->get_employee_staff_id(['id_user' => $record[0]->id_user]),
            'page_title' => 'Edit Staff ID',
        );
        
        $this->load->view('user/employee/requiredInfo/edit_staff_id', $data);
        
    }
    
    public function update_staff_id($id_user){
        if ($this->input->post()){
            
            $data_db = array(
                'id_staff' => $this->input->post('id_string'),
            );
            $this->load->model('user/employee_model', 'e_model');
            
            $count = $this->e_model->get_where('employees', ['id_staff' => $this->input->post(id_string), 'id_company' => $this->user_auth->get('id_company'), 'id_user <>' => $id_user], TRUE);
            if ($count > 0) {
                echo 'exist';
                return;
            }
            
            $where = ['id_user' => $id_user, 'id_company' => $this->user_auth->get('id_company')];
            
            if ($this->e_model->update_staff_id($data_db,$where )){
                echo 'success';
            }else{
                echo 'err';
            }
            return;
        }
    }
    
    public function edit_staff_email($id_string){

        $this->load->model('user/user_model', 'u_model');
        $this->load->model('user/employee_model', 'e_model');

        $record = $this->u_model->fetch_account(array('id_string' => $id_string, 'id_company' => $this->user_auth->get('id_company')));
        
        $data = array(
             'first_name' => $record[0]->first_name,
            'last_name' => $record[0]->last_name,
            'id_user' => $record[0]->id_user,
            'email' => $record[0]->email,
            'page_title' => 'Edit Staff Email',
        );
        
        $this->load->view('user/employee/requiredInfo/edit_staff_email', $data);
        
    }
    
    public function update_staff_email($id_user){
        if ($this->input->post()){
            $data_db = array(
                'email' => $this->input->post('email'),
            );
            $this->load->model('user/user_model', 'u_model');
            $this->load->model('user/employee_model', 'e_model');
            
            $email_exist = $this->u_model->email_exists($this->input->post('email'), $id_user, $this->user_auth->get('id_company'));
            
            if(!empty($email_exist)){
                echo 'exist';
                return;
            }
            
            $where = ['id_user' => $id_user, 'id_company' => $this->user_auth->get('id_company')];
            
            if ($this->e_model->update_employee_data($data_db,$where )){
                echo 'success';
            }else{
                echo 'err';
            }
            return;
        }
    }
    
    
}
