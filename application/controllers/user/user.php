<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Talentbase
 * Users controller
 * 
 * @category   Controller
 * @package    Users
 * @subpackage Main
 * @author     David A. <tokernel@gmail.com>
 * @author     Chuks O. <chuks.olloh@hired.com.ng>
 * @copyright  Copyright Â© 2014 Talentbase Nigeria Ltd.
 * @license    
 * @version    1.0.0
 * @link       
 * @since      File available since Release 1.0.0
 * 
 * @property user_auth $user_auth user authentication library.
 * @property auth_factory $auth_factory 
 * @property CI_Input $input
 * @property Company_model $c_model
 * @property User_model $u_model Description
 *  
 */
class User extends CI_Controller {

    private $account_blocked_message = '';

    // private $account_blocked_message = 'Your account has been blocked as you have exhausted the allowed log-in attempts. Please contact your Administrator to re-activate your account';

    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();

        /*
         * Loaded automatically in app.
         * 
         * lib - user_auth
         * helper - url
         */
        $this->load->library(array('user_nav'));
        $this->load->helper(['user_nav', 'auth']);

        $this->load->model('user/user_model', 'u_model');
        $this->load->model('company/company_model', 'c_model');

        $this->account_blocked_message = 'For security reasons, after ' . USER_LOGIN_ATTEMPTS . ' more failed login attempts this account will be locked and password reset instructions sent to the registered email.';
    }

// End func __construct

    public function cli_example() {
        echo "\n This is function body!\n";
    }

    public function example_form_wizard1() {

        $this->user_auth->check_login();
        //_form_wizards
        $buffer = $this->load->view('_form_wizards/form_wizard_1', null, true);

        $this->user_nav->run_page($buffer, 'Form wizard example 1');
    }

    public function example_form_wizard2() {

        $this->user_auth->check_login();
        //_form_wizards
        $buffer = $this->load->view('_form_wizards/form_wizard_2', null, true);

        $this->user_nav->run_page($buffer, 'Form wizard example 2', false);
    }

    public function example_form_wizard2_responder() {
        echo json_encode(
            array(
                'data' => true
            )
        );
    }

    public function access_denied() {
        echo '<h1>Access denied!</h1> <br />';
        echo '(This page is under construction)';
    }

    /**
     * Login page
     *
     * @access public
     * @return void
     * */
    public function login() {

        $redirect_url = '';
        if ($this->uri->segment(2)) {
            $redirect_url .= $this->uri->segment(2) . '/';
        }

        if ($this->uri->segment(3)) {
            $redirect_url .= $this->uri->segment(3) . '/';
        }

       

        $data = array(
            'email_message' => '',
            'password_message' => '',
        );

        // If there are no post request, just display login form.
        if ($this->input->server('REQUEST_METHOD') != 'POST') {
            $this->load->view("user/login", $data);
            return false;
        }

        $valid = true;

        // Define params
        $email = trim(strtolower($this->input->post("uemail", TRUE)));
        $password = $this->input->post("upw", TRUE);

        if ($email == '') {
            $data['email_message'] = 'This field is required.';
            $valid = false;
        }

        if ($password == '') {
            $data['password_message'] = 'This field is required.';
            $valid = false;
        }

        if ($valid == false) {
            $this->load->view("user/login", $data);
            return false;
        }

        // Call login function from user_auth library
        $ret = $this->user_auth->login($email, $password, DEFAULT_COMPANY);

        // User does not exists
        if ($ret === false) {

            $message = 'Invalid E-mail/password.';
            
        } elseif ($ret === null) {
            //account valid, something wrong
            $data['email_message'] = $this->user_auth->getLastError();
        } elseif (!empty($ret)) {

            $this->log_user_action($ret['id_user'], 'User logged in successfully.', LOG_TYPE_LOGIN);
            redirect(site_url('admin/dashboard'));
        }

        $this->load->view("user/login", $data);
        return false;
    }

    public function dashboard() {

        $this->user_auth->check_login();
        
        $nav_menu = $this->user_nav->get_assoc();
        $dashboard_buttons = build_dashboard_buttons_nav_part($nav_menu, 'employee');
      

        $buffer = $dashboard_buttons;

        $this->user_nav->run_page($buffer, 'Dashboard', false);
    }

    protected function attempts_check($email, $id_company) {

        $key = 'email_attemts_' . $email;

        // $this->session->sess_destroy(); return;

        $attempts = $this->session->userdata($key);

        $user = $this->u_model->fetch_account(array('email' => $email, 'id_company' => $id_company));

        // User with not exists.
        if (!$user) {
            return false;
        }

        $user = $user[0];

        if ($user->account_blocked == 1) {
            return '<strong>For security reasons, this account has been locked after multiple failed login attempts. Please check the registered email for password reset instructions.</strong>';
        }
        /* 		
          if($user->status == USER_STATUS_SUSPENDED) {
          return false;
          }
         */
        $message = '';

        if ($attempts == USER_LOGIN_ATTEMPTS) {

            $this->u_model->save(
                array(
                'status' => USER_STATUS_SUSPENDED,
                'account_blocked' => 1
                ), array(
                'email' => $email,
                'id_user' => $user->id_user
                )
            );

            $this->log_user_action($user->id_user, 'User account has been blocked as have exhausted the allowed log-in attempts.', LOG_TYPE_LOGIN);

            $this->user_login_attempts_reset_password_email($user);

            return '<strong>For security reasons, this account has been locked after multiple failed login attempts. Please check the registered email for password reset instructions.</strong>';
            // return $this->account_blocked_message;
        }

        if (!is_numeric($attempts)) {
            $attempts = 1;
        }

        if ($attempts > USER_LOGIN_ATTEMPTS) {
            return '<strong>For security reasons, this account has been locked after multiple failed login attempts. Please check the registered email for password reset instructions.</strong>';
        }

        $attempts++;

        $visitor_data = array(
            $key => $attempts
        );

        $this->session->set_userdata($visitor_data);

        $possible_attempts = (USER_LOGIN_ATTEMPTS - $attempts) + 1;

        return 'For security reasons, after ' . $possible_attempts . ' more failed login attempts this account will be locked and password reset instructions sent to the registered email.';
        // return 'You have '.$possible_attempts.' Attempts to login.';
    }

// End func attempts_check

    public function user_login_attempts_reset_password_email($user) {

        $this->load->library('mailgun_api');

        $sender = 'TalentBase Help Desk';

        $reset_password_url = site_url('user/reset_password') . '/' . $user->id_string . '/' . sha1($user->email . $user->id_string) . '/aac';

        $this->load->model('reference_model', 'ref');
        $company = $this->ref->fetch_all_records('companies', array('id_company' => $user->id_company));
        $company_name = $company[0]->company_name;

        $bdata = array(
            'full_name' => $user->first_name . ' ' . $user->last_name,
            'reset_password_url' => $reset_password_url,
            'company_name' => $company_name,
            'company_url' => 'https://' . $this->user_auth->get('company_id_string') . '.talentbase.ng/login',
        );

        $msg = $this->load->view('email_templates/user_forgot_password', $bdata, true);

        $this->mailgun_api->send_mail(
            array(
                'from' => $sender . ' <helpdesk@talentbase.ng>',
                'to' => $user->email,
                'subject' => 'Reset Forgotten Password: ' . $company_name . ' Staff Portal',
                'html' => '<html>' . $msg . '</html>'
            )
        );
    }

// End func user_login_attempts_reset_password_email


    protected function log_user_action($id_user, $message, $type = LOG_TYPE_MISC) {

        $this->u_model->log_db($id_user, $message, $type);
    }

// End func log_user_action

    /**
     * Forget password - for Admin and employee
     * 
     * @access public
     * @return void 
     */
    public function forgot_password() {

        $data = array(
            'message' => '',
            'success_message' => '',
            'email' => ''
        );

        // If there are no post request, just display form.
        if ($this->input->server('REQUEST_METHOD') != 'POST') {
            $this->load->view("user/forgot_password", $data);
            return false;
        }

        // Define params
        $email = trim(strtolower($this->input->post("uemail", TRUE)));

        if ($email == '') {
            $data['message'] = 'This field is required.';
            $this->load->view("user/forgot_password", $data);
            return false;
        }

        // Call login function from user_auth library
        $this->load->model('user/user_model', 'u_model');

        $user = $this->u_model->fetch_account(array('email' => $email));

        if (!$user) {
            $data['message'] = 'This email address not registered.';
            $data['email'] = $email;
            $this->load->view("user/forgot_password", $data);
            return false;
        }

        // Sending email.

        $this->load->library('mailgun_api');

        $sender = 'TalentBase Help Desk';

        $reset_password_url = site_url('user/reset_password') . '/' . $user[0]->id_string . '/' . sha1($email . $user[0]->id_string);

        $this->load->model('reference_model', 'ref');
        $company = $this->ref->fetch_all_records('companies', array('id_company' => $user[0]->id_company));
        $company_name = $company[0]->company_name;

        $bdata = array(
            'full_name' => $user[0]->first_name . ' ' . $user[0]->last_name,
            'reset_password_url' => $reset_password_url,
            'company_name' => $company_name,
            'company_url' => 'https://' . $this->user_auth->get('company_id_string') . '.talentbase.ng/login',
        );

        $msg = $this->load->view('email_templates/user_forgot_password', $bdata, true);

        $this->mailgun_api->send_mail(
            array(
                'from' => $sender . ' <helpdesk@talentbase.ng>',
                'to' => $email,
                'subject' => 'Reset Forgotten Password: ' . $company_name . ' Staff Portal',
                'html' => '<html>' . $msg . '</html>'
            )
        );

        $data['success_message'] = 'Email sent successfully.';
        $data['email'] = '';
        $this->load->view("user/forgot_password", $data);
        return false;
    }

// End func forget_password

    /**
     * Reset password - for Admin and employee
     * 
     * @access public
     * @return void 
     */
    public function reset_password($id_string, $key, $ac = NULL) {

        $this->load->model('user/user_model', 'u_model');

        $user = $this->u_model->fetch_account(array('id_string' => $id_string));
        //$user = $this->u_model->fetch_account(array('id_user' => $id_string));

        if (!$user) {
            show_404('page', false);
        }

        $db_key = sha1($user[0]->email . $id_string);

        if ($db_key != $key) {
            show_404('page', false);
        }

        $login_url = site_url('login');
        $login_title = 'Employee Login';

        $success_message = '';
        $error_message = '';

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $new_password = $this->input->post("password", TRUE);
            $confirm_password = $this->input->post("confirm_password", TRUE);

            if (strlen(trim($new_password)) < 6) {
                $error_message = 'Enter valid password (minimum 6 chars).';
            } elseif ($new_password != $confirm_password) {
                $error_message = 'Invalid confirmation.';
            } else {

                $data_ = array(
                    'password' => $this->user_auth->encrypt($new_password)
                );

                if ($ac == 'aac') {
                    $data_['account_blocked'] = 0;
                    $data_['status'] = USER_STATUS_ACTIVE;
                }


                $this->u_model->save(
                    $data_, array(
                    'id_string' => $id_string
                    )
                );

                $this->u_model->fetch_employee_sub_acccts($data_, $user[0]->email, $user[0]->id_company);

                redirect(site_url('login'));

                $success_message = 'Password changed successfully.';
            }
        }

        $bdata = array(
            'login_url' => $login_url,
            'login_title' => $login_title,
            'error_message' => $error_message,
            'success_message' => $success_message,
            'message' => $error_message
        );

        $this->load->view("user/reset_password", $bdata);

        return true;
    }

// End func reset_password

    public function access_to($c_id_string, $u_id_string) {

        $this->user_auth->check_login();
        $ret = $this->user_auth->login_by_cu_id_strings($c_id_string, $u_id_string);
        $this->redirect_user_by_type($ret['account_type']);
    }

// End func access_to

    /**
     * Redirect user to user;s page by account type.
     *
     * @access private
     * @param int $account_type
     */
    private function redirect_user_by_type($account_type, $redirect_url = '') {
        auth_redirect_by_user_type($account_type, $redirect_url);
    }

// End func redirect_user_by_type

    /**
     * Logout action - will redirect to login page.
     *
     * @access public
     * @return void
     * */
    public function logout() {
        // Call logout function from user_auth library
        $this->log_user_action($this->user_auth->get('id_user'), 'User logged out successfully.', LOG_TYPE_LOGOUT);
        $this->user_auth->logout();
        redirect(site_url('login'), 'refresh');
    }

// End func direct_access_by_id_string
}
