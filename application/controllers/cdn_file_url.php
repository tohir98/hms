<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Talentbase
 * Get file cdn temp url controller
 * 
 * @category   Controller
 * @package    Download
 * @subpackage CDN File get url
 * @author     David A. <tokernel@gmail.com>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @license    
 * @version    1.0.0
 * @link       
 * @since      File available since Release 1.0.0
 */
class Cdn_file_url extends CI_Controller {

    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();

        /**
         * Load libraries
         */
        $this->load->library(array('user_auth', 'admin_auth', 'cdn_api', 'session'));
    }

// End func __construct

    public function display_iframe() {

        $this->load->library('cdn_api');

        // Create a connection

        $is_resume = $this->input->post('is_resume');

        if ($is_resume == 1) {

            $this->load->model('recruiting/recruiting_model', 'r_model');

            $id_file = $this->input->post('id_file');
            $container_name = $this->r_model->fetch_container_by_id_file($id_file);

            // $container_name = RACKSPACE_CONTAINER_TEST_SITE_RESUME;
        } else {
            $container_name = $this->user_auth->get('cdn_container');
        }

        $this->cdn_api->connect(
                array(
                    'username' => RACKSPACE_USERNAME,
                    'api_key' => RACKSPACE_API_KEY,
                    'container' => $container_name
                )
        );

        $file_name = $this->input->post('file_url');

        $file_url = $this->cdn_api->temporary_url($file_name);
        $file_url = urlencode($file_url);

        echo '<iframe src="//docs.google.com/viewer?url=' . $file_url . '&embedded=true" width="100%" height="780" style="border: none;"></iframe>';
    }

// End func display_iframe

    /*
     * Access file directly.
     * Example: greenenergy.tb.com.ng/cdn_file_url/file_temp_url/__THIS_IS_TESTING_SHOULD_BE_REMOVED__/032b2cc936860b03048302d991c3498f.jpg
     * 
     * @access public
     * @return void
     */

    public function file_temp_url() {

        // Check login
        if (!$this->user_auth->logged_in() and $this->admin_auth->logged_in()) {
            exit('Authentication required');
        }

        // get file name from url
        $container = $this->uri->segment(3);
        $file = $this->uri->segment(4);

        if ($container == '') {
            exit('Container not found.');
        }

        if ($file == '') {
            exit('File not found.');
        }

        // get file temp url and access
        // Create a connection
        $this->cdn_api->connect(
                array(
                    'username' => RACKSPACE_USERNAME,
                    'api_key' => RACKSPACE_API_KEY,
                    'container' => $container
                )
        );

        $file_temp_url = $this->cdn_api->temporary_url($file);

        if ($file_temp_url == '') {
            exit('File not found.');
        }

        header('Location: ' . $file_temp_url);
        exit();
    }

// End func file_temp_url
}

// End class download
