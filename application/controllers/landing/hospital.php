<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of hospital
 *
 * @author TOHIR
 */
class hospital extends CI_Controller {

    public function __construct() {
        parent::__construct();

//        $this->load->library(array('user_auth', 'cdn_api'));
//
//        $this->load->helper('url');
    }
    
    public function welcome(){
        $hdata = [];
        $bdata = [];
        
        $this->load->view('hospital_templates/header', $hdata);
        $this->load->view('hospital_templates/body', $bdata);
        $this->load->view('hospital_templates/footer');
    }

}
