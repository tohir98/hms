<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Talentbase
 * Vacations Controlleir
 * 
 * @category   Controller
 * @package    Vacation
 * @subpackage Vacation
 * @author     Tunmise Akinsola <tunmise.akinsola@talentbase.ng>
 * @copyright  Copyright � 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

class Vacation extends CI_Controller {



    private $id_company;
    private $company_name;
    private $id_user_logged_in;
    private $company_url;
    private $sms_switch;

	
    /*
     * Class constructor
     * 
     * @access public 
     * @retun void
     */
    public function __construct() {

        parent::__construct();

        /*
         * Load libraries
         */
        $this->load->library(array('user_nav', 'sms_lib', 'mixpanel', 'tasks', 'bugsnag', 'nexmo', 'company_module_settings'));
        /*
         *  Load helper
         */
        $this->load->helper(array('vacation_helper', 'form_helper', 'notification_helper'));
        /*
         *  Load models
         */
        $this->load->model('vacation/vacation_model', 'v_model');

        $this->id_user_logged_in = $this->session->userdata('id_user');
        $this->id_company = $this->user_auth->get('id_company');
        $this->company_name = $this->user_auth->get('company_name');
        $this->company_url = site_url('login');
        $switch = $this->v_model->get('module_notification_settings', array('id_company'=>$this->id_company, 'id_module'=>2));
        $this->sms_switch = $switch[0]->status;

    }// End func __construct

    /*
     * View Employee Vacation (entitlements, planner, request, history)
     * 
     * @access public 
     * @return void
     */
    public function my_vacation(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_VIEW_MY);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        //get employee entitlements
        //if empty check if assigned from compensation
        $us = $this->v_model->get_employee_info($this->id_company, $this->user_auth->get('id_user'));
        $ent = $this->v_model->get_employee_entitlements($this->user_auth->get('id_user'));
        //print_r($us); die();
        if(empty($ent) AND !empty($us->id_pay_structure)){
            
            $role_vacation = $this->v_model->get_single_role_vacation($this->id_company, $us->id_pay_structure);
            foreach ($role_vacation as $rv) {
                if($rv->no_of_days > 0){
                    $allc_data = array(
                            'id_type' => $rv->id_vacation_type,
                            'id_company' => $rv->id_company,
                            'id_user' => $us->id_user,
                            'no_of_days' => $rv->no_of_days
                        );

                    $this->v_model->add('vacation_allocated', $allc_data);
                }
            }
        }

        $year = ($this->input->post('year')) ? $this->input->post('year') : '' ;
        $this->_update_leave_allocation($us->id_user);
        
        $period = date('Y');
        $data['vacation_types'] = $this->v_model->get_employee_vacation_types($this->user_auth->get('id_user'));
        $data['entitlements'] = $this->v_model->get_employee_entitlements($this->user_auth->get('id_user'), $year);
        $data['taken'] = $this->v_model->get_taken_vacations($this->user_auth->get('id_user'));
        $data['history'] = $this->v_model->get_employee_vacation_history($this->user_auth->get('id_user'));
        $data['pending_approvals'] = $this->v_model->get_employee_pending_vacation($this->user_auth->get('id_user'));
        $data['holidays'] = $this->v_model->get_holidays($this->id_company);     
        $data['request'] = $this->v_model->check_request($this->user_auth->get('id_user')); 
        $data['vacation_plan'] = $this->v_model->get('vacation_plan_request', array('id_company'=>$this->id_company, 'status'=> 1));
        $data['current_leave_year'] = ($this->input->post('year')) ? $this->input->post('year') : date('Y') ;
        $data['current_leave'] = $this->v_model->get_employee_leave_allowance($this->user_auth->get('id_user'), $this->id_company, $period);
        $data['relief'] = $this->company_module_settings->get($this->id_company, 2, 'relief');
        $data['users'] = $this->v_model->get('users', array('id_company'=>$this->id_company));

        $account_type = $this->user_auth->get_account_type($this->session->userdata('account_type'));
        if(!$account_type){
            $acct = "";
        }
        else{
            $acct = $account_type->name;
        }
        $this->mixpanel->track('page load',  array(
            "distinct_id" =>  $this->session->userdata('id_string'),
            "module id"=>"2",
            "module title"=>"Vacation",
            "page title" => "My Vacation", 
            "page id" => "2"
        ));

        $buffer = $this->load->view('vacation/my_vacation', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func my_vacation



    /*
     * Make vacation request
     * 
     * @access public 
     * @return void
     */
    public function vacation_request(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_VIEW_MY);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }    

        
        $ret = true;

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect(site_url('vacation/my_vacation'));
        } 
        
        $id_type = $this->input->post('type', true);
        $date_start = $this->input->post('start_date', true);
        $date_end = $this->input->post('end_date', true);
        $description = $this->input->post('desc', true);
        $id_user = $this->input->post('id_user');
        $admin_request = $this->input->post('is_admin_request');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('type', '', 'trim|required');
        $this->form_validation->set_rules('desc', '', 'trim|required');
        $this->form_validation->set_rules('start_date', '', 'trim|required');
        $this->form_validation->set_rules('end_date', '', 'trim|required');
    
        if($this->form_validation->run()==FALSE){
            $errors = "Please fill the form correctly.";
            $this->session->set_flashdata('msg', $errors);
            $this->session->set_flashdata('type', 'error');
            redirect('vacation/my_vacation', 'refresh');
            return;
        }

        $link = anchor('vacation/pending_approvals', 'You have a vacation request pending.');
        $manager = $this->v_model->get('employees', array('id_user'=>$this->user_auth->get('id_user')));  
        $admin = $this->v_model->get('employees', array('id_access_level'=>1, 'id_company'=>$this->id_company));    
        
        if(!$this->input->post('is_admin_request')){

            $alloc = $this->v_model->get_allocated_sum($this->id_company, $id_user);
            $used = $this->v_model->get_pending_used_sum($this->id_company, $id_user);
            $holi = $this->v_model->get_holidays($this->id_company);
            $days = get_working_days($date_start, $date_end, $holi);

            if($used > $alloc){
                $errors = "You have used up all allocated days for the year.";
                $this->session->set_flashdata('msg', $errors);
                $this->session->set_flashdata('type', 'error');

                if($admin){
                    redirect('vacation/employee_vacation', 'refresh');
                }
                else{
                    redirect('vacation/my_vacation', 'refresh');
                }
                
                return;
            }

            //add request to database
            $ret = $this->v_model->vacation_request( 
                    array(                                    
                        'id_company' => $this->user_auth->get('id_company'),
                        'id_user' => $this->user_auth->get('id_user'),
                        'date_start' => date('Y-m-d', strtotime($date_start)),
                        'date_end' => date('Y-m-d', strtotime($date_end)),
                        'description' => $description,
                        'id_type' => $id_type
                    )
                );
                    
            //get the next stage approver
            $stage_count = $this->v_model->get_last_stage($this->id_company);
            //for ($i=1; $i <= $stage_count; $i++) { 
                $app = $this->v_model->get('vacation_request_stages', array('id_company'=>$this->id_company, 'stage'=>1));

                if($app[0]->approver == 'Unit Manager'){
                    $id_manager = $manager[0]->id_manager;
                }
                elseif($app[0]->approver == 'Administrator'){
                    $id_manager = $admin[0]->id_user;
                }
                else{
                    $id_manager = $app[0]->id_user;
                }

                $status = $this->v_model->user_has_access($id_manager);
                $first_name =$this->user_auth->get('display_name');
                $last_name =$this->user_auth->get('last_name');
                $company_id_string = $this->user_auth->get('company_id_string');
                $approval_stage = 1;

                if($status == 1){
                    $appr = $this->v_model->get_employee_info($this->id_company, $id_manager);
                    $sms_content = "{$first_name} {$last_name} at {$this->company_name} has sent a vacation request. "
                            . "Kindly log into the employee portal at {$this->company_url} to approve or decline the request.";
                    $email_content = 'email_templates/vacation_request';
                    
                }
                else{
                    
                    $app = $this->v_model->get('users', array('account_type'=>1, 'id_company'=>$this->id_company));
                    $appr = $this->v_model->get_employee_info($this->id_company, $app[0]->id_user);
                    $approval_stage = 1;
                    $sms_content = "{$first_name} {$last_name} at {$this->company_name} has sent a vacation request. "
                            . "Kindly log into the employee portal at {$this->company_url} to approve or decline the request.";
                    $email_content = 'email_templates/vacation_request_not_approver';
                }

            //add approver
            $dt = array(
                        'id_approver' => $appr->id_user,
                        'id_user' => $this->id_user_logged_in,
                        'id_vacation'=> $ret,
                        'stage'=> $approval_stage,
                        'status'=> 0
                    );

            $in = $this->v_model->add('vacation_request_approver', $dt, true);
            
            //add task
            $task[] = array(
                            'id_user_from' => $this->id_user_logged_in,
                            'id_user_to' => $appr->id_user,
                            'id_module' => 2,
                            'subject' => $link,
                            'status' => 2,
                            'tb_name' => 'vacation_request_approver',
                            'tb_column_name' => 'id_request_approver',
                            'tb_id_record' => $in,
                            'date_will_end' => date('Y-m-d', strtotime('+ 2 days'))
                        );
            $this->tasks->push_task($task);

            //send sms
            $phone = $this->user_auth->get_employee_phone_number($appr->id_user);
            $phone = str_replace(array('(', ')', ' ', '-'), '', $phone);

            if(!empty($phone) && $this->sms_switch > 0) {
                $sms_params['id_module'] = VACATION_MODULE;                
                $sms_params['message'] = $sms_content;
                $sms_params['recipients']['id_user'] = $appr->id_user;
                $sms_params['recipients']['number'] = '+'.$phone;
                $sms_params['sender'] = substr($this->company_name, 0, 11);
                $this->sms_lib->send_sms($sms_params);
                
            }

            //send email
            if($appr->email){
                
                $employee = $this->v_model->get_employee_details($this->id_user_logged_in);
                $manager = $this->v_model->get_employee_details($appr->id_user);           
                $employee_office = $this->v_model->get_employee_company_details($this->id_user_logged_in);
                $subject = "Vacation Request From ". ucwords($employee->first_name." ".$employee->last_name);
                $k = array(
                        'manager_name' => ucwords($manager->first_name." ".$manager->last_name),
                        'description' => $description,
                        'start_date' => $date_start,
                        'end_date' => $date_end,
                        'employee_name' => ucwords($employee->first_name." ".$employee->last_name),
                        'subdomain' => $employee_office->id_string,
                        'company_name' => $this->company_name,
                        'role' => $employee_office->roles,
                        'department' => $employee_office->departments
                    );
                $msg = $this->load->view($email_content, $k, true);

                $this->load->library('mailgun_api');
                
                $this->mailgun_api->send_mail(
                                        array(
                                            'from' => $this->company_name.' <noreply@testbase.com.ng>',
                                            'to' => $appr->email,
                                            'subject' => $subject,
                                            'html' => '<html>'. $msg .'</html>'
                                        ));
            }

        }
        else{

            $em = $this->input->post('is_admin_request');//admin id
            $usr = $this->v_model->get_employee_details($em);
            $ret = $this->v_model->vacation_request( $this->user_auth->get('id_company'), 
                    array(                                    
                        'id_company' => $this->user_auth->get('id_company'),
                        'id_user' => $usr->id_user,
                        'date_start' => date('Y-m-d', strtotime($date_start)),
                        'date_end' => date('Y-m-d', strtotime($date_end)),
                        'description' => $description,
                        'id_type' => $id_type,
                        'status' => 'Approved'
                    )

                );


            //approve request
            
            $sms_params['id_module'] = VACATION_MODULE;
            
            $usr = $this->v_model->get_employee_details($em);
            
            //send sms to requestee
            $phone = $this->user_auth->get_employee_phone_number($usr->id_user);
            $phone = str_replace(array('(', ')', ' ', '-'), '', $usr->work_number);
            
            if(!empty($phone) && $this->sms_switch > 0){
                $company_id_string =$this->user_auth->get('company_id_string');
                
                $date_start = date('M j', strtotime($date_start));
                $date_end = date('M j', strtotime($date_end));
                $message = "Your vacation request for {$date_start} to {$date_end} has been approved by your manager/admin. "
                        . "Kindly log into the employee portal at {$this->company_url} to view the details.";
                $sms_params['message'] = $message;
                $sms_params['recipients']['id_user'] = $usr->id_user;
                $sms_params['recipients']['number'] = '+'.$phone;
                $sms_params['sender'] = substr($this->company_name, 0, 11);
                $res = $this->sms_lib->send_sms($sms_params);
            }

        }
        
         //add mixpanel
        $account_type = $this->user_auth->get_account_type($this->session->userdata('account_type'));
        if(!$account_type){
            $acct = "";
        }
        else{
            $acct = $account_type->name;
        }
        $this->mixpanel->track('Applied for Leave Request', array(
            "distinct_id" =>  $this->session->userdata('id_string')
        ));


        $success_msg = 'Vacation requested successful!';
        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('msg', $success_msg);

        redirect(site_url('vacation/my_vacation'));

    }// End func vacation_request


    /*
     * Make vacation request
     * 
     * @access public 
     * @return void
     */
    public function vacation_request_ajax(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_VIEW_MY);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }    


        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect(site_url('vacation/my_vacation'));
        } 
        
        $id_type = $this->input->post('type', true);
        $date_start = $this->input->post('start_date', true);
        $date_end = $this->input->post('end_date', true);
        $description = $this->input->post('desc', true);
        $id_user = $this->input->post('id_user');
        $admin_request = $this->input->post('is_admin_request');
        if ($this->input->post('relief')) {
            $id_relief = $this->input->post('relief');
        }


        $this->load->library('form_validation');
        $this->form_validation->set_rules('type', '', 'trim|required');
        $this->form_validation->set_rules('desc', '', 'trim|required');
        $this->form_validation->set_rules('start_date', '', 'trim|required');
        $this->form_validation->set_rules('end_date', '', 'trim|required');

        if($this->form_validation->run()==FALSE){

            echo 1;
            return;
        }

        $link = anchor('vacation/pending_approvals', 'You have a vacation request pending.');
        $manager = $this->v_model->get('employees', array('id_user'=>$this->user_auth->get('id_user')));  
        $admin = $this->v_model->get('employees', array('id_access_level'=>1, 'id_company'=>$this->id_company));    

        $alloc = $this->v_model->get_allocated_sum($this->id_company, $id_user);
        $used = $this->v_model->get_pending_used_sum($this->id_company, $id_user);
        $holi = $this->v_model->get_holidays($this->id_company);
        $days = get_working_days($date_start, $date_end, $holi);
        $days_left = $alloc[0]->no_of_days - $used[0]->no_of_days;

        if($days > $days_left){
            
            echo 2;
            return;
        }


        if(isset($id_relief)){

            $ur = $this->v_model->get('users', array('id_user'=>$id_relief, 'id_company'=>$this->id_company));
            
            if(!empty($ur)){
                $vacs = $this->v_model->get_vac_period($id_relief, $this->id_company, $date_start, $date_end);

                if(!empty($vacs)){
                    echo 4;
                    return;
                }
            }
        }

        //add request to database
        $request_data = array(                                    
                    'id_company' => $this->id_company,
                    'id_user' => $id_user,
                    'date_start' => date('Y-m-d', strtotime($date_start)),
                    'date_end' => date('Y-m-d', strtotime($date_end)),
                    'description' => $description,
                    'id_type' => $id_type,
                    'no_of_days' => $days
                );
        if (isset($id_relief)) {
            $request_data['id_relief'] = $id_relief;
        }

        $ret = $this->v_model->vacation_request($request_data);
                    
        //get the next stage approver
        $stage_count = $this->v_model->get_last_stage($this->id_company);
       
        $app = $this->v_model->get('vacation_request_stages', array('id_company'=>$this->id_company, 'stage'=>1));

        if($app[0]->approver == 'Unit Manager'){
            $id_manager = $manager[0]->id_manager;
        }
        elseif($app[0]->approver == 'Administrator'){
            $id_manager = $admin[0]->id_user;
        }
        else{
            $id_manager = $app[0]->id_user;
        }

        $status = $this->v_model->user_has_access($id_manager);
        $first_name =$this->user_auth->get('display_name');
        $last_name =$this->user_auth->get('last_name');
        $company_id_string = $this->user_auth->get('company_id_string');
        $approval_stage = 1;

        if($status == 1){
            $appr = $this->v_model->get_employee_info($this->id_company, $id_manager);
            $sms_content = "{$first_name} {$last_name} at {$this->company_name} has sent a vacation request. "
                    . "Kindly log into the employee portal at {$this->company_url} to approve or decline the request.";
            $email_content = 'email_templates/vacation_request';
            
        }
        else{
            
            $app = $this->v_model->get('users', array('account_type'=>1, 'id_company'=>$this->id_company));
            $appr = $this->v_model->get_employee_info($this->id_company, $app[0]->id_user);
            $approval_stage = 1;
            $sms_content = "{$first_name} {$last_name} at {$this->company_name} has sent a vacation request. "
                    . "Kindly log into the employee portal at {$this->company_url} to approve or decline the request.";
            $email_content = 'email_templates/vacation_request_not_approver';
        }

        //add approver
        $dt = array(
                    'id_approver' => $appr->id_user,
                    'id_user' => $this->id_user_logged_in,
                    'id_vacation'=> $ret,
                    'stage'=> $approval_stage,
                    'status'=> 0
                );

        $in = $this->v_model->add('vacation_request_approver', $dt, true);
      
        //add task
        $task[] = array(
                        'id_user_from' => $this->id_user_logged_in,
                        'id_user_to' => $appr->id_user,
                        'id_module' => 2,
                        'subject' => $link,
                        'status' => 2,
                        'tb_name' => 'vacation_request_approver',
                        'tb_column_name' => 'id_request_approver',
                        'tb_id_record' => $in,
                        'date_will_end' => date('Y-m-d', strtotime('+ 2 days'))
                    );
        $this->tasks->push_task($task);

        //send sms
        $phone = $this->user_auth->get_employee_phone_number($appr->id_user);
        $phone = str_replace(array('(', ')', ' ', '-'), '', $phone);
            
        if(!empty($phone) && $this->sms_switch > 0) {
            $sms_params['id_module'] = VACATION_MODULE;
            
            $sms_params['message'] = $sms_content;
            $sms_params['recipients']['id_user'] = $appr->id_user;
            $sms_params['recipients']['number'] = '+'.$phone;
            $sms_params['sender'] = substr($this->company_name, 0, 11);
            $this->nexmo->send_sms($sms_params);
            
        }

        //send email
        if($appr->email){

            $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
            $domainName = $_SERVER['HTTP_HOST'].'/';
            
            $employee = $this->v_model->get_employee_details($id_user);
            $manager = $this->v_model->get_employee_details($appr->id_user);           
            $employee_office = $this->v_model->get_employee_company_details($this->id_user_logged_in);
            $subject = "Vacation Request From ". ucwords($employee->first_name." ".$employee->last_name);
            $k = array(
                    'manager_name' => ucwords($manager->first_name." ".$manager->last_name),
                    'description' => $description,
                    'start_date' => $date_start,
                    'end_date' => $date_end,
                    'employee_name' => ucwords($employee->first_name." ".$employee->last_name),
                    'subdomain' => $employee_office->id_string,
                    'domain' => $protocol.$domainName,
                    'company_name' => $this->company_name,
                    'role' => $employee_office->roles,
                    'department' => $employee_office->departments
                );
            $msg = $this->load->view($email_content, $k, true);

            $this->load->library('mailgun_api');
            
            $this->mailgun_api->send_mail(
                                    array(
                                        'from' => $this->company_name.' <noreply@testbase.com.ng>',
                                        'to' => $appr->email,
                                        'subject' => $subject,
                                        'html' => '<html>'. $msg .'</html>'
                                    ));
        }


        echo 3;

    }// End func vacation_request



    /*
     * Make vacation request reminder
     * 
     * @access public 
     * @return void
     */
    public function vacation_request_reminder(){

       // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_VIEW_MY);          

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $id_vacation =  $this->input->post('id_vacation');
        $v = $this->v_model->get_vacation($id_vacation);
        
        $date_start = date('Y-m-d', strtotime($v->date_start));
        $date_end = date('Y-m-d', strtotime($v->date_end));
        $description = $v->description;
        
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $domainName = $_SERVER['HTTP_HOST'].'/';

        $manager = $this->v_model->get_employee_manager($this->user_auth->get('id_user'));    
        $employee = $this->v_model->get_employee_details($this->id_user_logged_in);
        $manager = $this->v_model->get_employee_details($manager->id_user);
        $employee_office = $this->v_model->get_employee_company_details($this->id_user_logged_in);
        $subject = "Vacation Request Reminder From ". ucwords($employee->first_name." ".$employee->last_name);
        $k = array(
                'manager_name' => ucwords($manager->first_name." ".$manager->last_name),
                'description' => $description,
                'start_date' => $date_start,
                'end_date' => $date_end,
                'employee_name' => ucwords($employee->first_name." ".$employee->last_name),
                'subdomain' => $employee_office->id_string,
                'domain' => $protocol.$domainName,
                'role' => $employee_office->roles,
                'department' => $employee_office->departments,
                'company_name' => $this->company_name
            );
        $msg = $this->load->view('email_templates/vacation_request', $k, true);

        $this->load->library('mailgun_api');
        $this->mailgun_api->send_mail(
                                array(
                                    'from' => $this->company_name.' <noreply@testbase.com.ng>',
                                    'to' => $manager->email,
                                    'subject' => $subject,
                                    'html' => '<html>'. $msg .'</html>'
                                ));

        $success_msg = 'Vacation request reminder sent successful!';
        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('msg', $success_msg);


        redirect(site_url('vacation/my_vacation'));

    }// End func vacation_request_reminder


    
    /*
     * Delete a vacation request
     * 
     * @access public
     * @return void
     */
    public function delete_vacation_request(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_VIEW_MY);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $ret = false;
        $id_vacation = $this->input->post('id_vacation', true);

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/my_vacation', 'refresh');
        } 
        
        
        $ret = $this->v_model->delete_vacation_request(
                                    array(                                    
                                        'id_vacation' => $id_vacation,                                
                                    ));

        //delete vacation task
        if(!$ret){
            $error_msg = 'Error! Unable to delete vacation request!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        }else {

            //delete task
            $tr = $this->v_model->get('vacation_request_approver', array(
                                                                        'id_vacation'=>$id_vacation, 
                                                                        'status' => 0

                                                                        ));
            $t = $this->v_model->get('tasks', array('tb_name'=> 'vacation_request_approver', 'tb_column_name'=> 'id_request_approver', 'tb_id_record' => $tr[0]->id_request_approver));
            if(isset($t)){
                $this->v_model->delete('tasks', array('id_task'=>$t[0]->id_task));
            }

            $success_msg = 'Vacation request deleted successfully!';
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('msg', $success_msg);
        }

        redirect('vacation/my_vacation');

    }// End func delete_vacation_request



    /*
     * Update a vacation request
     * 
     * @access public
     * @return void
     */
    public function update_vacation_request(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_VIEW_MY);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $ret = false;

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        } else {
            $id_vacation = $this->input->post('id_vacation', true);

            $date_start = $this->input->post('edit_start_date', true);
            $date_end = $this->input->post('edit_end_date', true);
            $description = $this->input->post('edit_desc', true);
            $id_type = $this->input->post('edit_type', true);
            if ($this->input->post('relief')) {
                $id_relief = $this->input->post('relief');
            }
        
            
            $request_data = array(
                            'date_start' => date('Y-m-d', strtotime($date_start)),
                            'date_end' => date('Y-m-d', strtotime($date_end)),
                            'description' => $description,
                            'id_type' => $id_type
                        );
            if (isset($id_relief)) {
                $request_data['id_relief'] = $id_relief;
            }
            $ret = $this->v_model->update_vacation_request($id_vacation, $request_data);
        
        }

        if(!$ret){
            $error_msg = 'Error! Unable to update vacation request!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        }else {
            $success_msg = 'Vacation request updated successfully!';
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('msg', $success_msg);
        }

        redirect(site_url('vacation/my_vacation'));

    }// End func update_vacation_request
    
    /*
     * Approve vacation request
     * 
     * @access public 
     * @return void
     */
    public function vacation_request_approve(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $ret = true;

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/pending_approvals', 'refresh');
        } 
        
        $id_vacation = $this->input->post('id_vacation_approve');
        $id_user = $this->input->post('id_user');
        $id_admin = $this->v_model->get('users', array('account_type'=>1, 'id_company'=>$this->id_company));

        //get stage
        $tr = $this->v_model->get('vacation_request_approver', array(
                                                                    'id_vacation'=>$id_vacation, 
                                                                    'status' => 0

                                                                    ));
      
        $stages = $this->v_model->get_rlast_stage($this->id_company);

        //get number of stages approved
        if($tr[0]->stage == $stages){ //if last stage
            $data = array(
                'status' => 2
            );

            if($tr[0]->id_approver == $this->id_user_logged_in){

                $this->v_model->update('vacation_request_approver', array('status'=>'2'), array('id_vacation'=>$id_vacation, 'id_user'=>$id_user));
                $q = $this->v_model->update('vacation', array('status'=>'Approved'), array('id_vacation'=>$id_vacation));
                
                if($q){

                    $vac_detail = $this->v_model->get_vacation_details($id_vacation);
                    $phone = $this->user_auth->get_employee_phone_number($vac_detail->id_user);
                    $phone = str_replace(array('(', ')', ' ', '-'), '', $phone);
                    
                    if($vac_detail && !empty($phone) && $this->sms_switch > 0) {
                        $sms_params['id_module'] = VACATION_MODULE;
                        
                        $company_id_string =$this->user_auth->get('company_id_string');
                        $date_start = date('M j', strtotime($vac_detail->date_start));
                        $date_end = date('M j', strtotime($vac_detail->date_end));
                        $message = "Your vacation request for {$date_start} to {$date_end} has been approved by your manager/admin. "
                                . "Kindly log into the employee portal at {$this->company_url} to view the details.";
                        $sms_params['message'] = $message;
                        $sms_params['recipients']['id_user'] = $vac_detail->id_user;
                        $sms_params['recipients']['number'] = '+'.$phone;
                        $sms_params['sender'] = substr($this->company_name, 0, 11);
                        $this->sms_lib->send_sms($sms_params);

                    }  

                    //update task
                    $t = $this->v_model->get('tasks', array('tb_name'=>'vacation_request_approver', 'tb_column_name'=>'id_request_approver', 'tb_id_record'=>$tr[0]->id_request_approver));
                    $task = array(
                            'id_task' => $t[0]->id_task,
                            'status' => TASK_STATUS_COMPLETE
                        );

                    $this->tasks->update_task($task);
                    
                    $employee = $this->v_model->get_employee_details($vac_detail->id_user);
                    $subject = "Vacation Request Approved ";
                    $k = array(
                            'description' => $vac_detail->description,
                            'start_date' => $date_start,
                            'end_date' => $date_end,
                            'company_name' => $this->company_name
                        );
                    $msg = $this->load->view('email_templates/vacation_approved', $k, true);

                    $this->load->library('mailgun_api');
                    $this->mailgun_api->send_mail(
                                            array(
                                                'from' => $this->company_name.' <noreply@testbase.com.ng>',
                                                'to' => $employee->email,
                                                'subject' => $subject,
                                                'html' => '<html>'. $msg .'</html>'
                                            ));
                    
                    $success_msg = 'Vacation request approved successful!';
                    $this->session->set_flashdata('type', 'success');
                    $this->session->set_flashdata('msg', $success_msg);

                    $account_type = $this->user_auth->get_account_type($this->session->userdata('account_type'));
                    if(!$account_type){
                        $acct = "";
                    }
                    else{
                        $acct = $account_type->name;
                    }
                    // $this->mixpanel->track('Approved Vacation Request', array(
                    //     "name"=> $this->session->userdata('display_name').' '. $this->session->userdata('last_name'),
                    //     "ip_address" => $this->session->userdata('ip_address'),
                    //     "access_level" =>  $acct,
                    //     "distinct_id" =>  $this->session->userdata('id_string')
                    // ));
                }
            }

        }
        else{

            $next_stage = $tr[0]->stage + 1;
            if($tr[0]->id_approver == $this->id_user_logged_in){
                $this->v_model->update('vacation_request_approver', array('status'=>1), array('id_vacation'=>$id_vacation, 'stage'=>$tr[0]->stage));

                //for ($i=$next_stage; $i <= $stages; $i++) { 
                        
                $st = $this->v_model->get('vacation_request_stages', array('id_company'=>$this->id_company, 'stage'=>$next_stage));
                $vac = $this->v_model->get('vacation', array('id_vacation'=>$id_vacation));

                if($st[0]->approver == 'Unit Manager'){
                    $employee = $this->v_model->get('employees', array('id_user'=>$vac[0]->id_user));
                    $id_approver = $employee[0]->id_manager;
                }
                elseif($st[0]->approver == 'Administrator'){
                    $id_approver = $id_admin[0]->id_user;
                }
                else{
                    $id_approver = $st[0]->id_user;
                }

                $status = $this->v_model->user_has_access($id_approver);
                $company_id_string =$this->user_auth->get('company_id_string');
                if($status == 1){
                    $em = $this->v_model->get_employee_info($this->id_company, $vac[0]->id_user);
                    $appr = $this->v_model->get_employee_info($this->id_company, $id_approver);
                    $sms_content = "{$em->first_name} {$em->last_name} at {$this->company_name} has sent a vacation request. "
                                . "Kindly log into the employee portal at {$this->company_url} to approve or decline the request.";
                    $email_content = 'email_templates/vacation_request';
                    //break;
                }//}if(!isset($em) OR !isset($appr))
                else{
                    $appr = $this->v_model->get_employee_info($this->id_company, $id_admin[0]->id_user);
                    $em = $this->v_model->get_employee_info($this->id_company, $id_user);
                    $sms_content = "{$em->first_name} {$em->last_name} at {$this->company_name} has sent a vacation request. "
                                . "Kindly log into the employee portal at {$this->company_url} to approve or decline the request.";
                    $email_content = 'email_templates/vacation_request_not_approver';
                }
                
                $vac_detail = $this->v_model->get_vacation_details($id_vacation);

                //update task
                $t = $this->v_model->get('tasks', array('tb_name'=>'vacation_request_approver', 'tb_column_name'=>'id_request_approver', 'tb_id_record'=>$tr[0]->id_request_approver));
                $task = array(
                        'id_task' => $t[0]->id_task,
                        'status' => TASK_STATUS_COMPLETE
                    );
                $this->tasks->update_task($task);

                //send sms
                $phone = $this->user_auth->get_employee_phone_number($appr->id_user);
                $phone = str_replace(array('(', ')', ' ', '-'), '', $phone);
                    
                if(!empty($phone) && $this->sms_switch > 0) {
                    $sms_params['id_module'] = VACATION_MODULE;
                    $date_start = date('M j', strtotime($vac_detail->date_start));
                    $date_end = date('M j', strtotime($vac_detail->date_end));
                    $sms_params['message'] = $sms_content;
                    $sms_params['recipients']['id_user'] = $appr->id_user;
                    $sms_params['recipients']['number'] = '+' .$phone;
                    $sms_params['sender'] = substr($this->company_name, 0, 11);
                    $this->sms_lib->send_sms($sms_params);

                }

                $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
                $domainName = $_SERVER['HTTP_HOST'].'/';

                //send email
                $subject = "Vacation Request Approval ";
                $k = array(
                    'employee_name' => $em->first_name.' '.$em->last_name,
                    'company_name' => $this->company_name,
                    'subdomain' => $this->user_auth->get('company_id_string'),
                    'domain' => $protocol.$domainName,
                    'department' => $em->departments,
                    'role' => $em->roles,
                    'location' => $em->location_tag,
                    'description' => $vac_detail->description,
                    'start_date' => date('M j', strtotime($vac_detail->date_start)),
                    'end_date' => date('M j', strtotime($vac_detail->date_end))
                    );
                $msg = $this->load->view($email_content, $k, true);

                $this->load->library('mailgun_api');
                $this->mailgun_api->send_mail(
                                        array(
                                            'from' => $this->company_name.' <noreply@testbase.com.ng>',
                                            'to' => $appr->email,
                                            'subject' => $subject,
                                            'html' => '<html>'. $msg .'</html>'
                                        ));

                $link = anchor('vacation/pending_approvals', 'You have a vacation request pending.');
               
                //data for insert
                $approver_list = array(
                        'id_approver' => $appr->id_user,
                        'id_user' => $id_user,
                        'id_vacation'=> $id_vacation,
                        'stage'=> $next_stage,
                        'status'=> 0
                    );
                
                $in = $this->v_model->add('vacation_request_approver', $approver_list, true);

                $task_data[] = array(
                                'id_user_from' => $id_user,
                                'id_user_to' => $appr->id_user,
                                'id_module' => 2,
                                'subject' => $link,
                                'status' => 2,
                                'tb_name' => 'vacation_request_approver',
                                'tb_column_name' => 'id_request_approver',
                                'tb_id_record' => $in,
                                'date_will_end' => date('Y-m-d', strtotime('+ 2 days'))
                            );
                $this->tasks->push_task($task_data);
            }
            else{
                $success_msg = 'You do not have approval access!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $success_msg);
            }
        }

        $success_msg = 'Vacation request approved successful!';
        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('msg', $success_msg);
 
        redirect(site_url('vacation/pending_request'));

    }// End func vacation_request_approve
    
    /*
     * Decline vacation request
     * 
     * @access public 
     * @return void
     */
    public function vacation_request_decline(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }
        
        $ret = false;

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        } else {
            $id_vacation = $this->input->post('id_vacation_decline', true);

            $ret = $this->v_model->vacation_request_decline($id_vacation, 
                    array(
                        'status' => 'Declined'
                    ));
        }

        if(!$ret){
            $error_msg = 'Error! Unable to decline vacation request!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        }else {
            
            $vac_detail = $this->v_model->get_vacation_details($id_vacation);
            $phone = $this->user_auth->get_employee_phone_number($vac_detail->id_user);
            $phone = str_replace(array('(', ')', ' ', '-'), '', $phone);
                
            if($vac_detail && isset($phone) && $this->sms_switch > 0) {
                $sms_params['id_module'] = VACATION_MODULE;
                
                $company_id_string =$this->user_auth->get('company_id_string');
                $date_start = date('M j', strtotime($vac_detail->date_start));
                $date_end = date('M j', strtotime($vac_detail->date_end));
                $message = "Your vacation request for {$date_start} to {$date_end} has been declined by your manager/admin. "
                        . "Kindly log into the employee portal at {$this->company_url} to view the details.";
                $sms_params['message'] = $message;
                $sms_params['recipients']['id_user'] = $vac_detail->id_user;
                $sms_params['recipients']['number'] = '+' .$phone;
                $sms_params['sender'] = substr($this->company_name, 0, 11);
                $this->sms_lib->send_sms($sms_params);

            }  

            //update task
            $tr = $this->v_model->get('vacation_request_approver', array(
                                                                    'id_vacation'=>$id_vacation, 
                                                                    'status' => 0

                                                                    ));
            $t = $this->v_model->get('tasks', array('tb_name'=>'vacation_request_approver', 'tb_column_name'=>'id_request_approver', 'tb_id_record'=>$tr[0]->id_request_approver));
            //print_r($t); die();
            $task = array(
                    'id_task' => $t[0]->id_task,
                    'status' => 1
                );

            $this->tasks->update_task($task);
            //$this->v_model->delete('tasks', array('id_task'=>$t[0]->id_task));

            $employee = $this->v_model->get_employee_details($this->id_user_logged_in);
            $manager = $this->v_model->get_employee_details($manager->id_user);
            $subject = "Vacation Request Declined ";
            $k = array(
                    'manager_name' => ucwords($manager->first_name." ".$manager->last_name),
                    'description' => $description,
                    'start_date' => $date_start,
                    'end_date' => $date_end,
                    'employee_name' => ucwords($employee->first_name." ".$employee->last_name),
                    'company_name' => $this->company_name
                );
            $msg = $this->load->view('email_templates/vacation_declined', $k, true);
            $this->load->library('mailgun_api');
            $this->mailgun_api->send_mail(
                                    array(
                                        'from' => $this->company_name.' <noreply@testbase.com.ng>',
                                        'to' => $employee->email,
                                        'subject' => $subject,
                                        'html' => '<html>'. $msg .'</html>'
                                    ));
            
            $success_msg = 'Vacation request declined successful!';
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('msg', $success_msg);

            $account_type = $this->user_auth->get_account_type($this->session->userdata('account_type'));
            if(!$account_type){
                $acct = "";
            }
            else{
                $acct = $account_type->name;
            }
        }

        redirect(site_url('vacation/pending_request'));

    }// End func vacation_request_decline



        /*
     * Approve vacation request
     * 
     * @access public 
     * @return void
     */
    public function bulk_vacation_request_approve(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $ret = true;

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/pending_approvals', 'refresh');
        } 
        
        $post_users = $this->input->post('id_user');

        foreach ($post_users as $user) {

            $v = explode('_', $user);
            $id_vacation = $v[1];
            $id_user = $v[0];
            $id_admin = $this->v_model->get('users', array('account_type'=>1, 'id_company'=>$this->id_company));

            //get stage
            $tr = $this->v_model->get('vacation_request_approver', array(
                                                                        'id_vacation'=>$id_vacation, 
                                                                        'status' => 0

                                                                        ));
          
            $stages = $this->v_model->get_rlast_stage($this->id_company);

            //get number of stages approved
            if($tr[0]->stage == $stages){ //if last stage
                $data = array(
                    'status' => 2
                );

                if($tr[0]->id_approver == $this->id_user_logged_in){

                    $this->v_model->update('vacation_request_approver', array('status'=>'2'), array('id_vacation'=>$id_vacation, 'id_user'=>$id_user));
                    $q = $this->v_model->update('vacation', array('status'=>'Approved'), array('id_vacation'=>$id_vacation));
                    
                    if($q){

                        $vac_detail = $this->v_model->get_vacation_details($id_vacation);
                        $phone = $this->user_auth->get_employee_phone_number($vac_detail->id_user);
                        $phone = str_replace(array('(', ')', ' ', '-'), '', $phone);
                            
                        if($vac_detail && !empty($phone) && $this->sms_switch > 0) {
                            $sms_params['id_module'] = VACATION_MODULE;
                            
                            $company_id_string =$this->user_auth->get('company_id_string');
                            $date_start = date('M j', strtotime($vac_detail->date_start));
                            $date_end = date('M j', strtotime($vac_detail->date_end));
                            $message = "Your vacation request for {$date_start} to {$date_end} has been approved by your manager/admin. "
                                    . "Kindly log into the employee portal at {$this->company_url} to view the details.";
                            $sms_params['message'] = $message;
                            $sms_params['recipients']['id_user'] = $vac_detail->id_user;
                            $sms_params['recipients']['number'] = '+' .$phone;
                            $sms_params['sender'] = substr($this->company_name, 0, 11);
                            $this->sms_lib->send_sms($sms_params);

                        }  

                        //update task
                        $t = $this->v_model->get('tasks', array('tb_name'=>'vacation_request_approver', 'tb_column_name'=>'id_request_approver', 'tb_id_record'=>$tr[0]->id_request_approver));
                        $task = array(
                                'id_task' => $t[0]->id_task,
                                'status' => TASK_STATUS_COMPLETE
                            );

                        $this->tasks->update_task($task);
                        
                        $employee = $this->v_model->get_employee_details($vac_detail->id_user);
                        $subject = "Vacation Request Approved ";
                        $k = array(
                                'description' => $vac_detail->description,
                                'start_date' => $date_start,
                                'end_date' => $date_end,
                                'company_name' => $this->company_name
                            );
                        $msg = $this->load->view('email_templates/vacation_approved', $k, true);

                        $this->load->library('mailgun_api');
                        $this->mailgun_api->send_mail(
                                                array(
                                                    'from' => $this->company_name.' <noreply@testbase.com.ng>',
                                                    'to' => $employee->email,
                                                    'subject' => $subject,
                                                    'html' => '<html>'. $msg .'</html>'
                                                ));
                        
                        $success_msg = 'Vacation request approved successful!';
                        $this->session->set_flashdata('type', 'success');
                        $this->session->set_flashdata('msg', $success_msg);

                        $account_type = $this->user_auth->get_account_type($this->session->userdata('account_type'));
                        if(!$account_type){
                            $acct = "";
                        }
                        else{
                            $acct = $account_type->name;
                        }

                    }
                }
            }
            else{

                $next_stage = $tr[0]->stage + 1;
                if($tr[0]->id_approver == $this->id_user_logged_in){
                    $this->v_model->update('vacation_request_approver', array('status'=>1), array('id_vacation'=>$id_vacation, 'stage'=>$tr[0]->stage));

                    //for ($i=$next_stage; $i <= $stages; $i++) { 
                            
                    $st = $this->v_model->get('vacation_request_stages', array('id_company'=>$this->id_company, 'stage'=>$next_stage));
                    $vac = $this->v_model->get('vacation', array('id_vacation'=>$id_vacation));

                    if($st[0]->approver == 'Unit Manager'){
                        $employee = $this->v_model->get('employees', array('id_user'=>$vac[0]->id_user));
                        $id_approver = $employee[0]->id_manager;
                    }
                    elseif($st[0]->approver == 'Administrator'){
                        $id_approver = $id_admin[0]->id_user;
                    }
                    else{
                        $id_approver = $st[0]->id_user;
                    }

                    $status = $this->v_model->user_has_access($id_approver);
                    $company_id_string =$this->user_auth->get('company_id_string');
                    if($status == 1){
                        $em = $this->v_model->get_employee_info($this->id_company, $vac[0]->id_user);
                        $appr = $this->v_model->get_employee_info($this->id_company, $id_approver);
                        $sms_content = "{$em->first_name} {$em->last_name} at {$this->company_name} has sent a vacation request. "
                                    . "Kindly log into the employee portal at {$this->company_url} to approve or decline the request.";
                        $email_content = 'email_templates/vacation_request';
                        //break;
                    }//}if(!isset($em) OR !isset($appr))
                    else{
                        $appr = $this->v_model->get_employee_info($this->id_company, $id_admin[0]->id_user);
                        $em = $this->v_model->get_employee_info($this->id_company, $id_user);
                        $sms_content = "{$em->first_name} {$em->last_name} at {$this->company_name} has sent a vacation request. "
                                    . "Kindly log into the employee portal at {$this->company_url} to approve or decline the request.";
                        $email_content = 'email_templates/vacation_request_not_approver';
                    }
                    
                    $vac_detail = $this->v_model->get_vacation_details($id_vacation);

                    //update task
                    $t = $this->v_model->get('tasks', array('tb_name'=>'vacation_request_approver', 'tb_column_name'=>'id_request_approver', 'tb_id_record'=>$tr[0]->id_request_approver));
                    $task = array(
                            'id_task' => $t[0]->id_task,
                            'status' => TASK_STATUS_COMPLETE
                        );
                    $this->tasks->update_task($task);

                    //send sms
                    $phone = $this->user_auth->get_employee_phone_number($appr->id_user);
                    $phone = str_replace(array('(', ')', ' ', '-'), '', $phone);
                        
                    if(!empty($phone) && $this->sms_switch > 0) {
                        $sms_params['id_module'] = VACATION_MODULE;                        
                        $date_start = date('M j', strtotime($vac_detail->date_start));
                        $date_end = date('M j', strtotime($vac_detail->date_end));
                        $sms_params['message'] = $sms_content;
                        $sms_params['recipients']['id_user'] = $appr->id_user;
                        $sms_params['recipients']['number'] = $phone;
                        $sms_params['sender'] = substr($this->company_name, 0, 11);
                        $this->sms_lib->send_sms($sms_params);

                    }

                    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
                    $domainName = $_SERVER['HTTP_HOST'].'/';

                    //send email
                    $subject = "Vacation Request Approval ";
                    $k = array(
                        'employee_name' => $em->first_name.' '.$em->last_name,
                        'company_name' => $this->company_name,
                        'subdomain' => $this->user_auth->get('company_id_string'),
                        'domain' => $protocol.$domainName,
                        'department' => $em->departments,
                        'role' => $em->roles,
                        'location' => $em->location_tag,
                        'description' => $vac_detail->description,
                        'start_date' => date('M j', strtotime($vac_detail->date_start)),
                        'end_date' => date('M j', strtotime($vac_detail->date_end))
                        );
                    $msg = $this->load->view($email_content, $k, true);

                    $this->load->library('mailgun_api');
                    $this->mailgun_api->send_mail(
                                            array(
                                                'from' => $this->company_name.' <noreply@testbase.com.ng>',
                                                'to' => $appr->email,
                                                'subject' => $subject,
                                                'html' => '<html>'. $msg .'</html>'
                                            ));

                    $link = anchor('vacation/pending_approvals', 'You have a vacation request pending.');
                   
                    //data for insert
                    $approver_list = array(
                            'id_approver' => $appr->id_user,
                            'id_user' => $id_user,
                            'id_vacation'=> $id_vacation,
                            'stage'=> $next_stage,
                            'status'=> 0
                        );
                    
                    $in = $this->v_model->add('vacation_request_approver', $approver_list, true);

                    $task_data[] = array(
                                    'id_user_from' => $id_user,
                                    'id_user_to' => $appr->id_user,
                                    'id_module' => 2,
                                    'subject' => $link,
                                    'status' => 2,
                                    'tb_name' => 'vacation_request_approver',
                                    'tb_column_name' => 'id_request_approver',
                                    'tb_id_record' => $in,
                                    'date_will_end' => date('Y-m-d', strtotime('+ 2 days'))
                                );
                    $this->tasks->push_task($task_data);
                }
                else{
                    $success_msg = 'You do not have approval access!';
                    $this->session->set_flashdata('type', 'error');
                    $this->session->set_flashdata('msg', $success_msg);
                }
            }
        }
        $success_msg = 'Vacation request approved successful!';
        $this->session->set_flashdata('type', 'success');
        $this->session->set_flashdata('msg', $success_msg);
 
        redirect(site_url('vacation/pending_request'));

    }// End func bulk_vacation_request_approve
    
    /*
     * Decline vacation request
     * 
     * @access public 
     * @return void
     */
    public function bulk_vacation_request_decline(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }
        
        $ret = false;
        $post_users = $this->input->post('id_user');

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        } 
        else {

            foreach ($post_users as $user) {

                $v = explode('_', $user);
                $id_vacation = $v[1];
                $id_user = $v[0];

                //$id_vacation = $this->input->post('id_vacation_decline', true);

                $ret = $this->v_model->vacation_request_decline($id_vacation, 
                        array(
                            'status' => 'Declined'
                        ));

                if(!$ret){
                    $error_msg = 'Error! Unable to decline vacation request!';
                    $this->session->set_flashdata('type', 'error');
                    $this->session->set_flashdata('msg', $error_msg);
                }else {
                    
                    $vac_detail = $this->v_model->get_vacation_details($id_vacation);
                    $phone = $this->user_auth->get_employee_phone_number($vac_detail->id_user);
                    $phone = str_replace(array('(', ')', ' ', '-'), '', $phone);
                    if($vac_detail && !empty($phone) && $this->sms_switch > 0) {
                        $sms_params['id_module'] = VACATION_MODULE;
                        $company_id_string =$this->user_auth->get('company_id_string');
                        $date_start = date('M j', strtotime($vac_detail->date_start));
                        $date_end = date('M j', strtotime($vac_detail->date_end));
                        $message = "Your vacation request for {$date_start} to {$date_end} has been declined by your manager/admin. "
                                . "Kindly log into the employee portal at {$this->company_url} to view the details.";
                        $sms_params['message'] = $message;
                        $sms_params['recipients']['id_user'] = $vac_detail->id_user;
                        $sms_params['recipients']['number'] = '+' .$phone;
                        $sms_params['sender'] = substr($this->company_name, 0, 11);
                        $this->sms_lib->send_sms($sms_params);

                    }  

                    //update task
                    $tr = $this->v_model->get('vacation_request_approver', array(
                                                                            'id_vacation'=>$id_vacation, 
                                                                            'status' => 0

                                                                            ));
                    $t = $this->v_model->get('tasks', array('tb_name'=>'vacation_request_approver', 'tb_column_name'=>'id_request_approver', 'tb_id_record'=>$tr[0]->id_request_approver));
                    $task = array(
                            'id_task' => $t[0]->id_task,
                            'status' => 1
                        );

                    $this->tasks->update_task($task);
                    $vac = $this->v_model->get('vacation', array('id_vacation'=>$id_vacation));
                    $employee = $this->v_model->get_employee_details($this->id_user_logged_in);
                    $manager = $this->v_model->get_employee_details($tr[0]->id_request_approver);
                    $subject = "Vacation Request Declined ";
                    $k = array(
                            'manager_name' => ucwords($manager->first_name." ".$manager->last_name),
                            'description' => $vac[0]->description,
                            'start_date' => $vac[0]->date_start,
                            'end_date' => $vac[0]->date_end,
                            'employee_name' => ucwords($employee->first_name." ".$employee->last_name),
                            'company_name' => $this->company_name
                        );
                    $msg = $this->load->view('email_templates/vacation_declined', $k, true);
                    $this->load->library('mailgun_api');
                    $this->mailgun_api->send_mail(
                                            array(
                                                'from' => $this->company_name.' <noreply@testbase.com.ng>',
                                                'to' => $employee->email,
                                                'subject' => $subject,
                                                'html' => '<html>'. $msg .'</html>'
                                            ));
                    
                    $success_msg = 'Vacation request declined successful!';
                    $this->session->set_flashdata('type', 'success');
                    $this->session->set_flashdata('msg', $success_msg);

                    $account_type = $this->user_auth->get_account_type($this->session->userdata('account_type'));
                    if(!$account_type){
                        $acct = "";
                    }
                    else{
                        $acct = $account_type->name;
                    }
                }
            }
        }

        

        redirect(site_url('vacation/pending_request'));

    }// End func bulk_vacation_request_decline




    /*
     * View Employee Vacation (entitlements, planner, request, history)
     * 
     * @access public 
     * @return void
     */
    public function my_vacation_allowance(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_VIEW_MY);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $year = date('Y');
        $leave =  $this->v_model->get_employee_leave_allowance($this->user_auth->get('id_user'), $this->id_company, $year);

        if(empty($leave)){
            $error_msg = 'Error! You have not been assigned vacation allowance.';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/my_vacation', 'refresh');
        }


        //get employee entitlements
        //if empty check if assigned from compensation
        // $us = $this->v_model->get_employee_info($this->id_company, $this->user_auth->get('id_user'));

        // $ent = $this->v_model->get_employee_entitlements($this->user_auth->get('id_user'));
        // //print_r($us); die();
        // if(empty($ent) AND !empty($us->id_pay_structure)){
            
        //     $role_vacation = $this->v_model->get_single_role_vacation($this->id_company, $us->id_pay_structure);
        //     foreach ($role_vacation as $rv) {
        //         if($rv->no_of_days > 0){
        //             $allc_data = array(
        //                     'id_type' => $rv->id_vacation_type,
        //                     'id_company' => $rv->id_company,
        //                     'id_user' => $us->id_user,
        //                     'no_of_days' => $rv->no_of_days
        //                 );

        //             $this->v_model->add('vacation_allocated', $allc_data);
        //         }
        //     }
        // }
        
        
        $data['allowance'] = $leave;
        // $data['vacation_types'] = $this->v_model->get_employee_vacation_types($this->user_auth->get('id_user'));
        // $data['entitlements'] = $this->v_model->get_employee_entitlements($this->user_auth->get('id_user'));
        // $data['taken'] = $this->v_model->get_taken_vacations($this->user_auth->get('id_user'));
        // $data['history'] = $this->v_model->get_employee_vacation_history($this->user_auth->get('id_user'));
        // $data['pending_approvals'] = $this->v_model->get_employee_pending_vacation($this->user_auth->get('id_user'));
        // $data['pending'] = $this->v_model->get_pending_allowance($this->user_auth->get('id_user'), $this->id_company, $year);  
        // $data['leave'] = $leave;
        // $data['request'] = $this->v_model->check_request($this->user_auth->get('id_user')); 
        // $data['allowance'] = $this->v_model->get_employee_allowance($this->id_company, $year, $leave->id_compensation_employee);
        // $data['allowance_history'] = $this->v_model->get_employee_allowance_history($this->user_auth->get('id_user'), $this->id_company, $year);
        $data['vacation_plan'] = $this->v_model->get('vacation_plan_request', array('id_company'=>$this->id_company, 'status'=> 1));
 
        // $account_type = $this->user_auth->get_account_type($this->session->userdata('account_type'));
        // if(!$account_type){
        //     $acct = "";
        // }
        // else{
        //     $acct = $account_type->name;
        // }


        $buffer = $this->load->view('vacation/my_vacation_allowance', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func my_vacation






    
    /*
     * View holidays
     * 
     * @access public
     * @return void
     */
    public function holidays(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_HOLIDAY);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $year = date('Y');
        $year_value = $this->uri->segment(3);
        if(is_numeric($year_value)){
            $year = $year_value;
        }

        $year_begin = date('Y-m-d', strtotime("1 january $year"));
        $year_end = date('Y-m-d', strtotime("31 december $year"));
        
        $usr = $this->v_model->get('users', array('id_user'=>$this->user_auth->get('id_user')));
        
        $data['holidays'] = $this->v_model->get_holidays($this->user_auth->get('id_company'), $year);
        $data['holiday_list'] = $this->v_model->get_holiday_list($this->user_auth->get('id_company'), $year_begin, $year_end);
        $data['locations'] = $this->v_model->get_company_locations($this->user_auth->get('id_company'));
        $data['user_location'] = $usr[0]->id_company_location;
        $data['holiday_year'] = $year;

        $params['id_module'] = VACATION_MODULE;
        $params['id_user'] = $this->user_auth->get('id_user');

        $data['permissions'] = array();
        if($this->user_auth->have_perm(VACATION_TYPES)) {
            array_push($data['permissions'], VACATION_TYPES);
        }

        $buffer = $this->load->view('vacation/holidays', $data, true);

        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func holiday



    /*
     * Delete an holiday
     * 
     * @access public
     * @return void
     */
    public function delete_holiday(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $ret = false;

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        } else {
            $id_holiday = $this->input->post('id_holiday', true);

            $ret = $this->v_model->delete_holiday(
                    array(                                    
                        'id_holiday' => $id_holiday,                                
                    ));
        }

        if(!$ret){
            $error_msg = 'Error! Unable to delete holiday!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        }else {
            $success_msg = 'Holiday deleted successfully!';
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('msg', $success_msg);
        }

        redirect(site_url('vacation/holidays'));

    }// End func delete_holiday

    /*
     * Create a new holiday
     * 
     * @access public
     * @return void
     */
    public function create_holiday(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $ret = false;

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);

        } else {
            $title = $this->input->post('title', true);
            $date = $this->input->post('date', true);
            $recurring = $this->input->post('recurring', true);
            $locations = $this->input->post('location', true);
            if(!$locations) {
                $locations = array();
            }
           
            $ret = $this->v_model->create_holiday( $this->user_auth->get('id_company'), 
                    array(                                    
                        'id_company' => $this->user_auth->get('id_company'),
                        'holiday_name' => $title,
                        'holiday_date' => date('Y-m-d', strtotime($date)),
                        'annual' => $recurring,
                        'company_locations' => implode(',',$locations)
                    ));
        }

        if(!$ret){
            $error_msg = 'Error! Unable to create holiday!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        }else {
            $success_msg = 'Holiday created successfully!';
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('msg', $success_msg);
        }

        redirect(site_url('vacation/holidays'));

    }// End func create_holiday

    /*
     * Update an holiday
     * 
     * @access public
     * @return void
     */
    public function update_holiday($action = ''){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $ret = false;

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        } else {
            $title = $this->input->post('edit_title', true);
            $date = $this->input->post('edit_date', true);
            $recurring = $this->input->post('edit_recurring', true);
            $locations = $this->input->post('edit_location', true);                    
            $id_holiday = $this->input->post('id_holiday', true);
            if(!$locations) {
                $locations = array();
            }
            
            $ret = $this->v_model->update_holiday( $id_holiday, 
                    array(                                    
                        'holiday_name' => $title,
                        'holiday_date' => date('Y-m-d', strtotime($date)),
                        'annual' => $recurring,
                        'company_locations' => implode(',',$locations)
                    ));
        }

        if(!$ret){
            $error_msg = 'Error! Unable to edit holiday!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        }else {
            $success_msg = 'Holiday edited successfully!';
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('msg', $success_msg);
        }

        redirect(site_url('vacation/holidays'));

    }// End func holiday

    /*
     * View vacation types
     * 
     * @access public
     * @return void
     */
    public function vacation_types(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $data['vacation_types'] = $this->v_model->get_vacation_types($this->user_auth->get('id_company'));
        $data['allowance_stage'] = $this->v_model->get_allowance_stages($this->user_auth->get('id_company'));
        $data['last_stage'] = $this->v_model->get_last_stage($this->id_company);
        $data['vacation_request'] = $this->v_model->get_plan_request($this->id_company);
        $data['request_stage'] = $this->v_model->get('vacation_request_stages', array('id_company'=>$this->user_auth->get('id_company')));
        $data['rlast_stage'] = $this->v_model->get_rlast_stage($this->id_company);

        $data['plan_stage'] = $this->v_model->get_plan_stages($this->user_auth->get('id_company'));
        $data['plast_stage'] = $this->v_model->get_last_pstage($this->id_company);
        $data['employees'] = $this->v_model->get_only_employees($this->id_company);
        //$data['pay_structure'] = $this->v_model->compensation_paygrade($this->id_company);

        $buffer = $this->load->view('vacation/vacation_types', $data, true);

        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func vacation_types




    /*
     * update vacation compensation allocation
     * 
     * @access public
     * @return void
     */

    public function update_allocated_vac()
    {
        //check if id vacation_compensation is valid
        $d = $this->input->post('days');
        $id_vac_com = $this->input->post('id');
        $id_pay_structure = $this->input->post('id_pay');
        $id_type = $this->input->post('id_type');
        if(!is_numeric($d)){
            echo $d;
            return;
        }

        if(empty($id_vac_com)){
            $adata = array(
                    'id_pay_structure'=> $id_pay_structure, 
                    'id_vacation_type' => $id_type,
                    'id_company' => $this->id_company,
                    'no_of_days' => $d
                );
            $q = $this->v_model->add('vacation_compensation', $adata);
        }
        else{
            $q = $this->v_model->update('vacation_compensation', array('no_of_days'=>$d), array('id_vacation_compensation'=>$id_vac_com)); 
        }

        if($q){
            echo 1;
            return;
        }
        else{
            echo 2;
            return;
        }
    }// End func update_allocated_vac



    /*
     * Vacation approval process setting
     * 
     * @access public
     * @return void
     */
    public function approval_process_setting(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $relief = $this->company_module_settings->get($this->id_company, 2, 'relief');

        if(empty($relief)){
            $setting_data = array(
                    'id_company' => $this->id_company,
                    'id_module' => 2,
                    'setting_name' => 'relief',
                    'setting_value' => 1
                );
            $this->company_module_settings->add($setting_data);
        }

        if(is_numeric($this->input->post('relief_setting'))){
            $setting_value = $this->input->post('relief_setting');
            $update_data = array('setting_value'=>$setting_value);
            $update_where = array('id_company' => $this->id_company, 'id_module' => 2, 'setting_name' => 'relief',);
            $q = $this->company_module_settings->update($update_data, $update_where);

            if($q){
                $this->session->set_flashdata('type', 'success');
                $this->session->set_flashdata('msg', 'Successful! Leave relief setting changed successfully!');
            }
            else{
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', 'Error! There was a problem processing your request!');
            }

            redirect('vacation/approval_process_setting', 'refresh');
        }

        $data['vacation_types'] = $this->v_model->get_vacation_types($this->user_auth->get('id_company'));
        $data['allowance_stage'] = $this->v_model->get_allowance_stages($this->user_auth->get('id_company'));
        $data['last_stage'] = $this->v_model->get_last_stage($this->id_company);
        $data['vacation_request'] = $this->v_model->get_plan_request($this->id_company);
        $data['request_stage'] = $this->v_model->get('vacation_request_stages', array('id_company'=>$this->user_auth->get('id_company')));
        $data['rlast_stage'] = $this->v_model->get_rlast_stage($this->id_company);
        $data['relief'] = $this->company_module_settings->get($this->id_company, 2, 'relief');

        $data['plan_stage'] = $this->v_model->get_plan_stages($this->user_auth->get('id_company'));
        $data['plast_stage'] = $this->v_model->get_last_pstage($this->id_company);
        $data['employees'] = $this->v_model->get_only_employees($this->id_company);

        $buffer = $this->load->view('vacation/approval_process_setting', $data, true);

        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func approval_process_setting




    /*
     * Vacaton planner setting
     * 
     * @access public
     * @return void
     */
    public function vacation_planner_setting(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }
        
        $vac_type = $this->v_model->get_vacation_types($this->user_auth->get('id_company'));
        $data['vacation_types'] = $vac_type;
        $data['allowance_stage'] = $this->v_model->get_allowance_stages($this->user_auth->get('id_company'));
        $data['last_stage'] = $this->v_model->get_last_stage($this->id_company);
        $data['vacation_request'] = $this->v_model->get_plan_request($this->id_company);
        $data['request_stage'] = $this->v_model->get('vacation_request_stages', array('id_company'=>$this->user_auth->get('id_company')));
        $data['rlast_stage'] = $this->v_model->get_rlast_stage($this->id_company);

        $data['plan_stage'] = $this->v_model->get_plan_stages($this->user_auth->get('id_company'));
        $data['plast_stage'] = $this->v_model->get_last_pstage($this->id_company);
        $data['employees'] = $this->v_model->get_only_employees($this->id_company);
        $data['current_leave_year'] = ($this->input->post('year')) ? $this->input->post('year') : date('Y') ;
        
        $restricted = "";
        $restrict = $this->v_model->get('vacation_restricted_period', array('id_company'=>$this->id_company));
        foreach ($restrict as $key => $v) {
            $restricted[$v->id_vacation_type][] = $v;
        }

        $data['restricted'] = $restricted;
        $buffer = $this->load->view('vacation/vacation_planner_setting', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func vacation_planner_setting


    /*
     * Vacaton planner setting
     * 
     * @access public
     * @return void
     */
    public function notification_setting(){
        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        if ($this->input->server('REQUEST_METHOD') == "POST") {
            $notification = $this->input->post('notification');
            $sms_set = $this->v_model->get('module_notification_settings', array('id_company'=>$this->id_company, 'id_module'=>2));
            
            if(empty($sms_set)){
                $sms_data = array(
                    'id_company' => $this->id_company,
                    'id_module' => 2,
                    'status' => $notification
                );

                //add
                $q = $this->v_model->add('module_notification_settings', $sms_data);
                if($q){
                    $error_msg = 'Success! Notification setting changed successfully!';
                    $this->session->set_flashdata('type', 'success');
                    $this->session->set_flashdata('msg', $error_msg);
                }
                else{
                    $error_msg = 'Error! There was a problem processing your request';
                    $this->session->set_flashdata('type', 'error');
                    $this->session->set_flashdata('msg', $error_msg);
                }
            }
            else{
                $sms_data = array(
                    'status' => $notification
                );
                $where = array(
                    'id_company' => $this->id_company,
                    'id_module' => 2,

                );
                //update
                $q = $this->v_model->update('module_notification_settings', $sms_data, $where);
                if($q){
                    $error_msg = 'Success! Send SMS setting changed successfully!';
                    $this->session->set_flashdata('type', 'success');
                    $this->session->set_flashdata('msg', $error_msg);
                }
                else{
                    $error_msg = 'Error! There was a problem processing your request';
                    $this->session->set_flashdata('type', 'error');
                    $this->session->set_flashdata('msg', $error_msg);
                }
            }

            redirect('vacation/notification_setting', 'refresh');
        }
        

        $data['notification'] = $this->v_model->get('module_notification_settings', array('id_company'=>$this->id_company, 'id_module'=>2));
        $buffer = $this->load->view('vacation/notification_setting', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);
    }

    /*
     * View vacation types
     * 
     * @access public
     * @return void
     */
    public function vacation_entitlement_setting(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }


        if($this->input->post('add_setting'))
        {
            $allowance_setting = $this->input->post('allowance');
            
            if(!empty($allowance_setting)){
                $allowance_data = array(
                    'id_company' => $this->id_company,
                    'vacation_allowance' => $allowance_setting
                    );

                $q = $this->v_model->add('vacation_allowance_setting', $allowance_data);
                if($q){
                    $error_msg = 'Success! Leave allowance setting was added successfully!';
                    $this->session->set_flashdata('type', 'success');
                    $this->session->set_flashdata('msg', $error_msg);
                }
                else{
                    $error_msg = 'Error! There was a problem processing your request!';
                    $this->session->set_flashdata('type', 'error');
                    $this->session->set_flashdata('msg', $error_msg);
                }
            }

            redirect('vacation/vacation_entitlement_setting', 'refresh');
        }

        if($this->input->post('edit_setting'))
        {
            $allowance_setting = $this->input->post('allowance');
            $id_allowance_setting = $this->input->post('id');

            if(!empty($allowance_setting)){
                $allowance_data = array(
                    'vacation_allowance' => $allowance_setting
                    );

                $where = array(
                    'id_vacation_allowance_setting'=>$id_allowance_setting
                    );

                $q = $this->v_model->update('vacation_allowance_setting', $allowance_data, $where);
                if($q){
                    $error_msg = 'Success! Leave allowance setting was updated successfully!';
                    $this->session->set_flashdata('type', 'success');
                    $this->session->set_flashdata('msg', $error_msg);
                }
                else{
                    $error_msg = 'Error! There was a problem processing your request!';
                    $this->session->set_flashdata('type', 'error');
                    $this->session->set_flashdata('msg', $error_msg);
                }
            }

            redirect('vacation/vacation_entitlement_setting', 'refresh');
        }


        $data['vacation_types'] = $this->v_model->get_vacation_types($this->user_auth->get('id_company'));
        $data['paygrade'] = $this->v_model->compensation_paygrade($this->id_company);
        $data['payroll'] = $this->v_model->get('modules', array('id_string'=>'payroll'));//has payroll

        //get default vacation
        $data['plan_stage'] = $this->v_model->get_plan_stages($this->user_auth->get('id_company'));
        $data['plast_stage'] = $this->v_model->get_last_pstage($this->id_company);
        $data['employees'] = $this->v_model->get_only_employees($this->id_company);
        $data['otherpay'] = $this->v_model->get('payroll_otherpay_settings', array('id_company'=>$this->id_company));
        $data['vacation_allowance_setting'] = $this->v_model->get('vacation_allowance_setting', array('id_company'=>$this->id_company));

        $buffer = $this->load->view('vacation/vacation_entitlement_setting', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func vacation_types



    /*
     * create vacation allowance stage
     * 
     * @access public
     * @return void
     */


    public function add_restricted_period_ajax()
    {
        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }    


        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect(site_url('vacation/my_vacation'));
        } 
        
        $id_type = $this->input->post('id_vactype', true);
        $date_start = $this->input->post('start_date', true);
        $date_end = $this->input->post('end_date', true);

        $this->load->library('form_validation');
        $this->form_validation->set_rules('id_vactype', '', 'trim|required');
        $this->form_validation->set_rules('start_date', '', 'trim|required');
        $this->form_validation->set_rules('end_date', '', 'trim|required');
    
        if($this->form_validation->run()==FALSE){

            echo 1;
            return;
        }


        if($this->v_model->get_date_conflicts2($this->id_company, $date_start, $date_end)){

            echo 4;
            return;
        }


        if(date('Y-m-d', strtotime($date_start)) > date('Y-m-d', strtotime($date_end))){

            echo 5;
            return;
        }

        //add request to database
        $ret = $this->v_model->add( 
                'vacation_restricted_period',
                array(                                    
                    'id_company' => $this->user_auth->get('id_company'),
                    'start_date' => date('Y-m-d', strtotime($date_start)),
                    'end_date' => date('Y-m-d', strtotime($date_end)),
                    'id_vacation_type' => $id_type
                )
            );
                    
        if($ret){
            echo 2;
            return;
        }
        else{
            echo 3;
            return;
        }

    }



        /*
     * create vacation allowance stage
     * 
     * @access public
     * @return void
     */


    public function edit_restricted_period_ajax()
    {
        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }    


        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect(site_url('vacation/my_vacation'));
        } 
        
        $id_restricted = $this->input->post('id_restricted', true);
        $date_start = $this->input->post('start_date', true);
        $date_end = $this->input->post('end_date', true);

        $this->load->library('form_validation');
        $this->form_validation->set_rules('id_restricted', '', 'trim|required');
        $this->form_validation->set_rules('start_date', '', 'trim|required');
        $this->form_validation->set_rules('end_date', '', 'trim|required');
    
        if($this->form_validation->run()==FALSE){

            echo 1;
            return;
        }

        if($this->v_model->get_date_conflicts2($this->id_company, $date_start, $date_end)){

            echo 4;
            return;
        }


        if(date('Y-m-d', strtotime($date_start)) > date('Y-m-d', strtotime($date_end))){

            echo 5;
            return;
        }

        //add request to database
        $ret = $this->v_model->update( 
                'vacation_restricted_period',
                array(                                    
                    'start_date' => date('Y-m-d', strtotime($date_start)),
                    'end_date' => date('Y-m-d', strtotime($date_end)),
                ),
                array(
                    'id_restricted_period'=> $id_restricted
                    )
            );
                    
        if($ret){
            echo 2;
            return;
        }
        else{
            echo 3;
            return;
        }


    }


        /*
     * create vacation allowance stage
     * 
     * @access public
     * @return void
     */


    public function delete_restricted_period_ajax()
    {
        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }    


        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect(site_url('vacation/my_vacation'));
        } 
        
        $id_restricted = $this->input->post('id_restricted', true);

        $this->load->library('form_validation');
        $this->form_validation->set_rules('id_restricted', '', 'trim|required');
    
        if($this->form_validation->run()==FALSE){

            echo 1;
            return;
        }

        //add request to database
        $ret = $this->v_model->delete( 
                'vacation_restricted_period',
                array(
                    'id_restricted_period'=> $id_restricted
                    )
            );
                    
        if($ret){
            echo 2;
            return;
        }
        else{
            echo 3;
            return;
        }

    }



    /*
     * create vacation allowance stage
     * 
     * @access public
     * @return void
     */
    public function create_allowance_stage(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

         if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/approval_process_setting', 'refresh');
        } else {

            $approver = $this->input->post('approver');

            if(empty($approver)){
                $error_msg = 'Invalid request method!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
                redirect('vacation/approval_process_setting', 'refresh');
                return;
            }

            if($approver == 'vm'){
                $approver = 'Administrator';
                $approver_id = '0';
            }elseif($approver == 'm'){
                $approver = 'Unit Manager';
                $approver_id = '0';
            }else{
                $e = $this->v_model->get_employee_details($approver);
                $approver = $e->first_name.' '.$e->last_name;
                $approver_id = $e->id_user;
            }

            //get last stage
            $last_stage = $this->v_model->get_last_stage($this->id_company);
            $stage = $last_stage + 1;
            $data = array(
                    'approver' => $approver,
                    'id_user' => $approver_id,
                    'id_company' => $this->id_company,
                    'stage' => $stage
                );

            $q = $this->v_model->add_stage($data);
            if($q){
                // $this->v_model->clear_allowance_request($this->id_company);
                // $dwhere = array('id_company'=>$this->id_company);
                // $this->db->delete('employee_vacation_allowance_stages', $dwhere);

                $error_msg = 'Vacation allowance approver stage successfully created!';
                $this->session->set_flashdata('type', 'success');
                $this->session->set_flashdata('msg', $error_msg);
                
            }
            else{
                $error_msg = 'There was a problem communicating with the server!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
            }

            redirect('vacation/approval_process_setting', 'refresh');
            return;

        }

    }// End func create allowance stage


    /*
     * edit vacation allowance stage
     * 
     * @access public
     * @return void
     */
    public function edit_allowance_stage(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }
 
         if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/approval_process_setting', 'refresh');
        } else {

            $approver = $this->input->post('approver');
            $id_stage = $this->input->post('id_allowance_stage');

            if(empty($approver)){
                $error_msg = 'Invalid request method!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
                redirect('vacation/approval_process_setting', 'refresh');
                return;
            }

            if($approver == 'vm'){
                $approver = 'Administrator';
                $approver_id = '0';
            }elseif($approver == 'm'){
                $approver = 'Unit Manager';
                $approver_id = '0';
            }else{
                $e = $this->v_model->get_employee_details($approver);
                $approver_id = $approver;
                $approver = $e->first_name.' '.$e->last_name;
                
            }

            $data = array(
                    'approver' => $approver,
                    'id_user' => $approver_id
                );

            $q = $this->v_model->edit_stage($data, $id_stage);
            if($q){
                
                //$this->v_model->clear_allowance_request($this->id_company);
                $data2 = array(
                    'approver' => $approver,
                    'id_approver' => $approver_id
                );

                // $where2 = array('stage'=>$id_stage, 'id_company'=>$this->id_company);
                // $this->v_model->update('employee_vacation_allowance_stages', $data2, $where2);


                $error_msg = 'Vacation allowance approver stage successfully edited!';
                $this->session->set_flashdata('type', 'success');
                $this->session->set_flashdata('msg', $error_msg);
            }
            else{
                $error_msg = 'There was a problem communicating with the server!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
            }

            redirect('vacation/approval_process_setting', 'refresh');
            return;

        }

    }// End func edit_allowance_stage


    /*
     * delete vacation allowance stage
     * 
     * @access public
     * @return void
     */
    public function delete_allowance_stage(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }
 
         if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/approval_process_setting', 'refresh');
        } else {

            $id_stage = $this->input->post('id_allowance_stage2');

            if(empty($id_stage)){
                $error_msg = 'Invalid request method!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
                redirect('vacation/approval_process_setting', 'refresh');
                return;
            }

            $q = $this->v_model->delete_stage($id_stage);
            if($q){

                $stage = $this->v_model->get_where('vacation_allowance_stages', array('id_company'=>$this->id_company), 'id_request_stage');
                $stage_count = count($stage); 
                $stage_num = $stage_count;
                for ($i=0; $i < $stage_num ; $i++) { 
                   $id = $stage[$i]->id_allowance_stage; 
                   $this->v_model->update('vacation_allowance_stages', array('stage'=>$stage_count), array('id_request_stage'=>$id));
                   $stage_count--;
                }

                // $this->v_model->clear_allowance_request($this->id_company);
                // $dwhere = array('id_company'=>$this->id_company);
                // $this->db->delete('employee_vacation_allowance_stages', $dwhere);

                $error_msg = 'Vacation allowance approver stage successfully deleted!';
                $this->session->set_flashdata('type', 'success');
                $this->session->set_flashdata('msg', $error_msg);
                
            }
            else{
                $error_msg = 'There was a problem communicating with the server!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
            }

            redirect('vacation/approval_process_setting', 'refresh');
            return;

        }

    }// End func delete_allowance_stage


    /*
     * create vacation request stage
     * 
     * @access public
     * @return void
     */
    public function create_approval_stage(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

         if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/approval_process_setting', 'refresh');
        } else {

            $approver = $this->input->post('approver');

            if(empty($approver)){
                $error_msg = 'Invalid request method!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
                redirect('vacation/approval_process_setting', 'refresh');
                return;
            }

            if($approver == 'vm'){
                $approver = 'Administrator';
                $approver_id = '0';
            }elseif($approver == 'm'){
                $approver = 'Unit Manager';
                $approver_id = '0';
            }else{
                $e = $this->v_model->get_employee_details($approver);
                $approver = $e->first_name.' '.$e->last_name;
                $approver_id = $approver;
            }

            //get last stage
            $last_stage = $this->v_model->get_rlast_stage($this->id_company);
            $stage = $last_stage + 1;
            $data = array(
                    'approver' => $approver,
                    'id_user' => $approver_id,
                    'id_company' => $this->id_company,
                    'stage' => $stage
                );

            $q = $this->v_model->add('vacation_request_stages', $data);
            if($q){
                // $this->v_model->clear_vacation_request($this->id_company);
                // $dwhere = array('id_company'=>$this->id_company);
                // $this->db->delete('employee_vacation_approval_stages', $dwhere);

                $error_msg = 'Vacation allowance approver stage successfully created!';
                $this->session->set_flashdata('type', 'success');
                $this->session->set_flashdata('msg', $error_msg);
                
            }
            else{
                $error_msg = 'There was a problem communicating with the server!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
            }

            redirect('vacation/approval_process_setting', 'refresh');
            return;

        }

    }// End func create_approval_stage



    /*
     * edit vacation approval stage
     * 
     * @access public
     * @return void
     */
    public function edit_approval_stage(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }
 
         if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/vacation_types', 'refresh');
        } else {

            $approver = $this->input->post('approver');
            $id_stage = $this->input->post('id_approval_stage');

            if(empty($approver)){
                $error_msg = 'Invalid request method!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
                redirect('vacation/vacation_types', 'refresh');
                return;
            }

            if($approver == 'vm'){
                $approver = 'Administrator';
                $approver_id = '0';
            }elseif($approver == 'm'){
                $approver = 'Unit Manager';
                $approver_id = '0';
            }else{
                $e = $this->v_model->get_employee_details($approver);
                $approver_id = $approver;
                $approver = $e->first_name.' '.$e->last_name;
                
            }

            $data = array(
                    'approver' => $approver,
                    'id_user' => $approver_id
                );

            $q = $this->v_model->update('vacation_request_stages', $data, array('id_request_stage'=>$id_stage, 'id_company'=>$this->id_company));
            if($q){

                $error_msg = 'Vacation allowance approver stage successfully edited!';
                $this->session->set_flashdata('type', 'success');
                $this->session->set_flashdata('msg', $error_msg);
            }
            else{
                $error_msg = 'There was a problem communicating with the server!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
            }

            redirect('vacation/approval_process_setting', 'refresh');
            return;

        }

    }// End func edit_approval_stage


    /*
     * delete vacation approval stage
     * 
     * @access public
     * @return void
     */
    public function delete_approval_stage(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }
 
         if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/vacation_types', 'refresh');
        } else {

            $id_stage = $this->input->post('id_approval_stage2');

            if(empty($id_stage)){
                $error_msg = 'Invalid request method!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
                redirect('vacation/vacation_types', 'refresh');
                return;
            }

            $q = $this->v_model->delete('vacation_request_stages', array('id_request_stage'=>$id_stage));
            if($q){

                $stage = $this->v_model->get_where('vacation_request_stages', array('id_company'=>$this->id_company), 'id_request_stage');
                $stage_count = count($stage); 
                $stage_num = $stage_count;
                for ($i=0; $i < $stage_num ; $i++) { 
                   $id = $stage[$i]->id_request_stage; 
                   $this->v_model->update('vacation_request_stages', array('stage'=>$stage_count), array('id_request_stage'=>$id));
                   $stage_count--;
                }

                $error_msg = 'Vacation request approver stage successfully deleted!';
                $this->session->set_flashdata('type', 'success');
                $this->session->set_flashdata('msg', $error_msg);
                
            }
            else{
                $error_msg = 'There was a problem communicating with the server!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
            }

            redirect('vacation/approval_process_setting', 'refresh');
            return;

        }

    }// End func delete_approval_stage



    /*
     * create vacation plan stage
     * 
     * @access public
     * @return void
     */
    public function create_plan_stage(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

         if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/vacation_types', 'refresh');
        } else {

            $approver = $this->input->post('approver');

            if(empty($approver)){
                $error_msg = 'Invalid request method!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
                redirect('vacation/vacation_types', 'refresh');
                return;
            }

            if($approver == 'm'){
                $approver = 'Administrator';
                $approver_id = '0';
            }elseif($approver == 'vm'){
                $approver = 'Unit Manager';
                $approver_id = '0';
            }else{
                $e = $this->v_model->get_employee_details($approver);
                $approver = $e->first_name.' '.$e->last_name;
                $approver_id = $e->id_user;
            }

            //get last stage
            $last_stage = $this->v_model->get_last_pstage($this->id_company);
            $stage = $last_stage + 1;
            $data = array(
                    'approver' => $approver,
                    'id_user' => $approver_id,
                    'id_company' => $this->id_company,
                    'stage' => $stage
                );

            $q = $this->v_model->add_pstage($data);
            if($q){

                $error_msg = 'Vacation plan approver stage successfully created!';
                $this->session->set_flashdata('type', 'success');
                $this->session->set_flashdata('msg', $error_msg);
                
            }
            else{
                $error_msg = 'There was a problem communicating with the server!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
            }

            redirect('vacation/vacation_types', 'refresh');
            return;

        }

    }// End func create_plan_stage


    /*
     * edit vacation plan stage
     * 
     * @access public
     * @return void
     */
    public function edit_plan_stage(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }
 
         if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/vacation_types', 'refresh');
        } else {

            $approver = $this->input->post('approver');
            $id_stage = $this->input->post('id_plan_stage');

            if(empty($approver)){
                $error_msg = 'Invalid request method!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
                redirect('vacation/vacation_types', 'refresh');
                return;
            }

            $e = $this->v_model->get_employee_details($approver);
            $approver_id = $approver;
            $approver = $e->first_name.' '.$e->last_name;

            $data = array(
                    'approver' => $approver,
                    'id_user' => $approver_id
                );

            $q = $this->v_model->edit_pstage($data, $id_stage);
            if($q){
                
                //$this->v_model->clear_allowance_request($this->id_company);
                $data2 = array(
                    'approver' => $approver,
                    'id_approver' => $approver_id
                );

                $error_msg = 'Vacation plan approver stage successfully edited!';
                $this->session->set_flashdata('type', 'success');
                $this->session->set_flashdata('msg', $error_msg);
            }
            else{
                $error_msg = 'There was a problem communicating with the server!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
            }

            redirect('vacation/vacation_planner_setting', 'refresh');
            return;

        }

    }// End func edit_plan_stage


    /*
     * edit vacation plan stage
     * 
     * @access public
     * @return void
     */
    public function delete_plan_stage(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }
 
         if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/vacation_types', 'refresh');
        } else {

            $id_stage = $this->input->post('id_plan_stage2');

            if(empty($id_stage)){
                $error_msg = 'Invalid request method!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
                redirect('vacation/vacation_types', 'refresh');
                return;
            }

            $q = $this->v_model->delete_pstage($id_stage);
            if($q){

                $error_msg = 'Vacation Plan approver stage successfully deleted!';
                $this->session->set_flashdata('type', 'success');
                $this->session->set_flashdata('msg', $error_msg);
                
            }
            else{
                $error_msg = 'There was a problem communicating with the server!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
            }

            redirect('vacation/vacation_types', 'refresh');
            return;

        }

    }// End func delete_plan_stage



    /*
     * Delete vacation type
     * 
     * @access public
     * @return void
     */
    public function delete_vacation_type(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $ret = false;

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        } else {
            $id_type = $this->input->post('id_type', true);

            $ret = $this->v_model->delete_vacation_type(
                    array(                                    
                        'id_type' => $id_type                              
                    ));
        }

        if(!$ret){
            $error_msg = 'Error! Unable to delete vacation type!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        }else {
            $success_msg = 'Vacation type deleted successfully!';
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('msg', $success_msg);
        }

        redirect(site_url('vacation/vacation_types'));

    }// End func delete_vacation_type


    /*
     * Create a new vacation type
     * 
     * @access public
     * @return void
     */
    public function create_vacation_type(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $ret = false;

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        } else {
            $name = $this->input->post('name', true);
            $carryover = $this->input->post('carryover', true);
            $desc = $this->input->post('description', true);

            $ret = $this->v_model->create_vacation_type( $this->user_auth->get('id_company'), 
                    array(                                    
                        'id_company' => $this->user_auth->get('id_company'),
                        'name' => $name,
                        'carry_allowed' => $carryover,
                        'description' => $desc
                    ));
        }

        if(!$ret){
            $error_msg = 'Error! Unable to create vacation type!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        }else {
            $success_msg = 'Vacation type created successfully!';
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('msg', $success_msg);
        }

        redirect(site_url('vacation/vacation_types'));

    }// End func create_vacation_type

    /*
     * Edit a vacation type
     * 
     * @access public
     * @return void
     */
    public function update_vacation_type(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $ret = false;

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        } else {
            $name = $this->input->post('edit_name', true);
            $carryover = $this->input->post('edit_carryover', true);
            $desc = $this->input->post('edit_description', true);
            $id_type = $this->input->post('id_type', true);

            $ret = $this->v_model->update_vacation_type( $id_type, 
                    array(                                    
                        'name' => $name,
                        'carry_allowed' => $carryover,
                        'description' => $desc
                    ));
        }

        if(!$ret){
            $error_msg = 'Error! Unable to edit vacation type!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        }else {
            $success_msg = 'Vacation type edited successfully!';
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('msg', $success_msg);
        }

        redirect(site_url('vacation/vacation_types'));

    }// End func edit_vacation_type


    /*
     * Allocate vacation to employees
     * 
     * @access public
     * @return void
     */

    public function employee_entitlement()
    {
        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_ALLOCATE);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $id_account_type = $this->session->userdata('account_type');
        $acct_type = $this->user_auth->get_account_type($id_account_type);

        $usr = $this->v_model->get('employees', array('id_user'=>$this->user_auth->get('id_user')));
        if($usr[0]->id_access_level != ACCOUNT_TYPE_ADMINISTRATOR AND $usr[0]->id_access_level != ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR AND $usr[0]->id_access_level != ACCOUNT_TYPE_UNIT_MANAGER){
            redirect('vacation/my_vacation', 'refresh');
        }

        $employees = '';

        if($usr[0]->id_access_level == ACCOUNT_TYPE_ADMINISTRATOR OR $usr[0]->id_access_level == ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR){
            $employees = $this->v_model->get_employees_info($this->user_auth->get('id_company'));
        }

        if($usr[0]->id_access_level == ACCOUNT_TYPE_UNIT_MANAGER){
            $employees = $this->v_model->get_manager_employees_info($this->id_company, $this->user_auth->get('id_user'));
        }

        $id_user = $this->user_auth->get('id_user');
        $this->_update_all_leave_allocation();
        $data['vsum'] = $this->v_model->get_vacation_count($this->user_auth->get('id_company'));
        $data['employees'] = $employees;
        $data['vacation_types'] = $this->v_model->get_vacation_types($this->user_auth->get('id_company'));
        $data['allocated'] = $this->v_model->get_allocated_vacations($this->user_auth->get('id_company'));
        $data['managers'] = $this->v_model->get_managers($this->id_company);
        $data['statuses'] = $this->user_auth->statuses();
        $data['access'] = $usr[0]->id_access_level;
        $data['role_vacation'] = $this->v_model->get_role_vacation($this->id_company);
        $buffer = $this->load->view('vacation/allocate_vacation', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);


    }




    /*
     * Allocate vacation to employees
     * 
     * @access public
     * @return void
     */
    public function allocate_vacation(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_ALLOCATE);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $id_account_type = $this->session->userdata('account_type');
        $acct_type = $this->user_auth->get_account_type($id_account_type);

        $usr = $this->v_model->get('employees', array('id_user'=>$this->user_auth->get('id_user')));
        if($usr[0]->id_access_level != ACCOUNT_TYPE_ADMINISTRATOR AND $usr[0]->id_access_level != ACCOUNT_TYPE_UNIT_MANAGER){
            redirect('vacation/my_vacation', 'refresh');
        }

        $id_user = $this->user_auth->get('id_user');
        
        // if($usr[0]->id_access_level == ACCOUNT_TYPE_ADMINISTRATOR){

        // }
        
        $data['vsum'] = $this->v_model->get_vacation_count($this->user_auth->get('id_company'));
        $data['employees'] = $this->v_model->get_employees_info($this->user_auth->get('id_company'));
        $data['vacation_types'] = $this->v_model->get_vacation_types($this->user_auth->get('id_company'));
        $data['allocated'] = $this->v_model->get_allocated_vacations($this->user_auth->get('id_company'));
        $data['managers'] = $this->v_model->get_managers($this->id_company);
        $data['statuses'] = $this->user_auth->statuses();
        $data['pending'] = $this->v_model->get_manager_pending_vacation($this->user_auth->get('id_user'));
        $data['request'] = $this->v_model->get_employee_vacation_allowance_request($this->user_auth->get('id_company'), $this->user_auth->get('id_user'));
        $data['allowance_approver'] = $this->v_model->get_count('vacation_allowance_approver', array('id_approver'=> $id_user));
        $data['request_approver'] = $this->v_model->get_count('vacation_request_approver', array('id_approver'=>$id_user));
        $data['access'] = $usr[0]->id_access_level;
        $data['role_vacation'] = $this->v_model->get_role_vacation($this->id_company);
        $data['relief'] = $this->company_module_settings->get($this->id_company, 2, 'relief');
        $data['users'] = $this->v_model->get('users', array('id_company'=>$this->id_company));
        $buffer = $this->load->view('vacation/allocate_vacation', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func allocate_vacation


    /*
     * Allocate vacation to employees modal
     * 
     * @access public
     * @return void
     */
    public function request_vacation_modal(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_ALLOCATE);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $id_string = $this->uri->segment(3);
        $user = $this->v_model->get_by_id_string($id_string);

        if(!isset($user)){
            return false;
        }
        $a = $this->v_model->get_employee_vacation_types($user->id_user);
        $data['vacation_types'] = $this->v_model->get_employee_vacation_types($user->id_user);
        $this->load->view('vacation/allocate_vacation_modal', $data);

    }// End func allocate_vacation

    /*
     * Allocate vacation to employees
     * 
     * @access public
     * @return void
     */
    public function create_allocation(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_ALLOCATE);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $ret = false;

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        } else {
            $employee = $this->input->post('emps', true);
            $no_of_days = $this->input->post('days', true);
            $id_type = $this->input->post('id_type', true);
            $id_user = $this->input->post('id_user', true);
            $recurring = $this->input->post('recurring', true);                    
            
            // $v_manager = $this->v_model->get_vacation_manager($id_user);

            // if($v_manager->id_vacation_manager < 1){
            //     $approver = $v_manager->id_manager;
            // }
            // $approver = $v_manager->id_manager_vacation;

            $ret = $this->v_model->allocate_vacation( 
                    array(                                    
                        'id_company' => $this->user_auth->get('id_company'),
                        'id_user' => $id_user,
                        'id_type' => $id_type,
                        'no_of_days' => $no_of_days,
                        'recurring' => $recurring,
                        'date_start' => date('Y-m-d', strtotime('now'))
                    ));
        }

        if($ret) {
                
            $success_msg = 'Vacation allocated successfully!<br/>';
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('msg', $success_msg);
        }else {
            $error_msg = 'Error! Unable to allocate vacation!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            
        }
        $us = $this->v_model->get_user($id_user);
        redirect(site_url('vacation/entitlement_details/'.$us->id_string));

    }// End func create_vacation

    /*
     * Delete an allocated vacation
     * 
     * @access public
     * @return void
     */
    public function delete_allocation(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_ALLOCATE);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $ret = false;

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        } else {
            $id_allocation = $this->input->post('id_allocation', true);
            $id_user = $this->input->post('id_user', true);

            $ret = $this->v_model->delete_allocation(
                    array(                                    
                        'id_allocation' => $id_allocation                             
                    ));
        }

        if(!$ret){
            $error_msg = 'Error! Unable to delete allocation!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        }else {
            $success_msg = 'Allocation deleted successfully!';
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('msg', $success_msg);
        }
        $us = $this->v_model->get_user($id_user);
        redirect(site_url('vacation/employee_entitlement/'.$us->id_string));

    }// End func delete_allocation


    /*
     * Delete an allocated vacation
     * 
     * @access public
     * @return void
     */
    public function update_allocation_form(){
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_ALLOCATE);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $id_user = $this->uri->segment(3);
        $id_type = $this->uri->segment(4);        

        $is_valid_vacation = $this->v_model->check_employee_allocated_vacation($id_user, $id_type);

        if (!$is_valid_vacation) {
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            echo 'error';
            return;
        }

        $data['allocation'] = $this->v_model->get_employee_allocated_vacation($id_user, $id_type);
        $this->load->view('vacation/update_allocation_form', $data);
    }


    
    /*
     * update an allocated vacation
     * 
     * @access public
     * @return void
     */
    public function update_allocation(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_ALLOCATE);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $ret = false;
        $id_allocation = $this->input->post('id_allocation', true);
        $id_user = $this->input->post('id_user', true);
        $recurring  = $this->input->post('edit_recurring', true);
        $days = $this->input->post('edit_days', true);

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        } else {
                       
            $ret = $this->v_model->update_allocation($id_allocation, 
                    array(                                    
                        'no_of_days' => $days,
                        'recurring' => $recurring,
                    ));
        }

        if(!$ret){
            $error_msg = 'Error! Unable to update allocation!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        }else {
            $success_msg = 'Allocation updated successfully!';
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('msg', $success_msg);
        }
        $us = $this->v_model->get_user($id_user);
        redirect(site_url('vacation/entitlement_details/'.$us->id_string));

    }// End func update_allocation


    /*
     * employee vacation entitlement
     * 
     * @access public 
     * @return void
     */
    public function entitlement_details(){

        // Check user login session access and permission
        $this->user_auth->check_login();

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $usrs = $this->v_model->get('employees', array('id_user'=>$this->user_auth->get('id_user')));
        if($usrs[0]->id_access_level != ACCOUNT_TYPE_ADMINISTRATOR AND $usrs[0]->id_access_level != ACCOUNT_TYPE_UNIT_MANAGER){
            redirect('vacation/my_vacation', 'refresh');
        }

        $id_string = $this->uri->segment(3);
        if(!$id_string){

            $error_msg = 'Error! Invalid parameter provided!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/allocate_vacation', 'refresh');
        }
        $usr = $this->v_model->get_by_id_string($id_string);
        
        //update vacation allocation
        $this->_update_leave_allocation($usr->id_user);
        
        $data['vacation_types'] = $this->v_model->get_vacation_types($this->id_company);
        $data['entitlements'] = $this->v_model->get_employee_entitlements($usr->id_user);
        $data['user'] = $usr;
        $data['other_vacation'] = $this->v_model->get_unallocated_vaction($usr->id_user, $this->id_company);
        $data['history'] = $this->v_model->get_employee_vacation_history($usr->id_user);
        //$data['leave'] = $this->v_model->get_leave_allowance($usr->id_user);  
        $data['allowance_approver'] = $this->v_model->get_count('vacation_allowance_stages', array('id_user'=> $this->user_auth->get('id_user')));
        $data['request_approver'] = $this->v_model->get_count('vacation_request_stages', array('id_user'=>$this->user_auth->get('id_user')));
        $data['access'] = $usrs[0]->id_access_level;
        $data['request'] = $this->v_model->check_request($usr->id_user);

        $buffer = $this->load->view('vacation/employee_entitlement', $data, true);
        $this->user_nav->run_page($buffer, 'TalentBase - Vacation', false);

    }// End func employee_entitlement

    
    
    /*
     * View pending vacation requests
     * 
     * @access public
     * @return void
     */
    public function pending_request(){

        // Check user login session access and permission
        $this->user_auth->check_login();

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $usr = $this->v_model->get('employees', array('id_user'=>$this->user_auth->get('id_user')));
        $yr = date('Y');

        if($usr[0]->id_access_level == ACCOUNT_TYPE_ADMINISTRATOR OR $usr[0]->id_access_level == ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR){

            $data['pending_allw'] = $this->v_model->admin_get_pending_allowance($this->id_company, $yr); 
            $data['pending'] = $this->v_model->get_admin_pending_vacation();
            $data['pending_plan'] = $this->v_model->get('vacation_plan', array('status'=>'Pending'));

        }
        elseif($usr[0]->id_access_level == ACCOUNT_TYPE_UNIT_MANAGER ){

            $data['pending_allw'] = '';//$this->v_model->manager_get_pending_allowance($this->id_company, $yr, $this->user_auth->get('id_user'));
            $data['pending'] = $this->v_model->get_manager_pending_vacation($this->user_auth->get('id_user'));
            $data['pending_plan'] = $this->v_model->get_manager_pending_plan('vacation_plan', array('status'=>'Pending'));
        }
        else{
            redirect('vacation/my_vacation', 'refresh');
        }
        $data['request'] = $this->v_model->get_employee_vacation_allowance_request($this->user_auth->get('id_company'), $this->user_auth->get('id_user'));
        $data['ongoing'] = $this->v_model->get_manager_ongoing_vacation($this->user_auth->get('id_user'));
        $data['holidays'] = $this->v_model->get_holidays($this->id_company);
        $data['allowance_approver'] = $this->v_model->get_count('vacation_allowance_approver', array('id_approver'=> $this->user_auth->get('id_user')));
        $data['request_approver'] = $this->v_model->get_count('vacation_request_approver', array('id_approver'=>$this->user_auth->get('id_user')));
        $data['access'] = $usr[0]->id_access_level;
        
        $data['permissions'] = array();
        if($this->user_auth->have_perm(VACATION_REQUEST_APPROVE)) {
            array_push($data['permissions'], VACATION_REQUEST_APPROVE);
        }

        if($this->user_auth->have_perm(VACATION_REQUEST_DECLINE)) {
            array_push($data['permissions'], VACATION_REQUEST_DECLINE);
        }

        $this->mixpanel->track('page load',  array(
            "distinct_id" =>  $this->session->userdata('id_string'),
            "module id"=>"2",
            "module title"=>"Vaction",
            "page title" => "Vacation - Pending Approvals", 
            "page id" => "2"
        ));

        $buffer = $this->load->view('vacation/pending_approvals', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func pending_approvals

    /*
     * View ongoiong vacation requests
     * 
     * @access public
     * @return void
     */
    public function ongoing_vacation(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_TYPES);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $usr = $this->v_model->get('employees', array('id_user'=>$this->user_auth->get('id_user')));
        if($usr[0]->id_access_level != ACCOUNT_TYPE_ADMINISTRATOR AND $usr[0]->id_access_level != ACCOUNT_TYPE_UNIT_MANAGER){
            redirect('vacation/my_vacation', 'refresh');
        }

        $data['pending'] = $this->v_model->get_manager_pending_vacation($this->user_auth->get('id_user'));
        $data['request'] = $this->v_model->get_employee_vacation_allowance_request($this->user_auth->get('id_company'), $this->user_auth->get('id_user'));
        $data['ongoing'] = $this->v_model->get_manager_ongoing_vacation($this->user_auth->get('id_user'));
        $data['holidays'] = $this->v_model->get_holidays($this->id_company);
        $data['allowance_approver'] = $this->v_model->get_count('vacation_allowance_approver', array('id_user'=> $this->user_auth->get('id_user')));
        $data['request_approver'] = $this->v_model->get_count('vacation_request_approver', array('id_user'=>$this->user_auth->get('id_user')));
        $data['access'] = $usr[0]->id_access_level;
        
        $data['permissions'] = array();
        if($this->user_auth->have_perm(VACATION_REQUEST_APPROVE)) {
            array_push($data['permissions'], VACATION_REQUEST_APPROVE);
        }

        if($this->user_auth->have_perm(VACATION_REQUEST_DECLINE)) {
            array_push($data['permissions'], VACATION_REQUEST_DECLINE);
        }

        $this->mixpanel->track('page load',  array(
            "distinct_id" =>  $this->session->userdata('id_string'),
            "module id"=>"2",
            "module title"=>"Vacation",
            "page title" => "Vacation - Ongoing Vacation", 
            "page id" => "2"
        ));

        $buffer = $this->load->view('vacation/ongoing_vacation', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func ongoing_vacation


    /*
     * employee vacation allowance
     * 
     * @access public
     * @return void
     */
    public function vacation_allowance(){

        // Check user login session access and permission
        $this->user_auth->check_login();


        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }
        $usr = $this->v_model->get('employees', array('id_user'=> $this->session->userdata('id_user')));

        $yr = date('Y');
        if($usr[0]->id_access_level == ACCOUNT_TYPE_ADMINISTRATOR OR $usr[0]->id_access_level == ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR){

            $data['request'] = $this->v_model->get('vacation_allowance_request', array('id_company'=>$this->id_company));
            $data['allowance'] = $this->v_model->admin_get_all_allowance($this->id_company, $yr);
            // $data['pending_allw'] = $this->v_model->admin_get_pending_allowance($this->id_company, $yr); 
            // $data['pending'] = $this->v_model->get_admin_pending_vacation($this->user_auth->get('id_user')); 
            // $data['history'] = $this->v_model->admin_get_allowance_history($this->id_company, $yr);
        }
        elseif($usr[0]->id_access_level == ACCOUNT_TYPE_UNIT_MANAGER ){

            $data['request'] = $this->v_model->get('vacation_allowance_request', array('year'=>$yr, 'id_company'=>$this->id_company, 'id_user'=>$this->user_auth->get('id_user'), 'status'=>1));
            $data['allowance'] = $this->v_model->manager_get_all_allowance($this->id_company, $yr, $this->user_auth->get('id_user')); 
            // $data['pending_allw'] = $this->v_model->manager_get_pending_allowance($this->id_company, $yr, $this->user_auth->get('id_user'));
            // $data['pending'] = $this->v_model->get_manager_pending_vacation($this->user_auth->get('id_user'));
            // $data['history'] = $this->v_model->manager_get_allowance_history($this->id_company, $yr, $this->user_auth->get('id_user'));
        }
                
        $buffer = $this->load->view('vacation/vacation_allowance', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func vacation_allowance


    /*
     * employee vacation allowance
     * 
     * @access public
     * @return void
     */
    public function pending_vacation_allowance(){

        // Check user login session access and permission
        $this->user_auth->check_login();


        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }
        $usr = $this->v_model->get('employees', array('id_user'=> $this->session->userdata('id_user')));

        $yr = date('Y');
        if($usr[0]->id_access_level == ACCOUNT_TYPE_ADMINISTRATOR OR $usr[0]->id_access_level == ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR){

            $data['pending_allw'] = $this->v_model->admin_get_pending_allowance($this->id_company, $yr); 
            $data['pending'] = $this->v_model->get_admin_pending_vacation($this->user_auth->get('id_user'));
            $data['pending_plan'] = $this->v_model->get('vacation_plan', array('status'=>'Pending'));

        }
        elseif($usr[0]->id_access_level == ACCOUNT_TYPE_UNIT_MANAGER ){

            $data['pending_allw'] = $this->v_model->manager_get_pending_allowance($this->id_company, $yr, $this->user_auth->get('id_user'));
            $data['pending'] = $this->v_model->get_manager_pending_vacation($this->user_auth->get('id_user'));
            $data['pending_plan'] = $this->v_model->get_manager_pending_plan('vacation_plan', array('status'=>'Pending'));
        }       
        
        $buffer = $this->load->view('vacation/pending_vacation_allowance', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func vacation_allowance


    /*
     * employee vacation allowance
     * 
     * @access public
     * @return void
     */
    public function vacation_allowance_history(){

        // Check user login session access and permission
        $this->user_auth->check_login();


        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }
        $usr = $this->v_model->get('employees', array('id_user'=> $this->session->userdata('id_user')));

        $yr = date('Y');
        if($usr[0]->id_access_level == ACCOUNT_TYPE_ADMINISTRATOR OR $usr[0]->id_access_level == ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR){

            $data['pending'] = $this->v_model->admin_get_pending_allowance($this->id_company, $yr);  
            $data['history'] = $this->v_model->admin_get_allowance_history($this->id_company, $yr);
        }
        elseif($usr[0]->id_access_level == ACCOUNT_TYPE_UNIT_MANAGER ){

            $data['pending'] = $this->v_model->manager_get_pending_allowance($this->id_company, $yr, $this->user_auth->get('id_user'));
            $data['history'] = $this->v_model->manager_get_allowance_history($this->id_company, $yr, $this->user_auth->get('id_user'));
        }
        else{
            redirect('vacation/my_vacation', 'refresh');
        }
        
        
        $buffer = $this->load->view('vacation/vacation_allowance_history', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func vacation_allowance


    /*
     * vacation allowance details
     * 
     * @access public
     * @return void
     */
    public function vacation_allowance_details(){

        // Check user login session access and permission
        $this->user_auth->check_login();

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $id = $this->uri->segment(3);
        if(empty($id) OR !is_numeric($id)){
            $error_msg = 'Error! Invalid parameter provided!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/vacation_allowance', 'refresh');
        }

        $lv = $this->v_model->get('compensation_employee_leave_allowance', array('id_employee_leave_allowance'=> $id));

        if(empty($lv)){
            $error_msg = 'Error! Invalid parameter provided!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/vacation_allowance', 'refresh');
        }

        $year = date('Y');
        $allw = $this->v_model->get_employee_allowance($this->id_company, $year, $lv[0]->id_compensation_employee); 
        $usr = $this->v_model->get('employees', array('id_user'=> $this->session->userdata('id_user')));
        if($usr[0]->id_access_level == ACCOUNT_TYPE_ADMINISTRATOR OR $usr[0]->id_access_level == ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR){

            $data['pending'] = $this->v_model->admin_get_pending_allowance($this->id_company, $year);  
        }
        elseif($usr[0]->id_access_level == ACCOUNT_TYPE_UNIT_MANAGER ){

            $data['pending'] = $this->v_model->manager_get_pending_allowance($this->id_company, $year, $this->user_auth->get('id_user'));

        }
        $data['allowance'] = $allw;
        $data['allowance_history'] = $this->v_model->get_employee_allowance_history($allw[0]->id_user, $this->id_company, $year);

        $buffer = $this->load->view('vacation/vacation_allowance_details', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func vacation_allowance_details



    /*
     * vacation allowance details
     * 
     * @access public
     * @return void
     */
    public function entitlement_allowance_details(){

        // Check user login session access and permission
        $this->user_auth->check_login();

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $id_string = $this->uri->segment(3);
        if(empty($id_string)){
            $error_msg = 'Error! Invalid parameter provided!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/vacation_allowance', 'refresh');
        }

        $u = $this->v_model->get('users', array('id_string'=>$id_string));
        if(empty($u)){
            $error_msg = 'Error! Invalid parameter provided!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/vacation_allowance', 'refresh');
        }

        //$lv = 

        $year = date('Y');
        $allw = $this->v_model->get_employee_leave_allowance($u[0]->id_user, $this->id_company, $year);//$this->v_model->get_employee_allowance($this->id_company, $year, $lv[0]->id_compensation_employee); 
        $usr = $this->v_model->get('employees', array('id_user'=> $this->session->userdata('id_user')));
        if($usr[0]->id_access_level == ACCOUNT_TYPE_ADMINISTRATOR OR $usr[0]->id_access_level == ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR){

            $data['pending'] = $this->v_model->admin_get_pending_allowance($this->id_company, $year);  
        }
        elseif($usr[0]->id_access_level == ACCOUNT_TYPE_UNIT_MANAGER ){

            $data['pending'] = $this->v_model->manager_get_pending_allowance($this->id_company, $year, $this->user_auth->get('id_user'));

        }
        $data['allowance'] = $allw;
        $data['allowance_history'] = $this->v_model->get_employee_allowance_history($allw[0]->id_user, $this->id_company, $year);

        $buffer = $this->load->view('vacation/entitlement_allowance_details', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func vacation_allowance_details



    /*
     * vacation allowance details
     * 
     * @access public
     * @return void
     */
    public function my_vacation_allowance_details(){

        // Check user login session access and permission
        $this->user_auth->check_login();

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $id_string = $this->uri->segment(3);
        if(empty($id_string)){
            $error_msg = 'Error! Invalid parameter provided!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/my_vacation_allowance', 'refresh');
        }

        $usr = $this->v_model->get('users', array('id_string'=> $id_string));
        $year = date('Y');

        if(empty($usr)){
            $error_msg = 'Error! Invalid parameter provided!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/my_vacation_allowance', 'refresh');
        }

        
        $data['allowance'] = $this->v_model->get_employee_leave_allowance($usr[0]->id_user, $this->id_company, $year);
        $data['allowance_history'] = $this->v_model->get_employee_allowance_history($this->user_auth->get('id_user'), $this->id_company, $year);

        $buffer = $this->load->view('vacation/my_vacation_allowance_details', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func vacation_allowance_details




    /*
     * vacation allowance requests
     * 
     * @access public
     * @return void
     */
    public function allowance_request(){

        // Check user login session access and permission
        $this->user_auth->check_login();

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $usr = $this->v_model->get('employees', array('id_user'=>$this->user_auth->get('id_user')));
                
        $data['pending'] = $this->v_model->get_manager_pending_vacation($this->user_auth->get('id_user'));
        $data['request'] = $this->v_model->get_employee_vacation_allowance_request($this->user_auth->get('id_company'), $this->user_auth->get('id_user'));
        $data['allowance_approver'] = $this->v_model->get_count('vacation_allowance_approver', array('id_approver'=> $this->user_auth->get('id_user')));
        $data['request_approver'] = $this->v_model->get_count('vacation_request_approver', array('id_user'=>$this->user_auth->get('id_user')));
        $data['admin'] = $this->v_model->get('users', array('account_type'=>1, 'id_company'=>$this->id_company));
        $data['access'] = $usr[0]->id_access_level;

        
        $data['permissions'] = array();
        if($this->user_auth->have_perm(VACATION_REQUEST_APPROVE)) {
            array_push($data['permissions'], VACATION_REQUEST_APPROVE);
        }

        if($this->user_auth->have_perm(VACATION_REQUEST_DECLINE)) {
            array_push($data['permissions'], VACATION_REQUEST_DECLINE);
        }

        $this->mixpanel->track('page load',  array(
            "distinct_id" =>  $this->session->userdata('id_string'),
            "module id"=>"2",
            "module title"=>"Vacation",
            "page title" => "Vacation - Allowance Request", 
            "page id" => "2"
        ));

        $buffer = $this->load->view('vacation/allowance_request', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func allowance_request


    /*
     * make vacation allowance requests
     * 
     * @access public
     * @return void
     */
    public function make_allowance_request(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/vacation_allowance_details', 'refresh');
            return;
        } 
        

        // if($this->input->post('is_admin_request')){
        //     $em = $this->input->post('id_user2');
        //     $usr = $this->v_model->get_employee_details($em);
        //     $lv = $this->v_model->get('compensation_employee_leave_allowance');
        //     $data = array(
        //             'id_user'=> $em, 
        //             'id_company' => $this->id_company,
        //             'date_requested' => date('Y-m-d'),
        //             'amount'=> $lv[0]->amount,
        //             'year' => date('Y'),
        //             'status' => 2
        //         );

        //     $in = $this->v_model->insert_allowance_request($data);

        //     if($in){
        //         //approve request
        //         $sms_params['id_module'] = VACATION_MODULE;
        //         //send sms to requestee
        //         if(isset($usr->work_number)){
        //             //send sms
        //             $sms_params['id_module'] = '';
        //             $phone = str_replace(array('(', ')', ' ', '-'), '', $usr->work_number);
        //             $company_id_string =$this->user_auth->get('company_id_string');
        //             $message = "Hi, {$usr->first_name} {$usr->last_name}, your vacation allowance has been requested and approved by your manager/admin. ";
        //             $sms_params['message'] = $message;
        //             $sms_params['recipients']['id_user'] = $usr->id_user;
        //             $sms_params['recipients']['number'] = $usr->id_country_code.$phone;
        //             $sms_params['sender'] = substr($this->company_name, 0, 11);
        //             $this->sms_lib->send_sms($sms_params);
        //         }


        //         $error_msg = 'Your request has been successfully sent.';
        //         $this->session->set_flashdata('type', 'success');
        //         $this->session->set_flashdata('msg', $error_msg);
        //         redirect('vacation/vacation_allowance_details', 'refresh');
        //     }
        //     else{
        //         $error_msg = 'There was a problem processing your request.';
        //         $this->session->set_flashdata('type', 'error');
        //         $this->session->set_flashdata('msg', $error_msg);
        //         redirect('vacation/vacation_allowance_details', 'refresh');
        //     }
        // }
        if($this->input->server('REQUEST_METHOD')=='POST'){
            $id_user = $this->input->post('id_user');

            $year = date('Y');
            $comp = $this->v_model->get('vacation_allowance_request', array('id_user'=>$id_user, 'id_company'=>$this->id_company, 'year'=>$year, 'status'=>2));            
            
            if(!empty($comp)){
                $error_msg = 'Leave allowance for the year '.$year.' has been collected by you.';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
                if($this->input->post('employee')){
                    redirect('vacation/my_vacation_allowance', 'refresh');
                }
                else{
                    redirect('vacation/vacation_allowance', 'refresh');
                }
                return;
            }
            $allowance = $this->v_model->get_employee_leave_allowance($id_user, $this->id_company, $year);

            $data = array(
                    'id_user'=> $id_user, 
                    'id_company' => $this->id_company,
                    'amount'=>$allowance[0]->amount,
                    'date_requested' => date('Y-m-d'),
                    'year' => $year
                );

            $stage_count = $this->v_model->get_last_stage($this->id_company);
            if(empty($stage_count)){
                
                $data['status'] = 2;
                $in = $this->v_model->insert_allowance_request($data);

                if($in){
                    $error_msg = 'Success! Your leave allowance has been approved.';
                    $this->session->set_flashdata('type', 'success');
                    $this->session->set_flashdata('msg', $error_msg);
                }
                else{
                    $error_msg = 'Error! There was a problem processing the request.';
                    $this->session->set_flashdata('type', 'error');
                    $this->session->set_flashdata('msg', $error_msg);
                }

                if($this->input->post('employee')){
                    redirect('vacation/my_vacation_allowance', 'refresh');
                }
                else{
                    redirect('vacation/vacation_allowance', 'refresh');
                }
                return;
            }

            $data['status'] = 1;
            $in = $this->v_model->insert_allowance_request($data);
            $admin = $this->v_model->get('employees', array('id_access_level'=>1, 'id_company'=>$this->id_company));    

            //for ($i=1; $i <= $stage_count; $i++) { 
            $stage = $this->v_model->get('vacation_allowance_stages', array('id_company'=>$this->id_company, 'stage'=>1));
            $approval_stage = 1;
            if($stage[0]->approver == 'Unit Manager'){
                $employee = $this->v_model->get('employees', array('id_user'=>$id_user));
                $id_approver = $employee[0]->id_manager;
            }
            elseif($stage[0]->approver == 'Administrator'){
                $id_approver = $admin[0]->id_user;
            }
            else{
                $id_approver = $stage[0]->id_user;
            }

            $status = $this->v_model->user_has_access($id_approver);
            if($status == 1){
                $approver = $this->v_model->get_employee_info($this->id_company, $id_approver);
            }
            else{
                
                $app = $this->v_model->get('users', array('account_type'=>1, 'id_company'=>$this->id_company));
                $approver = $this->v_model->get_employee_info($this->id_company, $app[0]->id_user);

            }

            //data for insert
            $approver_list = array(
                        'id_vacation_allowance_request'=> $in,
                        'id_allowance_stage' => $stage[0]->id_allowance_stage,
                        'id_approver' => $approver->id_user,
                        'id_user' => $id_user,
                        'stage' => 1,
                        'id_company' => $this->id_company,
                        'status' => 0
                    );
            $q = $this->v_model->add('vacation_allowance_approver', $approver_list, true);

            if($q){

                //add task
                $link = anchor('vacation/allowance_request', 'You have a vacation allowance request pending.');
                $task[] = array(
                                'id_user_from' => $this->id_user_logged_in,
                                'id_user_to' => $approver->id_user,
                                'id_module' => 2,
                                'subject' => $link,
                                'status' => 2,
                                'tb_name' => 'vacation_allowance_approver',
                                'tb_column_name' => 'id_allowance_approver',
                                'tb_id_record' => $in,
                                'date_will_end' => date('Y-m-d', strtotime('+ 2 days'))
                            );
                $this->tasks->push_task($task);

                $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
                $domainName = $_SERVER['HTTP_HOST'].'/';

                //send email
                $subject = "Vacation Allowance Requested ";
                $first_name = $this->user_auth->get('display_name');
                $last_name = $this->user_auth->get('last_name');
                $k = array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'company_name' => $this->company_name,
                    'subdomain' => $this->user_auth->get('company_id_string'),
                    'domain' => $protocol.$domainName,
                    'department' => $user->departments,
                    'role' => $user->roles,
                    'location' => $user->location_tag
                    );
                $msg = $this->load->view('email_templates/vacation_stage_allowance', $k, true);

                $this->load->library('mailgun_api');
                $this->mailgun_api->send_mail(
                                        array(
                                            'from' => $this->company_name.' <noreply@testbase.com.ng>',
                                            'to' => $approver->email,
                                            'subject' => $subject,
                                            'html' => '<html>'. $msg .'</html>'
                                        ));

                //send sms
                $sms_params['id_module'] = '';
                $phone = $this->user_auth->get_employee_phone_number($approver->id_user);
                $phone = str_replace(array('(', ')', ' ', '-'), '', $phone);

                if(!empty($phone) && $this->sms_switch > 0){    
                    $company_id_string =$this->user_auth->get('company_id_string');
                    $message = "{$first_name} {$last_name} has made a vacation allowance request. "
                            . "Kindly log into the employee portal at {$this->company_url} to approve or decline.";
                    $sms_params['message'] = $message;
                    $sms_params['recipients']['id_user'] = $approver->id_user;
                    $sms_params['recipients']['number'] = '+' .$phone;
                    $sms_params['sender'] = substr($this->company_name, 0, 11);
                    $res = $this->sms_lib->send_sms($sms_params);
                }
                
                $error_msg = 'Your request has been successfully sent.';
                $this->session->set_flashdata('type', 'success');
                $this->session->set_flashdata('msg', $error_msg); 
            }
            else{
                $error_msg = 'There was a problem adding into database!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
            }

            if($this->input->post('employee')){
                redirect('vacation/my_vacation_allowance', 'refresh');
            }
            else{
                redirect('vacation/vacation_allowance', 'refresh');
            }
        }
        else{
            $error_msg = 'Error! Invalid parameter provided.';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/my_vacation', 'refresh');
        }

    }// End func make_allowance_request


     /*
     * cancel vacation allowance requests
     * 
     * @access public
     * @return void
     */
    public function cancel_allowance_request(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/vacation_allowance', 'refresh');
            return;
        } else {

            $id_request = $this->input->post('id_request2');
            $id_user = $this->input->post('user');
            $year = $this->input->post('year');

            //delete approvers and allowance request
            $this->v_model->delete('vacation_allowance_approver', array('id_vacation_allowance_request'=>$id_request));
            $q = $this->v_model->delete_allowance_request($id_request);
            
            if($q){

                //update task
                $t = $this->v_model->get('tasks', array('tb_name'=> 'vacation_allowance_approver', 'tb_column_name'=> 'id_allowance_approver', 'tb_id_record' => $id_request));
                if(!empty($t)){
                    //$this->tasks->delete_task($t[0]->id_task);
                    $this->v_model->delete('tasks', array('id_task'=>$t[0]->id_task));
                }

                $error_msg = 'Your request has been successfully cancelled.';
                $this->session->set_flashdata('type', 'success');
                $this->session->set_flashdata('msg', $error_msg); 
            }
            else{
                $error_msg = 'There was a problem adding into database!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
            }

            if($this->input->post('employee')){
                redirect('vacation/my_vacation_allowance', 'refresh');
            }
            else{
                redirect('vacation/vacation_allowance', 'refresh');
            }
            
        }

    }// End func cancel_allowance_request


     /*
     * Approve vacation allowance requests
     * 
     * @access public
     * @return void
     */
    public function approve_allowance_request(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/vacation_allowance', 'refresh');
            return;
        } else {

            $id_request = $this->input->post('id_request');

            //get stage            
            $apprv = $this->v_model->get('vacation_allowance_approver', array('id_vacation_allowance_request'=>$id_request));
            $tr = $this->v_model->get_allowance_trig($apprv[0]->id_allowance_approver);

            $admin = $this->v_model->get('users', array('account_type'=>1, 'id_company'=>$this->id_company));
            $stages = $this->v_model->get_last_stage($this->id_company);
            $usr = $this->v_model->get_employee_info($this->id_company, $tr->id_user);

            if($tr->stage == $stages){
                $data = array(
                    'status' => 2
                );

                $this->v_model->update('vacation_allowance_approver', array('status'=>1), array('id_allowance_approver'=>$id_request));
                $q = $this->v_model->update_allowance_request($tr->id_vacation_allowance_request, $data);
                
                if($q){

                    //update task
                    $t = $this->v_model->get('tasks', array('tb_name'=>'vacation_allowance_approver', 'tb_column_name'=>'id_allowance_approver', 'tb_id_record'=>$id_request));
                    $task = array(
                            'id_task' => $t[0]->id_task,
                            'status' => TASK_STATUS_COMPLETE
                        );

                    $this->tasks->update_task($task);

                    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
                    $domainName = $_SERVER['HTTP_HOST'].'/';

                    $sms_params['id_module'] = '';
                    $company_id_string =$this->user_auth->get('company_id_string');
                    $phone = $this->user_auth->get_employee_phone_number($usr->id_user);
                    $phone = str_replace(array('(', ')', ' ', '-'), '', $phone);
                    
                    if(!empty($phone) && $this->sms_switch > 0){
                        $message = "Your vacation allowance request has been approved. "
                                . "Kindly log into the employee portal at {$this->company_url} to view the details.";
                        $sms_params['message'] = $message;
                        $sms_params['recipients']['id_user'] = $usr->id_user;
                        $sms_params['recipients']['number'] = '+' .$phone;
                        $sms_params['sender'] = substr($this->company_name, 0, 11);
                        $res = $this->sms_lib->send_sms($sms_params);
                    }

                    $subject = "Vacation Allowance Approved ";
                    $k = array(
                        'first_name' => $this->user_auth->get('display_name'),
                        'last_name' => $this->user_auth->get('last_name'),
                        'company_name' => $this->company_name,
                        'subdomain' => $this->user_auth->get('company_id_string'),
                        'domain' => $protocol.$domainName,
                        'department' => $usr->departments,
                        'role' => $usr->roles,
                        'location' => $usr->location_tag
                    );

                    $msg = $this->load->view('email_templates/vacation_allowance_approved', $k, true);
                    $this->load->library('mailgun_api');
                    $this->mailgun_api->send_mail(
                                            array(
                                                'from' => $this->company_name.' <noreply@testbase.com.ng>',
                                                'to' => $usr->email,
                                                'subject' => $subject,
                                                'html' => '<html>'. $msg .'</html>'
                                            ));

                }
            }
            else{
                $next_stage = $tr->stage + 1;
                    
                $st = $this->v_model->get('vacation_allowance_stages', array('id_company'=>$this->id_company, 'stage'=>$next_stage));
                
                if($st[0]->approver == 'Unit Manager'){
                    $id_manager = $usr->id_manager;
                }
                elseif($st[0]->approver == 'Administrator'){
                    $id_manager = $admin[0]->id_user;
                }
                else{
                    $id_manager = $st[0]->id_user;
                }

                $status = $this->v_model->user_has_access($id_manager);
                $company_id_string =$this->user_auth->get('company_id_string');
                if($status == 1){
                    $u = $this->v_model->get_employee_info($this->id_company, $id_manager); 
                    $email_content = 'email_templates/vacation_stage_allowance';
                    $sms_content = "{$usr->first_name} {$usr->last_name} has made a vacation allowance request. "
                        . "Kindly log into the employee portal at {$this->company_url} to approve or decline.";  
                }
                else{
                    $u = $this->v_model->get_employee_info($this->id_company, $admin[0]->id_user);
                    $email_content = 'email_templates/vacation_stage_allowance_not_approver';
                    $sms_content = "{$usr->first_name} {$usr->last_name} has made a vacation allowance request. "
                        . "Kindly log into the employee portal at {$this->company_url} to approve or decline.";
                }
                    
                $approver_list = array(
                            'id_vacation_allowance_request'=> $tr->id_vacation_allowance_request,
                            'id_allowance_stage' => $st[0]->id_allowance_stage,
                            'status' => 0,
                            'id_approver' => $u->id_user,
                            'id_user' => $tr->id_user,
                            'stage' => $next_stage,
                            'id_company' => $this->id_company
                        );              
                
                $this->v_model->update('vacation_allowance_approver', array('status'=>1), array('id_allowance_approver'=>$id_request));
                $q = $this->v_model->add('vacation_allowance_approver', $approver_list, true);

                if($q){
                    //update
                    $t = $this->v_model->get('tasks', array('tb_name'=>'vacation_allowance_approver', 'tb_column_name'=>'id_allowance_approver', 'tb_id_record'=>$id_request));
                    $task = array(
                            'id_task' => $t[0]->id_task,
                            'status' => TASK_STATUS_COMPLETE
                        );

                    $this->tasks->update_task($task);

                    //add task
                    $link = anchor('vacation/allowance_request', 'You have a vacation allowance request pending.');
                    $task_data[] = array(
                                'id_user_from' => $usr->id_user,
                                'id_user_to' => $u->id_user,
                                'id_module' => 2,
                                'subject' => $link,
                                'status' => 2,
                                'tb_name' => 'vacation_allowance_approver',
                                'tb_column_name' => 'id_allowance_approver',
                                'tb_id_record' => $q,
                                'date_will_end' => date('Y-m-d', strtotime('+ 2 days'))
                            );
                    $this->tasks->push_task($task_data);

                    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
                    $domainName = $_SERVER['HTTP_HOST'].'/';

                    $subject = "Vacation Allowance Requested ";
                    $k = array(
                        'first_name' => $usr->first_name,
                        'last_name' => $usr->last_name,
                        'company_name' => $this->company_name,
                        'subdomain' => $this->user_auth->get('company_id_string'),
                        'domain' => $protocol.$domainName,
                        'department' => $usr->departments,
                        'role' => $usr->roles,
                        'location' => $usr->location_tag
                        );
                    $msg = $this->load->view($email_content, $k, true);

                    $this->load->library('mailgun_api');
                    $this->mailgun_api->send_mail(
                                            array(
                                                'from' => $this->company_name.' <noreply@testbase.com.ng>',
                                                'to' => $u->email,
                                                'subject' => $subject,
                                                'html' => '<html>'. $msg .'</html>'
                                            ));
                    $phone = $this->user_auth->get_employee_phone_number($u->id_user);
                    $phone = str_replace(array('(', ')', ' ', '-'), '', $phone);
                    if(!empty($phone) && $this->sms_switch > 0){
                        $sms_params['id_module'] = 2;
                        $sms_params['message'] = $sms_content;
                        $sms_params['recipients']['id_user'] = $u->id_user;
                        $sms_params['recipients']['number'] = '+' .$phone;
                        $sms_params['sender'] = substr($this->company_name, 0, 11);
                        $res = $this->sms_lib->send_sms($sms_params);
                    }

                    $error_msg = 'The request has been successfully approved.';
                    $this->session->set_flashdata('type', 'success');
                    $this->session->set_flashdata('msg', $error_msg);
                }
                else{
                    $error_msg = 'There was a problem adding into database!';
                    $this->session->set_flashdata('type', 'error');
                    $this->session->set_flashdata('msg', $error_msg);
                }
            }

            redirect('vacation/vacation_allowance', 'refresh');
        }

    }// End func approve_allowance_reqest


     /*
     * decline vacation allowance requests
     * 
     * @access public
     * @return void
     */
    public function decline_allowance_request(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/vacation_allowance', 'refresh');
            return;
        } else {

            $id_request = $this->input->post('id_request2');
            $data = array(
                    'status' => 0
                );
            
            $q = $this->v_model->update_allowance_request($id_request, $data);

            if($q){

                $tr = $this->v_model->get('vacation_allowance_request', array('id_vacation_allowance_request'=>$id_request));
                $usr = $this->v_model->get_employee_info($this->id_company, $tr[0]->id_user);

                $sms_params['id_module'] = '';
                $phone = $this->user_auth->get_employee_phone_number($usr->id_user);
                $phone = str_replace(array('(', ')', ' ', '-'), '', $phone);
                
                if(!empty($phone) && $this->sms_switch > 0){
                    $company_id_string =$this->user_auth->get('company_id_string');
                    $message = "Your vacation allowance request has been declined. "
                            . "Kindly log into the employee portal at {$this->company_url} to view the details.";
                    $sms_params['message'] = $message;
                    $sms_params['recipients']['id_user'] = $usr->id_user;
                    $sms_params['recipients']['number'] = '+' .$phone;
                    $sms_params['sender'] = substr($this->company_name, 0, 11);
                    $res = $this->sms_lib->send_sms($sms_params);
                }

                $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
                $domainName = $_SERVER['HTTP_HOST'].'/';

                $subject = "Vacation Allowance Declined ";
                $k = array(
                    'first_name' => $usr->first_name,
                    'last_name' => $usr->last_name,
                    'company_name' => $this->company_name,
                    'subdomain' => $this->user_auth->get('company_id_string'),
                    'domain' => $protocol.$domainName,
                    'department' => $usr->departments,
                    'role' => $usr->roles,
                    'location' => $usr->location_tag
                );

                $msg = $this->load->view('email_templates/vacation_allowance_declined', $k, true);
                $this->load->library('mailgun_api');
                $this->mailgun_api->send_mail(
                                        array(
                                            'from' => $this->company_name.' <noreply@testbase.com.ng>',
                                            'to' => $usr->email,
                                            'subject' => $subject,
                                            'html' => '<html>'. $msg .'</html>'
                                        ));

                $error_msg = 'The request has been successfully declined.';
                $this->session->set_flashdata('type', 'success');
                $this->session->set_flashdata('msg', $error_msg); 
            }
            else{
                $error_msg = 'There was a problem adding into database!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
            }

            redirect('vacation/vacation_allowance', 'refresh');
        }

    }// End func decline_allowance_request




    /*
     * Approve vacation allowance requests
     * 
     * @access public
     * @return void
     */
    public function bulk_approve_allowance_request(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/vacation_allowance', 'refresh');
            return;
        } else {

            $request_post = $this->input->post('id_request3');

            foreach ($request_post as $id_request) {
            
                //get stage            
                $apprv = $this->v_model->get('vacation_allowance_approver', array('id_vacation_allowance_request'=>$id_request));
                $tr = $this->v_model->get_allowance_trig($apprv[0]->id_allowance_approver);

                $admin = $this->v_model->get('users', array('account_type'=>1, 'id_company'=>$this->id_company));
                $stages = $this->v_model->get_last_stage($this->id_company);
                $usr = $this->v_model->get_employee_info($this->id_company, $tr->id_user);

                if($tr->stage == $stages){
                    $data = array(
                        'status' => 2
                    );

                    $this->v_model->update('vacation_allowance_approver', array('status'=>1), array('id_allowance_approver'=>$id_request));
                    $q = $this->v_model->update_allowance_request($tr->id_vacation_allowance_request, $data);
                    
                    if($q){

                        //update task
                        $t = $this->v_model->get('tasks', array('tb_name'=>'vacation_allowance_approver', 'tb_column_name'=>'id_allowance_approver', 'tb_id_record'=>$id_request));
                        $task = array(
                                'id_task' => $t[0]->id_task,
                                'status' => TASK_STATUS_COMPLETE
                            );

                        $this->tasks->update_task($task);

                        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
                        $domainName = $_SERVER['HTTP_HOST'].'/';

                        $sms_params['id_module'] = '';
                        $company_id_string =$this->user_auth->get('company_id_string');
                        $phone = $this->user_auth->get_employee_phone_number($usr->id_user);
                        $phone = str_replace(array('(', ')', ' ', '-'), '', $phone);
                        if(!empty($phone) && $this->sms_switch > 0){    
                            $message = "Your vacation allowance request has been approved. "
                                    . "Kindly log into the employee portal at {$this->company_url} to view the details.";
                            $sms_params['message'] = $message;
                            $sms_params['recipients']['id_user'] = $usr->id_user;
                            $sms_params['recipients']['number'] = '+' .$phone;
                            $sms_params['sender'] = substr($this->company_name, 0, 11);
                            $res = $this->sms_lib->send_sms($sms_params);
                        }

                        $subject = "Vacation Allowance Approved ";
                        $k = array(
                            'first_name' => $this->user_auth->get('display_name'),
                            'last_name' => $this->user_auth->get('last_name'),
                            'company_name' => $this->company_name,
                            'subdomain' => $this->user_auth->get('company_id_string'),
                            'domain' => $protocol.$domainName,
                            'department' => $usr->departments,
                            'role' => $usr->roles,
                            'location' => $usr->location_tag
                        );

                        $msg = $this->load->view('email_templates/vacation_allowance_approved', $k, true);
                        $this->load->library('mailgun_api');
                        $this->mailgun_api->send_mail(
                                                array(
                                                    'from' => $this->company_name.' <noreply@testbase.com.ng>',
                                                    'to' => $usr->email,
                                                    'subject' => $subject,
                                                    'html' => '<html>'. $msg .'</html>'
                                                ));

                    }
                }
                else{
                    $next_stage = $tr->stage + 1;
                        
                    $st = $this->v_model->get('vacation_allowance_stages', array('id_company'=>$this->id_company, 'stage'=>$next_stage));
                    
                    if($st[0]->approver == 'Unit Manager'){
                        $id_manager = $usr->id_manager;
                    }
                    elseif($st[0]->approver == 'Administrator'){
                        $id_manager = $admin[0]->id_user;
                    }
                    else{
                        $id_manager = $st[0]->id_user;
                    }

                    $status = $this->v_model->user_has_access($id_manager);
                    $company_id_string =$this->user_auth->get('company_id_string');
                    if($status == 1){
                        $u = $this->v_model->get_employee_info($this->id_company, $id_manager); 
                        $email_content = 'email_templates/vacation_stage_allowance';
                        $sms_content = "{$usr->first_name} {$usr->last_name} has made a vacation allowance request. "
                            . "Kindly log into the employee portal at {$this->company_url} to approve or decline.";  
                    }
                    else{
                        $u = $this->v_model->get_employee_info($this->id_company, $admin[0]->id_user);
                        $email_content = 'email_templates/vacation_stage_allowance_not_approver';
                        $sms_content = "{$usr->first_name} {$usr->last_name} has made a vacation allowance request. "
                            . "Kindly log into the employee portal at {$this->company_url} to approve or decline.";
                    }
                        
                    $approver_list = array(
                                'id_vacation_allowance_request'=> $tr->id_vacation_allowance_request,
                                'id_allowance_stage' => $st[0]->id_allowance_stage,
                                'status' => 0,
                                'id_approver' => $u->id_user,
                                'id_user' => $tr->id_user,
                                'stage' => $next_stage,
                                'id_company' => $this->id_company
                            );              
                    
                    $this->v_model->update('vacation_allowance_approver', array('status'=>1), array('id_allowance_approver'=>$id_request));
                    $q = $this->v_model->add('vacation_allowance_approver', $approver_list, true);

                    if($q){
                        //update
                        $t = $this->v_model->get('tasks', array('tb_name'=>'vacation_allowance_approver', 'tb_column_name'=>'id_allowance_approver', 'tb_id_record'=>$id_request));
                        $task = array(
                                'id_task' => $t[0]->id_task,
                                'status' => TASK_STATUS_COMPLETE
                            );

                        $this->tasks->update_task($task);

                        //add task
                        $link = anchor('vacation/allowance_request', 'You have a vacation allowance request pending.');
                        $task_data[] = array(
                                    'id_user_from' => $usr->id_user,
                                    'id_user_to' => $u->id_user,
                                    'id_module' => 2,
                                    'subject' => $link,
                                    'status' => 2,
                                    'tb_name' => 'vacation_allowance_approver',
                                    'tb_column_name' => 'id_allowance_approver',
                                    'tb_id_record' => $q,
                                    'date_will_end' => date('Y-m-d', strtotime('+ 2 days'))
                                );
                        $this->tasks->push_task($task_data);

                        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
                        $domainName = $_SERVER['HTTP_HOST'].'/';

                        $subject = "Vacation Allowance Requested ";
                        $k = array(
                            'first_name' => $usr->first_name,
                            'last_name' => $usr->last_name,
                            'company_name' => $this->company_name,
                            'subdomain' => $this->user_auth->get('company_id_string'),
                            'domain' => $protocol.$domainName,
                            'department' => $usr->departments,
                            'role' => $usr->roles,
                            'location' => $usr->location_tag
                            );
                        $msg = $this->load->view($email_content, $k, true);

                        $this->load->library('mailgun_api');
                        $this->mailgun_api->send_mail(
                                                array(
                                                    'from' => $this->company_name.' <noreply@testbase.com.ng>',
                                                    'to' => $u->email,
                                                    'subject' => $subject,
                                                    'html' => '<html>'. $msg .'</html>'
                                                ));
                        $phone = $this->user_auth->get_employee_phone_number($u->id_user);
                        $phone = str_replace(array('(', ')', ' ', '-'), '', $phone);
                        
                        if(!empty($phone) && $this->sms_switch > 0){
                            $sms_params['id_module'] = '';
                            
                            $sms_params['message'] = $sms_content;
                            $sms_params['recipients']['id_user'] = $u->id_user;
                            $sms_params['recipients']['number'] = '+' .$phone;
                            $sms_params['sender'] = substr($this->company_name, 0, 11);
                            $res = $this->sms_lib->send_sms($sms_params);
                        }

                        $error_msg = 'The request has been successfully approved.';
                        $this->session->set_flashdata('type', 'success');
                        $this->session->set_flashdata('msg', $error_msg);
                    }
                    else{
                        $error_msg = 'There was a problem adding into database!';
                        $this->session->set_flashdata('type', 'error');
                        $this->session->set_flashdata('msg', $error_msg);
                    }
                }
            }

            redirect('vacation/pending_vacation_allowance', 'refresh');
        }

    }// End func approve_allowance_reqest


     /*
     * decline vacation allowance requests
     * 
     * @access public
     * @return void
     */
    public function bulk_decline_allowance_request(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/vacation_allowance', 'refresh');
            return;
        } else {

            $post_request = $this->input->post('id_request3');

            foreach ($post_request as $id_request) {
                            
                $data = array(
                        'status' => 0
                    );
                
                $q = $this->v_model->update_allowance_request($id_request, $data);

                if($q){

                    $tr = $this->v_model->get('vacation_allowance_request', array('id_vacation_allowance_request'=>$id_request));
                    $usr = $this->v_model->get_employee_info($this->id_company, $tr[0]->id_user);

                    $sms_params['id_module'] = '';
                    $phone = $this->user_auth->get_employee_phone_number($usr->id_user);
                    $phone = str_replace(array('(', ')', ' ', '-'), '', $phone);
                    if(!empty($phone) && $this->sms_switch > 0){
                        $company_id_string =$this->user_auth->get('company_id_string');
                        $message = "Your vacation allowance request has been declined. "
                                . "Kindly log into the employee portal at {$this->company_url} to view the details.";
                        $sms_params['message'] = $message;
                        $sms_params['recipients']['id_user'] = $usr->id_user;
                        $sms_params['recipients']['number'] = '+'.$phone;
                        $sms_params['sender'] = substr($this->company_name, 0, 11);
                        $res = $this->sms_lib->send_sms($sms_params);
                    }

                    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
                    $domainName = $_SERVER['HTTP_HOST'].'/';

                    $subject = "Vacation Allowance Declined ";
                    $k = array(
                        'first_name' => $usr->first_name,
                        'last_name' => $usr->last_name,
                        'company_name' => $this->company_name,
                        'subdomain' => $this->user_auth->get('company_id_string'),
                        'domain' => $protocol.$domainName,
                        'department' => $usr->departments,
                        'role' => $usr->roles,
                        'location' => $usr->location_tag
                    );

                    $msg = $this->load->view('email_templates/vacation_allowance_declined', $k, true);
                    $this->load->library('mailgun_api');
                    $this->mailgun_api->send_mail(
                                            array(
                                                'from' => $this->company_name.' <noreply@testbase.com.ng>',
                                                'to' => $usr->email,
                                                'subject' => $subject,
                                                'html' => '<html>'. $msg .'</html>'
                                            ));

                    $error_msg = 'The request has been successfully declined.';
                    $this->session->set_flashdata('type', 'success');
                    $this->session->set_flashdata('msg', $error_msg); 

                }
                
            }

            redirect('vacation/pending_vacation_allowance', 'refresh');
        }

    }// End func decline_allowance_request


    /*
     * View vacaation summaries
     * 
     * @access public
     * @return void
     */
    public function view_summary(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_VIEW_SUMMARY);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $ret = false;
        
        $data['departments'] = $this->v_model->get_company_departments($this->user_auth->get('id_company'));
        $data['emps'] = $this->v_model->get_employees($this->user_auth->get('id_company'));
        $data['locations'] = $this->v_model->get('company_locations', array('id_company'=>$this->id_company));
        
        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $data['form_data'] = '';
            $data['employees'] = $this->v_model->get_manager_vacation_history($this->id_company, $this->id_user_logged_in);
            $data['holidays'] = $this->v_model->get_holidays($this->id_company);
        } else {

            $sd = $this->input->post('start_date');
            $ed = $this->input->post('end_date');

            if(empty($sd) OR empty($ed)){
                $error_msg = 'Error! Please enter valid dates!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
                redirect('vacation/view_summary', 'refresh');
                return;
            }
            
            
            $from = explode("/", $this->input->post('start_date'));
            $to = explode("/", $this->input->post('end_date'));
            $from_date = $from[2]."-".$from[0]."-".$from[1];
            $to_date = $to[2]."-".$to[0]."-".$to[1];
            $status = $this->input->post('vac_status');
            $dept = $this->input->post('dept');
            $location = $this->input->post('location');
            $form = array(
                    'start_date'=>$sd,
                    'end_date'=>$ed,
                    'status'=>$status,
                    'dept'=>$dept,
                    'location'=>$location

                );
            $data['form_data'] = $form;
            $data['employees'] = $this->v_model->search($from_date, $to_date, $status, $dept, $location, $this->id_company, $this->id_user_logged_in);
            $data['holidays'] = $this->v_model->get_holidays($this->id_company);
            
        }
        
        $this->mixpanel->track('page load',  array(
            "distinct_id" =>  $this->session->userdata('id_string'),
            "module id"=>"2",
            "module title"=>"Vacation",
            "page title" => "Vacation - Summary", 
            "page id" => "2"
        ));
        
        $buffer = $this->load->view('vacation/summary', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func view_summary


    /*
     * Vacation Summary
     * 
     * @access public 
     * @return void
     */
    public function vacation_summary(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }
        
        $data['empty'] = '';
        $buffer = $this->load->view('vacation/vacation_summary', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func analytics


    /*
     * Vacation Summary
     * 
     * @access public 
     * @return void
     */
    public function generate_records_excel(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        $this->user_auth->check_perm(VACATION_TYPES);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }
        
        $sd = $this->input->post('start_date');
        $ed = $this->input->post('end_date');      
        
        $from = explode("/", $this->input->post('start_date'));
        $to = explode("/", $this->input->post('end_date'));
        $from_date = $from[2]."-".$from[0]."-".$from[1];
        $to_date = $to[2]."-".$to[0]."-".$to[1];
        $status = $this->input->post('status');
        $dept = $this->input->post('dept');
        $location = $this->input->post('location');
        
        $employees = $this->v_model->search($from_date, $to_date, $status, $dept, $location, $this->id_company, $this->id_user_logged_in);

        $datum = array();
            $datum[] = 'Employee';
            $datum[] = 'Vacation Type';
            $datum[] = 'Description';
            $datum[] = 'No. of Days';
            $datum[] = 'Start';
            $datum[] = 'End';
            $datum[] = 'Status';
            
            $records[] = $datum;

            foreach ($employees as $em) {
                $data = array();
                $data[] = $em->first_name." ".$em->last_name;
                $data[] = $em->name;
                $data[] = $em->description;
                $data[] = $em->no_of_days;
                $data[] = date('F jS, Y', strtotime($em->date_start));
                $data[] = date('F jS, Y', strtotime($em->date_end));
                $data[] = $em->status;
                
                $records[] = $data;                
            }

        $name = "VacationSummary".$this->company_name;
        $this->load->library('csv_helper');
        // $data = array(
        //         'id_company' => $this->id_company,
        //         'id_user' => $this->session->userdata('id_user'),
        //         'date_time' => date('Y-m-d G:i:s'),
        //         'year' => $period
        //     );

        //$this->v_model->add('vacation_planner_report_history', $data);
        $this->csv_helper->create_record_excel_file($records, $name);
        //print_r($records);
    }// End func analytics


    /*
     * cancel vacation
     * 
     * @access public
     * @return void
     */
    public function cancel_vacation(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_PENDING_APPROVAL);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Error! Problem processing your request!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/pending_approvals', 'refresh');
            return;
        } else{

            $id_vacation = $this->input->post('id_vacation_cancel', true);
            $left = $this->input->post('vacation_left', true);

            $vac = $this->v_model->get_vacation($id_vacation);
            $now = date("Y-m-d H:i:s", strtotime('now'));
            $params1 = array('date_end'=>$now);
            $ret = $this->v_model->update_vacation($id_vacation, $params1);
            $alloc = $this->v_model->get_employee_allocated_vacation($vac->id_user, $vac->id_type);
            $left = $left + $alloc->no_of_days;
            $params2 = array('no_of_days'=>$left);
            $this->v_model->return_allocation($vac->id_type, $vac->id_user, $params2);;
        }

        if(!$ret){
            $error_msg = 'Error! Unable to cancel vacation!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        }else {

            $vac_detail = $this->v_model->get_vacation_details($id_vacation);
            $date_start = date('M j', strtotime($vac_detail->date_start));
            $date_end = date('M j', strtotime($vac_detail->date_end));
            $description = $vac_detail->description;
            $employee = $this->v_model->get_employee_details($this->id_user_logged_in);
            $manager = $this->v_model->get_employee_details($manager->id_user);
            $subject = "Vacation Cancelled ";

            $k = array(
                    'manager_name' => ucwords($manager->first_name." ".$manager->last_name),
                    'description' => $description,
                    'start_date' => $date_start,
                    'end_date' => $date_end,
                    'employee_name' => ucwords($employee->first_name." ".$employee->last_name),
                    'company_name' => $this->company_name
                );
            $msg = $this->load->view('email_templates/vacation_cancelled', $k, true);

            $this->load->library('mailgun_api');
            $this->mailgun_api->send_mail(
                                    array(
                                        'from' => $this->company_name.' <noreply@testbase.com.ng>',
                                        'to' => $employee->email,
                                        'subject' => $subject,
                                        'html' => '<html>'. $msg .'</html>'
                                    ));

            $success_msg = 'Vacation cancelled successfully!';
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('msg', $success_msg);
        }

        redirect(site_url('vacation/pending_approvals'));

    }//cancel vacation




    /*
     * Manager View
     * 
     * @access public 
     * @return void
     */
    public function manager_view(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_PENDING_APPROVAL);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $id_user = $this->session->userdata('id_user');
        $user_info = $this->v_model->get('employees', array('id_user'=>$id_user));
        $dept = $this->v_model->get('departments', array('id_head'=>$id_user, 'id_company'=>$this->id_company));

        //if manager
        if($user_info[0]->is_manager == 0){
            $error_msg = 'Error! Only managers can view this page.';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/my_vacation', 'refresh');
        } 


        if($this->input->server('REQUEST_METHOD') == 'POST'){
            $id_dept = $this->input->post('select_dept');
            $month = date('F', strtotime($this->input->post('select_month')));
            $data['emp'] = $this->v_model->manager_view_vacation_direct($this->id_company, $id_user, $month);
        }
        else{
            $month = date('F', strtotime('now'));
            $data['emp'] = $this->v_model->manager_view_vacation_direct($this->id_company, $id_user, $month);
        }

        $data['month'] = $month;
        $data['manager'] = $user_info[0]->is_manager;
        $data['dept'] = $dept;

        $buffer = $this->load->view('vacation/manager_view', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }


    /*
     * Manager View
     * 
     * @access public 
     * @return void
     */
    public function manager_view_department(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_PENDING_APPROVAL);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $id_user = $this->session->userdata('id_user');
        $user_info = $this->v_model->get('employees', array('id_user'=>$id_user));
        if($user_info[0]->id_access_level == ACCOUNT_TYPE_ADMINISTRATOR OR $user_info[0]->id_access_level == ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR){
            $dept = $this->v_model->get('departments', array('id_company'=>$this->id_company));
        }
        else{
            $dept = $this->v_model->get('departments', array('id_head'=>$id_user, 'id_company'=>$this->id_company));
        }

        if(empty($dept) AND $user_info[0]->id_access_level != ACCOUNT_TYPE_ADMINISTRATOR AND $user_info[0]->id_access_level != ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR){
            redirect('vacation/my_vacation', 'refresh');
        }

        if($this->input->server('REQUEST_METHOD') == 'POST'){
            $id_dept = $this->input->post('select_dept');
            $month = date('F', strtotime($this->input->post('select_month')));
            $data['department'] = $this->v_model->manager_view_vacation_department2($this->id_company, $id_dept, $id_user, $month);
            $data['selected'] = $id_dept;

        }
        else{
            $month = date('F', strtotime('now'));
            $data['department'] = $this->v_model->manager_view_vacation_department($this->id_company, $dept, $id_user, $month);
            $data['selected'] = 0;
        }
        $data['dept'] = $dept;
        $data['month'] = $month;
        $data['manager'] = $user_info[0]->is_manager;

        $buffer = $this->load->view('vacation/manager_view_department', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }


    /*
     * Manager View
     * 
     * @access public 
     * @return void
     */
    public function manager_view_location(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_PENDING_APPROVAL);

        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $id_user = $this->session->userdata('id_user');
        $user_info = $this->v_model->get('employees', array('id_user'=>$id_user));
        $location = $this->v_model->get_manager_employees_location($this->id_company, $id_user);
        $dept = $this->v_model->get('departments', array('id_head'=>$id_user, 'id_company'=>$this->id_company));

        if($this->input->server('REQUEST_METHOD') == 'POST'){
            $id_location = $this->input->post('select_location');
            $month = date('F', strtotime($this->input->post('select_month')));
            $data['locate'] = $this->v_model->manager_view_vacation_location2($this->id_company, $id_user, $id_location, $month);
            $data['selected'] = $id_location;
        }
        else{
            $month = date('F', strtotime('now'));
            $data['locate'] = $this->v_model->manager_view_vacation_location($this->id_company, $id_user, $location, $month);
            $data['selected'] = 0;
        }
        $data['locations'] = $location;
        $data['dept'] = $dept;
        $data['month'] = $month;
        $data['manager'] = $user_info[0]->is_manager;

        $buffer = $this->load->view('vacation/manager_view_location', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }




   
    /*
     * vacation planner
     * 
     * @access public 
     * @return void
     */
    public function vacation_planner(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_VIEW_MY);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $months = array(
                        1=>'January',
                        2=>'February',
                        3=>'March',
                        4=>'April',
                        5=>'May',
                        6=>'June',
                        7=>'July',
                        8=>'August',
                        9=>'September',
                        10=>'October',
                        11=>'November',
                        12=>'December'
                    );

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $month_date = $this->uri->segment(3);

            if(empty($month_date)){

                $selected_date = ' ';

            }
            else{
                
                $mon= substr($month_date, 0, 2);

                $mon2 = ltrim($mon, 0);
                $yr= substr($month_date, 2, 4);
                $date = array(
                        'monyr' => $months[$mon2].' '.$yr,
                        'smonth' => $mon,
                        'emonth' => $mon,
                        'syear' => $yr,
                        'eyear' => $yr
                    );

                $m = $months[$mon2];

                $month = date('F', strtotime("$m $yr")); 
                $mn = date('m', strtotime("1 $m $yr")); 
                $year = $yr;
                $selected_date = $month.' '.$year;
                $mon_yr = $mn.'_'.$yr;
            }


            if($this->session->userdata('sess')){

                $page_session = $this->session->userdata('sess');
                $data['employees'] = $this->v_model->get_employee_planner_search($page_session['location'], $page_session['dept'], $this->id_company);
                $data['plans'] = $this->v_model->get_employee_plans_date2($this->id_company, $page_session['type'], $month, $year);
            }


        }
        else{

            $period = $this->input->post('period');
            $month = $this->input->post('month');
            $type = $this->input->post('type');
            $location = $this->input->post('location');
            $dept = $this->input->post('dept');

            if($period == 0){
                $period = date('Y');
                $m = $month[1];
            }

            if($month == 0){
                $m = $months[1];
                //$selected_date = $m.' '.$period;
            }
            else{
                $mon2 = ltrim($month, 0);
                $m = $months[$mon2];   
            }
            
            $selected_date = $m.' '.$period;
            //add session data
            $current_sess_data = array(
                    'period' => $period,
                    'month' => $month,
                    'type' => $type,
                    'location' => $location,
                    'dept' => $dept
                );
            $this->session->set_userdata('sess', $current_sess_data);

            $data['employees'] = $this->v_model->get_employee_planner_search($location, $dept, $this->id_company);
            $data['plans'] = $this->v_model->get_employee_plans_date2($this->id_company, $type, $month, $period);

        }
        
        $data['select_date'] = $selected_date; 
        $data['vacation_types']= $this->v_model->get('vacation_types', array('id_company'=>$this->id_company));
        //$data['pending_count'] = $this->v_model->get_pending_count($this->id_company, $period);
        $data['locations'] = $this->v_model->get('company_locations', array('id_company'=>$this->id_company));
        $data['departments'] = $this->v_model->get('departments', array('id_company'=>$this->id_company));
        //$data['pending'] = $this->v_model->get_manager_pending_vacation($this->user_auth->get('id_user'));
        $usr = $this->v_model->get('employees', array('id_user'=> $this->session->userdata('id_user')));
        $yr = date('Y');
        if($usr[0]->id_access_level == ACCOUNT_TYPE_ADMINISTRATOR OR $usr[0]->id_access_level == ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR){

            $data['pending_allw'] = $this->v_model->admin_get_pending_allowance($this->id_company, $yr); 
            $data['pending'] = $this->v_model->get_manager_pending_vacation($this->user_auth->get('id_user')); 
            $data['pending_plan'] = $this->v_model->get('vacation_plan', array('status'=>'Pending', 'id_company'=>$this->id_company));
            

        }
        elseif($usr[0]->id_access_level == ACCOUNT_TYPE_UNIT_MANAGER ){
 
            $data['pending_allw'] = $this->v_model->manager_get_pending_allowance($this->id_company, $yr, $this->user_auth->get('id_user'));
            $data['pending'] = $this->v_model->get_manager_pending_vacation($this->user_auth->get('id_user'));
            $data['pending_plan'] = $this->v_model->get_manager_pending_plan($this->user_auth->get('id_user'));

        }
        else{

        }
        
        $buffer = $this->load->view('vacation/vacation_planner', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func vacation_palnner




    /*
     * vacation planner Ajax
     * 
     * @access public 
     * @return void
     */
    public function vacation_planner_ajax(){
        
        $months = array(
                        1=>'January',
                        2=>'February',
                        3=>'March',
                        4=>'April',
                        5=>'May',
                        6=>'June',
                        7=>'July',
                        8=>'August',
                        9=>'September',
                        10=>'October',
                        11=>'November',
                        12=>'December'
                    );
            
        $period = $this->input->post('period');
        $type = $this->input->post('type');
        $location = $this->input->post('location');
        $dept = $this->input->post('dept');
        $month = $this->input->post('month');

        if($period == 0){
            $period = date('Y');
            $m = $months[1];
        }

        if($this->input->post('month')){
            $mon2 = ltrim($month, 0);
            $m = $months[$mon2];
        }
        else{
            $m = $months[1];
        }
      
        $selected_date = $m.' '.$period;
 
        $month = date('F', strtotime("$m $period"));
        $usr = $this->v_model->get('employees', array('id_user'=> $this->session->userdata('id_user')));
        $yr = date('Y');
        if($usr[0]->id_access_level == ACCOUNT_TYPE_ADMINISTRATOR OR $usr[0]->id_access_level == ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR){

            $data['employees'] = $this->v_model->get_employee_planner_search($location, $dept, $this->id_company);
            $data['plans'] = $this->v_model->get_employee_plans_date2($this->id_company, $type, $month, $period);

        }
        elseif($usr[0]->id_access_level == ACCOUNT_TYPE_UNIT_MANAGER ){
 
            $data['employees'] = $this->v_model->get_employee_planner_search($location, $dept, $this->id_company, $this->session->userdata('id_user'));
            $data['plans'] = $this->v_model->get_employee_plans_date2($this->id_company, $type, $month, $period);

        }
        else{
            redirect('vacation/my_vacation', 'refresh');
        }
       

        $data['select_date'] = $selected_date; 
        $data['vacation_types']= $this->v_model->get('vacation_types', array('id_company'=>$this->id_company));
        $data['pending_count'] = $this->v_model->get_pending_count($this->id_company, $period);
        $data['locations'] = $this->v_model->get('company_locations', array('id_company'=>$this->id_company));
        $data['departments'] = $this->v_model->get('departments', array('id_company'=>$this->id_company));
        $buffer = $this->load->view('vacation/vacation_planner_ajax', $data);

    }// End func vacation_palnner






    /*
     * approve vacation plan request
     * 
     * @access public 
     * @return void
     */
    public function approve_vacation_plan(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_VIEW_MY);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }


        if($this->input->server('REQUEST_METHOD') == 'POST'){
            $id_plan = $this->input->post('id_plan');
            $id_user = $this->input->post('id_user');
            $id_plan_request = $this->input->post('id_plan_request');

            //get the plan to be approved
            $plan = $this->v_model->get('vacation_plan_approver', array('id_user'=>$id_user, 'id_vacation_plan'=>$id_plan));
            $new_stage = $plan[0]->stage + 1;
            
            //update plan
            $update = $this->v_model->update('vacation_plan_approver', array('status'=>1), array('id_user'=>$id_user, 'id_plan_approver'=>$plan[0]->id_plan_approver));
            $stage = $this->v_model->get('vacation_request_stages', array('stage'=>$new_stage));

            //update task
            $t = $this->v_model->get('tasks', array('tb_name'=>'vacation_plan_approver', 'tb_column_name'=>'id_plan_approver', 'tb_id_record'=>$id_plan));
            $task = array(
                    'id_task' => $t[0]->id_task,
                    'status' => TASK_STATUS_COMPLETE
                );

            $this->tasks->update_task($task);

            if(!empty($stage)){
            //data for insert
                if($stage[0]->approver == 'Unit Manager'){
                    $e = $this->v_model->get('employees', array('id_user'=>$id_user));
                    $id_approver = $e[0]->id_manager;
                }
                elseif($stage[0]->approver == 'Administrator'){
                    $e = $this->v_model->get('employees', array('id_access_level'=>ACCOUNT_TYPE_ADMINISTRATOR));
                    $id_approver = $e[0]->id_user;
                }
                else{
                    $id_approver = $stage[0]->id_user;
                }

                $approver_list = array(
                            'id_vacation_plan'=> $id_plan,
                            //'id_request_stage' => $stage[0]->id_plan_stage,
                            'id_approver' => $id_approver,
                            'id_user' => $id_user,
                            'stage' => $new_stage,
                            'id_company' => $this->id_company,
                            'status' => 0
                        );
                $q = $this->v_model->add('vacation_plan_approver', $approver_list);

                //add task
                $link = anchor('vacation/vacation_planner', 'Vacation Plan Request is awaiting your approval or decline.');
                $task[] = array(
                    'id_user_from' => $id_user,
                    'id_user_to' => $id_approver,
                    'id_module' => 2,
                    'subject' => $link,
                    'status' => 2,
                    'tb_name' => 'vacation_plan_approver',
                    'tb_column_name' => 'id_plan_approver',
                    'tb_id_record' => $id_plan
                );
                $this->tasks->push_task($task);
            }
            else{
                //send email to user informing him of approval
                //update vacation plan
                $pln = $this->v_model->get('vacation_plan', array('id_user'=>$id_user, 'id_vacation_plan'=>$id_plan));
                $update = $this->v_model->update('vacation_plan', array('status'=>'Approved'), array('id_user'=>$id_user, 'id_vacation_plan'=>$id_plan));
                $employee = $this->v_model->get('users', array('id_user'=>$id_user));
                $k = array(
                        'start_date' => $pln[0]->start_date,
                        'end_date' => $pln[0]->end_date,
                        'employee_name' => ucwords($employee[0]->first_name." ".$employee[0]->last_name),
                        'company_name' => $this->company_name,
                        'subdomain' => $this->user_auth->get('company_id_string')
                    );
                $subject = "Vacation Plan Approved";
                $msg = $this->load->view('email_templates/vacation_plan_approved', $k, true);

                $this->load->library('mailgun_api');
                $this->mailgun_api->send_mail(
                                        array(
                                            'from' => $this->company_name.' <noreply@testbase.com.ng>',
                                            'to' => $employee[0]->email,
                                            'subject' => $subject,
                                            'html' => '<html>'. $msg .'</html>'
                                        ));
            }


            if($update){
                //update task
                $t = $this->v_model->get('tasks', array('tb_name'=>'vacation_plan_approver', 'tb_column_name'=>'id_plan_approver', 'tb_id_record'=>$id_plan));
                $task = array(
                        'id_task' => $t[0]->id_task,
                        'status' => TASK_STATUS_COMPLETE
                    );

                $this->tasks->update_task($task);

                echo 1;
            }
            else{
                echo 0;
            }


        }
        

    }// End func approve_vacation_plan



    /*
     * decline vacation plan request
     * 
     * @access public 
     * @return void
     */
    public function decline_vacation_plan(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_VIEW_MY);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        // if(!$this->input->is_ajax_request()){

        //     //return;
        // }

        if($this->input->server('REQUEST_METHOD') == 'POST'){
            $id_plan = $this->input->post('id_plan');
            $id_user = $this->input->post('id_user');
            $id_plan_request = $this->input->post('id_plan_request');

            //get the plan to be approved
            $plan = $this->v_model->get('vacation_plan_approver', array('id_user'=>$id_user, 'id_vacation_plan'=>$id_plan));

            //update plan
            $this->v_model->update('vacation_plan_approver', array('status'=>2), array('id_user'=>$id_user, 'id_plan_approver'=>$plan[0]->id_plan_approver));
            //$stage = $this->v_model->get('vacation_request_stages', array('stage'=>$new_stage));

            //send email to user informing him of approval
            //update vacation plan
            //update task
            $t = $this->v_model->get('tasks', array('tb_name'=>'vacation_plan_approver', 'tb_column_name'=>'id_plan_approver', 'tb_id_record'=>$id_plan));
            $task = array(
                    'id_task' => $t[0]->id_task,
                    'status' => TASK_STATUS_COMPLETE
                ); 
            $this->tasks->update_task($task);

            $pln = $this->v_model->get('vacation_plan', array('id_user'=>$id_user, 'id_vacation_plan'=>$id_plan));
            $this->v_model->update('vacation_plan', array('status'=>'Declined'), array('id_user'=>$id_user, 'id_vacation_plan'=>$id_plan));
            $employee = $this->v_model->get('users', array('id_user'=>$id_user));
            $k = array(
                    'start_date' => $pln[0]->start_date,
                    'end_date' => $pln[0]->end_date,
                    'employee_name' => ucwords($employee[0]->first_name." ".$employee[0]->last_name),
                    'company_name' => $this->company_name,
                    'subdomain' => $this->user_auth->get('company_id_string')
                );
            $subject = "Vacation Plan Declined";
            $msg = $this->load->view('email_templates/vacation_plan_declined', $k, true);

            $this->load->library('mailgun_api');
            $this->mailgun_api->send_mail(
                                    array(
                                        'from' => $this->company_name.' <noreply@testbase.com.ng>',
                                        'to' => $employee[0]->email,
                                        'subject' => $subject,
                                        'html' => '<html>'. $msg .'</html>'
                                    ));
            


        }
        

    }// End func decline_vacation_plan



    /*
     * bulk approve vacation plan request
     * 
     * @access public 
     * @return void
     */
    public function bulk_approve_vacation_plan(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_VIEW_MY);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }


        if($this->input->server('REQUEST_METHOD') == 'POST'){

            $id_users = $this->input->post('id_user');
            $id_plans = $this->input->post('id_plan');

            if($id_users){
                
                foreach ($id_plans as $plan) {
                    $d = explode('_', $plan);
                    $id_plan = $d[0];
                    $id_user = $d[1];
                    //$id_plan_request = $id_plan_requests[$key];

                    $vplan = $this->v_model->get('vacation_plan', array('id_vacation_plan'=>$id_plan));
                    if(!empty($vplan)){
                        if($vplan[0]->status == 'Pending'){
                            //if(!empty($id_plans[$key]) AND !empty($id_plan_requests[$key])){            
                                
                                //get the plan to be approved
                                $plan = $this->v_model->get('vacation_plan_approver', array('id_user'=>$id_user, 'id_vacation_plan'=>$id_plan));
                                $new_stage = $plan[0]->stage + 1;

                                //update plan
                                $this->v_model->update('vacation_plan_approver', array('status'=>1), array('id_user'=>$id_user, 'id_plan_approver'=>$plan[0]->id_plan_approver));
                                $stage = $this->v_model->get('vacation_request_stages', array('stage'=>$new_stage));

                                //update task
                                $t = $this->v_model->get('tasks', array('tb_name'=>'vacation_plan_approver', 'tb_column_name'=>'id_plan_approver', 'tb_id_record'=>$id_plan));
                                $task = array(
                                        'id_task' => $t[0]->id_task,
                                        'status' => TASK_STATUS_COMPLETE
                                    );

                                $this->tasks->update_task($task);

                                if(!empty($stage)){
                                //data for insert
                                    if($stage[0]->approver == 'Unit Manager'){
                                        $e = $this->v_model->get('employees', array('id_user'=>$id_user));
                                        $id_approver = $e[0]->id_manager;
                                    }
                                    elseif($stage[0]->approver == 'Administrator'){
                                        $e = $this->v_model->get('employees', array('account_type'=>ACCOUNT_TYPE_ADMINISTRATOR));
                                        $id_approver = $e[0]->id_user;
                                    }
                                    else{
                                        $id_approver = $stage[0]->id_user;
                                    }

                                    $approver_list = array(
                                                'id_vacation_plan'=> $id_plan,
                                                'id_plan_stage' => $stage[0]->id_plan_stage,
                                                'id_approver' => $id_approver,
                                                'id_user' => $this->id_user_logged_in,
                                                'stage' => $new_stage,
                                                'id_company' => $this->id_company,
                                                'status' => 0
                                            );
                                    $q = $this->v_model->add('vacation_plan_approver', $approver_list);

                                }
                                else{
                                    //send email to user informing him of approval
                                    //update vacation plan
                                    $pln = $this->v_model->get('vacation_plan', array('id_user'=>$id_user, 'id_vacation_plan'=>$id_plan));
                                    $this->v_model->update('vacation_plan', array('status'=>'Approved'), array('id_user'=>$id_user, 'id_vacation_plan'=>$id_plan));
                                    $employee = $this->v_model->get('users', array('id_user'=>$id_user));
                                    $k = array(
                                            'start_date' => $pln[0]->start_date,
                                            'end_date' => $pln[0]->end_date,
                                            'employee_name' => ucwords($employee[0]->first_name." ".$employee[0]->last_name),
                                            'company_name' => $this->company_name,
                                            'subdomain' => $this->user_auth->get('company_id_string')
                                        );
                                    $subject = "Vacation Plan Approved";
                                    $msg = $this->load->view('email_templates/vacation_plan_approved', $k, true);

                                    $this->load->library('mailgun_api');
                                    $this->mailgun_api->send_mail(
                                                            array(
                                                                'from' => $this->company_name.' <noreply@testbase.com.ng>',
                                                                'to' => $employee[0]->email,
                                                                'subject' => $subject,
                                                                'html' => '<html>'. $msg .'</html>'
                                                            ));
                                }

                            //}
                        }
                    }
                }
            }
        }
         

    }// End func bulk_approve_vacation_plan



 /*
     * bulk decline of vacation plan request
     * 
     * @access public 
     * @return void
     */
    public function bulk_decline_vacation_plan(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_VIEW_MY);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        if($this->input->server('REQUEST_METHOD') == 'POST'){
            $id_users = $this->input->post('id_user');
            $id_plans = $this->input->post('id_plan');

            if($id_users){
                
                foreach ($id_plans as $plan) {
                    $d = explode('_', $plan);
                    $id_plan = $d[0];
                    $id_user = $d[1];
                    //$id_plan = $plan;

                    $vplan = $this->v_model->get('vacation_plan', array('id_vacation_plan'=>$id_plan));
                    if(!empty($vplan)){
                        if($vplan[0]->status == 'Pending'){
                            
                                $this->v_model->update('vacation_plan_approver', array('status'=>2), array('id_vacation_plan'=>$id_plan));

                                //send email to user informing him of approval
                                //update vacation plan
                                //update task
                                $t = $this->v_model->get('tasks', array('tb_name'=>'vacation_plan_approver', 'tb_column_name'=>'id_plan_approver', 'tb_id_record'=>$id_plan));
                                $task = array(
                                        'id_task' => $t[0]->id_task,
                                        'status' => TASK_STATUS_COMPLETE
                                    );

                                $this->tasks->update_task($task);


                                $pln = $this->v_model->get('vacation_plan', array('id_vacation_plan'=>$id_plan));
                                $this->v_model->update('vacation_plan', array('status'=>'Declined'), array('id_vacation_plan'=>$id_plan));
                                $employee = $this->v_model->get('users', array('id_user'=>$pln[0]->id_user));
                                $k = array(
                                        'start_date' => $pln[0]->start_date,
                                        'end_date' => $pln[0]->end_date,
                                        'employee_name' => ucwords($employee[0]->first_name." ".$employee[0]->last_name),
                                        'company_name' => $this->company_name,
                                        'subdomain' => $this->user_auth->get('company_id_string')
                                    );
                                $subject = "Vacation Plan Declined";
                                $msg = $this->load->view('email_templates/vacation_plan_declined', $k, true);

                                $this->load->library('mailgun_api');
                                $this->mailgun_api->send_mail(
                                                        array(
                                                            'from' => $this->company_name.' <noreply@testbase.com.ng>',
                                                            'to' => $employee[0]->email,
                                                            'subject' => $subject,
                                                            'html' => '<html>'. $msg .'</html>'
                                                        ));
                              
                        }
                    }
                }
            }
        }
        
       

    }// End func bulk_approve_vacation_plan




    /*
     * employee vacation planner
     * 
     * @access public 
     * @return void
     */
    public function my_vacation_planner(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_VIEW_MY);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        if($this->input->post('request_button')){

            $id_type = $this->input->post('type', true);
            $date_start = $this->input->post('start_date', true);
            $date_end = $this->input->post('end_date', true);
            $id_request = $this->input->post('id_request');

            $this->load->library('form_validation');
            $this->form_validation->set_rules('type', '', 'trim|required');
            $this->form_validation->set_rules('start_date', '', 'trim|required');
            $this->form_validation->set_rules('end_date', '', 'trim|required');
        
            if($this->form_validation->run()==FALSE){
                $errors = "Please fill the form correctly.";
                $this->session->set_flashdata('msg', $errors);
                $this->session->set_flashdata('type', 'error');
                redirect('vacation/my_vacation_planner', 'refresh');
                return;
            }

            $sdate = date('Y-m-d', strtotime($date_start));
            $endate = date('Y-m-d', strtotime($date_end));
            $smonth_year = date('m_Y', strtotime($date_start));
            $emonth_year = date('m_Y', strtotime($date_end));
            $date_array = array('start'=> $date_start, 'end'=> $date_end);

            #check dates for irregularity
            if($endate < $sdate){
                $errors = "Error ! Start date cannot come before end date.";
                $this->session->set_flashdata('msg', $errors);
                $this->session->set_flashdata('type', 'error');
                redirect('vacation/my_vacation_planner', 'refresh');
                return;
            }


            $ct = $this->v_model->get_date_conflicts($this->id_company, $date_array, $id_request, $id_type, $this->id_user_logged_in);
            $req = $this->v_model->get('vacation_plan_request', array('id_plan_request'=>$id_request));
            if(!$ct){
                $holi = $this->v_model->get_holidays($this->id_company);
                $vacation_data = array(                                    
                                        'id_company' => $this->user_auth->get('id_company'),
                                        'id_plan_request' => $id_request,
                                        'id_user' => $this->id_user_logged_in,
                                        'start_date' => date('Y-m-d', strtotime($date_start)),
                                        'end_date' => date('Y-m-d', strtotime($date_end)),
                                        'month_year' => $smonth_year,
                                        'no_of_days' => get_working_days($date_start, $date_end, $holi),
                                        'id_type' => $id_type,
                                        'year' => $req[0]->period,
                                        'status' => 'draft'
                                    );

                $q = $this->v_model->add('vacation_plan', $vacation_data, true);


                if($q){
                    //add request approver
                    $stage = $this->v_model->get('vacation_request_stages', array('stage'=>1));//get current stage
                    //data for insert
                    $approver_list = array(
                                'id_vacation_plan'=> $q,
                                'id_request_stage' => $stage[0]->id_request_stage,
                                'id_approver' => $stage[0]->id_user,
                                'id_user' => $this->id_user_logged_in,
                                'stage' => 1,
                                'id_company' => $this->id_company,
                                'status' => 0
                            );
                    $q = $this->v_model->add('vacation_plan_approver', $approver_list, true);

                    $errors = "Vacation Plan Added successfully.";
                    $this->session->set_flashdata('msg', $errors);
                    $this->session->set_flashdata('type', 'success');
                }
                else{
                    $errors = "There was a problem processing your request.";
                    $this->session->set_flashdata('msg', $errors);
                    $this->session->set_flashdata('type', 'error');
                }

                $rq = $this->v_model->get('vacation_plan_request', array('id_plan_request'=>$id_request));
                $this->session->set_userdata('period', $rq[0]->period);
                $this->session->set_userdata('vac_type', $id_type);

                redirect('vacation/my_vacation_planner', 'refresh');
            }
            else{
                $rq = $this->v_model->get('vacation_plan_request', array('id_plan_request'=>$id_request));
                $this->session->set_userdata('period', $rq[0]->period);
                $this->session->set_userdata('vac_type', $id_type);

                $errors = "There is a conflict with date selected and previously planned date(s).";
                $this->session->set_flashdata('msg', $errors);
                $this->session->set_flashdata('type', 'error');
                redirect('vacation/my_vacation_planner', 'refresh');
                return;
            }
        }
        elseif($this->input->post('get_plans')){
            $year = $this->input->post('period');
            if($year == 0){
                $year = date('Y');
            }
            $vc = $this->input->post('types');
            
            $this->session->set_userdata('period', $year);
            $this->session->set_userdata('vac_type', $vc);

            $stat = true;
            //$days_allot = $vc[1];
            $employee_request = $this->v_model->get_employee_plans($this->id_user_logged_in, $this->id_company, $vc);
            $plan_request = $this->v_model->get_requested_plans($this->id_company, $vc, $year);
            $planned_sum = $this->v_model->get_planned_sum($this->id_company, $this->id_user_logged_in, $vc);
            $allocated_sum = $this->v_model->get('vacation_allocated', array('id_company'=>$this->id_company, 'id_type'=>$vc, 'id_user'=>$this->id_user_logged_in));
        }
        else{
            $vc = NULL;
            if($this->session->userdata('vac_type')){
                $vc = $this->session->userdata('vac_type');
                $year = $this->session->userdata('period');

                $plan_request = $this->v_model->get_requested_plans($this->id_company, $vc, $year);
                $planned_sum = $this->v_model->get_planned_sum($this->id_company, $this->id_user_logged_in, $vc);
                $allocated_sum = $this->v_model->get('vacation_allocated', array('id_company'=>$this->id_company, 'id_type'=>$vc, 'id_user'=>$this->id_user_logged_in));
            }
            else{
                $plan_request = '';
                $planned_sum = '';
                $allocated_sum = '';
                $year = date('Y');
            }
            $stat = false;
            $employee_request = $this->v_model->get_employee_plans($this->id_user_logged_in, $this->id_company, $vc);
            
        }
        
        $period = date('Y');
        $data['stat'] = $stat;
        $data['plan_request'] = $plan_request;
        $data['employee_request'] = $employee_request;
        $data['active_type'] = $this->v_model->get('vacation_plan_request', array('id_company'=>$this->id_company));
        $data['vacation_types']= $this->v_model->get_allocated($this->id_company, $this->id_user_logged_in);
        $data['vacation_allocated'] = $this->v_model->get_planned($this->id_company, $this->id_user_logged_in, $year);
        $data['allocated_sum'] = $allocated_sum;
        $data['planned_sum'] = $planned_sum;
        $data['leave'] = $this->v_model->get_employee_leave_allowance($this->id_user_logged_in, $this->id_company, $year);
        $data['vacation_plan'] = $this->v_model->get('vacation_plan_request', array('id_company'=>$this->id_company, 'status'=>1));

        $buffer = $this->load->view('vacation/my_vacation_planner', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func my_vacation_palnner



    /*
     * process vacation plan request for approval
     * 
     * @access public 
     * @return void
     */

    public function process_plan_request()    {
        
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_VIEW_MY);

        if($this->input->server('REQUEST_METHOD') == 'POST'){

            $id_type = $this->input->post('type', true);
            $date_start = $this->input->post('start_date', true);
            $date_end = $this->input->post('end_date', true);
            $id_request = $this->input->post('id_request');

            $this->load->library('form_validation');
            $this->form_validation->set_rules('type', '', 'trim|required');
            $this->form_validation->set_rules('start_date', '', 'trim|required');
            $this->form_validation->set_rules('end_date', '', 'trim|required');
        
            if($this->form_validation->run()==FALSE){
                echo "1";
                return;
            }

            $req = $this->v_model->get('vacation_plan_request', array('id_plan_request'=>$id_request));

            $sdate = date('Y-m-d', strtotime($date_start));
            $endate = date('Y-m-d', strtotime($date_end));
            $smonth_year = date('m_Y', strtotime($date_start));
            $emonth_year = date('m_Y', strtotime($date_end));
            $date_array = array('start'=> $date_start, 'end'=> $date_end);

            $syear = date('Y', strtotime($date_start));
            $eyear = date('Y', strtotime($date_end));

            if($syear != $req[0]->period OR $eyear != $req[0]->period){
                echo "6";
                return;
            }

            #check dates for irregularity
            if($endate < $sdate){
                echo "2";
                return;
            }

            $holi = $this->v_model->get_holidays($this->id_company);
            $allocated = $this->v_model->get_allocated_sum2($this->id_company, $this->id_user_logged_in, $id_type);
            $planned = $this->v_model->num_planned_days($id_request, $id_type, $this->id_user_logged_in, $this->id_company);
            $add_days = get_working_days($date_start, $date_end, $holi);
            $planned_sum = $planned[0]->no_of_days + $add_days;

            if($allocated[0]->no_of_days < $planned_sum){
                echo "5";
                return;
            }

            if($this->v_model->get_date_conflicts2($this->id_company, $date_start, $date_end)){

                echo 7;
                return;
            }


            $ct = $this->v_model->get_date_conflicts($this->id_company, $date_array, $id_request, $id_type, $this->id_user_logged_in);
            
            if(!$ct){
                
                $holi = $this->v_model->get_holidays($this->id_company);
                $vacation_data = array(                                    
                                        'id_company' => $this->user_auth->get('id_company'),
                                        'id_plan_request' => $id_request,
                                        'id_user' => $this->id_user_logged_in,
                                        'start_date' => date('Y-m-d', strtotime($date_start)),
                                        'end_date' => date('Y-m-d', strtotime($date_end)),
                                        'month_year' => $smonth_year,
                                        'no_of_days' => get_working_days($date_start, $date_end, $holi),
                                        'id_type' => $id_type,
                                        'year' => $req[0]->period,
                                        'status' => 'draft'
                                    );

                $q = $this->v_model->add('vacation_plan', $vacation_data, true);


                if($q){
                    //add request approver
                    $stage = $this->v_model->get('vacation_request_stages', array('stage'=>1));
                    if($stage[0]->approver == 'Unit Manager'){
                        $e = $this->v_model->get('employees', array('id_user'=>$this->id_user_logged_in));
                        $id_approver = $e[0]->id_manager;
                    }
                    elseif($stage[0]->approver == 'Administrator'){
                        $e = $this->v_model->get('employees', array('id_access_level'=>ACCOUNT_TYPE_ADMINISTRATOR, 'id_company'=>$this->id_company));
                        $id_approver = $e[0]->id_user;
                    }
                    else{
                        $id_approver = $stage[0]->id_user;
                    }
                    //data for insert
                    $approver_list = array(
                                'id_vacation_plan'=> $q,
                                'id_request_stage' => $stage[0]->id_request_stage,
                                'id_approver' => $id_approver,
                                'id_user' => $this->id_user_logged_in,
                                'stage' => 1,
                                'id_company' => $this->id_company,
                                'status' => 0
                            );
                    $q = $this->v_model->add('vacation_plan_approver', $approver_list, true);

                    $errors = "Vacation Plan Added successfully.";
                    $this->session->set_flashdata('msg', $errors);
                    $this->session->set_flashdata('type', 'success');

                    $rq = $this->v_model->get('vacation_plan_request', array('id_plan_request'=>$id_request));
                    $this->session->set_userdata('period', $rq[0]->period);
                    $this->session->set_userdata('vac_type', $id_type);

                    echo '3';
                }
                else{

                    echo '8';
                    return;
                }

            }
            else{
                echo '4';
                return;
            }
        }
    }// End func process_plan_request



     /*
     * update vacation plan request
     * 
     * @access public 
     * @return void
     */

    public function update_plan_request()
    {
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_VIEW_MY);

        if($this->input->server('REQUEST_METHOD') == 'POST'){

            $id_type = $this->input->post('edit_id_type', true);
            $date_start = $this->input->post('edit_start_date', true);
            $date_end = $this->input->post('edit_end_date', true);
            $id_vacation = $this->input->post('id_vacation');

            $this->load->library('form_validation');
            $this->form_validation->set_rules('id_vacation', '', 'trim|required');
            $this->form_validation->set_rules('edit_start_date', '', 'trim|required');
            $this->form_validation->set_rules('edit_end_date', '', 'trim|required');
        
            if($this->form_validation->run()==FALSE){
                echo "1";
                return;
            }

            $vc = $this->v_model->get('vacation_plan', array('id_vacation_plan'=>$id_vacation));
            $sdate = date('Y-m-d', strtotime($date_start));
            $endate = date('Y-m-d', strtotime($date_end));
            $smonth_year = date('m_Y', strtotime($date_start));
            $emonth_year = date('m_Y', strtotime($date_end));
            $date_array = array('start'=> $date_start, 'end'=> $date_end);
            $id_request = $vc[0]->id_plan_request;
            $num_days = $vc[0]->no_of_days;
            $holi = $this->v_model->get_holidays($this->id_company);

            $syear = date('Y', strtotime($date_start));
            $eyear = date('Y', strtotime($date_end));
            $req = $this->v_model->get('vacation_plan_request', array('id_plan_request'=>$id_request));
            if($syear != $req[0]->period OR $eyear != $req[0]->period){
                echo "6";
                return;
            }

            #check dates for irregularity
            if($endate < $sdate){
                echo "2";
                return;
            }

            $allocated = $this->v_model->get_allocated_sum2($this->id_company, $this->id_user_logged_in, $id_type);
            $planned = $this->v_model->num_planned_days($id_request, $id_type, $this->id_user_logged_in, $this->id_company);
            $add_days = get_working_days($date_start, $date_end, $holi);
            $planned_sum = ($planned[0]->no_of_days - $num_days) + $add_days;

            if($allocated[0]->no_of_days < $planned_sum){
                echo "5";
                return;
            }

            $vacation_data1 = array(                                    
                                'start_date' => '',
                                'end_date' => ''
                            );

            $this->v_model->update('vacation_plan', $vacation_data1, array('id_vacation_plan'=>$id_vacation));

            $ct = $this->v_model->get_date_conflicts($this->id_company, $date_array, $id_request, $id_type, $this->id_user_logged_in);
            
            if(!$ct){
                
                $holi = $this->v_model->get_holidays($this->id_company);

                $vacation_data = array(                                    
                                'start_date' => date('Y-m-d', strtotime($date_start)),
                                'end_date' => date('Y-m-d', strtotime($date_end)),
                                'month_year' => $smonth_year,
                                'no_of_days' => get_working_days($date_start, $date_end, $holi)
                            );

                $q = $this->v_model->update('vacation_plan', $vacation_data, array('id_vacation_plan'=>$id_vacation));


                if($q){

                    $errors = "Vacation Plan Added successfully.";
                    $this->session->set_flashdata('msg', $errors);
                    $this->session->set_flashdata('type', 'success');

                    $rq = $this->v_model->get('vacation_plan_request', array('id_plan_request'=>$id_request));
                    $this->session->set_userdata('period', $rq[0]->period);
                    $this->session->set_userdata('vac_type', $id_type);

                    echo '3';
                }
                else{

                    echo '4';
                    return;
                }

            }
            else{
                echo '4';
                return;
            }
        }
    }// End func update_plan_request





    /*
     * Method Description goes here
     *
     * @access    public
     * @param     array
     * @return    void
     */

    public function send_for_approval()
    {

        // Check user login session access and permission
        $this->user_auth->check_login();        

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Error! Please enter valid dates!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/my_vacation_planner', 'refresh');
            return;
        }

        $id_type = $this->input->post('id_type2');
        $id_request = $this->input->post('id_request2');

        $qr = $this->v_model->get('vacation_plan', array('id_plan_request'=>$id_request, 'id_type'=>$id_type, 'id_user'=>$this->id_user_logged_in, 'status'=>'draft'));

        if(!$qr){
            $error_msg = 'Error! There was a problem processing your request!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/my_vacation_planner', 'refresh');
            return;
        }
        else{

            //check number of days
            $sum = $this->v_model->num_planned_days($id_request, $id_type, $this->id_user_logged_in, $this->id_company);
            $allocated = $this->v_model->get('vacation_allocated', array('id_company'=>$this->id_company, 'id_type'=>$id_type, 'id_user'=>$this->id_user_logged_in));
           //print_r($sum[0]->no_of_days); die();
            if($sum[0]->no_of_days <= $allocated[0]->no_of_days){

                $q = $this->v_model->update('vacation_plan', array('status'=>'Pending'), array('id_plan_request'=>$id_request, 'id_type'=>$id_type, 'id_user'=>$this->id_user_logged_in, 'status'=>'draft'));
                if($q){
                    //add task
                    $link = anchor('vacation/vacation_planner', 'Vacation Plan Request is awaiting your approval or decline.');
                    foreach ($qr as $vr) {
                        $appr = $this->v_model->get('vacation_plan_approver', array('id_vacation_plan'=>$vr->id_vacation_plan, 'stage'=>1));
                        $task[] = array(
                            'id_user_from' => $this->id_user_logged_in,
                            'id_user_to' => $appr[0]->id_approver,
                            'id_module' => 2,
                            'subject' => $link,
                            'status' => 2,
                            'tb_name' => 'vacation_plan_approver',
                            'tb_column_name' => 'id_plan_approver',
                            'tb_id_record' => $vr->id_vacation_plan
                        );
                        $this->tasks->push_task($task);
                    }
                    
                    $error_msg = 'Success! Your request has been sent.';
                    $this->session->set_flashdata('type', 'success');
                    $this->session->set_flashdata('msg', $error_msg);
                }
                else{
                    $error_msg = 'Error! There was a problem processing your request!';
                    $this->session->set_flashdata('type', 'error');
                    $this->session->set_flashdata('msg', $error_msg);
                }
            }
            else{

                $error_msg = 'Error! You are requesting more days than allocated to you. Please try again and do not select more that the number of days alloacted to you!';
                $this->session->set_flashdata('type', 'error');
                $this->session->set_flashdata('msg', $error_msg);
            }
            $rq = $this->v_model->get('vacation_plan_request', array('id_plan_request'=>$id_request));
            $this->session->set_userdata('period', $rq[0]->period);
            $this->session->set_userdata('vac_type', $id_type);

            redirect('vacation/my_vacation_planner', 'refresh');
        }

    }


    /*
     * Add new vacation plan request
     *
     * @access    public
     * @param     array
     * @return    void
     */

    public function add_plan_request()
    {
        // Check user login session access and permission
        $this->user_auth->check_login(); 
        $this->user_auth->check_perm(VACATION_TYPES);       

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Error! Please enter valid dates!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/vacation_planner_setting', 'refresh');
            return;
        }

        $planner = array(
                'id_company' => $this->id_company,
                'period' => $this->input->post('period'),
                'due_date'=> date('Y-m-d', strtotime($this->input->post('date_due'))),
                'id_type' => $this->input->post('id_vactype'),
                'status' => 1
            );

        $q = $this->v_model->add('vacation_plan_request', $planner); 

        if($q){
            $error_msg = 'Vacation Plan Request created';
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('msg', $error_msg);

            if($this->input->post('send_email')){

                $employees = $this->v_model->get('users', array('id_company'=>$this->id_company, 'status'=>USER_STATUS_ACTIVE OR USER_STATUS_PROBATION_ACCESS));
                $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
                $domainName = $_SERVER['HTTP_HOST'].'/';


                foreach ($employees as $em) {
                    $subject = "Vacation Plan Requested";
                    $k = array(
                        'first_name' => $em->first_name,
                        'last_name' => $em->last_name,
                        'company_name' => $this->company_name,
                        'subdomain' => $this->user_auth->get('company_id_string'),
                        'domain' => $protocol.$domainName
                    );

                    $msg = $this->load->view('email_templates/vacation_plan_request', $k, true);
                    $this->load->library('mailgun_api');
                    $this->mailgun_api->send_mail(
                                            array(
                                                'from' => $this->session->userdata('display_name')." ".$this->session->userdata('last_name')." <".$this->session->userdata('email').">",
                                                'to' => $em->email,
                                                'subject' => $subject,
                                                'html' => '<html>'. $msg .'</html>'
                                            ));
                }
                
            }

        }
        else{
            $error_msg = 'Error! There was a problem processing your request.';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        }

        redirect('vacation/vacation_planner_setting', 'refresh');
    }//add_plan_request
    
    



    /*
     * edit vacation plan request
     *
     * @access    public
     * @param     array
     * @return    void
     */

    public function edit_plan_request()
    {
        // Check user login session access and permission
        $this->user_auth->check_login();     
        $this->user_auth->check_perm(VACATION_TYPES);    

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Error! Please enter valid dates!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/vacation_planner_setting', 'refresh');
            return;
        }

        $id_request = $this->input->post('id_request2');
        $id_type = $this->input->post('id_vactype2');
        $due_date = $this->input->post('date_due2');

        //check if id_request is valid
        //build array
        //update request

        $q = $this->v_model->get('vacation_plan_request', array('id_plan_request'=>$id_request, 'id_company'=>$this->id_company, 'id_type'=>$id_type));

        $planner = array(
                'due_date'=> date('Y-m-d', strtotime($due_date)),
            );

        $q = $this->v_model->update('vacation_plan_request', $planner, array('id_plan_request'=>$id_request, 'id_company'=>$this->id_company, 'id_type'=>$id_type)); 

        if($q){
            $error_msg = 'Vacation Plan Request updated';
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('msg', $error_msg);
        }
        else{
            $error_msg = 'Error! There was a problem processing your request.';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        }

        redirect('vacation/vacation_planner_setting', 'refresh');
    }//End func edit_plan_request




    /*
     * Deactivate vacation plan request for employees
     *
     * @access    public
     * @param    array
     * @return    void
     */

    public function deactivate_plan_request()
    {
        // Check user login session access and permission
        $this->user_auth->check_login();      
        $this->user_auth->check_perm(VACATION_TYPES);   

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Error! Invalid parameters passed!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/vacation_planner_setting', 'refresh');
            return;
        }


        $id_request = $this->input->post('id_request');
        $id_type = $this->input->post('id_vactype');
    
        $request = $this->v_model->get('vacation_plan_request', array('id_plan_request'=>$id_request));

        if(empty($request)){
            $error_msg = 'Error! Invalid parameters provided!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/vacation_planner_setting', 'refresh');
            return;
        }

        $q = $this->v_model->delete('vacation_plan_request', array('id_company'=>$this->id_company, 'id_plan_request'=>$id_request));

        if ($q) {
            $error_msg = 'Success! Vacation Plan removed';
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('msg', $error_msg);

        } else {
            $error_msg = 'Error! There was a problem processing your request!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
           
        }
        
        redirect('vacation/vacation_planner_setting', 'refresh');
        
    }//end func deactivate_plan_request


    /*
     * Deactivate vacation plan request for employees
     *
     * @access    public
     * @param    array
     * @return    void
     */

    public function cancel_vacation_plan_request()
    {

        $date = date('Y-m-d 00:00:00');
        $this->v_model->delete('vacation_plan_request', array('due_date <'=> $date));
        echo "it runs";
        
    }//end func deactivate_plan_request
    
    


    /*
     * Delete vacation plan request
     *
     * @access    public
     * @param    array
     * @return    void
     */

    public function delete_plan_request(){

        // Check user login session access and permission
        $this->user_auth->check_login();        

        if($this->input->server('REQUEST_METHOD') != 'POST'){
            $error_msg = 'Error! Invalid parameters passed!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/my_vacation_planner', 'refresh');
            return;
        }


        $id_request = $this->input->post('id_vacation');
    
        $request = $this->v_model->get('vacation_plan', array('id_company'=>$this->id_company, 'id_vacation_plan'=>$id_request));

        if(empty($request)){
            $error_msg = 'Error! Invalid parameters provided!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('vacation/my_vacation_planner', 'refresh');
            return;
        }
        $rq = $this->v_model->get('vacation_plan_request', array('id_plan_request'=>$request[0]->id_plan_request));
        $id_type = $request[0]->id_type;
        $period = $rq[0]->period;
        $q = $this->v_model->delete('vacation_plan', array('id_company'=>$this->id_company, 'id_vacation_plan'=>$id_request));

        if ($q) {
            $error_msg = 'Success! Vacation Plan removed';
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('msg', $error_msg);

        } else {
            $error_msg = 'Error! There was a problem processing your request!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
           
        }

        
        $this->session->set_userdata('period', $period);
        $this->session->set_userdata('vac_type', $id_type);
    
        redirect('vacation/my_vacation_planner', 'refresh');
        
    }// end func delete_plan_request





    /*
    *   Vacation setup step one
    *
    * 
    */
    public function setup_step_1(){
        
        // Check user login session access and permission
        $this->user_auth->check_login();

        if($this->session->userdata('account_type') != ACCOUNT_TYPE_ADMINISTRATOR AND $this->session->userdata('account_type') != ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR){
            $error_msg = 'Vacation cancelled successfully!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('employee/dashboard', 'refresh');
        }

        if($this->_check_setup()){
            redirect('vacation', 'refresh');          
        }

        if($this->input->server('REQUEST_METHOD') == "POST"){
            //print_r($this->input->post()); die();
            $array_count = 0;
            foreach ($this->input->post() as $key => $value) {

                if(is_array($value)){
                    
                    $data = array(
                            'name' => $value[0],
                            'id_company' => $this->id_company,
                            'carry_allowed' => $value[2],
                            'description' => $value[1]
                        );
                    $array_count ++;
                    $this->v_model->add('vacation_types', $data);

                }
                
            }

            # set this stage to complete
            $setupdata = array(
                    'id_module'=>2,
                    'id_company'=>$this->id_company,
                    'step'=>1,
                    'status'=>MODULE_SETUP_PENDING,
                    'created_by'=>  $this->id_user_logged_in,
                    'date_created'=>date('Y-m-d', strtotime('now'))
                );
            
            $this->v_model->add('module_setup', $setupdata);

            redirect('vacation/setup_step_2', 'refresh');
            return;
            
        }

        $step = $this->v_model->get_setup_step($this->id_company, 2);
        if(!empty($step)){
            $current_step = $step[0]->step + 1;
            if($current_step != 1){
                redirect('vacation/setup_step_'.$current_step, 'refresh');
                return;
            }
        }
        

        $data['defaults'] = array(
                'Annual'=> 'Paid time off work granted by employers to employees to be used for whatever the employee wishes.', 
                'Medical' => 'Time off from work that workers can use to stay home to address their health and safety needs without losing pay.', 
                'Emergency/Casual'=> 'Time taken if you unexpectedly need to take time off from your work due to an emergency or special personal circumstances.', 
                'Maternity'=> 'All female employees are entitled to maternity leave from work immediately before and after the birth of their child.'
            );
        $buffer = $this->load->view('vacation/setup/setup_step_1.php', $data, true);
        $this->user_nav->run_setup_page($buffer, 'Vacation', false);

    }//end func setup_step_1

  

    /*
    * Vacation setup step two
    * 
    */
    public function setup_step_2(){
        
        // Check user login session access and permission
        $this->user_auth->check_login();

        if($this->_check_setup()){
            redirect('vacation', 'refresh');          
        }

        if($this->session->userdata('account_type') != ACCOUNT_TYPE_ADMINISTRATOR AND $this->session->userdata('account_type') != ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR){
            $error_msg = 'Vacation cancelled successfully!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('employee/dashboard', 'refresh');
        }

        if($this->input->server('REQUEST_METHOD') == "POST"){
            if(!$this->input->post('skip_step')){
                foreach ($this->input->post() as $key => $value) {
                    $k = explode('_', $key);
                    if(is_array($value)){
                        
                        $data = array(
                                'id_vacation_type' => $k[1],
                                'id_company' => $this->id_company,
                                'id_pay_structure' => $k[2],
                                'no_of_days' => $value[0]
                            );
                        
                        $this->v_model->add('vacation_compensation', $data);


                        // $data = array(
                        //         'id_vacation_type' => $k[1],
                        //         'id_company' => $this->id_company,
                        //         'id_user' => $k[2],
                        //         'no_of_days' => $value[0],
                        //         'recurring' => 'No'
                        //     );
                        
                        // $this->v_model->add('vacation_compensation', $data);

                    }
                    
                }
            }    

            # set this stage to complete
            $setupdata = array(
                    'id_module'=>2,
                    'id_company'=>$this->id_company,
                    'step'=>2,
                    'status'=>MODULE_SETUP_PENDING,
                    'created_by'=>  $this->id_user_logged_in,
                    'date_created'=>date('Y-m-d', strtotime('now'))
                );
            
            $this->v_model->add('module_setup', $setupdata);

            redirect('vacation/setup_step_3', 'refresh');
            return;
            
        }

        $step = $this->v_model->get_setup_step($this->id_company, 2);
        $current_step = $step[0]->step + 1;
        if($current_step != 2){
            redirect('vacation/setup_step_'.$current_step, 'refresh');
            return;
        }
        $data['has_compensation'] = $this->v_model->get('company_modules', array('id_module'=>10, 'id_company'=>$this->id_company));
        $data['pay_structure'] = $this->v_model->compensation_paygrade($this->id_company);
        $data['vacation_types'] = $this->v_model->get('vacation_types', array('id_company'=>$this->id_company));
        $buffer = $this->load->view('vacation/setup/setup_step_2.php', $data, true);
        $this->user_nav->run_setup_page($buffer, 'Vacation', false);

    }//end func setup_step_2

   

    /*
    * Vacation setup step three
    *
    */
    public function setup_step_3(){
        
        // Check user login session access and permission
        $this->user_auth->check_login();

        if($this->_check_setup()){
            redirect('vacation', 'refresh');          
        }

        if($this->session->userdata('account_type') != ACCOUNT_TYPE_ADMINISTRATOR AND $this->session->userdata('account_type') != ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR){
            $error_msg = 'Vacation cancelled successfully!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('employee/dashboard', 'refresh');
        }

        if($this->input->server('REQUEST_METHOD') == "POST"){

            $stage = 1;
            foreach ($this->input->post() as $value) {
                $k = explode('_', $value);
                $data = array(
                        'approver' => $k[1],
                        'id_company' => $this->id_company,
                        'id_user' => $k[0],
                        'stage' => $stage
                    );
                
                $this->v_model->add('vacation_request_stages', $data);
                $stage++;
            }

            # set this stage to complete
            $setupdata = array(
                    'id_module'=>2,
                    'id_company'=>$this->id_company,
                    'step'=>3,
                    'status'=>MODULE_SETUP_PENDING,
                    'created_by'=>  $this->id_user_logged_in,
                    'date_created'=>date('Y-m-d', strtotime('now'))
                );
            
            $this->v_model->add('module_setup', $setupdata);

            redirect('vacation/setup_step_4', 'refresh');
            return;
            
        }

        $step = $this->v_model->get_setup_step($this->id_company, 2);
        $current_step = $step[0]->step + 1;        
        if($current_step != 3){
            redirect('vacation/setup_step_'.$current_step, 'refresh');
            return;
        }

        $data['employees'] = $this->v_model->get_only_employees($this->id_company);
        $buffer = $this->load->view('vacation/setup/setup_step_3.php', $data, true);
        $this->user_nav->run_setup_page($buffer, 'Vacation', false);

    }//end func setup_step_3



    /*
    * Vacation setup step four
    *
    */
    public function setup_step_4(){
        
        // Check user login session access and permission
        $this->user_auth->check_login();

        if($this->_check_setup()){
            redirect('vacation', 'refresh');          
        }

        if($this->session->userdata('account_type') != ACCOUNT_TYPE_ADMINISTRATOR AND $this->session->userdata('account_type') != ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR){
            $error_msg = 'Vacation cancelled successfully!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('employee/dashboard', 'refresh');
        }

        if($this->input->server('REQUEST_METHOD') == "POST"){
            
            $allowance_setting = $this->input->post('allowance');
            if($allowance_setting !== 0){
                $allowance_data = array(
                    'id_company' => $this->id_company,
                    'vacation_allowance' => $allowance_setting
                    );
                $this->v_model->add('vacation_allowance_setting', $allowance_data);
            }

            if($this->input->post('approver')){
                $stage = 1;
                foreach ($this->input->post('approver') as $value) {
                    $k = explode('_', $value);
                    $data = array(
                            'approver' => $k[1],
                            'id_company' => $this->id_company,
                            'id_user' => $k[0],
                            'stage' => $stage
                        );
                    
                    $this->v_model->add('vacation_allowance_stages', $data);
                    $stage++;
                }
            }   

            # set this stage to complete
            $setupdata = array(
                    'id_module'=>2,
                    'id_company'=>$this->id_company,
                    'step'=>4,
                    'status'=>MODULE_SETUP_PENDING,
                    'created_by'=>  $this->id_user_logged_in,
                    'date_created'=>date('Y-m-d', strtotime('now'))
                );
            
            $this->v_model->add('module_setup', $setupdata);
            $this->v_model->update('module_setup', array('status'=>MODULE_SETUP_ACTIVE), array('id_company'=>$this->id_company, 'id_module'=>2));

            redirect('vacation', 'refresh');
            return;
            
        }


        $step = $this->v_model->get_setup_step($this->id_company, 2);
        $current_step = $step[0]->step + 1;        
        if($current_step != 4){
            redirect('vacation/setup_step_'.$current_step, 'refresh');
            return;
        }

        $data['otherpay'] = $this->v_model->get('payroll_otherpay_settings', array('id_company'=>$this->id_company));
        $data['employees'] = $this->v_model->get_only_employees($this->id_company);
        $buffer = $this->load->view('vacation/setup/setup_step_4.php', $data, true);
        $this->user_nav->run_setup_page($buffer, 'Vacation', false);

    }//end func setup_step_4
    

    public function get_employee_allocated_vacation()
    {
        $id_user = $this->uri->segment(3);
        $allocated = $this->v_model->get_allocated($this->id_company, $id_user);
        $option = '';
        foreach ($allocated as $a) {
            $option .= '<option value="'.$a->id_type.'" data-days="'.$a->no_of_days.'">'.$a->name.'</option>';
        }

        $sel = "<select name='type' id='vactype'>";
        $sel .= $option;
        $sel .= "</select>";
        echo $sel;
    }


/*
 * Restart vacation setup
 * 
 * @access public
 * @return void
 */
    public function setup_restart(){

        if($this->_check_setup()){
            redirect('vacation', 'refresh');          
        }

        $this->v_model->delete('vacation_types', array('id_company'=>$this->id_company));
        $this->v_model->delete('vacation_compensation', array('id_company'=>$this->id_company));
        $this->v_model->delete('vacation_request_stages', array('id_company'=>$this->id_company));
        $this->v_model->delete('vacation_allowance_stages', array('id_company'=>$this->id_company));
        $q = $this->v_model->delete('module_setup', array('id_company'=>$this->id_company, 'id_module'=>2));
        
        if($q){
            $error_msg = 'Vacation data deleted!';
            $this->session->set_flashdata('type', 'success');
            $this->session->set_flashdata('msg', $error_msg);
        }   
        else{
            $error_msg = 'There was a problem processing your request!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
        }

        redirect('vacation/setup_step_1', 'refresh');

    }//end func setup_restart




/*
 * Acheck for vacation setup status
 * 
 * @access public
 * @return void
 */
    public function _check_setup(){

        $q = $this->v_model->get('module_setup', array('id_module'=> 2, 'id_company'=>$this->id_company, 'status'=>MODULE_SETUP_ACTIVE));

        if(empty($q)){
            return FALSE;
        }   
        else{
            return TRUE;
        }

    }//end func _check_setup



/*
     * vacation Analytics
     * 
     * @access public 
     * @return void
     */
    public function analytics(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_VIEW_MY);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }
        
        $data['employees_count'] = $this->v_model->get('employees', array('id_company'=>$this->id_company));
        //$data['stat'] = $stat;
        //$data['plan_request'] = $plan_request;
        $data['department'] = $this->v_model->get('departments', array('id_company'=>$this->id_company));;
        $data['locations'] = $this->v_model->get('company_locations', array('id_company'=>$this->id_company));
        $data['vacation_types']= $this->v_model->get_allocated($this->id_company, $this->id_user_logged_in);

        $buffer = $this->load->view('vacation/vacation_analytics', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func analytics


    /*
     * vacation Plan  Report
     * 
     * @access public 
     * @return void
     */
    public function vacation_plan_report(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_VIEW_MY);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }
        
        $data['empty'] = '';
        $buffer = $this->load->view('vacation/vacation_plan_report', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func analytics


    /*
     * Run vacation Planner Report
     * 
     * @access public 
     * @return void
     */
    public function run_planner_report(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_VIEW_MY);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }
        
        
        $data['vacation_types']= $this->v_model->get('vacation_types', array('id_company'=>$this->id_company));
        $data['locations'] = $this->v_model->get('company_locations', array('id_company'=>$this->id_company));
        $data['departments'] = $this->v_model->get('departments', array('id_company'=>$this->id_company));
        $buffer = $this->load->view('vacation/run_planner_report', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func analytics



    public function generate_planner_excel()
    {
        
        if($this->input->server('REQUEST_METHOD') != "POST"){
            $error_msg = 'Error! Invalid request method!';
            $this->session->set_flashdata('type', 'error');
            $this->session->set_flashdata('msg', $error_msg);
            redirect('employee/my_vacation', 'refresh');
        }
        $approved = array();
        $pending = array();
        $planner = array();
        $months = array(
                        1=>'January',
                        2=>'February',
                        3=>'March',
                        4=>'April',
                        5=>'May',
                        6=>'June',
                        7=>'July',
                        8=>'August',
                        9=>'September',
                        10=>'October',
                        11=>'November',
                        12=>'December'
                    );
            
        $period = $this->input->post('period');
        $type = $this->input->post('type');
        $location = $this->input->post('location');
        $dept = $this->input->post('dept');

        foreach ($months as $key => $m) {
          
            $selected_date = $m.' '.$period;
            $month = date('F', strtotime("$m $period"));
            $month_count = date('t', strtotime("$m $period"));
            $usr = $this->v_model->get('employees', array('id_user'=> $this->session->userdata('id_user')));
            $yr = date('Y');
            if($usr[0]->id_access_level == ACCOUNT_TYPE_ADMINISTRATOR OR $usr[0]->id_access_level == ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR){

                $employees = $this->v_model->get_employee_planner_search($location, $dept, $this->id_company);

            }
            elseif($usr[0]->id_access_level == ACCOUNT_TYPE_UNIT_MANAGER ){
     
                $employees = $this->v_model->get_employee_planner_search($location, $dept, $this->id_company, $this->session->userdata('id_user'));
            }
            

            $datum = array();
            $datum[] = 'First Name';
            $datum[] = 'Last Name';
            $datum[] = 'Staff ID';
            $datum[] = 'Department';
            $datum[] = 'Location';
            for ($i=1; $i <= 31; $i++) { 
                //$datum[]= '';
                if($i > $month_count){
                    $datum[]= '';
                }
                else{
                    $datum[]= $i;
                }
            }
            $planner[$m][] = $datum;

            foreach ($employees as $em) {
                $data = array();
                $data[] = $em->first_name;
                $data[] = $em->last_name;
                $data[] = $em->id_string;
                $data[] = $em->departments;
                $data[] = $em->location_tag;
                for ($i=1; $i <= 31; $i++) { 
                    if($i > $month_count){
                        $data[]= '';
                    }
                    else{
                        $data[]= $i;
                    }
                }
                
                $planner[$m][$em->id_user] = $data;
                
            }

            $approved[] = $this->v_model->get_employee_plans_approved($this->id_company, $type, $month, $period);
            $pending[] = $this->v_model->get_employee_plans_pending($this->id_company, $type, $month, $period);

        }

        $name = $period."VacationPlanner".$this->company_name;
        $this->load->library('csv_helper');
        $data = array(
                'id_company' => $this->id_company,
                'id_user' => $this->session->userdata('id_user'),
                'date_time' => date('Y-m-d G:i:s'),
                'year' => $period
            );

        $this->v_model->add('vacation_planner_report_history', $data);
        $gen = $this->csv_helper->create_planner_excel_file($planner, $approved, $pending, $name);


    }


    /*
     * vacation Planner Report History
     * 
     * @access public 
     * @return void
     */
    public function vacation_planner_report_history(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_VIEW_MY);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }
        
        $data['history'] = $this->v_model->get_planner_report_history($this->id_company);
        $buffer = $this->load->view('vacation/vacation_planner_report_history', $data, true);
        $this->user_nav->run_page($buffer, 'Vacation', false);

    }// End func analytics



    /*
     * vacation for a month in json format
     * 
     * @access public 
     * @return void
     */
    public function month_vacation_json(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_VIEW_MY);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $months = array(
                        1=>'January',
                        2=>'February',
                        3=>'March',
                        4=>'April',
                        5=>'May',
                        6=>'June',
                        7=>'July',
                        8=>'August',
                        9=>'September',
                        10=>'October',
                        11=>'November',
                        12=>'December'
                    );
        
        $dept = $this->uri->segment(3);
        $locate = $this->uri->segment(4);
        $month_num = ltrim($this->uri->segment(5), 0);
        $month = $months[$month_num];
        $year = $this->uri->segment(6);
        $no_of_days = date('t', strtotime("$month $year"));
        $days = '';
        $date = date('Y-m-d', strtotime('1 '.$month.' '.$year));
        $employee_count = count($this->v_model->get_employee_count($this->id_company, $date, $dept, $locate));
        $vacs = $this->v_model->get_vacs($this->id_company, $date, $dept, $locate);
       
        $count_array = array();
        for ($i=1; $i <= $no_of_days; $i++) { 
            $count_array[$i] = 0;
        }

        foreach ($vacs as $v) {
            $sday = date('j', strtotime($v->date_start));
            $eday = date('j', strtotime($v->date_end));

            

            if($eday < $sday)
            {
                for ($i=$sday; $i <= $no_of_days ; $i++) { 
                    $count_array[$i] ++;
                }
            }
            else
            {
                for ($i=$sday; $i <= $eday ; $i++) { 
                    $count_array[$i] ++;
                }
            }
        }

        $employees = '';
        $employees_off = '';
        foreach ($count_array as $ki => $v) {
            $k = $ki - 1;

            if($v > 0){
                $employees_off .= '['. $k .',' . $v . ']' .',' ;
                $employees .= '['. $k .',' . $employee_count . ']' .',' ;
            }
            else{
                $employees_off .= '['. $k .',' . 0 . ']' .',' ;
                $employees .= '['. $k .',' . 0 . ']' .',' ;
            }
        }


        $employees_off = rtrim($employees_off, ',');
        $employees = rtrim($employees, ',');

        $eo_data = '['.$employees_off.']';
        $e_data = '['.$employees.']';

        echo '[{ "label" : "Employees", "data" : '.$e_data.'}, { "label" : "Vacation", "data" : '.$eo_data.'}]';


    }// End func month_vacation_json



    /*
     * vacation allowance statistics in json format
     * 
     * @access public 
     * @return void
     */
    public function vacation_allowance_json(){

        // Check user login session access and permission
        $this->user_auth->check_login();
        //$this->user_auth->check_perm(VACATION_VIEW_MY);
        
        if(!$this->_check_setup()){
            redirect('vacation/setup_step_1', 'refresh');          
        }

        $months = array(
                        1=>'January',
                        2=>'February',
                        3=>'March',
                        4=>'April',
                        5=>'May',
                        6=>'June',
                        7=>'July',
                        8=>'August',
                        9=>'September',
                        10=>'October',
                        11=>'November',
                        12=>'December'
                    );


        $dept = $this->uri->segment(3);
        $locate = $this->uri->segment(4);
        $year = $this->uri->segment(5);
        $paid = '';
        $unpaid = '';
        $vac_sum = $this->v_model->get_vallowance_sum($this->id_company, $dept, $locate, $year);
        
        $month_num = date('n');
        $tots = $vac_sum;
        $ptots = 0;

        $count_array = array();
        for ($i=1; $i <= 12; $i++) { 
            
            $month = $months[$i];
            $date = date('Y-m-d', strtotime('1 '.$month.' '.$year));
 
            $vc = $this->v_model->get_vallowance_sum_month($this->id_company, $dept, $locate, $date);

            if(empty($vc[0]->amount)){
                $vc_amt = 0;
            }
            else{
                $vc_amt = $vc[0]->amount;
            }

            $tots = $tots - $vc_amt;
            $ptots = $ptots + $vc_amt;
            $ii = $i-1;
            $paid .= '['. $ii .',' . $ptots . ']' .',' ;
            $unpaid .= '['. $ii .',' . $tots . ']' .',' ;
           
        }

        $paid = rtrim($paid, ',');
        $unpaid = rtrim($unpaid, ',');
        $paid_data = '['.$paid.']';
        $unpaid_data = '['.$unpaid.']';

        echo '['.$unpaid_data.','.$paid_data.']';

    }// End func month_vacation_json



    /*
     * update employee leave days allocation
     * 
     * @access public 
     * @return void
     */

    function _update_leave_allocation($id_user){

        $us = $this->v_model->get_employee_info($this->id_company, $id_user);
        $ent = $this->v_model->get_employee_entitlements($id_user);
        
        if(empty($ent) AND !empty($us->id_payroll_paygrade)){
            $role_vacation = $this->v_model->get_single_role_vacation($this->id_company, $us->id_payroll_paygrade);
            $date = date('Y-m-d', strtotime('now'));
            foreach ($role_vacation as $rv) {
                if($rv->no_of_days > 0){
                    $allc_data = array(
                            'id_type' => $rv->id_vacation_type,
                            'id_company' => $rv->id_company,
                            'id_user' => $us->id_user,
                            'no_of_days' => $rv->no_of_days,
                            'date_start' => $date
                        );

                    $this->v_model->add('vacation_allocated', $allc_data);
                }
            }
        }
    }


    function _update_all_leave_allocation(){

        $usrs = $this->v_model->get('users', array('id_company'=>$this->id_company));

        foreach ($usrs as $usr) {
            $us = $this->v_model->get_employee_info($this->id_company, $usr->id_user);
            $ent = $this->v_model->get_employee_entitlements($us->id_user);
            
            if(empty($ent) AND !empty($us->id_payroll_paygrade)){
                $role_vacation = $this->v_model->get_single_role_vacation($this->id_company, $us->id_payroll_paygrade);
                $date = date('Y-m-d', strtotime('now'));
                foreach ($role_vacation as $rv) {
                    if($rv->no_of_days > 0){
                        $allc_data = array(
                                'id_type' => $rv->id_vacation_type,
                                'id_company' => $rv->id_company,
                                'id_user' => $us->id_user,
                                'no_of_days' => $rv->no_of_days,
                                'date_start' => $date
                            );

                        $this->v_model->add('vacation_allocated', $allc_data);
                    }
                }
            }
        }
    }

	
}// End Vacation class

