Vacation Module Documentatiomn
------------------------------------------------------------------

@author Obinna Ukpabi <obinna.ukpabi@talentbase.ng>
@module vacation
------------------------------------------------------------------


TOC
-----------

1. Files
2. Database
3. Libraries
4. Helpers


Files
--------------------------------------------------------------------

@controllers/vacation
- vacation.php


@models/vacation
- vacation_model.php


@views/vacation
- allocate_vacation.php
- allocate_vacation_modal.php
- allowance_request.php
- approval_process_setting.php
- employee_entitlement.php
- holidays.php
- my_vacation.php
- my_vacation_planner.php
- ongoing_vacation.php
- pending_approvals.php
- summary.php
- vacation_allowance.php
- vacation_allowance_details.php
- vacation_planner.php
- vacation_planner_setting.php
- vacation_types.php

@views/vacation/setup
- setup_step_1.php
- setup_step_2.php
- setup_step_3.php
- setup_step_4.php



Database
-----------------------------------------------------------------------

- vacation_allocated
- vacation_allowance_approver
- vacation_allowance_request
- vacation_allowance_stages
- vacation_compensation
- vacation_holidays
- vacation_plan
- vacation_plan_approver
- vacation_plan_request
- vacation_plan_stages
- vacation_request
- vacation_request_approver
- vacation_request_stages
- vacation_types