<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | Hooks
  | -------------------------------------------------------------------------
  | This file lets you define "hooks" to extend CI without hacking the core
  | files.  Please see the user guide for info:
  |
  |	http://codeigniter.com/user_guide/general/hooks.html
  |
 */
$hook = !empty($hook) ? $hook : [];
$hook['pre_controller'] = [];
$hook['pre_controller'][] = array(
    'class' => 'DateTimezoneSetter',
    'function' => 'setAppTimezone',
    'filename' => 'DateTimezoneSetter.php',
    'filepath' => 'hooks',
    //'params' => array('bread', 'wine', 'butter')
);

$hook['post_controller_constructor'] = array(
    'class' => 'DateTimezoneSetter',
    'function' => 'setDbTimezone',
    'filename' => 'DateTimezoneSetter.php',
    'filepath' => 'hooks',
    //'params' => array('bread', 'wine', 'butter')
);

$hook['post_controller'] = array(
    'class' => 'Api_Display_Handler',
    'function' => 'display',
    'filename' => 'api_display_handler.php',
    'filepath' => 'hooks',
    //'params' => array('bread', 'wine', 'butter')
);

/* End of file hooks.php */
/* Location: ./application/config/hooks.php */