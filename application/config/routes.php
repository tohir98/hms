<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */

$route['default_controller'] = "landing/hospital/welcome";

$route['login'] = "user/user/login";
$route['logout'] = "user/user/logout";
$route['admin/dashboard'] = "user/user/dashboard";

//Routes for patient
$route['patient'] = "patient/patient";
$route['patient/patient_directory'] = "patient/patient";
$route['patient/(:any)'] = "patient/patient/$1";

//API V1 routes
$route['api/v1/info'] = 'api_v1/api_info_controller/index';
$route['api/v1/auth/switch_account/(\d+)'] = 'api_v1/api_auth_controller/switch_account/$1';
$route['api/v1/auth/(:any)'] = 'api_v1/api_auth_controller/$1';
$route['api/v1/tasks'] = 'api_v1/api_tasks_controller/list_tasks';
$route['api/v1/tasks/([a-z_A-Z]+)'] = 'api_v1/api_tasks_controller/list_tasks/$1';
$route['api/v1/staff'] = 'api_v1/api_staff_controller/index';
$route['api/v1/staff/(\d+)'] = 'api_v1/api_staff_controller/get_by_id/$1';
$route['api/v1/staff/([A-Za-z0-9]+)'] = 'api_v1/api_staff_controller/get_by_id_string/$1';
$route['api/v1/department'] = 'api_v1/api_department_controller/index';
$route['api/v1/device/(:any)'] = 'api_v1/api_device_controller/$1';
