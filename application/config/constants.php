<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  |--------------------------------------------------------------------------
  | Constants required by Access Control - Account type
  |--------------------------------------------------------------------------
  |
 */
define('ACCOUNT_TYPE_ADMINISTRATOR', 1); // Administrator
define('ACCOUNT_TYPE_EMPLOYEE', 4); // Employee
define('ACCOUNT_TYPE_UNIT_MANAGER', 5); // Unit Manager
define('ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR', 6); // Secondary Admin


define('DEFAULT_COMPANY', 1); // default company




/*
  |--------------------------------------------------------------------------
  | Constants required by Recruiting - Jobs status
  |--------------------------------------------------------------------------
  |
 */
define('JOB_STATUS_DRAFT', 0); // 4. Draft
define('JOB_STATUS_PUBLISHED', 1); // 2. Published
define('JOB_STATUS_CLOSED', 2); // 1. Closed
define('JOB_STATUS_PENDING', 3); // 3. Pending

/*
  |--------------------------------------------------------------------------
  | Constants required by Recruiting - job level
  |--------------------------------------------------------------------------
  |
 */
define('JOB_LEVEL_PROF', 1); // Professional
define('JOB_LEVEL_RECENT_GR', 2); // Recent Grad
define('JOB_LEVEL_STUGENT', 3); // Student

/*
  |--------------------------------------------------------------------------
  | Constants required by Admin Module - Module status
  |--------------------------------------------------------------------------
  |
 */
define('DEACTIVATED', 0); //  Deactivated
define('ACTIVE', 1); //  Active
define('COMING_SOON', 2); //  Coming Soon

/*
  |--------------------------------------------------------------------------
  | Constants required by Loans Module - Payment status
  |--------------------------------------------------------------------------
  |
 */
define('LOAN_PAY_PLAN_STATUS_PAID', 1);
define('LOAN_PAY_PLAN_STATUS_OVERDUE', 2);
define('LOAN_PAY_PLAN_STATUS_UPCOMING', 3);
define('LOAN_PAY_PLAN_STATUS_PARTLY_PAID', 4);

/*
  |--------------------------------------------------------------------------
  | Constants required by Loans Module - Loans status
  |--------------------------------------------------------------------------
  |
 */
define('LOAN_PAYMENT_STATUS_GOOD_STANDING', 1);
define('LOAN_PAYMENT_STATUS_OVERDUE', 2);
define('LOAN_PAYMENT_STATUS_PAID_IN_FULL', 3);

/*
  |--------------------------------------------------------------------------
  | Constants required by Employee module - Phone Type
  |--------------------------------------------------------------------------
  |
 */
define('WORK_PHONE_TYPE', 1);
define('HOME_PHONE_TYPE', 2);
define('PERSONAL_PHONE_TYPE', 3);
define('EMPLOYEE_PRIMARY_PHONE', 1);

/*
  |--------------------------------------------------------------------------
  | Constants required by Events Module - Event colors
  |--------------------------------------------------------------------------
  |
 */
define('EVENT_COLOR_BLUE', 1);
define('EVENT_COLOR_GREEN', 2);
define('EVENT_COLOR_BROWN', 3);
define('EVENT_COLOR_RED', 4);
define('EVENT_COLOR_YELLOW', 5);
define('EVENT_COLOR_TEAL', 6);


/*
  |--------------------------------------------------------------------------
  | Constants required by Talentbase functionality
  |--------------------------------------------------------------------------
  |
  | This constants names starting with part name and continue
  | with sub part prefix.
  | Example: USER_STATUS_ACTIVE
  |  - explains that this constant for users functionality.
  |  - 'USER_' : users part.
  |  - 'STATUS_' : user status info.
  |  - 'ACTIVE' : status value.
  |
 */
define('USER_STATUS_INACTIVE', 0);
define('USER_STATUS_ACTIVE', 1);
define('USER_STATUS_SUSPENDED', 2);
define('USER_STATUS_TERMINATED', 3);
define('USER_STATUS_PROBATION_ACCESS', 4);
define('USER_STATUS_ACTIVE_NO_ACCESS', 5); // User exists, account is active like employee - not have access to portal. but cannot login.
define('USER_STATUS_PROBATION_NO_ACCESS', 6);

define('USER_TYPE_ADMIN', 1);
define('USER_TYPE_EMPLOYEE', 2);

/* Tasks based statuses */

define('TASK_STATUS_TO_DO', 0);
define('TASK_STATUS_COMPLETE', 1);
define('TASK_STATUS_IN_PROGRESS', 2);
define('TASK_STATUS_DECLINED', 3);

/* Recruiting based status */
define('RECRUITING_STATUS_CANCELLED', 0);
define('RECRUITING_STATUS_PUBLISHED', 1);
define('RECRUITING_STATUS_PENDING', 2);
define('RECRUITING_STATUS_DRAFT', 3);
define('RECRUITING_STATUS_CLOSED', 4);

/* Appraisal template type */
define('APPRAISAL_TYPE_RATINGS_ONLY', 1);
define('APPRAISAL_TYPE_RATINGS_AND_COMMENTS', 2);
define('APPRAISAL_TYPE_COMMENTS_ONLY', 3);

/* Appraisal types */
define('APPRAISAL_TYPE_REVIEW_SELF', 1);
define('APPRAISAL_TYPE_REVIEW_MANAGER', 2);
define('APPRAISAL_TYPE_REVIEW_BOTH', 3);

/* Appraisal review state */
define('APPRAISAL_REVIEW_STATUS_ACTIVE', 1);
define('APPRAISAL_REVIEW_STATUS_INACTIVE', 0);

define('APPRAISAL_CYCLE_STATUS_UPCOMING', 1);
define('APPRAISAL_CYCLE_STATUS_ACTIVE', 2);
define('APPRAISAL_CYCLE_STATUS_CLOSED', 3);

/* Appraisal completed status */
define('APPRAISAL_STATE_COMPLETED', 1); // Appraisal completed
define('APPRAISAL_STATE_PENDING', 0); // Appraisal pending

/* Appraisal section status */
define('APPRAISAL_SECTION_STATUS_ACTIVE', 1); // active section status
define('APPRAISAL_SECTION_STATUS_INACTIVE', 0); // inactive section status

/* Access control status */
define('ACCESS_CONTROL_MODULE_STATUS_REGULAR', 0); // regular permissions
define('ACCESS_CONTROL_MODULE_STATUS_ADVANCED', 1); // advanced permissions
define('ACCESS_CONTROL_MODULE_STATUS_RESTRICTED', 2); // restricted
define('ACCESS_CONTROL_MODULE_STATUS_DEFAULT', 3); // default

/* Loans Status */
define('LOANS_STATUS_CANCELLED', 0);
define('LOANS_STATUS_APPROVED', 1);
define('LOANS_STATUS_PENDING', 2);
define('LOANS_STATUS_DECLINE', 3);

/* Payroll Status */
define('PAYROLL_STATUS_IN_PROCESS_STAGE_1', 1);
define('PAYROLL_STATUS_IN_PROCESS_STAGE_2', 2);
define('PAYROLL_STATUS_IN_PROCESS_STAGE_3', 3);
define('PAYROLL_STATUS_AWAITING_APPROVAL', 4);
define('PAYROLL_STATUS_APPROVED', 5);

/* Payroll Run types */
define('PAYROLL_REGULAR_RUN', 2);
define('PAYROLL_OTHERPAY_RUN', 1);

/* Loan Interest Type */
define('NO_INTEREST', 0);
define('FIXED_INTEREST', 1);
define('REDUCING_BALANCE_INTEREST', 2);

define('COMPENSATION_VARIABLE_TYPE', 1);
define('COMPENSATION_FIXED_TYPE', 0);
define('COMPENSATION_MONTHLY', 2);
define('COMPENSATION_YEARLY', 3);
define('COMPENSATION_WEEKLY', 1);
define('TAXABLE', 1);
define('TAX_EXEMPT', 0);

/* Employee status */
define('EMPLOYEE_STATUS_IS_CEO', 2);
define('EMPLOYEE_STATUS_IS_MANAGER', 1);
define('EMPLOYEE_STATUS_IS_STAFF', 0);

/*
  | ---------------------------------------------------
  |	Temp directory for storing appraisals in pdf
  | ---------------------------------------------------
 */
define('FILE_PATH', APP_ROOT . 'files/');

if (!is_dir(FILE_PATH)) {
    @mkdir(FILE_PATH, 0755, true);
}

define('FILE_PATH_RESUMES', FCPATH . 'resumes_test_container');

define('FILE_PATH_SAMPLES', APP_ROOT . 'templates/');

if (!is_dir(FILE_PATH_SAMPLES)) {
    @mkdir(FILE_PATH_SAMPLES, 0755, true);
}

/*
  | ---------------------------------------------------
  |	System default values.
  | ---------------------------------------------------
 */
define('USER_LOGIN_ATTEMPTS', 6);
define('FIRST_COMPANY_REFID', 41020001);
define('FIRST_CONSULTANT_REFID', 7001001);

/*
  | ---------------------------------------------------
  |	Module setup status.
  | ---------------------------------------------------
 */
define('MODULE_SETUP_PENDING', 1);
define('MODULE_SETUP_ACTIVE', 2);


/*
  | ---------------------------------------------------
  | Module notification status.
  | ---------------------------------------------------
 */
define('NOTIFICATION_NONE', 0);
define('NOTIFICATION_EMAIL_SMS', 1);
define('NOTIFICATION_EMAIL', 2);
define('NOTIFICATION_SMS', 3);


/*
  | ---------------------------------------------------
  |	Loans approval default process
  | ---------------------------------------------------
 */
define('MANAGER', 0);

/*
  |--------------------------------------------------------------------------
  | Permission constants.
  |--------------------------------------------------------------------------
  |
  | Permission data contains in db as:
  |	module_perms.id_perm -> user_perms.id_perm
  |
  | This constants names starting with module name,
  | contains part name and ends with action name
  |
  | Example: PAYROLL_HISTORY_SEARCH
  |	means: {module_payroll}_{part_history}_{action_search}
  |
 */

/* Appraisal sections */
define('APPRAISAL_SECTION_VIEW', 96); // view appraisal section
define('APPRAISAL_SECTION_ADD', 97); // add new appraisal section
define('APPRAISAL_SECTION_EDIT', 98); // edit existing appraisal section
define('APPRAISAL_SECTION_DELETE', 99); // delete existing appraisal section

/* Company locations */
define('COMPANY_LOCATIONS_ADD', 12);
define('COMPANY_LOCATIONS_DELETE', 14);
define('COMPANY_LOCATIONS_EDIT', 13);
define('COMPANY_LOCATIONS_VIEW', 20);

/* Departments */
define('DEPARTMENTS_VIEW', 15);
define('DEPARTMENTS_ADD', 2);
define('DEPARTMENTS_EDIT', 3);
define('DEPARTMENTS_DELETE', 4);

/* Roles */
define('EMPLOYEE_ROLES_ADD', 21);
define('EMPLOYEE_ROLES_EDIT', 22);
define('EMPLOYEE_ROLES_VIEW', 19);
define('EMPLOYEE_ROLES_DELETE', 23);

/* Vacation */
define('VACATION_MODULE', 2);
define('VACATION_VIEW_MY', 5);
define('VACATION_TYPES', 6);
define('VACATION_HOLIDAY', 7);
define('VACATION_ALLOCATE', 54);
define('VACATION_PENDING_APPROVAL', 80);
define('VACATION_VIEW_SUMMARY', 81);
define('VACATION_HOLIDAY_ADD', 82);
define('VACATION_HOLIDAY_EDIT', 83);
define('VACATION_HOLIDAY_DELETE', 84);
define('VACATION_TYPE_ADD', 85);
define('VACATION_TYPE_DELETE', 86);
define('VACATION_TYPE_EDIT', 87);
define('VACATION_REQUEST_MAKE', 88);
define('VACATION_REQUEST_APPROVE', 89);
define('VACATION_REQUEST_DECLINE', 90);

/* Attendance */
define('ATTENDANCE_MODULE', 14);
define('ATTENDANCE_DEVICE_LOCATIONS', 132);
define('ATTENDANCE_CURRENT_EMPLOYEES', 133);
define('ATTENDANCE_ADD_EMPLOYEE', 134);
define('ATTENDANCE_TODAY', 135);
define('ATTENDANCE_RECORDS', 136);
define('ATTENDANCE_SETTINGS', 137);
define('ATTENDANCE_DELETE_AUTO_REPORT_SETTING', 155);
define('ATTENDANCE_EDIT_AUTO_REPORT_SETTING', 156);
define('ATTENDANCE_ADD_NEW_RECORD', 157);
define('ATTENDANCE_DELETE_SHIFT', 158);
define('ATTENDANCE_EDIT_SHIFT', 159);
define('ATTENDANCE_CREATE_NEW_SHIFT', 160);
define('ATTENDANCE_EMPLOYEE_ATTENDANCE_RECORD', 161);
define('ATTENDANCE_EDIT_EMPLOYEES_SHIFT', 162);
define('ATTENDANCE_REFRESH_ATTENDANCE', 163);
define('ATTENDANCE_REMOVE_EMPLOYEE', 164);
define('ATTENDANCE_SCHEDULE_OVERVIEW', 165);
define('ATTENDANCE_MY_SCHEDULE', 167);
define('ATTENDANCE_REMOVE_SHIFT_SETTINGS', 169);
define('ATTENDANCE_ADD_SHIFT_SETTINGS', 170);

/* Tasks */
define('TASKS_CHANGE_STATUS', 56);

/* Payroll */
define('PAYROLL_RUN', 74); // run payroll
//define('PAYROLL_SETTINGS_VIEW', 57); // view payroll settings

define('PAYROLL_CURRENCY_SET', 62); // Set payroll currency
define('PAYROLL_CURRENCY_EDIT', 63); // edit payroll currency

define('PAYROLL_SETTINGS_VIEW', 58); // view payroll settings {set company payroll pay types}
define('PAYROLL_COMPANY_PAY_TYPES_DELETE', 59); // delete company payroll pay types
define('PAYROLL_COMPANY_DEDUCTIONS_SET', 60); // set company payroll deductions
define('PAYROLL_COMPANY_DEDUCTIONS_DELETE', 61); // delete company payroll deductions

define('PAYROLL_INSPECT_PROCESS', 70); // select employee to process payroll
define('PAYROLL_EMPLOYEE_ON_PAYROLL_LIST', 74); // list employee on payroll
define('PAYROLL_EMPLOYEE_TO_PAYROLL_ADD', 75); // add employee to payroll
define('PAYROLL_EMPLOYEE_PAYSLIP_VIEW', 76); // view employee payslip
define('PAYROLL_EMPLOYEE_PAYSLIP_EDIT', 77); // edit employee payslip

define('PAYROLL_PAYSLIP_EDIT', 71); // edit payslip
define('PAYROLL_APPROVE_PROCESS', 73); // approve and initiate payment for payroll
define('PAYROLL_PREPARE_PROCESS', 78); // Start and prepare payroll for approval and payment
define('PAYROLL_CANCEL_PROCESS', 79); // Cancel Payroll process
// Tohir
define('PAYROLL_DISBURSEMENT', 227); // PAYROLL Disbursement
define('PAYROLL_MY_PAY_SLIP', 61); // PAYROLL Disbursement
/* Disbursement Status */
define('PAYROLL_DISBURSEMENT_PENDING', 0);
define('PAYROLL_DISBURSEMENT_APPROVED', 1);
define('PAYROLL_DISBURSEMENT_PROCESSING_CHARGE', 0.01);

define('PAYROLL_PRE_TAX_INCIDENTAL', 0);
define('PAYROLL_POST_TAX_INCIDENTAL', 1);
define('PAYROLL_INCIDENTAL', 187);
define('PAYROLL_RECORDS', 64);
define('PAYROLL_EMPLOYEE_COMPENSATION', 69); // view list of employee on payroll


/* Company */
define('COMPANY_CAREER_PAGE_SETTINGS_VIEW', 46); // View career page settings
define('COMPANY_CAREER_PAGE_SETTINGS_MANAGE', 47); // Manage career page settings

/* Recruiting */
define('RECRUITING_COMPANY_LIVE_JOBS_VIEW', 48);
define('RECRUITING_COMPANY_LIVE_JOBS_POST', 49);
define('RECRUITING_COMPANY_LIVE_JOBS_EDIT', 50);

/* Employee Request */
define('EMPLOYEE_REQUESTS', 177);
define('MY_REQUESTS', 176);
define('EMPLOYEE_REQUEST_SETTINGS', 178);
define('EMPLOYEE_REQUEST_RECORDS', 222);
define('EMPLOYEE_REQUEST_ANALYTICS', 223);

define('EMPLOYEE_REQUEST_PENDING', 0);
define('EMPLOYEE_REQUEST_APPROVED', 1);
define('EMPLOYEE_REQUEST_DECLINED', 2);
define('EMPLOYEE_REQUEST_FILLED', 3);

define('EDIT_MY_WORK_NUMBER', 231);
define('EDIT_MY_BIRTHDAY', 232);
define('EDIT_MY_HOME_ADDRESS', 233);
define('EDIT_MY_PERSONAL_NUMBER', 234);
define('EDIT_MY_EMERGENCY_CONTACT_1', 235);
define('EDIT_MY_EMERGENCY_CONTACT_2', 236);
define('EDIT_MY_BANK_ACCOUNT', 237);
define('EDIT_MY_PENSION_DETAILS', 238);

/* Access applications for live jobs */
define('RECRUITING_ACCESS_LIVE_JOB_APPLICATIONS', 166);

/* Downloads */
define('DOWNLOADS_SHARED_FILES_VIEW', 26); // View company shared files
define('DOWNLOADS_SHARED_FILES_ADD', 27); // Add company  shared files
define('DOWNLOADS_SHARED_FILES_EDIT', 28); // Edit company shared files
define('DOWNLOADS_SHARED_FILES_DELETE', 29); // Delete company shared files

/* Events */
define('EVENTS_COMPANY_VIEW', 30); // View all company events
define('EVENTS_COMPANY_ADD', 31); // Add new events
define('EVENTS_COMPANY_EDIT', 32); // Edit an existing event
define('EVENTS_COMPANY_DELETE', 33); // Delete company events

/* KPI */
define('KPI_APPRAISAL_SETTINGS', 55); // View appraisal settings
define('KPI_APPRAISAL_STAFF_RECORDS', 10); // search for staff appraisal records
define('KPI_APPRAISAL_ASSIGNED_APPRAISALS', 8); // View assigned appraisal form
define('KPI_APPRAISAL_CREATE_APPRAISAL_FORMS', 79); // Create appraisal forms

/* Access Control */
// define('ACCESS_CONTROL_ASSIGN_PERMISSIONS', 91); // Assign permissions to user


/* Employee Queries */
define('ISSUE_QUERY', 92); // Can issue out queries
define('VIEW_MY_QUERIES', 93); // Can View queries received
define('VIEW_QUERY_RECORDS', 94); // Can view query records
define('VIEW_REPLY_QUERY', 95); // can view and reply quries

/* Employee - Staff directory */
define('VIEW_STAFF_DIRECTORY', 1);
define('VIEW_EMPLOYEE_RECORDS', 138);
define('EDIT_REQUEST_EMPLOYEE_RECORDS', 173); // NEW FOR ALL EDIT AND REQUEST ACTIONS !!!
define('ADD_NEW_EMPLOYEE', 11);
define('CHANGE_EMPLOYEE_STATUS', 139);
define('RESET_EMPLOYEE_PASSWORD', 25);
define('MANAGER_VIEW', 168);

/* Admin perms for edit some data */
//define('EDIT_BENEFITS', 193); // Benefits
//define('EDIT_CREDENTIALS', 194); // Resume / Credentials
////define('EDIT_WORK_PROFILE', 105);
//define('EDIT_PERSONEL_PROFILE', 188); // Personal profile
//define('EDIT_BANK_DETAILS', 192); // Bank
//define('EDIT_EMERGENCY_CONTACTS', 191); // Emergency contacts
//define('EDIT_PROFILE_PICTURE', 189); // Profile picture
//define('EDIT_PROFILE_PERSONAL_INFO', 190); // Personal Info

/* Employee perms for edit */
//define('REQUEST_EMPLOYEE_PROFILE_PICTURE', 125);
//define('REQUEST_EMPLOYEE_PERSONAL_PROFILE', 126);
//define('REQUEST_EMPLOYEE_RESUME_CREDENTIALS', 127);
//define('REQUEST_EMPLOYEE_EMERGENCY_CONTACTS', 128);
//define('REQUEST_EMPLOYEE_BANK_DETAILS', 129);
//define('REQUEST_EMPLOYEE_BENEFITS_DETAILS', 130);
// Employee view personal data
//define('VIEW_MY_PROFILE_PICTURE', 111);
//define('VIEW_MY_PROFILE_WORK_INFO', 112);
//define('VIEW_MY_PROFILE_PERSONAL_INFO', 113);
//define('VIEW_MY_EMERGENCY_CONTACTS', 114);
define('VIEW_MY_BANK_DETAILS', 185);
define('VIEW_MY_BENEFITS_DETAILS', 186);
//define('VIEW_MY_CREDENTIALS', 140); // Uploaded docs
// Employee perms for edit profile
define('EDIT_MY_PROFILE_PICTURE', 189);
define('EDIT_MY_PROFILE_PERSONAL_INFO', 190);
define('EDIT_MY_EMERGENCY_CONTACTS', 191);
define('EDIT_MY_BANK_DETAILS', 192);
define('EDIT_MY_BENEFITS_DETAILS', 193);
define('EDIT_MY_CREDENTIALS', 194); // Uploaded docs

/* Constats for employee. Personal */
//define('VIEW_MY_PROFILE', 109);
define('VIEW_MY_TASKS', 36);

/* HR Clients */
define('HR_CLIENTS_VIEW', 100); // view hr clients
define('HR_CLIENTS_ADD', 101); // add new hr clients
define('HR_CLIENTS_EDIT_CLIENT_ACCESS', 123); // edit hr client access
define('HR_CLIENTS_ACCOUNT_HISTORY', 102); // account  history


/* HR Clients */
// define('COMPENSATION_EMPLOYEES_VIEW', 140);
// define('COMPENSATION_EMPLOYEE_ADD', 141);
// define('COMPENSATION_PAY_STRUCTURE_VIEW', 142);
// define('COMPENSATION_PAY_STRUCTURE_ADD', 143);
// define('COMPENSATION_PAY_STRUCTURE_EDIT', 144);
// define('COMPENSATION_PAY_STRUCTURE_DELETE', 145);
// define('EMPLOYEE_COMPENSATION_ADD', 146);
// define('EMPLOYEE_COMPENSATION_EDIT', 147);
// define('EMPLOYEE_COMPENSATION_DELETE', 148);
// define('EMPLOYEE_COMPENSATION_VIEW', 149);
define('COMPENSATION_SETTING', 151);
//define('PAY_STRUCTURE_LIST', 150);

/*
  |--------------------------------------------------------------------------
  | GOALS PERMISSION
  |--------------------------------------------------------------------------
 */
define('MY_GOALS', 174);
define('GOALS_OVERVIEW', 175);
define('ADD_GOALS', 203);
define('EDIT_GOALS', 204);
define('DELETE_GOALS', 205);
define('UPDATE_GOALS', 206);

/*
  |--------------------------------------------------------------------------
  | Goal activation status
  |--------------------------------------------------------------------------
 */
define('GOAL_STATUS_REWORK', 2);
define('GOAL_STATUS_ACTIVE', 1);
define('GOAL_STATUS_PENDING', 0);


/*
  |--------------------------------------------------------------------------
  | Training PERMISSION
  |--------------------------------------------------------------------------
 */
define('my_TRAINING', 239);
define('MANAGE_VENDORS', 240);
define('BROWSE_COURSES', 241);
define('MANAGE_COURSES', 242);
define('EMPLOYEE_TRAINING', 243);
define('APPROVAL_SETTINGS', 245);
define('PENDING_APPROVAL', 251);


/*
  |--------------------------------------------------------------------------
  | Training status
  |--------------------------------------------------------------------------
 */
define('TRAINING_PENDING', 0);
define('TRAINING_APPROVED', 1);


/*
  |--------------------------------------------------------------------------
  | Appraisal settings template frequency
  |--------------------------------------------------------------------------
 */
define('FREQUENCY_WEEKLY', 1);
define('FREQUENCY_MONTHLY', 2);
define('FREQUENCY_QUARTERLY', 3);
define('FREQUENCY_SEMI_ANNUAL', 4);
define('FREQUENCY_ANNUAL', 5);


/*
  |--------------------------------------------------------------------------
  | RACKSPACE - FILES SERVER CONNECTION VALUES
  |--------------------------------------------------------------------------
 */
define('RACKSPACE_USERNAME', 'cloudfile.access');
define('RACKSPACE_API_KEY', 'cad64e3f4984440ba78ff080c5a5fc9e');
if (ENVIRONMENT === 'production') {
    define('STORAGE_CONTAINER_ASSETS', 'tb_publicpgassets_001');
} else {
    define('STORAGE_CONTAINER_ASSETS', 'tbtest_publicpgassets_001');
}
define('RACKSPACE_CONTAINER_TEST_SITE', 'tbtest_publicpgassets_001'); // Rackspace container for public files on test site
//live site
if (ENVIRONMENT === 'production') {
    define('STORAGE_CONTAINER_SITE_RESUME', 'tb_4102_resumecontainer_001');
} else {
    define('STORAGE_CONTAINER_SITE_RESUME', 'tbtest_resumecontainer_001');
}
/*
  |--------------------------------------------------------------------------
  | TalentBase Contact info
  |--------------------------------------------------------------------------
 */
define('TALENTBASE_PHONE_NUMBER', '+234 (0) 8183377264');
define('TALENTBASE_SUPPORT_EMAIL', 'helpdesk@talentbase.ng');
define('TALENTBASE_SUPPORT_EMAIL_2', 'support@talentbase.ng');
define('TALENTBASE_NO_REPLY_EMAIL', 'no-reply@talentbase.ng');
define('TALENTBASE_WEBSITE', 'www.talentbase.ng');
define('TALENTBASE_ADDRESS', 'D1 Flat 505, 1004 Estate Victoria Island, Lagos, Nigeria');
define('TALENTBASE_BUSINESS_NAME', 'TalentBase NG.');


/*
  |--------------------------------------------------------------------------
  | Currency Value
  |--------------------------------------------------------------------------
 */

define('CURRENCY_NAIRA', '&#x20A6;');
define('CURRENCY_DEFAULT_ISO', 'NGN');




/*
  |--------------------------------------------------------------------------
  | Log type constants
  |--------------------------------------------------------------------------
 */

define('LOG_TYPE_MISC', 1);
define('LOG_TYPE_LOGIN', 2);
define('LOG_TYPE_LOGOUT', 3);

/*
  |--------------------------------------------------------------------------
  | Country ID - Nigeria
  |--------------------------------------------------------------------------
 */

define('NIGERIA', 115);

/*
  |--------------------------------------------------------------------------
  | Database Schema - Nigeria
  |--------------------------------------------------------------------------
 */

define('LOCAL_SCHEMA', 'ci');
define('TEST_SCHEMA', 'talentbasetest3');
define('LIVE_SCHEMA', 'talentbase_dbRS002');

/*
  |--------------------------------------------------------------------------
  | Country Phone Code - Nigeria - 234
  |--------------------------------------------------------------------------
 */

define('PHONE_CODE_NIGERIA', 234);

/*
  |--------------------------------------------------------------------------
  | Job Interview Status
  |--------------------------------------------------------------------------
 */
define('INTERVIEW_STATUS_UPCOMING', 1);
define('INTERVIEW_STATUS_COMPLETED', 2);

/*
  |--------------------------------------------------------------------------
  | Company activation status
  |--------------------------------------------------------------------------
 */
define('COMPANY_STATUS_ACTIVE', 1);
define('COMPANY_STATUS_DEACTIVATED', 0);

/*
  |--------------------------------------------------------------------------
  | Consultant activation status
  |--------------------------------------------------------------------------
 */
define('CONSULTANT_REQUEST_STATUS_ACTIVE', 1);
define('CONSULTANT_REQUEST_STATUS_PENDING', 2);
define('CONSULTANT_REQUEST_STATUS_DECLINED', 0);


/*
  |--------------------------------------------------------------------------
  | Mixpanel switch
  / 0 = off
  / 1 = on
  |--------------------------------------------------------------------------
 */
define('MIXPANEL_SWITCH', 1);


/*
  |--------------------------------------------------------------------------
  | Module Tour
  |--------------------------------------------------------------------------
 */
define('ALL_MODULE_TOUR', 1);
define('VACATION_TOUR', 1);
define('EMPLOYEE_QUERY_TOUR', 1);
define('COMPENSATION_TOUR', 1);
define('APPRAISAL_TOUR', 1);
define('EMPLOYEE_REQUEST_TOUR', 1);
define('ATTENDANCE_TOUR', 1);
define('STAFF_RECORDS_TOUR', 1);
define('RECRUITING_TOUR', 1);
define('PAYROLL_TOUR', 1);
define('EMPLOYEE_PURCHASE_TOUR', 1);
define('GOALS_TOUR', 1);
define('PERSONAL_TOUR', 1);
define('ACCESS_CONTROL_TOUR', 1);
define('DOCUMENT_TOUR', 1);
define('CALENDAR_TOUR', 1);
define('ACCOUNT_SETTING_TOUR', 1);

/*
  |--------------------------------------------------------------------------
  | Billing - SMS
  |--------------------------------------------------------------------------
 */
define('TALENTBASE_SMS_TRANSFER', 1);
define('THIRD-PARTY_SMS_TRANSFER', 2);
define('SMS_CHARACTER_LENGTH', 160);
define('SMS_PER_UNIT_CHARGE', 1.70);

/*
  |--------------------------------------------------------------------------
  | Reward - Types
  |--------------------------------------------------------------------------
 */
define('COMMISSION_REWARD', 1);
define('POINT_REWARD', 2);
define('RECOGNITION_REWARD', 3);
define('CASH_REWARD', 4);
define('REWARD_REPORT_PENDING', 0);
define('REWARD_REPORT_SENT', 1);

define('REWARD_MY_REWARD', 216);
define('EMPLOYEE_REWARDS', 215);
define('REWARD_ALLOCATIONS', 218);

/*
  |--------------------------------------------------------------------------
  | Mac Key - Interswitch web-pay
  |--------------------------------------------------------------------------
 */
define('WEB_PAY_MAC_KEY', '85536FFB95DE D21FABD489E C9AC2301D20 D85019A8F27C BBDF610D1CA C7AC4BD8400 021687CA484D A0E0F5031153 C3B31D1E47A 2D485C91EBF5 C45D6B87BDB 3A');


/*
  |--------------------------------------------------------------------------
  | Disclaimer
  |--------------------------------------------------------------------------
 */
define("DISCLAIMER", "<p><strong><em><font size = 1>Please beware of any generic job interview invitation emails or text messages sent by scammers who may or may not be impersonating TalentBase Nigeria Limited. TalentBase does not condone the condone the practise of said scammers who are in the practice of requesting job applicants to part with some amount of money for the purpose of recruitment. Any valid invitation sent via TalentBase should have the recruiter's contact information as well as refer to the specific job you have applied for. If unsure of the authenticity of any such interview invitation messages purportedly sent by TalentBase, please call 08183377264 to or email support@talentbase.ng to verify.</font></em></strong></p>");

/*
  |--------------------------------------------------------------------------
  |Payment for Billings
  |--------------------------------------------------------------------------
 */
define('OUTSTANDING', 0);
define('PAID', 1);


/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');


/* End of file constants.php */
/* Location: ./application/config/constants.php */

define('TB_ON_SITE_MODE', file_exists(__DIR__ . '/.ONSITE_MODE'));
if (file_exists(APP_ROOT . 'VERSION')) {
    define('TB_VERSION', trim(file_get_contents(APP_ROOT . 'VERSION')));
} else {
    define('TB_VERSION', '1.0.dev');
}

define('EXPIRING_DATE', '2015-05-04');


define('PAGINATION_PER_PAGE', 50);

define('PAYMENT_STATUS_PENDING', 0);
define('PAYMENT_STATUS_DONE', 1);
define('PAYMENT_STATUS_QUEUED', 1 << 1);
define('PAYMENT_STATUS_FAILED', 1 << 2);
define('PAYMENT_STATUS_CANCELED', 1 << 3);
define('PAYMENT_STATUS_ONGOING', 1 << 4);

