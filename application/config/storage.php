<?php

/**
 * Add storage configuration here
 */
if (!isset($config)) {
    $config = [];
}

$config['storage'] = array(
    /**
     * Adapter specifies the adapter to use:
     * - cloudfiles for rackspace
     * - amazons3 for AWS Simple Storage Service (S3)
     * - localstorage for local file storage
     */
    'adapter' => 'cloudfiles',
    /**
     * Temporary directory where files can be stored, this directory should be cleaned often
     */
    'tmp_dir' => '',
    /**
     * Rackspace Username and API Key
     */
    'cloudfiles' => array(
        'username' => '',
        'api_key' => ''
    ),
    /**
     * LocalStorage config
     */
    'localstorage' => array(
        /**
         * Storage directory Local storage path required for local storage
         */
        'dir' => ''
    ),
    'amazons3' => array(
        /**
         * S3 Bucket, storage bucket where files are stored by default on S3
         * required for amazons3
         */
        'bucket' => '',
        /**
         * AWS Access Key, required
         */
        'access_key' => '',
        /**
         * AWS Access Secret
         */
        'access_secret' => '',
    )
);
