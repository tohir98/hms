<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DateTimezoneSetter
 *
 * @author JosephT
 */
class DateTimezoneSetter {

    //put your code here
    private $timezone;

    public function __construct() {
        $this->timezone = config_item('timezone');
    }

    public function setAppTimezone() {
        if ($this->timezone) {
            date_default_timezone_set($this->timezone);
        }
    }
    
    public function setDbTimezone(){
        
        if($this->timezone && ($CI = get_instance())){
            $CI->load->database();
            $db = $CI->db;
            /*@var $db CI_DB_active_record*/
            if ($this->timezone) {
                $offset = timezone_offset_get(new DateTimeZone($this->timezone), new DateTime());
                $tz_offset = sprintf("%s%02d:%02d", ( $offset >= 0 ) ? '+' : '-', abs($offset / 3600), abs($offset % 3600));
                $db->query("SET time_zone = '{$tz_offset}'");
            }
        }
    }

}
