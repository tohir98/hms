<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Talentbase
 * Vacations Model
 * 
 * @category   Model
 * @package    Vacation
 * @subpackage Vacation
 * @author     Obinna Ukpabi <obinna.ukpabi@talentbase.ng>
 * @author     Tunmise Akinsola <tunmise.akinsola@talentbase.ng>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

class Vacation_model extends CI_Model {
	
    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();

    }// End func __construct

    /*
     * Get company holidays
     * 
     * @access public 
     * @param  $id_company
     * @return mixed (bool | array)
     */
    public function get_holidays($id_company, $year = null){

        if(!is_numeric($id_company)){
            return false;
        }

        if(empty($year)){
            $year = date('Y');
        }

        $year_begin = date('Y-m-d', strtotime("1 january $year"));
        $year_end = date('Y-m-d', strtotime("31 december $year"));

        $query = $this->db->get_where('vacation_holidays', array('id_company' => $id_company, 'holiday_date >'=>$year_begin, 'holiday_date <'=>$year_end));

        $c = $this->get_company_location($id_company);
        
        if($query->num_rows() > 0) {
            return $query->result();
        } else {
            if($year == date('Y')){
                $locate = '';
                foreach ($c as $cl) {
                    $locate.= $cl->id_location.',';
                }

                $locate = rtrim($locate, ',');
                $insert_data = array(

                    array(
                        'id_company' => $id_company,
                        'holiday_name' => 'New Year',
                        'holiday_date' => date('Y-m-d', strtotime('1 January')),
                        'annual' => 'Yes',
                        'company_locations' => $locate
                    ),
                    array(
                        'id_company' => $id_company,
                        'holiday_name' => 'id el maulud',
                        'holiday_date' => date('Y-m-d', strtotime('2 January')),
                        'annual' => 'Yes',
                        'company_locations' => $locate
                    ),
                    array(
                        'id_company' => $id_company,
                        'holiday_name' => 'Good Friday',
                        'holiday_date' => date('Y-m-d', strtotime('3 April')),
                        'annual' => 'Yes',
                        'company_locations' => $locate
                    ),
                    array(
                        'id_company' => $id_company,
                        'holiday_name' => 'Easter Monday',
                        'holiday_date' => date('Y-m-d', strtotime('6 April')),
                        'annual' => 'Yes',
                        'company_locations' => $locate
                    ),
                    array(
                        'id_company' => $id_company,
                        'holiday_name' => 'May Day',
                        'holiday_date' => date('Y-m-d', strtotime('1 May')),
                        'annual' => 'Yes',
                        'company_locations' => $locate
                    ),
                    array(
                        'id_company' => $id_company,
                        'holiday_name' => 'Democracy Day',
                        'holiday_date' => date('Y-m-d', strtotime('29 May')),
                        'annual' => 'Yes',
                        'company_locations' => $locate
                    ),
                    array(
                        'id_company' => $id_company,
                        'holiday_name' => 'Independence Day',
                        'holiday_date' => date('Y-m-d', strtotime('1 October')),
                        'annual' => 'Yes',
                        'company_locations' => $locate
                    ),
                    array(
                        'id_company' => $id_company,
                        'holiday_name' => 'Christmas Day',
                        'holiday_date' => date('Y-m-d', strtotime('25 December')),
                        'annual' => 'Yes',
                        'company_locations' => $locate
                    ),
                    array(
                        'id_company' => $id_company,
                        'holiday_name' => 'Boxing Day',
                        'holiday_date' => date('Y-m-d', strtotime('26 December')),
                        'annual' => 'Yes',
                        'company_locations' => $locate
                    ),
                    array(
                        'id_company' => $id_company,
                        'holiday_name' => 'id el fitr',
                        'holiday_date' => date('Y-m-d', strtotime('18 July')),
                        'annual' => 'Yes',
                        'company_locations' => $locate
                    ),
                    array(
                        'id_company' => $id_company,
                        'holiday_name' => 'id el kabil',
                        'holiday_date' => date('Y-m-d', strtotime('24 September')),
                        'annual' => 'Yes',
                        'company_locations' => $locate
                    )
                );

                $this->db->insert_batch('vacation_holidays', $insert_data); 
            }
        }

        
        return $this->db->get_where('vacation_holidays', array('id_company' => $id_company))->result();

    }// End func get_holidays



    /*
     * Get company holidays
     * 
     * @access public 
     * @param  $id_company
     * @return mixed (bool | array)
     */
    public function get_holiday_list($id_company, $year_begin, $year_end){

        if(!is_numeric($id_company)){
            return false;
        }
        
        $this->db->where('id_company', $id_company);
        $this->db->where('holiday_date >', $year_begin);
        $this->db->where('holiday_date <', $year_end);
        $this->db->group_by('holiday_date');
        $this->db->order_by('MONTH(holiday_date)', 'desc');
        $q = $this->db->get('vacation_holidays');
        return $q->result();

    }// End func get_holidays


     /*
     * Get company location
     * 
     * @access public
     * @param  arr
     * @return bool
     */

   public function get_company_location($id_company)
   {
      $this->db->where('id_company', $id_company);
      $this->db->order_by('id_location', 'desc');
      $query = $this->db->get('company_locations');
      return $query->result();
   }


    /*
     * Push Task 
     * 
     * @access public
     * @param  arr
     * @return bool
     */

   public function update_task($data, $id_task)
   {
      $query = $this->db->update('tasks', $data, array('id_task' => $id_task));
      return $query;
   }

    /*
     * Create a new holiday
     * 
     * @access public 
     * @param  $id_company
     * @return mixed (bool | array)
     */
    public function create_holiday($id_company, $params){

        if(!is_numeric($id_company)){
            return false;
        }

        $res = $this->db->insert('vacation_holidays', $params);

        if($res > 0 ) {
            return true;
        }

        return false;

    }// End func create_holidays

    /*
     * Deletes an holiday
     * 
     * @access public 
     * @param  $id_company
     * @return mixed (bool | array)
     */
    public function delete_holiday($params){

        if(empty($params)){
            return false;
        }

        $res = $this->db->delete('vacation_holidays', $params);

        if($res > 0 ) {
            return true;
        }

        return false;

    }// End func delete_holidays

    /*
     * Edit an holiday
     * 
     * @access public 
     * @param  $id_holiday
     * @return mixed (bool | array)
     */
    public function update_holiday($id_holiday, $params){

        if(!is_numeric($id_holiday)){
            return false;
        }

        $res = $this->db->update('vacation_holidays', $params, array('id_holiday' => $id_holiday));

        if($res > 0 ) {
            return true;
        }

        return false;

    }// End func edit_holidays

    /*
     * Get users' permission for a particular module
     * NOTE: to be removed!!!
     * @access public 
     * @param $params
     * @return mixed (bool | array)
     */
    public function get_user_module_perms($params) {

        if(empty($params)){
            return false;
        }

        $result = $this->db->get_where("user_perms", $params)->result_array();

        if(empty($result)){
            return false;
        }

        return $result;
    }

    /*
     * Get company location
     * 
     * @access public 
     * @param $id_company
     * @return mixed (bool | array)
     */
    public function get_company_locations($id_company){

        if(!is_numeric($id_company)){
            return false;
        }

        return $this->db->get_where('company_locations', array('id_company' => $id_company))->result();

    }// End func get_company_locations

    /*
     * Add new vacation request task
     * 
     * @access public 
     * @param  $id_company, $params
     * @return mixed (bool | array)
     */
    public function add_task($task){

        if(!$task){
            return false;
        }

        $q = $this->db->insert('tasks', $task);

        if ($q){
            return $this->db->insert_id();
        }

        


    }// End func vacation_request

    /*
     * Create a new vacation request
     * 
     * @access public 
     * @param  $id_company, $params
     * @return mixed (bool | array)
     */
    public function vacation_request($params){//, $task

        // if(!is_numeric($id_company)){
        //     return false;
        // }

        //$this->db->trans_start();
        //$this->db->insert('tasks', $task);
        //$id_task = $this->db->insert_id();
        //$params['id_task'] = '';
        $q = $this->db->insert('vacation', $params);
        //$this->db->trans_complete();
        //$this->db->trans_status() === FALSE

        if ($q === false){
            return false;
        }

        return $this->db->insert_id();


    }// End func vacation_request
    
    /*
     * Delete a vacation request
     * 
     * @access public 
     * @param  $params
     * @return mixed (bool | array)
     */
    public function delete_vacation_request($params) {
        
        if(empty($params)){
            return false;
        }

        $res = $this->db->delete('vacation', $params);

        if($res > 0 ) {
            return true;
        }

        return false;    
        
    }// End func delete_vacation_request
    
    /*
     * Edit an holiday
     * 
     * @access public 
     * @param  $id_vacation, $params
     * @return mixed (bool | array)
     */
    public function update_vacation_request($id_vacation, $params){

        if(!is_numeric($id_vacation)){
            return false;
        }

        $res = $this->db->update('vacation', $params, array('id_vacation' => $id_vacation));

        if($res > 0 ) {
            return true;
        }

        return false;

    }// End func update_vacation_request
    
    /*
     * Approve a vacation request
     * 
     * @access public 
     * @param  $id_vacation, $params
     * @return mixed (bool | array)
     */
    public function vacation_request_approve($id_vacation, $params) {
        if(!is_numeric($id_vacation)){
            return false;
        }

        $res = $this->db->update('vacation', $params, array('id_vacation' => $id_vacation));

        if($res > 0 ) {
            return true;
        }

        return false;
    }// End func vacation_request_approve
    
    /*
     * Decline a vacation request
     * 
     * @access public 
     * @param  $id_vacation, $params
     * @return mixed (bool | array)
     */
    public function vacation_request_decline($id_vacation, $params) {
        if(!is_numeric($id_vacation)){
            return false;
        }

        $res = $this->db->update('vacation', $params, array('id_vacation' => $id_vacation));

        if($res > 0 ) {
            return true;
        }

        return false;
    }// End func vacation_request_decline
    
    /*
     * Get company holidays
     * 
     * @access public 
     * @param  $id_company
     * @return mixed (bool | array)
     */
    public function get_vacation_types($id_company){

        if(!is_numeric($id_company)){
            return false;
        }

        $query = $this->db->get_where('vacation_types', array('id_company' => $id_company));

        if($query->num_rows() > 0) {
            return $query->result();
        } else {

            $insert_data = array(
                array(
                    'name' => 'Annual',
                    'carry_allowed' => '0',
                    'description' => 'General leave/vacation',
                    'id_company' => $id_company
                ),
                array(
                    'name' => 'Medical',
                    'carry_allowed' => '0',
                    'description' => 'Vacation for medical issues e.g. Medical checks',
                    'id_company' => $id_company
                ),
                array(
                    'name' => 'Emergency',
                    'carry_allowed' => '0',
                    'description' => 'Time off for inpromptum emergencies',
                    'id_company' => $id_company
                ),
                array(
                    'name' => 'Maternity',
                    'carry_allowed' => '0',
                    'description' => 'Time off for expectant mothers e.g. Pregancy, ',
                    'id_company' => $id_company
                )
            );

            $this->db->insert_batch('vacation_types', $insert_data); 
        }

        return $this->db->get_where('vacation_types', array('id_company' => $id_company))->result();

    }// End func get_vacation_types

    /*
     * Get employee vacation history
     * 
     * @access public 
     * @param  $id_user
     * @return mixed (bool | array)
     */
    public function get_employee_vacation_history($id_user){

        if(!is_numeric($id_user)){
            return false;
        }

        $this->db->select('vt.name, v.*');
        $this->db->from('vacation_types vt');
        $this->db->join('vacation v', 'v.id_type = vt.id_type');
        $this->db->where('id_user', $id_user);
        return $this->db->get()->result();

    }// End func get_employee_vacation_history


    /*
     * Get Company vacation history
     * 
     * @access public 
     * @param  $id_user
     * @return mixed (bool | array)
     */
    public function get_company_vacation_history($id_company){

        if(!is_numeric($id_company)){
            return false;
        }

        $this->db->select('vt.name, v.*, u.first_name, u.last_name');
        $this->db->from('vacation_types vt');
        
        $this->db->join('vacation v', 'v.id_type = vt.id_type');
        $this->db->join('users u', 'u.id_user = v.id_user');
        $this->db->where('v.id_company', $id_company);
        $this->db->order_by('v.id_vacation', 'desc');
        return $this->db->get()->result();

    }// End func get_employee_vacation_history


    /*
     * Get manager vacation history
     * 
     * @access public 
     * @param  $id_user
     * @return mixed (bool | array)
     */
    public function get_manager_vacation_history($id_company, $id_manager){

        if(!is_numeric($id_company) OR !is_numeric($id_manager)){
            return false;
        }

        $this->db->select('vt.name, v.*, u.first_name, u.last_name, e.id_manager, e.id_manager_vacation');
        $this->db->from('vacation_types vt');
        
        $this->db->join('vacation v', 'v.id_type = vt.id_type');
        $this->db->join('users u', 'u.id_user = v.id_user');
        $this->db->join('employees e', 'e.id_user = v.id_user');
        $this->db->where('v.id_company', $id_company);
        $this->db->where('e.id_manager', $id_manager);
        $this->db->or_where('e.id_manager_vacation', $id_manager);
        $this->db->order_by('v.id_vacation', 'desc');
        return $this->db->get()->result();

    }// End func get_employee_vacation_history



    /*
     * Get employee pending vacation request
     * 
     * @access public 
     * @param  $id_user
     * @return mixed (bool | array)
     */
    public function get_employee_pending_vacation($id_user){

        if(!is_numeric($id_user)){
            return false;
        }

        $this->db->select('vt.name, v.*, r.first_name AS relief_first_name, r.last_name AS relief_last_name');
        $this->db->from('vacation_types vt');
        $this->db->join('vacation v', 'v.id_type = vt.id_type');
        $this->db->join('users r', 'r.id_user = v.id_relief', 'left');
        $this->db->where('v.status', 'Pending');
        $this->db->where('v.id_user', $id_user);
        return $this->db->get()->result();

    }// End func get_employee_pending_vacation

    /*
     * Get vacation types allocated to employee
     * 
     * @access public 
     * @param  $id_user
     * @return mixed (bool | array)
     */
    public function get_employee_vacation_types($id_user){

        if(!is_numeric($id_user)){
            return false;
        }

        $this->db->select('vt.id_type, name, no_of_days, id_user');
        $this->db->from('vacation_types vt');
        $this->db->join('vacation_allocated va', 'va.id_type = vt.id_type');
        $this->db->where('id_user', $id_user);
        return $this->db->get()->result();

    }// End func get_employee_vacation_types

    /*
     * Get employee vacation entitlements
     * 
     * @access public 
     * @param  $id_user
     * @return mixed (bool | array)
     */
    public function get_employee_entitlements($id_user, $year = null){

        if(!is_numeric($id_user)){
            return false;
        }

        $year = (is_numeric($year)) ? $year : date('Y') ;

        $year_begin = date('Y-m-d', strtotime("1 january $year"));
        $year_end = date('Y-m-d', strtotime("31 december $year"));

        $this->db->select('va.id_allocation, vt.id_type, vt.name, va.no_of_days, va.date_start');
        $this->db->from('vacation_types vt');
        $this->db->join('vacation_allocated va', 'va.id_type = vt.id_type');
        $this->db->where('va.id_user', $id_user);
        $this->db->where('va.date_start >', $year_begin);
        $this->db->where('va.date_start <', $year_end);
        return $this->db->get()->result();

    }// End func get_employee_entitlements


    /*
     * Get employee vacation allowance request
     * 
     * @access public 
     * @param  $id_user
     * @return mixed (bool | array)
     */
    public function get_employee_vacation_allowance_request($id_company, $id_approver){

        if(!is_numeric($id_company)){
            return false;
        }

        $this->db->select('vp.id_allowance_approver, vp.id_vacation_allowance_request, vp.id_approver, vp.id_allowance_stage, vp.status AS vpstatus, va.*, vs.id_allowance_stage, u.first_name, u.last_name, e.id_role, e.id_dept, e.id_manager, r.roles, d.departments, ce.*');
        $this->db->from('vacation_allowance_approver vp');
        $this->db->join('vacation_allowance_request va', 'va.id_vacation_allowance_request = vp.id_vacation_allowance_request');
        $this->db->join('vacation_allowance_stages vs', 'vs.id_allowance_stage = vp.id_allowance_stage');
        $this->db->join('users u', 'va.id_user = u.id_user');
        $this->db->join('employees e', 'va.id_user = e.id_user');
        $this->db->join('roles r', 'e.id_role = r.id_role');
        $this->db->join('vacation_allowance ce', 'va.id_user = ce.id_user');
        //$this->db->join('compensation_employee_leave_allowance la', 'ce.id_pay_structure = la.id_pay_structure');
        $this->db->join('departments d', 'e.id_dept = d.deptid');
        $this->db->where('va.id_company', $id_company);
        $this->db->where('va.status', 1);
        $this->db->where('vp.id_approver', $id_approver);
        return $this->db->get()->result();

    }// End func get_employee_entitlements


    /*
     * Update employee vacation allowance request
     * 
     * @access public 
     * @param  $id_user
     * @return mixed (bool | array)
     */
    public function update_allowance_request($id_allowance, $data){

        if(!is_numeric($id_allowance)){
            return false;
        }

        $this->db->where('id_vacation_allowance_request', $id_allowance);
        $q = $this->db->update('vacation_allowance_request', $data);
        return $q;

    }// End func get_employee_entitlements


    /*
     * delete employee vacation allowance request
     * 
     * @access public 
     * @param  $id_user
     * @return mixed (bool | array)
     */
    public function delete_allowance_request($id_allowance){

        if(!is_numeric($id_allowance)){
            return false;
        }

        $this->db->where('id_vacation_allowance_request', $id_allowance);
        $q = $this->db->delete('vacation_allowance_request');
        return $q;

    }// End func get_employee_entitlements


     /*
     * delete employee vacation allowance request
     * 
     * @access public 
     * @param  $id_user
     * @return mixed (bool | array)
     *

     public function clear_allowance_request($id_company)
     {
        
        //select all request by company
        $req = $this->get('vacation_allowance_request', array('id_company'=>$id_company));

        foreach ($req as $r) {
            
            $usr = $this->v_model->get_employee_info($id_company, $r->id_user);
          
            $this->load->library('sms_lib');
            $company_name =$this->user_auth->get('company_id_string');
            $message = "Hi, {$usr->first_name} {$usr->last_name} Your vacation allowance request has been cancelled. "
                    . "Kindly log into the employee portal at {$company_name}.talentbase.ng to make another request.";
            $sms_params['id_module'] = '';
            $sms_params['message'] = $message;
            $sms_params['recipients']['id_user'] = $usr->id_user;
            $sms_params['recipients']['number'] = $usr->work_number;
            $res = $this->sms_lib->send_sms($sms_params);

            $k = array(
                'first_name' => $usr->first_name,
                'last_name' => $usr->last_name,
                'company_name' => $this->user_auth->get('company_name'),
                'subdomain' => $this->user_auth->get('company_id_string'),
                'department' => $usr->departments,
                'role' => $usr->roles,
                'location' => $usr->location_tag
                );

            $subject = 'Vacation Request Cancelled';
            $msg = $this->load->view('email_templates/re_vacation_allowance_request', $k, true);
            $this->load->library('mailgun_api');
            $this->mailgun_api->send_mail(
                                    array(
                                        'from' => 'TalentBase <noreply@testbase.com.ng>',
                                        'to' => $usr->email,
                                        'subject' => $subject,
                                        'html' => '<html>'. $msg .'</html>'
                                    ));
        }
        
        $q = $this->delete('vacation_allowance_request', array('id_company'=>$id_company));
        
        if($q){
            return true;
        }
     }*/



     /*
     * delete employee vacation allowance request
     * 
     * @access public 
     * @param  $id_user
     * @return mixed (bool | array)
     *

     public function clear_vacation_request($id_company)
     {
        
        //select all request by company
        $req = $this->get('vacation', array('id_company'=>$id_company, 'status'=>'Pending'));
        foreach ($req as $r) {
            
            $q = $this->delete('vacation', array('id_vacation'=>$r->id_vacation, 'status'=>1));
            if($q){
                $usr = $this->v_model->get_employee_info($id_company, $r->id_user);
                
                $this->load->library('sms_lib');
                $company_name =$this->user_auth->get('company_name');
                $message = "Your vacation allowance request has been cancelled. "
                        . "Kindly log into the employee portal at {$company_name}.talentbase.ng to make another request.";
                $sms_params['id_module'] = '';
                $sms_params['message'] = $message;
                $sms_params['recipients']['id_user'] = $usr->id_user;
                $sms_params['recipients']['number'] = $usr->work_number;
                $res = $this->sms_lib->send_sms($sms_params);

                $k = array(
                'first_name' => $usr->first_name,
                'last_name' => $usr->last_name,
                'company_name' => $this->user_auth->get('company_name'),
                'subdomain' => $this->user_auth->get('company_id_string'),
                'department' => $usr->departments,
                'role' => $usr->roles,
                'location' => $usr->location_tag
                );

                $subject = 'Vaction request cleared';
                $msg = $this->load->view('email_templates/re_vacation_allowance_request', $k, true);
                $this->load->library('mailgun_api');
                $this->mailgun_api->send_mail(
                                        array(
                                            'from' => 'TalentBase <noreply@testbase.com.ng>',
                                            'to' => $usr->email,
                                            'subject' => $subject,
                                            'html' => '<html>'. $msg .'</html>'
                                        ));
            }
        }

        return true;
     }*/





    /*
     * Get employee requested vacation
     * 
     * @access public 
     * @param  $id_user
     * @return mixed (bool | array)
     */
    public function get_taken_vacations($id_user){

        if(!is_numeric($id_user)){
            return false;
        }

        $this->db->select('id_type, date_start, date_end, status');
        $this->db->from('vacation');
        $this->db->where('id_user', $id_user);
        $query = $this->db->get()->result_array();
        $ret = array();
        foreach($query as $q) {
            $ret[$q['id_type']][] = array($q['date_start'], $q['date_end'], $q['status']);
        }
        
        return $ret;

    }// End func get_taken_vacations

    
    /*
     * Get details about a vacation
     * 
     * @access public 
     * @param  $id_vacation
     * @return Object 
     */
    public function get_vacation_details($id_vacation) {
        $this->db->select('*');
        $this->db->from('vacation v');
        $this->db->join('employees e', 'e.id_user = v.id_user');
        $this->db->where('v.id_vacation', $id_vacation);
        return $this->db->get()->row();
    }// End func get_vacation_details


    /*
     * Get details about a vacation
     * 
     * @access public 
     * @param  $id_vacation
     * @return Object 
     */
    public function get_vacation_manager($id_user) {
        $this->db->select('id_manager, id_manager_vacation');
        $this->db->from('employees');
        $this->db->where('id_user', $id_user);
        return $this->db->get()->row();
    }// End func get_vacation_manager

    
    /*
     * Get an employee's manager
     * 
     * @access public 
     * @param  $id_user
     * @return Object 
     */
    public function get_employee_manager($id_user) {
        $em = $this->get_employee_details($id_user);
        $manager = $em->id_manager_vacation;
        $this->db->select('*');
        $this->db->from('employees e');
        $this->db->join('users u', 'u.id_user = e.id_user');
        $this->db->where('e.id_user', $manager);
        return $this->db->get()->row();
    }// End func get_employee_manager
    
     /*
     * Create a new vacation type
     * 
     * @access public 
     * @param  $id_company
     * @return mixed (bool | array)
     */
    public function create_vacation_type($id_company, $params){

        if(!is_numeric($id_company)){
            return false;
        }

        $res = $this->db->insert('vacation_types', $params);

        if($res > 0 ) {
            return true;
        }

        return false;

    }// End func create_vacation_type

    /*
     * Deletes a vacation type
     * 
     * @access public 
     * @param  $params
     * @return mixed (bool | array)
     */
    public function delete_vacation_type($params){

        if(empty($params)){
            return false;
        }

        $res = $this->db->delete('vacation_types', $params);

        if($res > 0 ) {
            return true;
        }

        return false;

    }// End func delete_vacation_type

    /*
     * Edit a vacation type
     * 
     * @access public 
     * @param  $id_type, $params
     * @return mixed (bool | array)
     */
    public function update_vacation_type($id_type, $params){

        if(!is_numeric($id_type)){
            return false;
        }

        $res = $this->db->update('vacation_types', $params, array('id_type' => $id_type));

        if($res > 0 ) {
            return true;
        }

        return false;

    }// End func edit_vacation_type

    /*
     * Allocate vacation to employees
     * 
     * @access public 
     * @param  $id_type, $params
     * @return mixed (bool | array)
     */
    public function allocate_vacation($params){

        if(empty($params)){
            return false;
        }

        $res = $this->db->insert('vacation_allocated', $params);        
        return $res;
        

    }// End func allocate_vacation



    /*
     * Check if an employee has been allocation a certain vacationn ttype
     * 
     * @access private
     * @param  $id_user, $id_type
     * @return bool 
     */
    public function get_managers($id_company){

        if(!is_numeric($id_company)){
            return false;
        }

        $this->sql = "SELECT e.id_user, e.is_manager, u.first_name, u.last_name, u.id_user "
                    . "FROM employees e, users u "
                    . "WHERE u.id_user = e.id_user "
                    . "AND e.is_manager = ? "
                    . "AND e.id_company = ? ";
        
        $query = $this->db->query($this->sql, array(1, $id_company));

        // $this->db->select('e.id_user, e.is_manager, u.first_name, u.last_name, u.id_user');
        // $this->db->from('employees e');
        // $this->db->where('e.is_manager  ', $Value);
        return $query->result();

    }
    
    /*
     * Check if an employee has been allocation a certain vacationn ttype
     * 
     * @access private
     * @param  $id_user, $id_type
     * @return bool 
     */
    public function check_employee_allocated_vacation($id_user, $id_type){
        
        if(!is_numeric($id_user)){
            return false;
        }

        $query = $this->db->get_where('vacation_allocated', array('id_user' => $id_user, 'id_type' => $id_type));
        
        if($query->num_rows() > 0) {
            return true;
        }

        return false;

    }// End func check_employee_allocated_vacation


    /*
     * Get employee specific vacation allocation 
     * 
     * @access public
     * @param  $id_user, $id_type
     * @return bool 
     */
    public function get_employee_allocated_vacation($id_user, $id_type){
        
        if(!is_numeric($id_user)){
            return false;
        }

        $this->db->select('v.*, vt.*');
        $this->db->from('vacation_allocated v');
        $this->db->join('vacation_types vt', 'v.id_type = vt.id_type');
        $this->db->where('v.id_user', $id_user);
        $this->db->where('v.id_type', $id_type);
        $query = $this->db->get('vacation_allocated');

        //$query = $this->db->get_where('vacation_allocated', array('id_user' => $id_user, 'id_type' => $id_type));
        return $query->row();


    }// End func get_employee_allocated_vacation
    
    /*
     * Allocate vacation to employees
     * 
     * @access public 
     * @param  $params
     * @return mixed (bool | array)
     */
    public function delete_allocation($params){

        if(empty($params)){
            return false;
        }

        $res = $this->db->delete('vacation_allocated', $params);

        if($res > 0 ) {
            return true;
        }

        return false;

    }// End func delete_allocation
    
    /*
     * Update a vacation allocation
     * 
     * @access public 
     * @param  $params
     * @return mixed (bool | array)
     */
    public function update_allocation($id_allocation, $params){

        if(!is_numeric($id_allocation)){
            return false;
        }

        $res = $this->db->update('vacation_allocated', $params, array('id_allocation' => $id_allocation));

        if($res > 0 ) {
            return true;
        }

        return false;

    }// End func update_allocation


    /*
     * Update a vacation allocation
     * 
     * @access public 
     * @param  $params
     * @return mixed (bool | array)
     */
    public function return_allocation($id_type, $id_user, $params){

        if(!is_numeric($id_allocation)){
            return false;
        }
        $where = array(
            'id_type' => $id_type,
            'id_user' => $id_user
            );
        $res = $this->db->update('vacation_allocated', $params, $where);

        if($res > 0 ) {
            return true;
        }

        return false;

    }// End func update_allocation


    /*
     * Update a vacation allocation
     * 
     * @access public 
     * @param  $params
     * @return mixed (bool | array)
     */
    public function update_vacation($id_vacation, $params){

        if(!is_numeric($id_vacation)){
            return false;
        }

        $res = $this->db->update('vacation', $params, array('id_vacation' => $id_vacation));

        if($res > 0 ) {
            return true;
        }

        return false;

    }// End func update_allocation
    
    /*
     * Get allocated vacations for a particular company
     * 
     * @access public 
     * @param  $id_company
     * @return mixed (bool | array)
     */
    public function get_allocated_vacations($id_company) {
        if(!is_numeric($id_company)){
            return false;
        }
        
        $this->db->select('v.id_allocation, v.no_of_days, v.recurring, v.date_start, v.date_end, vt.description, vt.name, u.id_user, u.first_name, u.last_name');
        $this->db->from('vacation_allocated v');
        $this->db->join('users u', 'u.id_user = v.id_user');        
        $this->db->join('vacation_types vt', 'v.id_type = vt.id_type');
        $this->db->where('v.id_company', $id_company);
        return $this->db->get()->result();
    }

    /*
     * Get allocated vacations sum
     * 
     * @access public 
     * @param  $id_company
     * @return mixed (bool | array)
     */

    public function get_vacation_count($id_company)
    {
        $this->db->select('id_company, id_user, no_of_days');
        $this->db->select_sum('no_of_days');
        $this->db->where('id_company', $id_company);
        $this->db->group_by('id_user');
        $q = $this->db->get('vacation_allocated');
        return $q->result();
    }

    /*
     * Update a approver for specified user
     * 
     * @access public 
     * @param  $params
     * @return mixed (bool | array)
     */
    public function update_approver($id_user, $params){

        if(!is_numeric($id_user)){
            return false;
        }

        $res = $this->db->update('vacation_allocated', $params, array('id_user' => $id_user));
        $params2 = array('id_manager_vacation' => $params['approver']);
        $this->db->update('employees', $params2, array('id_user' => $id_user));

        if($res > 0 ) {
            return true;
        }

        return false;

    }// End func update_allocation
    
    /*
     * Get employees of a company
     * 
     * @access public 
     * @param  $id_type, $params
     * @return mixed (bool | array)
     */
    public function get_employees($id_company){

        if(!is_numeric($id_company)){
            return false;
        }

        $this->sql = "SELECT e.*, u.*, CONCAT_WS(' ', m.first_name, m.last_name) AS manager "
                    . "FROM employees e, users u, users m "
                    . "WHERE u.id_user = e.id_user "
                    . "AND e.id_manager_vacation = m.id_user "
                    . "AND e.id_company = ? ";
        
        $query = $this->db->query($this->sql, array($id_company));
        return $query->result();


    }// End func get_employees


    /*
     * Get employees info of a company
     * 
     * @access public 
     * @param  $id_type, $params
     * @return mixed (bool | array)
     */
    public function get_employees_info($id_company){

        if(!is_numeric($id_company)){
            return false;
        }

        $this->sql = "SELECT e.id_dept, e.id_role, e.id_user, e.id_manager, l.location_tag, r.id_role, r.roles, u.id_string, u.id_company_location, u.status, 
                    u.first_name, u.last_name, d.departments, CONCAT_WS(' ', m.first_name, m.last_name) AS manager "
                    . "FROM employees e "
                    . "LEFT JOIN users u on u.id_user = e.id_user "
                    . "LEFT JOIN users m on e.id_manager_vacation = m.id_user "
                    . "LEFT JOIN departments d on e.id_dept = d.deptid "
                    . "LEFT JOIN roles r on e.id_role = r.id_role "
                    . "LEFT JOIN company_locations l on u.id_company_location = l.id_location "
                    // . "LEFT JOIN compensation_employees ce on ce.id_user = u.id_user "
                    // . "LEFT JOIN compensation_employee_leave_allowance la on ce.id_compensation_employee = la.id_compensation_employee "
                    . "WHERE e.id_company = ? "
                    . "AND  u.status != ? AND u.status != ? ";
        
        $query = $this->db->query($this->sql, array($id_company, USER_STATUS_INACTIVE, USER_STATUS_TERMINATED));
        return $query->result();


    }// End func get_employees


    /*
     * Get employees info of a company
     * 
     * @access public 
     * @param  $id_type, $params
     * @return mixed (bool | array)
     */
    public function get_employee_info($id_company, $id_user){

        if(!is_numeric($id_company)){
            return false;
        }

        $this->sql = "SELECT e.id_dept, e.id_role, e.id_user, e.id_manager, e.id_country_code, e.work_number, l.location_tag, r.id_role, r.roles, u.email, u.id_string, u.id_company_location, u.status, 
                    u.first_name, u.last_name, d.departments, pc.id_payroll_paygrade "
                    . "FROM employees e "
                    . "LEFT JOIN users u on u.id_user = e.id_user "
                    // . "LEFT JOIN users m on e.id_manager_vacation = m.id_user "
                    . "LEFT JOIN departments d on e.id_dept = d.deptid "
                    . "LEFT JOIN roles r on e.id_role = r.id_role "
                    . "LEFT JOIN company_locations l on u.id_company_location = l.id_location "
                    . "LEFT JOIN payroll_compensation pc on u.id_user = pc.id_user "
                    //. "LEFT JOIN compensation_employee_leave_allowance la on ce.id_compensation_employee = la.id_compensation_employee "
                    . "WHERE e.id_company = ? "
                    . "AND e.id_user = ? ";
        
        $query = $this->db->query($this->sql, array($id_company, $id_user));
        return $query->row();


    }// End func get_employees



     /*
     * Get employees info of a company
     * 
     * @access public 
     * @param  $id_type, $params
     * @return mixed (bool | array)
     */
    public function get_manager_employees_info($id_company, $id_user){

        if(!is_numeric($id_company)){
            return false;
        }

        $this->sql = "SELECT e.id_dept, e.id_role, e.id_user, e.id_manager, e.id_country_code, e.work_number, l.location_tag, r.id_role, r.roles, u.email, u.id_string, u.id_company_location, u.status, 
                    u.first_name, u.last_name, d.departments "
                    . "FROM employees e "
                    . "LEFT JOIN users u on u.id_user = e.id_user "
                    . "LEFT JOIN departments d on e.id_dept = d.deptid "
                    . "LEFT JOIN roles r on e.id_role = r.id_role "
                    . "LEFT JOIN company_locations l on u.id_company_location = l.id_location "
                    // . "LEFT JOIN compensation_employees ce on u.id_user = ce.id_user "
                    // . "LEFT JOIN compensation_employee_leave_allowance la on ce.id_compensation_employee = la.id_compensation_employee "
                    . "WHERE e.id_company = ? "
                    . "AND e.id_manager = ? "
                    . "AND  u.status != ? AND u.status != ? ";
        
        $query = $this->db->query($this->sql, array($id_company, $id_user, USER_STATUS_INACTIVE, USER_STATUS_TERMINATED));
        return $query->result();


    }// End func get_employees



    /*
     * Get vacation allowance stages
     * 
     * @access public 
     * @param  $id_company
     * @return mixed (bool | array)
     */
    public function get_allowance_stages($id_company) {

         if(!is_numeric($id_company)){
            return false;
        }

        $this->sql = "SELECT vs.*,  u.first_name, u.last_name "
                    . "FROM vacation_allowance_stages vs "
                    . "LEFT JOIN users u on u.id_user = vs.id_user "
                    . "WHERE vs.id_company = ? "
                    . "ORDER BY vs.stage asc";
        
        $query = $this->db->query($this->sql, array($id_company));
        return $query->result();

    }


     /*
     * Get vacation allowance stages
     * 
     * @access public 
     * @param  $id_company
     * @return mixed (bool | array)
     */
    public function get_only_employees($id_company) {

         if(!is_numeric($id_company)){
            return false;
        }

        $this->db->select('e.id_user, u.first_name, u.last_name');
        $this->db->from('employees e');
        $this->db->join('users u', 'u.id_user = e.id_user', 'left');
        $this->db->where('e.id_company', $id_company);
        $this->db->where('u.status', USER_STATUS_ACTIVE OR USER_STATUS_PROBATION_ACCESS);
        //$this->db->or_where('u.status', );
        $query = $this->db->get();
        return $query->result();

    }


     /*
     * Get last stage
     * 
     * @access public 
     * @param  $id_company
     * @return mixed (bool | array)
     */
     public function get_last_stage($id_company)
     {
         $this->db->where('id_company', $id_company);
         $this->db->order_by('id_allowance_stage', 'desc');
         $q = $this->db->get('vacation_allowance_stages');
         return $q->num_rows();

     }


     /*
     * Get last stage
     * 
     * @access public 
     * @param  $id_company
     * @return mixed (bool | array)
     */
     public function get_rlast_stage($id_company)
     {
         $this->db->where('id_company', $id_company);
         $this->db->order_by('id_request_stage', 'desc');
         $q = $this->db->get('vacation_request_stages');
         return $q->num_rows();

     }


     /*
     * Add stage
     * 
     * @access public 
     * @param  $id_company
     * @return mixed (bool | array)
     */
     public function add_stage($data)
     {
        $q = $this->db->insert('vacation_allowance_stages', $data);
        return $q;

     }


     /*
     * edit stage
     * 
     * @access public 
     * @param  $id_company
     * @return mixed (bool | array)
     */
     public function edit_stage($data, $id_stage)
     {
        $q = $this->db->update('vacation_allowance_stages', $data, array('id_allowance_stage'=>$id_stage));
        return $q;

     }


     /*
     * delete stage
     * 
     * @access public 
     * @param  $id_company
     * @return mixed (bool | array)
     */
     public function delete_stage($id_stage)
     {
        $this->db->where('id_allowance_stage', $id_stage);
        $q = $this->db->delete('vacation_allowance_stages');
        return $q;

     }



 /*
     * Add plan stage
     * 
     * @access public 
     * @param  $id_company
     * @return mixed (bool | array)
     */
     public function add_pstage($data)
     {
        $q = $this->db->insert('vacation_plan_stages', $data);
        return $q;

     }


     /*
     * edit stage
     * 
     * @access public 
     * @param  $id_company
     * @return mixed (bool | array)
     */
     public function edit_pstage($data, $id_stage)
     {
        $q = $this->db->update('vacation_plan_stages', $data, array('id_plan_stage'=>$id_stage));
        return $q;

     }


     /*
     * delete stage
     * 
     * @access public 
     * @param  $id_company
     * @return mixed (bool | array)
     */
     public function delete_pstage($id_stage)
     {
        $this->db->where('id_plan_stage', $id_stage);
        $q = $this->db->delete('vacation_plan_stages');
        return $q;

     }


      /*
     * Get vacation allowance stages
     * 
     * @access public 
     * @param  $id_company
     * @return mixed (bool | array)
     */
    public function get_plan_stages($id_company) {

         if(!is_numeric($id_company)){
            return false;
        }

        $this->sql = "SELECT vs.*,  u.first_name, u.last_name "
                    . "FROM vacation_plan_stages vs "
                    . "LEFT JOIN users u on u.id_user = vs.id_user "
                    . "WHERE vs.id_company = ? "
                    . "ORDER BY vs.stage asc";
        
        $query = $this->db->query($this->sql, array($id_company));
        return $query->result();

    }


      /*
     * Get last pstage
     * 
     * @access public 
     * @param  $id_company
     * @return mixed (bool | array)
     */
     public function get_last_pstage($id_company)
     {
         $this->db->where('id_company', $id_company);
         $this->db->order_by('id_plan_stage', 'desc');
         $q = $this->db->get('vacation_plan_stages');
         return $q->num_rows();

     }


    /*
     * get allowanceapprover details
     * 
     * @access public 
     * @param  $id_company
     * @return mixed (bool | array)
     */
     public function get_allowance_trig($id)
     {
        $this->db->select('vp.*, vr.*, vs.id_allowance_stage, vs.approver, vs.id_user AS id_approver, vs.stage, vs.id_company');
        $this->db->from('vacation_allowance_approver vp');
        $this->db->join('vacation_allowance_request vr', 'vr.id_vacation_allowance_request = vp.id_vacation_allowance_request', 'left');
        $this->db->join('vacation_allowance_stages vs', 'vs.id_allowance_stage = vp.id_allowance_stage', 'left');
        $this->db->where('id_allowance_approver', $id);
        $q = $this->db->get();
        return $q->row();

     }

    /*
     * Get departments in a company
     * 
     * @access public 
     * @param  $id_company
     * @return mixed (bool | array)
     */
    public function get_company_departments($id_company) {
        if(!is_numeric($id_company)){
            return false;
        }
        
        return $this->db->get_where('departments', array('id_company' => $id_company))->result();
    }// End func get_company_departments
    
    /*
     * Get details of an employees
     * 
     * @access public 
     * @param  $id_user
     * @return mixed (bool | array)
     */
    public function get_employee_details($id_user){

        if(!is_numeric($id_user)){
            return false;
        }

        $this->db->select('u.*, e.*');
        $this->db->from('users u');
        $this->db->where('u.id_user', $id_user);
        $this->db->join('employees e', 'e.id_user = u.id_user', 'left');
        
        $q = $this->db->get();
        return $q->row();
        //return $this->db->get_where('users', array('id_user' => $id_user))->row();

    }// End func get_employee_details


    /*
     * Get employee company details
     * 
     * @access public 
     * @param  $id_company, $id_user
     * @return mixed (bool | array)
     */

    public function get_employee_company_details($id_user){

        if(!is_numeric($id_user)){
            return false;
        }
        $this->db->select('*');
        $this->db->from('employees e');
        $this->db->where('e.id_user', $id_user);
        //$this->db->join('users u', 'e.id_user = u.id_user', 'left');
        $this->db->join('companies c', 'e.id_company = c.id_company', 'left');
        $this->db->join('roles r', 'e.id_role = r.id_role', 'left');
        $this->db->join('departments d', 'e.id_dept = d.deptid', 'left');
        $q = $this->db->get();

        return $q->row();

    }// End func get_employee_details



     /*
     * Get list of inallocated vacation types
     * 
     * @access public 
     * @param  $id_company, $id_user
     * @return mixed (bool | array)
     */

     public function get_unallocated_vaction($id_user, $id_company){

        if(!is_numeric($id_user)){
            return false;
        }

        $this->sql = "SELECT * "
                    . "FROM  vacation_types "
                    . "WHERE id_company = ? "
                    . "AND id_type NOT IN(SELECT id_type FROM vacation_allocated WHERE id_user = ? AND id_company = ?) ";
        
        $query = $this->db->query($this->sql, array($id_company, $id_user, $id_company));
        return $query->result();
        

    }// End func get_unallocated_vacation
    
    /*
     * Get pending approval for a manager
     * 
     * @access public 
     * @param  $id_company, $id_user
     * @return mixed (bool | array)
     */
    public function get_manager_pending_vacation($id_user) {
        if(!is_numeric($id_user)){
            return false;
        }

        
        $this->db->select('v.id_vacation, v.description, v.date_start, v.date_end, v.status, vt.name, u.first_name, u.last_name, vr.*, r.first_name AS relief_first_name, r.last_name AS relief_last_name');
        $this->db->from('vacation v');
        $this->db->join('vacation_types vt', 'v.id_type = vt.id_type', 'left');        
        $this->db->join('users u', 'u.id_user = v.id_user', 'left');
        $this->db->join('employees e', 'e.id_user = u.id_user', 'left');
        $this->db->join('vacation_request_approver vr', 'vr.id_vacation = v.id_vacation', 'left');
        $this->db->join('users r', 'r.id_user = v.id_relief', 'left');
        $this->db->where('vr.id_approver', $id_user);
        $this->db->where('v.status', 'Pending');
        $this->db->where('vr.status', 0);
        return $this->db->get()->result();
    }// End func get_manager_pending_vacation



     /*
     * Get pending approval for a manager
     * 
     * @access public 
     * @param  $id_company, $id_user
     * @return mixed (bool | array)
     */
    public function get_admin_pending_vacation() {
                
        $this->db->select('v.id_vacation, v.description, v.date_start, v.date_end, v.status, vt.name, u.first_name, u.last_name, vr.*, r.first_name AS relief_first_name, r.last_name AS relief_last_name');
        $this->db->from('vacation v');
        $this->db->join('vacation_types vt', 'v.id_type = vt.id_type', 'left');        
        $this->db->join('users u', 'u.id_user = v.id_user', 'left');
        $this->db->join('employees e', 'e.id_user = u.id_user', 'left');
        $this->db->join('vacation_request_approver vr', 'vr.id_vacation = v.id_vacation', 'left');
        $this->db->join('users r', 'r.id_user = v.id_relief', 'left');
        $this->db->where('v.status', 'Pending');
        $this->db->where('vr.status', 0);
        return $this->db->get()->result();
        
    }// End func get_manager_pending_vacation

    
    /*
     * get vacation period
     * 
     * @access public 
     * @param  $id_company, $id_user
     * @return mixed (bool | array)
     */

    public function get_vac_period($id_relief, $id_company, $date_start, $date_end){
        // $this->db->select('*');
        // $this->db->from('vacation');
        // $this->db->where('id_user', $id_relief);
        // $this->db->where('id_company', $id_company);
        // $this->db->where('date_start >', $date_start);
        // $this->db->where('date_end <', $date_end);//->or_where('date_start <=', $date_end);
        // //$this->db->where('status', 'Approved');
        // return $this->db->get()->result();

        $sql = "SELECT * "
                ." FROM  vacation "
                ." WHERE id_user = ? "
                ." AND id_company = ? "
                ." AND (date_start < ? AND date_end > ?) "
                ." OR (date_start < ? AND date_end > ?) "
                ." OR (date_start > ? AND date_start < ?) ";

        $query = $this->db->query($sql, array($id_relief, $id_company, $date_start, $date_start, $date_start, $date_end, $date_start, $date_end));
        return $query->result();

    }



    /*
     * Get pending approval for a manager
     * 
     * @access public 
     * @param  $id_company, $id_user
     * @return mixed (bool | array)
     */
    public function get_manager_pending_plan($id_user) {
        if(!is_numeric($id_user)){
            return false;
        }
        
        $this->db->select('v.*');
        $this->db->from('vacation_plan_approver v');        
        $this->db->join('employees e', 'e.id_user = v.id_user', 'left');
        //$this->db->join('vacation_plan_approver vr', 'vr.id_vacation_plan = v.id_vacation_plan', 'left');
        $this->db->where('v.id_approver', $id_user);
        $this->db->where('v.status', 1);
        return $this->db->get()->result();
    }// End func get_manager_pending_vacation


    /*
     * Vacation Planner Search
     * 
     * @access public 
     * @param  $id_user
     * @return mixed (bool | array)
     */
    public function get_employee_planner_search($id_location, $id_dept, $id_company, $id_manager = null){

        if(!is_numeric($id_company) OR !is_numeric($id_location) OR !is_numeric($id_dept)){
            return false;
        }

        $this->db->select('u.*, e.*, d.departments, l.location_tag');
        $this->db->from('users u');
        $this->db->join('employees e', 'e.id_user = u.id_user', 'left');
        $this->db->join('departments d', 'd.deptid = e.id_dept', 'left');
        $this->db->join('company_locations l', 'l.id_location = u.id_company_location', 'left');
        $this->db->where('e.id_company', $id_company);
        $this->db->where('u.status', USER_STATUS_ACTIVE OR USER_STATUS_PROBATION_ACCESS);
        if($id_location != 0 ){
            $this->db->where('u.id_company_location', $id_location);
        }
        
        if($id_dept != 0){
            $this->db->where('e.id_dept', $id_dept);
        }

        if($id_manager){
            $this->db->where('e.id_manager', $id_manager);
        }
        
        $q = $this->db->get();
        return $q->result();

    }// End func vacation_planner_search


    /*
     * Get ongoing vacation for a manager
     * 
     * @access public 
     * @param  $id_company, $id_user
     * @return mixed (bool | array)
     */
    public function get_manager_ongoing_vacation($id_user) {
        if(!is_numeric($id_user)){
            return false;
        }
        $now = date("Y-m-d H:i:s", strtotime('now'));

        $this->sql = " SELECT v.* , vt.id_type, vt.name, u.first_name, u.last_name, u.id_user, e.id_manager_vacation "
                    ." FROM  vacation v, vacation_types vt, users u, employees e "
                    ." WHERE u.id_user = v.id_user "
                    ." AND v.id_type = vt.id_type "
                    ." AND e.id_user = u.id_user "
                    ." AND e.id_manager_vacation = ? "
                    ." AND v.status = 'Approved' "
                    ." AND date_end >= NOW() ";

        $query = $this->db->query($this->sql, array($id_user));
        return $query->result();
        
    }// End func get_manager_ongoing_vacation


    /*
     * Search vacations
     * 
     * @access public 
     * @param  $id_company, $id_user
     * @return mixed (bool | array)
     */
    public function search($from, $to, $status, $dept, $location, $id_company, $id_manager){
            
            $this->db->select('vacation.*, employees.id_manager, employees.id_manager_vacation, employees.id_user, employees.id_dept, users.first_name, users.last_name, users.id_user, users.id_company_location, vacation_types.id_type, vacation_types.name');
            $this->db->from('vacation');
            $this->db->join('employees', 'vacation.id_user = employees.id_user');
            $this->db->join('users', 'vacation.id_user = users.id_user');
            $this->db->join('vacation_types', 'vacation.id_type = vacation_types.id_type');
            if(!in_array("all", $dept)){
                $depts = implode(",", $dept);
                $this->db->where_in('employees.id_dept', $depts);
            }
            if(!in_array("all", $location)){
                $locations = implode(",", $location);
                $this->db->where_in('users.id_company_location', $locations);
            }
            if($status !== 'all'){
                $this->db->where('vacation.status', $status);
            }
            //$this->db->where('', $Value);
            $this->db->where('date_start >', $from);
            $this->db->where('date_start <', $to);
            $this->db->where('vacation.id_company', $id_company);
            $qr = $this->db->get();
            return $qr->result();     

    }


    /*
     * Cancel employee vacation
     * 
     * @access public 
     * @param  $params
     * @return mixed (bool | array)
     */
    public function cancel_vacation($params){

        if(empty($params)){
            return false;
        }

        $res = $this->db->delete('vacation_allocated', $params);

        if($res > 0 ) {
            return true;
        }

        return false;

    }// End func delete_allocation


    /*
     * Cancel employee vacation
     * 
     * @access public 
     * @param  $params
     * @return mixed (bool | array)
     */
    public function get_vacation($id_vacation){

        if(empty($id_vacation)){
            return false;
        }
        $this->db->where('id_vacation', $id_vacation);
        $res = $this->db->get('vacation');
        return $res->row();
        
    }// End func delete_allocation


    /*
     * Get leave allownace
     * 
     * @access public 
     * @param  $params
     * @return mixed (bool | array)
     */
    public function get_leave_allowance($id_user){

        if(empty($id_user)){
            return false;
        }

        $this->db->select('e.id_user, a.*');
        $this->db->from('compensation_employees e');
        $this->db->join('compensation_employee_leave_allowance a', 'a.id_compensation_employee = e.id_compensation_employee', 'left');
        $this->db->where('e.id_user', $id_user);
        $res = $this->db->get();
        return $res->row();
        
    }// End func delete_allocation


    /*
     * Insert allowance request
     * 
     * @access public 
     */
    public function insert_allowance_request($data){

        if(empty($data)){
            return false;
        }
        
        $this->db->insert('vacation_allowance_request', $data);
        return $this->db->insert_id();

        
    }// End func 


    /*
     * Insert allowance request
     * 
     * @access public 
     */
    public function check_request($id_user){

        if(empty($id_user)){
            return false;
        }
        $year = date('Y');
        $q = $this->db->get_where('vacation_allowance_request', array('status >'=>0, 'id_user'=>$id_user, 'year'=>$year));
        return $q->result();
        
    }// End func delete_allocation


    /*
     * Get User
     * 
     * @access public 
     * @param  $params
     * @return mixed (bool | array)
     */
    public function get_by_id_string($id_string){

        if(empty($id_string)){
            return false;
        }
        $this->db->where('id_string', $id_string);
        $res = $this->db->get('users');
        return $res->row();
        
    }// End func delete_allocation


    /*
     * Get Id string
     * 
     * @access public 
     * @param  $params
     * @return mixed (bool | array)
     */
    public function get_user($id_user){

        if(empty($id_user)){
            return false;
        }
        $this->db->where('id_user', $id_user);
        return $this->db->get('users')->row();
        
    }// End func delete_allocation





    /*
     * Get 
     * 
     * @access public 
     * @param  $params
     * @return mixed (bool | array)
     */
    public function get($table, $where=Null){

        if(empty($table)){
            return false;
        }
        
        $res = $this->db->get_where($table, $where);
        return $res->result();
        
    }// End func get


    /*
     * Get 
     * 
     * @access public 
     * @param  $params
     * @return mixed (bool | array)
     */
    public function get_where($table, $where=Null, $key){

        if(empty($table)){
            return false;
        }
        
        $res = $this->db->order_by($key, 'DESC')->get_where($table, $where);
        return $res->result();
        
    }// End func get

    /*
     * Get 
     * 
     * @access public 
     * @param  $params
     * @return mixed (bool | array)
     */
    public function get_count($table, $where=Null){

        if(empty($table)){
            return false;
        }
        
        $res = $this->db->get_where($table, $where);
        return $res->num_rows();
        
    }// End func get


    /*
     * add
     * 
     * @access public 
     */
    public function add($table, $data, $id=false){

        if(empty($data) OR empty($table)){
            return false;
        }

        $q = $this->db->insert($table, $data);

        if($id){
            return $this->db->insert_id();
        }
        else{
             return $q;
        }
                
    }// End func


    /*
     * add
     * 
     * @access public 
     */
    public function update($table, $data, $where){

        if(empty($data) OR empty($table)){
            return false;
        }
        
        $q = $this->db->update($table, $data, $where);
        return $q;

        
    }// End func



    /*
     * delete
     * 
     * @access public 
     */
    public function delete($table, $where){

        if(empty($where) OR empty($table)){
            return false;
        }
        
        $q = $this->db->delete($table, $where);
        return $q;

        
    }// End func



    /*
     * check if user has access
     * 
     * @access public 
     */
    public function user_has_access($id_user){

        if(empty($id_user) OR !is_numeric($id_user)){
            return false;
        }
        
        $this->db->where('id_user', $id_user);
        $q = $this->db->get('users')->row();
        return $q->status;

        
    }// End func


/**
 * Class class_name
 * 
 * @access public
 * @return void
 */


    public function validate_id($table, $where)
    {
        $q = $this->db->get_where($table, $where);
        if($q->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }


/**
 * Class class_name
 * 
 * @access public
 * @return void
 */


    public function validate_user($id_user)
    {
        $this->db->select('u.*, e.*');
        $this->db->from('users u');
        $this->db->join('employees e', 'e.id_user = u.id_user', 'left');
        $this->db->where('u.id_user', $id_user);
        $q = $this->db->get();
        if($q->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }



/**
 * Class class_name
 * 
 * @access public
 * @return void
 */


    public function validate_user_string($id_string)
    {
        $this->db->select('u.*, e.*');
        $this->db->from('users u');
        $this->db->join('employees e', 'e.id_user = u.id_user', 'left');
        $this->db->where('u.id_string', $id_string);
        $q = $this->db->get();
        if($q->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }



/**
 * Get setup steps
 * 
 * @access public
 * @return void
 */


    public function get_setup_step($id_company, $id_module)
    {
        $this->db->where('id_company', $id_company);
        $this->db->where('id_module', $id_module);
        $this->db->where('status', MODULE_SETUP_PENDING);
        $this->db->order_by('id_module_setup', 'desc');
        $q = $this->db->get('module_setup');

        return $q->result();

    }

/**
 * Get setup steps
 * 
 * @access public
 * @return void
 */


    public function get_pay_struture($id_company){

        $this->db->select('pg.*, ps.id_pay_structure, ps.id_role, r.roles');
        $this->db->from('compensation_pay_grade pg');
        $this->db->join('compensation_pay_structure ps', 'ps.id_pay_grade = pg.id_pay_grade', 'left');
        $this->db->join('roles r', 'r.id_role = ps.id_role', 'left');
        $this->db->where('pg.id_company', $id_company);
        $q = $this->db->get()->result();

        return $q;
    }   



/*
 * Get employee vacation allowance request
 * 
 * @access public 
 * @param  $id_user
 * @return mixed (bool | array)
 */
    public function get_allocated($id_company, $id_user){

        if(!is_numeric($id_company) || !is_numeric($id_user)){
            return false;
        }

        $this->db->select('v.*, vt.*');
        $this->db->from('vacation_types vt');
        $this->db->join('vacation_allocated v', 'v.id_type = vt.id_type', 'left');
        $this->db->where('v.id_company', $id_company);
        $this->db->where('v.id_user', $id_user);
        return $this->db->get()->result();

    }// End func get_employee_entitlements


/*
 * Get employee vacation allowance request
 * 
 * @access public 
 * @param  $id_user
 * @return mixed (bool | array)
 */
    public function get_planned($id_company, $id_user, $id_request){

        if(!is_numeric($id_company) || !is_numeric($id_user)){
            return false;
        }

        $this->db->select('v.*, vt.*');
        $this->db->from('vacation_types vt');
        $this->db->join('vacation_plan v', 'v.id_type = vt.id_type', 'left');
        $this->db->where('v.id_company', $id_company);
        $this->db->where('v.id_user', $id_user);
        $this->db->where('v.id_plan_request', $id_request);
        return $this->db->get()->result();

    }// End func get_employee_entitlements


/*
 * Get employee vacation allowance request
 * 
 * @access public 
 * @param  $id_user
 * @return mixed (bool | array)
 */
    public function get_employee_plans($id_user, $id_company, $id_vac_type = null){

        if(!is_numeric($id_company) || !is_numeric($id_user)){
            return false;
        }

        $this->db->select('v.*, vt.*, u.first_name, u.last_name');
        $this->db->from('vacation_plan v');
        $this->db->join('vacation_types vt', 'vt.id_type = v.id_type', 'left');
        $this->db->join('users u', 'u.id_user = v.id_user', 'left');
        $this->db->where('v.id_company', $id_company);
        $this->db->where('v.id_user', $id_user);
        if(isset($id_vac_type)){
            $this->db->where('v.id_type', $id_vac_type);
        }
        return $this->db->get()->result();

    }// End func get_employee_entitlements

/*
 * Get company role vacation 
 * @access public 
 * @param  $id_user
 * @return mixed (bool | array)
 */


    public function get_role_vacation($id_company)
    {
        $query = $this->db
              ->select('id_pay_structure, SUM(no_of_days) AS days_off')
              ->where('id_company', $id_company)
              ->group_by('id_pay_structure')
              ->get('vacation_compensation');

        return $query->result();
    }


/*
 * Get company role vacation 
 * @access public 
 * @param  $id_user
 * @return mixed (bool | array)
 */


    public function get_single_role_vacation($id_company, $id_paygrade)
    {
        $query = $this->db
              ->select('*')
              ->where('id_company', $id_company)
              ->where('id_pay_structure', $id_paygrade)
              ->get('vacation_compensation');

        return $query->result();
    }



/*


*/


    public function get_pending_count($id_company, $period)
    {
       $this->db->where('id_company', $id_company);
       $this->db->where('year', $period);
       $this->db->where('status', 'Pending');
       $q = $this->db->get('vacation_plan');
       return $q->num_rows();
    }




/*
 * Get employee vacation plans date
 * 
 * @access public 
 * @param  $id_user
 * @return mixed (bool | array)
 */
    public function get_employee_plans_date($id_company, $date, $month_year){

        if(!is_numeric($id_company)){
            return false;
        }

        $emp = array();
        $emply = $this->get('users', array('id_company'=>$id_company, 'status'=>USER_STATUS_ACTIVE OR USER_STATUS_PROBATION_ACCESS));
        foreach ($emply as $em) {

            $this->db->where('month_year', $month_year);
            $this->db->where('status !=', 'draft');
            $this->db->where('id_user', $em->id_user);

            $r = $this->db->get('vacation_plan')->result();
            
            if(!empty($r)){
                foreach($r as $k => $v ){
                    $apprv = $this->get('vacation_plan_approver', array('id_user'=>$em->id_user, 'status'=>1, 'id_vacation_plan'=>$v->id_vacation_plan)); 
                    foreach ($apprv as $appr) {
                        $approvs[] = $appr->id_approver;
                    }
                    $r[$k]->id_approver = $approvs;


                    $p_apprv = $this->get('vacation_plan_approver', array('id_user'=>$em->id_user, 'status'=>0, 'id_vacation_plan'=>$v->id_vacation_plan)); 
                    foreach ($p_apprv as $pend) {
                        $pends[] = $pend->id_approver;
                    }
                    $r[$k]->pending_approver = $pends;


                    $stages = $this->get('vacation_request_stages', array('id_company'=>$id_company));
                    foreach ($stages as $stg) {
                        $stage_approvers[] = $stg->id_user;
                    }
                    $r[$k]->stage_approvers = $stage_approvers;
                }
            }

            $emp[$em->id_user] = $r;
        }
        unset($r);
        return $emp;

    }// End func get_employee_plans_date



/*
 * Get employee vacation allowance request
 * 
 * @access public 
 * @param  $id_user
 * @return mixed (bool | array)
 */
    public function get_employee_plans_date2($id_company, $id_type, $month, $year){

        if(!is_numeric($id_company)){
            return false;
        }

        $month = date('m', strtotime($month));
        $emp = array();
        $emply = $this->db->get_where('users', array('id_company'=>$id_company, 'status'=>USER_STATUS_ACTIVE OR USER_STATUS_PROBATION_ACCESS))->result();

        foreach ($emply as $em) {
            $this->db->select('v.*, a.id_approver');
            $this->db->from('vacation_plan v');
            $this->db->join('vacation_plan_approver a', 'a.id_vacation_plan = v.id_vacation_plan', 'left');
            $this->db->where('v.id_user', $em->id_user);

            if($year != 0){
                $this->db->where('v.year', $year);
            }

            if($month != 0){
                $m = $month.'_'.$year;
                $this->db->where('v.month_year', $m);
            }
            else{
                $m = '01_'.$year;
                $this->db->where('v.month_year', $m);
            }

            
            if($id_type != 0){
                $this->db->where('v.id_type', $id_type);
            }

            $r = $this->db->get()->result();


            
            if(!empty($r)){
                foreach($r as $k => $v ){
                    $approvs = array();
                    $apprv = $this->get('vacation_plan_approver', array('id_user'=>$em->id_user, 'status'=>1, 'id_vacation_plan'=>$v->id_vacation_plan)); 
                    foreach ($apprv as $appr) {
                        $approvs[] = $appr->id_approver;
                    }
                    $r[$k]->id_approver = $approvs;

                    $pends = array();
                    $p_apprv = $this->get('vacation_plan_approver', array('id_user'=>$em->id_user, 'status'=>0, 'id_vacation_plan'=>$v->id_vacation_plan)); 
                    foreach ($p_apprv as $pend) {
                        $pends[] = $pend->id_approver;
                    }
                    $r[$k]->pending_approver = $pends;


                    $stages = $this->get('vacation_request_stages', array('id_company'=>$id_company));
                    foreach ($stages as $stg) {
                        $stage_approvers[] = $stg->id_user;
                    }
                    $r[$k]->stage_approvers = $stage_approvers;
                }
                unset($approvs);
                unset($pends);
            }

            $emp[$em->id_user] = $r;

        }

        return $emp;

    }// End func get_employee_plans_date




    /*
 * Get employee vacation allowance request
 * 
 * @access public 
 * @param  $id_user
 * @return mixed (bool | array)
 */
    public function get_employee_plans_approved($id_company, $id_type, $month, $year){

        if(!is_numeric($id_company)){
            return false;
        }

        $month = date('m', strtotime($month));
        $emp = array();
        $emply = $this->db->get_where('users', array('id_company'=>$id_company, 'status'=>USER_STATUS_ACTIVE OR USER_STATUS_PROBATION_ACCESS))->result();

        foreach ($emply as $em) {

            $this->db->where('id_user', $em->id_user);

            if($year != 0){
                $this->db->where('year', $year);
            }

            if($month != 0){
                $m = $month.'_'.$year;
                $this->db->where('month_year', $m);
            }
            else{
                $m = '01_'.$year;
                $this->db->where('month_year', $m);
            }

            if($id_type != 0){
                $this->db->where('id_type', $id_type);
            }

            $this->db->where('status', 'Approved');

            $r = $this->db->get('vacation_plan')->result();

            if(!empty($r)){
                foreach ($r as $rw) {
                            
                    $st_day = date('d', strtotime($rw->start_date));
                    $en_day = date('d', strtotime($rw->end_date));

                    for ($i=$st_day; $i <= $en_day ; $i++) { 
                       $emp[$em->id_user][] = $i;
                    }
                }
                
            }
            
        }

        return $emp;

    }// End func get_employee_plans_date


    /*
 * Get employee vacation allowance request
 * 
 * @access public 
 * @param  $id_user
 * @return mixed (bool | array)
 */
    public function get_employee_plans_pending($id_company, $id_type, $month, $year){

        if(!is_numeric($id_company)){
            return false;
        }

        $month = date('m', strtotime($month));
        $emp = array();
        $emply = $this->db->get_where('users', array('id_company'=>$id_company, 'status'=>USER_STATUS_ACTIVE OR USER_STATUS_PROBATION_ACCESS))->result();

        foreach ($emply as $em) {

            $this->db->where('id_user', $em->id_user);

            if($year != 0){
                $this->db->where('year', $year);
            }

            if($month != 0){
                $m = $month.'_'.$year;
                $this->db->where('month_year', $m);
            }
            else{
                $m = '01_'.$year;
                $this->db->where('month_year', $m);
            }

            if($id_type != 0){
                $this->db->where('id_type', $id_type);
            }

            $this->db->where('status', 'Pending');

            $r = $this->db->get('vacation_plan')->result();

            if(!empty($r)){
                foreach ($r as $rw) {
                            
                    $st_day = date('d', strtotime($rw->start_date));
                    $en_day = date('d', strtotime($rw->end_date));

                    for ($i=$st_day; $i <= $en_day ; $i++) { 
                       $emp[$em->id_user][] = $i;
                    }
                }
                
            }
            
        }

        return $emp;

    }// End func get_employee_plans_date


/*
 * Get employee vacation allowance request
 * 
 * @access public 
 * @param  $id_user
 * @return mixed (bool | array)
 */
    public function get_requested_plans($id_company, $id_vac_type, $year){

        if(!is_numeric($id_company) || !is_numeric($id_vac_type)){
            return false;
        }

        $this->db->select('v.*, vt.*');
        $this->db->from('vacation_types vt');
        $this->db->join('vacation_plan_request v', 'v.id_type = vt.id_type', 'left');
        $this->db->where('v.id_company', $id_company);
        $this->db->where('v.id_type', $id_vac_type);
        $this->db->where('v.period', $year);

        return $this->db->get()->result();

    }// End func get_employee_entitlements



/*
 * Get employee vacation allowance request
 * 
 * @access public 
 * @param  $id_user
 * @return mixed (bool | array)
 */
    public function get_allocated_sum($id_company, $id_user){

        if(!is_numeric($id_company) || !is_numeric($id_user)){
            return false;
        }

        $this->db->select_sum('no_of_days');
        $this->db->where('id_company', $id_company);
        $this->db->where('id_user', $id_user);
        return $this->db->get('vacation_allocated')->result();

    }// End func get_employee_entitlements


/*
 * Get employee vacation allowance request
 * 
 * @access public 
 * @param  $id_user
 * @return mixed (bool | array)
 */
    public function get_pending_used_sum($id_company, $id_user){

        if(!is_numeric($id_company) || !is_numeric($id_user)){
            return false;
        }

        $date = date('Y-m-d');

        $this->sql = " SELECT SUM(no_of_days) as no_of_days "
            .  " FROM vacation "
            .  " WHERE id_user = ? "
            .  " AND MONTH(date_start) =  Month('".$date."') "
            .  " AND id_company = ? "
            .  " AND status = 'Approved' "
            .  " AND status = 'Pending' ";

        $q = $this->db->query($this->sql, array($id_user, $id_company));

        return $q->result();

    }// End func get_employee_entitlements




/*
 * Get employee vacation allowance request
 * 
 * @access public 
 * @param  $id_user
 * @return mixed (bool | array)
 */
    public function get_allocated_sum2($id_company, $id_user, $id_type){

        if(!is_numeric($id_company) || !is_numeric($id_user)){
            return false;
        }

        $this->db->select_sum('no_of_days');
        $this->db->where('id_company', $id_company);
        $this->db->where('id_user', $id_user);
        $this->db->where('id_type', $id_type);
        return $this->db->get('vacation_allocated')->result();

    }// End func get_employee_entitlements


/*
 * Get employee vacation allowance request
 * 
 * @access public 
 * @param  $id_user
 * @return mixed (bool | array)
 */
    public function get_planned_sum($id_company, $id_user, $id_type){

        if(!is_numeric($id_company) || !is_numeric($id_user)){
            return false;
        }

        $this->db->select_sum('no_of_days');
        $this->db->where('id_company', $id_company);
        
        $this->db->where('id_type', $id_type);
        $this->db->where('status !=', 'Declined');
        $this->db->where('id_user', $id_user);
        return $this->db->get('vacation_plan')->result();

    }// End func get_employee_entitlements


/*
 * Get employee vacation allowance request
 * 
 * @access public 
 * @param  $id_user
 * @return mixed (bool | array)
 */
    public function get_plan_request($id_company){

        if(!is_numeric($id_company)){
            return false;
        }

        $this->db->select('vt.id_type, vt.name, v.id_company, v.status, v.due_date, v.period, v.id_plan_request');
        $this->db->from('vacation_types vt');
        $this->db->join('vacation_plan_request v', 'v.id_type = vt.id_type', 'left');
        $this->db->where('vt.id_company', $id_company);
        return $this->db->get()->result();

    }// End func get_employee_entitlements


/*
 * Get employee vacation allowance request
 * 
 * @access public 
 * @param  $id_user
 * @return mixed (bool | array)
 */
    public function num_planned_days($id_request, $id_type, $id_user, $id_company){

        if(!is_numeric($id_request) OR !is_numeric($id_type) OR !is_numeric($id_user)){
            return false;
        }

        $this->db->select_sum('no_of_days');
        $this->db->where('id_plan_request', $id_request);
        $this->db->where('id_type', $id_type);
        $this->db->where('id_user', $id_user);
        $this->db->where('id_company', $id_company);
        $this->db->where('status !=', 'Declined');
        $query = $this->db->get('vacation_plan');
        return $query->result();

    }// End func get_employee_entitlements






/*
 * Get employee vacation allowance request
 * 
 * @access public 
 * @param  $id_user
 * @return mixed (bool | array)
 */
    public function get_date_conflicts($id_company, $date, $id_request, $id_type, $id_user){

        if(!is_numeric($id_company)){
            return false;
        }

        $sdate = date('Y-m-d', strtotime($date['start']));
        $edate = date('Y-m-d', strtotime($date['end']));

        $this->sql = " SELECT * "
            .  " FROM vacation_plan "
            .  " WHERE id_type = ? "
            .  " AND id_plan_request = ? "
            .  " AND id_user = ? "
            .  " AND status != ? "
            .  " AND (DATE(start_date) >= ? AND DATE(start_date) <= ? ) "
            .  " OR (DATE(end_date) >= ? AND DATE(end_date) <= ? ) ";
            
            

        $q = $this->db->query($this->sql, array($id_type, $id_request, $id_user, 'Declined', $sdate, $edate, $sdate, $edate));

        if($q->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }

    }// End func get_employee_entitlements



/*
 * Get employee vacation allowance request
 * 
 * @access public 
 * @param  $id_user
 * @return mixed (bool | array)
 */
    public function get_date_conflicts2($id_company, $start_date, $end_date){

        if(!is_numeric($id_company)){
            return false;
        }

        $sdate = date('Y-m-d', strtotime($start_date));
        $edate = date('Y-m-d', strtotime($end_date));

        $this->sql = " SELECT * "
            .  " FROM vacation_restricted_period "
            .  " WHERE id_company = ? "
            .  " AND (DATE(start_date) >= ? AND DATE(start_date) <= ? ) "
            .  " OR (DATE(end_date) >= ? AND DATE(end_date) <= ? ) ";           

        $q = $this->db->query($this->sql, array($id_company, $sdate, $edate, $sdate, $edate));

        if($q->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }

    }// End func get_date_conflicts



    public function compensation_paygrade($id_company)
    {
        $payst = $this->db->get_where('payroll_paygrade', array('id_company'=> $id_company))->result();
        $struc = array();
        $num = 1;
        
        foreach ($payst as $k => $p) {

            $vac = $this->compensation_vac($id_company, $p->id_payroll_paygrade); 
            $struc[$num]['pay'] = $p;
            $struc[$num]['vac'] = $vac;
            $num++;
        }
      
        return $struc;
    }


    public function get_employee_leave_allowance($id_user, $id_company, $year)
    {
        //get company users with otherpay
        //get company users with otherpay
        $this->db->select('c.id_payroll_otherpay, c.id_user, o.*');
        $this->db->from('payroll_compensation c');
        $this->db->join('payroll_otherpay o', 'c.id_payroll_otherpay = o.id_payroll_otherpay', 'left');
        $this->db->where('c.id_company', $id_company);
        $this->db->where('c.id_user', $id_user);
        $op = $this->db->get()->result();

        $setting = $this->db->get_where('vacation_allowance_setting', array('id_company'=>$id_company))->result();
        $leave = array();
    
        if(!empty($op[0]->otherpay)){
            $otherpay = json_decode($op[0]->otherpay);

            foreach ($otherpay as $value) {
                $value->user = $this->get_employee($id_user);
                $value->request = $this->get('vacation_allowance_request', array('id_company'=>$id_company, 'id_user'=>$id_user, 'year'=>$year, 'status >'=> 0));
                $leave[] = $value;
            }
        }

        return $leave;
    }


    public function admin_get_all_allowance($id_company, $year)
    {
        //get company users with otherpay
        $this->db->select('c.id_payroll_otherpay, c.id_user, o.otherpay');
        $this->db->from('payroll_compensation c');
        $this->db->join('payroll_otherpay o', 'o.id_payroll_otherpay = c.id_payroll_otherpay', 'left');
        $this->db->where('c.id_company', $id_company);
        $this->db->where('c.id_user >', 0);
        $op = $this->db->get()->result();

        $setting = $this->db->get_where('vacation_allowance_setting', array('id_company'=>$id_company))->result();
        $leave = array();

        if(!empty($op)){
            foreach ($op as $p) {
                $otherpay = json_decode($p->otherpay);

                if(!empty($otherpay)){
                    foreach ($otherpay as $value) {
                        if($value->otherpay == $setting[0]->vacation_allowance){
                            $value->user = $this->get_employee_info($id_company, $p->id_user);
                            $value->request = $this->get('vacation_allowance_request', array('id_company'=>$id_company, 'id_user'=>$p->id_user, 'year'=>$year, 'status >'=> 0));
                            $leave[] = $value;
                        }
                    }
                }
            }
        }

        return $leave;
    }



    /*


    */ 


    public function manager_get_all_allowance($id_company, $year, $id_manager)
    {
        $this->db->select('va.*, u.id_user, u.first_name, c.id_compensation_employee, u.last_name, u.status AS user_status');
        $this->db->from('compensation_employee_leave_allowance va');
        $this->db->join('compensation_employees c', 'c.id_compensation_employee = va.id_compensation_employee', 'left');
        $this->db->join('users u', 'u.id_user = c.id_user', 'left');
        $this->db->join('employees e', 'e.id_user = u.id_user', 'left');
        $this->db->where('va.id_company', $id_company);
        $this->db->where('e.id_manager', $id_manager);
        $qr = $this->db->get()->result();

        foreach ($qr as $q) {
            $this->db->where('id_company', $id_company);
            $this->db->where('id_user', $q->id_user);
            $this->db->where('year', $year);
            $this->db->order_by('id_vacation_allowance_request', 'desc');
            $this->db->limit(1);
            $d = $this->db->get('vacation_allowance_request')->row();
            $q->request_status = (!empty($q->status)) ? $d->status : 0 ;
            $q->request = $d;
        }
        return $qr;
    }






    /*



    */

    public function manager_get_pending_allowance($id_company, $year, $id_manager)
    {
        $this->db->select('va.*, u.first_name, u.last_name, u.status AS user_status, ce.id_employee_leave_allowance');
        $this->db->from('vacation_allowance_request va');
        $this->db->join('users u', 'u.id_user = va.id_user', 'left');
        $this->db->join('employees e', 'e.id_user = u.id_user', 'left');
        $this->db->join('compensation_employees c', 'c.id_user = va.id_user', 'left');
        $this->db->join('compensation_employee_leave_allowance ce', 'ce.id_compensation_employee = c.id_compensation_employee', 'left');
        $this->db->where('va.id_company', $id_company);
        $this->db->where('va.year', $year);
        $this->db->where('va.status', 1);
        $this->db->where('e.id_manager', $id_manager);
        $d = $this->db->get()->result();
        return $d;
    }



    /*



    */

    public function admin_get_pending_allowance($id_company, $year)
    {
        $this->db->select('va.*, u.first_name, u.last_name, u.status AS user_status, ce.id_vacation_allowance');
        $this->db->from('vacation_allowance_request va');
        $this->db->join('users u', 'u.id_user = va.id_user', 'left');
        // $this->db->join('compensation_employees c', 'c.id_user = va.id_user', 'left');
        $this->db->join('vacation_allowance ce', 'ce.id_user = va.id_user', 'left');
        $this->db->where('va.id_company', $id_company);
        $this->db->where('va.year', $year);
        $this->db->where('va.status', 1);
        $d = $this->db->get()->result();
        return $d;
    }


    /*



    */

    public function admin_get_allowance_history($id_company, $year)
    {
        $this->db->select('va.*, u.first_name, u.last_name, u.status AS user_status, ce.id_vacation_allowance');
        $this->db->from('vacation_allowance_request va');
        $this->db->join('users u', 'u.id_user = va.id_user', 'left');
        // $this->db->join('compensation_employees c', 'c.id_user = va.id_user', 'left');
        $this->db->join('vacation_allowance ce', 'ce.id_user = va.id_user', 'left');
        $this->db->where('va.id_company', $id_company);
        //$this->db->where('va.year', $year);
        $this->db->where('va.status !=', 1);
        $d = $this->db->get()->result();
        return $d;
    }


    /*



    */

    public function manager_get_allowance_history($id_company, $year, $id_manager)
    {
        $this->db->select('va.*, u.first_name, u.last_name, u.status AS user_status');
        $this->db->from('vacation_allowance_request va');
        $this->db->join('users u', 'u.id_user = va.id_user', 'left');
        $this->db->join('employees e', 'e.id_user = u.id_user', 'left');
        // $this->db->join('compensation_employees c', 'c.id_user = va.id_user', 'left');
        // $this->db->join('compensation_employee_leave_allowance ce', 'ce.id_compensation_employee = c.id_compensation_employee', 'left');
        $this->db->where('va.id_company', $id_company);
        //$this->db->where('va.year', $year);
        $this->db->where('va.status !=', 1);
        $this->db->where('e.id_manager', $id_manager);
        $d = $this->db->get()->result();
        return $d;
    }
    

    

    public function get_employee_allowance($id_company, $year, $id_compensation)
    {

        $this->db->select('va.*, u.id_user, u.first_name, c.id_compensation_employee, u.last_name, u.status AS user_status');
        $this->db->from('compensation_employee_leave_allowance va');
        $this->db->join('compensation_employees c', 'c.id_compensation_employee = va.id_compensation_employee', 'left');
        $this->db->join('users u', 'u.id_user = c.id_user', 'left');
        $this->db->where('va.id_company', $id_company);
        $this->db->where('va.id_compensation_employee', $id_compensation);
        $this->db->where('va.year', $year);
        $qr = $this->db->get()->result();

        foreach ($qr as $q) {
            $this->db->where('id_company', $id_company);
            $this->db->where('id_user', $q->id_user);
            $this->db->where('year', $year);
            $this->db->order_by('id_vacation_allowance_request', 'desc');
            $this->db->limit(1);
            $d = $this->db->get('vacation_allowance_request')->row();
            $q->request_status = $q->status;
            $q->request = $d;
        }

        return $qr;
    }


    /*



    */

    public function get_pending_allowance($id_user, $id_company, $year)
    {
        $this->db->where('id_company', $id_company);
        $this->db->where('id_user', $id_user);
        $this->db->where('year', $year);
        $this->db->where('status', 1);
        $this->db->order_by('id_vacation_allowance_request', 'desc');
        $this->db->limit(1);
        $d = $this->db->get('vacation_allowance_request')->result();
        return $d;
    }



    /*



    */

    public function get_employee_allowance_history($id_user, $id_company, $year)
    {
        $this->db->where('id_company', $id_company);
        $this->db->where('id_user', $id_user);
        $this->db->where('year', $year);
        $this->db->where('status !=', 1);
        $this->db->order_by('id_vacation_allowance_request', 'desc');
        $this->db->limit(1);
        $d = $this->db->get('vacation_allowance_request')->result();
        return $d;
    }


    public function compensation_vac($id_company, $id_pay_structure)
    {
        $this->db->select('vc.*, v.name');
        $this->db->from('vacation_compensation vc');
        $this->db->join('vacation_types v', 'v.id_type = vc.id_vacation_type', 'left');
        $this->db->where('vc.id_company', $id_company);
        $this->db->where('vc.id_pay_structure', $id_pay_structure);
        $q = $this->db->get();
        return $q->result();
    }



    /*
 * Get employee vacation request
 * 
 * @access public 
 * @param  $id_user
 * @return mixed (bool | array)
 */
    public function get_vacs($id_company, $date, $dept, $locate){

        $this->sql = " SELECT v.* "
            .  " FROM vacation v, users u, employees e "
            .  " WHERE u.id_user = v.id_user "
            .  " AND e.id_user = u.id_user "
            .  " AND v.status = ? "
            .  " AND v.id_company = ? "
            .  " AND MONTH(date_start) =  Month('".$date."')  ";

            if ($dept != 0) {
                $this->sql .= " AND e.id_dept = '".$dept."' ";
            }

            if ($locate != 0) {
                $this->sql .= " AND u.id_company_location =  '".$locate."' ";
            }

        $q = $this->db->query($this->sql, array('Approved', $id_company));

        return $q->result();

    }// End func get_employee_entitlements



    
/*
 * Get employee vacation request
 * 
 * @access public 
 * @param  $id_user
 * @return mixed (bool | array)
 */
    public function get_employee_count($id_company, $date, $dept, $locate){

        $this->sql = " SELECT u.* "
            .  " FROM users u, employees e "
            .  " WHERE e.id_user = u.id_user "
            .  " AND e.id_company = ? ";

            if ($dept != 0) {
                $this->sql .= " AND e.id_dept = '".$dept."' ";
            }

            if ($locate != 0) {
                $this->sql .= " AND u.id_company_location =  '".$locate."' ";
            }

        $q = $this->db->query($this->sql, array($id_company));

        return $q->result();

    }// End func get_employee_entitlements



   /*
 * Get employee vacation allowance sum
 * 
 * @access public 
 * @param  $id_user
 * @return mixed (bool | array)
 */
    public function get_vallowance_sum($id_company, $dept, $locate, $year){

        //get vacation allowance setting
        //get otherpay types
        // $op = $this->db->get_where('payroll_otherpay', array('id_company'=>$id_company))->result();
        $this->sql = " SELECT * "
            .  " FROM payroll_otherpay v, users u, employees e "
            .  " WHERE e.id_user = v.id_user "
            .  " AND u.id_user = v.id_user "
            .  " AND v.id_company = ? ";

            if ($dept != 0) {
                $this->sql .= " AND e.id_dept = '".$dept."' ";
            }

            if ($locate != 0) {
                $this->sql .= " AND u.id_company_location =  '".$locate."' ";
            }
            
        $op = $this->db->query($this->sql, array($id_company))->result();

        $setting = $this->db->get_where('vacation_allowance_setting', array('id_company'=>$id_company))->result();
        $sum = 0;
        if(!empty($op)){
            foreach ($op as $p) {
                $otherpay = json_decode($p->otherpay);
                
                foreach ($otherpay as $value) {
                    if($value->otherpay == $setting[0]->vacation_allowance){
                        // $emp = $this->get_employee($p->id_user); 
                        $sum = $value->amount + $sum;
                        //$leave[] = $emp;
                    }
                    
                }
            }
        }

        return $sum;

    }// End func get_employee_entitlements



    /*
 * Get employee vacation allowance sum
 * 
 * @access public 
 * @param  $id_user
 * @return mixed (bool | array)
 */
    public function get_vallowance_sum_month($id_company, $dept, $locate, $date){

        $this->sql = " SELECT SUM(amount) as amount "
            .  " FROM vacation_allowance_request v, users u, employees e "
            .  " WHERE u.id_user = v.id_user "
            .  " AND e.id_user = v.id_user "
            .  " AND MONTH(date_requested) = Month('".$date."') "
            .  " AND v.id_company = ? "
            .  " AND v.status = ? ";

            if ($dept != 0) {
                $this->sql .= " AND e.id_dept = '".$dept."' ";
            }

            if ($locate != 0) {
                $this->sql .= " AND u.id_company_location =  '".$locate."' ";
            }

            
        $q = $this->db->query($this->sql, array($id_company, 2));

        return $q->result();

    }// End func get_employee_entitlements








    public function get_manager_employees_location($id_company, $id_manager)
    {

        $this->db->select('u.id_user, c.*');
        $this->db->from('users u');
        $this->db->join('employees e', 'e.id_user = u.id_user', 'left');
        $this->db->join('company_locations c', 'c.id_location = u.id_company_location', 'left');
        $this->db->where('e.id_company', $id_company);
        $this->db->where('e.id_manager', $id_manager);
        $this->db->group_by('c.id_location');
        
        return $this->db->get()->result();
    }



    /*


    */ 


    public function manager_view_vacation_direct($id_company, $id_manager, $month)
    {
        $usr = $this->get('employees', array('id_user'=>$id_manager));
        
        $date = date('Y-m-d', strtotime($month));
        $this->db->select('u.id_user, u.first_name, u.last_name, u.status');
        $this->db->from('users u');
        $this->db->join('employees e', 'e.id_user = u.id_user', 'left');
        $this->db->where('e.id_company', $id_company);
        if($usr[0]->id_access_level != ACCOUNT_TYPE_ADMINISTRATOR AND $usr[0]->id_access_level != ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR){
            $this->db->where('e.id_manager', $id_manager);
        }
        
        $qr = $this->db->get()->result();


        foreach ($qr as $q) {

            $this->sql = " SELECT * "
            .  " FROM vacation "
            .  " Where MONTH(date_start) = Month('".$date."') "
            .  " AND status = ? "
            .  " AND id_user = ? ";

            $query = $this->db->query($this->sql, array('Approved', $q->id_user))->result();
            $q->vacation = $query;
            
        }
        
        return $qr;
    }


    /*


    */ 


    public function manager_view_vacation_location($id_company, $id_manager, $id_locations, $month)
    {
        $usr = $this->get('employees', array('id_user'=>$id_manager));

        $date = date('Y-m-d', strtotime($month));
        $this->db->select('u.id_user, u.first_name, u.last_name, u.status');
        $this->db->from('users u');
        $this->db->join('employees e', 'e.id_user = u.id_user', 'left');
        $this->db->where('e.id_company', $id_company);

        if($usr[0]->id_access_level != ACCOUNT_TYPE_ADMINISTRATOR AND $usr[0]->id_access_level != ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR){
            $this->db->where('e.id_manager', $id_manager);
        }

        // foreach ($id_locations as $k => $l) {
        //     if($k == 0){
        //         $this->db->where('u.id_company_location', $l->id_location);
        //     }
        //     else{
        //         $this->db->or_where('u.id_company_location', $l->id_location);
        //     }
        // }
        
        $qr = $this->db->get()->result();

        foreach ($qr as $q) {
            $this->sql = " SELECT * "
            .  " FROM vacation "
            .  " Where MONTH(date_start) = Month('".$date."') "
            .  " AND status = ? "
            .  " AND id_user = ? ";

            $query = $this->db->query($this->sql, array('Approved', $q->id_user))->result();
            $q->vacation = $query;
        }
        return $qr;
    }



    /*


    */ 


    public function manager_view_vacation_location2($id_company, $id_manager, $id_location, $month)
    {
        $usr = $this->get('employees', array('id_user'=>$id_manager));

        $date = date('Y-m-d', strtotime($month));
        $this->db->select('u.id_user, u.first_name, u.last_name, u.status');
        $this->db->from('users u');
        $this->db->join('employees e', 'e.id_user = u.id_user', 'left');
        $this->db->where('e.id_company', $id_company);

        if($usr[0]->id_access_level != ACCOUNT_TYPE_ADMINISTRATOR AND $usr[0]->id_access_level != ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR){
            $this->db->where('e.id_manager', $id_manager);
        }

        if($id_location > 0){
            $this->db->where('u.id_company_location', $id_location);
        }

                
        $qr = $this->db->get()->result();

        foreach ($qr as $q) {
            $this->sql = " SELECT * "
            .  " FROM vacation "
            .  " Where MONTH(date_start) = Month('".$date."') "
            .  " AND status = ? "
            .  " AND id_user = ? ";

            $query = $this->db->query($this->sql, array('Approved', $q->id_user))->result();
            $q->vacation = $query;
        }
        return $qr;
    }



    /*


    */ 


    public function manager_view_vacation_department($id_company, $id_dept, $id_user, $month)
    {
        $usr = $this->get('employees', array('id_user'=>$id_user));

        $date = date('Y-m-d', strtotime("1 ".$month));
        $this->db->select('u.id_user, u.first_name, u.last_name, u.status');
        $this->db->from('users u');
        $this->db->join('employees e', 'e.id_user = u.id_user', 'left');
        $this->db->where('e.id_company', $id_company);
        if($usr[0]->id_access_level != ACCOUNT_TYPE_ADMINISTRATOR AND $usr[0]->id_access_level != ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR){
            foreach ($id_dept as $k => $d) {
                if($k == 0){
                    $this->db->where('e.id_dept', $d->deptid);
                }
                else{
                    $this->db->or_where('e.id_dept', $d->deptid);
                }
            }
            
        }
        
        $qr = $this->db->get()->result();

        foreach ($qr as $q) {
            $this->sql = " SELECT * "
            .  " FROM vacation "
            .  " Where MONTH(date_start) = Month('".$date."') "
            .  " AND status = ? "
            .  " AND id_user = ? ";

            $query = $this->db->query($this->sql, array('Approved', $q->id_user))->result();
            $q->vacation = $query;
        }
        return $qr;
    }


    /*


    */ 


    public function manager_view_vacation_department2($id_company, $id_dept, $id_user, $month)
    {
        $usr = $this->get('employees', array('id_user'=>$id_user));
        $dept = $this->get('departments', array('id_head'=>$id_user, 'id_company'=>$id_company));

        $date = date('Y-m-d', strtotime("1 ".$month));
        $this->db->select('u.id_user, u.first_name, u.last_name, u.status');
        $this->db->from('users u');
        $this->db->join('employees e', 'e.id_user = u.id_user', 'left');
        $this->db->where('e.id_company', $id_company);

        if($id_dept > 0){
            $this->db->where('e.id_dept', $id_dept);
        }
        else{
            
            if($usr[0]->id_access_level != ACCOUNT_TYPE_ADMINISTRATOR AND $usr[0]->id_access_level != ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR){

                
                foreach ($dept as $k => $d) {
                    if($k == 0){
                        $this->db->where('e.id_dept', $d->deptid);
                    }
                    else{
                        $this->db->or_where('e.id_dept', $d->deptid);
                    }
                }
                
            }
        }     

        
        $qr = $this->db->get()->result();

        foreach ($qr as $q) {
            $this->sql = " SELECT * "
            .  " FROM vacation "
            .  " Where MONTH(date_start) = Month('".$date."') "
            .  " AND status = ? "
            .  " AND id_user = ? ";

            $query = $this->db->query($this->sql, array('Approved', $q->id_user))->result();
            $q->vacation = $query;
        }
        return $qr;
    }



    
    /*
     * Get employees info of a company
     * 
     * @access public 
     * @param  $id_type, $params
     * @return mixed (bool | array)
     */
    public function get_planner_report_history($id_company){

        if(!is_numeric($id_company)){
            return false;
        }

        $this->sql = "SELECT pln.*, e.id_dept, e.id_role, e.id_user, e.id_country_code, e.work_number, l.location_tag, r.id_role, r.roles, u.email, u.id_string, u.id_company_location, u.status, 
                    u.first_name, u.last_name, d.departments "
                    . "FROM vacation_planner_report_history pln "
                    . "LEFT JOIN employees e on e.id_user = pln.id_user "
                    . "LEFT JOIN users u on u.id_user = e.id_user "
                    . "LEFT JOIN departments d on e.id_dept = d.deptid "
                    . "LEFT JOIN roles r on e.id_role = r.id_role "
                    . "LEFT JOIN company_locations l on u.id_company_location = l.id_location "
                    . "WHERE pln.id_company = ? ";
        
        $query = $this->db->query($this->sql, array($id_company));
        return $query->result();


    }// End func get_employees



    /*
     * Get employees info of a company
     * 
     * @access public 
     * @param  $id_type, $params
     * @return mixed (bool | array)
     */
    public function get_employee($id_user){

        if(!is_numeric($id_user)){
            return false;
        }

        $this->sql = "SELECT e.id_dept, e.id_role, e.id_user, l.location_tag, r.id_role, r.roles, u.email, u.id_string, u.id_company_location, u.status, 
                    u.first_name, u.last_name, d.departments "
                    . "FROM users u "
                    . "LEFT JOIN employees e on e.id_user = u.id_user "
                    . "LEFT JOIN departments d on e.id_dept = d.deptid "
                    . "LEFT JOIN roles r on e.id_role = r.id_role "
                    . "LEFT JOIN company_locations l on u.id_company_location = l.id_location "
                    . "WHERE u.id_user = ? ";
        
        $query = $this->db->query($this->sql, array($id_user));
        return $query->row();


    }// End func get_employees





    
} // End class Vacation_model

