<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Talentbase
 * Company Model
 * 
 * @category   Model
 * @package    Company
 * @subpackage Profile
 * @author     David A. <tokernel@gmail.com>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 * @property CI_DB_active_record $db database
 */
class Company_model extends CI_Model {

    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

// End func __construct

    public function module_enabled($id_company, $id_module) {
        $where = array('m.status' => 1, 'cm.status' => 1, 'cm.id_company' => $id_company);
        if (is_numeric($id_module)) {
            $where['m.id_module'] = $id_module;
        } else {
            $where['m.id_string'] = $id_module;
        }

        $result = $this->db
                ->select('cm.id_company,cm.id_module')
                ->from('company_modules AS cm')
                ->join('modules AS m', 'm.id_module=cm.id_module')
                ->where($where)
                ->get()
                ->result()
        ;

        return !empty($result);
    }

// End func module_enabled

    /**
     * Fetch companies by criteria
     *
     * @access public
     * @return array
     */
    public function fetch_companies($where = array()) {

        $sql = "select * from companies ";

        if (!empty($where)) {
            $sql .= " where ";

            foreach ($where as $field => $value) {
                $sql .= $field .= " = '" . $value . "' and ";
            }

            $sql = rtrim($sql, ' and ');
        }

        $result = $this->db->query($sql)->result();

        return $result;
    }

// End func fetch_companies

    public function load_company($subdomain, $child_subdomain = null) {

        $sql = "select * from companies where id_string = '" . $subdomain . "' ";

        $result = $this->db->query($sql)->result();

        if (!$result) {
            return false;
        }

        $result[0]->subdomain = $subdomain;
        $result[0]->child_subdomain = '';

        if (!$child_subdomain) {
            return $result;
        }

      
        $sub_result = $this->db->get_where('companies', array(
            'id_own_company' => $result[0]->id_company,
            'id_string' => $child_subdomain,
        ))->result();

        if (!$sub_result) {
            return $result;
        }

        $sub_result[0]->child_subdomain = $child_subdomain;

        return $sub_result;
    }

// End func load_company

    /**
     * Fetch Company profile by array values from companies table.
     * Return false if record not exists.
     * 
     * @access public
     * @param array $query_fields
     * @return mixed (bool | array)
     */
    public function fetch_profile(array $query_fields) {

        if (empty($query_fields)) {
            trigger_error('query fields cannot be empty!', E_USER_WARNING);
        }

        $sql = "SELECT * "
                . "FROM companies "
                . "WHERE id_company <> 0 ";

        foreach ($query_fields as $field => $value) {

            $sql .= "AND " . $field . " = '" . $value . "' ";
        }

        $result = $this->db->query($sql)->result();

        if (empty($result)) {
            return FALSE;
        } else {
            return $result;
        }
    }

// End func fetch_profile

    public function fetch_company_modules_ids($id_company) {

        $sql = "select * from company_modules where id_company = '" . $id_company . "' ";

        $result = $this->db->query($sql)->result();

        if (!$result) {
            return false;
        }

        $mudules = array();

        foreach ($result as $m) {
            $mudules[] = $m->id_module;
        }

        return $mudules;
    }

// End func fetch_company_modules_ids

    /**
     * Fetch company profile info by id_company
     * 
     * @access public
     * @param int $id_company
     * @return mixed (array | bool)
     */
    public function fetch_by_id($id_company) {

        $result = $this->fetch_profile(
                array(
                    'id_company' => $id_company
        ));

        if (empty($result)) {
            return false;
        }

        return $result[0];
    }

// End func fetch_by_id

    /**
     * Update company profile info by id_company
     * 
     * @access public
     * @param int $id_company
     * @param array $data
     * @return mixed (array | bool)
     */
    public function update_company_info($id_company, array $data) {

        if (empty($data) or ! is_numeric($id_company)) {
            return false;
        }

        return $this->db->update("companies", $data, array('id_company' => $id_company));
    }

// End func update_company_info

    /**
     * Update company's logo format
     * 
     * @access public
     * @return bool
     */
    public function update_logo_path(array $params) {

        if (empty($params)) {
            return false;
        }

        return $this->db->update('companies', $params['data'], array('id_company' => $params['id_company']));
    }

// End func update_logo_path

    /**
     * Update company's banner image
     * 
     * @access public
     * @param array $params 
     * @return int 
     */
    public function update_banner_path(array $params) {

        if (empty($params)) {
            return false;
        }

        return $this->db->update('companies', $params['data'], array('id_company' => $params['id_company']));
    }

// End func update_banner_path

    /**
     * Insert or Update Admin profile data
     * 
     * @access public
     * @param array $data
     * @return bool
     */
    public function save_profile(array $data) {

        if (isset($data['id_company'])) {
            return $this->update_profile($data);
        } else {
            return $this->insert_profile($data);
        }
    }

// End func save_profile

    public function update_profile(array $data) {
        
    }

// End func update_profile

    public function insert_profile(array $data) {
        
    }

// End func insert_profile
}

// End class Company_model

// End of file company_model.php
