<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Talentbase
 * Reference Model - Fetching reference data from any table.
 * 
 * @category   Model
 * @package    Reference
 * @subpackage Reference
 * @author     David A. <tokernel@gmail.com>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

class Reference_model extends CI_Model {
	
	/**
	 * Class constructor
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		
	}// End func __construct
	
	public function fetch_all_records($table, $where = NULL) {
		
		if(!$this->db->table_exists($table)){
			trigger_error("Table `".$table."` not exists.", E_USER_ERROR);
		}
		
		if(is_null($where)) {
			$result = $this->db->get($table)->result();
		} else {
			$result = $this->db->get_where($table, $where)->result();
		}	
		
		if(empty($result)){
			return false;
		}
		
		return $result;
		
	} // End func fetch_all_records
        
}// End class Event_model
