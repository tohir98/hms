<?php

use db\Entity;

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Talentbase
 * User Model
 * 
 * @category   Model
 * @package    Users
 * @subpackage User
 * @author     David A. <tokernel@gmail.com>
 * @copyright  Copyright Ã‚Â© 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 * @property CI_DB_active_record $db database.
 */
class User_model extends CI_Model {

    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

// End func __construct

    /**
     * Insert log message of user
     * 
     * @access public
     * @param type $id_user
     * @param type $message
     * @param type $type 
     * @return void
     */
    public function log_db($id_user, $message, $type) {

        $this->db->insert(
            'user_logs', array(
            'id_user' => $id_user,
            'message' => $message,
            'type' => $type,
            'log_date' => date('Y-m-d H:i:s')
            )
        );
    }

// End func log_db

    public function can_edit_email($id_company) {

        $sql = "SELECT * FROM companies WHERE id_company = '" . $id_company . "' AND id_own_company = 0";
        $result = $this->db->query($sql)->result();

        if (!$result) {
            return false;
        } else {
            return true;
        }
    }

// End func can_edit_email

    /**
     * Return all company user's count  
     */
    public function count($id_company) {

        $sql = "select count(*) as cnt from users where id_company = '" . $id_company . "' ";

        $result = $this->db->query($sql)->result_array();
        return $result[0]['cnt'];
    }

// End func count

    public function insert_default_perms($id_company, $id_user, $user_default_perms) {

        if (empty($user_default_perms)) {
            trigger_error('Default permissions is empty.', E_USER_WARNING);
            return false;
        }

        $sql = " insert into user_perms (id_user, id_company, id_module, id_perm) values ";

        foreach ($user_default_perms as $module_id => $perms) {
            foreach ($perms as $perm_name => $perm_id) {
                $sql .= "('" . $id_user . "', '" . $id_company . "', '" . $module_id . "', '" . $perm_id . "'),";
            }
        }

        $sql = rtrim($sql, ',');

        return $this->db->query($sql);
    }

// End func insert_default_perms

    public function last_id_string($id_company) {
        $sql = "select id_string from users where id_company = '" . $id_company . "' order by id_string desc limit 1 ";

        $result = $this->db->query($sql)->result_array();

        if (!$result) {
            return '';
        }

        return $result[0]['id_string'];
    }

// End func last_id_string

    public function fetch_company_admin($id_company) {

        $sql = "Select * from users 
				where id_company = '" . $id_company . "'
				and account_type = '" . USER_TYPE_ADMIN . "' ";

        $result = $this->db->query($sql)->result();

        if (!$result) {
            return false;
        }

        return $result[0];
    }

// End func fetch_company_admin

    /**
     * Fetch user account by array values from users table.
     * Return false if record not exists.
     * 
     * EXAMPLE 1:
     * 
     * $query_fields = array(
     *                  'email' => 'user@example.com', 
     *                  'password' => 'user_password'
     *                 );
     * 
     * This will create query with email and password fields.
     * 
     * EXAMPLE: 2
     * 
     * $query_fields = array(
     *                   'id_user' => 55
     *                 );
     * 
     * This will create query with id only.
     * 
     * This approach gives us possiblity to get user record with many different 
     * criterias instead of creating many functions like:
     * 
     * fetch_account_by_email_password()
     * fetch_account_by_id()
     * 
     * @access public
     * @param array $query_fields
     * @return mixed (bool | array)
     */
    public function fetch_account(array $query_fields) {
        if (empty($query_fields)) {
            trigger_error('query fields cannot be empty!', E_USER_WARNING);
        }
        return $this->db->get_where('users', $query_fields)->result();
    }

// End func fetch_account        

    /**
     * Checks if user exists in company's sub-companies - Chuks
     * 
     * @access public
     * @param array $params
     * @return mixed (bool | array)
     */
    public function fetch_sub_accts(array $params) {

// Fetch all company's sub-companies
        $subs = $this->db->get_where('companies', array('id_own_company' => $params['id_company']))->result();

        if (empty($subs)) {
            return false;
        }

        $pass = '';

        if (isset($params['password'])) {
            $pass = $params['password'];
        }

        foreach ($subs as $sub) {
            $sql = "select u.*, c.company_name, c.id_string as company_id_string, c.status as company_status, c.cdn_container as cdn_container "
                . "from users u, companies c "
                . "where u.id_company = c.id_company "
                . "and u.id_company = " . $sub->id_company . " "
                . "and email = '" . $params['email'] . "' "
                . "and password = '" . $pass . "' ";

            $ret = $this->db->query($sql)->result();

            if (!empty($ret)) {
                return $ret;
            }
        }

        return false;
    }

// End func fetch_account_sub_accts

    /**
     * fetch user groups, modules and permissions
     * 
     * @access public
     * @param user id
     * @param account type 
     * @return bool
     */
    public function fetch_user_groups_modules_perms($id_user, $id_company) {

        if (empty($id_user)) {
            return false;
        }
        /*
          $sql = "select
          g.subject as group_subject,
          m.id_module,
          m.subject as module_subject,
          m.id_string as module_id_string,
          p.subject as perm_subject,
          p.id_perm,
          p.id_string as perm_id_string,
          p.in_menu,
          u.id_user
          from user_perms u
          left join module_perms p on p.id_perm = u.id_perm
          and p.in_menu = 1
          left join modules m on m.id_module = p.id_module
          left join module_group g on g.id_group = m.id_group
          where u.id_user = ". $id_user ."
          and u.id_company = ".$id_company."
          group by u.id_perm
          order by g.id_sort, m.id_module ";
         */

        $sql = "SELECT
                	g.subject AS group_subject,
	                m.id_module,
                    m.subject AS module_subject,
                    m.id_string AS module_id_string,
	                p.subject AS perm_subject,
                    p.id_perm,
	                p.id_string AS perm_id_string,
	                p.in_menu,
	                u.id_user
                    FROM user_perms u
                    LEFT JOIN module_perms p ON p.id_perm = u.id_perm
                    AND p.in_menu = 1
                    LEFT JOIN modules m ON m.id_module = p.id_module
                    LEFT JOIN module_group g ON g.id_group = m.id_group
                    WHERE u.id_company = " . $id_company . " AND u.id_module IN (
	                    SELECT id_module FROM company_modules WHERE id_company = " . $id_company . "
                    )
                    AND u.id_user = " . $id_user . "
                    GROUP BY u.id_perm
                    order by g.id_sort, m.menu_order, m.subject, p.menu_order, p.subject
                    ";

        $result = $this->db->query($sql)->result_array();

        if (empty($result)) {
            return false;
        }

        return $result;
    }

// End func fetch_user_modules_perms

    public function insert_perms_by_modules($id_company, $id_user, $modules) {

//$ids = array();

        $modules_str = implode(',', $modules);

        $ins_sql = "insert into user_perms (id_user, id_perm, id_module, id_company) values ";

        $module_perms = $this->db->query("select * from module_perms where id_module in (" . $modules_str . ") ")->result();

        foreach ($module_perms as $p) {
            if ($p->status != ACCESS_CONTROL_MODULE_STATUS_RESTRICTED) {
                $ins_sql .= "(" . $id_user . ", " . $p->id_perm . ", " . $p->id_module . ", " . $id_company . "), ";
            }
        }

        $ins_sql = rtrim($ins_sql, ', ');

        $this->db->query($ins_sql);
    }

// End func insert_perms_by_modules

    public function cleanup_temp_perms($date) {

        $this->db->query("DELETE FROM user_perms_temp
							WHERE date_expire < '" . $date . "'
							ORDER BY date_expire");

        return $this->db->affected_rows();
    }

// End func cleanup_temp_perms

    /**
     * Get user permission
     * 
     * @access public
     * @param id_user
     * @param id_perm 
     * @return bool
     */
    public function get_user_perm(array $param_fields) {

        if (empty($param_fields)) {
            return false;
        }

        $result = $this->db->get_where("user_perms", $param_fields)->result();

        if (!empty($result)) {
            return true;
        }

        $result = $this->db->get_where("user_perms_temp", $param_fields)->result();

        if (!empty($result)) {
            return true;
        }

        return false;
    }

// End func get_user_perm

    public function fetch_perms_temp($where) {

        return $this->db->get_where("user_perms_temp", $where)->result();
    }

// End func fetch_temp_perm

    public function delete_perm_temp($where) {

        $res = $this->fetch_perms_temp($where);

        if (!empty($res)) {
            $this->db->update('tasks', array('status' => TASK_STATUS_COMPLETE), array('tb_id_record' => $res[0]->id_perms_temp));
        }

        $this->db->delete('user_perms_temp', $where);
    }

// End func delete_perm_temp

    public function insert_perm_temp($data) {

        $this->db->insert_batch('user_perms_temp', $data);
    }

// End func insert_perm_temp

    public function replace_perm_temp($id_perm, $data) {

        $result = $this->db->get_where('user_perms_temp', array(
                'id_perm' => $id_perm,
                'id_user' => $data['id_user'],
                'id_company' => $data['id_company']
                )
            )->result();

        if (!empty($result)) {
            $this->db->delete('user_perms_temp', array(
                'id_perm' => $id_perm,
                'id_user' => $data['id_user'],
                'id_company' => $data['id_company']
            ));
        }

//		$this->insert_perm_temp (
//			array (
//				$data
//			)	
//		);

        $q = $this->insert_perm_temp_2(
            array(
                $data
            )
        );

        return $q;
    }

// End func replace_perm_temp

    /**
     * 
     * @param array $data
     * @return boolean
     */
    public function insert_perm_temp_2(array $data) {

        if (empty($data)) {
            return false;
        }

        $i = array();

        $this->db->trans_start();

        foreach ($data as $values) {
            $this->db->insert('user_perms_temp', $values);
            $i[] = $this->db->insert_id();
        }

        $this->db->trans_complete();

        return $i;
    }

// End func insert_perm_temp_2 -- Chuks

    /**
     * Save user data.
     * If id_user exists in array, than it will call update function.
     * Else, will call insert function.
     * 
     * @access public
     * @param array $data 
     * @return bool
     */
    final public function save(array $data, $where = null) {

        if (!is_null($where)) {
            return $this->update($data, $where);
        } else {
            return $this->insert($data);
        }
    }

// End func save

    final protected function insert(array $data) {
        $this->db->trans_begin();
        $this->db->insert('users', $data);
        return $this->db->insert_id();
    }

    public function trans_complete() {
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            trigger_error('Cannot insert account.' . E_USER_ERROR);
        } else {
            $this->db->trans_commit();
        }
    }

    final protected function update(array $data, $where) {
        $this->db->update('users', $data, $where);
    }

// End func update

    public function update_sub_company_emails($id_own_company, $current_email, $new_email) {

        $sql = "
              UPDATE users
              SET email = '" . $new_email . "'
              WHERE email = '" . $current_email . "'
              AND id_company IN(SELECT id_company FROM companies WHERE id_own_company = '" . $id_own_company . "') ";

        $this->db->query($sql);
    }

// End func update_sub_company_emails

    public function email_exists_in_sub_accounts($email, $id_user = NULL, $id_company) {

        $sql = "SELECT * FROM users
                WHERE email = '" . $email . "'
                AND id_company NOT IN (SELECT id_company FROM companies WHERE id_own_company = '" . $id_company . "') ";

        if (!is_null($id_user)) {
            $sql .= " AND id_user <> '" . $id_user . "' ";
        }

        $result = $this->db->query($sql)->result();

        if (!$result) {
            return false;
        } else {
            return true;
        }
    }

// End func email_exists_in_sub_accounts

    public function email_exists($email, $id_user = NULL, $id_company = NULL) {

        $sql = "SELECT id_user from users where email = '" . $email . "' ";

        if (!is_null($id_user)) {
            $sql .= " and id_user <> '" . $id_user . "' ";
        }

        if (!is_null($id_company)) {
            $sql .= " AND id_company NOT IN (SELECT id_company FROM companies WHERE id_own_company = '" . $id_company . "') AND id_company = '" . $id_company . "' ";
        }

        $result = $this->db->query($sql)->result();

        if (empty($result)) {
            return false;
        } else {
            return true;
        }
    }

    public function isParent($id_company) {
        $sql = "SELECT id_company FROM companies WHERE id_own_company = '0' and id_company = '" . $id_company . "' ";
        $result = $this->db->query($sql)->result();
        if (empty($result)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function isCompanyAdmin($id_user) {
        $sql = "select id_user from users where id_user = '" . $id_user . "' and account_type = '" . ACCOUNT_TYPE_ADMINISTRATOR . "' AND id_company = '" . $this->user_auth->get('id_company') . "' ";
        $result = $this->db->query($sql)->result();
        if (empty($result)) {
            return false;
        } else {
            return true;
        }
    }

// End func email_exists

    /**
     * Check user permission
     * 
     * @access public
     * @return void 
     */
    public function check_perms($id_user, $id_company) {

        if ($id_company == '' or $id_user == '') {
            return false;
        }

        $sql = "select u.*, c.*, p.id_perm "
            . "from users u, user_perms p, companies c "
            . "where u.id_user = p.id_user "
            . "and c.id_company = u.id_company "
            . "and u.id_user = " . $id_user . " "
            . "and p.id_company = " . $id_company;

        $result = $this->db->query($sql)->result();

        if (empty($result)) {
            return false;
        }

        return $result;
    }

// End func check_perms

    /**
     * Validate user entry 
     * 
     * @access public
     * @return mixed (bool | array)
     */
    public function fetch_user_client_details(array $query_fields) {

        if (empty($query_fields)) {
            return false;
        }

        return $this->db->get_where('users', $query_fields)->result();
    }

// End func validate_id_string

    /**
     * Fetch client company info
     * 
     * @access public
     * @return mixed (bool | array)
     */
    public function fetch_client_company_info(array $query_fields) {

        if (empty($query_fields)) {
            return false;
        }

        return $this->db->get_where('companies', $query_fields)->result();
    }

// End func fetch_client_company_info

    /**
     * get_accout types
     * 
     * @access public
     * @return mixed (bool | array)
     */
    public function get_account_types($id_account_type) {

        return $this->db->get_where('accounts', array('accounttype' => $id_account_type))->row();
    }

// End func get account types

    /**
     * get access level
     * 
     * @access public
     * @return mixed (bool | array)
     */
    public function get_access_level($id_user) {

        return $this->db->get_where('employees', array('id_user' => $id_user))->row();
    }

// End func get account types

    public function fetch_user_role_dept_details(array $params) {

        if (empty($params)) {
            return false;
        }

        $sql = "select r.roles, d.departments "
            . "from roles r, departments d, employees e "
            . "where e.id_role = r.id_role "
            . "and e.id_dept = d.deptid "
            . "and e.id_user = " . $params['id_user'] . " "
            . "and e.id_company = " . $params['id_company'];

        $res = $this->db->query($sql)->result();

        if (!empty($res)) {
            return $res[0];
        }

        return false;
    }

// End func fetch_user_role_dept_details

    public function get_user_sub_companies($user_email, $id_company) {

        $sql = "SELECT
	              c.id_string as c_id_string,
	              u.id_string as u_id_string,
	              c.*,
	              u.*,
                  e.id_access_level
                FROM
	            companies c, users u, employees e
                WHERE
	            c.id_own_company = '" . $id_company . "'
	            AND c.id_company = u.id_company
	            AND e.id_user = u.id_user
	            AND c.status = 1
	            AND u.email = '" . $user_email . "'
                group by c.id_company
	            ";

        return $this->db->query($sql)->result();
    }

    public function get_all_companies($user_email, $id_company, $rowClass = Entity::class) {
        $company = $this->db->get_where('companies', array(
                'id_company' => intval($id_company),
            ))->result()[0];

        if (!$company) {
            return [];
        }

        $parentId = $company->id_company;

        if ($company->id_own_company) {
            $parentId = intval($company->id_own_company);
        }

        return $this->db
                ->select('c.id_string as c_id_string, u.id_string as u_id_string,c.*,u.*,e.id_access_level')
                ->from('companies c')
                ->join('users u', 'c.id_company=u.id_company')
                ->join('employees e', 'e.id_user = u.id_user AND e.id_company=c.id_company')
                ->where("u.email", $user_email)
                ->where("(c.id_company = {$parentId} OR c.id_own_company={$parentId})", null, false)
                //->group_by('c.id_company')
                ->order_by('c.id_own_company, c.company_name')
                ->get()
                ->result($rowClass)
        ;
    }

    public function get_user_role_in_company($id_user, $id_company) {
        $sql = "SELECT r.roles FROM companies c, employees e, roles r WHERE r.id_role = e.id_role AND e.id_user = '" . $id_user . "' AND c.id_company = e.id_company AND c.id_company = '" . $id_company . "' ";
        return $this->db->query($sql)->result();
    }

    public function fetch_user_and_company_data($u_id_string, $c_id_string) {
        $sql = "SELECT
            u.id_string as u_id_string,
             u.status as u_status,
             c.id_string as c_id_string,
             u.*,
             c.*
            FROM
            users u, companies c
            WHERE u.id_string = '" . $u_id_string . "' AND c.id_string = '" . $c_id_string . "' AND u.id_company = c.id_company ";

        return $this->db->query($sql)->result();
    }

// End func fetch_user_and_company_data

    public function assignAllPermissions($id_user, $id_company) {

        if ($id_user == '' || $id_company == '') {
            return false;
        }

        $sql = "SELECT
m.*
FROM
module_perms m, company_modules c
WHERE m.id_module = c.id_module AND c.id_company = " . $id_company . " ";

        $modules = $this->db->query($sql)->result();

        $ids = $this->get_user_perms($id_user, $id_company);
        $data = [];

        if (!empty($modules)) {
            foreach ($modules as $module) {
                if (!in_array($module->id_perm, $ids)) {
                    $data[] = [
                        'id_user' => $id_user,
                        'id_perm' => $module->id_perm,
                        'id_module' => $module->id_module,
                        'id_company' => $id_company
                    ];
                }
            }
        }

        if (!empty($data)) {
            $this->db->insert_batch('user_perms', $data);
        }
    }

    public function get_user_perms($id_user, $id_company) {

        $results = $this->db->get_where('user_perms', array('id_user' => $id_user, 'id_company' => $id_company))->result();

        $ids = array();

        if (!empty($results)) {
            foreach ($results as $res) {
                $ids[] = $res->id_perm;
            }
            return $ids;
        }

        return false;
    }

    /**
     * @author Obinna Ukpabi <obinna.ukpabi@talentbase.ng>
     */
    public function assignEmployeeDefaultPermissions($id_user, $id_company, $default_perms) {

        $user = $this->db->get_where('employees', array('id_user' => $id_user, 'id_company' => $id_company))->result();
        if ($user[0]->account_type !== ACCOUNT_TYPE_ADMINISTRATOR) {
            //check if have basic default permission
            foreach ($default_perms as $id_module => $perm) {

                foreach ($perm as $key => $id_perm) {

                    $check_perm = $this->db->get_where('user_perms', array('id_perm' => $id_perm, 'id_company' => $id_company, 'id_module' => $id_module, 'id_user' => $id_user))->result();

                    if (empty($check_perm)) {

                        $perm_data = array(
                            'id_user' => $id_user,
                            'id_module' => $id_module,
                            'id_company' => $id_company,
                            'id_perm' => $id_perm
                        );
                        $this->db->insert('user_perms', $perm_data);
                    }
                }
            }
        }
    }

//end modification by obinna 

    /**
     * Save user data in temp table.
     * If id_user exists in array, than it will call update function.
     * Else, will call insert function.
     * 
     * @access public
     * @param array $data 
     * @return bool
     */
    final public function save_temp(array $data, $where = null) {

        if (!is_null($where)) {
            return $this->update_temp($data, $where);
        } else {
            return $this->insert_temp($data);
        }
    }

// End func save

    final protected function insert_temp(array $data) {
        $this->db->trans_begin();
        $this->db->insert('users_temp', $data);
        return $this->db->insert_id();
    }

    final protected function update_temp(array $data, $where) {
        $this->db->update('users_temp', $data, $where);
    }

// End func update

    public function fetch_employee_sub_acccts(array $data_, $email, $id_company) {
        $sql = "select * from users WHERE email = '" . $email . "' AND id_company IN (SELECT id_company FROM companies WHERE id_own_company = '" . $id_company . "' )";

        $result = $this->db->query($sql)->result();

        if (empty($result)) {
            return;
        }

        foreach ($result as $res) {
            $this->save(
                $data_, array(
                'id_user' => $res->id_user
                )
            );
        }
    }

    public function fetch_sub_companies($id_company) {
        return $this->db->get_where('companies', ['id_own_company' => $id_company])->result();
    }

    public function fetch_company_groups($id_company, $email) {
        $sql = "SELECT c.id_string c_id_string, u.id_string u_id_string, c.company_name
                    FROM companies c, users u
                    WHERE c.id_own_company = (SELECT id_own_company FROM companies WHERE id_company = '" . $id_company . "')
                    AND c.id_company = u.id_company
                    AND u.email = '" . $email . "' ";
        return $this->db->query($sql)->result();
    }

    public function get_parent_company($id_company, $email) {

        return $this->db->select('u.id_string u_id_string, c.id_string c_id_string, c.company_name')
                ->from('users u')
                ->join('companies c', ' u.id_company = c.id_company')
                ->where('u.id_company', $id_company)
                ->where('u.email', $email)->get()->result();
    }

    public function get_company_parent($id_company, $email) {
        $sql = "SELECT u.id_string u_id_string, c.id_string c_id_string, c.company_name
                    FROM users u, companies c
                    WHERE u.id_company = (
                    SELECT id_own_company
                    from companies
                    WHERE id_company = '" . $id_company
            . "' ) AND u.email = '" . $email . "' AND u.id_company = c.id_company";

        return $this->db->query($sql)->result();
    }

    public function fetch_account_info($id_company, $params = array(), $limit = PAGINATION_PER_PAGE, $offset = 0, $rowClass = '\stdClass', $activeOnly = false) {

        $params['u.id_company'] = (int) $id_company;

        if ($activeOnly) {
            $this->db->where_in('u.status', [USER_STATUS_ACTIVE, USER_STATUS_PROBATION_ACCESS]);
        }

        foreach ($params as $key => $param) {
            if (is_array($param)) {
                $this->db->where_in($key, $param);
            } else {
                $this->db->where($key, $param);
            }
        }

        if ($limit) {
            $this->db->limit($limit, $offset);
        }

        return $this->db
                ->select('u.*, e.*, r.roles, d.departments, cl.address as location_address, cl.city as location_city, s.state AS location_state, c.cdn_container, c.company_name')
                ->from('users u')
                ->join('employees e', 'e.id_user = u.id_user AND e.id_company=u.id_company')
                ->join('companies c', 'c.id_company = u.id_company')
                ->join('roles r', 'e.id_role = r.id_role AND r.id_company=e.id_company', 'left')
                ->join('departments d', 'd.deptid = e.id_dept AND d.id_company=e.id_company', 'left')
                ->join('company_locations cl', 'cl.id_location = u.id_company_location AND cl.id_company=u.id_company', 'left')
                ->join('states s', 's.id_state = cl.id_state', 'left')
                //->limit($limit, $offset)
                //->where($params)
                ->get()
                ->result($rowClass);
    }

    public function setSecondaryPermissions($id_user, $idCompany = '') {

        $id_company = $idCompany === '' ? $this->user_auth->get('id_company') : $idCompany;

        $admin_id = $this->getCompanyAdmin($id_company)->id_user;

        $sql = "INSERT INTO user_perms (id_perm, id_module, id_company, id_user) SELECT id_perm, id_module, $id_company, $id_user FROM user_perms where id_user = '" . $admin_id . "' and id_module NOT IN (11)";

        return $this->db->query($sql);
    }

    public function getCompanyAdmin($id_company) {
        return $this->db->get_where('users', ['id_company' => $id_company, 'account_type' => ACCOUNT_TYPE_ADMINISTRATOR])->row();
    }

}

// End class User_model
// End file user_model.php
