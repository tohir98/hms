<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Talentbase
 * Admin Model extends User_model (models/user/user_model.php)
 * 
 * @category   Model
 * @package    User
 * @subpackage Admin
 * @author     David A. <tokernel@gmail.com>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

class Admin_model extends User_model
{
	
	/**
	 * Class constructor
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		
	} // End func __construct

	/**
	 * Fetch Admin profile by array values from admins table.
	 * Return false if record not exists.
	 * 
	 * EXAMPLE 1:
	 * 
	 * $query_fields = array(
	 *                  'id_user' => 55, 
	 *                  'id_company' => 77
	 *                 );
	 * 
	 * This will create query with id_user and id_company fields.
	 * 
	 * EXAMPLE: 2
	 * 
	 * $query_fields = array(
	 *                   'id_user' => 55
	 *                 );
	 * 
	 * This will create query with id only.
	 * 
	 * This approach gives us possiblity to get admin record with many different 
	 * criterias instead of creating many functions like:
	 * 
	 * fetch_profile_by_id_user_company()
	 * fetch_profile_by_id()
	 * 
	 * @access public
	 * @param array $query_fields
	 * @return mixed (bool | array)
	 */
	public function fetch_profile(array $query_fields) {
		
	} // End func fetch_profile
	
	/**
	 * Insert or Update Admin profile data
	 * 
	 * @access public
	 * @param array $data
	 * @return bool
	 */
	public function save_profile(array $data) {
	
		// If in controller you're working with Admin and user 
		// data (Employer profile and Employer account), than for 
		// updating data in users table you need to call $this->save($data) 
		// (which updates user account data) and than do saving 
		// functionality here for Admin profile.
		
		// In other words, save() is the function for account 
		// (works with users table) and the save_profile() is the 
		// function to insert/update Admin profile data.
		
		if(isset($data['id_user'])) {
			return $this->update_profile($data);
		} else {
			return $this->insert_profile($data);
		}
		
	} // End func save_profile
	
	public function update_profile(array $data) {
		
	} // End func update_profile
	
	public function insert_profile(array $data) {
		
	} // End func insert_profile
	
} // End class Admin_model

// End of file admin_model.php
?>