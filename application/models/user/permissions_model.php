<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Talentbase
 * Permissions Model
 * 
 * @category   Model
 * @package    User
 * @subpackage Permissions
 * @author     David A. <tokernel@gmail.com>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

class Permissions_model extends CI_Model
{
	
	/**
	 * Class constructor
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		
	} // End func __construct

} // End class Permissions_model

// End of file permissions_model.php
?>