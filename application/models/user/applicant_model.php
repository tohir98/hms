<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Talentbase
 * Applicant Model
 * 
 * !!! THIS MODEL WORKS WITH "hired" DATABASE !!!
 * 
 * @category   Model
 * @package    Users
 * @subpackage Applicant
 * @author     David A. <tokernel@gmail.com>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

class Applicant_model extends CI_Model
{
	
	/**
	 * Instance of HIRED database
	 * 
	 * @access private
	 * @var object
	 */
	private $hired_db;
	 
	/**
	 * Class constructor
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->hired_db  = $this->load->database("default", TRUE);
		
	} // End func __construct
	
	public function get_all_table_records($table_name){
		
		if($table_name == '' or !$this->hired_db->table_exists($table_name)){
			return false;
		}
		
		$ret = $this->hired_db->get($table_name)->result();
		
		if(empty($ret)){
			return false;
		}
		
		return $ret;
		
	} // End func get_all_table_records
	
	public function getCandidateSkills($id){
        
        if(empty($id)){
            return array();
        }
        
        // Build sql query statement
        $sql = "SELECT skillid
            FROM candidateskills
            WHERE candidateid = ". $id;
        
        // Execute sql query statement
        $res = $this->talent_db->query($sql);
        // Return resultset
        return $res->result();
    }
	
	public function fetch_record($table, $id){
        
        $ret = $this->hired_db->get_where($table, array("candidateid" => $id))->result();
        
		if(!$ret) {
			return array();
		}
		
        return $ret;
    }
	
	/**
	 * Fetch applicant account by array values from candidate table.
	 * 
	 * @access public
	 * @param array $query_fields
	 * @return mixed (bool | array)
	 */
	public function fetch_account(array $query_fields) {
            
		if(empty($query_fields)) {
			trigger_error('query fields cannot be empty!', E_USER_WARNING);
		}
		
		$sql = "SELECT a.* "
              . "FROM hired_candidate a "
              . "WHERE a.candidateid <> - 99 ";
		
		foreach($query_fields as $field => $value) {
			
			if(substr($field, 0, 2) == 'a.') {
				$sql .= "AND ".$field." = '". $value ."' ";
			} else {
				$sql .= "AND a.".$field." = '". $value ."' ";
			}
			
		}
		                
		$result = $this->hired_db->query($sql)->result();
                
		if(empty($result)){
			return FALSE;
        } else {
			return $result;
        }
		
	} // End func fetch_account

	public function update_account(array $data, array $where) {
		
		$this->hired_db->update('hired_candidate', $data, $where);
		
	} // End func update_account
	
	public function insert_account(array $data) {
		
		$this->hired_db->insert('hired_candidate', $data);
		return $this->hired_db->insert_id();
		
	} // End func insert_account
	
	public function insert_data_batch($table_name, $data) {
		
		$this->hired_db->insert_batch($table_name, $data);
		
	} // End func insert_data
	
	public function fetch_course($id_course) {
		
		$sql = "select * from hired_courses where courseid = '".$id_course."' ";
		
		$result = $this->hired_db->query($sql)->result();
		
		if(!$result) {
			return false;
		}
		
		return $result[0]->name;
		
	} // End func fetch_course
    
    
    public function get_company_name($id_string){
        
        $sql = "SELECT company_name FROM companies WHERE id_string = '".$id_string."' ";
		
		$result = $this->db->query($sql)->row();
		
		if(!$result) {
			return false;
		}
		
		return $result->company_name;
        
    } // End func get_company_name
		
} // End class Applicant_model

// End file applicant_model.php
