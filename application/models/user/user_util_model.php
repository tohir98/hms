<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Talentbase
 * User_util Model extends User_model (models/user/user_model.php)
 * 
 * @category   Model
 * @package    Users
 * @subpackage User utility
 * @author     David A. <tokernel@gmail.com>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

class User_util_model extends User_model
{
	
	/**
	 * Class constructor
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		
	} // End func __construct

} // End class User_util_model

// End of file user_util_model.php
?>