<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of checklist_model
 *
 * @author JosephT
 * @property CI_DB_active_record $db 
 * @property CI_Loader $load loader
 */
class Checklist_Model extends CI_Model {

    //put your code here

    protected static $defaultChecklist = ['onboarding', 'exit'];
    protected static $defaultData = array(
        array(
            'ref' => 'onboarding',
            'title' => 'Onboarding Checklist',
            'description' => 'List of tasks to be performed by new staff members',
        ),
        array(
            'ref' => 'exit',
            'title' => 'Exit Checklist',
            'description' => 'List of tasks to be performed by members exiting.',
        ),
    );

    const TBL_CHECKLIST = 'checklist';
    const TBL_CHECKLIST_ITEM = 'checklist_item';
    const TBL_CHECKLIST_APPROVER = 'checklist_approver';
    const TBL_EMPLOYEE_CHECKLIST = 'employee_checklist';
    const TBL_EMPLOYEE_CHECKLIST_APPROVER = 'employee_checklist_approver';

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function getChecklists($id_company, array $where = []) {
        $where['id_company'] = (int) $id_company;
        return $this->db->get_where(self::TBL_CHECKLIST, $where)
                        ->result(employee\Checklist::class);
    }

    public function loadDefaultChecklists($id_company) {
        $count = $this->db
                        ->select('count(1) as c', false)
                        ->from(self::TBL_CHECKLIST)
                        ->where('id_company', (int) $id_company)
                        ->where_in('ref', static::$defaultChecklist)
                        ->get()
                        ->result()[0]->c;

        if ($count < 1) {
            $data = self::$defaultData;
            foreach ($data as &$row) {
                $row['id_company'] = $id_company;
                $row['id_user_creator'] = $this->user_auth->get('id_user');
            }

            $this->db->insert_batch(self::TBL_CHECKLIST, $data);
        }
    }

    public function countAssignedEmployee($id_checklist, $statuses = []) {
        if (!empty($statuses)) {
            $this->db->where_in('completion_status', $statuses);
        }

        return $this->db
                        ->select('count(1) as c')
                        ->from(self::TBL_EMPLOYEE_CHECKLIST)
                        ->where('id_checklist', (int) $id_checklist)
                        ->get()
                        ->result()[0]->c;
    }

    public function countItems($id_checklist) {
        return $this->db
                        ->select('count(1) as c')
                        ->from(self::TBL_CHECKLIST_ITEM)
                        ->where('id_checklist', (int) $id_checklist)
                        ->where('is_deleted', 0)
                        ->get()
                        ->result()[0]->c;
    }

    /**
     * 
     * @param int $id_company
     * @param string $ref
     * @return employee\Checklist checklist
     */
    public function getByRef($id_company, $ref) {
        return $this->db->get_where(self::TBL_CHECKLIST, ['ref' => $ref, 'id_company' => $id_company], 1)
                        ->result(employee\Checklist::class)[0];
    }

    public function toggleStatus($id_company, $ref) {
        $checklist = $this->getByRef($id_company, $ref);

        if (!$checklist || ($checklist->enabled && $this->countAssignedEmployee($checklist->id, [CHECKLIST_STATUS_ONGOING, CHECKLIST_STATUS_PENDING]))) {
            return false;
        }

        $set = array(
            'enabled' => \db\Expr::make('!enabled'),
            'date_updated' => \db\Expr::make('now()'),
        );

        $this->db->update(self::TBL_CHECKLIST, $set, array(
            'id' => $checklist->id,
        ));

        return $this->db->affected_rows() > 0;
    }

    /**
     * 
     * @param int $idChecklist
     * @return employee\ChecklistItem[]
     */
    public function getChecklistItems($idChecklist) {
        return $this->db
                        ->order_by('item_order', 'ASC')
                        ->get_where(self::TBL_CHECKLIST_ITEM, array(
                            'id_checklist' => (int) $idChecklist,
                            'is_deleted' => 0,
                        ))
                        ->result(\employee\ChecklistItem::class);
    }

    /**
     * 
     * @param int $idChecklist
     * @return employee\ChecklistApprover[]
     */
    public function getChecklistApprovers($idChecklist) {
        return $this->db
                        ->order_by('approver_order', 'ASC')
                        ->get_where(self::TBL_CHECKLIST_APPROVER, array(
                            'id_checklist' => (int) $idChecklist
                        ))
                        ->result(\employee\ChecklistApprover::class);
    }

    public function saveChecklist($data, $idChecklist, $idCompany, &$ref = null) {
        //if checklist ID
        $this->load->helper(['string']);
        if (!$ref) {
            if ($idChecklist) {
                throw new RuntimeException('Check list has ref but no ID');
            }

            $ref = $data['ref'] = strtoupper(random_string('alnum', 10));
        }

        $data['id_company'] = $idCompany;

        $items = (array) $data['items'];
        $approvers = (array) $data['approvers'];
        unset($data['items'], $data['approvers']);

        $this->db->trans_start();
        if ($idChecklist) {
            $this->db->update(self::TBL_CHECKLIST, $data, array(
                'id' => (int) $idChecklist,
                'id_company' => (int) $idCompany,
            ));
        } else {
            $data['id_user_creator'] = $this->user_auth->get('id_user');
            $this->db->insert(self::TBL_CHECKLIST, $data);
            $idChecklist = $this->db->insert_id();
        }

        if (!$idChecklist) {
            log_message('error', 'Checklist could not be saved, checklist ID missing.');
            $this->db->trans_rollback();
            return false;
        }

        $this->saveChecklistItems($idChecklist, $items);
        $this->saveApprovers($idChecklist, $approvers);

        $this->db->trans_complete();
        return true;
    }

    public function saveApprovers($idChecklist, $approvers) {
        $this->db->delete(self::TBL_CHECKLIST_APPROVER, array(
            'id_checklist' => (int) $idChecklist,
        ));

        if (!empty($approvers)) {
            $rows = [];
            $order = 0;
            foreach ($approvers as $idUser) {
                $rows[] = array(
                    'id_checklist' => $idChecklist,
                    'id_user' => $idUser,
                    'approver_order' => ++$order,
                );
            }

            $this->db->insert_batch(self::TBL_CHECKLIST_APPROVER, $rows);
        }

        return true;
    }

    public function deleteChecklistItems($idChecklist, array $itemIds) {
        $this->db->where('id_checklist', (int) $idChecklist)
                ->where_in('id', $itemIds)
                ->update(self::TBL_CHECKLIST_ITEM, array(
                    'is_deleted' => 1
        ));

        return $this->db->affected_rows() > 0;
    }

    public function saveChecklistItems($idChecklist, array $items) {
        foreach ($items as $item) {
            $item['params'] = json_encode((array) $item['params']);
            $item['id_checklist'] = $idChecklist;
            if ($item['id']) {
                $this->db->update(self::TBL_CHECKLIST_ITEM, $item, array(
                    'id' => $item['id'],
                    'id_checklist' => $idChecklist,
                ));
            } else {
                $this->db->insert(self::TBL_CHECKLIST_ITEM, $item);
            }
        }

        return true;
    }

    public function updateChecklist($idChecklist, array $data) {
        if (!empty($data)) {
            $this->db->update(self::TBL_CHECKLIST, $data, ['id' => (int) $idChecklist]);
            return $this->db->affected_rows() > 0;
        }

        return false;
    }

    public function createEmployeeChecklist($checklist, $user, $approvers, $due_date = null) {
        $data = array(
            'id_checklist' => $checklist->id,
            'id_user' => $user->id_user,
            'id_company' => $user->id_company,
            'due_date' => $due_date,
            'id_user_from' => $this->user_auth->get('id_user'),
        );

        $this->db->insert(self::TBL_EMPLOYEE_CHECKLIST, $data);
        $id = $this->db->insert_id();

        if ($id < 1) {
            return null;
        }

        if (!empty($approvers)) {
            $approversData = array();
            foreach ($approvers as $idx => $approver) {
                $approversData[] = array(
                    'id_user' => $approver,
                    'id_employee_checklist' => $id,
                    'approver_order' => ($idx + 1) * 100
                );
            }

            $this->db->insert_batch(self::TBL_EMPLOYEE_CHECKLIST_APPROVER, $approversData);
        }

        return $id;
    }

    public function getEmployeeChecklists($id_company, $checklistId = null, $limit = null, $offset = null, $rowClass = stdClass::class) {

        if ($checklistId !== null) {
            $this->db->where('ec.id_checklist', $checklistId);
        }

        if ($limit) {
            $this->db->limit($limit, $offset);
        }

        return $this->db
                        ->select('c.title, c.ref, c.description, ec.id_checklist, ec.completion_status, ec.created_on as date_assigned, ec.due_date, (SELECT COUNT(1) FROM tasks t WHERE t.id_employee_checklist=ec.id) as task_count, GROUP_CONCAT(eca.status SEPARATOR "|") as approval_status, u.first_name, u.last_name, u.email, u.id_user, u.id_string,ec.id', false)
                        ->from(self::TBL_EMPLOYEE_CHECKLIST . ' as ec')
                        ->join(self::TBL_CHECKLIST . ' as c', 'c.id=ec.id_checklist')
                        ->join(self::TBL_EMPLOYEE_CHECKLIST_APPROVER . ' AS eca', 'ec.id = eca.id_employee_checklist', 'left')
                        ->join('users as u', 'u.id_user = ec.id_user')
                        ->where('ec.id_company', (int) $id_company)
                        ->group_by('ec.id')
                        ->get()
                        ->result(class_exists($rowClass) ? $rowClass : stdClass::class);
    }

}
