<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Talentbase
 * Employee Model extends User_model (models/user/user_model.php)
 * 
 * @category   Model
 * @package    User
 * @subpackage Employee
 * @author     David A. <tokernel@gmail.com>
 * @copyright  Copyright © 2014 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

class Employee_model extends User_model
{
	
	/**
	 * Class constructor
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		
	} // End func __construct
	
	public function fetch_global($query_fields, $id_statuses = NULL) {
		
		if(empty($query_fields)) {
			trigger_error('query fields cannot be empty!', E_USER_WARNING);
		}
		
		$id_sratuases_sql = '';
		
		if(is_array($id_statuses)) {
			$id_sratuases_sql = " and u.status in ( ";
			foreach($id_statuses as $id) {
				$id_sratuases_sql .= $id . ',';
			}	
			$id_sratuases_sql = rtrim($id_sratuases_sql, ',');
			$id_sratuases_sql .= ") ";
		}
		
		$sql = "SELECT 
					u.*, 
					e.*, 
					roles.roles, roles.job_description, roles.id_role, 
					d.departments, 
					l.location_tag as location,
					GROUP_CONCAT(
						IFNULL(jt.id_job, '') 
						ORDER BY jt.id_job
						SEPARATOR ','
					) as emp_job_types,
					mng.first_name as manager_first_name,
					mng.last_name as manager_last_name,
					ac.name as access_level_name,
					co.country as country_origin,
					st.state as state,
					ct.country as country
                FROM employees e 
				left join users u 
				on u.id_user = e.id_user 
				left join roles 
				on roles.id_role = e.id_role
				left join departments d
				on d.deptid = e.id_dept
				left join company_locations l 
				on l.id_location = u.id_company_location
				left join employee_job_types jt
				on jt.id_user = u.id_user
				left join users mng
				on mng.id_user = e.id_manager
				left join accounts ac
				on ac.accounttype = e.id_access_level
				left join countries co
				on co.id_country = u.id_nationality
				left join states st
				on st.id_state = u.id_state
				left join countries ct 
				on ct.id_country = u.id_country
				"
              . "WHERE e.id_user = u.id_user " . $id_sratuases_sql;
		
		foreach($query_fields as $field => $value) {
			
			if(substr($field, 0, 2) == 'u.') {
				$sql .= "AND ".$field." = '". $value ."' ";
			} else {
				$sql .= "AND u.".$field." = '". $value ."' ";
			}
			
		}
		
		$sql .= " group by e.id_user ";
		
		$result = $this->db->query($sql)->result();
                
		if(empty($result)){
			return FALSE;
        } else {
			return $result;
        }
		
	} // End func fetch_global
	
	public function fetch_managers($id_company, $except_id = NULL) {
		
		$sql = "SELECT * FROM employees e
				left join users u
				on u.id_user = e.id_user
				where e.id_company = '".$id_company."' 
				and (e.is_manager = 1 or e.is_manager = 2) 
				";
		
		if(!is_null($except_id)) {
			$sql .= " and e.id_user <> '" . $except_id ."' 
					  and e.id_manager <> '" . $except_id ."' 
					"; 
		}
		
		$result = $this->db->query($sql)->result();
                
		if(empty($result)){
			return FALSE;
        } else {
			return $result;
        }
		
	} // End func fetch_managers
	
	public function fetch_profile(array $param_fields) {
		
		if(empty($param_fields)){
            return false;
        }
            
        $result = $this->db->get_where("employees", $param_fields)->result();
            
        if(empty($result)){
            return false;
        }
            
        return $result;
		
	} // End func fetch_profile
		
	public function update_profile(array $data, array $where) {
		
		if(empty($data)){
			return false;
		}
		
		return $this->db->update("employees", $data, $where); 
		
	} // End func update_profile
	
	public function insert_profile(array $data) {
		return $this->db->insert('employees', $data);
	} // End func insert_profile
    
    public function insert_bank_details(array $data) {
		return $this->db->insert('employee_bank_details', $data);
	} // End func insert_bank_details
    
	public function insert_job_types(array $data) {
		return $this->db->insert_batch('employee_job_types', $data);
	} // End func insert_job_types
	
	public function remove_credentials($id_cred, $id_user) {
		
		return $this->db->delete('employee_credentials', array('id_cred' => $id_cred, 'id_user' => $id_user));
		
	} // End func remove_credentials
	
	public function update_document($data, $where) {
		return $this->db->update("employee_credentials", $data, $where); 
	} // End func update_document
    
	public function update_document_file($data, $where) {
		return $this->db->update("employee_credential_files", $data, $where); 
	} // End func update_document
	
	public function fetch_docs_requests($id_user) {
		
		$sql = " select *
					from employee_credentials 
					where id_user = '".$id_user."'
					and type = 0 
					and due_date >= '".date('Y-m-d')."'
					";
		
		$result = $this->db->query($sql)->result();
            
        if(empty($result)){
            return false;
        }

        return $result;
		
	} // End func fetch_docs_requests
	
	public function fetch_docs_uploaded($id_user) {
		
		$sql = " select *
					from employee_credentials 
					where id_user = '".$id_user."'
					and type = 1 
					";
		
		$result = $this->db->query($sql)->result();
            
        if(empty($result)){
            return false;
        }

        return $result;
		
	} // End func fetch_docs_uploaded
	
	public function getch_documents_request_count($id_user) {
		
		$sql = " select count(*) as cnt 
					from employee_credentials 
					where id_user = '".$id_user."'
					and type = 0 ";
		
		$result = $this->db->query($sql)->result();
            
        if(empty($result)){
            return 0;
        }

        return $result[0]->cnt;
		
	} // End func getch_documents_request_count
	
	public function fetch_document(array $param_fields) {
		
		if(empty($param_fields)){
            return false;
        }
            
        $result = $this->db->get_where("employee_credentials", $param_fields)->result();
            
        if(empty($result)){
            return false;
        }
            
        return $result;
	}
	
	public function insert_credentials(array $data) {
		return $this->db->insert_batch('employee_credentials', $data);
	}
	
	public function update_credentials($data, $where) {
		return $this->db->update('employee_credentials', $data, $where);
	} // End func update_credentials
	
	public function fetch_job_types($id_user) {
		
		$sql = "SELECT * FROM employee_job_types ej
				LEFT JOIN job_types jt
				ON jt.id_type = ej.id_job
				WHERE ej.id_user = " . $id_user;
		
		$result = $this->db->query($sql)->result();
                
		if(empty($result)){
			return FALSE;
        } else {
			return $result;
        }
		
	} // End func fetch_job_types
	
	public function fetch_job_functions($id_role, $id_company) {
		
		$sql = "select * "
			. "from roles_job_functions ef "
			. "left join job_functions jf "
			. "on ef.id_function = jf.id "
			. "where ef.id_role = " . $id_role ." "
			. "and ef.id_company = ". $id_company;
		
		$result = $this->db->query($sql)->result();
		
		if(empty($result)) {
			return false;
		} else {
			return $result;
		}
		
	} // End func fetch_job_functions
	
	public function fetch_credentials($id_user) {
		/*
		$sql = "SELECT 
					e.*, 
					u.first_name, 
					u.last_name 
				FROM 
					employee_credentials e
				left join users u
				on u.id_user = e.id_uploader_user
				WHERE e.id_user = " . $id_user . " 
				order by e.type ";
		*/
		/*$sql = "SELECT 
					e.*, 
					u.first_name, 
					u.last_name 
				FROM 
					employee_credentials e
				left join users u
				on u.id_user = " . $id_user . "
				WHERE e.id_user = " . $id_user . " 
				order by e.type ";*/
		
		/* SQL SCRIPT ADDED BY CHUKS */
		$sql = "SELECT e.*, u.first_name, u.last_name, u.id_user as id_manager "
			. "FROM employee_credentials e, users u "
			. "WHERE e.id_uploader_user = u.id_user "
			. "AND e.id_user = ". $id_user;
		
		$result = $this->db->query($sql)->result();
                
		if(empty($result)){
			return FALSE;
        } else {
			return $result;
        }
		
	} // End func fetch_credentials
    
     public function fetch_credential_files($id_user){
         
         $sql = "SELECT * FROM employee_credential_files WHERE id_cred IN (SELECT id_cred FROM employee_credentials WHERE id_user = '".$id_user."')";
         $result = $this->db->query($sql)->result();
                
		if(empty($result)){
			return FALSE;
        } else {
			return $result;
        }
     }


     public function delete_job_types($id_user) {
		
		return $this->db->delete('employee_job_types', array('id_user' => $id_user));
				
	} // End func delete_job_types


	public function insert_note($id_company, $id_user, $note) {
		
		$data = array(
			'note' => $note,
			'date_added' => date('Y-m-d H:i:s'),
			'id_user' => $id_user,
			'id_company' => $id_company,
			'id_creator' => $this->user_auth->get('id_user')
		);
		
		$this->db->insert('applied_jobs_cnd_notes', $data);
		
		return true;
		
	} // End func insert_note


	public function fetch_notes($id_company, $id_user, $id_manager = NULL){
		
		// ENSURE PARAMS PASSED CONTAIN VALUES
		if($id_company == '' or $id_user == ''){
			return false;
		}
		
		if($id_manager == null){
			// BUILD SQL QUERY STATEMENT
			$sql = "select n.*, u.first_name, u.last_name "
				. "from employee_notes n, employees e, users u "
				. "where n.id_user = e.id_user "
				. "and u.id_user = e.id_manager "
				. "and e.id_user = ". $id_user. " "
				. "and e.id_company = ". $id_company;
		} else {
			// BUILD SQL QUERY STATEMENT
			$sql = "select n.*, u.first_name, u.last_name "
				. "from employee_notes n, employees e, users u "
				. "where n.id_user = e.id_user "
				. "and u.id_user = e.id_manager "
				. "and e.id_user = ". $id_user. " "
				. "and e.id_manager = ". $id_manager ." "
				. "and n.access = 2 "
				. "and e.id_company = ". $id_company;
		}
		
		$ret = $this->db->query($sql)->result();
		
		if(!empty($ret)){
			return $ret;
		}
		
		return false;
		
//		$this->db->select('n.*,	u.first_name, u.last_name, u.profile_picture_name, e.id_role, e.id_dept, r.roles, d.departments');
//		$this->db->from('employee_notes n');
//		$this->db->join('employees e', 'e.id_user = n.id_creator', 'left');
//		$this->db->join('users u', 'u.id_user = n.id_creator', 'left');
//		$this->db->join('roles r', 'r.id_role = e.id_role', 'left');
//		$this->db->join('departments d', 'd.deptid = e.id_dept', 'left');
//		$this->db->where('n.id_company', $id_company);
//		$this->db->where('n.id_user', $id_user);
//		$q = $this->db->get();
//		return $q->result();
		
	}// End func fetch_notes

	public function insert_employee_note($data) {
		
		return $this->db->insert('employee_notes', $data);
		
	} // End func insert_employee_note
	
	public function update_employee_note($data, $where) {
		
		$this->db->update('employee_notes', $data, $where);
		
		return true;
		
	} // End func insert_employee_note
	
	public function is_manager($id_user, $id_company)
	{
		$this->db->where('id_user', $id_user);
		$this->db->where('id_company', $id_company);
		$q = $this->db->get('employees')->row();

		if($q->is_manager > 0){
			return true;
		}
		else{
			return false;
		}
	}// End func is_manager


	public function get_manager_employees($id_user, $id_company)
	{
		
		$this->db->select('u.id_user, u.first_name, u.last_name, u.status, u.id_string, u.profile_picture_name, e.id_role, e.id_dept, e.work_number, r.roles, d.departments');
		$this->db->from('employees e');
		$this->db->join('users u', 'u.id_user = e.id_user', 'inner');
		$this->db->join('roles r', 'r.id_role = e.id_role', 'inner');
		$this->db->join('departments d', 'd.deptid = e.id_dept', 'inner');
		$this->db->where('e.id_company', $id_company);
		$this->db->where('e.id_manager', $id_user);
		$q = $this->db->get();
		return $q->result();
	}// End func get_manager_employees

	public function get_employee($id_user, $id_company)
	{
		
		$this->db->select('u.id_user, u.first_name, u.last_name, u.email, u.status, u.profile_picture_name, e.id_role, e.work_number, e.id_dept, r.roles, d.departments');
		$this->db->from('employees e');
		$this->db->join('users u', 'u.id_user = e.id_user', 'left');
		$this->db->join('roles r', 'r.id_role = e.id_role', 'left');
		$this->db->join('departments d', 'd.deptid = e.id_dept', 'left');
		$this->db->where('e.id_company', $id_company);
		$this->db->where('e.id_user', $id_user);
		$q = $this->db->get();
		return $q->row();
	}// End func get_employee
	
	public function get_manager_employees_location(array $params){
		
		if($params['id_location'] == null) {
			$clause = " ";
		} else {
			$clause = "and u.id_company_location = ". $params['id_location'];
		}
		
		$sql = "select u.id_user, u.first_name, u.last_name, u.status, u.id_string, u.profile_picture_name, "
			. "e.id_role, e.id_dept, e.work_number, r.roles, d.departments, l.location_tag, l.id_location "
			. "from users u, employees e, departments d, roles r, company_locations l "
			. "where l.id_head = ". $params['id_user'] ." "
			. "and l.id_company = ". $params['id_company'] ." "
			. "and e.id_user = u.id_user "
			. "and e.id_role = r.id_role "
			. "and d.deptid = e.id_dept "
			. "and u.id_company_location = l.id_location ". $clause;
		
		return $this->db->query($sql)->result();
		
	} // End func get_manager_employees_location
	
	public function is_location_head(array $params){
		
		if(empty($params)){
			return false;
		}
		
		$res = $this->db->get_where('company_locations', $params)->result();
		
		if(!empty($res)){
			return true;
		}
		
		return false;
		
	} // End func validate_location_head
	
	/**
	 * Fetch company's admin
	 * 
	 * @param type $id_company
	 */
	public function get_company_admin($id_company){

		$sql = "SELECT u.first_name, u.last_name, e.id_country_code, e.work_number, u.email, u.id_user "
			. "FROM users u, employees e "
			. "WHERE u.id_company = ". $id_company ." "
			. "AND e.id_user = u.id_user "
			. "AND u.account_type = ". ACCOUNT_TYPE_ADMINISTRATOR ." ";
		
		$res = $this->db->query($sql)->result();
		
		$data = array();
		
		if(!empty($res)){
			$i = $res[0];
			
			$data[] = array(
				'first_name' => $i->first_name,
				'last_name' => $i->last_name,
				'work_number' => $i->id_country_code.$i->work_number,
				'email' => $i->email,
				'id_user' => $i->id_user
			);
			
			return $data;
		}
		
		return false;
			
	}// End func get_manager_employees
	
	public function get_company_admin_manager($id_company, $id_user){
		
		$data = array();
		
		// Admin info
		//$admin = $this->get_company_admin($id_company);
		//$admin = $this->db->get('users', array('id_company' => $id_company, 'account_type' => 1))->result();
		
		$sql = "SELECT u.first_name, u.last_name, e.id_country_code, e.work_number, u.email, u.id_user "
			. "FROM users u, employees e "
			. "WHERE u.id_company = ". $id_company ." "
			. "AND e.id_user = u.id_user "
			. "AND u.account_type = ". ACCOUNT_TYPE_ADMINISTRATOR ." ";
		
		$admin = $this->db->query($sql)->result();

		if(!empty($admin)){
			$a = $admin[0]; 
			
			$data[] = array(
				'first_name' => $a->first_name,
				'last_name' => $a->last_name,
				'work_number' => $a->id_country_code.$a->work_number,
				'email' => $a->email,
				'id_user' => $a->id_user
			);
		}
		
		// Employee manager info
		$mgr = $this->get_employee_manager_details($id_company, $id_user);
		
		if(!empty($mgr)){
			$m = $mgr[0];
			
			$data[] = array(
				'first_name' => $m->first_name,
				'last_name' => $m->last_name,
				'work_number' => $m->id_country_code.$m->work_number,
				'email' => $m->email,
				'id_user' => $m->id_user
			);
		}
		
		return $data;
		
	} // End func get_company_admin_manager
	
	public function get_company_admin_manager_user($id_company, $id_user){
		
		$data = array();
		
		// Admin info
		//$admin = $this->get_company_admin($id_company);
		//$admin = $this->db->get('users', array('id_company' => $id_company, 'account_type' => 1))->result();
		
		$sql = "SELECT u.first_name, u.last_name, e.id_country_code, e.work_number, u.email, u.id_user "
			. "FROM users u, employees e "
			. "WHERE u.id_company = ". $id_company ." "
			. "AND e.id_user = u.id_user "
			. "AND u.account_type = ". ACCOUNT_TYPE_ADMINISTRATOR ." ";
		
		$admin = $this->db->query($sql)->result();

		if(!empty($admin)){
			$a = $admin[0]; 
			
			$data[] = array(
				'first_name' => $a->first_name,
				'last_name' => $a->last_name,
				'work_number' => $a->id_country_code.$a->work_number,
				'email' => $a->email,
				'id_user' => $a->id_user
			);
		}
		
		// Employee manager info
		$mgr = $this->get_employee_manager_details($id_company, $id_user);
		
		if(!empty($mgr)){
			$m = $mgr[0];
			
			$data[] = array(
				'first_name' => $m->first_name,
				'last_name' => $m->last_name,
				'work_number' => $m->id_country_code.$m->work_number,
				'email' => $m->email,
				'id_user' => $m->id_user
			);
		}
		
		// Employee info
		$emp = $this->get_employee_details($id_company, $id_user);
		
		if(!empty($emp)){
			$e = $emp[0];
			
			$data[] = array(
				'first_name' => $e->first_name,
				'last_name' => $e->last_name,
				'work_number' => $e->id_country_code.$e->work_number,
				'email' => $e->email,
				'id_user' => $e->id_user
			);
		}
		
		return $data;
		
	} // End func get_company_admin_manager_user
	
	
	public function get_employee_manager_details($id_company, $id_user){
		
		if($id_company == '' or $id_user == ''){
			return false;
		}
		
		$sql = "SELECT m.first_name, m.last_name, e.id_country_code, e.work_number, m.email, e.id_manager, m.id_user "
			. "FROM users m, employees e, users i "
			. "WHERE m.id_user = e.id_manager "
			. "AND e.id_user = i.id_user "
			. "AND i.id_user = ". $id_user ." "
			. "AND m.id_company = ". $id_company;
		
		return $this->db->query($sql)->result();
		
	} // End func get_employee_manager_details
	
	public function get_employee_details($id_company, $id_user){
		
		$sql = "select u.first_name, u.last_name, e.id_country_code, e.work_number, u.email, u.id_user "
			. "from users u, employees e "
			. "where u.id_user = e.id_user "
			. "and u.id_user = ". $id_user ." "
			. "and u.id_company = ". $id_company;
		
		return $this->db->query($sql)->result();
		
	} // End func get_employee_details
	
	public function add_document($data) {
		
		$q = $this->db->insert('employee_credentials', $data);
		
		return $q;
		
	} // End func insert_note
	
	public function insert_employee_credentials(array $data){
		
		$res = $this->db->insert('employee_credentials', $data);
		return $res;
		
	} // End func insert_employee_credentials
	
	public function employee_phone_code(array $params){
		
		if(!empty($params)){
			$res = $this->db->get_where('employees', $params)->result();
			return $res[0]->id_country_code;
		}
		
		return false;
		
	} // End func employee_phone_code
    
    
    // ************************** Tohir *********************************************
    
    /**
     * Class class_name
     * 
     * @access public
     * @return void
     */

	public function get_where($table, $where, $count=false)
	{
		$q = $this->db->get_where($table, $where);
		if($count){
			return $q->num_rows();
		}
		else{
			return $q->result();
		}
	}
   
    public function get_newly_hired($min_date, $max_date, $department, $location ){
           
           $sql = "SELECT count(e.id_user) users, e.employment_date 
                    FROM employees e, users u
                    WHERE e.id_company = '".$this->user_auth->get('id_company')."' AND e.employment_date >= '".$min_date."' AND e.employment_date <= '".$max_date."'
                    AND e.id_user = u.id_user ";
           
            if ($department != 0) {
                $sql .= " AND e.id_dept = '".$department."' ";
            }

            if ($location != 0) {
                $sql .= " AND u.id_company_location =  '".$location."' ";
            }
           
            $sql .= " GROUP By e.employment_date ";

            $result = $this->db->query($sql)->result();

            return $result;
           
       }    // End func get_newly_hired
       
    public function get_employess_uptill($max_date, $department, $location ){
           
           $sql = "SELECT count(e.id_user) users, e.employment_date 
                    FROM employees e, users u
                    WHERE e.id_company = '".$this->user_auth->get('id_company')."' AND e.employment_date <= '".$max_date."'
                    AND e.id_user = u.id_user ";
           
            if ($department != 0) {
                $sql .= " AND e.id_dept = '".$department."' ";
            }

            if ($location != 0) {
                $sql .= " AND u.id_company_location =  '".$location."' ";
            }

            return $this->db->query($sql)->row()->users;

//            return $result;
           
       }    // End func get_newly_hired
       
       public function get_total_terminated_emps($min_date, $max_date, $department, $location ){
           
            $sql = "SELECT count(u.id_user) users, u.date_termination 
                    FROM employees e, users u
                    WHERE u.id_company = '".$this->user_auth->get('id_company')."' AND u.date_termination >= '".$min_date."' AND u.date_termination <= '".$max_date."'
                    AND e.id_user = u.id_user ";
           
            if ($department != 0) {
                $sql .= " AND e.id_dept = '".$department."' ";
            }

            if ($location != 0) {
                $sql .= " AND u.id_company_location =  '".$location."' ";
            }
            
            $sql .= " GROUP By u.date_termination ";
            
            $result = $this->db->query($sql)->result();
            $sum = 0;
            if (!empty($result)){
                foreach ($result as $b) {
                    $sum += $b->users;
                }
            }

            return $sum;
           
       }    // End func total_terminated_emp
       
       public function get_total_emps(){
           
           $sql = "SELECT count(id_user) total FROM users WHERE id_company = '".$this->user_auth->get('id_company')."' ";
           
           $result = $this->db->query($sql)->row();
           
           return $result->total;
           
       }    // End func get_total_emps
       
       public function get_emps_by_gender($min_date, $max_date, $department, $location, $id_gender ){
           
         $sql = "SELECT count(e.id_user) users, e.employment_date 
                    FROM employees e, users u
                    WHERE e.id_company = '".$this->user_auth->get('id_company')."' AND e.employment_date >= '".$min_date."' AND e.employment_date <= '".$max_date."'
                    AND e.id_user = u.id_user AND u.id_gender = '".$id_gender."' ";
           
            if ($department != 0) {
                $sql .= " AND e.id_dept = '".$department."' ";
            }

            if ($location != 0) {
                $sql .= " AND u.id_company_location =  '".$location."' ";
            }
           
            $sql .= " GROUP By e.employment_date ";
            
            $result = $this->db->query($sql)->result();
            $sum = 0;
            if (!empty($result)){
                foreach ($result as $b) {
                    $sum += $b->users;
                }
            }

            return $sum;
            
       }    // End func get_emps_by_gender
       
    public function fetch_bank_details($id_user) {
		
		$sql = " SELECT *
					FROM employee_bank_details 
					WHERE id_user = '".$id_user."'
					and id_company = '".$this->user_auth->get('id_company')."' 
					";
		
		$result = $this->db->query($sql)->result();
            
        if(empty($result)){
            return false;
        }

        return $result;
		
	} // End func fetch_bank_details
    
     public function fetch_phone_details($id_user) {
		
		$sql = " SELECT e.*, p.phone_type FROM employee_phone e, phone_types p
                    WHERE e.id_phone_type = p.id_phone_type AND e.id_user = '".$id_user."'
					AND e.id_company = '".$this->user_auth->get('id_company')."' 
					";
		
		$result = $this->db->query($sql)->result();
            
        if(empty($result)){
            return false;
        }

        return $result;
		
	} // End func fetch_phone_details
    
    public function fetch_employee_phone_number($id_user, $id_phone_type, $id_company=''){
        if ($id_company === '') {
            $id_company = $this->user_auth->get('id_company');
        }
        $sql = " SELECT * FROM employee_phone
                    WHERE id_phone_type = '".$id_phone_type."' AND id_user = '".$id_user."'
					AND id_company = '".$id_company."' 
					";
		
		$result = $this->db->query($sql)->row();
        
        if(empty($result)){
            return '';
        }

        return '(' .$result->id_country_code. ') -' .$result->phone_number;
        
    }   // End func fetch_employee_phone_number
    
    public function fetch_employee_primary_number($id_user, $primary, $id_company = '') {

		if ($id_company == '') {
			$id_company = $this->user_auth->get('id_company');
		}
		
        $sql = " SELECT * FROM employee_phone
                    WHERE `primary` = '".$primary."' AND id_user = ".$id_user."
					AND id_company = ".$id_company." ";
		
		$result = $this->db->query($sql)->row();
        
        
        if( empty($result) ){

            $result = $this->fetch_employee_phone_number($id_user, WORK_PHONE_TYPE, $id_company);
            
            if( $result == '' ) {
                $result = $this->fetch_employee_phone_number($id_user, PERSONAL_PHONE_TYPE, $id_company);
            }
			
        } else {
            return  '(' .$result->id_country_code. ') -' .$result->phone_number;
        }
		
		return  $result;
    }   
    
    public function fetch_employee_phone_details($id_user, $id_phone_type){
       
        $sql = " SELECT * FROM employee_phone
                    WHERE id_phone_type = '".$id_phone_type."' AND id_user = '".$id_user."'
					AND id_company = '".$this->user_auth->get('id_company')."' 
					";
		
		$result = $this->db->query($sql)->row();
        if(empty($result)){
            return false;
        }

        return $result;
        
    }   // End func fetch_employee_phone_details
    
    public function update_phone_details(array $data, array $where) {
		
		if(empty($data)){
			return false;
		}
		
		return $this->db->update("employee_phone", $data, $where); 
		
	} // End func update_phone_details
    
    
     public function fetch_bank_details_by_ref_id($id_ref){
        
        $sql = " SELECT * FROM employee_bank_details
                    WHERE ref_id = '".$id_ref."' AND id_company = '".$this->user_auth->get('id_company')."'	";
		
		$result = $this->db->query($sql)->row();
        
        if(empty($result)){
            return false;
        }

        return $result;
        
    }   // End func fetch_bank_details
    
    public function update_bank_details(array $data, array $where) {
		
		if(empty($data)){
			return false;
		}
		
		return $this->db->update("employee_bank_details", $data, $where); 
		
	} // End func update_bank_details
    
    public function insert_data(array $data){
		
		if(empty($data)){
			return false;
		}
		
		$this->db->insert($data['table'], $data['vals']);
		
		return $this->db->insert_id();
		
	}// End func insert
    
    
    /**
 * Check id data exist in db
 * 
 * @access public
 * @param str $table 
 * @param arr $data
 * @return bool
 */
    public function check($table, $where)
    {
            
        foreach ($where as $k => $v) {
            $this->db->where($k, $v);
        }

        $query = $this->db->get($table);
        if($query->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }
            
    }   // End func check
    
    public function is_primary_contact($id_user, $phone_type){
        
         $sql = " SELECT * FROM employee_phone
                    WHERE id_company = '".$this->user_auth->get('id_company')."' 
                    AND id_user = '".$id_user."' 
                    AND id_phone_type = '".$phone_type."' AND `primary` = 1	";
		
		$result = $this->db->query($sql)->result();
        
        if(empty($result)){
            return false;
        }

        return true;
        
    }   // End func is_primary_contact
    
    public function reporting_data($id_company, $id_string = '')
	{	
		$this->db->select('u.*, r.roles, d.departments, cl.location_tag');
		$this->db->from('users u');
		$this->db->join('employees e', 'u.id_user = e.id_user', 'inner');
		$this->db->join('roles r', 'r.id_role = e.id_role', 'left');
		$this->db->join('departments d', 'd.deptid = e.id_dept', 'left');
		$this->db->join('company_locations cl', 'u.id_company_location = cl.id_location', 'inner');
		$this->db->where('e.id_company', $id_company);
        if ($id_string != ''){
            $this->db->where('u.id_string', $id_string);
        }
		$result = $this->db->get()->result();
        
        if (empty($result)) {
            return false;
        }
        
        $data = array();
        $j = 0;
        foreach($result as $r) {
            $data[$j]['id_string'] = $r->id_string;
            $data[$j]['first_name'] = $r->first_name;
            $data[$j]['last_name'] = $r->last_name;
            $data[$j]['status'] = $r->status;
            $data[$j]['departments'] = $r->departments;
            $data[$j]['location_tag'] = $r->location_tag;
            $data[$j]['work_email'] = '<font color="red"> ✖ </font>';
            $data[$j]['work_email_'] = 0;
            if ( $r->email != '' ) {
                $data[$j]['work_email'] = '✔';
                $data[$j]['work_email_'] = 1;
            }
            
            $data[$j]['id_gender_'] = 0;
            if ( $r->id_gender != '' ) {
                $data[$j]['id_gender_'] = 1;
            }
            
            $data[$j]['id_marital_status_'] = 0;
            if ( $r->id_marital_status != '' ) {
                $data[$j]['id_marital_status_'] = 1;
            }
            
            $data[$j]['id_country_'] = 0;
            if ( $r->id_country != '' && $r->id_country != '0' ) {
                $data[$j]['id_country_'] = 1;
            }
            
            $data[$j]['city_'] = 0;
            if ( $r->city != '' && $r->city != '0' ) {
                $data[$j]['city_'] = 1;
            }
            
            $data[$j]['id_state_'] = 0;
            if ( $r->id_state != '' && $r->id_state != '0' ) {
                $data[$j]['id_state_'] = 1;
            }
            
            $data[$j]['id_nationality_'] = 0;
            if ( $r->id_nationality != '' && $r->id_nationality != '0' ) {
                $data[$j]['id_nationality_'] = 1;
            }
            
            $data[$j]['work_phone_'] = 0;
            $data[$j]['work_phone'] = $this->_check_work_number($r->id_user, $id_company, WORK_PHONE_TYPE);
            if ( $data[$j]['work_phone'] == '✔' ) {
                $data[$j]['work_phone_'] = 1;
            }
            
            $data[$j]['home_address_'] = 0;
            $data[$j]['home_address'] = '<font color="red"> ✖ </font>';
            if ( $r->address != '' ) {
                $data[$j]['home_address'] = '✔';
                $data[$j]['home_address_'] = 1;
            }
            
            $data[$j]['birthday_'] = 0;
            $data[$j]['birthday'] = '<font color="red"> ✖ </font>';
            if ( $r->dob != '0000-00-00 00:00:00' ) {
                $data[$j]['birthday'] = '✔';
                $data[$j]['birthday_'] = 1;
            }
            
            $data[$j]['personal_phone_'] = 0;
            $data[$j]['personal_phone'] = $this->_check_work_number($r->id_user, $id_company, PERSONAL_PHONE_TYPE);
            if ( $data[$j]['personal_phone_'] == '✔') {
                $data[$j]['personal_phone_'] = 1;
            }
            
            // Emergency
            $data[$j]['emergency1_'] = 0;
            $data[$j]['emergency1'] = '<font color="red"> ✖ </font>';
            if ( $r->emergency_email != '' or $r->emergency_phone_number != '' ) {
                $data[$j]['emergency1'] = '✔';
                $data[$j]['emergency1_'] = 1;
            }
            
            $data[$j]['emergency2_'] = 0;
            $data[$j]['emergency2'] = '<font color="red"> ✖ </font>';
            if ( $r->emergency_email2 != '' or $r->emergency_phone_number2 != '' ) {
                $data[$j]['emergency2'] = '✔';
                $data[$j]['emergency2_'] = 1;
            }
            
            $data[$j]['bank_account_'] = 0;
            $data[$j]['bank_account'] = $this->_check_bank_account($r->id_user, $id_company);
            if ( $data[$j]['bank_account'] == '✔') {
                $data[$j]['bank_account_'] = 1;
            }
            
            $data[$j]['documents'] = $this->_get_document_count($r->id_user);
            
            $data[$j]['uploaded_docs'] = $this->_get_uploaded_document_count($r->id_user);
            
            $data[$j]['pension_details_'] = 0;
            $data[$j]['pension_details'] = '<font color="red"> ✖ </font>';
            if ( $r->rsa_pin != '' && strtolower($r->rsa_pin) != 'none' ) {
                $data[$j]['pension_details'] = '✔';
                $data[$j]['pension_details_'] = 1;
            }
            
            ++$j;
        }
        
		return $data;
	}// End func get_manager_employees
    
    private function _check_work_number($id_user, $id_company, $phone_type){
        
        $j = $this->check('employee_phone', array(
            'id_user' => $id_user,
            'id_company' => $id_company,
            'id_phone_type' => $phone_type
        ));
        
        if ($j) {
            return '✔';
        }else{
            return '<font color="red"> ✖ </font>';
        }
        
    }   // end func _check_work_number
    
    private function _check_bank_account($id_user, $id_company){
        
        $j = $this->check('employee_bank_details', array(
            'id_user' => $id_user,
            'id_company' => $id_company,
        ));
        
        if ($j) {
            return '✔';
        }else{
            return '<font color="red"> ✖ </font>';
        }
        
    }   // end func _check_bank_account
    
    private function _get_document_count($id_user){
        
        $sql = "SELECT count(* ) kount from employee_credentials WHERE id_user = '".$id_user."' ";
        return $this->db->query($sql)->row()->kount;
        
    }   // end func _get_document_count
    
    private function _get_uploaded_document_count($id_user){
        
        $sql = "SELECT count(* ) kount from employee_credentials WHERE id_user = '".$id_user."' and type = 1 ";
        return $this->db->query($sql)->row()->kount;
        
    }   // end func _get_document_count
    
    public function access_approver($id_company)
	{
		if(!$id_company){
			return false;
		}

		$this->db->select('a.*, u.first_name, u.last_name');
		$this->db->from('employee_data_approver a');
		$this->db->join('users u', 'u.id_user = a.id_user', 'left');
		$this->db->where('u.id_company', $id_company);
		$q = $this->db->get();
		if($q->num_rows() > 0){
			return $q->result();
		}
		else{
			return false;
		}
	}// End func access_approver
    
    public function fetch_pending_approvals_details($id_temp_request)
	{
		$id_company = $this->user_auth->get('id_company');

		$this->db->select('r.*, u.first_name, u.last_name');
		$this->db->from('employee_temp_request r');
		$this->db->join('users u', 'u.id_user = r.id_user', 'inner');
		$this->db->where('u.id_company', $id_company);
        $this->db->where('r.id_temp_request', $id_temp_request);
		$q = $this->db->get();
        
		if($q->num_rows() > 0){
			//return $q->result();
            $data = array();
            $j = 0;
            foreach ($q->result() as $u) {
                $data[$j]['id_temp_request'] = $u->id_temp_request;
                $data[$j]['id_user'] = $u->id_user;
                $data[$j]['first_name'] = $u->first_name;
                $data[$j]['last_name'] = $u->last_name;
                
                $this->db->select('request_type');
                $this->db->from('employee_temp_request_task');
                $this->db->where('id_temp_request', $u->id_temp_request);
                $this->db->where('status', EMPLOYEE_REQUEST_FILLED);
                $ent = $this->db->get()->result();
                
                $data[$j]['data_submitted'] = $ent;
                
                $j++;
            }
            
            return $data;
		}
		else{
			return false;
		}
	}// End func access_approver
    

    
    public function fetch_pending_approvals()
	{
		$id_company = $this->user_auth->get('id_company');

		$this->db->select('r.*, u.first_name, u.last_name');
		$this->db->from('employee_temp_request r');
		$this->db->join('users u', 'u.id_user = r.id_user', 'inner');
		$this->db->where('u.id_company', $id_company);
		$q = $this->db->get();
        
		if($q->num_rows() > 0){
			//return $q->result();
            $data = array();
            $j = 0;
            
            foreach ($q->result() as $u) {
                
                
                $this->db->select('request_type');
                $this->db->from('employee_temp_request_task');
                $this->db->where('id_temp_request', $u->id_temp_request);
                $this->db->where('status', EMPLOYEE_REQUEST_FILLED);
                $ent = $this->db->get()->result();
                
                if (!empty($ent)) {
                    $data[$j]['id_temp_request'] = $u->id_temp_request;
                    $data[$j]['id_user'] = $u->id_user;
                    $data[$j]['first_name'] = $u->first_name;
                    $data[$j]['last_name'] = $u->last_name;
                    $data[$j]['data_submitted'] = $ent;
                }
                
                
                
                $j++;
            }
            
            return $data;
		}
		else{
			return false;
		}
	}// End func access_approver
    
    public function fetch_employee_requests()
	{
		$id_company = $this->user_auth->get('id_company');

		$this->db->select('tt.*, tr.id_user, tr.id_string, tr.id_company');
		$this->db->from('employee_temp_request tr');
		$this->db->join('employee_temp_request_task tt', 'tr.id_temp_request = tt.id_temp_request', 'inner');
		$this->db->where('tr.id_company', $id_company);
        $this->db->where('tr.id_user', $this->user_auth->get('id_user'));
        $this->db->where('tt.status', EMPLOYEE_REQUEST_PENDING);
		$q = $this->db->get();
        
		if($q->num_rows() > 0){
			return $q->result();
		}
		else{
			return false;
		}
	}// End func fetch_employee_requests




    public function insert_migrated_data($table, array $data) {
		return $this->db->insert_batch($table, $data);
	} // End func insert_migrated_data
    
    private function _getEmailViaRequestId($id) {
        $sql = "SELECT first_name, email FROM users WHERE id_user = (SELECT id_user FROM employee_temp_request WHERE id_temp_request = '" . $id . "' ) ";
        return $this->db->query($sql)->row();
    }

    public function sendDeclineEmail(array $params) {
        $this->load->helper(['payroll_helper']);

        $sender = $this->user_auth->get('display_name') . ' ' . $this->user_auth->get('last_name');
        $subject = 'Update Task: Information Request from HR';
        $userInfo = $this->_getEmailViaRequestId($params['id_temp_request']);
        
        $k = array(
            'reason' => $params['reason'],
            'first_name' => $userInfo->first_name,
            'decliner' => $this->user_auth->get('display_name') . ' '.$this->user_auth->get('last_name'),
            'requestSubject' => $params['requestSubject'],
            'company_name' => $this->user_auth->get('company_name')
        );
        
        $email = $userInfo->email;

        $msg = $this->load->view('email_templates/employee_request_decline', $k, true);
        

        send_email_now($sender, $email, $subject, $msg);
    }
    
    public function getIdTempRequest(array $params){
        if(!empty($params)){
			$res = $this->db->get_where('employee_temp_request', $params)->result();
			return $res[0]->id_temp_request;
		}
		
		return false;
    }
    
    public function update_employee_data($data, $where) {
		return $this->db->update("users", $data, $where); 
	} // End func update_document
    
    public function get_employee_staff_id(array $params) {
		if(!empty($params)){
			$res = $this->db->get_where('employees', $params)->result();
			return $res[0]->id_staff;
		}
		
		return false;
	} // End func update_document
    
    public function update_staff_id($data, $where) {
		return $this->db->update("employees", $data, $where); 
	} // End func update_document
    
    public function activate_account($full_name, $id_string, $email){
        // Send reset password email.
        $this->load->library('mailgun_api');

        $sender = 'TalentBase Help Desk';

        // Load model
        $this->load->model('user/user_model', 'u_model');

        $i = $this->u_model->fetch_user_role_dept_details(
                array(
                    'id_user' => $this->user_auth->get('id_user'),
                    'id_company' => $this->user_auth->get('id_company')
        ));

        //$reset_password_url = site_url('user/reset_password') . '/' . $id_user . '/' . sha1($email . $id_user);
        $reset_password_url = site_url('user/reset_password') . '/' . $id_string . '/' . sha1($email . $id_string);

        $bdata = array(
            'full_name' => $full_name,
            'reset_password_url' => $reset_password_url,
            'email' => $email,
            'company_name' => $this->user_auth->get('company_name'),
            'company_url' => site_url('login'),
            'role' => $i->roles,
            'dept' => $i->departments,
            'sender_full_name' => $this->user_auth->get('display_name') . ' ' . $this->user_auth->get('last_name')
        );

        $msg = $this->load->view('email_templates/admin_reset_employee_password', $bdata, true);

        $this->mailgun_api->send_mail(
                array(
                    'from' => $sender . ' <' . TALENTBASE_SUPPORT_EMAIL . '>',
                    'to' => $email,
                    'bcc' => 'helpdesk@talentbase.ng',
                    'subject' => 'Reset Your Password (' . $this->user_auth->get('company_name') . ') Staff Portal',
                    'html' => '<html>' . $msg . '</html>'
                )
        );
        
        return;
    }

    // ************************** End Tohir *********************************************
	
} // End class Employee_model
