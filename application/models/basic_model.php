
<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Talentbase
 * Vacations Model
 * 
 * @category   Model
 * @package    Basic
 * @subpackage Basic
 * @author     Obinna Ukpabi <obinna.ukpabi@talentbase.ng>
 * @copyright  Copyright © 2015 Talentbase Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

class Basic_model extends CI_Model {
	
    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();

    }// End func __construct



	/*
     * Get one
     * 
     * @access public 
     * @param  $params
     * @return mixed (bool | array)
     */
    public function get_one($table, $where=Null){

        if(empty($table)){
            return false;
        }
        
        $res = $this->db->get_where($table, $where);
        return $res->row();
        
    }// End func get


    /*
     * Get many
     * 
     * @access public 
     * @param  $params
     * @return mixed (bool | array)
     */
    public function get_many($table, $where=Null){

        if(empty($table)){
            return false;
        }
        
        $res = $this->db->get_where($table, $where);
        return $res->result();
        
    }// End func get



    /*
     * add
     * 
     * @access public 
     */
    public function add($table, $data, $id=false){

        if(empty($data) OR empty($table)){
            return false;
        }

        $q = $this->db->insert($table, $data);

        if($id){
            return $this->db->insert_id();
        }
        else{
             return $q;
        }
                
    }// End func


    /*
     * edit/update
     * 
     * @access public 
     */
    public function update($table, $data, $where){

        if(empty($data) OR empty($table)){
            return false;
        }
        
        $q = $this->db->update($table, $data, $where);
        return $q;

        
    }// End func



    /*
     * delete
     * 
     * @access public 
     */
    public function delete($table, $where){

        if(empty($where) OR empty($table)){
            return false;
        }
        
        $q = $this->db->delete($table, $where);
        return $q;

        
    }// End func


}