<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of common_model
 *
 * @author TOHIR
 */
class common_model extends CI_Model {
    
    private $genders = array(
        '' => 'N/A',
        0 => 'N/A',
        1 => 'Male',
        2 => 'Female'
    );
    private $marital = array(
        '' => 'N/A',
        0 => 'N/A',
        1 => 'Single',
        2 => 'Married'
    );
    
    private $bloodGroup = array(
        0 => 'N/A',
        1 => 'AA',
        2 => 'AB',
        3 => 'O+',
        4 => 'AS',
    );
    
    public function __construct() {
        parent::__construct();
    }
    
    public function getBloodGroups(){
        return $this->bloodGroup;
    }
    
    public function get($table, $where) {
        $q = $this->db->get_where($table, $where);
        return $q->result();
    }
    
    public function fetch_states_by_id($id_country) {

        $states = $this->db->query("select id_state, state 
								from 
									states
								where id_country = '" . $id_country . "' "
                )->result();

        if (empty($states)) {
            return false;
        }

        return $states;
    }
    
}
