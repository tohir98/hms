<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of patient_model
 *
 * @author TOHIR
 */
class patient_model extends CI_Model {
    
    const TBL_PATIENT = 'patient';

    public function __construct() {
        parent::__construct();
    }
    
    public function addPatient(array $data){
        $this->db->insert(self::TBL_PATIENT, array(
            'firstName' => $data['firstName'],
            'middleName' => $data['middleName'],
            'lastName' => $data['lastName'],
            'dob' => date('Y-m-d', strtotime($data['dob'])),
            'gender_id' => $data['id_gender'],
            'marital_status_id' => $data['id_marital_status'],
            'height' => $data['height'],
            'weight' => $data['weight'],
            'blood_group_id' => $data['blood_group_id'],
            'address' => $data['address'],
            'city' => $data['city'],
            'state_id' => $data['id_state'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'country_id' => $data['country']
        ));
        
        return $this->db->insert_id();
    }
    
    public function fetchPatients(){
        return $this->db
            ->select()
            ->from(self::TBL_PATIENT)
            ->get()->result();
    }
}
