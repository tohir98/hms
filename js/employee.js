/* global angular, Const_NIGERIA */

'use strict';

var EmployeeApp = angular.module('EmployeeApp', ['app.Talentbase']);


EmployeeApp.controller('formCtrl', function ($scope, SiteUrl, $http, $timeout) {
    $scope.employee = window.Job ? window.Job : {
        title: '',
        id_category: [],
        id_function: [],
        id_type: [],
        description: '',
        desired_qualities: '',
        id_job_level: window.Const_JOB_LEVEL_PROF,
        id_state: [],
        status: ''
    };


    $scope.statesStatus = 'Select a country';
    $scope.states = {};
    $scope.systemQuestions = window.SystemQuestions || {};
    $scope.adminQuestions = window.AdminQuestions || {};




    $scope.loadStates = function () {
        if (!$scope.employee.id_country) {
            return;
        }
        var url = SiteUrl.get('patient/states_select/' + $scope.employee.id_country + '.json');
        $scope.statesStatus = 'Loading states...';
        $http.get(url)
                .success(function (data) {
                    $scope.statesStatus = '';
                    $scope.states = data;
                    //console.log('States', $scope.states);
                })
                .error(function () {
                    $scope.statesStatus = 'Not Applicable';
                });
    };

    $scope.attachementInView = -1;
    $scope.removeAttachment = function (id, force) {
        var isExisting = !isNaN(parseInt(id));
        if (!isExisting || force) {
            var backup = $scope.job.file_attachment;
            delete $scope.job.file_attachment[id];
            if (isExisting) {
                var url = SiteUrl.get('recruiting/recruiting/delete_file/' + id + '/' + $scope.job.id_reference);
                $http.post(url)
                        .success(function (data) {
                            console.log('Deleted in background', data);
                        })
                        .error(function () {
                            console.warn('Could not complete deletion');
                            $scope.job.file_attachment[id] = backup;
                        });
            }
        } else {
            $scope.attachementInView = id;
            $('#delete_doc_box').modal();
        }
    };

    $scope.addAttachment = function () {
        $scope.job.file_attachment['tmp_' + Date.now()] = '';
    };

    $scope.questionType = '-';
    $scope.addQuestion = function (type, q) {
        //console.log('Add cliecked', type, q);
        if (q !== undefined) {
            $scope.job.questions['tmp_' + Date.now()] = q;
        } else {
            $scope.questionType = type;
            $scope.questionSelected = '';
            $scope.questionArr = type === 'admin' ? $scope.adminQuestions : $scope.systemQuestions;
            $('#questionModal').modal();
        }
    };

    $scope.questionInView = -1;
    $scope.removeQuestion = function (qId, force) {
        if (!force) {
            $scope.questionInView = qId;
            $('#delete_ques_box').modal();
        } else {
            delete $scope.job.questions[qId];
        }
    };


    $scope.isStateSelected = function (stateId) {
        stateId = (stateId) + '';
    };

    $scope.loadStates();

    $scope.picture = 'later';
    $scope.showPicContainer = function (picture) {
        return picture === $scope.picture;
    };

    $scope.employee.status = '';
    $scope.showStatusContainer = function (status) {
        return status === $scope.employee.status;
    };

    $('#work_number').mask("- (999) - (999) - (9999)");
    $('#personal_number').mask("- (999) - (999) - (9999)");


});

