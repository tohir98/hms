$(document).ready(function() {
	
	/* TEMPLATE FORM - STEP 2 */
	$('#add_comment_by').bind('click', function(){
		var num = $('#appr_comments div').length;

		$('#appr_comments').clone(true)
				.attr({id: num, style: 'display: show'})
				.appendTo('#comments_by');

	});
	
	$('.remove_form_part_2').click(function() {
        $(this).parent().parent().parent().remove();
        return false;
    });
	
	/* TEMPLATE FORM - STEP 3 */
	var count;

	$('#ranges').bind('change', function(){
		count = $(this).val();

		if(count > 0){
			/* Clear previous inner elements */
			$('#ranges_elements').empty();
			
			for(var x = 0; x < count; x++){
				$('#box_ranges').clone(true)
						.attr({id: x, style: 'display: show'})
						.appendTo('#ranges_elements');
			}
		} else if(count == 0) {
			$('#ranges_elements').empty();
		}
	}); 

 /* TEMPLATE FORM - STEP 1 */

	$('input.btn_add_kpi').bind('click', function(){
		var n = $(this).attr('data-id');

        var cl = $('#kpi_origin_2').clone(true);
        cl.find('.div_kpi_title').hide();
		
		cl.find('.kpi_section').attr('data-id_2', n);
		cl.find('.kpi_section').val(n);
		
		cl.find('.kpis').attr('id', 'id_001');
		
        cl.attr({style: 'display: show; border:0px solid lightgrey; padding: 6px; margin-bottom:5px; margin-top: 5px'});
		cl.attr('id', 'id_01');
		cl.appendTo($(this).parent());
	});

});

function init_sections_selection() {

    $('.sections').unbind();
    $('.sections').bind('change', function() {
		
        var i = $(this).val();
		var p = $(this).find('option:selected').text();
        var clone_index = $(".kpi_clones").length; // Total id div

        if(i > 0) {
            //var label_ = $("#sections option:selected").text();
			var label_ = $(this).find('option:selected').text();
            var HEAD_ = '<strong> '+label_+'</strong>';

            var cn = $('#kpi_clones').clone(true);
            cn.attr({style: 'display: show'});
			cn.attr('id', 'id_'+clone_index);
            cn.find('.kpi_origin_tpl').remove();
            //cn.find('.btn_kpi').append(HEAD_);
			cn.find('.tag_name').append(HEAD_);

            cn.find('.id_section').val(i);
            cn.find('.btn_add_kpi').attr('data-id', i);

            cn.appendTo('#div_section_kpi');
        }
		
		// Remove from dropdown list.
		$('#sections :selected').remove();
		
    });

}
