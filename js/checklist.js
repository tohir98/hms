/* global angular, TalentBase */
var checklistApp = angular.module('checklistApp', ['app.Talentbase', 'cgBusy']);

checklistApp.run(function ($rootScope, $http, SiteUrl, $timeout) {
    $rootScope.taskTypes = window.taskTypes || {};
    $rootScope.requestTypeNames = window.requestTypeNames || {};
    $rootScope.checklist = window.checklist || {items: [], approvers: [], title: '', description: ''};
    $rootScope.employees = [];
    $rootScope.employeePromise = $http.get(SiteUrl.get('employee/search'))
            .success(function (data) {
                $rootScope.employees = data;
                $timeout(function () {
                    $('select[chosenfy]').trigger('liszt:updated');
                }, 50);
            });
});
checklistApp.controller('EditCtrl', function ($scope, $timeout, $http, SiteUrl, $filter) {

    $scope.blankItem = {title: '', summary: '', item_type: '', params: {}, duration: 7};
    $scope.itemValid = function (item) {
        return item.title !== '' && item.item_type !== '';
    };

    $scope.findEmployee = function (id_user) {
        var f = $filter('filter')($scope.employees, {id_user: id_user});
        return f.length > 0 ? f[0] : {};
    };

    $scope.removeItem = function (idx) {
        var item = $scope.checklist.items[idx];
        $scope.checklist.items.splice(idx, 1);
        //check web
        if(item.id){
            $http.post(SiteUrl.get('employee/checklist/delete_item/'+$scope.checklist.ref), {
                item_id: item.id
            }).success(function(d){
                console.log('response: ', d);
            });
        }
    };

    $scope.addItem = function () {
        var idx = $scope.checklist.items.push(angular.copy($scope.blankItem)) - 1;
        $scope.editItemPopup(idx);
    };

    $scope.move = function (idx, dir) {
        //move up
        if (dir > 0 && idx > 0) {
            $scope.swap(idx, idx - 1);
        }

        //move down
        if (dir < 0 && idx < $scope.checklist.items.length - 1) {
            $scope.swap(idx, idx + 1);
        }
    };

    $scope.swap = function (idx1, idx2) {
        var tmp = $scope.checklist.items[idx2];
        $scope.checklist.items[idx2] = $scope.checklist.items[idx1];
        $scope.checklist.items[idx1] = tmp;
    };

    $scope.allItemsValid = function () {
        angular.forEach($scope.checklist.items, function (item) {
            if (!$scope.itemValid(item)) {
                return false;
            }
        });
        return true;
    };

    $scope.editItemPopup = function (idx) {
        $timeout(function () {
            $('#addItemPopup_' + idx).modal();
        }, 100);
    };

    $scope.fileChange = function (item) {
        TalentBase.filePicker(function (files) {
            console.log('Files received: ', files);
            if (angular.isArray(files) && files.length > 0) {
                $timeout(function () {
                    item.attached_file_name = files[0].filePath;
                    item.attachment_description = files[0].fileTitle;
                }, 100);
            }
        });
    };

    $scope.int = function (c) {
        return parseInt(c, 10);
    };

    $scope.isApprover = function (employee) {
        //console.log('Looking for '+ employee.id_user+ ' in ', $scope.checklist.approvers);
        return $scope.checklist.approvers.indexOf(employee.id_user) > -1;
    };

    $scope.initRequests = function (requests) {
        var sels = {};
        angular.forEach($scope.requestTypeNames, function (name, idx) {
            sels[idx] = requests.indexOf(idx) > -1;
        });
//        console.log(sels);
        return sels;
    };

    $scope.changedReq = function (requests, selections) {
        angular.forEach(selections, function (bool, idx) {
            var pos = requests.indexOf(idx);
            if (bool && pos < 0) {
                requests.push(idx);
            }

            if (!bool && pos >= 0) {
                requests.splice(pos, 1);
            }
        });
    };

    angular.forEach($scope.checklist.items, function (item) {
        if (item.params && item.params.requests) {
            item.params.requests_selection = $scope.initRequests(item.params.requests);
        }
    });
});

checklistApp.controller('AssignCtrl', function ($scope) {
    $scope.data = window.assignData || {};
    
});

checklistApp.directive('paramFields', function () {

    var render = function (collection, prefix) {
        var flds = [];
        angular.forEach(collection, function (c, idx) {
            var name = prefix + '[' + idx + ']';
            if (typeof c === 'object') {
                flds.push(render(c, name));
            } else {
                flds.push('<input type="hidden" value="' + c + '" name="' + name + '" />');
            }
        });

        return flds.join("\n");
    };

    return {
        scope: {
            params: '='
        },
        link: function (scope, element, attrs) {
            element.html(render(scope.params, attrs.prefix));
        }
    };
});