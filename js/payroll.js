var payrollApp = angular.module('payroll', ['angularUtils.directives.dirPagination', 'cgBusy', 'datatables']);


var getSiteUrl = function () {
    return window.location.protocol + '//' + window.location.host + '/';
};


var rTrim = function (myStr) {
    var strLen = myStr.length;
    return myStr.slice(0, strLen - 2);
};

payrollApp.controller('UnpaidEmployees', function ($scope, $http) {

    $scope.itemsPerPage = 10;
    $scope.data = {checkIt: true};
    $scope.statuses = {'0': 'Inactive', '1': 'Active', '2': 'Suspended', '4': 'Probation/Access', '5': 'Active/No access', '6': 'Probation/No access'};

    $scope.myPromise = $http.get(getSiteUrl() + "payroll/json_unpaid_employees")
            .success(function (response) {
                $scope.employees = response;
            });

    $scope.checkAll = function () {
        if ($scope.selectedAll) {
            $scope.selectedAll = true;
        } else {
            $scope.selectedAll = false;
        }
        angular.forEach($scope.employees, function (item) {
            item.Selected = $scope.selectedAll;
        });

    };
});

payrollApp.directive('myTable', function () {
    return function (scope, element, attrs) {

        // apply DataTable options, use defaults if none specified by user
        var options = {};
        if (attrs.myTable.length > 0) {
            options = scope.$eval(attrs.myTable);
        } else {
            options = {
                "bStateSave": true,
                "iCookieDuration": 2419200, /* 1 month */
                "bJQueryUI": false,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": false,
                "bInfo": false,
                "bDestroy": true
            };
        }


        var explicitColumns = [];
        element.find('th').each(function (index, elem) {
            explicitColumns.push($(elem).text());
        });
        if (explicitColumns.length > 0) {
            options["aoColumns"] = explicitColumns;
        } else if (attrs.aoColumns) {
            options["aoColumns"] = scope.$eval(attrs.aoColumns);
        }

        // aoColumnDefs is dataTables way of providing fine control over column config
        if (attrs.aoColumnDefs) {
            options["aoColumnDefs"] = scope.$eval(attrs.aoColumnDefs);
        }

        if (attrs.fnRowCallback) {
            options["fnRowCallback"] = scope.$eval(attrs.fnRowCallback);
        }

        // apply the plugin
        var dataTable = element.dataTable(options);



        // watch for any changes to our data, rebuild the DataTable
        scope.$watch(attrs.aaData, function (value) {
            var val = value || null;
            if (val) {
                dataTable.fnClearTable();
                dataTable.fnAddData(scope.$eval(attrs.aaData));
            }
        });
    };
});



payrollApp.controller('CompensationEmployees', function ($scope, $http, $filter, $compile, DTOptionsBuilder, DTColumnDefBuilder) {

    $scope.check_all = function () {
        if ($scope.select_all) {
            $scope.select_all = true;
        } else {
            $scope.select_all = false;
        }

        angular.forEach($scope.employees, function (employee) {
            employee.selected = $scope.select_all;
        });
    };

    $scope.selectedIds = function () {
        var selected = [];
        angular.forEach($scope.employees, function (employee) {
            if (employee.selected) {
                selected.push(employee.id_user);
            }
        });

        return selected.join(', ');
    };


    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withPaginationType('full_numbers')
            .withDisplayLength(10)
            .withOption('bFilter', true)
            //.withBootstrap();
            ;

    $scope.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0).notSortable(),
        DTColumnDefBuilder.newColumnDef(1).notSortable()
    ];

    $scope.data = {checkIt: true, removedItems: []};
    $scope.allowance_component = [];

    $scope.myPromise = $http.get(getSiteUrl() + "payroll/json_employee_otherpay")
            .success(function (response) {
                $scope.otherpay = response;
            });

    $scope.getOtherPay = function (id_user) {
        $scope.userID = id_user;

        $scope.myPromise = $http.get(getSiteUrl() + "payroll/json_employee_incidental/" + id_user)
                .success(function (response) {
                    $scope.incident = response;
                });


        $scope.myPromise = $http.get(getSiteUrl() + "payroll/json_employee_allowance_component/" + id_user)
                .success(function (response) {
                    $scope.allowance_component = response;
                });

        $scope.removeAll();

    };

    window.getOtherPay = $scope.getOtherPay;

    $scope.removeOtherPay = function (idx) {
        $scope.data.removedItems.push(angular.copy($scope.otherpay[idx]));
        $scope.otherpay.splice(idx, 1);
        return false;
    };

    $scope.addRemovedItem = function (idx) {
        if ($scope.data.removedItems.length > 0) {
            $scope.otherpay.push(angular.copy($scope.data.removedItems[idx]));
            $scope.data.removedItems.splice(idx, 1);
        }
        return false;
    };

    $scope.removeAll = function () {
        while ($scope.otherpay.length > 0) {
            $scope.removeOtherPay(0);
        }
    };

    $scope.fromJson = function (json) {
        var j1 = JSON.parse(json);
        var result = '';
        for (var i = 0, len = j1.length; i < len; ++i) {
            result += $scope.data.allowances[j1[i]] + ', ';

        }
        return rTrim(result);
    };

    $scope.computePercentage = function (json, percentage) {
        var j1 = JSON.parse(json);
        var result = '';
        var sum = 0;
        for (var i = 0, len = j1.length; i < len; ++i) {
            result += $scope.data.allowances[j1[i]] + ', ';

            for (var m = 0; m < $scope.allowance_component.length; m++) {
                if ($scope.allowance_component[m].component_name === $scope.data.allowances[j1[i]]) {
                    sum += parseFloat($scope.allowance_component[m].amount);
                }
            }

        }
        var final = sum * (percentage / 100);
        return final.toFixed(2);
    };

    $scope.getEmployeePays = function (idUser) {

        var id = parseInt(idUser, 10);
        var x = function (v) {
            return parseInt(v.id_user, 10) === id;
        };
        addPay = $filter('filter')($scope.pays, x);

        return addPay;

    };
    $scope.getEmployeeIncidentals = function (idUser) {

        var id = parseInt(idUser, 10);
        var x = function (v) {
            return parseInt(v.id_user, 10) === id;
        };
        addPay = $filter('filter')($scope.incidentals, x);

        return addPay;

    };





//        $filter('filter')(pays, {id_user: employee.id_user});
    $scope.myCallback = function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $('td:eq(2)', nRow).bind('click', function () {
            $scope.$apply(function () {
                $scope.someClickHandler(aData);
            });
        });
        return nRow;
    };

    $scope.columnDefs = [
        {
            "mData": null,
            "aTargets": [0],
            "bSortable": false,
            "mRender": function (data, type, oObj) {
                return '<input class=\"_item_checkbox\" type=\"checkbox\" value=\"' + oObj.id_user + '\" id=\"employees\" name=\"employees[]\" data-id_user=\"' + oObj.id_user + '\" />';
            }
        },
        {
            "mData": null,
            "bSearchable": true,
            "bSortable": true,
            "aTargets": [1],
            "mRender": function (data, type, oObj) {
                return oObj.first_name + ' ' + oObj.last_name;
            }
        },
        {
            "mData": null,
            "aTargets": [2],
            "mRender": function (data, type, oObj) {
                var takeHome = $filter('number')(oObj.take_home, 2);
                return oObj.currency + ' ' + takeHome;
            }
        },
        {
            "mData": null,
            "aTargets": [3],
            "mRender": function (data, type, oObj) {
                var id = parseInt(oObj.id_user, 10);
                var x = function (v) {
                    return parseInt(v.id_user, 10) === id;
                };
                var addPay = $filter('filter')($scope.pays, x);
                var incs = $filter('filter')($scope.incidentals, x);
                var div = '';
                angular.forEach(addPay, function (pay) {
                    div += '<div>'
                            + pay.pay + ': ' + $filter('currency')(pay.amount, ' ')
                            +
                            '</div>';
                });

                angular.forEach(incs, function (inc) {
                    div += '<div>'
                            + 'Incidental : ' + $filter('currency')(inc.amount, ' ')
                            +
                            '</div>';
                });

                var a = '<a  onclick=getOtherPay(' + oObj.id_user
                        + ') href=\"#addNewModal\" data-toggle=\"modal\">Add/Edit </a>';
                //console.log('B:',$('<div></div>').append(a).html());
                return div + a;
            }
        }
    ];

    $scope.overrideOptions = {
        "bStateSave": true,
        "iCookieDuration": 2419200, /* 1 month */
        "bJQueryUI": false,
        "bPaginate": false,
        "sScrollY": "300px",
        "sScrollX": "100%",
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": true,
        "bDestroy": true
    };


});

payrollApp.controller('PayGrades', function ($scope, $http) {

    $scope.data = {checkIt: true};

    $scope.myPromise = $http.get(getSiteUrl() + "payroll/compensation/json_paygrades")
            .success(function (response) {
                $scope.paygrades = response;
            });

    $scope.checkAll = function () {
        if ($scope.selectedAll) {
            $scope.selectedAll = true;
        } else {
            $scope.selectedAll = false;
        }
        angular.forEach($scope.paygrades, function (item) {
            item.Selected = $scope.selectedAll;
        });

    };
});

payrollApp.controller('GrossPayComponentController', function ($scope, $http) {

    $scope.form = {};
    $scope.savedComponents = [];
    $scope.data = {components: [], other_allowances: {}, grosspay: '0.00', basic_percent: '40.0', housing_percent: '20.0',
        transport_percent: '20.0', basic_amount:0, housing_amount: 0, transport_amount:0, id_period: '', tmpComponents: [], removed_allowances: [], removedSavedAllowances: []};


    $scope.myPromise = $http.get(getSiteUrl() + "payroll/compensation/json_get_other_allowances")
            .success(function (response) {
                $scope.data.other_allowances = response;
                $scope.removeAll();
            });

    $scope.periods = {'1': 'Week', '2': 'Month', '3': 'Annual'};
    $scope.sizes = '';

    $scope.blankComponent = {id: null, component_percent: 0, component_name: '', amount: 0};

    $scope.addItem = function () {
        $scope.data.components.push(angular.copy($scope.blankComponent));
    };

    $scope.addButton = function () {
        $scope.data.removed_allowances.push();
    };

    $scope.addRemovedItem = function (idx) {
        if ($scope.data.tmpComponents.length > 0) {
            var c = $scope.data.other_allowances.push(angular.copy($scope.data.tmpComponents[idx]));
            $scope.data.removed_allowances.splice(idx, 1);
            $scope.data.tmpComponents.splice(idx, 1);
            var d = $scope.data.other_allowances[c - 1];
            $scope.data[d.component_name + '_percent'] = d.component_percent;
            $scope.hasChanged($scope.data.other_allowances[c - 1].component_name, 'percent');
        }
    };


    $scope.componentsCheck = function () {
        if ($scope.data.components.length < 1) {
            $scope.addItem();
        }
    };

    $scope.getTotalPercent = function () {
        var sum = 0;

        if ($scope.savedComponents.length < 1) {
            sum = sum + parseFloat($scope.data.basic_percent) + parseFloat($scope.data.housing_percent) + parseFloat($scope.data.transport_percent);
        } else {
            for (var i = 0; i < $scope.savedComponents.length; i++) {
//                sum += parseFloat($scope.savedComponents[i].component_percent);
                sum += parseFloat($scope.data[$scope.savedComponents[i].component_name + '_percent']);
            }
        }
//        
        for (var i = 0; i < $scope.data.components.length; i++) {
//            sum += parseFloat($scope.data.components[i].component_percent);
            sum += parseFloat($scope.data[$scope.data.components[i].component_name + '_percent']);
        }
        for (var i = 0; i < $scope.data.other_allowances.length; i++) {
//            sum += parseFloat($scope.data.other_allowances[i].component_percent);
            sum += parseFloat($scope.data[$scope.data.other_allowances[i].component_name + '_percent']);
        }
        return sum.toFixed(3);
    };

    $scope.getTotalAmount = function () {
        var sum = 0;

        if ($scope.savedComponents.length < 1) {
//            sum += $scope.data.grosspay * (parseFloat($scope.data.basic_percent) / 100);
//            sum += $scope.data.grosspay * (parseFloat($scope.data.housing_percent) / 100);
//            sum += $scope.data.grosspay * (parseFloat($scope.data.transport_percent) / 100);
            sum += parseFloat($scope.data.basic_amount) + parseFloat($scope.data.housing_amount) + parseFloat($scope.data.transport_amount);
        } else {

            for (var i = 0; i < $scope.savedComponents.length; i++) {
//                sum += $scope.data.grosspay * ($scope.savedComponents[i].component_percent / 100);
                sum += parseFloat($scope.data[$scope.savedComponents[i].component_name + '_amount']);
            }
        }

        for (var i = 0; i < $scope.data.components.length; i++) {
//            sum += $scope.data.grosspay * ($scope.data.components[i].component_percent / 100);
            sum += parseFloat($scope.data[$scope.data.components[i].component_name + '_amount']);
        }

        for (var i = 0; i < $scope.data.other_allowances.length; i++) {
//            sum += $scope.data.grosspay * ($scope.data.other_allowances[i].component_percent / 100);
            sum += parseFloat($scope.data[$scope.data.other_allowances[i].component_name + '_amount']);
        }
        return sum.toFixed(2);
    };

    $scope.removeComponent = function (idx) {
        $scope.data.components.splice(idx, 1);
    };

    $scope.removeAllowance = function (idx) {
        $scope.data.tmpComponents.push(angular.copy($scope.data.other_allowances[idx]));
        $scope.data.removed_allowances.push(angular.copy($scope.data.other_allowances[idx]));
        $scope.data.other_allowances.splice(idx, 1);
    };

    $scope.addRemovedSavedComponent = function (idx) {
        if ($scope.data.removedSavedAllowances.length > 0) {
            $scope.savedComponents.push(angular.copy($scope.data.removedSavedAllowances[idx]));
            $scope.data.removedSavedAllowances.splice(idx, 1);
        }
    };

    $scope.removeSavedAllowance = function (idx) {
        $scope.data.removedSavedAllowances.push(angular.copy($scope.savedComponents[idx]));
        $scope.savedComponents.splice(idx, 1);
    };

    $scope.update = function () {
        $scope.data.id_period = $scope.item.id_payroll_period;
    };

    $scope.removeAll = function () {
        while ($scope.data.other_allowances.length > 0) {
            $scope.removeAllowance(0);
        }
    };

    $scope.hasChanged = function (component, type) {
        
        var fld = component + '_' + type, target = component + '_' + (type !== 'percent' ? 'percent' : 'amount');
        console.log(fld);
        if (type === 'percent') {
            $scope.data[target] = $scope.data.grosspay * parseFloat($scope.data[fld]) / 100.0;
        } else {
            $scope.data[target] = Number(100 * parseFloat($scope.data[fld]) / $scope.data.grosspay).toFixed(3);
        }
    };
   

    $scope.allPercentChanged = function () {
        $scope.hasChanged('basic', 'amount');
        $scope.hasChanged('transport', 'amount');
        $scope.hasChanged('housing', 'amount');
        angular.forEach($scope.data.other_allowances, function(d){
            $scope.hasChanged(d.component_name, 'amount');
        });
        angular.forEach($scope.savedComponents, function(d){
            $scope.hasChanged(d.component_name, 'amount');
        });
    };

    $scope.allPercentChanged();

});

payrollApp.controller('PaymentScheduleController', function ($scope) {
    $scope.addNew = false;

    $scope.addSchedule = function () {
        var schedule = $scope.id_schedule;

        if ($scope.id_schedule === 'addSchedule') {
            $scope.addNew = true;
        } else {
            $scope.addNew = false;
        }

    };
});

payrollApp.controller('DeductionComponentController', function ($scope, $http) {

    $scope.data = {other_deduc: [], defined_pays: [''], paye: 0, grossPay: 0, tmpComponents: [], removed_deductions: [],
        tmpOtherDeductions: [], removedOtherDeductions: [], deduct: [], saveDeduction: [], savedDed: [], removed_saved_deductions: []};

    $scope.blankDeduction = {id: null, deduction_amount: '0', deduction_name: ''};

    $scope.myPromise = $http.get(getSiteUrl() + "payroll/compensation/json_get_other_deductions")
            .success(function (response) {
                $scope.data.other_deduc = response;
                $scope.removeAll();
                $scope.computeSaveDeduction();
            });

    $scope.addDeduction = function () {
        $scope.data.deductions.push(angular.copy($scope.blankDeduction));
    };

    $scope.computeSaveDeduction = function () {

        if ($scope.data.savedDed.length > 0) {

            for (var j = 0; j < $scope.data.deduct.length; j++) {
                var track = true;
                for (var i = 0; i < $scope.data.savedDed.length; i++) {
                    if ($scope.data.savedDed[i].deduction_name === $scope.data.deduct[j].deduction) {
                        $scope.data.saveDeduction.push(angular.copy($scope.data.deduct[j]));
                        track = false;
                    }
                }
                if (track) {
                    $scope.data.removed_saved_deductions.push(angular.copy($scope.data.deduct[j]));
                }
            }

            for (var i = 0; i < $scope.data.savedDed.length; i++) {

                for (var m = 0; m < $scope.data.removedOtherDeductions.length; m++) {
                    if ($scope.data.savedDed[i].deduction_name === $scope.data.removedOtherDeductions[m].deduction) {
                        $scope.data.saveDeduction.push(angular.copy($scope.data.removedOtherDeductions[m]));
                        $scope.data.removedOtherDeductions.splice(m, 1);
                    }
                }
            }
        }

    };

    $scope.getTotalDeduction = function () {
        var sum = 0;

        if ($scope.data.saveDeduction.length > 0) {
            for (var i = 0; i < $scope.data.saveDeduction.length; i++) {
                sum += parseFloat($scope.data.saveDeduction[i].amount);
            }
        } else {

            for (var i = 0; i < $scope.data.deduct.length; i++) {
                sum += parseFloat($scope.data.deduct[i].amount);
            }

            for (var i = 0; i < $scope.data.other_deduc.length; i++) {
                sum += parseFloat($scope.data.other_deduc[i].amount);
            }
        }

        return sum;
    };


    $scope.removeDeduction = function (idx) {
        $scope.data.tmpComponents.push(angular.copy($scope.data.deduct[idx]));
        $scope.data.removed_deductions.push(angular.copy($scope.data.deduct[idx]));
        $scope.data.deduct.splice(idx, 1);
    };

    $scope.removeSavedDeduction = function (idx) {
        $scope.data.removed_saved_deductions.push(angular.copy($scope.data.saveDeduction[idx]));
        $scope.data.saveDeduction.splice(idx, 1);
    };

    $scope.removeOtherDeduction = function (idx) {
        $scope.data.tmpOtherDeductions.push(angular.copy($scope.data.other_deduc[idx]));
        $scope.data.removedOtherDeductions.push(angular.copy($scope.data.other_deduc[idx]));
        $scope.data.other_deduc.splice(idx, 1);
    };

    $scope.addRemovedItem = function (idx) {
        if ($scope.data.tmpComponents.length > 0) {
            $scope.data.deduct.push(angular.copy($scope.data.tmpComponents[idx]));
            $scope.data.removed_deductions.splice(idx, 1);
            $scope.data.tmpComponents.splice(idx, 1);
        }
    };

    $scope.addRemovedSavedItem = function (idx) {
        if ($scope.data.removed_saved_deductions.length > 0) {
            $scope.data.saveDeduction.push(angular.copy($scope.data.removed_saved_deductions[idx]));
            $scope.data.removed_saved_deductions.splice(idx, 1);
        }
    };

    $scope.addRemovedOtherItem = function (idx) {
        if ($scope.data.tmpOtherDeductions.length > 0) {
            $scope.data.other_deduc.push(angular.copy($scope.data.tmpOtherDeductions[idx]));
            $scope.data.removedOtherDeductions.splice(idx, 1);
            $scope.data.tmpOtherDeductions.splice(idx, 1);
        }
    };

    $scope.fromJson = function (json) {
        var j1 = JSON.parse(json);
        var result = '';
        for (var i = 0, len = j1.length; i < len; ++i) {
            result += $scope.data.allowances[j1[i]] + ', ';

        }
        return rTrim(result);
    };

    $scope.computePercentage = function (json, percentage) {
        var j1 = JSON.parse(json);
        var result = '';
        var sum = 0;
        for (var i = 0, len = j1.length; i < len; ++i) {
            result += $scope.data.allowances[j1[i]] + ', ';

            for (var m = 0; m < $scope.data.allowance_component.length; m++) {
                if ($scope.data.allowance_component[m].component_name === $scope.data.allowances[j1[i]]) {
                    sum += parseFloat($scope.data.allowance_component[m].amount);
                }
            }

        }
        var final = sum * (percentage / 100);
        return final.toFixed(2);
    };

    $scope.removeAll = function () {
        while ($scope.data.other_deduc.length > 0) {
            $scope.removeOtherDeduction(0);
        }
    };


});


payrollApp.controller('ContributionComponentController', function ($scope, $http) {
    $scope.data = {contributions: [], defined_pays: [], gpay: 0, removedOtherContributions: [], removedDefaultContributions: [], other_contributions: [], savedCon: [], saveContribition: []};
    $scope.blankContribution = {id: null, contribution_amount: '0.00'};

    $scope.myPromise = $http.get(getSiteUrl() + "payroll/compensation/json_get_other_contributions")
            .success(function (response) {
                $scope.data.other_contributions = response;
                $scope.removeAll();
                $scope.computeSaveContribution();
            });

    $scope.addContribution = function () {
        $scope.data.contributions.push(angular.copy($scope.blankContribution));
    };

    $scope.computeSaveContribution = function () {

        if ($scope.data.savedCon.length > 0) {

            for (var j = 0; j < $scope.data.contributions.length; j++) {
                var track = true;
                for (var i = 0; i < $scope.data.savedCon.length; i++) {
                    if ($scope.data.savedCon[i].contribution_name === $scope.data.contributions[j].contribution) {
                        $scope.data.saveContribition.push(angular.copy($scope.data.contributions[j]));
                        track = false;
                    }
                }
                if (track) {
                    $scope.data.removedDefaultContributions.push(angular.copy($scope.data.contributions[j]));
                }
            }

            for (var i = 0; i < $scope.data.savedCon.length; i++) {

                for (var m = 0; m < $scope.data.removedOtherContributions.length; m++) {
                    if ($scope.data.savedCon[i].contribution_name === $scope.data.removedOtherContributions[m].contribution) {
                        $scope.data.saveContribition.push(angular.copy($scope.data.removedOtherContributions[m]));
                        $scope.data.removedOtherContributions.splice(m, 1);
                    }
                }
            }
        }

    };

    $scope.getTotalContribution = function () {
        var sum = 0;

        if ($scope.data.saveContribition.length > 0) {
            for (var i = 0; i < $scope.data.saveContribition.length; i++) {
                sum += parseFloat($scope.data.saveContribition[i].amount);
            }
        } else {
            for (var i = 0; i < $scope.data.contributions.length; i++) {
                sum += parseFloat($scope.data.contributions[i].amount);
            }

            for (var i = 0; i < $scope.data.other_contributions.length; i++) {
                sum += parseFloat($scope.data.other_contributions[i].amount);
            }
        }

        return sum;
    };

    $scope.removeContribution = function (idx) {
        $scope.data.removedDefaultContributions.push(angular.copy($scope.data.contributions[idx]));
        $scope.data.contributions.splice(idx, 1);
    };

    $scope.removeSavedContribution = function (idx) {
        $scope.data.removedDefaultContributions.push(angular.copy($scope.data.saveContribition[idx]));
        $scope.data.saveContribition.splice(idx, 1);
    };

    $scope.addDefaultContribution = function (idx) {
        if ($scope.data.removedDefaultContributions.length > 0) {
            $scope.data.contributions.push(angular.copy($scope.data.removedDefaultContributions[idx]));
            $scope.data.removedDefaultContributions.splice(idx, 1);
        }
    };

    $scope.addSavedContribution = function (idx) {
        if ($scope.data.removedDefaultContributions.length > 0) {
            $scope.data.saveContribition.push(angular.copy($scope.data.removedDefaultContributions[idx]));
            $scope.data.removedDefaultContributions.splice(idx, 1);
        }
    };

    $scope.fromJson = function (json) {
        var j1 = JSON.parse(json);
        var result = '';
        for (var i = 0, len = j1.length; i < len; ++i) {
            result += $scope.data.allowances[j1[i]] + ', ';
        }
        return rTrim(result);
    };

    $scope.computePercentage = function (json, percentage) {
        var j1 = JSON.parse(json);
        var result = '';
        var sum = 0;
        for (var i = 0, len = j1.length; i < len; ++i) {
            result += $scope.data.allowances[j1[i]] + ', ';

            if ($scope.data.allowance_component instanceof Array) {
                for (var m = 0; m < $scope.data.allowance_component.length; m++) {
                    if ($scope.data.allowance_component[m].component_name === $scope.data.allowances[j1[i]]) {
                        sum += parseFloat($scope.data.allowance_component[m].amount);
                    }
                }
            }

        }
        var final = sum * (percentage / 100);
        return final.toFixed(2);
    };

    $scope.removeOtherContribution = function (idx) {
        $scope.data.removedOtherContributions.push(angular.copy($scope.data.other_contributions[idx]));
        $scope.data.other_contributions.splice(idx, 1);
    };

    $scope.addRemovedOtherItem = function (idx) {
        if ($scope.data.removedOtherContributions.length > 0) {
            $scope.data.other_contributions.push(angular.copy($scope.data.removedOtherContributions[idx]));
            $scope.data.removedOtherContributions.splice(idx, 1);
        }
    };


    $scope.removeAll = function () {
        while ($scope.data.other_contributions.length > 0) {
            $scope.removeOtherContribution(0);
        }
    };

});


payrollApp.controller('OtherPayComponentController', function ($scope) {
    $scope.data = {otherpays: [], defined_pays: [], removed_otherpays: [], removed_saved_otherpays: [], savedOtherpays: [], savedPays: []};
    $scope.blankOtherPay = {id: null, otherpay: '', amount: '0.00'};

    $scope.addOtherPay = function () {
        $scope.data.otherpays.push(angular.copy($scope.blankOtherPay));
    };

    $scope.computeSaveOtherPays = function () {

        if ($scope.data.savedOtherpays.length > 0) {

            for (var j = 0; j < $scope.data.otherpays.length; j++) {
                var track = true;
                for (var i = 0; i < $scope.data.savedOtherpays.length; i++) {
                    if ($scope.data.savedOtherpays[i].otherpay === $scope.data.otherpays[j].otherpay) {
                        $scope.data.savedPays.push(angular.copy($scope.data.otherpays[j]));
                        track = false;
                    }
                }
                if (track) {
                    $scope.data.removed_otherpays.push(angular.copy($scope.data.otherpays[j]));
                }
            }

        }

    };

    $scope.getTotalPay = function () {
        var sum = 0;

        if ($scope.data.savedPays.length > 0) {
            for (var i = 0; i < $scope.data.savedPays.length; i++) {
                sum += parseFloat($scope.data.savedPays[i].amount);
            }
        } else {
            for (var i = 0; i < $scope.data.otherpays.length; i++) {
                sum += parseFloat($scope.data.otherpays[i].amount);
            }
        }

        return sum;
    };

    $scope.removeOtherPay = function (idx) {
        $scope.data.removed_otherpays.push(angular.copy($scope.data.otherpays[idx]));
        $scope.data.otherpays.splice(idx, 1);
    };

    $scope.removeSavedOtherPay = function (idx) {
        $scope.data.removed_otherpays.push(angular.copy($scope.data.savedPays[idx]));
        $scope.data.savedPays.splice(idx, 1);
    };

    $scope.addRemovedOtherPay = function (idx) {
        if ($scope.data.removed_otherpays.length > 0) {
            $scope.data.otherpays.push(angular.copy($scope.data.removed_otherpays[idx]));
            $scope.data.removed_otherpays.splice(idx, 1);
        }
    };

    $scope.addRemovedSavedOtherPay = function (idx) {
        if ($scope.data.removed_otherpays.length > 0) {
            $scope.data.savedPays.push(angular.copy($scope.data.removed_otherpays[idx]));
            $scope.data.removed_otherpays.splice(idx, 1);
        }
    };

    $scope.removeAll = function () {
        while ($scope.data.otherpays.length > 0) {
            $scope.removeOtherPay(0);
        }
    };

    $scope.fromJson = function (json) {
        var j1 = JSON.parse(json);
        var result = '';
        for (var i = 0, len = j1.length; i < len; ++i) {
            result += $scope.data.allowances[j1[i]] + ', ';
        }
        return rTrim(result);
    };

    $scope.computePercentage = function (json, percentage) {
        var j1 = JSON.parse(json);
        var result = '';
        var sum = 0;
        for (var i = 0, len = j1.length; i < len; ++i) {
            result += $scope.data.allowances[j1[i]] + ', ';

            for (var m = 0; m < $scope.data.allowance_component.length; m++) {
                if ($scope.data.allowance_component[m].component_name === $scope.data.allowances[j1[i]]) {
                    sum += parseFloat($scope.data.allowance_component[m].amount);
                }
            }

        }
        var final = sum * (percentage / 100);
        return final.toFixed(2);
    };

});

payrollApp.controller('ConfirmationController', function ($scope) {
    $scope.payGradeTemplate = false;

    $scope.showTemplate = function () {
        console.log('show');
        $scope.payGradeTemplate = true;
    };

    $scope.hideTemplate = function () {
        if ($scope.showTemplate == 1) {
            $scope.payGradeTemplate = true;
        } else {
            $scope.payGradeTemplate = false;
        }

    };

    $scope.hideTemplate();

});

payrollApp.controller('AddDeductionController', function ($scope) {
    $scope.data.type = '1';

});

payrollApp.controller('payrollStep3', function ($scope, $filter) {

    $scope.myCallback = function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $('td:eq(2)', nRow).bind('click', function () {
            $scope.$apply(function () {
                $scope.someClickHandler(aData);
            });
        });
        return nRow;
    };

    $scope.columnDefs = [
        {
            "mData": null,
            "aTargets": [0],
            "mRender": function (data, type, oObj) {
                return oObj.first_name + ' ' + oObj.last_name;
            }
        },
        {
            "mData": null,
            "aTargets": [1],
            "mRender": function (data, type, oObj) {
                var takeHome = $filter('number')(oObj.take_home, 2);
                return oObj.currency + '' + takeHome;
            }
        },
        {
            "mData": null,
            "aTargets": [2],
            "mRender": function (data, type, oObj) {
                if (oObj.previousTakeHome === 'NA') {
                    return oObj.previousTakeHome;
                } else {
                    return oObj.currency + '' + oObj.previousTakeHome;
                }
            }
        },
        {
            "mData": null,
            "aTargets": [3],
            "bSortable": false,
            "mRender": function (data, type, oObj) {
                var url = getSiteUrl() + "payroll/paycheck/" + oObj.id_string_enc + '/' + oObj.id_schedule;
                return '<a href=' + url + '> View Payslip </a>';
            }
        }
    ];

    $scope.overrideOptions = {
        "bStateSave": true,
        "iCookieDuration": 2419200, /* 1 month */
        "bJQueryUI": false,
        "bPaginate": true,
        "sPaginationType": 'full_numbers',
//            "sScrollY": "300px",
//            "sScrollX": "100%",
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": true,
        "bDestroy": true
    };

});

payrollApp.controller('payrollStep4', function ($scope, $filter) {

    $scope.myCallback = function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $('td:eq(2)', nRow).bind('click', function () {
            $scope.$apply(function () {
                $scope.someClickHandler(aData);
            });
        });
        return nRow;
    };

    $scope.columnDefs = [
        {
            "mData": null,
            "aTargets": [0],
            "mRender": function (data, type, oObj) {
                return oObj.first_name + ' ' + oObj.last_name;
            }
        },
        {
            "mData": null,
            "aTargets": [1],
            "mRender": function (data, type, oObj) {
                var takeHome = $filter('number')(oObj.take_home, 2);
                return oObj.currency + '' + takeHome;
            }
        },
        {
            "mData": null,
            "aTargets": [2],
            "mRender": function (data, type, oObj) {
                if (oObj.previousTakeHome == 'NA') {
                    return oObj.previousTakeHome;
                } else {
                    return oObj.currency + '' + oObj.previousTakeHome;
                }

            }
        },
        {
            "mData": null,
            "aTargets": [3],
            "bSortable": false,
            "mRender": function (data, type, oObj) {
                var url = getSiteUrl() + "payroll/payCheckView/" + oObj.id_string_enc + '/' + oObj.id_schedule;
                return '<a href=' + url + '> View Payslip </a>';
            }
        }
    ];

    $scope.overrideOptions = {
        "bStateSave": true,
        "iCookieDuration": 2419200, /* 1 month */
        "bJQueryUI": false,
        "bPaginate": true,
        "sPaginationType": 'full_numbers',
//            "sScrollY": "300px",
//            "sScrollX": "100%",
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": true,
        "bDestroy": true
    };

});

payrollApp.controller('PayCheck', function ($scope, $http) {
    console.log('payccheck controller');
    $scope.getEmployeeEarnings = function (userID, refID) {
        console.log(userID);
        console.log(refID);
    };
});

payrollApp.controller('PayrollDisbursement', function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder) {
    //console.log('Welcome to disbursement');
    //$scope.master = false;

    $scope.employees = [];
    var vm = this;
    $scope.paymentMethod = '';
    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withPaginationType('full_numbers')
            .withDisplayLength(10)
            .withOption('bFilter', false)
            //.withBootstrap();
            ;

    $scope.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0).notSortable(),
        DTColumnDefBuilder.newColumnDef(1).notSortable()
    ];

    vm.employees = $scope.employees;
    $scope.selectedAll = true;
    $scope.employeePromise = $http.get(ajaxSource).success(function (data) {
        $scope.employees = data.aaData;
        vm.employees = $scope.employees;
        $scope.setSelected(true);
    });

    $scope.setSelected = function (sel) {
        angular.forEach($scope.employees, function (record) {
            record.selected = sel && record.can_pay;
        });
    };

    $scope.toggleCheck = function () {
        $scope.setSelected($scope.selectedAll);
    };

    $scope.computeTotalPayout = function () {
        var total = 0;
        angular.forEach($scope.employees, function (employee) {
            if (employee.selected && employee.valid_acct && employee.can_pay) {
                total += employee.payout;
            }
        });

        return total;
    };

    $scope.enablePaymentButton = function () {
        return $scope.paymentMethod !== '' && $scope.computeTotalPayout() > 0;
    };

    $scope.showPayButton = function () {
        return paymentType === 'Take Home Pay';
    };

    $scope.checkHack = function (s) {
        $scope.paymentMethod = s;
    };

    $scope.idsConcat = function (validAcct) {
        var ids = [];
        angular.forEach($scope.employees, function (employee) {
            if (employee.selected && employee.can_pay && (validAcct === undefined || employee.valid_acct === validAcct)) {
                ids.push(employee.id_user);
            }
        });
        return ids.join(',');
    };
});