/* global angular */

var checklistAssignApp = angular.module('checklistAssign', ['app.Talentbase', 'cgBusy', 'datatables']);


checklistAssignApp.controller('AssignedCtrl', function($scope, $http){
    var url = window.location.pathname + '?json=1';
    console.log('URL', url);
    $scope.assignments = [];
    $scope.employeePromise = $http.get(url)
            .success(function(data){
                $scope.assignments = data;
            });
            
});