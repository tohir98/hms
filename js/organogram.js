var Organogram = function (a) {
    var labelWidth = 180,
            labelHeight = 50,
            MIN_WIDTH = 920,
            MIN_HEIGHT = 800,
            nodeSpacingX = 40,
            nodeSpacingY = 10;

    var margin = {top: 20, right: 120, bottom: 20, left: labelWidth + 30},
    width = 960 - margin.right - margin.left,
            height = 800 - margin.top - margin.bottom;

    var i = 0,
            duration = 750,
            root;

    var tree = d3.layout.tree()
            .size([height, width]);

    var diagonal = d3.svg.diagonal()
            .projection(function (d) {
                return [d.y, d.x];
            });

    var getDepth = function (node, mLevel) {
        mLevel = mLevel || 1;
        node._depth = mLevel;
        if (node.children && node.children.length > 0) {
            var maxDepth = 0, cDepth;
            for (var i = 0; i < node.children.length; i++) {
                cDepth = getDepth(node.children[i], mLevel + 1);
                if (cDepth > maxDepth) {
                    maxDepth = cDepth;
                }
            }
            return (1 + maxDepth);
        } else {
            return 1;
        }
    };
    var svgObj = d3.select("#organogram_content").append("svg")
            .attr("width", width + margin.right + margin.left)
            .attr("height", height + margin.top + margin.bottom);
    var svg = svgObj.append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var updateSize = function () {
        var maxVisibleDepth = root ? getDepth(root) : 1;
        console.log('Adjusting size for depth: ' + maxVisibleDepth);
        width = Math.max(((labelWidth + nodeSpacingX) * maxVisibleDepth), MIN_WIDTH);

        height = Math.max(((labelHeight + nodeSpacingY) * Math.pow(2, maxVisibleDepth)), MIN_HEIGHT);
        svgObj.attr("width", width + margin.right + margin.left)
                .attr("height", height + margin.top + margin.bottom);
        tree.size([height, width]);

    };

    updateSize();

    var collapse = function (d) {
        if (d.children) {
            d._children = d.children;
            d._children.forEach(collapse);
            d.children = null;
        }
    };

    var expand = function (d) {
        if (d._children) {
            d.children = d._children;
            d._children = null;
        }
        if (d.children) {
            d.children.forEach(expand);
        }
    };

    var renderer = function (r) {
        //console.log('Depth: ', getDepth(r));
        getDepth(r);
        root = r;
        root.x0 = height / 2;
        root.y0 = 0;
        root.children = root.children || [];
        root.children.forEach(collapse);
        update(root);
    };

    d3.select(self.frameElement).style("height", "800px");

    function update(source) {

        // Compute the new tree layout.
        var nodes = tree.nodes(root).reverse(),
                links = tree.links(nodes);

        // Normalize for fixed-depth.
        nodes.forEach(function (d) {
            d.y = d.depth * 180;
        });

        // Update the nodes…
        var node = svg.selectAll("g.node")
                .data(nodes, function (d) {
                    return d.id || (d.id = ++i);
                });

        // Enter any new nodes at the parent's previous position.
        var nodeEnter = node.enter().append("g")
                .attr("class", "node")
                .attr("transform", function (d) {
                    return "translate(" + source.y0 + "," + source.x0 + ")";
                })
                .on("click", click);

        nodeEnter.append("circle")
                .attr("r", 1e-6)
                .style("fill", function (d) {
                    return d._children ? "green" : "#fff";
                });


        var labelSectionDomRoot = nodeEnter.append("foreignObject")
                .attr("x", function (d) {
                    return d.children || d._children ? -labelWidth - 10 : 10;
                })
                .attr("y", -(labelHeight / 2))
//                        .attr("text-anchor", function (d) {
//                            return d.children || d._children ? "end" : "start";
//                        })
                .style("fill-opacity", 1e-6)
                .attr('width', labelWidth)
                .attr('height', labelHeight)

                .append("xhtml:body")
                .style("margin", 0)
                .style("padding", 0)
                //                        .text(function (d) {
                //                            return  d.name + "\n"  + d.hod;
                //                        })

                ;


        var labelDiv = labelSectionDomRoot.append("div")
                .attr("class", "organogramLabel")
                ;
        labelDiv.append("h3")
                .attr("class", "organogramLabelTitle")
                .text(function (d) {
                    return d.title || '-';
                });

        labelDiv.append("h4")
                .attr("class", "organogramLabelSubtitle")
                .text(function (d) {
                    var h = d.subtitle || '';
                    return  h.trim() === '' ? ' - ' : h;
                })
                ;


        // Transition nodes to their new position.
        var nodeUpdate = node.transition()
                .duration(duration)
                .attr("transform", function (d) {
                    return "translate(" + d.y + "," + d.x + ")";
                });

        nodeUpdate.select("circle")
                .attr("r", 4.5)
                .style("fill", function (d) {
                    return d._children ? "lightsteelblue" : "#fff";
                });

        nodeUpdate.select("text")
                .style("fill-opacity", 1);

        // Transition exiting nodes to the parent's new position.
        var nodeExit = node.exit().transition()
                .duration(duration)
                .attr("transform", function (d) {
                    return "translate(" + source.y + "," + source.x + ")";
                })
                .remove();

        nodeExit.select("circle")
                .attr("r", 1e-6);

        nodeExit.select("text")
                .style("fill-opacity", 1e-6);

        // Update the links…
        var link = svg.selectAll("path.link")
                .data(links, function (d) {
                    return d.target.id;
                });

        // Enter any new links at the parent's previous position.
        link.enter().insert("path", "g")
                .attr("class", "link")
                .attr("d", function (d) {
                    var o = {x: source.x0, y: source.y0};
                    return diagonal({source: o, target: o});
                });

        // Transition links to their new position.
        link.transition()
                .duration(duration)
                .attr("d", diagonal);

        // Transition exiting nodes to the parent's new position.
        link.exit().transition()
                .duration(duration)
                .attr("d", function (d) {
                    var o = {x: source.x, y: source.y};
                    return diagonal({source: o, target: o});
                })
                .remove();

        // Stash the old positions for transition.
        nodes.forEach(function (d) {
            d.x0 = d.x;
            d.y0 = d.y;
        });
    }

    // Toggle children on click.
    function click(d) {
        //console.log("My depth", d._depth);
        if (d.children) {
            d._children = d.children;
            d.children = null;
        } else {
            d.children = d._children;
            d._children = null;
        }
        updateSize();
        update(d);
    }

    this.expandAll = function () {
        expand(root);
        updateSize();
        update(root);
    };

    this.collapseAll = function () {
        collapse(root);
        updateSize();
        update(root);
    };

//    this.savePng = function () {
//        var html = svgObj.attr("version", 1.1)
//                .attr("xmlns", "http://www.w3.org/2000/svg")
//                .node().parentNode.innerHTML;
//
//        //console.log(html);
//        var imgsrc = 'data:image/svg+xml;base64,' + btoa(html);
//        var img = '<img src="' + imgsrc + '">';
//        $("#organogram_content").append(img);
//
//
//        var canvasDom = document.createElement('canvas'); 
//        $(canvasDom).height(svgObj.attr('height'))
//                .width(svgObj.attr('width'));
//        
//        
//        $('#organogram_content').append(canvasDom);
//        var canvas = canvasDom,
//                context = canvas.getContext("2d");
//        var oldData = canvas.toDataURL("image/png");
//        
//        var image = new Image;
//        image.src = imgsrc;
//        image.onload = function () {
//            context.drawImage(image, 0, 0);
//
//            var canvasdata = canvas.toDataURL("image/png");
//            //var pngimg = '<img src="' + canvasdata + '">';
//            //d3.select("#pngdataurl").html(pngimg);
//            console.log('Changed: ', oldData != canvasdata);
//
//            var a = document.createElement("a");
//            a.download = "organogram.png";
//            a.href = canvasdata;
//            a.click();
//            //$(img).remove();
//        };
//        context.drawImage(image, 0, 0);
//    };
    renderer(a);
};


