/* global angular */

var staffDirApp = angular.module('staffDir', ['app.Talentbase', 'cgBusy', 'datatables', 'datatables.tabletools', 'datatables.scroller', 'datatables.fixedcolumns']);

staffDirApp.controller('ListCtrl', function ($scope, $http, SiteUrl, $filter, DTOptionsBuilder, DTColumnDefBuilder) {
    $scope.config = window.staffDirConfig || {};
    $scope.employees = [];
    $scope.filter = {};
    $scope.noSelect = false;
    $scope.data = {selectedChecklist: ''};

    $scope.employeesPromise = $http.get(SiteUrl.get('user/employee/view_staff_directory_json'))
            .success(function (data) {
                $scope.employees = data.aaData;
            });


    $scope.toggleAll = function () {
        angular.forEach($scope.employees, function (emp) {
            emp.is_selected = !emp.is_selected;
        });
    };

    $scope.validPhone = function (p) {
        return p && p.replace(/[^0-9]+/g, '').length > 0;
    };

    $scope.statusMessages = {};
    $scope.statusMessages[$scope.config.probation_no_access] = 'This employee is on probation and has no access to employee Portal. Please set probation end-date below';
    $scope.statusMessages[$scope.config.probation_access] = 'This employee is on probation and has access to employee Portal. Please set probation end-date below';
    $scope.statusMessages[$scope.config.active_no_access] = 'This employee will not have access to the employee portal. Recommended for semi/unskilled employees';

    $scope.currentEmployee = {};
    $scope.editStatus = function (employee) {
        $scope.currentEmployee = angular.copy(employee);
        $('#change_employee_status_modal').modal();
    };

    $scope.resetPassword = function (employee) {
        $scope.currentEmployee = angular.copy(employee);
        $('#reset_employee_password_modal').modal();
    };

    $scope.selectedEmployees = [];

    $scope.bulkAction = function (employees, modal) {
        $scope.selectedEmployees = [];
        $scope.noSelect = false;
        if (employees) {
            if (employees instanceof Array) {
                $scope.selectedEmployees = employees;
            } else {
                $scope.selectedEmployees.push(employees);
            }
        } else {
            $scope.selectedEmployees = $filter('filter')($scope.employees, {is_selected: true});
        }

        if ($scope.selectedEmployees.length < 1) {
            $scope.noSelect = true;
            return;
        }

        $(modal).modal();
    };

    $scope.buildIdList = function (employees, field, delim) {
        field = field || 'id_user';
        delim = delim || ',';
        var list = [];
        angular.forEach(employees, function (e) {
            list.push(e[field]);
        });
        return list.join(delim);
    };

    $scope.assignChecklist = function () {
        if ($scope.data.selectedChecklist !== '' && $scope.selectedEmployees.length > 0) {
            window.location = SiteUrl.get('employee/checklist/assign/' + $scope.data.selectedChecklist + '?ids=' +
                    $scope.buildIdList($scope.selectedEmployees, 'id_string', '|'));
        }
    };

    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withPaginationType('full_numbers')
            .withDisplayLength(10)
            .withOption('bFilter', true)
            .withTableTools(SiteUrl.get('/js/plugins/datatable/swf/copy_csv_xls_pdf.swf'))
            .withTableToolsButtons([
                'copy',
                'print', {
                    'sExtends': 'collection',
                    'sButtonText': 'Save',
                    'aButtons': ['csv', 'xls', 'pdf']
                }
            ])
            //.withScroller()
            //.withDOM('lfrti')
            //.withOption('deferRender', true)
            // Do not forget to add the scorllY option!!!
            ;
    //.withBootstrap();
    ;

    $scope.dtColumnDefs = [DTColumnDefBuilder.newColumnDef(0).notSortable(), DTColumnDefBuilder.newColumnDef(1).notSortable()];
});


staffDirApp.filter('capitalize', function () {
    return function (input) {
        if (!input) {
            return input;
        }

        var parts = input.split(/[_\s]+/);
        for (var i = 0; i < parts.length; i++) {
            parts[i] = parts[i][0].toUpperCase() + parts[i].substr(1);
        }
        return parts.join(' ');
    };
});

