/* global angular, TalentBase */

var JobCreditApp = angular.module('JobCreditApp', ['cgBusy', 'datatables', 'app.Talentbase']);

JobCreditApp.controller('ActionCtrl', function ($scope, $filter) {
    $scope.availableActions = JobCreditApp.availableActions || [];
    $scope.appliedActions = JobCreditApp.appliedActions || [];
    $scope.currentBalance = JobCreditApp.currentBalance || 0;

    $scope.creditsRequired = function () {
        var t = 0;
        angular.forEach($scope.availableActions, function (action) {
            if (action.is_selected && !$scope.isSelected(action.id)) {
                t += parseInt(action.credits, 10);
            }
        });

        return t;
    };

    $scope.isSelected = function (actionId) {
        return $filter('filter')($scope.appliedActions, {id_job_action: actionId}).length > 0;
    };

    $scope.confirmActions = function () {
        var message = "Please cofirm that you want to apply the following action(s):<br/>";
        angular.forEach($scope.availableActions, function (act) {
            if (act.is_selected) {
                message += '- ' + act.name + '<br/>';
            }
        });
        var credits = $scope.creditsRequired();
        message += '<p>This will cost a total of ' + credits + ' job credit(s), making your new balance: ' + ($scope.currentBalance - credits) + ' </p>';
        TalentBase.doConfirm({
            message: message,
            onAccept: function () {
                $('#JobCreditActionForm').submit();
            },
            title: 'Confirm Actions',
            acceptText: 'Yes, Continue'
        });
    };

});

JobCreditApp.directive('buyCredit', function () {
    return {
        restrict: 'A',
        link: function (scope, elem) {
            elem.click(function (e) {
                e.preventDefault();
                $('#buyCreditModal').modal();
            });
        }
    };
});


JobCreditApp.controller('BuyCreditCtrl', function ($scope) {
    $scope.creditPackages = JobCreditApp.creditPackages;
    $scope.selectedPackage = $scope.creditPackages[0];
    $scope.quantity = 1;

});

JobCreditApp.controller('HistoryCtrl', function ($scope, $http, SiteUrl, DTOptionsBuilder) {
    $scope.creditHistory = [];
    $scope.creditPromise = $http.get(SiteUrl.get('/recruiting/job_credit/history'))
            .success(function (data) {
                $scope.creditHistory = data;
            });

    $scope.sources = {
        jobPost: 'Job Post',
        paymentReceipt: 'Credit Purchase',
        superAdmin: 'Support'
    };

    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withPaginationType('full_numbers')
            .withDisplayLength(10)
            //.withOption('bFilter', false)
            .withOption('order', [[0, 'desc'], [1, 'desc']])
            //.withBootstrap();
            ;

    
    
});

JobCreditApp.filter('formatCredit', function () {
    return function (input) {
        if (input < 0) {
            return "(" + Math.abs(input) + ")";
        } else {
            return input;
        }
    };
});