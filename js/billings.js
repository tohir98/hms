/* global angular */

var billingApp = angular.module('AddBilling', []);

billingApp.controller('AddController', function ($scope) {
    $scope.data = {items: [], tax: 5};
    $scope.blankItem = {id: null, item_name: '', description: '', unit_cost: 0, qty: 0};

    $scope.addItem = function () {
        $scope.data.items.push(angular.copy($scope.blankItem));
    };

    $scope.removeItem = function (idx) {
        $scope.data.items.splice(idx, 1);
        $scope.itemsCheck();
    };

    $scope.itemsCheck = function () {
        if ($scope.data.items.length < 1) {
            $scope.addItem();
        }
    };

    $scope.getSubtotal = function () {
        var sum = 0;
        for (var i = 0; i < $scope.data.items.length; i++) {
            sum += $scope.data.items[i].unit_cost * $scope.data.items[i].qty;
        }
        return sum;
    };

    $scope.getTotal = function () {
        return $scope.getSubtotal() * (1 + $scope.data.tax / 100.0);
    };

    $scope.itemsCheck();

});


var userBillingApp = angular.module('BillingApp', ['app.Talentbase', 'datatables', 'cgBusy']);

userBillingApp.columnFilters = function (filterable, that) {
    var api = that.api();
    api.columns().indexes().flatten().each(function (i) {
        if (filterable.hasOwnProperty(i)) {
            var column = api.column(i);
            var select = $('<select style="width: auto;"><option value="">' + filterable[i] + '</option></select>')
                    .appendTo($(column.header()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                        column.search(val ? val : '', true, false)
                                .draw();
                    });

            column.data().unique().sort().each(function (d, j) {
                var f = $('<span>' + d + '</span>').text().trim();

                select.append('<option value="' + f + '">' + f + '</option>');
            });
        }
    });
};

userBillingApp.controller('ReceiptsCtrl', function ($scope, $http, SiteUrl, DTOptionsBuilder, DTColumnDefBuilder) {
    $scope.receipts = [];
    $scope.receiptsPromise = $http.get(SiteUrl.get('billings/payment_bills?json=1'))
            .success(function (d) {
                $scope.receipts = d;
            })
            ;

    $scope.filterable = {
        3: 'VAT Inclusive',
        6: 'Payment Status'
    };

    $scope.initComplete = function () {
        userBillingApp.columnFilters($scope.filterable, this);
    };

    $scope.dtColumnDefs = [];
    angular.forEach(Object.keys($scope.filterable), function (idx) {
        //console.log('IDX', typeof  idx, idx);
        $scope.dtColumnDefs.push(DTColumnDefBuilder.newColumnDef(parseInt(idx, 10)).notSortable());
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withPaginationType('full_numbers')
            .withDisplayLength(10)
            .withOption('order', [[0, 'desc'], [1, 'desc']])
            .withOption('initComplete', $scope.initComplete)
            //.withOption('aoColumnDefs', [{bSortable: false, aTargets: [3, 5]}])
            ;


    $scope.url = SiteUrl.get;

    $scope.cancelReceipt = function (receipt) {
        window.location.href = SiteUrl.get('payment/receipt/' + receipt.id + '/' + receipt.ref + '/cancel');
    };
});

userBillingApp.controller('TransactionsCtrl', function ($scope, $http, SiteUrl, DTOptionsBuilder, DTColumnDefBuilder) {
    $scope.transactions = [];
    $scope.txnPromise = $http.get(SiteUrl.get('billings/transaction_history?json=1'))
            .success(function (d) {
                $scope.transactions = d;
            })
            ;

    $scope.filterable = {
        3: 'Payment Method',
        5: 'Status'
    };

 
    $scope.dtColumnDefs = [];
    angular.forEach(Object.keys($scope.filterable), function (idx) {
        //console.log('IDX', typeof  idx, idx);
        $scope.dtColumnDefs.push(DTColumnDefBuilder.newColumnDef(parseInt(idx, 10)).notSortable());
    });

    $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withPaginationType('full_numbers')
            .withDisplayLength(10)
            .withOption('order', [[0, 'desc'], [1, 'desc']])
            .withOption('initComplete', function () {
                userBillingApp.columnFilters($scope.filterable, this);
            });


    $scope.url = SiteUrl.get;


});
