angular.module('tb.payment', ['cgBusy', 'datatables', 'datatables.tabletools', 'datatables.scroller', 'datatables.fixedcolumns'])
        .controller('PayoutCtrl', function ($scope, $http, DTOptionsBuilder, DTColumnDefBuilder) {
            $scope.recipients = [];

            $scope.recipientPromise = $http.get(recipientsUrl).success(function (data) {
                $scope.recipients = data;
            });

            $scope.dtOptions = DTOptionsBuilder.newOptions()
                    //.withPaginationType('full_numbers')
                    //.withDisplayLength(10)
                    .withOption('bFilter', false)
                    .withOption('paging', false)
                    .withTableTools('/js/plugins/datatable/swf/copy_csv_xls_pdf.swf')
                    .withTableToolsButtons([
                        'copy',
                        'print', {
                            'sExtends': 'collection',
                            'sButtonText': 'Save',
                            'aButtons': ['csv', 'xls', 'pdf']
                        }
                    ])
                    .withScroller()
                    .withDOM('lfrti')
                    //.withOption('deferRender', true)
                    // Do not forget to add the scorllY option!!!
                    .withOption('scrollY', 300)
                    .withOption('scrollX', '100%')
                    ;
            //.withBootstrap();
            ;

            $scope.dtColumnDefs = [DTColumnDefBuilder.newColumnDef(0).notSortable()];



            $scope.computeTotalPayment = function () {
                var t = 0;
                angular.forEach($scope.recipients, function (r) {
                    t += parseFloat(r.amount);
                });
                return t;
            };

            $scope.computeTotalProcessingFee = function () {
                var t = 0;
                angular.forEach($scope.recipients, function (r) {
                    t += parseFloat(r.processing_fee);
                });
                return t;
            };
        });
