/* global angular, Const_NIGERIA */

'use strict';

var JobApp = angular.module('JobApp', ['app.Talentbase']);


JobApp.controller('formCtrl', function ($scope, SiteUrl, $http, $timeout) {
    $scope.job = window.Job ? window.Job : {
        title: '',
        id_category: [],
        id_function: [],
        id_type: [],
        description: '',
        desired_qualities: '',
        id_country: window.Const_NIGERIA,
        id_job_level: window.Const_JOB_LEVEL_PROF,
        id_state: [],
        file_attachment: {},
        questions: {}
    };

    $scope.questionTypes = {
        admin: 'Previous Questions',
        system: 'Sample Questions'
    };


    $scope.statesStatus = 'Select a country';
    $scope.states = {};
    $scope.systemQuestions = window.SystemQuestions || {};
    $scope.adminQuestions = window.AdminQuestions || {};




    $scope.loadStates = function () {
        if (!$scope.job.id_country) {
            return;
        }
        var url = SiteUrl.get('recruiting/recruiting/states_select/' + $scope.job.id_country + '.json');
        $scope.statesStatus = 'Loading states...';
        $http.get(url)
                .success(function (data) {
                    $scope.statesStatus = '';
                    $scope.states = data;
                    //console.log('States', $scope.states);
                })
                .error(function () {
                    $scope.statesStatus = 'Not Applicable';
                });
    };

    $scope.attachementInView = -1;
    $scope.removeAttachment = function (id, force) {
        var isExisting = !isNaN(parseInt(id));
        if (!isExisting || force) {
            var backup = $scope.job.file_attachment;
            delete $scope.job.file_attachment[id];
            if (isExisting) {
                var url = SiteUrl.get('recruiting/recruiting/delete_file/' + id + '/' + $scope.job.id_reference);
                $http.post(url)
                        .success(function (data) {
                            console.log('Deleted in background', data);
                        })
                        .error(function () {
                            console.warn('Could not complete deletion');
                            $scope.job.file_attachment[id] = backup;
                        });
            }
        } else {
            $scope.attachementInView = id;
            $('#delete_doc_box').modal();
        }
    };

    $scope.addAttachment = function () {
        $scope.job.file_attachment['tmp_' + Date.now()] = '';
    };

    $scope.questionType = '-';
    $scope.addQuestion = function (type, q) {
        //console.log('Add cliecked', type, q);
        if (q !== undefined) {
            $scope.job.questions['tmp_' + Date.now()] = q;
        } else {
            $scope.questionType = type;
            $scope.questionSelected = '';
            $scope.questionArr = type === 'admin' ? $scope.adminQuestions : $scope.systemQuestions;
            $('#questionModal').modal();
        }
    };

    $scope.questionInView = -1;
    $scope.removeQuestion = function (qId, force) {
        if (!force) {
            $scope.questionInView = qId;
            $('#delete_ques_box').modal();
        } else {
            delete $scope.job.questions[qId];
        }
    };

    $scope.hasQuestions = function () {
        return Object.keys($scope.job.questions).length > 0;
    };

    $scope.isStateSelected = function (stateId) {
        stateId = (stateId) + '';
//        for (var j in $scope.job.id_state) {
//            if ($scope.job.hasOwnProperty(j) && parseInt($scope.job.id_state[j]) === stateId) {
//                return true;
//            }
//        }
        //       return $scope.job.id_state.indexOf(stateId) > -1;
    };
    
    $scope.loadStates();


});

JobApp.directive('ckeditor', function ($timeout) {

    return {
        restrict: 'A',
        link: function (scope, element) {
            $timeout(function () {
                window.CKEDITOR.replace(element.attr('name'), {
                    toolbar: [{name: 'document', items: ['Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList']}],
                    uiColor: '#F9F9F9'
                });
            }, 110);
        }
    };
});
