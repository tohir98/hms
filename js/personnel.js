var personnelApp = angular.module('PersonnelReport', ['angularUtils.directives.dirPagination', 'cgBusy', 'datatables', 'ngResource']);
var getSiteUrl = function(){
    return window.location.protocol + '//' + window.location.host + '/';
};
personnelApp.controller('AddController', function($scope, $http) {
    
    $scope.employees = [{id_string:'',
            'last_name':'',
            'status':'',
            'departments':'',
            'location_tag':'',
            'work_email':'',
            'work_phone':'',
            'home_address':'',
            'birthday':'',
            'personal_phone':'',
            'emergency1':'',
            'emergency2':'',
            'bank_account':'',
            'documents':'',
            'uploaded_docs':'',
            'pension_details':'',
            is_checked: false} ];
    
    $scope.statuses = {'0':'Inactive', '1':'Active', '2':'Suspended', '4':'Probation/Access', '5':'Active/No access', '6':'Probation/No access'};

	$scope.myPromise = $http.get(getSiteUrl() + "user/employee/personnel_report_json")
        .success(function(response) {$scope.employees = response;});

	
	
	//$scope.itemsCheck();

});
