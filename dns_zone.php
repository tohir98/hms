<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

error_reporting(E_ALL & ~E_NOTICE);
$inFile = $argv[1];
if (!$inFile || !file_exists($inFile)) {
    die('Invalid input file');
}

$outFile = $argv[2];
if (!$outFile) {
    $outFile = basename($inFile) . '.zone';
}

$contents = json_decode(file_get_contents($inFile), true);

$recordsList = $contents['recordsList']['records'];

$domain = $contents['name'];

$text = [];
$text[] = '; Records for ' . $domain;
$text[] = '$TTL ' . $contents['ttl'];

//name ttl in type priority value
//$format = "%-25s %-10s IN %-15s %-10s %-40s; created: %s, last updated: %s";
$format = "%-25s %-10s IN %-15s %-10s %-40s";

foreach ($recordsList as $record) {
    $type = $record['type'];

    if (!in_array($type, ['NS', 'SOA'])) {
        $entry = $record['name'];
        if (in_array($type, ['A', 'CNAME', 'TXT', 'MX']) && strpos($entry, $domain) >= 0) {
//            $tmp = explode('.' . $domain, $entry);
//            array_pop($tmp);
//            $entry = join('', $tmp);
            $entry = $entry . '.';
        }
        

        $data = $record['data'];
        if ($type !== 'TXT' && !preg_match('/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/', $data)) {
            $data .= '.';
        }

        if ($type === 'TXT') {
            $data = "\"{$data}\"";
        }


        $text[] = sprintf($format, $entry, //
                $record['ttl'], //
                $type, //
                $record['priority'] ? : '', //
                $data, //
                $record['created'], //
                $record['updated']  //
        );
    }
}

file_put_contents($outFile, join("\n", $text));
