HMS v.1.0.0
==================

Version One of HMS (after Beta version).

This project based on Codeigniter framework: http://ellislab.com/codeigniter

Documentation: http://ellislab.com/codeigniter/user-guide/

Base coding standards and requirements.
---------------------------------------

Set up
------
Install composer, see https://getcomposer.org/download/ for information on how to set up composer, run composer install from the project root directory.

```
composer install
```

MySQL Database Versioning
-------------------------
Versioning of database should follow the guidelines in the specified guidelines document: https://bitbucket.org/talentbase/talentbasev01/wiki/Database%20Migration%20Guide


MySQL Table and field names
---------------------------

Invalid names:

```
userpermissions, userid, usergroupvalid, usergroupid
```

Valid names:
```
user_permissions, id_user, valid_user_group, id_user_group
```

PHP Variables and function names
--------------------------------
Invalid names:

```
function getidbyemail();
function GetIDByEmail();
function Get_ID_By_Email();

$userid;
$useraccountmodifieddate;
$UserAccountModifiedDate;
```

Valid names:

```
function get_id_by_email();

$id_user;
$date_modified_user_account;
```

PHP file comments
-----------------

File header

```
/**
 * Talentbase
 * {Name} Controller, accepts {methods/actions} needs.
 * 
 * @category   Web project
 * @package    {Package of application}
 * @subpackage {Sub package of application}
 * @author     {Developer name} <developer email>
 * @copyright  Copyright © 2015 CleanSoft Nigeria Ltd.
 * @license    
 * @version    1.0.0
 * @link       
 * @since      File available since Release 1.0.0
 * @deprecated {File deprecated in Release 2.0.0 - write this tag/line if actually this file is deprecated}
 * @todo       {to do works explained here}
 */
 ```

For some reasons, view files can pass this style of comment and just reference to Controller name.
 
Variable comments in class

``` 
/**
 * {Variable description}
 *
 * @var bool
 * @access private
 */
 private $_good = true;
```

Function comments

```
/**
 * {Function description}
 *
 * @access public
 * @param string $arg1 {something about this arg}
 * @param int $arg2 {something about this arg}
 * @return string|bool 
 * @since {Method available since Release 1.0.0}
 * @deprecated {Method deprecated in Release 2.0.0}
 * @todo {explain what to do}
 */
public function test($arg1, $arg2) {

} // End func test
``` 


Copyright © 2015 CleanSoft Technologies.
----------------------------------------
