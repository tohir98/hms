<?php

namespace setup;

require_once 'CompanySetup.php';

if (!Setup::isSetup('email')) {
    setup_redirect('email');
}

if (Setup::isSetup('company')) {
    setup_redirect('license');
}

$error = '';
$data = [];

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $data = filter_input_array(INPUT_POST);
    $error = '';
    $logoValidator = null;
    if(CompanySetup::validate($data, $error, $logoValidator)){
        $setup = new CompanySetup($data, $logoValidator);
        if($setup->process()){
            setup_redirect('license');
        }
        
        $error = 'An error occured while saving your company data.';
    }
}

$page['title'] = 'Company Setup';

$page['callback'] = function() use ($data, $error) {
    ?>
    <link href="../css/plugins/colorpicker/colorpicker.css" rel="stylesheet" type="text/css"/>
    <script src="../js/plugins/colorpicker/bootstrap-colorpicker.js" type="text/javascript"></script>

    <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h1 class="panel-title">
                    Company and Admin Setup
                </h1>
            </div>
            <div class="panel-body">
                <?= display_error($error) ?>
                <fieldset>
                    <legend>
                        Company Information
                    </legend>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Company Name</label> 
                        <div class="controls col-md-4">
                            <input type="text" required="" class="form-control input-sm" name="company[company_name]" value="<?= $data['company']['company_name'] ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Short Profile</label> 
                        <div class="controls col-md-8">
                            <textarea required="" rows="4" name="company[banner_text]" class="form-control input-sm" ><?= $data['company']['banner_text'] ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Company Profile</label> 
                        <div class="controls col-md-8">
                            <textarea required="" rows="10" name="company[about_us]" class="form-control input-sm" ><?= $data['company']['about_us'] ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Industry</label> 
                        <div class="controls col-md-4">
                            <select class="form-control input-sm" name="company[id_industry]">
                                <option></option>
                                <?php foreach (get_indusries() as $id => $ind):
                                    ?>
                                    <option value="<?= $id ?>" <?= $id == $data['company']['id_industry'] ? 'selected=""' : '' ?> ><?= $ind; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Company Website</label> 
                        <div class="controls col-md-6">
                            <input type="url" class="form-control input-sm" name="company[website]" value="<?= $data['company']['website'] ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Logo Type</label> 
                        <div class="controls col-md-3">
                            <select class="form-control input-sm" name="logo_type" id="logo_type">
                                <option></option>
                                <?php foreach (['text' => 'Text', 'image' => 'Image'] as $id => $type):
                                    ?>
                                    <option value="<?= $id ?>" <?= $id == $data['logo_type'] ? 'selected=""' : '' ?> ><?= $type; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group logo-property" id='text'>
                        <label class="col-md-4 control-label">Logo Text</label> 
                        <div class="controls col-md-4">

                            <input type="text" class="form-control input-sm" name="company[logo_text]" value="<?= $data['company']['logo_text'] ?>" size="16" maxlength="16"/>

                        </div>
                    </div>

                    <div class="form-group logo-property" id='image'>
                        <label class="col-md-4 control-label">Upload Logo</label> 
                        <div class="controls col-md-4">
                            <input type="file" class="form-control input-sm" name="logo_image_file" required=""/>
                            <div class="help-block">
                                <em>Recommended size: 84x84</em>
                            </div>

                        </div>
                    </div>

                    <div class="form-group" >
                        <label class="col-md-4 control-label">Background Color</label> 
                        <div class="controls col-md-4">
                            <input type="text" data-horizontal="true" class="form-control demo colorpicker-element input-sm" name="company[bg_color]" required="" value="<?= $data['company']['bg_color'] ?>" id='company_bg_color'/>

                        </div>
                    </div>


                </fieldset>

                <fieldset>
                    <legend>Admin Account</legend>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Name</label> 
                        <div class="controls col-md-3">
                            <input type="text" required="" class="form-control input-sm" name="admin[first_name]" value="<?= $data['admin']['first_name'] ?>" placeholder="First name"/>
                        </div>
                        <div class="controls col-md-3">
                            <input type="text" required="" class="form-control input-sm" name="admin[last_name]" value="<?= $data['admin']['last_name'] ?>" placeholder="Last name"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">E-Mail</label> 
                        <div class="controls col-md-5">
                            <input type="email" required="" class="form-control input-sm" name="admin[email]" value="<?= $data['admin']['email'] ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Department</label> 
                        <div class="controls col-md-5">
                            <input type="text" required="" class="form-control input-sm" name="dept[department]" value="<?= $data['dept']['department'] ? : 'Corporate' ?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Password</label> 
                        <div class="controls col-md-4">
                            <input type="password" required="" class="form-control input-sm" name="admin[password]"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Re-type Password</label> 
                        <div class="controls col-md-4">
                            <input type="password" required="" class="form-control input-sm" name="password_1"/>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="panel-footer text-right">
                <button type="submit" class="btn btn-success">Save Company &amp; Admin Information</button>
            </div>
        </div>

    </form>

    <script>
        $(function () {
            $('#logo_type').change(function () {
                $('.logo-property').hide().find('input').attr('disabled', 'disabled');
                var id = $('#logo_type').val();
                if (id !== '') {
                    $('#' + id).show().find('input').removeAttr('disabled');
                }
            }).change();

            $('#company_bg_color').colorpicker({
                format: 'rgba', // force this format
                horizontal: true
            });
            ;
        });
    </script>
    <?php
};

function get_indusries() {
//    static $rows = [];
//    if (empty($rows)) {
//        $result = Setup::getDb()->query('select id_industry, name from industries');
//        foreach ($result as $row) {
//            $rows[$row['id_industry']] = $row['name'];
//        }
//    }
//    return $rows;
    $result = Setup::getDb()->query('select id_industry, name from industries');
    foreach ($result as $row) {
        yield $row['id_industry'] => $row['name'];
    }
}
