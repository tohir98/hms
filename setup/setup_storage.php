<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace setup;

require_once APP_ROOT . 'application/libraries/storage.php';

if (!Setup::isSetup('db')) {
    setup_redirect('db');
}

if(Setup::isSetup('storage')){
    setup_redirect('email');
}

$page['title'] = 'Storage Configuration';

$data = array();
$adapters = array(
    'amazons3' => 'Amazon S3',
    'cloudfiles' => 'Rackspace Cloudfiles',
    'localstorage' => 'Local Storage',
);
$error = '';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $storageConfig = filter_input_array(INPUT_POST);

    if (!empty($storageConfig)) {
        try {
            $storage = new \storage($storageConfig, true);
            $temp = __DIR__ . uniqid('/storage_test_'). '.txt';
            file_put_contents($temp, 'Test file from setup');
            
            if ($storage->getAdapter()->saveFile($temp, basename($temp))) {
                //write config
                $export =  "<?php\n\n\$config['storage'] = " . var_export($storageConfig, true) . ";\n";
                $saved = file_put_contents(CONFIG_DIR . 'storage.php', $export);
                if ($saved) {
                    setup_redirect('email');
                }

                $error = 'Failed writing storage configuration.';
            } else {
                $error = 'Test file could not be saved.';
            }
        } catch (\Exception $ex) {
            $error = $ex->getMessage();
        }
        $data = $storageConfig;
    }
}

$page['callback'] = function() use ($error, $data, $adapters) {
    ?>
    <form class="form-horizontal" action="" method="post">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2 class="panel-title">Configure Storage</h2>
            </div>
            <div class="panel-body">
                <?= display_error($error); ?>
                <div class="form-group">
                    <label class="col-md-3 control-label">Storage Adapter</label> 
                    <div class="controls col-md-4">
                        <select class="form-control input-sm" required="" name="adapter" id='adapter'>
                            <option></option>
                            <?php foreach ($adapters as $adapter => $name): ?>
                                <option value="<?= $adapter ?>" <?= $adapter == $data['adapter'] ? 'selected=""' : '' ?>><?= $name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <fieldset>
                    <!--                    <legend>Storage Options</legend>-->
                    <div class="storage-options" id="amazons3">
                        <div class="form-group">
                            <label class="col-md-3 control-label">S3 Bucket</label> 
                            <div class="controls col-md-4">
                                <input type="text" required="" class="form-control input-sm" name="amazons3[bucket]" value="<?= $data['amazons3']['bucket'] ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Access Key</label> 
                            <div class="controls col-md-4">
                                <input type="text" class="form-control input-sm" name="amazons3[access_key]" value="<?= $data['amazons3']['access_key'] ?>" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Access Secret</label> 
                            <div class="controls col-md-4">
                                <input type="text" class="form-control input-sm" name="amazons3[access_secret]" value="<?= $data['amazons3']['access_secret'] ?>" required=""/>
                            </div>
                        </div>
                    </div>
                    <div class="storage-options" id="cloudfiles">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Username</label> 
                            <div class="controls col-md-4">
                                <input type="text" class="form-control input-sm" name="cloudfiles[username]" value="<?= $data['cloudfiles']['username'] ?>" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">API Key</label> 
                            <div class="controls col-md-4">
                                <input type="text" class="form-control input-sm" name="cloudfiles[api_key]" value="<?= $data['cloudfiles']['api_key'] ?>" required=""/>
                            </div>
                        </div>
                    </div>
                    <div class="storage-options" id="localstorage">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Storage Path</label> 
                            <div class="controls col-md-4">
                                <input type="text" class="form-control input-sm" name="localstorage[dir]" value="<?= $data['localstorage']['dir'] ?>" required=""/>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="panel-footer text-right">
                <button type="submit" class="btn btn-success">Save Config</button>
            </div>

        </div>

    </form>
    <script>
        $(function () {
            $('#adapter').change(function () {
                $('.storage-options input,.storage-options select').attr('disabled', '');
                $('.storage-options').hide();
                var id = $(this).val();
                $('#' + id).show()
                        .find('input,select').removeAttr('disabled');
            }).change();
        });
    </script>
    <?php
};
