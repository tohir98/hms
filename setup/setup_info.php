<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace setup;


if(!Setup::isSetup('license')){
    setup_redirect('license');
}

$page['title'] = 'Install Information';


$page['callback'] = function(){
    $package = \Packageinfo::instance();
    $license = Setup::licenseInfo();
    ?>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h1 class="panel-title">Info</h1>
    </div>
    <table class="table table-striped">
        <tr>
            <th>Registered to</th>
            <td><?= $license['company_name']; ?></td>
        </tr>
        
        <tr>
            <th>Activation Date</th>
            <td><?= date('M d, Y', strtotime($license['activation_date'])); ?></td>
        </tr>
        
        <tr>
            <th>Expires </th>
            <td><?= date('M d, Y', strtotime("+{$license['validity']} months", strtotime($license['activation_date']))); ?></td>
        </tr>
        
        <tr>
            <th>Package</th>
            <td>
                <?= $license['package'] ?>
            </td>
        </tr>
        
        <tr>
            <th>Version</th>
            <td>
                <?= $package->version ?>
            </td>
        </tr>
        
        <tr>
            <th>Home Page</th>
            <td>
                <a target="_blank" href="http://<?= $_SERVER['HTTP_HOST'] ?>"><?= $_SERVER['HTTP_HOST'] ?></a>
            </td>
        </tr>
        
    </table>
</div>
<?php
};