<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace setup;

ini_set('display_errors', 'Off');
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);

use Exception;
use mysqli;

if (!defined('APP_ROOT')) {
    define('APP_ROOT', realpath(__DIR__ . '/..') . DIRECTORY_SEPARATOR);
}

if (!defined('ENVIRONMENT')) {
    define('ENVIRONMENT', getenv('TB_ENV') ? : 'production');
}

if (!defined('BASEPATH')) {
    define('BASEPATH', APP_ROOT . 'system');
}

define('CONFIG_DIR', APP_ROOT . 'application/config/');

if (!defined('TALENTBASE_BUSINESS_NAME')) {
    require_once CONFIG_DIR . 'constants.php';
}

require APP_ROOT . 'application/libraries/packageinfo.php';

if (!file_exists(CONFIG_DIR . '.ONSITE_MODE')) {
    header("HTTP/1.0 403 Access Denied");
    die('Access Denied');
}

require_once 'layout.phtml';

/**
 * Description of Setup
 *
 * @author JosephT
 */
class Setup {

    //put your code here
    private static $checks = array(
        'db',
        'storage',
        'email',
        'company',
        'license',
        'info',
    );
    public static $extensions = array(
        'ldap',
        'mbstring',
        'mysqli',
        'pdo_mysql',
        'curl',
        'zip',
        ['gd', 'gd2'],
            //'zxy',
    );

    public static function isSetup($state = null) {
        foreach (self::$checks as $ch) {
            $m = 'has' . ucfirst($ch);
            if (!method_exists(__CLASS__, $m)) {
                return false;
            }
            $r = call_user_func(array(__CLASS__, $m));
            if (!$r) {
                return false;
            }
            if ($state === $ch) {
                return true;
            }
        }
        return true;
    }

    public static function hasDb() {
        return file_exists(CONFIG_DIR . 'database.php');
    }

    public static function hasStorage() {
        return file_exists(CONFIG_DIR . 'storage.php');
    }

    public static function hasEmail() {
        return file_exists(CONFIG_DIR . 'email.php');
    }

    public static function hasCompany() {
        if (self::hasDb()) {
            $result = self::getDb()->query("select * from companies");
            $rowCount = $result->num_rows;
            return $rowCount > 0;
        }
        return false;
    }

    public static function hasLicense() {
        return file_exists(CONFIG_DIR . 'talentbase.lic');
    }

    public static function saveLicense($content) {
        return file_put_contents(CONFIG_DIR . 'talentbase.lic', gzencode($content));
    }

    public static function licenseInfo() {
        if (self::hasLicense()) {
            //$c = file(CONFIG_DIR . 'talentbase.lic');
            $c = preg_split('/[\r\n]+/', gzdecode(file_get_contents(CONFIG_DIR . 'talentbase.lic')));
            $info = json_decode($c[1], true);
            if ($info) {
                $info['key'] = $c[0];
            }
            return (array) $info;
        }
        return [];
    }

    public static function isStep($c) {
        return in_array($c, self::$checks);
    }

    public static function checkMissingExtensions() {
        $missing = array();
        foreach (self::$extensions as $ext) {
            $ext = is_array($ext) ? $ext : [$ext];
            $found = false;
            foreach ($ext as $ext1) {
                if (extension_loaded($ext1)) {
                    $found = true;
                }
            }

            if (!$found) {
                $missing = join('|', $ext);
            }
        }
        return $missing;
    }

    /**
     * @return \mysqli Description
     */
    public static function getDb() {
        static $mysqli = null;
        if (!$mysqli) {
            include CONFIG_DIR . 'database.php';
            $dbMain = $db['default'];
            $mysqli = @new mysqli($dbMain['hostname'], $dbMain['username'], $dbMain['password'], $dbMain['database']);

            if ($mysqli->connect_error || $mysqli->error) {
                throw new Exception('DB Error while retrieving system modules: ' . ($mysqli->connect_error ? : $mysqli->error));
            }
        }

        return $mysqli;
    }

    public static function validateLicense($content) {
        $parts = preg_split('/[\r\n]+/', $content);
        $licenseInfo = json_decode($parts[1], true);
        //var_dump($parts, \Packageinfo::instance()->get('name'));
        return $licenseInfo['package'] === \Packageinfo::instance()->get('name');
    }

    public static function writeDatabase(array $config, &$error) {
        $mysqli = @new mysqli($config['host'], $config['username'], $config['password'], $config['name'], intval($config['port'])? : 3306);
        if ($mysqli->connect_error) {
            $error = $mysqli->connect_error;
            return false;
        }

        $mysqli->close();
        //write config.
        $host = $config['port'] != 3306 ? "{$config['host']}:{$config['port']}" : $config['host'];
        $content = <<<DBCONFIG
<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

if (!defined('__LOCAL_')) {
    define('__LOCAL_', 22);
}

\$active_group = 'default';
\$active_record = TRUE;

\$db = array(
   'default' => array(
       'hostname' => '{$host}',
       'username' => '{$config['username']}',
       'password' => '{$config['password']}',
       'database' => '{$config['name']}',
       'dbdriver' => 'mysqli',
       'dbprefix' => '',
       'pconnect' => false,
       'char_set' => 'utf8',
       'dbcollat' => 'utf8_general_ci',
       'autoinit' => true,
   ),
);

DBCONFIG;

        return file_put_contents(CONFIG_DIR . 'database.php', $content);
    }

}

function setup_redirect($step) {
    header('Location: index.php?step=' . $step);
    exit;
}

function setup_make_baseurl() {
    $scheme = $_SERVER['HTTPS'] ? 'https' : 'http';
    $host = $_SERVER['HTTP_HOST'];
    $path = explode('/setup', $_SERVER['REQUEST_URI'])[0];
    $port = in_array($_SERVER['SERVER_PORT'], [80, 443]) ? '' : ":{$_SERVER['SERVER_PORT']}";
    return "{$scheme}://{$host}{$port}{$path}/";
}

if (substr(PHP_SAPI, 0, 3) != 'cli' && !file_exists(CONFIG_DIR . 'BASEURL')) {
    file_put_contents(CONFIG_DIR . 'BASEURL', setup_make_baseurl());
}

if (Setup::isSetup() && !defined('SKIP_SETUP_CHECK')) {
    header('Location: index.php?step=info');
    die();
}

