<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function db_util_insert(mysqli $db, $tableName, $rows = null, $replace = false, $is_multiple = false, $pk = null) {
    if (!count($rows)) {
        return null;
    }

    $action = $replace ? "REPLACE" : "INSERT";
    $q = "{$action} INTO `{$tableName}`";

    $fields = $is_multiple ? array_keys(end($rows)) : array_keys($rows);

    if (!$is_multiple) {
        $rows = array($rows);
    }
    $valueSet = array();
    foreach ($rows as $row) {
        $k = array();
        foreach ($row as $val) {
            if (is_object($val)) {
                $k[] = (string) $val;
            } elseif ($val === null) {
                $k[] = 'NULL';
            } else {
                $k[] = '\'' . $db->real_escape_string($val) . '\'';
            }
        }
        $valueSet[] = '(' . join(',', $k) . ')';
    }

    $values = join(', ', $valueSet);
    if (is_numeric($fields[0])) {
        //treat as values only
        $q .= " VALUES {$values}";
    } else {
        $fields = join(',', $fields);
        $q .= " ({$fields}) VALUES {$values}";
    }

    $successful = $db->query($q);
    if (!$successful) {
        return 0;
    }

    if ($is_multiple) {
        return $successful;
    }

    $insertId = $db->insert_id;
    return $pk && $row[$pk] ? $row[$pk] : $insertId;
}
