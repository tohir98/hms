<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace setup;

use Packageinfo;
use storage;
use user_auth;

//require APP_ROOT . 'application/libraries/packageinfo.php';
require_once 'FileUploadHandler.php';
require_once 'db_utils.php';
require_once APP_ROOT . 'application/libraries/storage.php';

/**
 * Description of CompanySetup
 *
 * @author JosephT
 */
class CompanySetup {

//create company 
//create user
//add user to admins
//create general role in roles
//create default department.
//insert employee record
//add modules.

    protected $companyData;
    protected $adminData;
    protected $deptData;

    /**
     *
     * @var FileUploadHandler
     */
    private $logoValidator;

    /**
     *
     * @var storage
     */
    private $storage;
    private $db;
    static $uploadErrors = array(
        FileUploadHandler::ERR_FILE_TOO_LARGE => 'Uploaded file is too large',
        FileUploadHandler::ERR_INVALID_TYPE => 'The uploaded file is not acceptable',
        FileUploadHandler::ERR_UPLOAD_FAILED => 'Upload failed',
        FileUploadHandler::ERR_NO_FILE_UPLOADED => 'You upload is empty.',
    );

    public function __construct($postData, FileUploadHandler $logoValidator = null) {
        $this->companyData = $postData['company'];
        $this->adminData = $postData['admin'];
        $this->deptData = $postData['dept'];
        $this->logoValidator = $logoValidator;

        include CONFIG_DIR . 'storage.php';
        $this->storage = new storage($config['storage'], true);
        $this->db = Setup::getDb();
    }

    public function process() {
        $this->db->autocommit(false);
        $result = false;
        if ($this->_process_all()) {
            $this->db->commit();
            $result = true;
        } else {
            $this->db->rollback();
        }
        $this->db->autocommit(true);
        return $result;
    }

    private function _error($error) {
        file_put_contents('install.log', $error . PHP_EOL, FILE_APPEND);
        file_put_contents('install.log', ' DB Error : ' . $this->db->error . PHP_EOL, FILE_APPEND);
        return false;
    }

    private function _process_all() {

        if (!($companyId = $this->createCompany())) {
            return $this->_error('company creation failed');
        }

        if (!$this->createCompanyContainer($companyId)) {
            return $this->_error('container creation failed.');
        }

        if (!($userId = $this->createAdminUser($companyId))) {
            return $this->_error('User creation failed');
        }

        if (!$this->createCompanyAdmin($userId, $companyId)) {
            return $this->_error('COULD NOT ADD ADMIN');
        }

        if (!($roleId = $this->createCompanyRole($companyId))) {
            return $this->_error('Default role could not be created');
        }

        if (!($deptId = $this->createDept($companyId))) {
            return $this->_error('Department could not added');
        }

        if (!$this->createEmployee($userId, $companyId, $roleId, $deptId)) {
            return $this->_error('employee data coud not be created');
        }

        if (!$this->addCompanyModules($companyId, $userId)) {
            return $this->_error('Modules could not be added.');
        }

        return true;
    }

    private function createAdminUser($companyId) {
        if (!class_exists('user_auth')) {
            include_once APP_ROOT . 'application/libraries/user_auth.php';
        }

        $this->adminData['password'] = user_auth::encrypt($this->adminData['password']);
        $this->adminData['status'] = USER_STATUS_ACTIVE;
        $this->adminData['id_string'] = $this->companyData['id_prefix'] . '000001';
        $this->adminData['date_registered'] = $this->now();
        $this->adminData['account_type'] = ACCOUNT_TYPE_ADMINISTRATOR;
        $this->adminData['id_company'] = $companyId;
        $this->adminData['id_gender'] = 0;
        $this->adminData['id_creator'] = 0;
        $this->adminData['id_company_location'] = 0;
        //$this->adminData['status'] = 1;

        return static::insert('users', $this->adminData);
    }

    private function createCompanyAdmin($userId, $companyId) {
        static::insert('admins', array(
            'id_user' => $userId,
            'id_company' => $companyId,
        ));

        return true;
    }

    private function createCompanyRole($companyId) {
        return static::insert('roles', array(
                    'id_company' => $companyId,
                    'roles' => 'General',
                    'date_created' => $this->now()
        ));
    }

    private function createDept($companyId) {
        return static::insert('departments', array(
                    'id_company' => $companyId,
                    'departments' => $this->deptData['department'],
                    'created' => $this->now(),
                    'id_head' => 0,
                    'id_parent' => 0
        ));
    }

    private function now() {
        return date('Y-m-d H:i:s');
    }

    private function createEmployee($userId, $companyId, $roleId, $deptId) {
        $data = array(
            'id_user' => $userId,
            'id_role' => $roleId,
            'id_company' => $companyId,
            'id_activated_by' => null,
            'id_dept' => $deptId,
            'id_manager' => $userId,
            'is_manager' => 1,
            'id_manager_vacation' => $userId,
            'id_access_level' => 1,
                //'id_country_code' => $phone_code,
                //'work_number' => $mobile
        );

        return static::insert('employees', $data, false, false, 'id_user');
    }

    private function createCompanyContainer($companyId) {
        $container = uniqid('container_');
        if ($this->storage->create_container($container)) {
            return $this->db->query('UPDATE companies SET cdn_container="' . $container . '" WHERE id_company = ' . intval($companyId));
        }
        return false;
    }

    private function createCompany() {
        if ($this->logoValidator->isValid()) {
            //save files            
            $logoPath = uniqid('logo_') . microtime(true) . '.' . $this->logoValidator->getFileExtension();
            if ($this->storage->getAdapter()->saveFile($this->logoValidator->getTmpFileName(), $logoPath, STORAGE_CONTAINER_ASSETS)) {
                $this->companyData['logo_path'] = $logoPath;
            } else {
                return false;
            }
        }

        $this->companyData['status'] = 1;
        $this->companyData['id_prefix'] = strtoupper(substr(preg_replace('/[^A-Za-z0-9]+/', '', $this->companyData['company_name']), 0, 3));
        $this->companyData['id_string'] = strtolower(preg_replace('/[a-zA-Z]+/', '', $this->companyData['company_name']));
        $this->companyData['cdn_container'] = '';
        return static::insert('companies', $this->companyData);
    }

    private function getModuleId($module) {
        static $modules = array();
        if (empty($modules)) {
            $result = $this->db->query('select id_string, id_module FROM modules');
            foreach ($result as $row) {
                $modules[$row['id_string']] = $row['id_module'];
            }
        }

        return $modules[$module];
    }

    private function getModuleAllModulePerms($modules) {
        $in_str = array('NULL');

        foreach ($modules as $id_str) {
            $in_str[] = $this->getModuleId($id_str);
        }

        $inList = join(', ', $in_str);

        $query = "select id_perm, id_module FROM module_perms WHERE id_module IN ({$inList})";
        return $this->db->query($query);
    }

    private function addCompanyModules($companyId, $user_id) {
        $myModules = Packageinfo::instance()->modules;
        $data = array();
        $userPerms = array();
        foreach ($myModules as $module) {
            // Check if module = LOANS
            // Build company modules
            $data[] = array(
                'id_module' => $this->getModuleId($module),
                'id_company' => $companyId
            );
        }

        foreach ($this->getModuleAllModulePerms($myModules) as $modulePerm) {
            $userPerms[] = array(
                'id_user' => $user_id,
                'id_perm' => $modulePerm['id_perm'],
                'id_module' => $modulePerm['id_module'],
                'id_company' => $companyId
            );
        }

        return $this->insert('company_modules', $data, false, true) &&
                static::insert('user_perms', $userPerms, false, true);
    }

    public function insert($tableName, $rows = null, $replace = false, $is_multiple = false, $pk = null) {
        return db_util_insert($this->db, $tableName, $rows, $replace, $is_multiple, $pk);
    }

    /**
     * 
     * @param array $data
     * @param type $error
     * @param FileUploadHandler $logoValidator
     */
    public static function validate(array &$data, &$error, &$logoValidator = null) {
        $required = array(
            'company' => ['company_name', 'banner_text', 'about_us', 'bg_color'],
            'logo_type',
            'dept' => ['department'],
            'admin' => ['first_name', 'last_name', 'email', 'password'],
                //'password_1',
        );

        if ($data['logo_type'] === 'text') {
            $required['company'][] = 'logo_text';
        } else {
            $logoValidator = new FileUploadHandler('logo_image_file', array(
                'max_file_size' => 102400, //100KB
                'allowed_extensions', ['jpg', 'gif', 'png', 'jpeg', 'bmp']
            ));
        }

        foreach ($required as $key => $field) {
            if (is_numeric($key) && !$data[$field]) {
                $error = self::_fieldToName($field) . ' is required.';
                return false;
            }

            if (is_array($field)) {
                foreach ($field as $fld) {
                    if (!trim($data[$key][$fld])) {
                        $error = self::_fieldToName($fld) . ' is required.';
                        return false;
                    }
                }
            }
        }

        if ($logoValidator && !$logoValidator->validateUpload()) {
            $error = self::$uploadErrors[$logoValidator->error];
            return false;
        }

        if (!filter_var($data['admin']['email'], FILTER_VALIDATE_EMAIL)) {
            $error = 'Invalid email supplied';
            return false;
        }

        if ($data['password_1'] != $data['admin']['password']) {
            $error = 'Passwords do not match';
            return false;
        }

        return true;
    }

    public static function _fieldToName($field) {
        $parts = explode('_', $field);
        foreach ($parts as $i => $prt) {
            $parts[$i] = ucfirst($prt);
        }

        return join(' ', $parts);
    }

}
