<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace setup;

if (!Setup::isSetup('storage')) {
    setup_redirect('storage');
}

if(Setup::isSetup('email')){
    setup_redirect('company');
}

$error = '';
$data = [];

$page['title'] = 'Configure E-Mail';

$protocols = array(
    'smtp' => 'SMPT (Recommended)',
    'sendmail' => 'Sendmail',
    'mail' => 'PHP mail Function',
);



if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $config = [];
    $config['mailtype'] = 'html';
    $config['charset'] = 'utf-8';
    $config['bcc_batch_mode'] = true;


    $emailConfig = filter_input_array(INPUT_POST);

    if (!empty($emailConfig) && is_array($emailConfig)) {
        $emailConfig = array_merge($config, $emailConfig);
        $content = "<?php\n\n"
                . "\$emailConfig = "
                . var_export($emailConfig, true)
                . ";\n\n"
                . '$emailConfig[\'newline\'] = "\r\n";'
                . PHP_EOL
                . '$config= array_merge((array) $config, $emailConfig);'
                . PHP_EOL
        ;

        if (file_put_contents(CONFIG_DIR . 'email.php', $content)) {
            setup_redirect('company');
        }

        $error = 'Could not save email config.';
    }
}

$page['callback'] = function() use ($data, $error, $protocols) {
    ?>
    <form class="form-horizontal" method="POST" action="">
        <div class="panel panel-primary" >
            <div class="panel-heading">
                <h1 class="panel-title">Mail Settings</h1>
            </div>
            <div class="panel-body">
                <?= display_error($error); ?>
                <div class="form-group">
                    <label class="col-md-3 control-label">Mail Protocol</label> 
                    <div class="controls col-md-4">
                        <select class="form-control input-sm" required="" name="protocol" id='protocol'>
                            <option></option>
                            <?php foreach ($protocols as $protocol => $name): ?>
                                <option value="<?= $protocol ?>" <?= $protocol == $data['protocol'] ? 'selected=""' : '' ?>><?= $name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <fieldset>
                    <!--                    <legend>Storage Options</legend>-->
                    <div class="protocol-options" id="sendmail">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Mail Path</label> 
                            <div class="controls col-md-4">
                                <input type="text" required="" class="form-control input-sm" name="mailpath" value="<?= $data['mailpath'] ?>"/>
                            </div>
                        </div>
                    </div>

                    <div class="protocol-options" id="smtp">
                        <div class="form-group">
                            <label class="col-md-3 control-label">SMTP Host</label> 
                            <div class="controls col-md-4">
                                <input type="text" class="form-control input-sm" name="smtp_host" value="<?= $data['smtp_host'] ?>" required=""/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">User</label> 
                            <div class="controls col-md-4">
                                <input type="text" class="form-control input-sm" name="smtp_user" value="<?= $data['smtp_user'] ?>" required=""/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Password</label> 
                            <div class="controls col-md-4">
                                <input type="password" class="form-control input-sm" name="smtp_pass"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Port</label> 
                            <div class="controls col-md-2">
                                <input type="number" class="form-control input-sm" name="smtp_port" value="<?= $data['smtp_port'] ? : 25 ?>" required=""/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">SMTP Encryption</label> 
                            <div class="controls col-md-2">
                                <select class="form-control input-sm" name="smtp_crypto" id='smtp_crypto'>
                                    <option></option>
                                    <?php foreach (['' => 'None', 'tls' => 'TLS', 'ssl' => 'SSL'] as $secure => $name): ?>
                                        <option value="<?= $secure ?>" <?= $secure == $data['smtp_crypto'] ? 'selected=""' : '' ?>>
                                            <?= $name ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                    </div>
                </fieldset>
            </div>
            <div class="panel-footer text-right">
                <button type="submit" class="btn btn-success">Save Config</button>
            </div>
        </div>
    </form>
    <script>
        $(function () {
            $('#protocol').change(function () {
                $('.protocol-options input,.protocol-options select').attr('disabled', '');
                $('.protocol-options').hide();
                var id = $(this).val();
                $('#' + id).show()
                        .find('input,select').removeAttr('disabled');
            }).change();
        });
    </script>
    <?php
};
