<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace setup;

use Packageinfo;

if (!Setup::isSetup('company')) {
    setup_redirect('company');
}

if (!filter_has_var(INPUT_GET, 'redo') && Setup::isSetup('license')) {
    setup_redirect('info');
}

$page['title'] = 'Activate Application';

$data = [];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = filter_input_array(INPUT_POST, array(
        'key' => FILTER_DEFAULT,
        'email' => FILTER_VALIDATE_EMAIL
    ));

    $error = '';
    if ($data['email'] && $data['key']) {
        $context = stream_context_create(array(
            'http' => array(
                'method' => 'POST',
                'content' => http_build_query($data),
                'header' => "Content-Type: application/x-www-form-urlencoded",
                'timeout' => 15,
                'ignore_errors' => true,
            )
        ));

        $content = file_get_contents(Packageinfo::instance()->get('server') . 'license/activate', false, $context);
        $statusCode = 0;
        foreach ($http_response_header as $header) {
            if (preg_match('/^HTTP\/1\.\d/', $header)) {
                $statusCode = intval(preg_split('/\s+/', $header)[1]);
                break;
            }
        }

        if ($statusCode === 200) {
            if (Setup::validateLicense($content) && Setup::saveLicense($content)) {
                setup_redirect('info');
            }
            //write license
            $error = 'The license is not for this package.';
        } else {
            $error = $content;
        }
    } else {
        $error = 'Ensure all fields are properly filled';
    }
}


$page['callback'] = function() use ($data, $error) {
    ?>

    <?= display_error($error); ?>
    <form class="form-horizontal" method="POST" action="">
        <div class="panel panel-primary"> 
            <div class="panel-heading">
                <h2 class="panel-title">License Information</h2>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-md-4">License Key</label>
                    <div  class="col-md-6">
                        <input class="form-control" type="text" name="key" value="<?= $data['key'] ?>" required=""/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-4">E-Mail</label>
                    <div  class="col-md-5">
                        <input class="form-control" type="email" name="email" value="<?= $data['email'] ?>" required=""/>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-right">
                <button type="submit" class="btn btn-success">Activate Application</button>
            </div>
        </div>
    </form>
    <?php
};
