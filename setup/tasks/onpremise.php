<?php

use Robo\Task\Shared\TaskInterface;
use setup\Setup;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


require_once __DIR__ . '/../Setup.php';
require_once APP_ROOT . 'application/libraries/zipper.php';

abstract class OnpremiseBaseTask implements TaskInterface {

    protected $runId;

    /**
     *
     * @var RoboFile
     */
    protected $roboFile;

    /**
     *
     * @var Packageinfo
     */
    protected $packageInfo;
    protected $licenseInfo;
    protected $tempPath;

    public function __construct(RoboFile $roboFile, $temp = '') {
        $this->packageInfo = Packageinfo::instance();

        if (!$this->packageInfo->name) {
            throw new RuntimeException('Package info missing, probably not premise version.');
        }

        $this->licenseInfo = (array) Setup::licenseInfo();
        $this->runId = uniqid('task');
        $this->roboFile = $roboFile;
        $this->tempPath = $temp && is_dir($temp) && is_writable($temp) ? $temp : sys_get_temp_dir();
    }

    public function say() {
        echo '{', get_class($this), '} -> ', join(' ', func_get_args()), PHP_EOL;
    }

    protected function _result($code, $message = '') {
        return new Robo\Result($this, $code, $message);
    }

    protected function composerCheck() {
        if (!file_exists($this->_composerPath())) {
            $exec = new \Robo\Task\ExecTask('php -r "readfile(\'https://getcomposer.org/installer\');" | php -- --install-dir="' . APP_ROOT . '"');
            return $exec->run()->wasSuccessful();
        }
        return true;
    }

    protected function _composerPath() {
        return APP_ROOT . 'composer.phar';
    }

}

class OnpremiseBackupTask extends OnpremiseBaseTask {

    protected $backupDir;

    public function __construct(\RoboFile $roboFile, $temp = '') {
        parent::__construct($roboFile, $temp);
    }

    public function getBackupDir() {
        return $this->backupDir;
    }

    public function setBackupDir($backDir) {
        $this->backupDir = realpath($backDir);
        return $this;
    }

    protected function backup() {
        require CONFIG_DIR . 'storage.php';
        $execTask = new Robo\Task\ExecStackTask();


        if (isset($config['storage']) && $config['storage']['adapter'] === 'localstorage') {
            $files_dir = $config['storage']['localstorage']['dir'];
            if (is_dir($files_dir) && ($files_dir = realpath($files_dir))) {
                $this->say('Backing up files dir: ' . $files_dir);
                $backupName = $this->backupDir . '/tbapp_files_' . strtolower(date('l')) . '.tar.gz';
                $execTask->exec("tar -czf {$backupName} {$files_dir}");
            }
        }


        require CONFIG_DIR . 'database.php';
        $dbMain = $db['default'];
        list($host, $port) = explode(':', $dbMain['hostname']);
        $port = intval($port) ? : 3306;
        
        $targetfile = $this->backupDir . '/tbapp_database_' . strtolower(date('l')) .'sql.gz';
        $command = "mysqldump -h{$host} -u{$dbMain['username']} -p{$dbMain['password']} -P{$port} {$dbMain['database']} | gzip >> {$targetfile}";
        
        $execTask->exec($command);


        return $execTask->run();
    }

    public function run() {
        return $this->backup();
    }

}

/**
 * Description of OnpremiseTasks
 *
 * Required: unzip composer git
 * @author JosephT
 */
class OnpremiseUpdateTask extends OnpremiseBaseTask {

    use \Robo\Task\Composer;

    private $autoUpdate = true;

    public function autoUpdate($bool = true) {
        $this->autoUpdate = $bool;
        return $this;
    }

    private function checkUpdate() {
        //check for available update
        $version = null;
        //$result = null;
        if ($this->isUpdateAvailable($version)) {
            //perform update.
            if (!$this->autoUpdate) {
                return $this->_result(-1, 'Update notification has not been fixed, auto-update only for now.');
            }

            if (!$this->composerCheck()) {
                return $this->_result(-1, 'Composer could not be verified, might be needed.');
            }

            //download update.
            if (!$this->downloadUpdate($version)) {
                return $this->_result(-2, 'Download of latest update failed.');
            }

            if (!$this->performBackup()) {
                return $this->_result(-3, ' Could not back up current folder.');
            }

            $rollback = true;

            if (($updateResult = $this->performUpdate($rollback)) && $updateResult->wasSuccessful()) {
                $this->say('Update was successful.');
                return $updateResult;
            } elseif ($rollback) {
                $this->say('Update failed, performing rollback, not guranteeable though.');
                //perform rollback
                return $this->performRollback();
            }

            return $updateResult;
        } else {
            return $this->_result(0, 'Application is up to date');
        }
    }

    private function performBackup() {
        $this->say('Creating backup of existing code.');
        Zipper::zipDir(APP_ROOT, $this->_backupFile(), false, ['vendor', 'files', 'application/logs', 'cron/logs']);
        $this->say('Backup completed successfully.');
        return true;
    }

    /**
     * @return Robo\Result Description
     */
    private function performUpdate(&$doRollback = true) {
        $extractFiles = new \Robo\Task\ExecTask('unzip -o -q ' . $this->_updateFile());
        //extract files.
        if (!$extractFiles->run()->wasSuccessful()) {
            $doRollback = false;
            return $this->_result(-2, 'File extraction failed.');
        }

        //run composer update
        $commandStack = new \Robo\Task\ExecStackTask();

        $commandStack->stopOnFail(true);

        if ($this->roboFile->dbMigrate('default', ['start' => 400, 'mode' => DBMigrationMode::NON_INTERACTIVE]) !== 0) {
            return $this->_result(-100, 'DB migration failed, do rolback.');
        }

        $result = $commandStack->exec("php {$this->_composerPath()} install")
                ->exec("php {$this->_composerPath()} update")
                ->run()
        ;

        if (!$result->wasSuccessful()) {
            $this->say('Composer update faiiled');
            return $result;
        }

        return $this->_result(0, 'Successfully completed.');
    }

    private function performRollback() {
        $this->say('Performing rollback from back up file.');
        //report failure
        $extractFiles = new \Robo\Task\ExecTask('unzip -o -q ' . $this->_backupFile());
        $this->say('Update has failed, report failure.');
        $result = $extractFiles->run();
        $this->say('Rollback completed: ', $result->wasSuccessful() ? 'Successfully' : ' With issues');
        return $result;
    }

    private function isUpdateAvailable(&$version = null) {
        $package = $this->packageInfo->get('name');
        $version = TB_VERSION;
        $server = $this->packageInfo->server;
        //check update 
        $checkUrl = $server
                . 'release/update_available?'
                . http_build_query(['version' => $version, 'package' => $package]);

        $updateResult = json_decode(file_get_contents($checkUrl));
        if ($updateResult && $updateResult->available && ($version = $updateResult->version->version)) {
            $this->say('New update available, version: ', $updateResult->version->version, 'released: ', date('M d, y', strtotime($updateResult->version->release_date)));
            return true;
        }
        return false;
    }

    private function downloadUpdate($version) {
        $dlUrl = sprintf("%srelease/download/%s?key=%s&version=%s", $this->packageInfo->server, $this->packageInfo->name, urlencode($this->licenseInfo['key']), urlencode($version));

        $this->say('Downloading from ' . $dlUrl);
        return file_put_contents($this->_updateFile(), fopen($dlUrl, 'r'));
    }

    private function _updateFile() {
        return $this->tempPath . DIRECTORY_SEPARATOR . 'tbaseUpdate.' . $this->runId . '.zip';
    }

    private function _backupFile() {
        return $this->tempPath . DIRECTORY_SEPARATOR . 'tbaseAppBackup.' . $this->runId . '.zip';
    }

    public function cleanUp() {
        if (is_file($this->_updateFile())) {
            @unlink($this->_updateFile());
        }

        if (file_exists($this->_backupFile())) {
            @unlink($this->_backupFile());
        }
    }

    public function run() {
        return $this->checkUpdate();
    }

}
