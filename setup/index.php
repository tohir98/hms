<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace setup;

require_once 'Setup.php';


$step = filter_input(INPUT_GET, 'step', FILTER_SANITIZE_STRING) ? : 'db';

$page = array();

if(!Setup::isStep($step)){
    $step = 'db';
}

include_once __DIR__ . "/setup_{$step}.php";
_display($page);