<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace setup;

$missing = Setup::checkMissingExtensions();
$page['title'] = 'Database Setup';
$data = [];

if (Setup::isSetup('db')) {
    setup_redirect('storage');
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $inData = filter_input_array(INPUT_POST, array(
        'db' => array('flags' => FILTER_REQUIRE_ARRAY)
    ));

    if ($inData['db']) {
        $data = $inData['db'];
    }

    $error = null;
    if (Setup::writeDatabase($data, $error)) {
        ob_start();
        $status = include_once APP_ROOT . 'dbvc/migration.php';
        $log = ob_get_clean();
        file_put_contents('setup.db.log', $log, FILE_APPEND);

        if ($status == 0) {
            setup_redirect('storage');
        }

        $error = 'Database could not be intialized, see setup_db.log for details';
        rename(CONFIG_DIR . 'database.php', CONFIG_DIR . 'database.failed.' . time() . '.php');
    }
}

$page['callback'] = function() use ($missing, $data, $error) {
    //$hasMissingExtensions = count($missing) > 0;
    ?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h2 class="panel-title">
                Required Extensions
            </h2>
        </div>
        <div class="panel-body">
            <div class="row">
                <?php
                foreach (Setup::$extensions as $extension) :
                    $extension = is_array($extension) ? join('|', $extension) : $extension;
                    ?>
        <?php $isMissing = in_array($extension, $missing); ?>
                    <div class="col-md-3">
                        <span class="label label-<?= $isMissing ? 'danger' : 'success' ?>"><i class="glyphicon glyphicon-<?= $isMissing ? 'remove' : 'ok' ?>"></i></span> <?= $extension; ?>
                    </div>
    <?php endforeach; ?>
            </div>
        </div>
    </div>
    <?php
    if (count($missing)):
        ?>
        <div class="alert alert-warning">
            Install missing extensions to continue.
        </div>
    <?php else: ?>
        <?= display_error($error, 'Connection error, please review:') ?>
        <form class="form form-horizontal" method="post" action="">
            <div class="panel panel-primary">
                <div class="panel panel-heading">
                    <h2 class="panel-title">Setup Database</h2>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Database Host</label> 
                        <div class="controls col-md-4">
                            <input type="text" name="db[host]" required="" value="<?= $data['host'] ?>" class="form-control input-sm"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Username</label> 
                        <div class="controls col-md-4">
                            <input type="text" name="db[username]" required="" value="<?= $data['username'] ?>" class="form-control input-sm"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Password</label> 
                        <div class="controls col-md-4">
                            <input type="password" name="db[password]" value="" class="form-control input-sm"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Database Name</label> 
                        <div class="controls col-md-4">
                            <input type="text" name="db[name]" value="<?= $data['name'] ?>" class="form-control input-sm"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Port</label> 
                        <div class="controls col-md-2">
                            <input type="number" name="db[port]" value="<?= $data['port'] ? : 3306 ?>" class="form-control input-sm"/>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-right">
                    <button class="btn btn-success" type="submit">Save DB Config</button>
                </div>
            </div>
        </form>
    <?php endif; ?>
    <?php
};
